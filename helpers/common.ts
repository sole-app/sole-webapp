class CommonHelper {
    static formatDate = (date) => {
        let d = new Date(date);
        let ye = new Intl.DateTimeFormat('en', { year: 'numeric' }).format(d);
        let mo = new Intl.DateTimeFormat('en', { month: 'long' }).format(d);
        let da = new Intl.DateTimeFormat('en', { day: '2-digit' }).format(d);

        return `${da} ${mo}, ${ye}`
    };

    static notificationData = (date) => {
        let d = new Date(date);
        let M = 'AM';
        let ho = d.getHours();
        let min = d.getMinutes();
        if (ho > 12) {
            ho = ho - 12;
            M = 'PM';
        }

        let ye = new Intl.DateTimeFormat('en', { year: 'numeric' }).format(d);
        let mo = new Intl.DateTimeFormat('en', { month: 'long' }).format(d);
        let da = new Intl.DateTimeFormat('en', { day: '2-digit' }).format(d);
        let hos = String(ho).padStart(2, '0')
        let mins = String(min).padStart(2, '0')

        return `${mo} ${da}, ${ye} at ${hos}:${mins} ${M}`
    };

    static lastSentDate = (date) => {
        let d = new Date(date);
        let M = 'AM';
        let ho = d.getHours();
        let min = d.getMinutes();
        if (ho > 12) {
            ho = ho - 12;
            M = 'PM';
        }
        const weekday = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
        let dayString = weekday[d.getDay()];

        let ye = new Intl.DateTimeFormat('en', { year: 'numeric' }).format(d);
        let mo = new Intl.DateTimeFormat('en', { month: 'short' }).format(d);
        let da = new Intl.DateTimeFormat('en', { day: '2-digit' }).format(d);

        return `${dayString} ${da} ${mo} ${ye} ${ho}:${min} ${M}`
    };

    static formatShortDate = (date) => {
        let d = new Date(date);
        let ye = new Intl.DateTimeFormat('en', { year: 'numeric' }).format(d);
        let mo = new Intl.DateTimeFormat('en', { month: '2-digit' }).format(d);
        let da = new Intl.DateTimeFormat('en', { day: '2-digit' }).format(d);

        return `${da}-${mo}-${ye}`
    };

    static formatDateToDB = (date) => {
        let d = new Date(date);
        let ye = new Intl.DateTimeFormat('en', { year: 'numeric' }).format(d);
        let mo = new Intl.DateTimeFormat('en', { month: '2-digit' }).format(d);
        let da = new Intl.DateTimeFormat('en', { day: '2-digit' }).format(d);

        return `${ye}-${mo}-${da}`
    };

    static formatCurrency = (amount, decimal = 2) => {
        let formatter = new Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'USD',

            // These options are needed to round to whole numbers if that's what you want.
            //minimumFractionDigits: 0, // (this suffices for whole numbers, but will print 2500.10 as $2,500.1)
            maximumFractionDigits: decimal, // (causes 2500.99 to be printed as $2,501)
        });

        return formatter.format(amount);
    };

    static formatCurrencyWithoutSymbol = (amount, decimal = 2) => {
        let formatter = new Intl.NumberFormat('en-US', {
            // These options are needed to round to whole numbers if that's what you want.
            //minimumFractionDigits: 0, // (this suffices for whole numbers, but will print 2500.10 as $2,500.1)
            maximumFractionDigits: decimal, // (causes 2500.99 to be printed as $2,501)
        });

        return formatter.format(amount);
    };

    static getCurrentDate = () => {
        let today = new Date();
        return today.getFullYear() + '-' + String(today.getMonth() + 1).padStart(2, '0') + '-' + String(today.getDate()).padStart(2, '0');
    };

    static getCurrentFY = () => {
        let today = new Date();
        let year = today.getFullYear()
        if ((today.getMonth() + 1) > 6) {
            return 'Current FY ' + (year).toString().substr(2) + '/' + (year + 1).toString().substr(2)
        }
        return 'Current FY ' + (year - 1).toString().substr(2) + '/' + (year).toString().substr(2)
    };

    static getFYList = () => {
        let list = [];
        let today = new Date();
        let year = today.getFullYear();
        if ((today.getMonth() + 1) > 6) {
            list.push('Current FY ' + (year).toString().substr(2) + '/' + (year + 1).toString().substr(2))
            list.push('Previous FY ' + (year - 1).toString().substr(2) + '/' + (year).toString().substr(2))
        } else {
            list.push('Current FY ' + (year - 1).toString().substr(2) + '/' + (year).toString().substr(2))
            list.push('Previous FY ' + (year - 2).toString().substr(2) + '/' + (year - 1).toString().substr(2))
        }
        return list
    };

    static getDateFromFYYears = (year) => {
        let dates = {
            'start_date': '',
            'end_date': ''
        };
        year = year.replace("Current FY ", "");
        year = year.replace("Previous FY ", "");
        let years = year.split("/");
        if (typeof years[0] !== 'undefined' && !isNaN(years[0])) {
            dates = {
                'start_date': '20' + years[0] + '-07-01',
                'end_date': '20' + (parseInt(years[0]) + 1) + '-06-30'
            }
        }
        return dates
    };

    static getCurrentPeriod = (date, type) => {
        let period = ''
        let d = new Date(date)
        if (type == 'MONTHLY') {
            var newDate = new Date(d.setMonth(d.getMonth() - 1))
        } else {
            var newDate = new Date(d.setMonth(d.getMonth() - 12))
        }
        period += CommonHelper.formatDate(newDate) + ' - ' + CommonHelper.formatDate(date)
        return period;
    }

    static getDateFromFYDates = (type = 'YEAR') => {
        let dates = {
            'start_date': '',
            'end_date': ''
        };
        let today = new Date();
        let year = today.getFullYear();
        if (type == 'YEAR') {
            if ((today.getMonth() + 1) > 6) {
                dates.start_date = (year).toString() + '-07-01';
                dates.end_date = (year + 1).toString() + '-06-30';
            } else {
                dates.start_date = (year - 1).toString() + '-07-01';
                dates.end_date = (year).toString() + '-06-30';
            }
        } else {
            let month = today.getMonth() + 1;
            let monthString = month.toString();
            let date = CommonHelper.lastday(year, month);
            let dateString = date.toString(); 
            if (month < 10) {
                monthString = '0' + month;
            }
            if (date < 10) {
                dateString = '0' + date;
            }
            dates.start_date = (year).toString() + '-' + monthString + '-01';
            dates.end_date = (year).toString() + '-' + monthString + '-' + dateString;
        }

        return dates
    };

    static lastday = (y,m) => {
        return new Date(y, m, 0).getDate();
    }

    static getFileName(url) {
        if (url) {
          let filename = url.substring(url.lastIndexOf('/') + 1);
          return filename;
        }
        return "";
    }
}

export default CommonHelper;