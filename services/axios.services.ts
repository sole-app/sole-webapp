import CookiesServices from "./cookies.services";
import config from '../config/config'
import axios from 'axios';
import store from '../store/store';
import { snackbarOpen } from '../reducers/slices/snackbarSlice';
import { SNACKBAR_TYPE } from "../components/Layout/Snackbar";
import { setTokenExpired } from "../store/actions";

class AxiosService {
    static getUserAuthHeader = () => {
        const token = CookiesServices.getAuthToken();
        return {
            apiKey: config.apiKey,
            Authorization: 'Bearer ' + token,
        };
    };

    static getApiKeyHeader = () => {
        return {
            'apiKey': config.apiKey
        };
    };

    static post = async (url: string, params = {}, successMsg = false, errorMsg = true) => {
        const authHeaders = AxiosService.getUserAuthHeader();
        authHeaders['content-type'] = 'multipart/form-data';
        const rawResult = await axios.post(url, params, {
            headers: authHeaders,
        });
        if (rawResult.status === 200) {
            let result = rawResult.data;
            if (result.success) {
                if (successMsg) {
                    store.store.dispatch(snackbarOpen({
                        type: SNACKBAR_TYPE.SUCCESS,
                        message: result.message,
                    }));
                }
            }
            else {
                if (result.message !== 'token_expired') {
                    if (errorMsg) {
                        if (result.message !== 'Additional Input Required') {
                            store.store.dispatch(snackbarOpen({
                                type: SNACKBAR_TYPE.ERROR,
                                message: result.message,
                            }));
                        }
                    }
                } else {
                    store.store.dispatch(setTokenExpired(true));
                }
                
                /*if (result.message == 'token_expired') {
                    CookiesServices.deleteAll();
                    localStorage.clear();
                    location.reload();
                }*/
            }
            return result;
        }
    };

    static get = async (url: string, params = '', successMsg = false, errorMsg = true) => {
        const authHeaders = AxiosService.getUserAuthHeader();
        authHeaders['content-type'] = 'multipart/form-data';
        const rawResult = await axios.get(url + params, {
            headers: authHeaders,
        });
        if (rawResult.status === 200) {
            let result = rawResult.data;
            if (result.success) {
                if (successMsg) {
                    store.store.dispatch(snackbarOpen({
                        type: SNACKBAR_TYPE.SUCCESS,
                        message: result.message,
                    }));
                }
            }
            else {
                if (result.message !== 'token_expired') {
                    if (errorMsg) {
                        store.store.dispatch(snackbarOpen({
                            type: SNACKBAR_TYPE.ERROR,
                            message: result.message,
                        }));
                    }
                } else {
                    store.store.dispatch(setTokenExpired(true));
                }            
                
                /*if (result.message == 'token_expired') {
                    CookiesServices.deleteAll();
                    localStorage.clear();
                    location.reload();
                }*/
            }
            return result;
        }
    };

    static postSync = async (url: string, params = {}) => {
        const authHeaders = AxiosService.getUserAuthHeader();
        authHeaders['content-type'] = 'multipart/form-data';
        axios.post(url, params, { headers: authHeaders});
    };
}

export default AxiosService;