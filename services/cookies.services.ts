import Cookies from 'js-cookie';
import store from '../store/store';
import { addUser, removeUser, addSubscription, setRefreshSubscription } from '../store/actions';

export const TOKEN_STORAGE_KEY = 'token';
export const USER_STORAGE_KEY = 'userData';
export const USER_EMAIL_KEY = 'userEmail';
export const DEVICE_ID = 'deviceId';

export const AUDITOR = 'auditor';
export const ACCOUNTANT_NAME = 'actName';
export const ACCOUNTANT_EMAIL = 'actEmail';
export const BOOKKEEPER_NAME = 'bperName';
export const BOOKKEEPER_EMAIL = 'bperEmail';

class CookiesServices {
    static get(key) {
        return Cookies.get(key);
    }

    static set(key, value) {
        Cookies.set(key, value);
    }

    static getDeviceId() {
        return CookiesServices.get(DEVICE_ID);
    }

    static setDeviceId(id) {
        CookiesServices.set(DEVICE_ID, id);
    }

    static getAuthToken() {
        return CookiesServices.get(TOKEN_STORAGE_KEY);
    }

    static setAuthToken(token) {
        CookiesServices.set(TOKEN_STORAGE_KEY, token);
    }

    static getUserEmail() {
        var statsFromRedux = store.store.getState();

        return statsFromRedux.user_details.email;
    }

    static setUserEmail(email) {
        // CookiesServices.set(USER_EMAIL_KEY, email);
    }

    static setLoginResponse(user) {
        CookiesServices.setAuthToken(user.token);
        CookiesServices.setUserData(user);
    }

    static getUserData() {
        var statsFromRedux = store.store.getState();

        return statsFromRedux.user_details;
    }

    static updateSubscription(subscription) {
        CookiesServices.setSubscription(subscription);
    }

    static setSubscription(subscription) {
        store.store.dispatch(addSubscription(subscription));
        store.store.dispatch(setRefreshSubscription());
    }

    static setUserData(user) {
        store.store.dispatch(addUser(user));
    }

    static setAuditorData(data) {
        CookiesServices.set(AUDITOR, data);
    }

    static getAuditorData() {
        let auditorData = CookiesServices.get(AUDITOR);
        if (auditorData != '') {
            try {
                auditorData = JSON.parse(auditorData);
            } catch (e) { }
        }
        return auditorData;
    }

    static getAccountantName() {
        const auditor = CookiesServices.getAuditorData();

        if (auditor != null) {
            return auditor.accountant != null ? auditor.accountant.name : '';
        }
        else {
            return '';
        }
    }

    static getAccountantEmail() {
        const auditor = CookiesServices.getAuditorData();

        if (auditor != null) {
            return auditor.accountant != null ? auditor.accountant.email : '';
        }
        else {
            return '';
        }
    }

    static getBookkeeperName() {
        const auditor = CookiesServices.getAuditorData();

        if (auditor != null) {
            return auditor.bookkeeper != null ? auditor.bookkeeper.name : '';
        }
        else {
            return '';
        }
    }

    static getBookkeeperEmail() {
        const auditor = CookiesServices.getAuditorData();

        if (auditor != null) {
            return auditor.bookkeeper != null ? auditor.bookkeeper.email : '';
        }
        else {
            return '';
        }
    }

    static deleteAll() {
        Object.keys(Cookies.get()).forEach(function (cookieName) {
            var neededAttributes = {
                // Here you pass the same attributes that were used when the cookie was created
                // and are required when removing the cookie
            };
            Cookies.remove(cookieName, neededAttributes);
        });
        store.store.dispatch(removeUser());
    }

    static getUserName() {
        let userData = CookiesServices.getUserData();
        return (userData != '') ? userData.full_name : 'Sole';
    }

    static setBank(bank) {
        let userData = CookiesServices.getUserData();
        if (userData != '') {
            if (typeof userData.bank !== 'undefined' && typeof userData.bank.account_holder !== 'undefined') {
                if (Object.keys(bank).length > 0) {
                    userData.bank.account_holder = bank.account_holder;
                    userData.bank.account_number = bank.account_number;
                    userData.bank.bank_name = bank.bank_name;
                    userData.bank.bsb = bank.bsb_number;
                } else {
                    userData.bank.account_holder = '';
                    userData.bank.account_number = '';
                    userData.bank.bank_name = '';
                    userData.bank.bsb = '';
                }
            }
        }
        CookiesServices.setUserData(userData);
    }

    static setUserName(fname, lname) {
        let userData = CookiesServices.getUserData();
        userData.first_name = fname;
        userData.last_name = lname;
        userData.full_name = fname + ' ' + lname;
        userData.full_name = userData.full_name.trim();
        CookiesServices.setUserData(userData);
    }
}

export default CookiesServices;