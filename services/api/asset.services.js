import { get, post } from '../axios.services';
import apiRoutes from '../../config/apiRoutes';

export const List = async (param) => {
    return get(apiRoutes.asset.list, param);
};

export const Add = async (param) => {
    return post(apiRoutes.asset.add, param, false, false);
};

export const Update = async (param) => {
    return post(apiRoutes.asset.update, param, false, false);
};

export const Delete = async (param) => {
    return post(apiRoutes.asset.delete, param, true);
};

export const Info = async (param) => {
    return get(apiRoutes.asset.info, param);
};