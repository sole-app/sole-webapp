import { post, postSync } from '../axios.services';
import apiRoutes from '../../config/apiRoutes';

export const getBankLists = async (param = {}) => {
    return post(apiRoutes.bankConnection.getInstituteList, param, false, true);
};

export const createCustomer = async (param = {}) => {
    return post(apiRoutes.bankConnection.createCustomer, param, false, false);
};

export const listAccounts = async (param = {}) => {
    return post(apiRoutes.bankConnection.listAccounts, param);
};

export const selectAccount = async (param = {}) => {
    return post(apiRoutes.bankConnection.selectAccount, param);
};

export const updateCustomer = async (param = {}) => {
    return post(apiRoutes.bankConnection.updateCustomer, param, false, false);
};

export const additionalAuth = async (param = {}) => {
    return post(apiRoutes.bankConnection.additionalAuth, param);
};

export const refreshAccountData = (param = {}) => {
    postSync(apiRoutes.bankConnection.refreshAccountData, param);
};