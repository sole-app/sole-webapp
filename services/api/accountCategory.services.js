import axios from 'axios';
import { getUserAuthHeader } from '../axios.services';
import apiRoutes from '../../config/apiRoutes';

export const getSpecificCategories = async (parentCategoryIds = []) => {
    const cat = await list();
    let categories = [];
    for (let i = 0; i < cat.length; i++) {
        if (parentCategoryIds.indexOf(cat[i].account_category_id) !== -1) {
            for (let j = 0; j < cat[i].account_subcategories.length; j++) {
                let temp = cat[i].account_subcategories[j];
                temp['parent_category'] = cat[i]['name'];
                categories.push(temp);
            }
        }
    }

    return categories;
};

export const getAllCategories = async () => {
    const cat = await list();
    let categories = {};
    for (let i = 0; i < cat.length; i++) {
        categories = cat;
    }

    return categories;
};

export const list = async (formData = {}) => {
    let categories = localStorage.getItem("CATEGORIES") || '';
    if (categories === '') {
        let api = await getList(formData);
        if (api.success) {
            categories = JSON.stringify(api.data.account_categories);
            localStorage.setItem("CATEGORIES", categories);
        }
    }

    return JSON.parse(categories) || [];
};

export const getList = async (formData = {}) => {
    const authHeaders = getUserAuthHeader();
    authHeaders['content-type'] = 'multipart/form-data';
    const rawResult = await axios.get(apiRoutes.accountCategory.list, {
        headers: authHeaders,
    });
    if (rawResult.status === 200) {
        return rawResult.data;
    }
};