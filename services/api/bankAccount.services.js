import { post } from '../axios.services';
import apiRoutes from '../../config/apiRoutes';

export const listAccounts = async (formData = {}) => {
    return post(apiRoutes.bankAccount.list, formData);
};

export const deleteAccount = async (formData = {}) => {
    return post(apiRoutes.bankAccount.delete, formData);
};

export const makePrimary = async (formData = {}) => {
    return post(apiRoutes.bankAccount.makePrimary, formData);
};