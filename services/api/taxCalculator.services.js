import { post } from '../axios.services';
import apiRoutes from '../../config/apiRoutes';

export const getFinancialYears = async (param) => {
    return post(apiRoutes.tax.getFinancialYears, param);
};

export const getTaxCalculation = async (param) => {
    return post(apiRoutes.tax.getTaxCalculation, param);
};

export const calculateTax = async (param) => {
    return post(apiRoutes.tax.calculateTax, param, false, false);
};

export const getIncomeByFinYear = async (param) => {
    return post(apiRoutes.tax.income, param);
};

export const getExpenseByFinYear = async (param) => {
    return post(apiRoutes.tax.expenses, param);
};