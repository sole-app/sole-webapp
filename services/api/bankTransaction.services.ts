import AxiosService from '../axios.services';
import apiRoutes from '../../config/apiRoutes';

class BankTransactionServices {

    static bankTransaction = async (param = '') => {
        return AxiosService.get(apiRoutes.bankTransaction.bankTransactions, param);
    };

    static soleTransactions = async (param = '') => {
        return AxiosService.get(apiRoutes.bankTransaction.soleTransactions, param);
    };

    static markPersonal = async (param = {}) => {
        return AxiosService.post(apiRoutes.bankTransaction.markPersonal, param);
    };

    static undo = async (param = {}) => {
        return AxiosService.post(apiRoutes.bankTransaction.undo, param);
    };

    static undoCashInvoice = async (param = {}) => {
        return AxiosService.post(apiRoutes.bankTransaction.undoCashInvoice, param);
    };

    static getPossibleMatchExpenses = async (param = '') => {
        return AxiosService.get(apiRoutes.bankTransaction.getPossibleMatchExpenses, param);
    };

    static fetchPossibleMatchInvoices = async (param = '') => {
        return AxiosService.get(apiRoutes.bankTransaction.fetchPossibleMatchExpenses, param);
    };

    static setMatchExpenses = async (param = {}) => {
        return AxiosService.post(apiRoutes.bankTransaction.setMatchExpenses, param);
    };

    static getPossibleMatchInvoices = async (param = {}) => {
        return AxiosService.post(apiRoutes.bankTransaction.getPossibleMatchInvoices, param);
    };


    static setMatchInvoices = async (param = {}) => {
        return AxiosService.post(apiRoutes.bankTransaction.setMatchInvoices, param);
    };

    static setMatch = async (param = {}) => {
        return AxiosService.post(apiRoutes.bankTransaction.setMatch, param);
    };

    static getMatchTransactionsByClient = async (param = {}) => {
        return AxiosService.post(apiRoutes.bankTransaction.getMatchTransactionsByClient, param);
    }

    static fetchAllBalance = async (param = '') => {
        return AxiosService.get(apiRoutes.bankTransaction.fetchAllBalance, param);
    }

    static getCashflow = async (param = {}) => {
        return AxiosService.post(apiRoutes.bankTransaction.getCashflow, param);
    }
}

export default BankTransactionServices;