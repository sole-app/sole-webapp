import AxiosService from '../axios.services';
import apiRoutes from '../../config/apiRoutes';

class PreviewServices {
    static View = async (param) => {
        return AxiosService.post(apiRoutes.preview.view, param);
    };
}

export default PreviewServices;