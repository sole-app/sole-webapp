import { get, post } from '../axios.services';
import apiRoutes from '../../config/apiRoutes';

export const List = async (param = '') => {
    return get(apiRoutes.client.listV2, param);
};

export const Add = async (param) => {
    return post(apiRoutes.client.add, param, false, false);
};

export const Update = async (param) => {
    return post(apiRoutes.client.update, param, false, false);
};

export const Delete = async (param) => {
    return post(apiRoutes.client.delete, param, true);
};

export const Info = async (param = 0) => {
    return get(apiRoutes.client.info, param.toString());
};