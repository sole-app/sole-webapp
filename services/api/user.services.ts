import AxiosService from '../axios.services';
import apiRoutes from '../../config/apiRoutes';
import CookiesServices from "../cookies.services";

class UserServices {
    static logoutUser = async (param) => {
        const result = await AxiosService.post(apiRoutes.user.userLogout, param, false);
        if (result.success) {
            CookiesServices.deleteAll();
            localStorage.clear();
        }
        return result;
    };

    static updateUserProfile = async (param) => {
        const result = await AxiosService.post(apiRoutes.user.updateUserProfile, param, false, false);
        if (result.success) {
            CookiesServices.setLoginResponse(result.data.user);
        }
        return result;
    };

    static deleteAccount = async () => {
        const result = await AxiosService.post(apiRoutes.user.deleteAccount, {}, true);
        if (result.success) {
            CookiesServices.deleteAll();
        }
        return result;
    };

    static notification = async (param) => {
        return AxiosService.get(apiRoutes.user.notification, param);
    };

}

export default UserServices;