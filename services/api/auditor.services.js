import { post } from '../axios.services';
import apiRoutes from '../../config/apiRoutes';
import { setAuditorData } from "../cookies.services";

export const getAuditor = async (param) => {
    const result = await post(apiRoutes.auditor.get, param);
    if (result.success) {
        if (Object.keys(result.data.auditor_details).length > 0) {
            setAuditorData(result.data.auditor_details);
        }
    }
    return result;
};

export const addAuditor = async (param) => {
    return post(apiRoutes.auditor.add, param, true);
};

export const updateAuditor = async (param) => {
    return post(apiRoutes.auditor.update, param, true);
};