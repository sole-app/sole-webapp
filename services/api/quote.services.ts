import AxiosService from '../axios.services';
import apiRoutes from '../../config/apiRoutes';

class QuoteServices {
    static List = async (param) => {
        return AxiosService.get(apiRoutes.quote.list, param);
    };

    static Add = async (param) => {
        return AxiosService.post(apiRoutes.quote.add, param, false, false);
    };

    static Update = async (param) => {
        return AxiosService.post(apiRoutes.quote.update, param, false, false);
    };

    static Delete = async (param) => {
        return AxiosService.post(apiRoutes.quote.delete, param, true);
    };

    static Info = async (param) => {
        return AxiosService.get(apiRoutes.quote.info, param);
    };

    static Summary = async (param) => {
        return AxiosService.get(apiRoutes.quote.summary, param);
    };

    static TopEarners = async (param) => {
        return AxiosService.get(apiRoutes.quote.topEarners, param);
    };
}

export default QuoteServices;