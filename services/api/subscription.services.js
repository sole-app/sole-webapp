import { post } from '../axios.services';
import apiRoutes from '../../config/apiRoutes';

export const addSubscription = async (param) => {
    return post(apiRoutes.subscription.add, param, true);
};

export const validateCoupon = async (code) => {
    let param = new FormData();
    param.append('name', code);
    return post(apiRoutes.subscription.validateCoupon, param, true);
};

export const cancel = async () => {
    return post(apiRoutes.subscription.cancel, {}, true);
};

export const resume = async () => {
    return post(apiRoutes.subscription.resume, {}, true);
};