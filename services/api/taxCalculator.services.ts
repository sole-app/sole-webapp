import AxiosService from '../axios.services';
import apiRoutes from '../../config/apiRoutes';

class TaxcalculatorServices {

    static getFinancialYears = async (param) => {
        return AxiosService.post(apiRoutes.tax.getFinancialYears, param);
    };

    static getTaxCalculation = async (param) => {
        return AxiosService.post(apiRoutes.tax.getTaxCalculation, param);
    };

    static calculateTax = async (param) => {
        return AxiosService.post(apiRoutes.tax.calculateTax, param, false, false);
    };

    static getIncomeByFinYear = async (param) => {
        return AxiosService.post(apiRoutes.tax.income, param);
    };

    static getExpenseByFinYear = async (param) => {
        return AxiosService.post(apiRoutes.tax.expenses, param);
    };

}

export default TaxcalculatorServices;