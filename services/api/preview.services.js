import { post } from '../axios.services';
import apiRoutes from '../../config/apiRoutes';

export const preview = async (param) => {
    return post(apiRoutes.preview.view, param);
};