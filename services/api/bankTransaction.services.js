import {get, post} from '../axios.services';
import apiRoutes from '../../config/apiRoutes';

export const bankTransaction = async (param = '') => {
    return get(apiRoutes.bankTransaction.bankTransactions, param);
};

export const soleTransactions = async (param = '') => {
    return get(apiRoutes.bankTransaction.soleTransactions, param);
};

export const markPersonal = async (param = {}) => {
    return post(apiRoutes.bankTransaction.markPersonal, param);
};

export const undo = async (param = {}) => {
    return post(apiRoutes.bankTransaction.undo, param);
};

export const undoCashInvoice = async (param = {}) => {
    return post(apiRoutes.bankTransaction.undoCashInvoice, param);
};

export const getPossibleMatchExpenses = async (param = '') => {
    return get(apiRoutes.bankTransaction.getPossibleMatchExpenses, param);
};

export const fetchPossibleMatchInvoices = async (param = '') => {
    return get(apiRoutes.bankTransaction.fetchPossibleMatchExpenses, param);
};

export const setMatchExpenses = async (param = {}) => {
    return post(apiRoutes.bankTransaction.setMatchExpenses, param);
};

export const getPossibleMatchInvoices = async (param = {}) => {
    return post(apiRoutes.bankTransaction.getPossibleMatchInvoices, param);
};


export const setMatchInvoices = async (param = {}) => {
    return post(apiRoutes.bankTransaction.setMatchInvoices, param);
};

export const setMatch = async (param = {}) => {
    return post(apiRoutes.bankTransaction.setMatch, param);
};

export const getMatchTransactionsByClient = async (param = {}) => {
    return post(apiRoutes.bankTransaction.getMatchTransactionsByClient, param);
}

export const fetchAllBalance = async (param = '') => {
    return get(apiRoutes.bankTransaction.fetchAllBalance, param);
}

export const getCashflow = async (param = {}) => {
    return post(apiRoutes.bankTransaction.getCashflow, param);
}