import { post } from '../axios.services';
import apiRoutes from '../../config/apiRoutes';

export const sendReport = async (param) => {
    return post(apiRoutes.report.send, param, false, false);
};