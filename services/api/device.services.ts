import axios from 'axios';
import AxiosService from '../axios.services';
import apiRoutes from '../../config/apiRoutes';
import CookiesServices from "../cookies.services";
import { browserName, deviceType, osVersion, osName } from 'react-device-detect';

// FIXME: We need to revisit this flow for device/user registration and auth
class DeviceServices {

    static registerDevice = async () => {
        let param = {
            device_uid: '_' + Math.random().toString(36).substr(2, 9),
            platform: "WEBP",
            device_name: browserName + " " + deviceType,
            device_os: osName + " (" + osVersion + ")",
            app_version: 1
        };

        const authHeaders = AxiosService.getApiKeyHeader();
        const result = await axios.post(apiRoutes.device.register, param, {
            headers: authHeaders,
        });

        if (result.status === 200) {
            let reponse = result.data;
            if (reponse.success) {
                CookiesServices.setDeviceId(reponse.data.device_id);
            }
            return result;
        }
    };

}

export default DeviceServices;