import AxiosService from '../axios.services';
import apiRoutes from '../../config/apiRoutes';

class AssetServices {
    static List = async (param) => {
        return AxiosService.get(apiRoutes.asset.list, param);
    };

    static Add = async (param) => {
        return AxiosService.post(apiRoutes.asset.add, param, false, false);
    };

    static Update = async (param) => {
        return AxiosService.post(apiRoutes.asset.update, param, false, false);
    };

    static Delete = async (param) => {
        return AxiosService.post(apiRoutes.asset.delete, param, true);
    };

    static Info = async (param) => {
        return AxiosService.get(apiRoutes.asset.info, param);
    };
}

export default AssetServices;