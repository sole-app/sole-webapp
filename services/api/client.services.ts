import AxiosService from '../axios.services';
import apiRoutes from '../../config/apiRoutes';

class ClientServices {

    static List = async (param = '') => {
        return AxiosService.get(apiRoutes.client.listV2, param);
    };

    static Add = async (param) => {
        return AxiosService.post(apiRoutes.client.add, param, false, false);
    };

    static Update = async (param) => {
        return AxiosService.post(apiRoutes.client.update, param, false, false);
    };

    static Delete = async (param) => {
        return AxiosService.post(apiRoutes.client.delete, param, true);
    };

    static Info = async (param = 0) => {
        return AxiosService.get(apiRoutes.client.info, param.toString());
    };
}

export default ClientServices;