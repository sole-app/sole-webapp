import { get, post } from '../axios.services';
import apiRoutes from '../../config/apiRoutes';
import { deleteAll, setLoginResponse } from "../cookies.services";

export const logoutUser = async (param) => {
    const result = await post(apiRoutes.user.userLogout, param, false);
    if (result.success) {
        deleteAll();
        localStorage.clear();
    }
    return result;
};

export const updateUserProfile = async (param) => {
    const result = await post(apiRoutes.user.updateUserProfile, param, false, false);
    if (result.success) {
        setLoginResponse(result.data.user);
    }
    return result;
};

export const deleteAccount = async () => {
    const result = await post(apiRoutes.user.deleteAccount, {}, true);
    if (result.success) {
        deleteAll();
    }
    return result;
};

export const notification = async (param) => {
    return get(apiRoutes.user.notification, param);
};