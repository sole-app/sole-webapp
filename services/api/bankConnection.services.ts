import AxiosService from '../axios.services';
import apiRoutes from '../../config/apiRoutes';

class BankconnectionServices {

    static getBankLists = async (param = {}) => {
        return AxiosService.post(apiRoutes.bankConnection.getInstituteList, param, false, true);
    };

    static createCustomer = async (param = {}) => {
        return AxiosService.post(apiRoutes.bankConnection.createCustomer, param, false, false);
    };

    static listAccounts = async (param = {}) => {
        return AxiosService.post(apiRoutes.bankConnection.listAccounts, param);
    };

    static selectAccount = async (param = {}) => {
        return AxiosService.post(apiRoutes.bankConnection.selectAccount, param);
    };

    static updateCustomer = async (param = {}) => {
        return AxiosService.post(apiRoutes.bankConnection.updateCustomer, param, false, false);
    };

    static additionalAuth = async (param = {}) => {
        return AxiosService.post(apiRoutes.bankConnection.additionalAuth, param);
    };

    static refreshAccountData = (param = {}) => {
        AxiosService.postSync(apiRoutes.bankConnection.refreshAccountData, param);
    };
}

export default BankconnectionServices;