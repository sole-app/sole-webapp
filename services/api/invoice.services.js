import { get, post } from '../axios.services';
import apiRoutes from '../../config/apiRoutes';

export const List = async (param) => {
    return get(apiRoutes.invoice.list, param);
};

export const Add = async (param) => {
    return post(apiRoutes.invoice.add, param, false, false);
};

export const Update = async (param) => {
    return post(apiRoutes.invoice.update, param, false, false);
};

export const Delete = async (param) => {
    return post(apiRoutes.invoice.delete, param, true);
};

export const Info = async (param) => {
    return get(apiRoutes.invoice.info, param);
};

export const Summary = async (param) => {
    return get(apiRoutes.invoice.summary, param);
};

export const TopEarners = async (param) => {
    return get(apiRoutes.invoice.topEarners, param);
};

export const PaidWithCash = async (param) => {
    return post(apiRoutes.invoice.paidWithCash, param, true);
};

export const convertToInvoice = async (param) => {
    return post(apiRoutes.invoice.convertToInvoice, param, true);
};

export const followUpReminder = async (param) => {
    return post(apiRoutes.invoice.followUpReminder, param);
};

export const download = async (param) => {
    return post(apiRoutes.invoice.download, param);
};