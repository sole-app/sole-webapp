import AxiosService from '../axios.services';
import apiRoutes from '../../config/apiRoutes';
import CookiesServices from "../cookies.services";

class AuditorService {

    static getAuditor = async (param) => {
        const result = await AxiosService.post(apiRoutes.auditor.get, param);
        if (result.success) {
            if (Object.keys(result.data.auditor_details).length > 0) {
                CookiesServices.setAuditorData(result.data.auditor_details);
            }
        }
        return result;
    };

    static addAuditor = async (param) => {
        return AxiosService.post(apiRoutes.auditor.add, param, true);
    };

    static updateAuditor = async (param) => {
        return AxiosService.post(apiRoutes.auditor.update, param, true);
    };

}

export default AuditorService;