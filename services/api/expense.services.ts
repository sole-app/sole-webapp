import AxiosService from '../axios.services';
import apiRoutes from '../../config/apiRoutes';

class ExpenseServices {
    static List = async (param) => {
        return AxiosService.get(apiRoutes.expense.list, param);
    };

    static Add = async (param) => {
        return AxiosService.post(apiRoutes.expense.add, param, false, false);
    };

    static Update = async (param) => {
        return AxiosService.post(apiRoutes.expense.update, param, false, false);
    };

    static Delete = async (param) => {
        return AxiosService.post(apiRoutes.expense.delete, param, true);
    };

    static Info = async (param) => {
        return AxiosService.get(apiRoutes.expense.info, param);
    };

    static Summary = async (param) => {
        return AxiosService.get(apiRoutes.expense.summary, param);
    };

    static download = async (param) => {
        return AxiosService.post(apiRoutes.expense.download, param);
    };
}

export default ExpenseServices;