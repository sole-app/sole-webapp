import { get, post } from '../axios.services';
import apiRoutes from '../../config/apiRoutes';

export const List = async (param) => {
    return get(apiRoutes.expense.list, param);
};

export const Add = async (param) => {
    return post(apiRoutes.expense.add, param, false, false);
};

export const Update = async (param) => {
    return post(apiRoutes.expense.update, param, false, false);
};

export const Delete = async (param) => {
    return post(apiRoutes.expense.delete, param, true);
};

export const Info = async (param) => {
    return get(apiRoutes.expense.info, param);
};

export const Summary = async (param) => {
    return get(apiRoutes.expense.summary, param);
};

export const download = async (param) => {
    return post(apiRoutes.expense.download, param);
};