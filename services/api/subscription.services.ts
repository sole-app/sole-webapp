import AxiosService from '../axios.services';
import apiRoutes from '../../config/apiRoutes';

class SubscriptionServices {

    static addSubscription = async (param) => {
        return AxiosService.post(apiRoutes.subscription.add, param, true);
    };

    static validateCoupon = async (code) => {
        let param = new FormData();
        param.append('name', code);
        return AxiosService.post(apiRoutes.subscription.validateCoupon, param, true);
    };

    static cancel = async () => {
        return AxiosService.post(apiRoutes.subscription.cancel, {}, true);
    };

    static resume = async () => {
        return AxiosService.post(apiRoutes.subscription.resume, {}, true);
    };

}

export default SubscriptionServices;