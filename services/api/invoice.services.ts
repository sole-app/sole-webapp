import AxiosService from '../axios.services';
import apiRoutes from '../../config/apiRoutes';

class InvoiceServices {
    static List = async (param) => {
        return AxiosService.get(apiRoutes.invoice.list, param);
    };

    static Add = async (param) => {
        return AxiosService.post(apiRoutes.invoice.add, param, false, false);
    };

    static Update = async (param) => {
        return AxiosService.post(apiRoutes.invoice.update, param, false, false);
    };

    static Delete = async (param) => {
        return AxiosService.post(apiRoutes.invoice.delete, param, true);
    };

    static Info = async (param) => {
        return AxiosService.get(apiRoutes.invoice.info, param);
    };

    static Summary = async (param) => {
        return AxiosService.get(apiRoutes.invoice.summary, param);
    };

    static TopEarners = async (param) => {
        return AxiosService.get(apiRoutes.invoice.topEarners, param);
    };

    static PaidWithCash = async (param) => {
        return AxiosService.post(apiRoutes.invoice.paidWithCash, param, true);
    };

    static convertToInvoice = async (param) => {
        return AxiosService.post(apiRoutes.invoice.convertToInvoice, param, true);
    };

    static followUpReminder = async (param) => {
        return AxiosService.post(apiRoutes.invoice.followUpReminder, param);
    };

    static download = async (param) => {
        return AxiosService.post(apiRoutes.invoice.download, param);
    };
}

export default InvoiceServices;