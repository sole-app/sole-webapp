import AxiosService from '../axios.services';
import apiRoutes from '../../config/apiRoutes';

class NotificationServices {

    static get = async (param = '') => {
        return AxiosService.get(apiRoutes.notification.get, param);
    };

    static add = async (param = {}) => {
        return AxiosService.post(apiRoutes.notification.add, param);
    };
}

export default NotificationServices;