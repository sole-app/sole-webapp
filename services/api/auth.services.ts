import axios from 'axios';
import AxiosService from '../axios.services';
import apiRoutes from '../../config/apiRoutes';
import CookiesServices from "../cookies.services";
import store from '../../store/store';
import { snackbarOpen } from '../../reducers/slices/snackbarSlice';
import { SNACKBAR_TYPE } from "../../components/Layout/Snackbar";
import DeviceServices from "../api/device.services";

class AuthServices {
    static login = async (param, social = '') => {
        let device_id = CookiesServices.getDeviceId() ? CookiesServices.getDeviceId() : null;
        if (device_id === null) {
            await DeviceServices.registerDevice();
        }
        device_id = CookiesServices.getDeviceId() ? CookiesServices.getDeviceId() : null;

        param.device_id = device_id;
        param.platform = 'WEBP';
        const authHeaders = AxiosService.getApiKeyHeader();
        const rawResult = await axios.post(apiRoutes.auth.login, param, {
            headers: authHeaders,
        });
        if (rawResult.status === 200) {
            let result = rawResult.data;
            if (result.success) {
                CookiesServices.setLoginResponse(result.data.user);
            }
            return result;
        }
        else {
            store.store.dispatch(snackbarOpen({
                type: SNACKBAR_TYPE.ERROR,
                message: 'something went wrong.',
            }));
        }
    };

    static signUp = async (param, social = '') => {
        CookiesServices.setUserEmail(param.email);

        let device_id = CookiesServices.getDeviceId() ? CookiesServices.getDeviceId() : null;
        if (device_id === null) {
            await DeviceServices.registerDevice();
        }
        device_id = CookiesServices.getDeviceId() ? CookiesServices.getDeviceId() : null;

        param.device_id = device_id;

        const authHeaders = AxiosService.getApiKeyHeader();
        authHeaders['GOOGLE_CAPTCHA_TOKEN'] = param.captcha;
        authHeaders['PLATFORM'] = "WEB";
        param.platform = 'WEBP';
        const rawResult = await axios.post(apiRoutes.auth.signUp, param, {
            headers: authHeaders
        });
        if (rawResult.status === 200) {
            let result = rawResult.data;
            if (result.success) {
                store.store.dispatch(snackbarOpen({
                    type: SNACKBAR_TYPE.SUCCESS,
                    message: result.message,
                }));
            }
            return result;
        }
        else {
            store.store.dispatch(snackbarOpen({
                type: SNACKBAR_TYPE.ERROR,
                message: 'something went wrong.',
            }));
        }
    };

    static resendVerificationCode = async (email = null) => {
        let param = {}
        if (email) {
            param = {
                email: email
            };
        } else {
            param = {
                email: CookiesServices.getUserEmail()
            };
        }
        const authHeaders = AxiosService.getApiKeyHeader();
        const rawResult = await axios.post(apiRoutes.auth.resendVerificationCode, param, {
            headers: authHeaders,
        });
        if (rawResult.status === 200) {
            let result = rawResult.data;
            if (result.success) {
                store.store.dispatch(snackbarOpen({
                    type: SNACKBAR_TYPE.SUCCESS,
                    message: result.message,
                }));
            } else {
                store.store.dispatch(snackbarOpen({
                    type: SNACKBAR_TYPE.ERROR,
                    message: result.message,
                }));
            }
            return result;
        }
        else {
            store.store.dispatch(snackbarOpen({
                type: SNACKBAR_TYPE.ERROR,
                message: 'something went wrong.',
            }));
        }
    };

    static accountActivate = async (param, email_to_veriify = null) => {
        if (email_to_veriify) {
            param.email = email_to_veriify
        } else {
            param.email = CookiesServices.getUserEmail();
        }
        param.platform = "WEBP";
        let device_id = CookiesServices.getDeviceId() ? CookiesServices.getDeviceId() : null;
        if (device_id === null) {
            await DeviceServices.registerDevice();
        }
        device_id = CookiesServices.getDeviceId() ? CookiesServices.getDeviceId() : null;

        param.device_id = device_id;
        const authHeaders = AxiosService.getApiKeyHeader();
        const rawResult = await axios.post(apiRoutes.auth.accountActivate, param, {
            headers: authHeaders,
        });
        if (rawResult.status === 200) {
            let result = rawResult.data;
            if (result.success) {
                store.store.dispatch(snackbarOpen({
                    type: SNACKBAR_TYPE.SUCCESS,
                    message: result.message,
                }));
                CookiesServices.setLoginResponse(result.data.user);
            } else {
                store.store.dispatch(snackbarOpen({
                    type: SNACKBAR_TYPE.ERROR,
                    message: result.message,
                }));
            }
            return result;
        }
        else {
            store.store.dispatch(snackbarOpen({
                type: SNACKBAR_TYPE.ERROR,
                message: 'something went wrong.',
            }));
        }
    };

    static forgotPassword = async (param) => {
        const authHeaders = AxiosService.getApiKeyHeader();
        const rawResult = await axios.post(apiRoutes.auth.forgotPassword, param, {
            headers: authHeaders,
        });
        if (rawResult.status === 200) {
            let result = rawResult.data;
            if (result.success) {
                store.store.dispatch(snackbarOpen({
                    type: SNACKBAR_TYPE.SUCCESS,
                    message: result.message,
                }));
            }
            return result;
        }
        else {
            store.store.dispatch(snackbarOpen({
                type: SNACKBAR_TYPE.ERROR,
                message: 'something went wrong.',
            }));
        }
    };

}

export default AuthServices;