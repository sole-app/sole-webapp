import { get, post } from '../axios.services';
import apiRoutes from '../../config/apiRoutes';

export const getNotifications = async (param = '') => {
    return get(apiRoutes.notification.get, param);
};

export const add = async (param = {}) => {
    return post(apiRoutes.notification.add, param);
};