import AxiosService from '../axios.services';
import apiRoutes from '../../config/apiRoutes';

class BankAccountServices {
    static listAccounts = async (formData = {}) => {
        return AxiosService.post(apiRoutes.bankAccount.list, formData);
    };

    static deleteAccount = async (formData = {}) => {
        return AxiosService.post(apiRoutes.bankAccount.delete, formData);
    };

    static makePrimary = async (formData = {}) => {
        return AxiosService.post(apiRoutes.bankAccount.makePrimary, formData);
    };
}

export default BankAccountServices;