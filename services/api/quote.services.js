import { get, post } from '../axios.services';
import apiRoutes from '../../config/apiRoutes';

export const List = async (param) => {
    return get(apiRoutes.quote.list, param);
};

export const Add = async (param) => {
    return post(apiRoutes.quote.add, param, false, false);
};

export const Update = async (param) => {
    return post(apiRoutes.quote.update, param, false, false);
};

export const Delete = async (param) => {
    return post(apiRoutes.quote.delete, param, true);
};

export const Info = async (param) => {
    return get(apiRoutes.quote.info, param);
};

export const Summary = async (param) => {
    return get(apiRoutes.quote.summary, param);
};

export const TopEarners = async (param) => {
    return get(apiRoutes.quote.topEarners, param);
};