import axios from 'axios';
import { getApiKeyHeader, } from '../axios.services';
import apiRoutes from '../../config/apiRoutes';
import { getDeviceId, setLoginResponse, setUserEmail, getUserEmail } from "../cookies.services";
import store from '../../store/store';
import { snackbarOpen } from '../../reducers/slices/snackbarSlice';
import { SNACKBAR_TYPE } from "../../components/Layout/Snackbar";
import { registerDevice } from "../api/device.services";

export const login = async (param, social = '') => {
    let device_id = getDeviceId() ? getDeviceId() : null;
    if (device_id === null) {
        await registerDevice();
    }
    device_id = getDeviceId() ? getDeviceId() : null;

    param.device_id = device_id;
    param.platform = 'WEBP';
    const authHeaders = getApiKeyHeader();
    const rawResult = await axios.post(apiRoutes.auth.login, param, {
        headers: authHeaders,
    });
    if (rawResult.status === 200) {
        let result = rawResult.data;
        if (result.success) {
            setLoginResponse(result.data.user);
        }
        return result;
    }
    else {
        store.store.dispatch(snackbarOpen({
            type: SNACKBAR_TYPE.ERROR,
            message: 'something went wrong.',
        }));
    }
};

export const signUp = async (param, social = '') => {
    setUserEmail(param.email);

    let device_id = getDeviceId() ? getDeviceId() : null;
    if (device_id === null) {
        await registerDevice();
    }
    device_id = getDeviceId() ? getDeviceId() : null;

    param.device_id = device_id;

    const authHeaders = getApiKeyHeader();
    authHeaders['GOOGLE_CAPTCHA_TOKEN'] = param.captcha;
    authHeaders['PLATFORM'] = "WEB";
    param.platform = 'WEBP';
    const rawResult = await axios.post(apiRoutes.auth.signUp, param, {
        headers: authHeaders
    });
    if (rawResult.status === 200) {
        let result = rawResult.data;
        if (result.success) {
            store.store.dispatch(snackbarOpen({
                type: SNACKBAR_TYPE.SUCCESS,
                message: result.message,
            }));
        }
        return result;
    }
    else {
        store.store.dispatch(snackbarOpen({
            type: SNACKBAR_TYPE.ERROR,
            message: 'something went wrong.',
        }));
    }
};

export const resendVerificationCode = async (email = null) => {
    let param = {}
    if (email) {
        param = {
            email: email
        };
    } else {
        param = {
            email: getUserEmail()
        };
    }
    const authHeaders = getApiKeyHeader();
    const rawResult = await axios.post(apiRoutes.auth.resendVerificationCode, param, {
        headers: authHeaders,
    });
    if (rawResult.status === 200) {
        let result = rawResult.data;
        if (result.success) {
            store.store.dispatch(snackbarOpen({
                type: SNACKBAR_TYPE.SUCCESS,
                message: result.message,
            }));
        } else {
            store.store.dispatch(snackbarOpen({
                type: SNACKBAR_TYPE.ERROR,
                message: result.message,
            }));
        }
        return result;
    }
    else {
        store.store.dispatch(snackbarOpen({
            type: SNACKBAR_TYPE.ERROR,
            message: 'something went wrong.',
        }));
    }
};

export const accountActivate = async (param, email_to_veriify = null) => {
    if (email_to_veriify) {
        param.email = email_to_veriify
    } else {
        param.email = getUserEmail();
    }
    param.platform = "WEBP";
    let device_id = getDeviceId() ? getDeviceId() : null;
    if (device_id === null) {
        await registerDevice();
    }
    device_id = getDeviceId() ? getDeviceId() : null;

    param.device_id = device_id;
    const authHeaders = getApiKeyHeader();
    const rawResult = await axios.post(apiRoutes.auth.accountActivate, param, {
        headers: authHeaders,
    });
    if (rawResult.status === 200) {
        let result = rawResult.data;
        if (result.success) {
            store.store.dispatch(snackbarOpen({
                type: SNACKBAR_TYPE.SUCCESS,
                message: result.message,
            }));
            setLoginResponse(result.data.user);
        } else {
            store.store.dispatch(snackbarOpen({
                type: SNACKBAR_TYPE.ERROR,
                message: result.message,
            }));
        }
        return result;
    }
    else {
        store.store.dispatch(snackbarOpen({
            type: SNACKBAR_TYPE.ERROR,
            message: 'something went wrong.',
        }));
    }
};

export const forgotPassword = async (param) => {
    const authHeaders = getApiKeyHeader();
    const rawResult = await axios.post(apiRoutes.auth.forgotPassword, param, {
        headers: authHeaders,
    });
    if (rawResult.status === 200) {
        let result = rawResult.data;
        if (result.success) {
            store.store.dispatch(snackbarOpen({
                type: SNACKBAR_TYPE.SUCCESS,
                message: result.message,
            }));
        }
        return result;
    }
    else {
        store.store.dispatch(snackbarOpen({
            type: SNACKBAR_TYPE.ERROR,
            message: 'something went wrong.',
        }));
    }
};