import axios from 'axios';
import AxiosService from '../axios.services';
import apiRoutes from '../../config/apiRoutes';
import store from '../../store/store';
import CookiesServices from '../cookies.services';
import { snackbarOpen } from '../../reducers/slices/snackbarSlice';
import { SNACKBAR_TYPE } from "../../components/Layout/Snackbar";

class DashboardServices {

    static getDashboard = async (param = {}) => {
        return AxiosService.post(apiRoutes.dashboard.get, param, false, false);
    };

    static setGoal = async (goalAmount) => {
        const authHeaders = AxiosService.getApiKeyHeader();
        authHeaders['Authorization'] = "Bearer " + CookiesServices.getAuthToken();
        authHeaders['Content-Type'] = "application/json";

        const rawResult = await axios.post(apiRoutes.goal.set, {goal: Number(goalAmount)}, {
            headers: authHeaders
        });

        if (rawResult.status === 200) {
            let result = rawResult.data;
            if (result.success) {
                store.store.dispatch(snackbarOpen({
                    type: SNACKBAR_TYPE.SUCCESS,
                    message: result.message,
                }));
            } else {
                store.store.dispatch(snackbarOpen({
                    type: SNACKBAR_TYPE.ERROR,
                    message: result.message,
                }));
            }
            return result;
        }
    };
}

export default DashboardServices;