import AxiosService from '../axios.services';
import apiRoutes from '../../config/apiRoutes';

class ReportServices {
    static send = async (param) => {
        return AxiosService.post(apiRoutes.report.send, param, false, false);
    };
}

export default ReportServices;