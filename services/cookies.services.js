import Cookies from 'js-cookie';
import store from '../store/store';
import { addUser, removeUser, addSubscription, setRefreshSubscription } from '../store/actions';

export const TOKEN_STORAGE_KEY = 'token';
export const USER_STORAGE_KEY = 'userData';
export const USER_EMAIL_KEY = 'userEmail';
export const DEVICE_ID = 'deviceId';

export const AUDITOR = 'auditor';
export const ACCOUNTANT_NAME = 'actName';
export const ACCOUNTANT_EMAIL = 'actEmail';
export const BOOKKEEPER_NAME = 'bperName';
export const BOOKKEEPER_EMAIL = 'bperEmail';

export const get = (key) => {
    return Cookies.get(key);
}

export const set = (key, value) => {
    Cookies.set(key, value);
}

export const getDeviceId = () => {
    return get(DEVICE_ID);
}

export const setDeviceId = (id) => {
    set(DEVICE_ID, id);
}

export const getAuthToken = () => {
    return get(TOKEN_STORAGE_KEY);
}

export const setAuthToken = (token) => {
    set(TOKEN_STORAGE_KEY, token);
}

export const getUserEmail = () => {
    var statsFromRedux = store.store.getState();

    return statsFromRedux.user_details.email;
}

export const setUserEmail = (email) => {
    // set(USER_EMAIL_KEY, email);
}

export const setLoginResponse = (user) => {
    setAuthToken(user.token);
    setUserData(user);
}

export const getUserData = () => {
    var statsFromRedux = store.store.getState();

    return statsFromRedux.user_details;
}

export const updateSubscription = (subscription) => {
    setSubscription(subscription);
}

export const setSubscription = (subscription) => {
    store.store.dispatch(addSubscription(subscription));
    store.store.dispatch(setRefreshSubscription());
}

export const setUserData = (user) => {
    store.store.dispatch(addUser(user));
}

export const setAuditorData = (data) => {
    set(AUDITOR, data);
}

export const getAuditorData = () => {
    let auditorData = get(AUDITOR);
    if (auditorData != '') {
        try {
            auditorData = JSON.parse(auditorData);
        } catch (e) { }
    }
    return auditorData;
}

export const getAccountantName = () => {
    const auditor = getAuditorData();

    if (auditor != null) {
        return auditor.accountant != null ? auditor.accountant.name : '';
    }
    else {
        return '';
    }
}

export const getAccountantEmail = () => {
    const auditor = getAuditorData();

    if (auditor != null) {
        return auditor.accountant != null ? auditor.accountant.email : '';
    }
    else {
        return '';
    }
}

export const getBookkeeperName = () => {
    const auditor = getAuditorData();

    if (auditor != null) {
        return auditor.bookkeeper != null ? auditor.bookkeeper.name : '';
    }
    else {
        return '';
    }
}

export const getBookkeeperEmail = () => {
    const auditor = getAuditorData();

    if (auditor != null) {
        return auditor.bookkeeper != null ? auditor.bookkeeper.email : '';
    }
    else {
        return '';
    }
}

export const deleteAll = () => {
    Object.keys(Cookies.get()).forEach(function (cookieName) {
        var neededAttributes = {
            // Here you pass the same attributes that were used when the cookie was created
            // and are required when removing the cookie
        };
        Cookies.remove(cookieName, neededAttributes);
    });
    store.store.dispatch(removeUser());
}

export const getUserName = () => {
    let userData = getUserData();
    return (userData != '') ? userData.full_name : 'Sole';
}

export const setBank = (bank) => {
    let userData = getUserData();
    if (userData != '') {
        if (typeof userData.bank !== 'undefined' && typeof userData.bank.account_holder !== 'undefined') {
            if (Object.keys(bank).length > 0) {
                userData.bank.account_holder = bank.account_holder;
                userData.bank.account_number = bank.account_number;
                userData.bank.bank_name = bank.bank_name;
                userData.bank.bsb = bank.bsb_number;
            } else {
                userData.bank.account_holder = '';
                userData.bank.account_number = '';
                userData.bank.bank_name = '';
                userData.bank.bsb = '';
            }
        }
    }
    setUserData(userData);
}

export const setUserName = (fname, lname) => {
    let userData = getUserData();
    userData.first_name = fname;
    userData.last_name = lname;
    userData.full_name = fname + ' ' + lname;
    userData.full_name = userData.full_name.trim();
    setUserData(userData);
};