import { getAuthToken } from "./cookies.services";
import config from '../config/config'
import axios from 'axios';
import store from '../store/store';
import { snackbarOpen } from '../reducers/slices/snackbarSlice';
import { SNACKBAR_TYPE } from "../components/Layout/Snackbar";
import { setTokenExpired } from "../store/actions";

export const getUserAuthHeader = () => {
    const token = getAuthToken();
    return {
        apiKey: config.apiKey,
        Authorization: 'Bearer ' + token,
    };
};

export const getApiKeyHeader = () => {
    return {
        'apiKey': config.apiKey
    };
};

export const post = async (url, params = {}, successMsg = false, errorMsg = true) => {
    const authHeaders = getUserAuthHeader();
    authHeaders['content-type'] = 'multipart/form-data';
    const rawResult = await axios.post(url, params, {
        headers: authHeaders,
    });
    if (rawResult.status === 200) {
        let result = rawResult.data;
        if (result.success) {
            if (successMsg) {
                store.store.dispatch(snackbarOpen({
                    type: SNACKBAR_TYPE.SUCCESS,
                    message: result.message,
                }));
            }
        }
        else {
            if (result.message !== 'token_expired') {
                if (errorMsg) {
                    if (result.message !== 'Additional Input Required') {
                        store.store.dispatch(snackbarOpen({
                            type: SNACKBAR_TYPE.ERROR,
                            message: result.message,
                        }));
                    }
                }
            } else {
                store.store.dispatch(setTokenExpired(true));
            }

            /*if (result.message == 'token_expired') {
                deleteAll();
                localStorage.clear();
                location.reload();
            }*/
        }
        return result;
    }
};

export const get = async (url, params = '', successMsg = false, errorMsg = true) => {
    const authHeaders = getUserAuthHeader();
    authHeaders['content-type'] = 'multipart/form-data';
    const rawResult = await axios.get(url + params, {
        headers: authHeaders,
    });
    if (rawResult.status === 200) {
        let result = rawResult.data;
        if (result.success) {
            if (successMsg) {
                store.store.dispatch(snackbarOpen({
                    type: SNACKBAR_TYPE.SUCCESS,
                    message: result.message,
                }));
            }
        }
        else {
            if (result.message !== 'token_expired') {
                if (errorMsg) {
                    store.store.dispatch(snackbarOpen({
                        type: SNACKBAR_TYPE.ERROR,
                        message: result.message,
                    }));
                }
            } else {
                store.store.dispatch(setTokenExpired(true));
            }

            /*if (result.message == 'token_expired') {
                deleteAll();
                localStorage.clear();
                location.reload();
            }*/
        }
        return result;
    }
};

export const postSync = async (url, params = {}) => {
    const authHeaders = getUserAuthHeader();
    authHeaders['content-type'] = 'multipart/form-data';
    axios.post(url, params, { headers: authHeaders });
};