# Changelog

## Jun 08-09, 2021
* (Fixed) the Account Details page issue to not get init email & mobile number from cookies.
* (Fixed) Remove many warning display during npm run dev.
* (Fixed) Removed reset1 button on thankyou page and also reset cookies after payment.
* (Fixed) Try to fixed .svg not display in server.
* (Added) Added snackbar with redux store to call for axios.   

## Jun 04, 2021
* (Fixed) key object warning on register page step components.

## Jun 03, 2021
* (Added) When input file change on we display image on img tag in "AccountDetail" component. 

## May 30-31, 2021
* (Added) Create the separate file for cookies related operation (cookies.services.ts).
* (Added) Constant industry array in industry.services.ts file.
* (Added) Save the user data and auth token in cookies.
* (Fixed) On account verification screen when user add one character now it automatic change focus to next input.
* (Added) Account details form submit api call with profile photo update.    

## May 28-29, 2021
* (Changed) Components names in PascalCase.
* (Added) After first step signup call added api calls for resend verification code and active account by code.

## May 26, 2021
* (Added) Axios and Basic api structure with test signup api call in CreateAccount component.

## May 25, 2021
* (Added) Axios package for api calls & also added dev dependency for typescript.     


