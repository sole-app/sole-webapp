const loggedReducer = (state = {}, action) => {
    switch (action.type) {
        case 'ADD_USER_DETAILS':
            return action.payload;
        case "REMOVE_USER_DETAILS":
            return {};
        case 'ADD_SUBSCRIPTION':
            return {
                ...state,
                subscription: action.payload
            }
        default:
            return state;
    }
};

export default loggedReducer;