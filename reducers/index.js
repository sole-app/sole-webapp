import { combineReducers } from 'redux';
import loggedReducer from './isLogged';
import snackbarReducer from './slices/snackbarSlice';
import subscriptionReducer from './subscription';
import bankTransactionReducer from './bankTransaction';

const rootReducer = combineReducers({
    user_details: loggedReducer,
    snackbar: snackbarReducer,
    subscription: subscriptionReducer,
    bankTransaction: bankTransactionReducer,
});

export default rootReducer;