let initialState = {
    refreshSubscription: false,
}

const subscriptionReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'REFRESH_SUBSCRIPTION':
            return {
                ...state,
                refreshSubscription: (!state.refreshSubscription)
            }
        default:
            return state;
    }
};

export default subscriptionReducer;