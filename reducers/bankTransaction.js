let initialState = {
    primaryAccount: 0,
    highlightPrimary: true,
    tokenExpired: false,
}

const bankTransactionReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'SET_PRIMARY_ACCOUNT':
            return {
                ...state,
                primaryAccount: action.payload
            }
        case 'SET_HIGHTLIGHT_PRIMARY_ACCOUNT':
            return {
                ...state,
                highlightPrimary: action.payload
            }
        case 'SET_TOKEN_EXPIRED':
            return {
                ...state,
                tokenExpired: action.payload
            }
        default:
            return state;
    }
};

export default bankTransactionReducer;