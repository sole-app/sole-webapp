import { createSlice } from '@reduxjs/toolkit'
import { SNACKBAR_TYPE } from "../../components/Layout/Snackbar";

export const snackbarSlice = createSlice({
    name: 'counter',
    initialState: {
        open: false,
        type: SNACKBAR_TYPE.SUCCESS,
        message: "",
    },
    reducers: {
        snackbarOpen: (state, action) => {
            state.open = true;
            state.type = action.payload.type;
            state.message = action.payload.message;
        },
        snackbarClose: (state, action) => {
            state.open = false;
            state.type = SNACKBAR_TYPE.SUCCESS;
            state.message = "";
        },

    },
});

// Action creators are generated for each case reducer function
export const { snackbarClose, snackbarOpen } = snackbarSlice.actions

export default snackbarSlice.reducer