const colors = require('tailwindcss/colors')

module.exports = {
  mode: 'jit',
  purge: ['./pages/**/*.{js,ts,jsx,tsx}', './components/**/*.{js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      screens: {
        'sm': '640px',
        'md': '768px',
        'lg': '1024px',
        'xl': '1280px',
        '2xl': '1536px',
        '3xl': '1800px',
      },

      width: {
        '104': '30rem',
      },

      minWidth: {
        '96': '26rem',
      },

      colors: {
        transparent: 'transparent',
        current: 'currentColor',
        cyan: colors.cyan,
        'blue-gray': colors.blueGray,
        'brand-blue': '#4d4dff',
        'brand-lightblue': '#eaeaff',
        'brand-darkblue': '#3a3afb',
        'brand-navyblue': '#191944',
      },
    },
  },

  variants: {
    extend: {},
  },

  plugins: [
    require('@tailwindcss/forms'),
  ],
}
