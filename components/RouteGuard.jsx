import { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import store from '../store/store';

export { RouteGuard };

function RouteGuard({ children }) {
    const storeData = store.store.getState();
    const router = useRouter();
    const [expired, setExpired] = useState(false);

    useEffect(() => {
        // on initial load - run auth check 
        subscriptionCheck(router.asPath);

        // on route change start - hide page content by setting expired to false  
        const hideContent = () => setExpired(false);
        router.events.on('routeChangeStart', hideContent);

        // on route change complete - run auth check 
        router.events.on('routeChangeComplete', subscriptionCheck)

        // unsubscribe from events in useEffect return function
        return () => {
            router.events.off('routeChangeStart', hideContent);
            router.events.off('routeChangeComplete', subscriptionCheck);
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [storeData]);

    function subscriptionCheck(url) {
        // redirect to login page if accessing a private page and not logged in 
        const publicPaths = ['/signup', '/', '/settings/subscription', '/settings/profile'];
        const path = url.split('?')[0];
        let isExpired = false;
        if (typeof storeData.user_details !== 'undefined'
            && typeof storeData.user_details.subscription !== 'undefined'
            && typeof storeData.user_details.subscription.subscription_expire !== 'undefined'
            && storeData.user_details.subscription.subscription_expire
        ) {
            isExpired = true;
        }
        //isExpired = false; //for test
        if (isExpired && !publicPaths.includes(path)) {
            setExpired(true);
            router.push({
                pathname: '/settings/subscription'
            });
        } else {
            setExpired(false);
        }
    }

    return (!expired && children);
}