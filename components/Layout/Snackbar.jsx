import React from "react";
import { Snackbar as SnBar } from "@material-ui/core"
import MuiAlert from '@material-ui/lab/Alert'
import { connect } from 'react-redux'
import { snackbarClose } from '../../reducers/slices/snackbarSlice'

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

class Snackbar extends React.Component {
    constructor(props) {
        super(props);
    }

    handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        this.props.dispatch(snackbarClose());
    };

    render() {
        return (
            <SnBar open={this.props.open} autoHideDuration={6000} onClose={this.handleClose} anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'right',
            }}>
                <Alert onClose={this.handleClose} severity={this.props.type}>
                    {this.props.message}
                </Alert>
            </SnBar>
        );
    }
}

export const SNACKBAR_TYPE = {
    SUCCESS: 'success',
    ERROR: 'error',
    WARNING: 'warning',
    INFO: 'info',
};

Snackbar.defaultProps = {
    type: SNACKBAR_TYPE.success,
    message: "",
    open: false
};

const mapStateToProps = (state) => ({
    open: state.snackbar.open,
    type: state.snackbar.type,
    message: state.snackbar.message,
});

export default connect(mapStateToProps)(Snackbar);
