function classNames(...classes) {
    return classes.filter(Boolean).join(' ')
}

export default function YearMonthFilter(props) {
    const { type, setType } = props;

    return (
        <nav className="flex space-x-4" aria-label="Tabs">
            <span
                className={classNames(
                    (type === 'YEAR') ? 'bg-indigo-100 text-brand-blue' : 'text-gray-500 hover:text-gray-700',
                    'px-3 py-2 font-medium text-sm rounded-full hover-pointer'
                )}
                aria-current={type === 'YEAR' ? 'page' : undefined}
                onClick={() => setType('YEAR')}
            >
                This Year
            </span>
            <span
                className={classNames(
                    (type === 'MONTH') ? 'bg-indigo-100 text-brand-blue' : 'text-gray-500 hover:text-gray-700',
                    'px-3 py-2 font-medium text-sm rounded-full hover-pointer'
                )}
                aria-current={type === 'MONTH' ? 'page' : undefined}
                onClick={() => setType('MONTH')}
            >
                This Month
            </span>
        </nav>
    );
}