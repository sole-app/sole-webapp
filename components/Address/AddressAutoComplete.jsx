import React, { Component } from "react";
import PlacesAutocomplete from "react-places-autocomplete";
import { TextField } from '@material-ui/core';
import clsx from 'clsx';
import Styles from '../../styles/Address.module.scss';
import { IconContext } from "react-icons";
import { IoIosPin } from "react-icons/io";

const searchOptions = {
    componentRestrictions: { country: ['au'] },
};

class AddressAutoComplete extends Component {
    constructor(props) {
        super(props);

        this.state = {
            name: props.field.name,
            address: props.value || "",
            type: props.type || 'N' //N = normal, A = Advanced
        };
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        // You don't have to do this check first, but it can help prevent an unneeded render
        if (nextProps.value !== this.state.value) {
            this.setState({ address: nextProps.value });
        }
    }


    handleError = error => {
        if (this.state.type === 'N') {
            this.props.form.setFieldError(this.state.name, error);
        }
    };

    handleChange = address => {
        this.setState(() => {
            this.props.form.setFieldTouched(`${this.state.name}.value`);
            this.props.form.setFieldTouched(`${this.state.name}.address`);
            this.props.form.setFieldValue(this.state.name, address);
            this.props.form.setFieldValue("google_address", false);
            return { address };
        });
    };

    handleSelect = address => {
        this.setState(() => {
            this.props.form.setFieldValue(this.state.name, address);
            this.props.form.setFieldValue("google_address", true);
            return { address };
        });
    };

    render() {
        const {
            field: { name, ...field }, // { name, value, onChange, onBlur }
            form: { touched, errors }, // also values, setXXXX, handleXXXX, dirty, isValid, status, etc.
            classes,
            label,
            ...props
        } = this.props;

        const error = errors[name];
        const touch = touched[name];
        return (
            <PlacesAutocomplete
                searchOptions={searchOptions}
                name={name}
                id={name}
                {...props}
                value={this.state.address}
                onChange={this.handleChange}
                onSelect={this.handleSelect}
                onError={this.handleError}
            >
                {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
                    <div className={clsx(Styles.formGroup, "w100", (this.state.type === 'N' ? "p-rel" : "relative"))}>

                        {this.state.type === 'N'
                            ?
                            <TextField
                                {...getInputProps({
                                    placeholder: "Address...",
                                    className: "location-search-input form-control"
                                })}
                                label="Address"
                                name="address_line1"
                                id="address_line1"
                                type="text"
                                variant="outlined"
                                className="w100"
                            />
                            :
                            <TextField
                                {...getInputProps({
                                    placeholder: "Address...",
                                    className: "location-search-input form-control"
                                })}
                                name="address_line1"
                                id="address_line1"
                                type="text"
                                variant="outlined"
                                className="w100 address-input"
                            />
                        }

                        <div className="autocomplete-dropdown-container">
                            {loading && <div>Loading...</div>}
                            {suggestions.map(suggestion => {
                                const className = suggestion.active
                                    ? "suggestion-item--active"
                                    : "suggestion-item";
                                // inline style for demonstration purpose
                                const style = suggestion.active
                                    ? { backgroundColor: "#fafafa", cursor: "pointer" }
                                    : { backgroundColor: "#ffffff", cursor: "pointer" };
                                return (
                                    <div key={suggestion.key}
                                        {...getSuggestionItemProps(suggestion, {
                                            className,
                                            style
                                        })}
                                        className="dFlex"
                                    >
                                        <IconContext.Provider value={{ color: "#b2b2b2", className: 'p-rel', style: { top: '2px', fontSize: "14px" } }}>
                                            <IoIosPin />
                                        </IconContext.Provider> <span className="place">{suggestion.description}</span>
                                    </div>
                                );
                            })}
                        </div>
                    </div>
                )}
            </PlacesAutocomplete>
        );
    }
}

export default AddressAutoComplete;