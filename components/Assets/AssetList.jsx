import { Fragment, useState, useEffect } from "react";
import { Listbox, Transition } from "@headlessui/react";
import { CheckIcon, SelectorIcon } from "@heroicons/react/solid";
import { SearchIcon, AdjustmentsIcon } from "@heroicons/react/outline";
import { getFileName, formatShortDate } from "../../helpers/common";
import { List, Delete } from "../../services/api/asset.services";
import Skeleton from "react-loading-skeleton";
import "react-loading-skeleton/dist/skeleton.css";
import ConfirmationDialog from "../ConfirmationDialog";
import AlertDialog from "../AlertDialog";
import Pagination from "../Pagination";
import NumberFormat from "react-number-format";
import ReactTooltip from "react-tooltip";
import Filter from "../../components/Assets/Filter";

// For status dropdown
const sortList = [
  { id: "date_of_purchase", name: "Default(By Created Date)", order: "DESC" },
  { id: "name", name: "Name", order: "ASC" },
  { id: "price", name: "Price - Low to High", order: "ASC" },
  { id: "price", name: "Price - High to Low", order: "DESC" },
];

let globalSearch = "";
let apiRunning = false;

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

export default function AssetList(props) {
  const [open, setOpen] = useState(false);
  const [loader, setLoader] = useState(true);
  const [records, setRecords] = useState([]);
  const [confirmation, setConfirmation] = useState(false);
  const [deleteId, setDeleteId] = useState("");
  const [activeKey, setActiveKey] = useState("");
  const [alert, setAlert] = useState(false);
  const [deleteType, setDeleteType] = useState("NORMAL");
  const [sort, setSort] = useState(sortList[0]);
  const [searchName, setSearchName] = useState("");
  const [filterObj, setFilterObj] = useState({});

  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [paginationObj, setpaginationObj] = useState([]);
  const [isFilter, setIsFilter] = useState(false);
  const [openAlert, setOpenAlert] = useState(false);
  const [msg, setMsg] = useState("");
  const [alertType, setAlertType] = useState("Error");

  // GET ALL LIST
  const getList = async (
    newPage,
    newRowPerPage,
    filter = {},
    resultCheck = false
  ) => {
    props.setIsListUpdate(false);
    setLoader(true);
    let filterStatus = false;

    let filterByName;
    let formData = "";
    formData += "?page_size=" + newRowPerPage;
    formData += "&page=" + (newPage + 1);
    if (typeof filter.name !== "undefined") {
      formData += "&filter_by[name]=" + filter.name;
      filterByName = filter.name;
      filterStatus = true;
    } else {
      formData += "&filter_by[name]=" + searchName;
      filterByName = searchName;
      filterStatus = true;
    }

    if (typeof filter.sort !== "undefined") {
      formData +=
        "&sort_by[" + filter.sort.id + "]=" + filter.sort.order.toLowerCase();
    } else {
      formData += "&sort_by[" + sort.id + "]=" + sort.order.toLowerCase();
    }

    let fromFilter = false;
    if (typeof filter.fromFilter !== "undefined" && filter.fromFilter) {
      fromFilter = true;
      filterStatus = true;
    }

    if (
      typeof filter.paid_with_cash !== "undefined" &&
      filter.paid_with_cash &&
      fromFilter
    ) {
      formData += "&filter_by[paid_with_cash]=" + 1;
    } else if (
      typeof filterObj.paid_with_cash !== "undefined" &&
      filterObj.paid_with_cash &&
      !fromFilter
    ) {
      formData += "&filter_by[paid_with_cash]=" + 1;
    }

    if (typeof filter.start_date !== "undefined" && fromFilter) {
      formData += "&filter_by[start_date]=" + filter.start_date;
    } else if (typeof filterObj.start_date !== "undefined" && !fromFilter) {
      formData += "&filter_by[start_date]=" + filterObj.start_date;
    }

    if (typeof filter.end_date !== "undefined" && fromFilter) {
      formData += "&filter_by[end_date]=" + filter.end_date;
    } else if (typeof filterObj.end_date !== "undefined" && !fromFilter) {
      formData += "&filter_by[end_date]=" + filterObj.end_date;
    }

    if (typeof filter.min_price !== "undefined" && fromFilter) {
      formData += "&filter_by[min_price]=" + filter.min_price;
    } else if (typeof filterObj.min_price !== "undefined" && !fromFilter) {
      formData += "&filter_by[min_price]=" + filterObj.min_price;
    }

    if (typeof filter.max_price !== "undefined" && fromFilter) {
      formData += "&filter_by[max_price]=" + filter.max_price;
    } else if (typeof filterObj.max_price !== "undefined" && !fromFilter) {
      formData += "&filter_by[max_price]=" + filterObj.max_price;
    }

    let cIds = [];
    if (typeof filter.category_id !== "undefined" && fromFilter) {
      cIds = filter.category_id;
    } else if (typeof filterObj.category_id !== "undefined" && !fromFilter) {
      cIds = filterObj.category_id;
    }
    if (cIds.length > 0) {
      for (let i = 0; i < cIds.length; i++) {
        formData +=
          "&filter_by[account_subcategory_id][" + (i + 1) + "]=" + cIds[i];
      }
    }

    setIsFilter(filterStatus);
    apiRunning = true;
    const result = await List(formData);
    if (result.success) {
      if (resultCheck) {
        if (filterByName === globalSearch) {
          setRecords(result.data.asset_details);
          apiRunning = false;
          setpaginationObj({
            total: result.data.total,
            total_pages: result.data.total_pages,
          });
        }
      } else {
        setRecords(result.data.asset_details);
        apiRunning = false;
        setpaginationObj({
          total: result.data.total,
          total_pages: result.data.total_pages,
        });
      }
    }
    if (!apiRunning) {
      setLoader(false);
    }
  };

  useEffect(() => {
    if (props.isListUpdate) {
      setLoader(true);
      getList(0, rowsPerPage);
    }
  }, [props.isListUpdate]);

  useEffect(() => {
    getList(page, rowsPerPage, {}, true);
    // getList(page,rowsPerPage, filter);
  }, [searchName]);

  const handleConfirmationClose = async (id) => {
    if (id) {
      const formData = new FormData();
      formData.append("asset_id", id);

      const result = await Delete(formData);
      if (result.success) {
        records.splice(activeKey, 1);
        if (records.length === 0) {
          props.setEmptyView(true);
        }
        setRecords(records);
        setConfirmation(false);
        setAlertType("Success");
        setMsg(result.message);
        setOpenAlert(true);
      } else {
        setConfirmation(false);
      }
      setDeleteId("");
    } else {
      setDeleteId("");
      setConfirmation(false);
    }
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
    getList(newPage, rowsPerPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value));
    getList(page, parseInt(event.target.value));
  };

  const changeName = (event) => {
    // let filter = {
    //     name: event.target.value
    // };
    // setSearchName(event.target.value);
    // getList(page,rowsPerPage, filter);

    globalSearch = event.target.value;
    setSearchName(event.target.value);
  };

  const changeSort = (sortData) => {
    let filter = {
      sort: sortData,
    };
    setSort(sortData);
    getList(page, rowsPerPage, filter);
  };

  const applyFilter = (filter) => {
    setFilterObj(filter);
    filter.fromFilter = true;
    getList(page, rowsPerPage, filter);
    setOpen(false);
  };

  const changeAlert = (type = "") => {
    setAlert(!alert);
  };

  return (
    <div className="mt-8 w-full flex flex-col">
      <ConfirmationDialog
        id="ringtone-menu"
        title="Remove Asset?"
        message={
          deleteType === "NORMAL"
            ? "This asset will be permanently deleted."
            : "This asset is already reconciled, are you sure you want to delete the asset and undo the reconcilation?"
        }
        keepMounted
        open={confirmation}
        onClose={handleConfirmationClose}
        value={deleteId}
      />
      <AlertDialog
        id="ringtone-menu2"
        title="Note"
        message="This asset is already reconciled and can't be edited. You can open in view mode only."
        keepMounted
        open={alert}
        onClose={changeAlert}
      />
      <AlertDialog
        id="ringtone-menu2"
        title={alertType}
        message={msg}
        keepMounted
        open={openAlert}
        onClose={setOpenAlert}
      />

      <div className="-my-2 w-full shadow bg-white overflow-hidden border-b border-gray-200 sm:rounded-lg">
        <div className="pt-2 w-full align-middle inline-block min-w-full">
          <div>
            <div className="grid grid-cols-5 gap-5 p-7">
              <div className="col-span-3 mt-1 relative rounded-md">
                <div className="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
                  <SearchIcon
                    className="h-5 w-5 text-gray-500"
                    aria-hidden="true"
                  />
                </div>
                <input
                  type="text"
                  name="search"
                  id="search"
                  className="bg-gray-50 focus:ring-indigo-500 focus:border-indigo-500 block w-full pl-10 sm:text-sm p-3 border-gray-300 rounded-md"
                  placeholder="Search by asset name"
                  onChange={changeName}
                />
              </div>

              <div>
                <Listbox value={sort} onChange={changeSort}>
                  {({ open }) => (
                    <>
                      <div className="mt-1 relative">
                        <div className="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
                          <AdjustmentsIcon
                            className="h-5 w-5 z-10 text-gray-500"
                            aria-hidden="true"
                          />
                        </div>

                        <Listbox.Button className="bg-gray-100  relative w-full border border-gray-300 rounded-md shadow-sm pl-10 pr-10 py-3 text-left cursor-default focus:outline-none focus:ring-1 focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
                          <span className="block truncate">{sort.name}</span>
                          <span className="absolute inset-y-0 right-0 flex items-center pr-2 pointer-events-none">
                            <SelectorIcon
                              className="h-5 w-5 text-gray-400"
                              aria-hidden="true"
                            />
                          </span>
                        </Listbox.Button>

                        <Transition
                          show={open}
                          as={Fragment}
                          leave="transition ease-in duration-100"
                          leaveFrom="opacity-100"
                          leaveTo="opacity-0"
                        >
                          <Listbox.Options
                            static
                            className="absolute z-10 mt-1 w-full bg-white shadow-lg max-h-60 rounded-md py-1 text-base ring-1 ring-black ring-opacity-5 overflow-auto focus:outline-none sm:text-sm"
                          >
                            {sortList.map((status) => (
                              <Listbox.Option
                                key={status.id}
                                className={({ active }) =>
                                  classNames(
                                    active
                                      ? "text-white bg-brand-blue"
                                      : "text-gray-900",
                                    "cursor-default select-none relative py-2 pl-3 pr-9"
                                  )
                                }
                                value={status}
                              >
                                {({ selected, active }) => (
                                  <>
                                    <span
                                      className={classNames(
                                        selected
                                          ? "font-semibold"
                                          : "font-normal",
                                        "block truncate"
                                      )}
                                    >
                                      {status.name}
                                    </span>

                                    {selected ? (
                                      <span
                                        className={classNames(
                                          active
                                            ? "text-white"
                                            : "text-brand-blue",
                                          "absolute inset-y-0 right-0 flex items-center pr-4"
                                        )}
                                      >
                                        <CheckIcon
                                          className="h-5 w-5"
                                          aria-hidden="true"
                                        />
                                      </span>
                                    ) : null}
                                  </>
                                )}
                              </Listbox.Option>
                            ))}
                          </Listbox.Options>
                        </Transition>
                      </div>
                    </>
                  )}
                </Listbox>
              </div>

              {/* Status dropdown */}
              <div className="mt-1">
                <button
                  className="bg-gray-100 relative w-full border border-gray-300 rounded-md shadow-sm pl-10 pr-10 py-3 text-left cursor-default focus:outline-none focus:ring-1 focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                  onClick={() => {
                    setOpen(!open);
                  }}
                >
                  <span className="block truncate">Filter by</span>
                  <span className="absolute inset-y-0 right-0 flex items-center pr-2 pointer-events-none">
                    <SelectorIcon
                      className="h-5 w-5 text-gray-400"
                      aria-hidden="true"
                    />
                  </span>

                  <div className="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
                    <AdjustmentsIcon
                      className="h-5 w-5 z-10 text-gray-500"
                      aria-hidden="true"
                    />
                  </div>
                </button>
              </div>
            </div>

            <div className="overflow-y-auto w-full overflow-x-auto">
              <table className="min-w-full hover-row divide-y divide-gray-200">
                <thead className="">
                  <tr>
                    <th
                      scope="col"
                      className="px-7 py-3 text-left text-sm font-medium text-gray-500 uppercase tracking-wider"
                    >
                      #
                    </th>
                    <th
                      scope="col"
                      className="px-7 py-3 text-left text-sm font-medium text-gray-500 uppercase tracking-wider"
                    >
                      Category
                    </th>
                    <th
                      scope="col"
                      className="px-7 py-3 text-left text-sm font-medium text-gray-500 uppercase tracking-wider"
                    >
                      Asset Name
                    </th>
                    <th
                      scope="col"
                      className="px-7 py-3 text-left text-sm font-medium text-gray-500 uppercase tracking-wider"
                    >
                      Supplier
                    </th>
                    <th
                      scope="col"
                      className="px-7 py-3 text-left text-sm font-medium text-gray-500 uppercase tracking-wider"
                    >
                      Asset Value
                    </th>
                    <th
                      scope="col"
                      className="px-7 py-3 text-left text-sm font-medium text-gray-500 uppercase tracking-wider"
                    >
                      Date Of Purchase
                    </th>
                    <th
                      scope="col"
                      className="px-7 py-3 text-right text-sm font-medium text-gray-500 uppercase tracking-wider"
                    >
                      Actions
                    </th>
                  </tr>
                </thead>
                <tbody className="bg-white divide-y divide-gray-200">
                  {loader == false ? (
                    records.length > 0 ? (
                      records.map((val, key) => (
                        <tr key={key}>
                          <td className="px-6 py-4 whitespace-nowrap">
                            <div className="flex items-center">
                              <div className="text-md font-medium text-gray-900">
                                {rowsPerPage * page + key + 1}
                              </div>
                            </div>
                          </td>

                          <td className="px-7 py-4 flex items-center whitespace-nowrap">
                            <span className="inline-flex items-center justify-center h-12 w-12 rounded-full bg-gray-100">
                              <img
                                src={`/images/production/subcategory_pic/${getFileName(
                                  val.account_subcategory.subcategory_icon
                                )}`}
                                alt="Subcategory"
                                width="24"
                                height="24"
                                className="h-8 w-auto"
                              />
                            </span>
                            <div className="text-md pl-4 text-gray-900">
                              {val.account_subcategory.name}
                            </div>
                          </td>

                          <td className="px-7 py-4 whitespace-nowrap">
                            <div className="text-md text-gray-900">
                              {val.name}
                            </div>
                          </td>

                          <td className="px-7 py-4 whitespace-nowrap">
                            <div className="text-md text-gray-900">
                              {val?.client?.name}
                            </div>
                          </td>

                          <td className="px-7 py-4 whitespace-nowrap">
                            <div className="text-md font-semibold text-gray-900">
                              {val.price ? (
                                <NumberFormat
                                  value={parseFloat(val.price).toFixed(2)}
                                  displayType={"text"}
                                  thousandSeparator={true}
                                  prefix={"$"}
                                  renderText={(value) => <div>{value}</div>}
                                />
                              ) : (
                                "-"
                              )}
                            </div>
                          </td>

                          <td className="px-7 py-4 whitespace-nowrap">
                            <div className="text-md text-gray-900">
                              {formatShortDate(val.date_of_purchase)}
                            </div>
                          </td>

                          <td className="px-7 py-4 whitespace-nowrap text-right text-xsm flex justify-end items-center font-medium">
                            <button
                              className="text-gray-700 flex px-2 py-2 text-sm hover:text-brand-blue"
                              onClick={() => {
                                props.setIsInfo(true);
                                props.setUpdateId(val.asset_id);
                                props.setIsOpen(true);
                              }}
                            >
                              <span data-for="view" data-tip="View">
                                <svg
                                  xmlns="http://www.w3.org/2000/svg"
                                  className="h-6 w-6"
                                  fill="none"
                                  viewBox="0 0 24 24"
                                  stroke="currentColor"
                                  strokeWidth="2"
                                >
                                  <path
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                    d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"
                                  />
                                  <path
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                    d="M2.458 12C3.732 7.943 7.523 5 12 5c4.478 0 8.268 2.943 9.542 7-1.274 4.057-5.064 7-9.542 7-4.477 0-8.268-2.943-9.542-7z"
                                  />
                                </svg>
                              </span>
                              <ReactTooltip
                                id="view"
                                className="custom-tooltip bg-gray-900"
                                textColor="#ffffff"
                                backgroundColor="#111827"
                                effect="solid"
                              />
                            </button>

                            <button
                              className="text-gray-700 flex px-2 py-2 text-sm hover:text-brand-blue"
                              onClick={() => {
                                if (
                                  val.bank_transaction_id !== null &&
                                  parseInt(val.bank_transaction_id) > 0
                                ) {
                                  changeAlert("EDIT");
                                } else {
                                  props.setUpdateId(val.asset_id);
                                  props.setIsOpen(true);
                                }
                              }}
                            >
                              <span data-for="edit" data-tip="Edit">
                                <svg
                                  xmlns="http://www.w3.org/2000/svg"
                                  className="h-6 w-6"
                                  fill="none"
                                  viewBox="0 0 24 24"
                                  stroke="currentColor"
                                  strokeWidth="2"
                                >
                                  <path
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                    d="M15.232 5.232l3.536 3.536m-2.036-5.036a2.5 2.5 0 113.536 3.536L6.5 21.036H3v-3.572L16.732 3.732z"
                                  />
                                </svg>
                              </span>
                              <ReactTooltip
                                id="edit"
                                className="custom-tooltip bg-gray-900"
                                textColor="#ffffff"
                                backgroundColor="#111827"
                                effect="solid"
                              />
                            </button>

                            <button
                              className="text-gray-700 flex px-2 py-2 text-sm hover:text-brand-blue"
                              onClick={() => {
                                if (
                                  val.bank_transaction_id !== null &&
                                  parseInt(val.bank_transaction_id) > 0
                                ) {
                                  setDeleteType("RECONCILE");
                                } else {
                                  setDeleteType("NORMAL");
                                }
                                setDeleteId(val.asset_id);
                                setActiveKey(key);
                                setConfirmation(true);
                              }}
                            >
                              <span data-for="delete" data-tip="Delete">
                                <svg
                                  xmlns="http://www.w3.org/2000/svg"
                                  className="h-6 w-6"
                                  fill="none"
                                  viewBox="0 0 24 24"
                                  stroke="currentColor"
                                  strokeWidth="2"
                                >
                                  <path
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                    d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"
                                  />
                                </svg>
                              </span>
                              <ReactTooltip
                                id="delete"
                                className="custom-tooltip bg-gray-900"
                                textColor="#ffffff"
                                backgroundColor="#111827"
                                effect="solid"
                              />
                            </button>
                          </td>
                        </tr>
                      ))
                    ) : (
                      <tr>
                        <td colspan="7" className="py-5" align="center">
                          <span>No records found.</span>
                        </td>
                      </tr>
                    )
                  ) : (
                    Array.from(Array(5), (e, i) => {
                      return (
                        <tr key={i}>
                          <td className="px-6 py-4 whitespace-nowrap">
                            <Skeleton />
                          </td>
                          <td className="px-7 py-4 whitespace-nowrap">
                            <Skeleton />
                          </td>
                          <td className="px-7 py-4 whitespace-nowrap">
                            <Skeleton />
                          </td>
                          <td className="px-7 py-4 whitespace-nowrap">
                            <Skeleton />
                          </td>
                          <td className="px-7 py-4 whitespace-nowrap">
                            <Skeleton />
                          </td>
                          <td className="px-7 py-4 whitespace-nowrap">
                            <Skeleton />
                          </td>
                          <td className="px-7 py-4 whitespace-nowrap text-right text-xsm font-medium">
                            <Skeleton />
                          </td>
                        </tr>
                      );
                    })
                  )}
                </tbody>
              </table>
            </div>
            {loader == false ? (
              <Pagination
                total={paginationObj.total}
                page={page}
                onPageChange={handleChangePage}
                rowsPerPage={rowsPerPage}
                onRowsPerPageChange={handleChangeRowsPerPage}
              />
            ) : null}
          </div>
        </div>
      </div>

      <Filter
        open={open}
        setOpen={setOpen}
        categories={props.accountCategories}
        filterObj={filterObj}
        applyFilter={applyFilter}
      />
    </div>
  );
}
