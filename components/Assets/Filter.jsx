import { Fragment, useState } from "react";
import { Dialog, Transition, Switch } from "@headlessui/react";
import { XIcon } from "@heroicons/react/outline";
import { Form, Formik } from "formik";
import { enGB } from "date-fns/locale";
import Styles from "../../styles/Invoices.module.scss";
import { formatDateToDB } from "../../helpers/common";
import DateTimePicker from "../DateTimePicker";

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

export default function Filter(props) {
  const { clients = [], categories = [] } = props;
  const [dateRange, setDateRange] = useState([null, null]);
  const [startDate, endDate] = dateRange;
  const [from, setFrom] = useState("");
  const [to, setTo] = useState("");
  const [catIds, setCatIds] = useState(
    typeof props.filterObj.category_id !== "undefined"
      ? props.filterObj.category_id
      : []
  );

  const [bycashEnabled, setBycashEnabled] = useState(false);
  const [bydateEnabled, setBydateEnabled] = useState(false);
  const [byrangeEnabled, setByrangeEnabled] = useState(false);
  const [bycategoryEnabled, setBycategoryEnabled] = useState(false);

  const [showDate, setShowDate] = useState(false);
  const [showRange, setShowRange] = useState(false);
  const [showCategory, setShowCategory] = useState(false);

  function togglebydate() {
    setShowDate(!showDate);
  }

  function togglebyrange() {
    if (showRange) {
      setFrom("");
      setTo("");
    }
    setShowRange(!showRange);
  }

  function togglebycategory() {
    setShowCategory(!showCategory);
  }

  const setCategories = (id) => {
    let ids = catIds;
    let index = ids.indexOf(id);
    if (index === -1) {
      //add
      ids.push(id);
    } else {
      //remove
      ids.splice(index, 1);
    }
    setCatIds(ids);
  };

  const apply = () => {
    let filter = {};
    if (bycashEnabled) {
      filter.paid_with_cash = 1;
    }

    if (showDate) {
      if (startDate) {
        filter.start_date = formatDateToDB(startDate);
      }
      if (endDate) {
        filter.end_date = formatDateToDB(endDate);
      }
    }

    if (showRange) {
      if (from) {
        filter.min_price = from;
      }
      if (to) {
        filter.max_price = to;
      }
    }

    if (showCategory) {
      filter.category_id = catIds;
    }
    props.applyFilter(filter);
  };

  const reset = () => {
    setBycashEnabled(false);
    setBycategoryEnabled(false);
    setShowCategory(false);
    setCatIds([]);
    setByrangeEnabled(false);
    setShowRange(false);
    setShowDate(false);
    setBydateEnabled(false);
    setDateRange([null, null]);
    //setStartDate(null);
    //setEndDate(null);
    setFrom(null);
    setTo(null);
    let filter = {};
    props.applyFilter(filter);
  };

  return (
    <Transition.Root show={props.open} as={Fragment}>
      <Dialog
        as="div"
        static
        className="fixed inset-0 overflow-hidden"
        onClose={props.setOpen}
      >
        <div className="absolute inset-0 overflow-hidden">
          <Transition.Child
            as={Fragment}
            enter="ease-in-out duration-500"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in-out duration-500"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <Dialog.Overlay className="absolute inset-0 bg-gray-500 bg-opacity-75 transition-opacity pointer-events-none" />
          </Transition.Child>

          <div className="fixed inset-y-0 max-w-full right-0 flex">
            <Transition.Child
              as={Fragment}
              enter="transform transition ease-in-out duration-500 sm:duration-700"
              enterFrom="translate-x-full"
              enterTo="translate-x-0"
              leave="transform transition ease-in-out duration-500 sm:duration-700"
              leaveFrom="translate-x-0"
              leaveTo="translate-x-full"
            >
              <div className="w-screen lg:max-w-2xl">
                <Formik>
                  <Form className="h-full divide-y divide-gray-200 flex flex-col bg-white shadow-xl">
                    <div className="flex-1 h-0 pb-8 overflow-y-auto">
                      <div className="border-b border-gray-200 bg-gray-100 py-4 px-4 sm:px-6">
                        <div className="flex items-center justify-between">
                          <Dialog.Title className="text-xl font-medium text-gray-800">
                            Asset Filter
                          </Dialog.Title>
                          <div className="ml-3 h-7 flex items-center">
                            <button
                              type="button"
                              className="rounded-full hover:bg-gray-300 p-2 text-gray-500 hover:text-gray-700 focus:outline-none focus:ring-2 focus:ring-white"
                              onClick={() => props.setOpen(false)}
                            >
                              <span className="sr-only">Close panel</span>
                              <XIcon className="h-6 w-6" aria-hidden="true" />
                            </button>
                          </div>
                        </div>
                      </div>

                      <div className="flex-1 flex flex-col justify-between">
                        <div className="px-4 divide-y divide-gray-200 sm:px-6">
                          <div className="space-y-6 border-gray-200 mb-4 pt-6 pb-7 grid grid-cols-1 gap-y-2 sm:grid-cols-1 sm:gap-x-8">
                            <div className="bg-gray-100 relative shadow sm:rounded-lg">
                              <Switch.Group
                                as="div"
                                className="px-4  py-5 sm:p-6"
                              >
                                <div className="inline-flex justify-between w-full">
                                  <Switch.Label
                                    as="h3"
                                    className="text-lg leading-6 font-medium text-gray-700"
                                  >
                                    Paid by Cash
                                  </Switch.Label>
                                  <div className="mt-5 sm:mt-0 sm:ml-6 sm:flex-shrink-0 sm:flex sm:items-center">
                                    <Switch
                                      checked={bycashEnabled}
                                      onChange={(e) => {
                                        setBycashEnabled(e);
                                      }}
                                      className={classNames(
                                        bycashEnabled
                                          ? "bg-green-600"
                                          : "bg-gray-300",
                                        "relative inline-flex flex-shrink-0 h-6 w-11 border-2 border-transparent rounded-full cursor-pointer transition-colors ease-in-out duration-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                                      )}
                                    >
                                      <span
                                        className={classNames(
                                          bycashEnabled
                                            ? "translate-x-5"
                                            : "translate-x-0",
                                          "pointer-events-none relative inline-block h-5 w-5 rounded-full bg-white shadow transform ring-0 transition ease-in-out duration-200"
                                        )}
                                      >
                                        <span
                                          className={classNames(
                                            bycashEnabled
                                              ? "opacity-0 ease-out duration-100"
                                              : "opacity-100 ease-in duration-200",
                                            "absolute inset-0 h-full w-full flex items-center justify-center transition-opacity pointer-events-none"
                                          )}
                                          aria-hidden="true"
                                        >
                                          <svg
                                            className="h-3 w-3 text-gray-400"
                                            fill="none"
                                            viewBox="0 0 12 12"
                                          >
                                            <path
                                              d="M4 8l2-2m0 0l2-2M6 6L4 4m2 2l2 2"
                                              stroke="currentColor"
                                              strokeWidth={2}
                                              strokeLinecap="round"
                                              strokeLinejoin="round"
                                            />
                                          </svg>
                                        </span>
                                        <span
                                          className={classNames(
                                            bycashEnabled
                                              ? "opacity-100 ease-in duration-200"
                                              : "opacity-0 ease-out duration-100",
                                            "absolute inset-0 h-full w-full flex items-center justify-center transition-opacity pointer-events-none"
                                          )}
                                          aria-hidden="true"
                                        >
                                          <svg
                                            className="h-3 w-3 text-green-600"
                                            fill="currentColor"
                                            viewBox="0 0 12 12"
                                          >
                                            <path d="M3.707 5.293a1 1 0 00-1.414 1.414l1.414-1.414zM5 8l-.707.707a1 1 0 001.414 0L5 8zm4.707-3.293a1 1 0 00-1.414-1.414l1.414 1.414zm-7.414 2l2 2 1.414-1.414-2-2-1.414 1.414zm3.414 2l4-4-1.414-1.414-4 4 1.414 1.414z" />
                                          </svg>
                                        </span>
                                      </span>
                                    </Switch>
                                  </div>
                                </div>
                              </Switch.Group>
                            </div>

                            {/* Date filter */}
                            <div className="bg-gray-100 relative shadow sm:rounded-lg">
                              <Switch.Group
                                as="div"
                                className="px-4  py-5 sm:p-6"
                              >
                                <div className="inline-flex justify-between w-full">
                                  <Switch.Label
                                    as="h3"
                                    className="text-lg leading-6 font-medium text-gray-700"
                                  >
                                    Date Range
                                  </Switch.Label>
                                  <div className="mt-5 sm:mt-0 sm:ml-6 sm:flex-shrink-0 sm:flex sm:items-center">
                                    <Switch
                                      checked={bydateEnabled}
                                      onChange={(e) => {
                                        setBydateEnabled(e);
                                        togglebydate();
                                      }}
                                      className={classNames(
                                        bydateEnabled
                                          ? "bg-green-600"
                                          : "bg-gray-300",
                                        "relative inline-flex flex-shrink-0 h-6 w-11 border-2 border-transparent rounded-full cursor-pointer transition-colors ease-in-out duration-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                                      )}
                                    >
                                      <span
                                        className={classNames(
                                          bydateEnabled
                                            ? "translate-x-5"
                                            : "translate-x-0",
                                          "pointer-events-none relative inline-block h-5 w-5 rounded-full bg-white shadow transform ring-0 transition ease-in-out duration-200"
                                        )}
                                      >
                                        <span
                                          className={classNames(
                                            bydateEnabled
                                              ? "opacity-0 ease-out duration-100"
                                              : "opacity-100 ease-in duration-200",
                                            "absolute inset-0 h-full w-full flex items-center justify-center transition-opacity pointer-events-none"
                                          )}
                                          aria-hidden="true"
                                        >
                                          <svg
                                            className="h-3 w-3 text-gray-400"
                                            fill="none"
                                            viewBox="0 0 12 12"
                                          >
                                            <path
                                              d="M4 8l2-2m0 0l2-2M6 6L4 4m2 2l2 2"
                                              stroke="currentColor"
                                              strokeWidth={2}
                                              strokeLinecap="round"
                                              strokeLinejoin="round"
                                            />
                                          </svg>
                                        </span>
                                        <span
                                          className={classNames(
                                            bydateEnabled
                                              ? "opacity-100 ease-in duration-200"
                                              : "opacity-0 ease-out duration-100",
                                            "absolute inset-0 h-full w-full flex items-center justify-center transition-opacity pointer-events-none"
                                          )}
                                          aria-hidden="true"
                                        >
                                          <svg
                                            className="h-3 w-3 text-green-600"
                                            fill="currentColor"
                                            viewBox="0 0 12 12"
                                          >
                                            <path d="M3.707 5.293a1 1 0 00-1.414 1.414l1.414-1.414zM5 8l-.707.707a1 1 0 001.414 0L5 8zm4.707-3.293a1 1 0 00-1.414-1.414l1.414 1.414zm-7.414 2l2 2 1.414-1.414-2-2-1.414 1.414zm3.414 2l4-4-1.414-1.414-4 4 1.414 1.414z" />
                                          </svg>
                                        </span>
                                      </span>
                                    </Switch>
                                  </div>
                                </div>
                              </Switch.Group>

                              <div
                                className={classNames(
                                  showDate ? "block" : "hidden",
                                  "collapse px-4 py-5 sm:p-6 border-t border-gray-200"
                                )}
                              >
                                <div className="grid grid-cols-1 mt-0">
                                  <div className="relative date-picker bg-white border-2 focus:ring-indigo-400 focus:border-indigo-400 border-gray-200 rounded-md">
                                    <DateTimePicker
                                      range={true}
                                      startDate={startDate}
                                      endDate={endDate}
                                      onDateChange={(val) => {
                                        setDateRange(val);
                                      }}
                                      inputClassName={
                                        "input py-3 px-4 block w-full border-0"
                                      }
                                      maxDate={new Date()}
                                      locale={enGB}
                                    />
                                  </div>
                                </div>
                              </div>
                            </div>

                            {/* Range filter */}
                            <div className="bg-gray-100 relative shadow sm:rounded-lg">
                              <Switch.Group
                                as="div"
                                className="px-4  py-5 sm:p-6"
                              >
                                <div className="inline-flex justify-between w-full">
                                  <Switch.Label
                                    as="h3"
                                    className="text-lg leading-6 font-medium text-gray-700"
                                  >
                                    Range ($)
                                  </Switch.Label>
                                  <div className="mt-5 sm:mt-0 sm:ml-6 sm:flex-shrink-0 sm:flex sm:items-center">
                                    <Switch
                                      checked={byrangeEnabled}
                                      onChange={(e) => {
                                        setByrangeEnabled(e);
                                        togglebyrange();
                                      }}
                                      className={classNames(
                                        byrangeEnabled
                                          ? "bg-green-600"
                                          : "bg-gray-300",
                                        "relative inline-flex flex-shrink-0 h-6 w-11 border-2 border-transparent rounded-full cursor-pointer transition-colors ease-in-out duration-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                                      )}
                                    >
                                      <span
                                        className={classNames(
                                          byrangeEnabled
                                            ? "translate-x-5"
                                            : "translate-x-0",
                                          "pointer-events-none relative inline-block h-5 w-5 rounded-full bg-white shadow transform ring-0 transition ease-in-out duration-200"
                                        )}
                                      >
                                        <span
                                          className={classNames(
                                            byrangeEnabled
                                              ? "opacity-0 ease-out duration-100"
                                              : "opacity-100 ease-in duration-200",
                                            "absolute inset-0 h-full w-full flex items-center justify-center transition-opacity pointer-events-none"
                                          )}
                                          aria-hidden="true"
                                        >
                                          <svg
                                            className="h-3 w-3 text-gray-400"
                                            fill="none"
                                            viewBox="0 0 12 12"
                                          >
                                            <path
                                              d="M4 8l2-2m0 0l2-2M6 6L4 4m2 2l2 2"
                                              stroke="currentColor"
                                              strokeWidth={2}
                                              strokeLinecap="round"
                                              strokeLinejoin="round"
                                            />
                                          </svg>
                                        </span>
                                        <span
                                          className={classNames(
                                            byrangeEnabled
                                              ? "opacity-100 ease-in duration-200"
                                              : "opacity-0 ease-out duration-100",
                                            "absolute inset-0 h-full w-full flex items-center justify-center transition-opacity pointer-events-none"
                                          )}
                                          aria-hidden="true"
                                        >
                                          <svg
                                            className="h-3 w-3 text-green-600"
                                            fill="currentColor"
                                            viewBox="0 0 12 12"
                                          >
                                            <path d="M3.707 5.293a1 1 0 00-1.414 1.414l1.414-1.414zM5 8l-.707.707a1 1 0 001.414 0L5 8zm4.707-3.293a1 1 0 00-1.414-1.414l1.414 1.414zm-7.414 2l2 2 1.414-1.414-2-2-1.414 1.414zm3.414 2l4-4-1.414-1.414-4 4 1.414 1.414z" />
                                          </svg>
                                        </span>
                                      </span>
                                    </Switch>
                                  </div>
                                </div>
                              </Switch.Group>

                              <div
                                className={classNames(
                                  showRange ? "block" : "hidden",
                                  "collapse px-4 py-5 sm:p-6 border-t border-gray-200"
                                )}
                              >
                                <div className="grid grid-cols-1 mt-0">
                                  <div>
                                    <div className="flex gap-x-2">
                                      <div className="w-6/12">
                                        <span className="block mb-2">From</span>
                                        <input
                                          type="text"
                                          className="input w-full rounded-md border-2 border-gray-200 py-3 px-4 mr-2 text-sm font-medium bg-white"
                                          onChange={(e) =>
                                            setFrom(e.target.value)
                                          }
                                          value={from}
                                        />
                                      </div>

                                      <div className="w-6/12">
                                        <span className="block mb-2">To</span>
                                        <input
                                          type="text"
                                          className="input w-full rounded-md border-2 border-gray-200 py-3 px-4 mr-2 text-sm font-medium bg-white"
                                          onChange={(e) =>
                                            setTo(e.target.value)
                                          }
                                          value={to}
                                        />
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>

                            {/* Category filter */}
                            <div className="bg-gray-100 relative shadow sm:rounded-lg">
                              <Switch.Group
                                as="div"
                                className="px-4  py-5 sm:p-6"
                              >
                                <div className="inline-flex justify-between w-full">
                                  <Switch.Label
                                    as="h3"
                                    className="text-lg leading-6 font-medium text-gray-700"
                                  >
                                    Category
                                  </Switch.Label>
                                  <div className="mt-5 sm:mt-0 sm:ml-6 sm:flex-shrink-0 sm:flex sm:items-center">
                                    <Switch
                                      checked={bycategoryEnabled}
                                      onChange={(e) => {
                                        setBycategoryEnabled(e);
                                        togglebycategory();
                                      }}
                                      className={classNames(
                                        bycategoryEnabled
                                          ? "bg-green-600"
                                          : "bg-gray-300",
                                        "relative inline-flex flex-shrink-0 h-6 w-11 border-2 border-transparent rounded-full cursor-pointer transition-colors ease-in-out duration-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                                      )}
                                    >
                                      <span
                                        className={classNames(
                                          bycategoryEnabled
                                            ? "translate-x-5"
                                            : "translate-x-0",
                                          "pointer-events-none relative inline-block h-5 w-5 rounded-full bg-white shadow transform ring-0 transition ease-in-out duration-200"
                                        )}
                                      >
                                        <span
                                          className={classNames(
                                            bycategoryEnabled
                                              ? "opacity-0 ease-out duration-100"
                                              : "opacity-100 ease-in duration-200",
                                            "absolute inset-0 h-full w-full flex items-center justify-center transition-opacity pointer-events-none"
                                          )}
                                          aria-hidden="true"
                                        >
                                          <svg
                                            className="h-3 w-3 text-gray-400"
                                            fill="none"
                                            viewBox="0 0 12 12"
                                          >
                                            <path
                                              d="M4 8l2-2m0 0l2-2M6 6L4 4m2 2l2 2"
                                              stroke="currentColor"
                                              strokeWidth={2}
                                              strokeLinecap="round"
                                              strokeLinejoin="round"
                                            />
                                          </svg>
                                        </span>
                                        <span
                                          className={classNames(
                                            bycategoryEnabled
                                              ? "opacity-100 ease-in duration-200"
                                              : "opacity-0 ease-out duration-100",
                                            "absolute inset-0 h-full w-full flex items-center justify-center transition-opacity pointer-events-none"
                                          )}
                                          aria-hidden="true"
                                        >
                                          <svg
                                            className="h-3 w-3 text-green-600"
                                            fill="currentColor"
                                            viewBox="0 0 12 12"
                                          >
                                            <path d="M3.707 5.293a1 1 0 00-1.414 1.414l1.414-1.414zM5 8l-.707.707a1 1 0 001.414 0L5 8zm4.707-3.293a1 1 0 00-1.414-1.414l1.414 1.414zm-7.414 2l2 2 1.414-1.414-2-2-1.414 1.414zm3.414 2l4-4-1.414-1.414-4 4 1.414 1.414z" />
                                          </svg>
                                        </span>
                                      </span>
                                    </Switch>
                                  </div>
                                </div>
                              </Switch.Group>

                              <div
                                className={classNames(
                                  showCategory ? "block" : "hidden",
                                  "collapse px-4 py-5 sm:p-6 border-t border-gray-200"
                                )}
                              >
                                <div className="grid grid-cols-1 mt-0">
                                  <div className="bg-white rounded-bl-md rounded-br-md">
                                    <h3 className="text-lg bg-gray-200 rounded-tl-md rounded-tr-md p-3 border-b border-gray-300 text-gray-700">
                                      Assets
                                    </h3>

                                    <div className="divide-y divide-gray-100">
                                      {categories.map((category, key) => (
                                        <div
                                          key={key}
                                          className="relative flex items-center py-2"
                                        >
                                          <div className="min-w-0  flex-1 text-sm">
                                            <label
                                              for={`category_${key}`}
                                              className="font-medium p-4  text-gray-700 select-none"
                                            >
                                              {category.name}
                                            </label>
                                          </div>

                                          <div className="ml-3 flex items-center p-4 h-5">
                                            <input
                                              id={`category_${key}`}
                                              name={`category_${key}`}
                                              type="checkbox"
                                              value={
                                                category.account_subcategory_id
                                              }
                                              className="focus:ring-indigo-500 h-4 w-4 text-brand-blue rounded-full border-0 bg-gray-300"
                                              onClick={(e) =>
                                                setCategories(
                                                  category.account_subcategory_id
                                                )
                                              }
                                              defaultChecked={
                                                catIds.indexOf(
                                                  category.account_subcategory_id
                                                ) !== -1
                                              }
                                            />
                                          </div>
                                        </div>
                                      ))}
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="flex-shrink-0  px-4 py-4 flex justify-between">
                      <div>
                        <button
                          type="button"
                          className="ml-4 inline-flex justify-center py-2 px-4 tracking-wider border border-transparent uppercase shadow-sm  font-medium text-sm rounded-full text-white bg-red-600 hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-500"
                          onClick={reset}
                        >
                          Reset filter
                        </button>
                      </div>
                      <div>
                        <button
                          type="button"
                          className="ml-4 inline-flex justify-center py-2 px-4 tracking-wider border border-transparent uppercase shadow-sm  font-medium text-sm rounded-full text-white bg-green-600 hover:bg-green-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-500"
                          onClick={apply}
                        >
                          Apply filter
                        </button>
                      </div>
                    </div>
                  </Form>
                </Formik>
              </div>
            </Transition.Child>
          </div>
        </div>
      </Dialog>
    </Transition.Root>
  );
}
