import clsx from "clsx";
import Styles from "../../styles/Home.module.scss";
import { useEffect, useState } from "react";
import { GoogleLogin } from "react-google-login";
import { signUp, login } from "../../services/api/auth.services";
import AppleLogin from "react-apple-login";
import {
  getDeviceId,
  setLoginResponse,
  setUserEmail,
  deleteAll,
} from "../../services/cookies.services";
import { registerDevice } from "../../services/api/device.services";
import { useRouter } from "next/router";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import AlertDialog from "../AlertDialog";
import SocialButton from "./SocialButton";

const process = "Processing...";

export default function Social(props) {
  const router = useRouter();
  const [dialogOpen, setDialogOpen] = useState(false);
  const [dialogMsg, setDialogMsg] = useState("");
  const [openAlert, setOpenAlert] = useState(false);
  const [msg, setMsg] = useState("");
  const [alertType, setAlertType] = useState("Error");

  const apiCall = async (formData, isLogin = true) => {
    setDialogMsg(process);
    setDialogOpen(true);

    var result = null;
    let device_id = getDeviceId() ? getDeviceId() : null;
    if (device_id === null) {
      await registerDevice();
    }
    device_id = getDeviceId() ? getDeviceId() : null;

    formData.append("device_id", device_id);
    if (!isLogin) {
      result = await signUp(formData, "yes");
    } else {
      result = await login(formData, "yes");
    }
    if (result.success) {
      if (!isLogin) {
        setLoginResponse(result.data.user);
        setUserEmail(result.data.user.email);

        setDialogMsg("");
        setDialogOpen(false);

        localStorage.setItem("display-welcome-popup", true);
        router.push({
          pathname: "/dashboard",
        });
      } else {
        setLoginResponse(result.data.user);
        setUserEmail(result.data.user.email);

        router.push({
          pathname: "/dashboard",
        });
      }
    } else {
      if (isLogin) {
        apiCall(formData, false);
        return;
      }
      setDialogMsg(result.message);
      setDialogOpen(true);
    }
  };

  const responseGoogle = async (response) => {
    if (response) {
      if (response.error == "popup_closed_by_user") {
        return false;
      }

      if (response.profileObj && response.profileObj.googleId != "") {
        if (response.profileObj.email != "") {
          const formData = new FormData();
          var name = response.profileObj.name.split(" ");
          formData.append("email", response.profileObj.email);
          formData.append("signup_source", 3);
          formData.append("first_name", name[0] ? name[0] : "");
          formData.append("last_name", name[1] ? name[1] : "");
          formData.append("social_access_token", response.accessToken);

          apiCall(formData);
        } else {
          setAlertType("Error");
          setMsg("Email is required.");
          setOpenAlert(true);
        }
      } else {
        setAlertType("Error");
        setMsg(
          response.details || "Something went wrong, please try again later."
        );
        setOpenAlert(true);
      }
    }
  };

  const errorGoogle = async (error) => {
    if (error) {
      if (error.error == "idpiframe_initialization_failed") {
        //block cookie in incongnito mode
        setAlertType("Error");
        setMsg(
          error.details || "Something went wrong, please try again later."
        );
        setOpenAlert(true);
      }
    }
  };

  const responseFacebook = async (response) => {
    if (
      typeof response._profile !== "undefined" &&
      typeof response._profile.email !== "undefined" &&
      response._profile.email != "" &&
      typeof response._token !== "undefined" &&
      typeof response._token.accessToken !== "undefined" &&
      response._token.accessToken != ""
    ) {
      const formData = new FormData();
      formData.append("email", response._profile.email);
      formData.append("signup_source", 2);
      formData.append("first_name", response._profile.firstName);
      formData.append("last_name", response._profile.lastName);
      formData.append("social_access_token", response._token.accessToken);

      apiCall(formData);
    } else {
      setAlertType("Error");
      setMsg("Something went wrong, please try again later.");
      setOpenAlert(true);
    }
  };

  const errorFacebook = async (error) => {
    setAlertType("Error");
    setMsg("Something went wrong, please try again later.");
    setOpenAlert(true);
  };

  const responseApple = async (response) => {
    if (response) {
      if (response.error && response.error == "popup_closed_by_user") {
        return false;
      }
      if (
        typeof response.authorization !== "undefined" &&
        typeof response.authorization.id_token !== "undefined"
      ) {
        let userInfo = response.authorization.id_token.split(".")[1];
        userInfo = JSON.parse(atob(userInfo));

        if (typeof userInfo.email !== "undefined" && userInfo.email !== "") {
          const formData = new FormData();
          formData.append("email", userInfo.email);
          formData.append("signup_source", 5);
          formData.append(
            "first_name",
            typeof userInfo.name !== "undefined" &&
              typeof userInfo.name.firstName !== "undefined"
              ? userInfo.name.firstName
              : ""
          );
          formData.append(
            "last_name",
            typeof userInfo.name !== "undefined" &&
              typeof userInfo.name.lastName !== "undefined"
              ? userInfo.name.lastName
              : ""
          );
          formData.append(
            "social_access_token",
            response.authorization.id_token
          );
          apiCall(formData);
        } else {
          setAlertType("Error");
          setMsg("Email is required.");
          setOpenAlert(true);
        }
      }
    }
  };

  useEffect(async () => {
    deleteAll();
  }, []);

  return (
    <>
      <AlertDialog
        id="ringtone-menu2"
        title={alertType}
        message={msg}
        keepMounted
        open={openAlert}
        onClose={setOpenAlert}
      />
      <GoogleLogin
        //clientId="940579618961-8ms2bd9lcqfpca39pvr8fk3gt0ieh3ae.apps.googleusercontent.com"
        clientId="202415546798-f79sc453se03bv1ltg0l5utmgbk59f7n.apps.googleusercontent.com"
        render={(renderProps) => (
          <button
            onClick={renderProps.onClick}
            disabled={renderProps.disabled}
            className={clsx(Styles.roundBtn, "grey-bg")}
          >
            <img
              src="/images/icon/google.svg"
              alt="Google"
              width={25}
              height={25}
            />
          </button>
        )}
        onSuccess={(res) => responseGoogle(res)}
        onFailure={(err) => errorGoogle(err)}
        isSignedIn={false}
      />
      <SocialButton
        provider="facebook"
        appId="862252904296668"
        onLoginSuccess={responseFacebook}
        onLoginFailure={errorFacebook}
      >
        <img src="/images/icon/fb.svg" alt="Facebook" width={25} height={25} />
      </SocialButton>
      <AppleLogin
        clientId="au.com.soleapp"
        scope={"email name"}
        redirectURI="https://webapp.soleapp.com.au/"
        responseMode="form_post"
        callback={(res) => responseApple(res)}
        usePopup={true}
        render={(renderProps) => (
          <button
            onClick={renderProps.onClick}
            className={clsx(Styles.roundBtn, "grey-bg")}
          >
            <img
              src="/images/icon/mac.svg"
              alt="Apple"
              width={25}
              height={25}
            />
          </button>
        )}
      />

      <Dialog
        open={dialogOpen}
        onClose={(event, reason) => {
          if (reason !== "backdropClick") {
            setDialogOpen(false);
            deleteAll();
          }
        }}
        aria-describedby="alert-dialog-slide-description"
      >
        <div style={{ width: 300 }}>
          <DialogTitle>Sole App</DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-slide-description">
              {dialogMsg != process ? (
                dialogMsg
              ) : (
                <p style={{ textAlign: "center", marginTop: 0 }}>{dialogMsg}</p>
              )}
            </DialogContentText>
          </DialogContent>
          {dialogMsg != process ? (
            <DialogActions>
              <Button
                onClick={() => {
                  setDialogOpen(false);
                  deleteAll();
                }}
              >
                Ok
              </Button>
            </DialogActions>
          ) : null}
        </div>
      </Dialog>
    </>
  );
}
