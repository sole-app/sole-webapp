import clsx from "clsx";
import { makeStyles } from "@material-ui/core/styles";
import {
  TextField,
  InputLabel,
  MenuItem,
  FormHelperText,
  FormControl,
  Select,
} from "@material-ui/core";
import Styles from "../../styles/Register.module.scss";
import { Field, Formik, Form, ErrorMessage } from "formik";
import { getUserEmail, getUserData } from "../../services/cookies.services";
import IndustryServices from "../../services/industry.services";
import React, { useEffect, useState, Fragment } from "react";
import { updateUserProfile } from "../../services/api/user.services";
import AccountDetailsSchema from "../../schemas/AccountDetails.schema";
import ReactTooltip from "react-tooltip";
import { InformationCircleIcon } from "@heroicons/react/outline";
import AddressAutoComplete from "../Address/AddressAutoComplete";
import { useRouter } from "next/router";
import { Dialog, Transition } from "@headlessui/react";
import { ImpulseSpinner } from "react-spinners-kit";

const useStyles = makeStyles((theme) => ({
  fControl: {
    width: "100%",
  },
  fSelection: {
    minWidth: "223px",
  },
  selectEmpty: {
    marginTop: theme.spacing(3),
  },
  maxHeight: {
    maxHeight: 100,
  },
  root: {
    padding: "1em",
    paddingTop: "2em",
    boxSizing: "border-box",
  },
  btnMargin10: {
    margin: "10px 10px",
  },
}));

const initialValues = {
  first_name: "",
  last_name: "",
  full_name: "",
  email: "",
  business_name: "",
  industry_id: "",
  mobile_no: "",
  ABN: "",
  TFN: "",
  address: "",
  profile_pic: "",
  google_address: false,
};

const industryOptions = [];
Object.keys(IndustryServices).forEach(function (key) {
  industryOptions.push(
    <MenuItem value={key} key={key}>
      {IndustryServices[key]}
    </MenuItem>
  );
});

export default function AccountDetails(props) {
  const classes = useStyles();
  const router = useRouter();
  const [getInitialValues, setInitialValues] = useState(initialValues);
  const [files, setFiles] = useState([]);
  let [isOpen, setIsOpen] = useState(true);
  const [skip, setSkip] = useState(false);

  function closeModal() {
    setIsOpen(false);
  }

  const [profileImage, setProfileImage] = useState({
    src: "/images/icon/camera.svg",
    height: 29,
    width: 29,
  });

  useEffect(() => {
    if (getUserEmail()) {
      let userData = getUserData();
      let newState = getInitialValues;
      newState.full_name = userData.full_name;
      newState.email = getUserEmail();
      newState.business_name = userData.business_name;
      newState.industry_id = userData.industry_id;
      newState.mobile_no = userData.mobile_number;
      newState.ABN = userData.abn;
      newState.TFN = userData.tfn;
      newState.address = userData.address ? userData.address.address : "";
      if (userData.profile_pic) {
        setProfileImage({
          src: userData.profile_pic,
          height: "100%",
          width: 100,
        });
      }
      setInitialValues(newState);
    }
  }, []);

  const handleFileChange = (event) => {
    setFiles(event.target.files);
    let imgSrc = URL.createObjectURL(event.target.files[0]);
    if (imgSrc) {
      setProfileImage({
        src: URL.createObjectURL(event.target.files[0]),
        height: "100%",
        width: 100,
      });
    }
  };

  const handleFormSubmit = async (values, setSubmitting) => {
    const formData = new FormData();
    formData.append("first_name", values.first_name);
    formData.append("last_name", values.last_name);
    formData.append("industry_id", values.industry_id);
    if (values.business_name != "") {
      formData.append("business_name", values.business_name);
    }
    if (values.mobile_no != "") {
      formData.append("mobile_no", values.mobile_no);
    }
    if (values.ABN != "") {
      formData.append("ABN", values.ABN);
    }
    if (typeof files[0] !== "undefined") {
      formData.append("profile_pic", files[0]);
    }

    if (values.google_address) {
      // Full address, format in specific fields
      if (typeof values.address !== "undefined" && values.address != "") {
        var fullAddress = values.address.toString().split(",");
        if (fullAddress.length > 1) {
          const stateSuburb = fullAddress[1].trim().split(" ");

          formData.append("address_line1", fullAddress[0]);
          formData.append("suburb", stateSuburb[0]);
          formData.append("city", stateSuburb[0]);
          formData.append("state", stateSuburb[stateSuburb.length - 1]);
          formData.append("country", "Australia");
        } else {
          formData.append("address_line1", fullAddress[0]);
          formData.append("suburb", "");
          formData.append("city", "");
          formData.append("state", "");
          formData.append("country", "Australia");
        }
      }
    } else {
      formData.append("address_line1", values.address);
      formData.append("suburb", "");
      formData.append("city", "");
      formData.append("state", "");
      formData.append("country", "");
    }

    const result = await updateUserProfile(formData);
    if (result.success) {
      localStorage.setItem("display-welcome-popup", true);
      setSubmitting(false);
      router.push({
        pathname: "/dashboard",
      });
    } else {
      setSubmitting(false);
    }
  };

  const handleSkip = () => {
    localStorage.setItem("display-welcome-popup", true);
    setSkip(true);
    router.push({
      pathname: "/dashboard",
    });
  };

  return (
    <>
      <Dialog
        open={isOpen}
        as="div"
        className="fixed inset-0 z-10 overflow-y-auto"
        onClose={closeModal}
      >
        <div className="min-h-screen px-4 text-center bg-mask">
          {/* This element is to trick the browser into centering the modal contents. */}
          <span
            className="inline-block h-screen align-middle"
            aria-hidden="true"
          >
            &#8203;
          </span>
          <div className="inline-block w-full max-w-md p-8 my-8 overflow-hidden text-left align-middle transition-all transform bg-white shadow-xl rounded-2xl">
            <Dialog.Title
              as="h2"
              className="text-lg font-medium leading-6 text-gray-900"
            >
              Welcome
            </Dialog.Title>
            <div className="mt-2">
              <p className="text-sm text-gray-500">
                Thank you for choosing Sole! We are looking forward to helping
                you manage your business with faster & easier accounting and
                payments. We need a few details to get you set up, it shouldn’t
                take more than a couple of minutes.
              </p>
            </div>

            <div className="mt-4">
              <button
                type="button"
                className="inline-flex justify-center px-4 py-2 text-sm font-medium text-blue-900 bg-blue-100 border border-transparent rounded-md hover:bg-blue-200 focus:outline-none focus-visible:ring-2 focus-visible:ring-offset-2 focus-visible:ring-blue-500"
                onClick={closeModal}
              >
                OK
              </button>
            </div>
          </div>
        </div>
      </Dialog>
      <Formik
        enableReinitialize
        initialValues={getInitialValues}
        validationSchema={AccountDetailsSchema}
        onSubmit={(values, { setSubmitting }) => {
          handleFormSubmit(values, setSubmitting);
        }}
      >
        {({
          values,
          errors,
          handleChange,
          handleSubmit,
          handleBlur,
          touched,
          isSubmitting,
          setFieldValue,
        }) => (
          <Form onSubmit={handleSubmit} className="py-16">
            <div className={`${Styles.accountDetails} ${classes.root}`}>
              <div
                className={`${clsx(Styles.verifyIcon, "text-center")} ${
                  classes.centerImg
                }`}
              >
                <img
                  src="/images/icon/create-account-blue.svg"
                  alt="Account"
                  width={76}
                  height={61}
                  className="mx-auto mb-3"
                />
              </div>

              <h4 className={Styles.topHead}>
                Your email address has been verified.<br></br>
                Please complete your account details.
              </h4>

              <div className="border border-gray-100 bg-gray-50 rounded-md mt-8 shadow-md">
                <div
                  className={clsx(
                    Styles.dFlex,
                    "align-items-baseline",
                    "mb-block",
                    " p-6"
                  )}
                >
                  <div className={Styles.userPhoto}>
                    <label>Upload your business logo</label>
                    <div className={Styles.upImg}>
                      <div className={Styles.file}>
                        <label htmlFor="profile_pic">
                          <div>
                            <input
                              id="profile_pic"
                              name="profile_pic"
                              type="file"
                              onChange={(event) => {
                                setFieldValue(
                                  "profile_pic",
                                  event.currentTarget.files[0]
                                );
                                handleFileChange(event);
                              }}
                              accept="'image/*"
                            />

                            <InformationCircleIcon
                              className="h-8 w-8 text-gray-400 ml-2 absolute -top-4 -right-4 bg-white rounded-full p-1"
                              data-for="profile-image"
                              data-tip
                            />
                            <ReactTooltip
                              id="profile-image"
                              className="custom-tooltip bg-gray-900"
                              textColor="#ffffff"
                              backgroundColor="#111827"
                              effect="solid"
                              aria-haspopup="true"
                            >
                              <p className="w-64">
                                Please upload your business logo here. Sole app
                                uses your logo on all invoices. The best size is
                                500 x 500 pixels.
                              </p>
                            </ReactTooltip>
                          </div>
                          <img
                            className={classes.maxHeight}
                            src={profileImage.src}
                            alt="Edit"
                            width={profileImage.width}
                            height={profileImage.height}
                          />
                        </label>
                      </div>
                    </div>
                  </div>

                  <div className={Styles.userDetail}>
                    <div
                      className={clsx(
                        Styles.dFlex,
                        "justify-content-between",
                        "mb-block"
                      )}
                    >
                      <div className={clsx(Styles.formGroup, "w50")}>
                        <TextField
                          name="first_name"
                          id="first_name"
                          label="First name*"
                          type="text"
                          variant="outlined"
                          className={classes.fControl}
                          value={values.first_name}
                          onChange={handleChange}
                          onBlur={handleBlur}
                        />
                        <ErrorMessage
                          name="first_name"
                          component="span"
                          className={Styles.error_text}
                        />
                      </div>

                      <div className={clsx(Styles.formGroup, "w50")}>
                        <TextField
                          name="last_name"
                          id="last_name"
                          label="Last name*"
                          type="text"
                          variant="outlined"
                          className={classes.fControl}
                          value={values.last_name}
                          onChange={handleChange}
                          onBlur={handleBlur}
                        />
                        <ErrorMessage
                          name="last_name"
                          component="span"
                          className={Styles.error_text}
                        />
                      </div>
                    </div>

                    <div
                      className={clsx(
                        Styles.dFlex,
                        "justify-content-between",
                        "mb-block"
                      )}
                    >
                      <div className={clsx(Styles.formGroup, "w50")}>
                        <TextField
                          required
                          disabled
                          label="Email Address"
                          name="email"
                          id="email"
                          type="email"
                          variant="outlined"
                          className={classes.fControl}
                          value={values.email}
                          inputProps={{ readOnly: true }}
                        />
                        <ErrorMessage
                          name="email"
                          component="span"
                          className={Styles.error_text}
                        />
                      </div>

                      <div className={clsx(Styles.formGroup, "w50")}>
                        <TextField
                          label="Mobile Number*"
                          name="mobile_no"
                          id="mobile_no"
                          placeholder="Enter 10 digit mobile number"
                          type="text"
                          variant="outlined"
                          className={classes.fControl}
                          value={values.mobile_no}
                          onChange={handleChange}
                          onBlur={handleBlur}
                        />
                        <ErrorMessage
                          name="mobile_no"
                          component="span"
                          className={Styles.error_text}
                        />
                      </div>
                    </div>

                    <div
                      className={clsx(
                        Styles.dFlex,
                        "justify-content-between",
                        Styles.formGroup
                      )}
                    >
                      <Field
                        name="address"
                        component={AddressAutoComplete}
                        type="N"
                      />
                    </div>

                    <div
                      className={clsx(Styles.dFlex, "justify-content-between")}
                    >
                      <div className={clsx(Styles.formGroup, "w100")}>
                        <TextField
                          name="business_name"
                          id="business_name"
                          label="Business name"
                          type="text"
                          variant="outlined"
                          className={classes.fControl}
                          value={values.business_name}
                          onChange={handleChange}
                          onBlur={handleBlur}
                        />
                      </div>
                    </div>

                    <div
                      className={clsx(Styles.dFlex, "justify-content-between")}
                    >
                      <div className={clsx(Styles.formGroup, "w100")}>
                        <TextField
                          label="ABN (if applicable)"
                          name="ABN"
                          id="ABN"
                          type="number"
                          variant="outlined"
                          className={classes.fControl}
                          value={values.ABN}
                          onChange={handleChange}
                          onBlur={handleBlur}
                        />
                        <ErrorMessage
                          name="ABN"
                          component="span"
                          className={Styles.error_text}
                        />
                      </div>
                    </div>

                    <div
                      className={clsx(Styles.dFlex, "justify-content-between")}
                    >
                      <div className={clsx(Styles.formGroup, "w100")}>
                        <FormControl
                          variant="outlined"
                          className={classes.formControl}
                        >
                          <InputLabel id="demo-simple-select-outlined-label">
                            Industry*
                          </InputLabel>
                          <Select
                            name="industry_id"
                            id="industry_id"
                            labelId="demo-simple-select-outlined-label"
                            label="Industry"
                            className={classes.fSelection}
                            value={values.industry_id}
                            onChange={handleChange}
                            onBlur={handleBlur}
                          >
                            <MenuItem value="none">
                              <em>Select Industry</em>
                            </MenuItem>
                            {industryOptions}
                          </Select>
                          <FormHelperText></FormHelperText>

                          <ErrorMessage
                            name="industry_id"
                            component="span"
                            className={Styles.error_text}
                          />
                        </FormControl>
                      </div>
                    </div>
                  </div>
                </div>

                <div
                  className={clsx(
                    Styles.formWizardButtons,
                    "text-center",
                    "space30",
                    "bg-white",
                    "pt-5",
                    "px-5",
                    "pb-2",
                    "rounded-md",
                    "align-items-center",
                    "dFlex",
                    "mb-block",
                    "justify-content-center"
                  )}
                >
                  <button
                    type="submit"
                    className={`${clsx(
                      Styles.roundBtn,
                      "blue-bg",
                      "mtop0",
                      " btn-next"
                    )} ${classes.btnMargin10}`}
                    disabled={isSubmitting}
                  >
                    <span>
                      {" "}
                      {isSubmitting ? "Please wait..." : "Continue"}{" "}
                    </span>
                  </button>

                  {/* <Link href="/dashboard">
                                        <a className='text-gray-500'>Skip</a>
                                    </Link> */}

                  <button
                    type="button"
                    className={`${clsx(Styles.roundBtn, "mtop0", " outline")} ${
                      classes.btnMargin10
                    }`}
                    onClick={handleSkip}
                  >
                    Skip&nbsp;&nbsp;
                    {skip && (
                      <ImpulseSpinner
                        frontColor={"blue"}
                        backColor="rgba(255,255,255, 0.5)"
                      />
                    )}
                  </button>
                </div>
              </div>
            </div>
          </Form>
        )}
      </Formik>

      <div className="max-w-4xl mx-auto grid md:grid-cols-2 gap-8 mt-0">
        <a
          href="https://soleapp.com.au/sole-bootcamp/"
          target="_blank"
          className="grad-box p-7 rounded-md"
        >
          <div className="flex  items-center">
            <div>
              <h3 className="text-white font-bold text-lg">SOLE BOOTCAMP</h3>
              <p className="text-brand-navyblue font-semibold">
                Need to get going fast? Do our 10 minute crash course
              </p>
            </div>

            <img
              src="/images/icon/bootcamp.svg"
              alt="Account"
              width={100}
              height={90}
              className="mx-auto mb-3"
            />
          </div>
        </a>

        <a
          href="https://soleapp.com.au/academy/"
          target="_blank"
          className="grad-box p-6 rounded-md"
        >
          <div className="flex  items-center">
            <div>
              <h3 className="text-white font-bold text-lg">SOLE ACADEMY</h3>
              <p className="text-brand-navyblue font-semibold">
                A library of articles and video content to help you grow
              </p>
            </div>

            <img
              src="/images/icon/academy.svg"
              alt="Account"
              width={100}
              height={90}
              className="mx-auto mb-3"
            />
          </div>
        </a>
      </div>
    </>
  );
}
