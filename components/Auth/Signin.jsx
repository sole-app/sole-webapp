import { useState, useEffect } from "react";
import clsx from "clsx";
import Link from "next/link";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Styles from "../../styles/Home.module.scss";
import { Form, Formik, ErrorMessage } from "formik";
import { login, forgotPassword } from "../../services/api/auth.services";
import Social from "./Social";
import { useRouter } from "next/router";
import SigninSchema from "../../schemas/signin.schema";
import ForgotPasswordSchema from "../../schemas/ForgotPassword.schema";
import AlertDialog from "../AlertDialog";

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

const useStyles = makeStyles((theme) => ({
  fControl: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
    width: "100%",
  },
}));

const initialValues = {
  email: "",
  password: "",
};

const initialFPValues = {
  email: "",
};

export default function Signin() {
  const classes = useStyles();
  const router = useRouter();
  const [loginSuccess, setLoginSuccess] = useState("");
  const [showLoginSuccess, setShowLoginSuccess] = useState(false);

  const [forgotPassword, setforgotPassword] = useState(false);
  const [logIn, setlogIn] = useState(true);
  const [openAlert, setOpenAlert] = useState(false);
  const [msg, setMsg] = useState("");

  useEffect(() => {
    let query = window.location.search.substring(1);
    let vars = query.split("&");
    for (var i = 0; i < vars.length; i++) {
      var pair = vars[i].split("=");
      if (
        typeof pair[0] !== "undefined" &&
        pair[0] == "email" &&
        typeof pair[1] !== "undefined" &&
        pair[1] !== ""
      ) {
        initialValues.email = pair[1];
      }
    }
  }, []);

  const toggleforgot = () => {
    setforgotPassword(true);
    setlogIn(!logIn);
  };

  const handleFormSubmit = async (values, setSubmitting) => {
    // HIDE for dashboard-ui branch

    const result = await login({
      email: values.email,
      password: values.password,
      signup_source: 1,
    });
    if (result.success) {
      setSubmitting(false);
      setShowLoginSuccess(true);
      router.push("dashboard");
    } else {
      setSubmitting(false);
      setShowLoginSuccess(true);
      setMsg(result.message);
      setOpenAlert(true);
    }
  };

  const handleFPFormSubmit = async (values, setSubmitting, resetForm) => {
    // HIDE for dashboard-ui branch
    const result = await forgotPassword({
      email: values.email,
    });
    if (result.success) {
      initialFPValues.email = "";
    } else {
      setMsg(result.message);
      setOpenAlert(true);
    }
    setSubmitting(false);
    resetForm({ values: "" });
  };

  return (
    <>
      <AlertDialog
        id="ringtone-menu2"
        message={msg}
        keepMounted
        open={openAlert}
        onClose={setOpenAlert}
      />
      <div className="h-full block">
        <div className={Styles.createAccount}>
          <div className={Styles.topCl}>
            <h2 className={Styles.smTit}>
              {logIn ? "Sign in with your Sole account" : "Forgot Password"}
            </h2>
            <div className={Styles.signupOpt}>
              {logIn ? (
                <>
                  <Formik
                    initialValues={initialValues}
                    validationSchema={SigninSchema}
                    onSubmit={(values, { setSubmitting }) => {
                      handleFormSubmit(values, setSubmitting);
                    }}
                  >
                    {({
                      values,
                      errors,
                      handleChange,
                      handleSubmit,
                      handleBlur,
                      touched,
                      isSubmitting,
                    }) => (
                      <Form onSubmit={handleSubmit}>
                        <div
                          className={clsx(
                            Styles.dFlex,
                            "justify-content-between",
                            "mb-block"
                          )}
                        >
                          <div className={clsx(Styles.formGroup, "w100")}>
                            <TextField
                              name="email"
                              id="email"
                              label="Email Address*"
                              type="email"
                              variant="outlined"
                              className={classes.fControl}
                              value={values.email}
                              onChange={handleChange}
                              onBlur={handleBlur}
                            />
                            <ErrorMessage
                              name="email"
                              component="span"
                              className="text-red-700"
                            />
                          </div>
                          <div className={clsx(Styles.formGroup, "w100")}>
                            <TextField
                              name="password"
                              id="password"
                              type="password"
                              label="Password*"
                              variant="outlined"
                              className={classes.fControl}
                              value={values.password}
                              onChange={handleChange}
                              onBlur={handleBlur}
                            />
                            <ErrorMessage
                              name="password"
                              component="span"
                              className="text-red-700"
                            />
                          </div>
                          <div className="text-right -mt-2">
                            <span
                              className="text-xs cursor-pointer hover:text-brand-blue"
                              onClick={toggleforgot}
                            >
                              Forgot Password?
                            </span>
                          </div>

                          <div className={clsx(Styles.formGroup, "w100")}>
                            <button
                              type="submit"
                              className={clsx(
                                Styles.roundBtn,
                                "blue-bg",
                                " btn-next"
                              )}
                              disabled={isSubmitting}
                            >
                              <span>
                                {" "}
                                {isSubmitting ? "Please wait..." : "Login"}{" "}
                              </span>
                            </button>
                          </div>
                        </div>
                      </Form>
                    )}
                  </Formik>
                </>
              ) : (
                <>
                  <Formik
                    initialValues={initialFPValues}
                    validationSchema={ForgotPasswordSchema}
                    onSubmit={(values, { setSubmitting, resetForm }) => {
                      handleFPFormSubmit(values, setSubmitting, resetForm);
                    }}
                  >
                    {({
                      values,
                      errors,
                      handleChange,
                      handleSubmit,
                      handleBlur,
                      touched,
                      isSubmitting,
                    }) => (
                      <Form onSubmit={handleSubmit}>
                        <div
                          className={clsx(
                            Styles.dFlex,
                            "justify-content-between",
                            "mb-block"
                          )}
                        >
                          <div className={clsx(Styles.formGroup, "w100")}>
                            <TextField
                              name="email"
                              id="email"
                              label="Email Address*"
                              type="email"
                              variant="outlined"
                              className={classes.fControl}
                              value={values.email}
                              onChange={handleChange}
                              onBlur={handleBlur}
                            />
                            <ErrorMessage
                              name="email"
                              component="span"
                              className="text-red-700"
                            />
                          </div>

                          <div className={clsx(Styles.formGroup, "w100")}>
                            <button
                              type="submit"
                              className={clsx(
                                Styles.roundBtn,
                                "blue-bg",
                                " btn-next"
                              )}
                              disabled={isSubmitting}
                            >
                              <span>
                                {" "}
                                {isSubmitting ? "Please wait..." : "Send"}{" "}
                              </span>
                            </button>
                          </div>
                        </div>
                      </Form>
                    )}
                  </Formik>
                </>
              )}

              <p className={Styles.or}>Or via social media</p>
              <div className={clsx(Styles.altOption)}>
                <Social type="login" />
              </div>
            </div>
            <p className={Styles.note}>
              By signing in, you agree to Sole's
              <a target="_blank" href="https://www.soleapp.com.au/privacy/">
                {" "}
                Privacy Policy{" "}
              </a>{" "}
              and&nbsp;
              <a target="_blank" href="https://www.soleapp.com.au/terms/">
                Terms and Conditions
              </a>
            </p>
            {showLoginSuccess ? (
              <div id="loginMsg" className={clsx("alert-success")}>
                <p>{loginSuccess}</p>
                <div
                  className={clsx(
                    "flex items-center justify-center mt-6 space-x-3 text-center"
                  )}
                >
                  <a href="https://play.google.com/store/apps/details?id=com.au.app.soleapp">
                    <img
                      src="https://sole-webapp-demo.netlify.app/images/icon/small-icon-google-play.png"
                      alt="Google Play Store"
                      width="38"
                      height="38"
                    />
                  </a>

                  <a href="https://apps.apple.com/au/app/sole-accounting-made-easy/id1453135227">
                    <img
                      src="https://sole-webapp-demo.netlify.app/images/icon/small-icon-app-store.svg"
                      alt="iOS Store"
                      width="40"
                      height="40"
                    />
                  </a>
                </div>
              </div>
            ) : (
              ""
            )}
          </div>
        </div>

        <p className={Styles.alreadyAcnt}>
          Don't have an account yet?
          <Link href="/signup">
            <a> Sign Up</a>
          </Link>
        </p>
      </div>
    </>
  );
}
