import React from "react";
import SocialLogin from "react-social-logins";
import clsx from "clsx";
import Styles from "../../styles/Home.module.scss";

class SocialButton extends React.Component {
  render() {
    const { children, triggerLogin, ...props } = this.props;
    return (
      <button
        onClick={triggerLogin} {...props}
        className={clsx(Styles.roundBtn, "grey-bg")}
      >
        {children}
      </button>
    );
  }
}

export default SocialLogin(SocialButton);