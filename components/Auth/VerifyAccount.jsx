import clsx from "clsx";
import { Form, Formik } from "formik";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Styles from "../../styles/Register.module.scss";
import {
  resendVerificationCode,
  accountActivate,
} from "../../services/api/auth.services";
import { useEffect, useState } from "react";
import { getAuthToken } from "../../services/cookies.services";
import { ImpulseSpinner } from "react-spinners-kit";

const useStyles = makeStyles((theme) => ({
  numControl: {
    marginTop: theme.spacing(2),
    width: "42px",
    borderColor: "#191944 !important",
    borderTop: "none",
    marginLeft: "7px",
    marginRight: "7px",
  },
  root: {
    padding: "1em",
    paddingTop: "2em",
    boxSizing: "border-box",
  },
  centerImg: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
}));

export default function VerifyAccount(props) {
  const classes = useStyles();
  const [verifyAccount, setVerifyAccount] = useState(false);
  const [resendOtp, setResendOtp] = useState(false);

  useEffect(() => {
    if (getAuthToken()) {
      // props.next();
    }
  }, []);

  const handleResend = async () => {
    // props.progress(true);
    setResendOtp(true);
    const result = await resendVerificationCode(props.email);
    // props.progress(false);
    if (result.success) {
      setResendOtp(false);
      //TODO : need to change with Snackbars component's
      alert(result.message);
    } else {
      setResendOtp(false);
      //TODO : need to change with Snackbars component's
      alert(result.message);
    }
    setResendOtp(false);
  };

  const handleFormSubmit = async (values, formikHelpers) => {
    setVerifyAccount(true);
    // props.progress(true);
    let token =
      values.n1 + values.n2 + values.n3 + values.n4 + values.n5 + values.n6;
    const result = await accountActivate(
      {
        token: token,
      },
      props.email
    );
    // props.progress(false);
    if (result.success) {
      // props.next();
      props.setVerifyOtpStatus(true);
      props.setWelcomeFullScreen(true);
    }
    setVerifyAccount(false);
  };

  const changeFocus = (e) => {
    const { maxLength, value, name } = e.target;
    const [fieldName, fieldIndex] = name.split("n");
    // Check if they hit the max character length
    if (value.length >= maxLength) {
      // Check if it's not the last input field
      if (parseInt(fieldIndex, 10) < 7) {
        // Get the next input field
        const nextSibling = document.querySelector(
          `input[name=n${parseInt(fieldIndex, 10) + 1}]`
        );
        // If found, focus the next field
        if (nextSibling !== null) {
          nextSibling.focus();
          nextSibling.select();
        }
      }
    }
  };

  return (
    <Formik
      initialValues={{
        n1: "",
        n2: "",
        n3: "",
        n4: "",
        n5: "",
        n6: "",
      }}
      onSubmit={handleFormSubmit}
      handleChange={(e) => {
        ha;
      }}
      /*validationSchema={CreateAccountSchema}*/
    >
      {({ values, handleChange, handleBlur, errors, touched }) => (
        <Form className="h-screen items-center flex">
          <div className={`${Styles.verifyAccount} ${classes.root}`}>
            <div
              className={`${clsx(Styles.verifyIcon, "text-center")} ${
                classes.centerImg
              }`}
            >
              <img
                src="/images/icon/mobile.svg"
                alt="Mobile"
                width={96}
                height={81}
              />
            </div>

            <h4 className={clsx(Styles.topHead, "space30")}>
              A verification code has been sent<br></br> via text and email
            </h4>

            <p className={Styles.guide}>
              Please enter the 6 digit verification code we sent<br></br> to
              verify your account and continue the registration process.
            </p>

            <div
              className={clsx(
                Styles.addCode,
                "justify-content-center",
                "dFlex"
              )}
            >
              <TextField
                name="n1"
                id="n1"
                required
                className={classes.numControl}
                onChange={(e) => {
                  handleChange(e);
                  changeFocus(e);
                }}
                onBlur={handleBlur}
                inputProps={{ maxLength: 1 }}
              />
              <TextField
                name="n2"
                id="n2"
                required
                className={classes.numControl}
                onChange={(e) => {
                  handleChange(e);
                  changeFocus(e);
                }}
                onBlur={handleBlur}
                inputProps={{ maxLength: 1 }}
              />
              <TextField
                name="n3"
                id="n3"
                required
                className={classes.numControl}
                onChange={(e) => {
                  handleChange(e);
                  changeFocus(e);
                }}
                onBlur={handleBlur}
                inputProps={{ maxLength: 1 }}
              />
              <TextField
                name="n4"
                id="n4"
                required
                className={classes.numControl}
                onChange={(e) => {
                  handleChange(e);
                  changeFocus(e);
                }}
                onBlur={handleBlur}
                inputProps={{ maxLength: 1 }}
              />
              <TextField
                name="n5"
                id="n5"
                required
                className={classes.numControl}
                onChange={(e) => {
                  handleChange(e);
                  changeFocus(e);
                }}
                onBlur={handleBlur}
                inputProps={{ maxLength: 1 }}
              />
              <TextField
                name="n6"
                id="n6"
                required
                className={classes.numControl}
                onChange={(e) => {
                  handleChange(e);
                  changeFocus(e);
                }}
                onBlur={handleBlur}
                inputProps={{ maxLength: 1 }}
              />
            </div>

            <div
              className={clsx(
                Styles.formWizardButtons,
                "align-items-center",
                "dFlex",
                "mb-block",
                "justify-content-center"
              )}
            >
              <button
                type="button"
                className={clsx(Styles.roundBtn, "mtop0", " outline")}
                onClick={handleResend}
              >
                Resend&nbsp;&nbsp;
                {resendOtp && (
                  <ImpulseSpinner
                    frontColor={"white"}
                    backColor="rgba(255,255,255, 0.5)"
                  />
                )}
              </button>

              <button
                type="submit"
                className={clsx(
                  Styles.roundBtn,
                  "blue-bg",
                  "mtop0",
                  " btn-next"
                )}
              >
                Continue&nbsp;&nbsp;
                {verifyAccount && (
                  <ImpulseSpinner
                    frontColor={"white"}
                    backColor="rgba(255,255,255, 0.5)"
                  />
                )}
              </button>
            </div>
          </div>
        </Form>
      )}
    </Formik>
  );
}
