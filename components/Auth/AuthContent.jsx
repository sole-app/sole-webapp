import { useState } from "react";
import { createStyles, makeStyles } from '@material-ui/core';
import Head from 'next/head';
import Link from 'next/link';
import { BrowserView, MobileView } from 'react-device-detect';
import clsx from 'clsx';
import Styles from '../../styles/Home.module.scss';
import Snackbar, { SNACKBAR_TYPE } from "../../components/Layout/Snackbar";
import Signup from '../../components/Auth/Signup';
import Signin from '../../components/Auth/Signin';

export default function AuthContent(props) {
    const { type } = props;
    const [welcomeFullScreen, setWelcomeFullScreen] = useState(false);
    const innerClasses = useStyles();
    return (
        <div>
            <Head>
                <title>Sole App | Accounting made easy</title>
                <link rel='icon' href='/favicon.ico' />
            </Head>
            <BrowserView>
                <Snackbar type={SNACKBAR_TYPE}></Snackbar>
                <main className={Styles.main}>
                    <section className={Styles.welcome}>
                        <div className={Styles.fullWrap}>
                            {!welcomeFullScreen && (<div className={Styles.leftAside}>
                                <div className={Styles.bgPoster}>
                                    <div className={Styles.intro}>
                                        <div className={Styles.shapeLogo}>
                                            <img
                                                src="/images/logo/white-logo.png"
                                                className={Styles.logo}
                                                alt="logo"
                                                width={156}
                                                height={43}
                                            />
                                        </div>
                                        <h1 className={Styles.mainTit}>
                                            <span className={Styles.dBlock}>
                                                You didn't think<br></br> you needed anyone.
                                            </span>
                                            <span className={Styles.blueTxt}>Until you met Sole.</span>
                                        </h1>

                                        <div className={Styles.quickCard}>
                                            <h3 className={Styles.blueTxt}>Sole - Accounting &<br></br> Invoicing made easy</h3>

                                            <h6 className={Styles.planHead}>All the features you need:</h6>
                                            <ul className={Styles.planPoint}>
                                                <li>Unlimited professional quotes & invoices within seconds and on-the-go.</li>
                                                <li>Never lose a receipt again with seamless expense management.</li>
                                                <li>Track your business performance at a glance and in real time.</li>
                                                <li>No more tax surprises - match directly out of your bank account & track your tax estimate live.</li>
                                                <li>Enjoy special partner offers from Sole's extended business network.</li>
                                            </ul>
                                        </div>
                                    </div>

                                    <div className={Styles.introBanner}>
                                        <img src="/images/Signup-banner.png" alt="Sign up banner" width="100%" height="100%" />
                                    </div>
                                </div>
                            </div>)}

                            <div className={welcomeFullScreen ? `${Styles.rightAside} ${innerClasses.fullScreen}` : Styles.rightAside}>

                                <div className={clsx(Styles.logo, "text-center", "pad-30", "visible-mb")}>
                                    <a href="#">
                                        <img
                                            src="/images/text-logo.png"
                                            alt="Logo"
                                            width={80}
                                            height={34} />
                                    </a>
                                </div>

                                {(type === 'signin') &&
                                    <Signin setWelcomeFullScreen={setWelcomeFullScreen} />
                                }

                                {(type === 'signup') &&
                                    <Signup setWelcomeFullScreen={setWelcomeFullScreen} />
                                }

                            </div>
                        </div>
                    </section>
                </main>
            </BrowserView>

            <MobileView>
                <section className={Styles.mobileView}>
                    <div className={Styles.bgPoster}>
                        <div className={Styles.shapeLogo}>
                            <img
                                src="/images/logo/white-logo.png"
                                className={Styles.logo}
                                alt="logo"
                                width={156}
                                height={43}
                            />
                        </div>
                        <div className={Styles.intro}>
                            <h1 className={Styles.mainTit}>
                                <span className={Styles.dBlock}>Sole is best viewed on the <span className={Styles.blueTxt}>mobile app</span> or desktop version.</span>
                            </h1>

                            <h5 className={Styles.smTit}> Download Now</h5>

                            <div className={Styles.storeBtn}>
                                <div className="flex justify-between mx-5 mt-10">
                                    <Link href="#">
                                        <a className="mr-5">
                                            <img
                                                src="https://sole-webapp-demo.netlify.app/images/icon/google-play-badge-dark.svg"
                                                alt="Google Play Store"
                                                width="135"
                                                height="40"
                                            />
                                        </a>
                                    </Link>

                                    <Link href="#">
                                        <a className="ml-5">
                                            <img
                                                src="/images/icon/ios-app.png"
                                                alt="iOS Store"
                                                width="140"
                                                height="40"
                                            />
                                        </a>
                                    </Link>
                                </div>
                            </div>
                        </div>

                        <div className={Styles.footer}>
                            <Link href="https://soleapp.com.au/academy/">
                                <a target="_blank" className="text-white">Sole App Knowledge Base</a>
                            </Link>
                            <p>&copy; Sole 2022 All Rights Reserved</p>
                        </div>
                    </div>
                </section>
            </MobileView>
        </div>
    );
}
const useStyles = makeStyles(() =>
    createStyles({
        fullScreen: {
            width: '100% !important'
        }
    }),
);
