import React from "react";
import { useState, useEffect } from "react";
import clsx from "clsx";
import Link from "next/link";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Styles from "../../styles/Home.module.scss";
import { Form, Formik, ErrorMessage } from "formik";
import CreateAccountSchema from "../../schemas/CreateAccount.schema";
import { signUp } from "../../services/api/auth.services";
import ReCAPTCHA from "react-google-recaptcha";
import VerifyAccount from "../Auth/VerifyAccount";
import AccountDetails from "../Auth/AccountDetails";
import { ImpulseSpinner } from "react-spinners-kit";
import AlertDialog from "../AlertDialog";
import Social from "./Social";
import { getDeviceId } from "../../services/cookies.services";
import { registerDevice } from "../../services/api/device.services";

const useStyles = makeStyles((theme) => ({
  fControl: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
    width: "100%",
  },
}));

const initialValues = {
  email: "",
  mobile_no: "",
  password: "",
  confirm_password: "",
};

export default function Signup({ setWelcomeFullScreen }) {
  const classes = useStyles();
  const recaptchaRef = React.createRef();
  const [signupStatus, setSignupStatus] = useState(false);
  const [verifyOtpStatus, setVerifyOtpStatus] = useState(false);
  const [newEmail, setNewEmail] = useState(null);
  const [creatingAccount, setCreatingAccount] = useState(false);
  const [openAlert, setOpenAlert] = useState(false);
  const [msg, setMsg] = useState("");
  const [loader, setLoader] = useState(true);

  useEffect(() => {
    let query = window.location.search.substring(1);
    let vars = query.split("&");
    for (var i = 0; i < vars.length; i++) {
      var pair = vars[i].split("=");
      if (
        typeof pair[0] !== "undefined" &&
        pair[0] == "email" &&
        typeof pair[1] !== "undefined" &&
        pair[1] !== ""
      ) {
        initialValues.email = pair[1];
      }
    }
    setLoader(false);
  }, []);

  const handleFormSubmit = async (values, setSubmitting) => {
    recaptchaRef.current.reset();
    // Execute the reCAPTCHA when the form is submitted
    const captcha = await recaptchaRef.current.executeAsync();
    setCreatingAccount(true);
    setNewEmail(values.email);

    let device_id = getDeviceId() ? getDeviceId() : null;
    if (device_id === null) {
      await registerDevice();
    }
    device_id = getDeviceId() ? getDeviceId() : null;

    const result = await signUp({
      email: values.email,
      signup_source: 1,
      device_id: device_id,
      password: values.password,
      confirm_password: values.confirm_password,
      country_code: "+61",
      mobile_no: values.mobile_no,
      captcha: captcha,
    });

    if (result.success) {
      setSubmitting(false);
      setSignupStatus(true);
      setCreatingAccount(false);
    } else {
      setCreatingAccount(false);
      setSubmitting(false);
      setMsg(result.message);
      setOpenAlert(true);
    }
  };

  if (loader) {
    return null;
  }

  if (signupStatus && !verifyOtpStatus) {
    return (
      <VerifyAccount
        email={newEmail}
        setVerifyOtpStatus={setVerifyOtpStatus}
        setWelcomeFullScreen={setWelcomeFullScreen}
      />
    );
  } else if (signupStatus && verifyOtpStatus) {
    return <AccountDetails />;
  } else {
    return (
      <>
        <AlertDialog
          id="ringtone-menu2"
          message={msg}
          keepMounted
          open={openAlert}
          onClose={setOpenAlert}
        />

        <div className={Styles.h100}>
          <div className={Styles.createAccount}>
            <div className={Styles.topCl}>
              <h2 className={Styles.smTit}>
                Sign up for free in under a minute
              </h2>
              <div className={Styles.signupOpt}>
                <Formik
                  initialValues={initialValues}
                  validationSchema={CreateAccountSchema}
                  onSubmit={(values, { setSubmitting }) => {
                    handleFormSubmit(values, setSubmitting);
                  }}
                >
                  {({
                    values,
                    errors,
                    handleChange,
                    handleSubmit,
                    handleBlur,
                    touched,
                    isSubmitting,
                  }) => (
                    <Form onSubmit={handleSubmit}>
                      <ReCAPTCHA
                        ref={recaptchaRef}
                        size="invisible"
                        sitekey="6LdIQZMbAAAAAO9CxTWaFeWpPwBK2UAkB1kq5Obn"
                      />
                      <div
                        className={clsx(
                          Styles.dFlex,
                          "justify-content-between",
                          " mb-block"
                        )}
                      >
                        <div className={clsx(Styles.formGroup, "w100")}>
                          <TextField
                            name="email"
                            id="email"
                            label="Email Address*"
                            type="email"
                            variant="outlined"
                            className={classes.fControl}
                            value={values.email}
                            onChange={handleChange}
                            onBlur={handleBlur}
                          />
                          <ErrorMessage
                            name="email"
                            component="span"
                            className={Styles.error_text}
                          />
                        </div>

                        <div className={clsx(Styles.formGroup, "w100")}>
                          <TextField
                            name="Mobile Number"
                            id="mnumber"
                            label="Mobile Number*"
                            type="text"
                            variant="outlined"
                            className={classes.fControl}
                            onChange={(e) => {
                              values.mobile_no = e.target.value;
                            }}
                            onBlur={handleBlur}
                          />
                          <ErrorMessage
                            name="mobile_no"
                            component="span"
                            className={Styles.error_text}
                          />
                        </div>

                        <div
                          className={clsx(
                            Styles.dFlex,
                            "justify-content-between",
                            "mb-block"
                          )}
                        >
                          <div className={clsx(Styles.formGroup, "w100")}>
                            <TextField
                              name="password"
                              id="password"
                              type="password"
                              label="Password*"
                              variant="outlined"
                              className={classes.fControl}
                              value={values.password}
                              onChange={handleChange}
                              onBlur={handleBlur}
                            />
                            <ErrorMessage
                              name="password"
                              component="span"
                              className={Styles.error_text}
                            />
                          </div>

                          <div className={clsx(Styles.formGroup, "w100")}>
                            <TextField
                              name="confirm_password"
                              id="confirm_password"
                              label="Confirm Password*"
                              type="password"
                              variant="outlined"
                              className={classes.fControl}
                              value={values.confirm_password}
                              onChange={handleChange}
                              onBlur={handleBlur}
                            />
                            <ErrorMessage
                              name="confirm_password"
                              component="span"
                              className={Styles.error_text}
                            />
                          </div>
                        </div>

                        <div
                          className={clsx(
                            Styles.dFlex,
                            "justify-content-between text-xs"
                          )}
                        >
                          <div className={Styles.pwdRequire}>
                            <h6>Password Criteria:</h6>
                            <ul>
                              <li>
                                - Must contain at least 8 characters and one
                                number
                              </li>
                              <li>
                                - One uppercase and one special case character
                              </li>
                            </ul>
                          </div>
                        </div>

                        <div className={clsx(Styles.formGroup, "w100 mt-10")}>
                          <button
                            type="submit"
                            className={clsx(
                              Styles.roundBtn,
                              "blue-bg",
                              " btn-next"
                            )}
                          >
                            {creatingAccount ? (
                              <ImpulseSpinner
                                frontColor={"white"}
                                backColor="rgba(255,255,255, 0.5)"
                              />
                            ) : (
                              <span> Create Account</span>
                            )}
                          </button>
                        </div>
                      </div>
                    </Form>
                  )}
                </Formik>
              </div>

              <p className={clsx(Styles.note, "text-center")}>
                By creating an account, I agree with Sole’s
                <a target="_blank" href="https://www.soleapp.com.au/privacy/">
                  {" "}
                  Privacy Policy{" "}
                </a>{" "}
                and&nbsp;
                <a target="_blank" href="https://www.soleapp.com.au/terms/">
                  Terms and Conditions
                </a>
              </p>

              <div className={clsx(Styles.altOption)}>
                <Social type="login" />
              </div>

              <div className={clsx(Styles.alreadyAcnt, "mbtm0", "space40")}>
                <p>
                  Already have an account?
                  <Link href="/">
                    <a> Log In</a>
                  </Link>
                </p>
                <div
                  className={clsx(
                    "flex justify-content-center space10 text-center"
                  )}
                >
                  <a
                    href="https://play.google.com/store/apps/details?id=com.au.app.soleapp"
                    className={clsx("m-right10")}
                  >
                    <img
                      src="https://sole-webapp-demo.netlify.app/images/icon/small-icon-google-play.png"
                      alt="Google Play Store"
                      width="38"
                      height="38"
                    />
                  </a>

                  <a href="https://apps.apple.com/au/app/sole-accounting-made-easy/id1453135227">
                    <img
                      src="https://sole-webapp-demo.netlify.app/images/icon/small-icon-app-store.svg"
                      alt="iOS Store"
                      width="40"
                      height="40"
                    />
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}
