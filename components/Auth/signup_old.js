import clsx from 'clsx';
import Link from 'next/link';
import Styles from '../../styles/Home.module.scss';
import Social from './social';

export default function Signup() {

    const responseGoogle = (response) => {
        console.log(response);
    }
    
    const responseFacebook = (response) => {
        console.log('response');
        console.log(response);
    }

    return (
        <div className={Styles.h100}>
            <div className={Styles.createAccount}>
                <div className={Styles.topCl}>
                    <h2 className={Styles.smTit}>Create a new account</h2>

                    <div className={Styles.signupOpt}>
                          <Link href="/register">
                                <a  className={clsx(Styles.roundBtn, "blue-bg")}>
                                    <span>  Sign up with your email address</span>
                                </a>
                            </Link>

                        <p className={Styles.or}>OR</p>
                        <div className={clsx(Styles.altOption)}>
                            <Social type="register"/>
                        </div>    
                        <p className={Styles.note}>By signing up, you agree to Sole's <a href="https://soleapp.com.au/privacy"> Privacy Policy </a> and  <a href="https://soleapp.com.au/terms">Terms and Conditions</a></p>
                        <br></br>
                        
                    </div>
                    <p className={Styles.alreadyAcnt}>
                        <br></br>
                        <Link href="/login"> 
                            <a><span className={clsx("txt-bold")}>Already have an account?</span></a>
                        </Link> <br></br><br></br>Please download SOLE APP on your mobile to login. Webapp is coming soon!   
                        <Link href="/dashboard"> 
                            <a> Dashboardd</a>
                        </Link>
                        <br></br><br></br>
                        <div className={clsx("flex, justify-content-between, text-center")}>
                            <a href="https://play.google.com/store/apps/details?id=com.au.app.soleapp" className={clsx("m-right10")}>
                                <img src="https://sole-webapp-demo.netlify.app/images/icon/small-icon-google-play.png" alt="Google Play Store" width="38"  height="38" />
                            </a>

                            <a href="https://apps.apple.com/au/app/sole-accounting-made-easy/id1453135227">
                                <img src="https://sole-webapp-demo.netlify.app/images/icon/small-icon-app-store.svg" alt="iOS Store" width="40" height="40" />
                            </a>
                        </div>                 
                    </p>                    
                </div> 
            </div>              
                
            <p className={Styles.alreadyAcnt}>
                <br></br>
                {/* <Link href="/login"> 
                    <a> Sign In</a>
                </Link> */}
                {/* <Link href="/dashboard"> 
                    <a> Dashboard</a>
                </Link> */}
            </p>
        </div>
   );
}
