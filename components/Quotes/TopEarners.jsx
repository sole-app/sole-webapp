import { useState, useEffect } from "react";
import Styles from "../../styles/Invoices.module.scss";
import { Doughnut, Bar } from "react-chartjs-2";
import { TopEarners as TopEarnersData } from "../../services/api/quote.services";
import { getDateFromFYDates } from "../../helpers/common";
import YearMonthFilter from "../Common/YearMonthFilter";

const options = {
  maintainAspectRatio: false,
  plugins: {
    title: {
      display: true,
      text: "",
    },

    legend: {
      display: true,
      position: "right",
      align: "center",
      margin: 20,
      onClick: null,
      borderRadius: 15,

      labels: {
        // This more specific font property overrides the global property
        font: {
          size: 13,
          weight: "bold",
          lineHeight: 2,
          padding: 20,
          family: "'Inter', sans-serif",
        },
        boxWidth: 20,
        boxHeight: 20,
        borderRadius: 15,
        padding: 20,
      },
    },

    tooltip: {
      callbacks: {
        label: function (context) {
          let label = context.label || "";

          if (label) {
            label += ": $";
          }
          if (context.formattedValue !== null) {
            label += context.formattedValue;
          }
          return label;
        },
      },
    },
  },
};

export default function TopEarners(props) {
  const [type, setType] = useState("YEAR"); //YEAR, MONTH
  const [loader, setLoader] = useState(true);
  const [hasData, setHasData] = useState(false);
  const [donutData, setDonutData] = useState({
    datasets: [
      {
        data: [0, 0, 0],
        backgroundColor: ["#d1d5db", "#4C4DFE", "#2ef8ff"],
        hoverBackgroundColor: ["#d1d5db", "#4C4DFE", "#2ef8ff"],
      },
    ],
  });

  const getData = async (filterType) => {
    let apiRunning = true;
    setLoader(apiRunning);
    setType(filterType);
    setHasData(false);

    let formData = "";
    let dates = getDateFromFYDates(filterType);
    formData += "?start_date=" + dates.start_date;
    formData += "&end_date=" + dates.end_date;

    const res = await TopEarnersData(formData);
    if (res.success) {
      let ddata = donutData;
      if (
        typeof res.data.top_three_clients !== "undefined" &&
        res.data.top_three_clients.length > 0
      ) {
        let data = [];
        let labels = [];
        let clients = res.data.top_three_clients;
        for (let i = 0; i < clients.length; i++) {
          data[i] = clients[i].total;
          labels[i] = clients[i].client_name;
        }
        ddata.datasets[0].data = data;
        ddata.labels = labels;
        setDonutData(ddata);
        setHasData(true);
      }
      apiRunning = false;
    }
    if (!apiRunning) {
      setLoader(false);
    }
  };

  useEffect(() => {
    getData(type);
  }, [props.refreshSummary]);

  return (
    <div className="mt-8 w-full xl:w-1/2">
      <div className="grid grid-cols-1 h-full">
        {/* Top Earners */}
        <div className="bg-white overflow-hidden shadow rounded-lg">
          <div className="mx-auto border-b border-grey-400 p-3 sm:p-4  xl:p-5  lg:flex lg:items-center lg:justify-between">
            <h3 className="text-lg font-medium tracking-wide text-gray-900 sm:text-xl">
              Potential Top Earners
            </h3>

            <div className="mt-8 flex lg:mt-0 lg:flex-shrink-0">
              <YearMonthFilter type={type} setType={getData} />
            </div>
          </div>

          {/* Chart */}
          <div className={Styles.earnerChart}>
            <div className="flex">
              <div className="w-full">
                {!loader && (
                  <>
                    {hasData ? (
                      <Doughnut
                        data={donutData}
                        options={options}
                        width={400}
                        height={250}
                      />
                    ) : (
                      <div className="block justify-between mt-16  flex-1 2xl:flex text-center">
                        No quotes found for this{" "}
                        {type === "YEAR" ? "year" : "month"}
                      </div>
                    )}
                  </>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
