import { Fragment, useState, useEffect } from "react";
import { Dialog, Transition, Switch } from "@headlessui/react";
import {
  XIcon,
  XCircleIcon,
  PlusSmIcon,
  InformationCircleIcon,
} from "@heroicons/react/outline";
import { ErrorMessage, Formik, FieldArray } from "formik";
import QuoteSchema from "../../schemas/Quote.schema";
import { makeStyles } from "@material-ui/core/styles";
import { Info, Update, Add } from "../../services/api/quote.services";
import {
  Add as AddClientData,
  List as ListClient,
} from "../../services/api/client.services";
import { getUserData } from "../../services/cookies.services";
import CircularProgress from "@material-ui/core/CircularProgress";
import NumberFormat from "react-number-format";
import Stack from "@mui/material/Stack";
import Autocomplete, { createFilterOptions } from "@mui/material/Autocomplete";
import { preview } from "../../services/api/preview.services";
import TextField from "@mui/material/TextField";
import DateTimePicker from "../DateTimePicker";
import EmailDialog from "../../components/EmailDialog";
import Skeleton from "react-loading-skeleton";
import ReactTooltip from "react-tooltip";
import { FiPlusCircle } from "react-icons/fi";
import { IconContext } from "react-icons";
import AlertDialog from "../AlertDialog";

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

const useStyles = makeStyles((theme) => ({
  fControl: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
    width: "100%",
  },
  dollarPrice: {
    paddingLeft: "0.60rem",
  },
}));

let initialValues = {
  client: null,
  client_name: "",
  client_id: "",
  description: "",
  notes: "",
  items: [
    {
      description: "",
      qty: "",
      disc: "",
      price: "",
    },
  ],
  quoteExpDate: "",
  quoteDate: "",
};

const gst_percentage = 10;

export default function AddQuote(props) {
  const { setRefreshSummary, refreshSummary } = props;
  const user = getUserData();
  const [gstEnabled, setGstEnabled] = useState(true);
  const [clientList, setClientList] = useState([]);
  const [totAmount, setTotAmount] = useState(0);
  const [subtotAmount, setSubtotAmount] = useState(0);
  const [discount, setDiscount] = useState(0);
  const [gstAmount, setGstAmount] = useState(0);
  const [loader, setLoader] = useState(true);
  const [draft, setDraft] = useState(false);
  const [preview, setPreview] = useState(false);
  const [quoteStatus, setQuoteStatus] = useState("DRAFT");
  const [maxDate, setMaxDate] = useState(new Date());
  const [isVisible, setIsVisible] = useState();
  const [link, setLink] = useState("");
  const [loading, setLoading] = useState(false);
  const [openEmailDialog, setOpenEmailDialog] = useState(false);
  const [email, setEmail] = useState("");
  const [error, setError] = useState(false);
  const [currentClient, setCurrentClient] = useState({});
  const [clientLoader, setClientLoader] = useState(false);
  const [userData, setUserData] = useState(getUserData());
  const [openAlert, setOpenAlert] = useState(false);
  const [msg, setMsg] = useState("");
  const [alertType, setAlertType] = useState("Error");
  const filter = createFilterOptions();

  const closeSlider = () => {
    setIsVisible(false);
  };

  const openSlider = async (quote_id) => {
    setLoading(true);
    setIsVisible(true);
    const formData = new FormData();
    formData.append("id", quote_id);
    formData.append("type", "quote");

    const resultInfo = await preview(formData);
    if (resultInfo.success) {
      setLink(resultInfo.data.link);
      setLoading(false);
    } else {
      setLoading(false);
    }
  };

  const classes = useStyles();

  const handleFormSubmit = async (values, setSubmitting) => {
    let q_date = values.quoteDate;
    let qx_date = values.quoteExpDate;

    const formData = new FormData();
    formData.append("client_id", values.client_id ? values.client_id : "");
    formData.append("description", values.description);
    formData.append(
      "valid_till",
      qx_date.getFullYear() +
        "-" +
        (qx_date.getMonth() + 1) +
        "-" +
        qx_date.getDate()
    );
    formData.append(
      "quote_date",
      q_date.getFullYear() +
        "-" +
        (q_date.getMonth() + 1) +
        "-" +
        q_date.getDate()
    );
    formData.append("send", draft ? 0 : 1);
    formData.append("save", draft ? 0 : 1);
    formData.append("gst_percentage", gstEnabled ? gst_percentage : 0);
    formData.append("notes", values.notes);

    values.items.map((v, k) => {
      formData.append("item[" + k + "][description]", v.description);
      formData.append("item[" + k + "][price]", v.price);
      formData.append("item[" + k + "][qty]", v.qty);
      formData.append(
        "item[" + k + "][discount_percentage]",
        v.disc !== "" ? v.disc : 0
      );
    });

    let result;
    if (props.updateId) {
      formData.append("id", props.updateId);
      result = await Update(formData);
    } else {
      result = await Add(formData);
    }
    if (result.success) {
      let quote_id =
        props.updateId === "" ? result.data.quote_id : props.updateId;
      setSubmitting(false);
      if (!preview) {
        props.setIsOpen(false);
      }
      props.setIsListUpdate(true);
      if (props.updateId === "") {
        props.setEmptyView(false);
      }
      if (preview) {
        props.setUpdateId(quote_id);
        await openSlider(quote_id);
      }
      setEmail("");
      setAlertType("Success");
      setMsg(result.message);
      setOpenAlert(true);
    } else {
      setSubmitting(false);
      setAlertType("Error");
      setMsg(result.message);
      setOpenAlert(true);
    }
    setRefreshSummary(!refreshSummary);
  };

  const AddClient = async (client_name) => {
    setClientLoader(true);
    const formData1 = new FormData();
    formData1.append("type", 1);
    formData1.append("name", client_name);
    const result1 = await AddClientData(formData1);
    setClientLoader(false);
    if (result1.success) {
      setEmail("");
      let temp = clientList;
      temp.push({
        label: client_name,
        id: result1.data.id,
        email: "",
      });
      setClientList(temp);
      return {
        client_id: result1.data.id,
        client_name: client_name,
      };
    }
    return {};
  };

  useEffect(async () => {
    if (props.isOpen) {
      setEmail("");
      setLoader(true);
      setDraft(false);
      setSubtotAmount(0);
      setDiscount(0);
      setGstEnabled(false);
      setGstAmount(0);
      setTotAmount(0);
      const formData = "?filter_by[type]=client&page_size=1000";

      const resultInfoClients = await ListClient(formData);
      if (resultInfoClients.success) {
        let temp = [];
        resultInfoClients.data.clients_details.map((v) => {
          temp.push({
            email: v.email,
            label: v.name,
            id: v.client_id,
          });
        });
        setClientList(temp);

        if (props.updateId) {
          const resultInfo = await Info(props.updateId);
          if (resultInfo.success) {
            initialValues.description = resultInfo.data.description;
            if (resultInfo.data.notes && resultInfo.data.notes !== "null") {
              initialValues.notes = resultInfo.data.notes;
            } else {
              initialValues.notes = "";
            }
            initialValues.quoteExpDate = new Date(resultInfo.data.valid_till);
            initialValues.quoteDate = new Date(resultInfo.data.quote_date);

            // Set Invoice Status
            setQuoteStatus(resultInfo.data.status);

            if (resultInfo.data.client) {
              let selectedClient = {
                client_id: resultInfo.data.client.client_id,
                name: resultInfo.data.client.name,
                email: "",
              };

              resultInfoClients.data.clients_details.map((v) => {
                if (v.client_id == selectedClient.client_id) {
                  selectedClient.email = v.email;
                }
              });

              initialValues.client = {
                id: selectedClient.client_id,
                label: selectedClient.name,
                email: selectedClient.email,
              };

              initialValues.client_id = selectedClient.client_id;
              initialValues.client_name = selectedClient.name;
              setEmail(selectedClient.email);
            }
            let temp = [];
            resultInfo.data.quote_item.map((vv, kk) => {
              temp.push({
                description: vv.description,
                qty: vv.qty,
                disc: vv.discount_percentage,
                price: vv.price,
              });
            });
            initialValues.items = temp;
            let gst = false;
            if (resultInfo.data.total_gst > 0) {
              gst = true;
            }
            setGstEnabled(gst);
            calculation(initialValues.items, "", "", "", gst);
            setLoader(false);
          }
        } else {
          initialValues.client = null;
          initialValues.description = "";
          initialValues.notes = "";
          initialValues.items = [
            {
              description: "",
              qty: "",
              disc: "",
              price: "",
            },
          ];
          initialValues.quoteExpDate = "";
          initialValues.quoteDate = new Date();
          setLoader(false);
        }
      }
    }
  }, [props.isOpen]);

  const calculation = (data, index, key, val, g = gstEnabled) => {
    let sub_total = 0;
    let gst_total = 0;
    let total = 0;
    let dis = 0;
    data.map((vv, kk) => {
      if (val === "" && kk === index && key !== "disc") {
        return false;
      } else {
        let temp_p = vv.price;
        let temp_q = vv.qty;
        let temp_d = vv.disc;

        if (index === kk) {
          if (key === "price") temp_p = val;
          else if (key === "qty") temp_q = val;
          else if (key === "disc") temp_d = val;
        }
        if (isNaN(temp_d) || parseInt(temp_d) < 1 || temp_d === "") {
          temp_d = 0;
        }

        if (temp_p && temp_q) {
          dis += temp_d ? temp_p * temp_q * (temp_d / 100) : 0;
          sub_total += temp_p * temp_q;
        }
      }
    });

    gst_total = g === true ? (sub_total - dis) * (gst_percentage / 100) : 0;
    total = sub_total + gst_total - dis;

    setGstAmount(gst_total);
    setSubtotAmount(sub_total);
    setTotAmount(total);
    setDiscount(dis);
  };

  const changeDate = (val, setFieldValue) => {
    setFieldValue("quoteDate", val);
    let date = new Date(val);
    setMaxDate(date);
    date.setDate(date.getDate() + 1);
    setFieldValue("quoteExpDate", date);
  };

  return (
    <>
      <Transition.Root show={props.isOpen} as={Fragment}>
        <Dialog
          as="div"
          static
          className="fixed inset-0 overflow-hidden"
          onClose={props.setIsOpen}
        >
          <div className="absolute inset-0 overflow-hidden">
            <Transition.Child
              as={Fragment}
              enter="ease-in-out duration-500"
              enterFrom="opacity-0"
              enterTo="opacity-100"
              leave="ease-in-out duration-500"
              leaveFrom="opacity-100"
              leaveTo="opacity-0"
            >
              <Dialog.Overlay className="absolute inset-0 bg-gray-500 bg-opacity-75 transition-opacity pointer-events-none" />
            </Transition.Child>

            <div className="fixed inset-y-0 max-w-full right-0 flex">
              <Transition.Child
                as={Fragment}
                enter="transform transition ease-in-out duration-500 sm:duration-700"
                enterFrom="translate-x-full"
                enterTo="translate-x-0"
                leave="transform transition ease-in-out duration-500 sm:duration-700"
                leaveFrom="translate-x-0"
                leaveTo="translate-x-full"
              >
                <div className="w-screen xl:max-w-7xl">
                  <Formik
                    initialValues={initialValues}
                    validationSchema={QuoteSchema}
                    onSubmit={(values, { setSubmitting }) => {
                      if (!draft && email == "" && !error) {
                        setSubmitting(false);
                        setOpenEmailDialog(true);
                        return;
                      }

                      handleFormSubmit(values, setSubmitting);
                    }}
                  >
                    {({
                      values,
                      errors,
                      handleChange,
                      handleSubmit,
                      handleBlur,
                      touched,
                      isSubmitting,
                      setFieldValue,
                    }) => (
                      <form
                        id="quote-form"
                        onSubmit={handleSubmit}
                        className="h-full divide-y divide-gray-200 flex flex-col bg-white shadow-xl"
                      >
                        <div className="flex-1 h-0 pb-8 overflow-y-auto">
                          <div className="border-b border-gray-200 bg-gray-100 py-4 px-4 sm:px-6">
                            <div className="flex items-center justify-between">
                              <Dialog.Title className="text-xl font-medium text-gray-800">
                                {props.updateId ? "Edit " : "New "}
                                Quote
                                <p className="mt-1 font-normal text-sm text-gray-500">
                                  Fields marked with * are mandatory
                                </p>
                              </Dialog.Title>
                              <div className="ml-3 h-7 flex items-center">
                                <button
                                  type="button"
                                  className="inline-flex items-center mr-2 px-4 py-1.5 border border-indigo-400 text-sm font-medium  rounded-full text-brand-blue bg-gray-50 hover:bg-indigo-100 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-purple-500"
                                  disabled={isSubmitting}
                                  onClick={() => {
                                    setPreview(true);
                                    setDraft(true);
                                    handleSubmit();
                                  }}
                                >
                                  {isSubmitting ? "Please wait..." : "Preview"}
                                </button>

                                <button
                                  type="button"
                                  className="rounded-full hover:bg-gray-300 p-2 text-gray-500 hover:text-gray-700 focus:outline-none focus:ring-2 focus:ring-white"
                                  onClick={() => props.setIsOpen(false)}
                                >
                                  <span className="sr-only">Close panel</span>
                                  <XIcon
                                    className="h-6 w-6"
                                    aria-hidden="true"
                                  />
                                </button>
                              </div>
                            </div>
                          </div>

                          <div className="flex-1 flex flex-col justify-between">
                            <div className="px-4  sm:px-6">
                              {!loader ? (
                                <>
                                  <div className="border-b border-gray-200 mb-4 pt-6 pb-7 ">
                                    <div className="flex items-center justify-between border-b border-gray-200 pb-7">
                                      <div className="mt-1 flex rounded-md shadow-sm pointer-events-none">
                                        <span className="inline-flex items-center px-3 rounded-l-md border-2 border-r-0 font-semibold border-gray-200 bg-gray-100 text-gray-500 text-lg">
                                          {props.updateId
                                            ? quoteStatus.toUpperCase()
                                            : "DRAFT"}
                                        </span>
                                        <input
                                          type="text"
                                          name="quote-name"
                                          id="quote-name"
                                          readOnly
                                          tab={-1}
                                          className="input py-3 px-4 block w-full text-xl border-2  text-gray-800 focus:ring-indigo-400 focus:border-indigo-400 border-gray-200 rounded-tr-md rounded-br-md"
                                          value="Quotation"
                                        />
                                      </div>

                                      <div className="h-32 w-32 flex items-center justify-center rounded-md border-2 border-gray-100 relative">
                                        {userData &&
                                        userData.profile_pic !== "" &&
                                        userData.profile_pic !== null ? (
                                          <img
                                            className="h-8 w-8 rounded-full"
                                            src={
                                              userData &&
                                              userData.profile_pic !== "" &&
                                              userData.profile_pic !== null
                                                ? userData.profile_pic
                                                : "/images/avatar/1.jpg"
                                            }
                                            alt="User"
                                            width="80"
                                            height="80"
                                          />
                                        ) : (
                                          <img
                                            src="/images/icon/dummy-logo.jpg"
                                            alt="Sole logo"
                                            width="180"
                                            height="180"
                                            className="object-cover"
                                          />
                                        )}
                                        <InformationCircleIcon
                                          className="h-8 w-8 text-gray-400 ml-2 absolute -top-2 -right-2 bg-white rounded-full p-1"
                                          data-for="profile-image"
                                          data-tip
                                        />
                                        <ReactTooltip
                                          id="profile-image"
                                          className="custom-tooltip bg-gray-900"
                                          textColor="#ffffff"
                                          backgroundColor="#111827"
                                          effect="solid"
                                          aria-haspopup="true"
                                        >
                                          <div className="w-64">
                                            Please upload your business logo
                                            from your profile under settings.
                                            Sole app uses your logo on all
                                            invoices. The best suggested size is
                                            500 x 500 pixel.
                                          </div>
                                        </ReactTooltip>
                                      </div>
                                    </div>

                                    <div className="items-baseline space-y-6  grid grid-cols-1 gap-y-2 sm:pt-0 md:grid-cols-2 sm:gap-x-8">
                                      <div className="from-address">
                                        <p>
                                          <strong className="font-bold block mb-2">
                                            From
                                          </strong>
                                          <span className="uppercase">
                                            {!!user?.business_name
                                              ? user.business_name
                                              : user?.full_name}
                                          </span>{" "}
                                          <br></br>
                                          {user?.address?.address_line1
                                            .split(",")
                                            .map((str) => (
                                              <p key={str}>{str}</p>
                                            ))}
                                          {user?.address?.suburb
                                            .split(",")
                                            .map((str) => (
                                              <p key={str}>{str}</p>
                                            ))}
                                          {user?.address?.state
                                            .split(",")
                                            .map((str) => (
                                              <p key={str}>
                                                {str} {user?.address?.country}
                                              </p>
                                            ))}
                                        </p>
                                      </div>

                                      <div className="client-name mt-10">
                                        <div className="flex justify-between">
                                          <strong className="font-bold block mb-2">
                                            To
                                          </strong>
                                        </div>

                                        <div className="flex justify-between">
                                          <label
                                            htmlFor="company"
                                            className="block text-md tracking-wider font-medium text-gray-500"
                                          >
                                            Client Name*
                                          </label>
                                          {clientLoader && (
                                            <div
                                              style={{ marginRight: "13px" }}
                                            >
                                              <CircularProgress size={15} />
                                            </div>
                                          )}
                                        </div>

                                        <div className="mt-1">
                                          <Stack spacing={2}>
                                            <Autocomplete
                                              disablePortal
                                              id="client_name"
                                              name="client_name"
                                              value={values.client}
                                              onBlur={handleBlur}
                                              freeSolo
                                              className="p-0 form-control"
                                              options={clientList}
                                              getOptionLabel={(option) => {
                                                if (
                                                  option?.value ===
                                                  "Add new Client"
                                                ) {
                                                  return (
                                                    <div className="py-2 flex gap-1">
                                                      Add New Client&nbsp;&nbsp;
                                                      <IconContext.Provider
                                                        value={{
                                                          color: "#4d4dff",
                                                          className:
                                                            "global-class-name h-6 w-6",
                                                        }}
                                                      >
                                                        <div>
                                                          <FiPlusCircle />
                                                        </div>
                                                      </IconContext.Provider>
                                                    </div>
                                                  );
                                                } else {
                                                  return option.label;
                                                }
                                              }}
                                              onChange={async (e, value) => {
                                                if (
                                                  value?.value ==
                                                  "Add new Client"
                                                ) {
                                                  setFieldValue(
                                                    "client_name",
                                                    value.label
                                                  );
                                                  setFieldValue("client", {
                                                    label: value.label,
                                                  });
                                                  setCurrentClient({
                                                    label: value.label,
                                                  });
                                                  let client_details =
                                                    await AddClient(
                                                      value.label
                                                    );
                                                  if (
                                                    !!client_details?.client_id
                                                  ) {
                                                    setFieldValue(
                                                      "client_id",
                                                      client_details.client_id
                                                    );
                                                    setFieldValue(
                                                      "client_name",
                                                      client_details.client_name
                                                    );
                                                    setFieldValue("client", {
                                                      label:
                                                        client_details.client_name,
                                                      id: client_details.client_id,
                                                    });
                                                    setCurrentClient({
                                                      label:
                                                        client_details.client_name,
                                                      id: client_details.client_id,
                                                    });
                                                  }
                                                } else {
                                                  if (
                                                    typeof value !==
                                                      "undefined" &&
                                                    value !== null &&
                                                    Object.keys(value).length >
                                                      0
                                                  ) {
                                                    if (
                                                      typeof value === "object"
                                                    ) {
                                                      setFieldValue(
                                                        "client_id",
                                                        value.id
                                                      );
                                                      setFieldValue(
                                                        "client_name",
                                                        value.label
                                                      );
                                                      setEmail(value.email);
                                                    } else {
                                                      setFieldValue(
                                                        "client_id",
                                                        ""
                                                      );
                                                      setFieldValue(
                                                        "client_name",
                                                        ""
                                                      );
                                                      setFieldValue("client", {
                                                        label: "",
                                                      });
                                                      setEmail("");
                                                    }
                                                  } else {
                                                    setFieldValue(
                                                      "client_id",
                                                      ""
                                                    );
                                                    setFieldValue(
                                                      "client_name",
                                                      ""
                                                    );
                                                    setEmail("");
                                                  }
                                                }
                                              }}
                                              renderInput={(params) => (
                                                <TextField
                                                  {...params}
                                                  variant="outlined"
                                                />
                                              )}
                                              filterOptions={(
                                                options,
                                                params
                                              ) => {
                                                const filtered = filter(
                                                  options,
                                                  params
                                                );
                                                // Suggest the creation of a new value
                                                if (params.inputValue !== "") {
                                                  filtered.push({
                                                    label: params.inputValue,
                                                    value: `Add new Client`,
                                                  });
                                                }
                                                return filtered;
                                              }}
                                            />
                                          </Stack>
                                          <ErrorMessage
                                            name="client_name"
                                            component="span"
                                            className="error_text"
                                          />
                                        </div>
                                      </div>
                                    </div>

                                    <div className="border-gray-200  mt-4 pt-6 grid grid-cols-1 sm:grid-cols-2 sm:gap-x-8">
                                      <div>
                                        <label
                                          htmlFor="last-name"
                                          className="block text-md tracking-wider font-medium text-gray-500"
                                        >
                                          Quote Date*
                                        </label>
                                        <div className="mt-1 relative border-2 date-picker bg-white focus:ring-indigo-400 focus:border-indigo-400 border-gray-100 rounded-md">
                                          <DateTimePicker
                                            date={values.quoteDate}
                                            onDateChange={(val) => {
                                              changeDate(val, setFieldValue);
                                            }}
                                            name="quoteDate"
                                            id="quoteDate"
                                            inputClassName={
                                              "input py-3 px-4 block w-full border-0"
                                            }
                                            placeholder=""
                                          />
                                        </div>
                                        <ErrorMessage
                                          name="quoteDate"
                                          component="span"
                                          className="error_text"
                                        />
                                      </div>

                                      <div>
                                        <label
                                          htmlFor="last-name"
                                          className="block text-md tracking-wider font-medium text-gray-500"
                                        >
                                          Quote Expiry Date*
                                        </label>
                                        <div className="mt-1 relative border-2 date-picker bg-white focus:ring-indigo-400 focus:border-indigo-400 border-gray-200 rounded-md">
                                          <DateTimePicker
                                            date={values.quoteExpDate}
                                            onDateChange={(val) => {
                                              setFieldValue(
                                                "quoteExpDate",
                                                val
                                              );
                                            }}
                                            name="quoteExpDate"
                                            id="quoteExpDate"
                                            minDate={maxDate}
                                            inputClassName={
                                              "input py-3 px-4 block w-full border-0"
                                            }
                                            placeholder=""
                                          />
                                        </div>
                                        <ErrorMessage
                                          name="quoteExpDate"
                                          component="span"
                                          className="error_text"
                                        />
                                      </div>

                                      <div className="sm:col-span-2 mt-7">
                                        <label
                                          htmlFor="company"
                                          className="block text-md tracking-wider font-medium text-gray-500"
                                        >
                                          Quote Description
                                        </label>
                                        <div className="mt-1">
                                          <input
                                            type="text"
                                            name="description"
                                            id="description"
                                            className={
                                              "placeholder-gray-400 py-3 px-4 block w-full border-2 border-gray-200  focus:ring-indigo-400 focus:border-indigo-400 border-gray-100 rounded-md " +
                                              classes.fControl
                                            }
                                            variant="outlined"
                                            value={values.description}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            placeholder="Quote Description*"
                                          />
                                          <ErrorMessage
                                            name="description"
                                            component="span"
                                            className="error_text"
                                          />
                                        </div>
                                      </div>
                                    </div>
                                  </div>

                                  <table className="min-w-full border-none">
                                    <thead>
                                      <tr>
                                        <th
                                          scope="col"
                                          className="pr-7 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                                        >
                                          Items
                                        </th>
                                        <th
                                          scope="col"
                                          className="pr-7 w-40 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                                        >
                                          Qty
                                        </th>
                                        <th
                                          scope="col"
                                          className="pr-7 py-3 w-40 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                                        >
                                          Price
                                        </th>
                                        <th
                                          scope="col"
                                          className="pr-7 py-3 w-24 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                                        >
                                          Discount
                                        </th>
                                        <th
                                          scope="col"
                                          className="pr-7 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                                        >
                                          <span className="sr-only">
                                            Action
                                          </span>
                                        </th>
                                      </tr>
                                    </thead>

                                    {/* <tbody className="bg-white divide-y divide-gray-200">   */}
                                    <FieldArray
                                      name="items"
                                      render={({ insert, remove, push }) => (
                                        <tbody className="bg-white divide-y divide-gray-200">
                                          {values.items.length > 0 &&
                                            values.items.map((vv, kk) => (
                                              <tr
                                                key={kk}
                                                className="align-top"
                                              >
                                                <td className="pr-4 py-4">
                                                  <div className=" flex items-center whitespace-nowrap">
                                                    <div className="text-gray-900 pr-4">
                                                      {kk + 1}
                                                    </div>
                                                    <div className="relative py-3 px-4 block w-full border-2 border-gray-200 focus-within:ring-indigo-400 focus-within:border-indigo-400 border-gray-100 rounded-md">
                                                      <label
                                                        htmlFor="name"
                                                        className="absolute -top-2 bg-white tracking-wide left-2 -mt-px inline-block px-1  text-sm font-medium text-gray-500"
                                                      >
                                                        Item Description*
                                                      </label>
                                                      <input
                                                        type="text"
                                                        className="block w-full border-0 p-0 text-gray-900 placeholder-gray-500 focus:ring-0 sm:text-sm"
                                                        placeholder=""
                                                        lines="3"
                                                        variant="outlined"
                                                        name={`items.${kk}.description`}
                                                        value={vv.description}
                                                        onChange={handleChange}
                                                        onBlur={handleBlur}
                                                      />
                                                    </div>
                                                  </div>

                                                  <ErrorMessage
                                                    name={`items.${kk}.description`}
                                                    component="span"
                                                    className="error_text"
                                                  />
                                                </td>

                                                <td className="pr-4 py-4 whitespace-nowrap">
                                                  <div className="relative py-3 px-4 block w-full border-2 border-gray-200 focus-within:ring-indigo-400 focus-within:border-indigo-400 border-gray-100 rounded-md">
                                                    <label
                                                      htmlFor="name"
                                                      className="absolute -top-2 bg-white tracking-wide left-2 -mt-px inline-block px-1  text-sm font-medium text-gray-500"
                                                    >
                                                      Quantity*
                                                    </label>
                                                    <input
                                                      type="number"
                                                      className="block w-full  border-0 p-0 text-gray-900 placeholder-gray-500 focus:ring-0 sm:text-sm"
                                                      placeholder=""
                                                      name={`items.${kk}.qty`}
                                                      value={vv.qty}
                                                      onChange={async (e) => {
                                                        handleChange(e);
                                                        calculation(
                                                          values.items,
                                                          kk,
                                                          "qty",
                                                          e.target.value
                                                        );
                                                      }}
                                                      onBlur={handleBlur}
                                                      min={0}
                                                    />
                                                  </div>
                                                  <ErrorMessage
                                                    name={`items.${kk}.qty`}
                                                    component="span"
                                                    className="error_text"
                                                  />
                                                </td>

                                                <td className="pr-4 py-4 whitespace-nowrap">
                                                  <div className="relative py-3 px-4 block w-full border-2 border-gray-200 focus-within:ring-indigo-400 focus-within:border-indigo-400 border-gray-100 rounded-md">
                                                    <label
                                                      htmlFor="name"
                                                      className="absolute -top-2 bg-white tracking-wide left-2 -mt-px inline-block px-1  text-sm font-medium text-gray-500"
                                                    >
                                                      Price per unit*
                                                    </label>
                                                    <div className="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
                                                      <span className="text-gray-500 font-semibold sm:text-md">
                                                        $
                                                      </span>
                                                    </div>
                                                    <input
                                                      type="number"
                                                      className={
                                                        "block w-full  border-0 p-0 text-gray-900 placeholder-gray-500 focus:ring-0 sm:text-sm " +
                                                        classes.dollarPrice
                                                      }
                                                      placeholder="0.00"
                                                      name={`items.${kk}.price`}
                                                      value={vv.price}
                                                      onChange={(e) => {
                                                        handleChange(e);
                                                        calculation(
                                                          values.items,
                                                          kk,
                                                          "price",
                                                          e.target.value
                                                        );
                                                      }}
                                                      onBlur={(e) => {
                                                        let temp = values.items;
                                                        temp[kk].price =
                                                          parseFloat(
                                                            e.target.value
                                                          ).toFixed(2);
                                                        setFieldValue(
                                                          "items",
                                                          temp
                                                        );
                                                      }}
                                                      min={0}
                                                    />
                                                  </div>
                                                  <ErrorMessage
                                                    name={`items.${kk}.price`}
                                                    component="span"
                                                    className="error_text"
                                                  />
                                                </td>

                                                <td className="pr-4 py-4 whitespace-nowrap">
                                                  <div className="relative py-3 px-4 block w-full border-2 border-gray-200 focus-within:ring-indigo-400 focus-within:border-indigo-400 border-gray-100 rounded-md">
                                                    <label
                                                      htmlFor="name"
                                                      className="absolute -top-2 bg-white tracking-wide left-2 -mt-px inline-block px-1  text-sm font-medium text-gray-500"
                                                    >
                                                      Disc %
                                                    </label>
                                                    <input
                                                      type="number"
                                                      className="block w-full  border-0 p-0 text-gray-900 placeholder-gray-500 focus:ring-0 sm:text-sm"
                                                      placeholder=""
                                                      name={`items.${kk}.disc`}
                                                      value={vv.disc}
                                                      onChange={(e) => {
                                                        handleChange(e);
                                                        calculation(
                                                          values.items,
                                                          kk,
                                                          "disc",
                                                          e.target.value
                                                        );
                                                      }}
                                                      onBlur={handleBlur}
                                                      min={0}
                                                    />
                                                  </div>
                                                </td>

                                                <td className="pr-4 py-4 whitespace-nowrap">
                                                  {values.items.length > 1 ? (
                                                    <button
                                                      type="button"
                                                      className="rounded-md text-gray-500 hover:text-gray-700 focus:outline-none focus:ring-2 focus:ring-white"
                                                      onClick={() => {
                                                        calculation(
                                                          values.items,
                                                          kk,
                                                          "",
                                                          ""
                                                        );
                                                        remove(kk);
                                                      }}
                                                    >
                                                      <span className="sr-only">
                                                        Remove item
                                                      </span>
                                                      <XCircleIcon
                                                        className="h-6 w-6"
                                                        aria-hidden="true"
                                                      />
                                                    </button>
                                                  ) : null}
                                                </td>
                                              </tr>
                                            ))}
                                          <tr>
                                            <td>
                                              <button
                                                type="button"
                                                className="inline-flex items-center pl-6 py-2 border-none text-sm leading-4 font-medium  text-blue-700 hover:text-blue-800 focus:outline-none  focus:ring-blue-500"
                                                onClick={() =>
                                                  push({
                                                    description: "",
                                                    qty: "",
                                                    disc: "",
                                                    price: "",
                                                  })
                                                }
                                              >
                                                <PlusSmIcon
                                                  className="-ml-0.5 mr-2 h-4 w-4"
                                                  aria-hidden="true"
                                                />
                                                Add New Item
                                              </button>
                                            </td>
                                          </tr>
                                        </tbody>
                                      )}
                                    />
                                  </table>

                                  <div className="grid grid-cols-2 gap-4  mt-4 pt-2">
                                    <div className="col-span-2 ml-auto w-full lg:w-1/2">
                                      <ul className="-my-4">
                                        <li className="flex items-center py-3 space-x-3">
                                          <div className="min-w-0 flex-1">
                                            <p className="text-md font-medium text-gray-700">
                                              Include GST (10%)
                                            </p>
                                          </div>

                                          <div className="flex-shrink-0">
                                            <Switch
                                              checked={gstEnabled}
                                              onChange={() => {
                                                setGstEnabled(!gstEnabled);
                                                calculation(
                                                  values.items,
                                                  "",
                                                  "",
                                                  "",
                                                  !gstEnabled
                                                );
                                              }}
                                              className={classNames(
                                                gstEnabled
                                                  ? "bg-green-600"
                                                  : "bg-gray-300",
                                                "relative inline-flex flex-shrink-0 h-6 w-11 border-2 border-transparent rounded-full cursor-pointer transition-colors ease-in-out duration-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                                              )}
                                            >
                                              <span className="sr-only">
                                                GST Enable/Disable
                                              </span>
                                              <span
                                                className={classNames(
                                                  gstEnabled
                                                    ? "translate-x-5"
                                                    : "translate-x-0",
                                                  "pointer-events-none relative inline-block h-5 w-5 rounded-full bg-white shadow transform ring-0 transition ease-in-out duration-200"
                                                )}
                                              >
                                                <span
                                                  className={classNames(
                                                    gstEnabled
                                                      ? "opacity-0 ease-out duration-100"
                                                      : "opacity-100 ease-in duration-200",
                                                    "absolute inset-0 h-full w-full flex items-center justify-center transition-opacity pointer-events-none"
                                                  )}
                                                  aria-hidden="true"
                                                >
                                                  <svg
                                                    className="h-3 w-3 text-gray-400"
                                                    fill="none"
                                                    viewBox="0 0 12 12"
                                                  >
                                                    <path
                                                      d="M4 8l2-2m0 0l2-2M6 6L4 4m2 2l2 2"
                                                      stroke="currentColor"
                                                      strokeWidth={2}
                                                      strokeLinecap="round"
                                                      strokeLinejoin="round"
                                                    />
                                                  </svg>
                                                </span>
                                                <span
                                                  className={classNames(
                                                    gstEnabled
                                                      ? "opacity-100 ease-in duration-200"
                                                      : "opacity-0 ease-out duration-100",
                                                    "absolute inset-0 h-full w-full flex items-center justify-center transition-opacity pointer-events-none"
                                                  )}
                                                  aria-hidden="true"
                                                >
                                                  <svg
                                                    className="h-3 w-3 text-green-600"
                                                    fill="currentColor"
                                                    viewBox="0 0 12 12"
                                                  >
                                                    <path d="M3.707 5.293a1 1 0 00-1.414 1.414l1.414-1.414zM5 8l-.707.707a1 1 0 001.414 0L5 8zm4.707-3.293a1 1 0 00-1.414-1.414l1.414 1.414zm-7.414 2l2 2 1.414-1.414-2-2-1.414 1.414zm3.414 2l4-4-1.414-1.414-4 4 1.414 1.414z" />
                                                  </svg>
                                                </span>
                                              </span>
                                            </Switch>
                                          </div>
                                        </li>

                                        <li className="flex items-center py-3 space-x-3">
                                          <div className="min-w-0 flex-1">
                                            <p className="text-md font-medium text-gray-700">
                                              Subtotal
                                            </p>
                                          </div>
                                          <div className="flex-shrink-0">
                                            <p className="text-md font-medium text-gray-600">
                                              <NumberFormat
                                                value={subtotAmount.toFixed(2)}
                                                displayType={"text"}
                                                thousandSeparator={true}
                                                prefix={"$"}
                                                renderText={(value) => (
                                                  <p>{value}</p>
                                                )}
                                              />
                                            </p>
                                          </div>
                                        </li>

                                        <li className="flex items-center py-3 space-x-3">
                                          <div className="min-w-0 flex-1">
                                            <p className="text-md font-medium text-gray-700">
                                              Discount
                                            </p>
                                          </div>
                                          <div className="flex-shrink-0">
                                            <p className="text-md font-medium text-gray-600">
                                              <NumberFormat
                                                value={discount.toFixed(2)}
                                                displayType={"text"}
                                                thousandSeparator={true}
                                                prefix={"$"}
                                                renderText={(value) => (
                                                  <div>{value}</div>
                                                )}
                                              />
                                            </p>
                                          </div>
                                        </li>

                                        <li className="flex items-center py-3 space-x-3">
                                          <div className="min-w-0 flex-1">
                                            <p className="text-md font-medium text-gray-700">
                                              GST
                                            </p>
                                          </div>

                                          <div className="flex-shrink-0">
                                            <p className="text-md font-medium text-gray-600">
                                              <NumberFormat
                                                value={gstAmount.toFixed(2)}
                                                displayType={"text"}
                                                thousandSeparator={true}
                                                prefix={"$"}
                                                renderText={(value) => (
                                                  <div>{value}</div>
                                                )}
                                              />
                                            </p>
                                          </div>
                                        </li>

                                        <li className="flex items-center border-t border-gray-200 py-3 space-x-3">
                                          <div className="min-w-0 flex-1">
                                            <p className="text-xl font-semibold text-gray-900">
                                              Total
                                            </p>
                                          </div>

                                          <div className="flex-shrink-0">
                                            <p className="text-lg font-semibold text-gray-800">
                                              <NumberFormat
                                                value={totAmount.toFixed(2)}
                                                displayType={"text"}
                                                thousandSeparator={true}
                                                prefix={"$"}
                                                renderText={(value) => (
                                                  <div>{value}</div>
                                                )}
                                              />
                                            </p>
                                          </div>
                                        </li>
                                      </ul>
                                    </div>
                                  </div>

                                  <div className="sm:col-span-2  mt-5  border-t border-gray-200 mb-4 pt-6 pb-7">
                                    <label
                                      htmlFor="company"
                                      className="block text-md tracking-wider font-medium text-gray-500"
                                    >
                                      Quote notes
                                    </label>
                                    <div className="mt-1">
                                      <textarea
                                        type="text"
                                        rows={5}
                                        name="notes"
                                        id="notes"
                                        className={
                                          "placeholder-gray-400 py-3 px-4 block w-full border-2 border-gray-200  focus:ring-indigo-400 focus:border-indigo-400 border-gray-100 rounded-md " +
                                          classes.fControl
                                        }
                                        variant="outlined"
                                        value={values.notes}
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                      />
                                    </div>
                                  </div>
                                </>
                              ) : (
                                <div className="box-center">
                                  <CircularProgress />
                                </div>
                              )}
                            </div>
                          </div>
                        </div>

                        <div className="flex-shrink-0  px-4 py-4 flex justify-end">
                          <div>
                            <button
                              type="button"
                              id="draft"
                              className="bg-white py-2 px-4 border border-gray-300 rounded-full tracking-wider shadow-sm text-sm uppercase font-medium text-gray-700 bg-gray-100 hover:bg-gray-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                              disabled={isSubmitting ? true : false}
                              onClick={() => {
                                setPreview(false);
                                setDraft(true);
                                handleSubmit();
                              }}
                            >
                              {isSubmitting && draft
                                ? "Please wait..."
                                : "Save as Draft"}
                            </button>
                            <button
                              type="button"
                              id="send"
                              className="ml-4 inline-flex justify-center py-2 px-4 tracking-wider border border-transparent uppercase shadow-sm  font-medium text-sm rounded-full text-white bg-green-600 hover:bg-green-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-500"
                              disabled={isSubmitting ? true : false}
                              onClick={() => {
                                setPreview(false);
                                setDraft(false);
                                handleSubmit();
                              }}
                            >
                              {isSubmitting && !draft
                                ? "Please wait..."
                                : "Save & Send"}
                            </button>
                          </div>
                        </div>
                      </form>
                    )}
                  </Formik>

                  {/* Preview Slider   */}
                  <div>
                    <div
                      className={`${
                        isVisible ? "translate-x-0" : "  translate-x-full"
                      } "w-screen lg:max-w-7xl absolute preview transition transform ease-in-out duration-500 inset-0 overflow-y-auto`}
                    >
                      <div className="h-full items-center justify-center  text-center bg-white shadow-xl">
                        <div className="bg-gray-100 py-4 px-4 sm:px-6 border-b border-gray-200 ">
                          <div className="flex items-center justify-between">
                            <h3 className="text-lg font-medium text-left text-gray-800">
                              Quote Preview
                            </h3>

                            <div className="ml-3 h-7 flex items-center">
                              <button
                                type="button"
                                className="rounded-full hover:bg-gray-300 p-2 text-gray-500 hover:text-gray-700 focus:outline-none focus:ring-2 focus:ring-white"
                                onClick={closeSlider}
                              >
                                <span className="sr-only">Close panel</span>
                                <XIcon className="h-6 w-6" aria-hidden="true" />
                              </button>
                            </div>
                          </div>
                        </div>

                        {!loading ? (
                          <iframe
                            src={link + "#toolbar=0"}
                            height="95%"
                            width="100%"
                          ></iframe>
                        ) : (
                          <div className="space-y-6 border-gray-200 mb-4 pt-6 pb-7 grid grid-cols-1 gap-y-2 sm:grid-cols-1">
                            {Array.from(Array(1), (e, i) => {
                              return (
                                <div className="sm:col-span-2" key={i}>
                                  <label className="info-label">
                                    <Skeleton />
                                  </label>
                                  <p>
                                    <Skeleton />
                                  </p>
                                </div>
                              );
                            })}
                          </div>
                        )}
                      </div>
                    </div>
                  </div>
                </div>
              </Transition.Child>
            </div>

            <EmailDialog
              id="email-dialog"
              open={openEmailDialog}
              email={email}
              error={error}
              client={currentClient}
              type="quote"
              setDraft={setDraft}
              setEmail={setEmail}
              setError={setError}
              onClose={setOpenEmailDialog}
            />
            <AlertDialog
              id="ringtone-menu2"
              title={alertType}
              message={msg}
              keepMounted
              open={openAlert}
              onClose={setOpenAlert}
            />
          </div>
        </Dialog>
      </Transition.Root>
    </>
  );
}
