export default function EmptyView() {

  return (
    <div className="flex items-center m-7 justify-center shadow h-3/4 rounded-md bg-white">
      <div className="text-center">
        <img
          src="/images/icon/undraw_file_searching_re_3evy.svg"
          width={200}
          height={200}
          alt="Create your first Quote"
          className="mx-auto" />

        <h3 className="mt-2 text-2xl font-medium text-gray-900">Create your first Quote</h3>
        <p className="mt-1 text-sm text-gray-500">Get started by creating a new Quote.</p>
        <div className="mt-6">
          {/* <div className="inline-flex rounded-md">
                    <button 
                    className="inline-flex items-center justify-center pr-5 pl-2 py-1 border border-transparent text-base font-medium rounded-full text-white bg-brand-blue hover:bg-brand-darkblue"
                    >
                    <IoIosAddCircleOutline className="w-10 h-10 pr-2 pl-0" /> Create An Quote
                    </button>
                </div> */}
        </div>
      </div>
    </div>
  )
}
