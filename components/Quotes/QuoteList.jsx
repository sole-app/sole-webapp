import { Fragment, useState, useEffect } from "react";
import { Menu, Listbox, Transition } from "@headlessui/react";
import { CheckIcon, SelectorIcon } from "@heroicons/react/solid";
import { SearchIcon, DotsVerticalIcon } from "@heroicons/react/outline";
import { Delete, List } from "../../services/api/quote.services";
import { convertToInvoice as convertToInvoices } from "../../services/api/invoice.services";
import Skeleton from "react-loading-skeleton";
import "react-loading-skeleton/dist/skeleton.css";
import ConfirmationDialog from "../ConfirmationDialog";
import Pagination from "../Pagination";
import NumberFormat from "react-number-format";
import ReactTooltip from "react-tooltip";
import { formatDateToDB, formatShortDate } from "../../helpers/common";
import AlertDialog from "../AlertDialog";

// For status dropdown
const statuses = [
  { id: "all", name: "All Status" },
  { id: "draft", name: "Draft" },
  { id: "sent", name: "Sent" },
];

let globalSearch = "";
let apiRunning = false;

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

export default function QuoteList(props) {
  const { setRefreshSummary, refreshSummary } = props;
  const [loader, setLoader] = useState(true);
  const [records, setRecords] = useState([]);
  const [confirmation, setConfirmation] = useState(false);
  const [deleteId, setDeleteId] = useState("");
  const [activeKey, setActiveKey] = useState("");
  const [selected, setSelected] = useState(statuses[0]);
  const [search, setSearch] = useState("");
  const [openAlert, setOpenAlert] = useState(false);
  const [msg, setMsg] = useState("");
  const [alertType, setAlertType] = useState("Error");

  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [paginationObj, setpaginationObj] = useState([]);

  // GET ALL LIST
  const getList = async (
    newPage,
    newRowPerPage,
    filter = {},
    resultCheck = false
  ) => {
    props.setIsListUpdate(false);

    setLoader(true);
    let filterByName;
    let formData = "";
    formData += "?page_size=" + newRowPerPage;
    formData += "&page=" + (newPage + 1);
    formData += "&sort_by[quote_id]=desc";

    if (typeof filter.search !== "undefined") {
      formData += "&filter_by[search]=" + filter.search;
      filterByName = filter.search;
    } else {
      formData += "&filter_by[search]=" + search;
      filterByName = search;
    }

    if (typeof filter.status !== "undefined") {
      if (filter.status.id !== "all") {
        formData += "&filter_by[status]=" + filter.status.id;
      }
    } else {
      if (selected.id !== "all") {
        formData += "&filter_by[status]=" + selected.id;
      }
    }
    apiRunning = true;
    const result = await List(formData);
    if (result.success) {
      if (resultCheck) {
        if (filterByName === globalSearch) {
          setRecords(result.data.quote_details);
          apiRunning = false;
          setpaginationObj({
            total: result.data.total || 0,
          });
        }
      } else {
        setRecords(result.data.quote_details);
        apiRunning = false;
        setpaginationObj({
          total: result.data.total || 0,
        });
      }
    }
    if (!apiRunning) {
      setLoader(false);
    }
  };

  const changeSearch = (event) => {
    globalSearch = event.target.value;
    setSearch(event.target.value);
  };

  const changeStatus = (statusData) => {
    let filter = {
      status: statusData,
    };
    setSelected(statusData);
    getList(page, rowsPerPage, filter);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
    getList(newPage, rowsPerPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value));
    getList(page, parseInt(event.target.value));
  };

  useEffect(() => {
    if (props.isListUpdate) {
      setLoader(true);
      getList(0, rowsPerPage);
    }
  }, [props.isListUpdate]);

  useEffect(() => {
    getList(page, rowsPerPage, {}, true);
  }, [search]);

  const convertToInvoice = async (quote) => {
    if (quote.status === "draft") {
      setAlertType("Error");
      setMsg("Quotes in draft status cannot be converted to invoices.");
      setOpenAlert(true);
    } else {
      let sent_date = quote.sent_at;
      if (sent_date === "" || sent_date === null) {
        let d = new Date();
        sent_date = formatDateToDB(d);
      }
      const formData = new FormData();
      formData.append("id", quote.quote_id);
      formData.append("due_date", sent_date);

      const result = await convertToInvoices(formData);
      if (result.success) {
        getList(0, rowsPerPage);
        setRefreshSummary(!refreshSummary);
      }
    }
  };

  const handleConfirmationClose = async (id) => {
    if (id) {
      const formData = new FormData();
      formData.append("id", id);

      const result = await Delete(formData);
      if (result.success) {
        records.splice(activeKey, 1);
        if (records.length === 0) {
          props.setEmptyView(true);
        }
        setRecords(records);
        setConfirmation(false);
      } else {
        setConfirmation(false);
      }
      setDeleteId("");
      setRefreshSummary(!refreshSummary);
    } else {
      setDeleteId("");
      setConfirmation(false);
    }
  };

  const quoteStatusStyle = (status) => {
    switch (status) {
      case "draft":
        return (
          <span className="px-4 py-1 inline-flex text-xs leading-5 font-medium rounded-full bg-gray-200 text-gray-800">
            {status.toUpperCase()}
          </span>
        );
      case "sent":
        return (
          <span className="px-4 py-1 inline-flex text-xs leading-5 font-medium rounded-full bg-green-100 text-green-800">
            {status.toUpperCase()}
          </span>
        );
      default:
        return (
          <span className="px-4 py-1 inline-flex text-xs leading-5 font-medium rounded-full bg-gray-200 text-gray-800">
            {status.toUpperCase()}
          </span>
        );
    }
  };

  return (
    <div className="mt-8 w-full flex flex-col">
      <AlertDialog
        id="ringtone-menu2"
        title={alertType}
        message={msg}
        keepMounted
        open={openAlert}
        onClose={setOpenAlert}
      />
      <ConfirmationDialog
        id="ringtone-menu"
        title="Remove Quote?"
        message="Are you sure you want to delete this item? This will be permanently removed."
        keepMounted
        open={confirmation}
        onClose={handleConfirmationClose}
        value={deleteId}
      />
      <div className="-my-2 w-full shadow bg-white overflow-hidden border-b border-gray-200 sm:rounded-lg ">
        <div className="pt-2 w-full align-middle inline-block min-w-full">
          <div>
            <div className="grid grid-cols-5 gap-5 p-7">
              <div className="col-span-4 mt-1 relative rounded-md">
                <div className="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
                  <SearchIcon
                    className="h-5 w-5 text-gray-500"
                    aria-hidden="true"
                  />
                </div>
                <input
                  type="text"
                  name="search"
                  id="search"
                  className="bg-gray-100 focus:ring-indigo-500 focus:border-indigo-500 block w-full pl-10 sm:text-sm p-3 border-gray-300 rounded-md"
                  placeholder="Quote # or Client"
                  onChange={changeSearch}
                />
              </div>

              <div>
                <Listbox value={selected} onChange={changeStatus}>
                  {({ open }) => (
                    <>
                      <div className="mt-1 relative">
                        <Listbox.Button className="bg-gray-100 relative w-full border border-gray-300 rounded-md shadow-sm pl-3 pr-10 py-3 text-left cursor-default focus:outline-none focus:ring-1 focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
                          <span className="block truncate">
                            {selected.name}
                          </span>
                          <span className="absolute inset-y-0 right-0 flex items-center pr-2 pointer-events-none">
                            <SelectorIcon
                              className="h-5 w-5 text-gray-400"
                              aria-hidden="true"
                            />
                          </span>
                        </Listbox.Button>

                        <Transition
                          show={open}
                          as={Fragment}
                          leave="transition ease-in duration-100"
                          leaveFrom="opacity-100"
                          leaveTo="opacity-0"
                        >
                          <Listbox.Options
                            static
                            className="absolute z-10 mt-1 w-full bg-white shadow-lg max-h-60 rounded-md py-1 text-base ring-1 ring-black ring-opacity-5 overflow-auto focus:outline-none sm:text-sm"
                          >
                            {statuses.map((status) => (
                              <Listbox.Option
                                key={status.id}
                                className={({ active }) =>
                                  classNames(
                                    active
                                      ? "text-white bg-brand-blue"
                                      : "text-gray-900",
                                    "cursor-default select-none relative py-2 pl-3 pr-9"
                                  )
                                }
                                value={status}
                              >
                                {({ selected, active }) => (
                                  <>
                                    <span
                                      className={classNames(
                                        selected
                                          ? "font-semibold"
                                          : "font-normal",
                                        "block truncate"
                                      )}
                                    >
                                      {status.name}
                                    </span>

                                    {selected ? (
                                      <span
                                        className={classNames(
                                          active
                                            ? "text-white"
                                            : "text-brand-blue",
                                          "absolute inset-y-0 right-0 flex items-center pr-4"
                                        )}
                                      >
                                        <CheckIcon
                                          className="h-5 w-5"
                                          aria-hidden="true"
                                        />
                                      </span>
                                    ) : null}
                                  </>
                                )}
                              </Listbox.Option>
                            ))}
                          </Listbox.Options>
                        </Transition>
                      </div>
                    </>
                  )}
                </Listbox>
              </div>
            </div>
            <div className="overflow-y-auto w-full overflow-x-auto">
              <table className="min-w-full hover-row divide-y divide-gray-200">
                <thead className="">
                  <tr>
                    <th
                      scope="col"
                      className="px-7 py-3 text-left text-sm font-medium text-gray-500 uppercase tracking-wider"
                    >
                      #
                    </th>
                    <th
                      scope="col"
                      className="px-7 py-3 text-left text-sm font-medium text-gray-500 uppercase tracking-wider"
                    >
                      QUOTE #
                    </th>
                    <th
                      scope="col"
                      className="px-7 py-3 text-left text-sm font-medium text-gray-500 uppercase tracking-wider"
                    >
                      Client Name
                    </th>
                    <th
                      scope="col"
                      className="px-7 py-3 text-left text-sm font-medium text-gray-500 uppercase tracking-wider"
                    >
                      Quote Date
                    </th>
                    <th
                      scope="col"
                      className="px-7 py-3 text-left text-sm font-medium text-gray-500 uppercase tracking-wider"
                    >
                      Total
                    </th>
                    <th
                      scope="col"
                      className="px-7 py-3 text-left text-sm font-medium text-gray-500 uppercase tracking-wider"
                    >
                      Description
                    </th>
                    <th
                      scope="col"
                      className="px-7 py-3 text-left text-sm font-medium text-gray-500 uppercase tracking-wider"
                    >
                      Status
                    </th>
                    <th
                      scope="col"
                      className="px-7 py-3 text-right text-sm font-medium text-gray-500 uppercase tracking-wider"
                    >
                      Actions
                    </th>
                  </tr>
                </thead>
                <tbody className="bg-white divide-y divide-gray-200">
                  {loader == false ? (
                    records.length > 0 ? (
                      records.map((val, key) => (
                        <tr key={key}>
                          <td className="px-6 py-4 whitespace-nowrap">
                            <div className="flex items-center">
                              <div className="text-md font-medium text-gray-900">
                                {key + 1}
                              </div>
                            </div>
                          </td>

                          <td className="px-6 py-4 whitespace-nowrap">
                            <div className="flex items-center">
                              <div className="text-md font-medium text-gray-900">
                                {val.quote_no}
                              </div>
                            </div>
                          </td>

                          <td className="px-7 py-4 whitespace-nowrap">
                            <div className="text-md text-gray-900">
                              {val.client.name}
                            </div>
                          </td>

                          <td className="px-7 py-4 whitespace-nowrap">
                            <div className="text-md text-gray-900">
                              {formatShortDate(val.quote_date)}
                            </div>
                          </td>

                          <td className="px-7 py-4 whitespace-nowrap">
                            <div className="text-md font-semibold text-gray-900">
                              <NumberFormat
                                value={parseFloat(val.total).toFixed(2)}
                                displayType={"text"}
                                thousandSeparator={true}
                                prefix={"$"}
                                renderText={(value) => <div>{value}</div>}
                              />
                            </div>
                          </td>

                          <td className="px-7 py-4 whitespace-nowrap">
                            <div className="text-md text-gray-900">
                              {val.description}
                            </div>
                          </td>

                          <td className="px-7 py-4 whitespace-nowrap text-md text-gray-500">
                            {quoteStatusStyle(val.status.toLowerCase())}
                          </td>

                          <td className="px-7 py-4 whitespace-nowrap text-right text-xsm flex justify-end items-center font-medium">
                            <button
                              className="text-gray-700 flex px-4 py-2 text-sm hover:text-brand-blue"
                              onClick={() => {
                                props.setUpdateId(val.quote_id);
                                props.setIsInfoOpen(true);
                              }}
                            >
                              <span data-for="view" data-tip="View">
                                <svg
                                  xmlns="http://www.w3.org/2000/svg"
                                  className="h-6 w-6"
                                  fill="none"
                                  viewBox="0 0 24 24"
                                  stroke="currentColor"
                                  strokeWidth="2"
                                >
                                  <path
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                    d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"
                                  />
                                  <path
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                    d="M2.458 12C3.732 7.943 7.523 5 12 5c4.478 0 8.268 2.943 9.542 7-1.274 4.057-5.064 7-9.542 7-4.477 0-8.268-2.943-9.542-7z"
                                  />
                                </svg>
                              </span>
                              <ReactTooltip
                                id="view"
                                className="custom-tooltip bg-gray-900"
                                textColor="#ffffff"
                                backgroundColor="#111827"
                                effect="solid"
                              />
                            </button>

                            <button
                              className="text-gray-700 flex px-4 py-2 text-sm hover:text-brand-blue"
                              onClick={() => {
                                props.setUpdateId(val.quote_id);
                                props.setIsOpen(true);
                              }}
                            >
                              <span data-for="edit" data-tip="Edit">
                                <svg
                                  xmlns="http://www.w3.org/2000/svg"
                                  className="h-6 w-6"
                                  fill="none"
                                  viewBox="0 0 24 24"
                                  stroke="currentColor"
                                  strokeWidth="2"
                                >
                                  <path
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                    d="M15.232 5.232l3.536 3.536m-2.036-5.036a2.5 2.5 0 113.536 3.536L6.5 21.036H3v-3.572L16.732 3.732z"
                                  />
                                </svg>
                              </span>
                              <ReactTooltip
                                id="edit"
                                className="custom-tooltip bg-gray-900"
                                textColor="#ffffff"
                                backgroundColor="#111827"
                                effect="solid"
                              />
                            </button>

                            {(val.status.toLowerCase() === "sent" ||
                              "draft") && (
                              <button
                                className="text-gray-700 flex px-4 py-2 text-sm hover:text-brand-blue"
                                onClick={() => {
                                  convertToInvoice(val);
                                }}
                              >
                                <span
                                  data-for="convert"
                                  data-tip="Convert To Invoice"
                                >
                                  <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    fill="none"
                                    viewBox="0 0 24 24"
                                    stroke-width="1.5"
                                    stroke="currentColor"
                                    className="w-6 h-6"
                                  >
                                    <path
                                      stroke-linecap="round"
                                      stroke-linejoin="round"
                                      d="M16.023 9.348h4.992v-.001M2.985 19.644v-4.992m0 0h4.992m-4.993 0l3.181 3.183a8.25 8.25 0 0013.803-3.7M4.031 9.865a8.25 8.25 0 0113.803-3.7l3.181 3.182m0-4.991v4.99"
                                    />
                                  </svg>
                                </span>
                                <ReactTooltip
                                  id="convert"
                                  className="custom-tooltip bg-gray-900"
                                  textColor="#ffffff"
                                  backgroundColor="#111827"
                                  effect="solid"
                                />
                              </button>
                            )}

                            <button
                              className="text-gray-700 flex px-4 py-2 text-sm hover:text-brand-blue"
                              onClick={() => {
                                setDeleteId(val.quote_id);
                                setActiveKey(key);
                                setConfirmation(true);
                              }}
                            >
                              <span data-for="delete" data-tip="Delete">
                                <svg
                                  xmlns="http://www.w3.org/2000/svg"
                                  className="h-6 w-6"
                                  fill="none"
                                  viewBox="0 0 24 24"
                                  stroke="currentColor"
                                  strokeWidth="2"
                                >
                                  <path
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                    d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"
                                  />
                                </svg>
                              </span>
                              <ReactTooltip
                                id="delete"
                                className="custom-tooltip bg-gray-900"
                                textColor="#ffffff"
                                backgroundColor="#111827"
                                effect="solid"
                              />
                            </button>
                          </td>
                        </tr>
                      ))
                    ) : (
                      <tr>
                        <td colspan="7" className="p-6" align="center">
                          <span>No records found.</span>
                        </td>
                      </tr>
                    )
                  ) : (
                    Array.from(Array(5), (e, i) => {
                      return (
                        <tr key={i}>
                          <td className="px-6 py-4 whitespace-nowrap">
                            <Skeleton />
                          </td>
                          <td className="px-7 py-4 whitespace-nowrap">
                            <Skeleton />
                          </td>
                          <td className="px-7 py-4 whitespace-nowrap">
                            <Skeleton />
                          </td>
                          <td className="px-7 py-4 whitespace-nowrap">
                            <Skeleton />
                          </td>
                          <td className="px-7 py-4 whitespace-nowrap">
                            <Skeleton />
                          </td>
                          <td className="px-7 py-4 whitespace-nowrap">
                            <Skeleton />
                          </td>
                          <td className="px-7 py-4 whitespace-nowrap">
                            <Skeleton />
                          </td>
                          <td className="px-7 py-4 whitespace-nowrap text-right text-xsm font-medium">
                            <Skeleton />
                          </td>
                        </tr>
                      );
                    })
                  )}
                </tbody>
              </table>
            </div>
          </div>
          {loader == false ? (
            <Pagination
              total={paginationObj.total}
              page={page}
              onPageChange={handleChangePage}
              rowsPerPage={rowsPerPage}
              onRowsPerPageChange={handleChangeRowsPerPage}
            />
          ) : null}
        </div>
      </div>
    </div>
  );
}
