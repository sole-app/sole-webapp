import { useState, useEffect } from "react";
import { Summary as SummaryQuote } from "../../services/api/quote.services";
import { getDateFromFYDates, formatCurrency } from "../../helpers/common";
import Skeleton from "react-loading-skeleton";
import "react-loading-skeleton/dist/skeleton.css";
import YearMonthFilter from "../Common/YearMonthFilter";

export default function QuoteAnalytic(props) {
  const [loader, setLoader] = useState(true);
  const [type, setType] = useState("YEAR"); //YEAR, MONTH
  const [summary, setSummary] = useState([]);

  const getData = async (filterType) => {
    setType(filterType);
    let apiRunning = true;
    setLoader(apiRunning);

    let dates = getDateFromFYDates(filterType);
    let formData = "";
    formData += "?start_date=" + dates.start_date;
    formData += "&end_date=" + dates.end_date;

    const res = await SummaryQuote(formData);
    if (res.success) {
      setSummary(res.data);
      apiRunning = false;
    }
    if (!apiRunning) {
      setLoader(false);
    }
  };

  useEffect(() => {
    getData(type);
  }, [props.refreshSummary]);

  return (
    <div className="mt-8 xl:pr-8 w-full  xl:w-1/2">
      <div className="grid grid-cols-1 h-full">
        <div className="bg-white overflow-hidden shadow rounded-lg">
          <div className="p-7 xl:p-8">
            <div className="hidden sm:block">
              <YearMonthFilter type={type} setType={getData} />
            </div>

            <div className="flex justify-between items-center mt-16  flex-1">
              {loader ? (
                <>
                  <dl className="pr-8 w-full text-center xl:text-left 2xl:w-1/2">
                    <dt className="text-lg tracking-wide pb-2 font-medium text-gray-400 truncate">
                      All Quotes
                    </dt>
                    <dd>
                      <div className="text-3xl 2xl:text-4xl whitespace-nowrap font-bold text-gray-900">
                        <Skeleton />
                      </div>
                    </dd>

                    <dd>
                      <span className="inline-flex items-center px-8 py-2 mt-5 rounded-full text-sm font-medium bg-green-100 text-green-800">
                        <Skeleton />
                      </span>
                    </dd>
                  </dl>

                  <div className="right-aside ">
                    <div className=" flex-col flex justify-between mt-1 space-y-3 2xl:space-y-4">
                      {/* Draft Amount */}
                      <dl className="">
                        <dt className="text-md tracking-wide pb-1 font-medium text-gray-400 truncate">
                          Draft
                        </dt>
                        <dd>
                          <div className="whitespace-nowrap font-bold text-gray-900 text-xl 2xl:text-2xl">
                            <Skeleton />
                          </div>
                        </dd>
                      </dl>

                      {/* Sent Amount */}
                      <dl className="">
                        <dt className="text-md tracking-wide pb-1 font-medium text-gray-400 truncate">
                          Sent
                        </dt>
                        <dd>
                          <div className="text-lg 2xl:text-2xl whitespace-nowrap font-bold text-brand-blue">
                            <Skeleton />
                          </div>
                        </dd>
                      </dl>
                    </div>
                  </div>
                </>
              ) : (
                <>
                  {summary.total_draft_amount + summary.total_sent_amount >
                  0 ? (
                    <>
                      <dl className="pr-8 w-full text-center xl:text-left 2xl:w-1/2">
                        <dt className="text-lg tracking-wide pb-2 font-medium text-gray-400 truncate">
                          All Quotes
                        </dt>
                        <dd>
                          <div className="text-3xl 2xl:text-4xl whitespace-nowrap font-bold text-gray-900">
                            {formatCurrency(
                              summary.total_draft_amount +
                                summary.total_sent_amount
                            )}
                          </div>
                        </dd>
                        <dd>
                          <span className="inline-flex items-center px-8 py-2 mt-5 rounded-full text-sm font-medium bg-green-100 text-green-800">
                            {summary.has_previous_data ? (
                              <>
                                {summary.quote_gain < 0 ? "-" : "+"}
                                {summary.quote_gain}% Since Last{" "}
                                {type == "YEAR" ? "Year" : "Month"}
                              </>
                            ) : (
                              <>
                                {`No comparison for last ${
                                  type == "YEAR" ? "year" : "month"
                                }`}
                              </>
                            )}
                          </span>
                        </dd>
                      </dl>

                      <div className="right-aside ">
                        <div className=" flex-col flex justify-between mt-1 space-y-3 2xl:space-y-4">
                          {/* Draft Amount */}
                          <dl className="">
                            <dt className="text-md tracking-wide pb-1 font-medium text-gray-400 truncate">
                              Draft
                            </dt>
                            <dd>
                              <div className="whitespace-nowrap font-bold text-gray-900 text-xl 2xl:text-2xl">
                                {formatCurrency(summary.total_draft_amount)}
                              </div>
                            </dd>
                          </dl>

                          {/* Sent Amount */}
                          <dl className="">
                            <dt className="text-md tracking-wide pb-1 font-medium text-gray-400 truncate">
                              Sent
                            </dt>
                            <dd>
                              <div className="text-lg 2xl:text-2xl whitespace-nowrap font-bold text-brand-blue">
                                {formatCurrency(summary.total_sent_amount)}
                              </div>
                            </dd>
                          </dl>
                        </div>
                      </div>
                    </>
                  ) : (
                    <>
                      <div className="block justify-between mt-16  flex-1 2xl:flex text-center">
                        No quotes found for this{" "}
                        {type === "YEAR" ? "year" : "month"}
                      </div>
                    </>
                  )}
                </>
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
