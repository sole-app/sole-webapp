import { useState, useEffect } from "react";
import SendToSection from "./SendToSection";
import { getAuditor } from "../../services/api/auditor.services";
import {
  getUserData,
  getAuditorData,
  setAuditorData,
} from "../../services/cookies.services";
import Link from "next/link";

import {
  CollectionIcon,
  CalculatorIcon,
  NewspaperIcon,
  DocumentTextIcon,
  CurrencyDollarIcon,
  DocumentReportIcon,
} from "@heroicons/react/outline";

// The reporting suite has been designed to help you forecast your cash-flows and understand your business finances. It is still recommended to use a qualified tax agent to help you complete your tax returns and discuss any major decisions with relevant experts.

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

export default function ReportSidebar(props) {
  const [loader, setLoader] = useState(false);
  const [auditor, setAuditor] = useState({});
  const [userData, setUserData] = useState({});
  const [index, setIndex] = useState(0);

  const { reportType, title } = props;

  const subNavigation = [
    {
      name: "Assets Listing",
      description: "Shows details of all your assets",
      href: "assets",
      icon: CollectionIcon,
      current: reportType === "assets",
    },
    {
      name: "Transaction Listing",
      description:
        "Shows details about your business and personal transactions, including description, amount, date, category, and more.",
      href: "transaction-listing",
      icon: NewspaperIcon,
      current: reportType === "transaction-listing",
    },
    {
      name: "GST Extract",
      description: "Details of GST Extract",
      href: "gst-extract",
      icon: CalculatorIcon,
      current: reportType === "gst-extract",
    },
    {
      name: "Profit and Loss",
      description:
        "Shows how much you made and spent so you can see how profitable you are. Also called a P&L or income statement.",
      href: "profit-loss",
      icon: CurrencyDollarIcon,
      current: reportType === "profit-loss",
    },
    /*{
      name: 'Balance Sheet',
      description: 'Snapshot of what your business owns or is due to receive from others (assets), what it owes to others (liabilities), and what you\'ve invested or retained in your business (equity).',
      href: 'balance-sheet',
      icon: DocumentTextIcon,
      current: (reportType === 'balance-sheet')
    },
    {
      name: 'Consolidated Report',
      description: 'All reports in single file',
      href: 'consolidated-report',
      icon: DocumentReportIcon,
      current: (reportType === 'consolidated-report')
    },*/
  ];

  useEffect(async () => {
    let user = getUserData();
    setLoader(true);
    if (Object.keys(userData).length === 0) {
      setUserData(user);
    }

    if (Object.keys(auditor).length === 0) {
      await getAuditors();
    }

    let indexData = 0;
    if (reportType === "assets") {
      indexData = 0;
    }
    if (reportType === "transaction-listing") {
      indexData = 1;
    } else if (reportType === "gst-extract") {
      indexData = 2;
    } else if (reportType === "profit-loss") {
      indexData = 3;
    } else if (reportType === "balance-sheet") {
      indexData = 4;
    } else if (reportType === "tax-summary") {
      indexData = 0;
    } else if (reportType === "tax-details") {
      indexData = 0;
    } else if (reportType === "consolidated-report") {
      indexData = 5;
    }
    setIndex(indexData);

    setLoader(false);
  }, [reportType]);

  const setAccountantBookkeeper = async () => {
    const auditorData = getAuditorData();

    if (auditorData !== null) {
      setAuditor(auditorData);
    }
  };

  // Fetch Audito Info
  const getAuditorInfo = async () => {
    const formData = new FormData();
    const result = await getAuditor(formData);
    if (result.success) {
      setAuditorData(result.data.auditor_details);
      await setAccountantBookkeeper();
    }
  };

  const getAuditors = async () => {
    let data = getAuditorData();
    if (typeof data !== "undefined" && data !== null) {
      setAccountantBookkeeper();
    } else {
      getAuditorInfo();
    }
  };

  return (
    <div className="h-screen flex bg-blue-gray-50 overflow-hidden">
      <div className="flex-1 flex xl:overflow-hidden">
        <nav
          aria-label="Sections"
          className=" flex-shrink-0 w-96 bg-white border-l border-r border-blue-gray-200 flex flex-col"
        >
          <div className="flex-1 min-h-0 overflow-y-auto">
            {subNavigation.map((item) => (
              <Link key={item.name} href={item.href}>
                <a
                  key={item.name}
                  className={classNames(
                    item.current
                      ? "bg-blue-50 bg-opacity-50"
                      : "hover:bg-blue-50 hover:bg-opacity-50",
                    "flex items-center p-6 border-b border-blue-gray-200"
                  )}
                  aria-current={item.current ? "page" : undefined}
                >
                  <item.icon
                    className="flex-shrink-0 -mt-0.5 h-6 w-6 text-blue-gray-400"
                    aria-hidden="true"
                  />

                  <div className="ml-3">
                    <p className="font-medium  text-md text-blue-gray-900">
                      {item.name}
                    </p>
                    <p className="mt-1 text-sm text-blue-gray-500">
                      {item.description}
                    </p>
                  </div>
                </a>
              </Link>
            ))}
          </div>
        </nav>

        {/* Identify view to load based on settingType passed in props */}
        {!loader ? (
          <>
            <SendToSection
              title={subNavigation[index].name}
              description={subNavigation[index].description}
              reportType={reportType}
              userData={userData}
              auditor={auditor}
            />
          </>
        ) : (
          ""
        )}
      </div>
    </div>
  );
}
