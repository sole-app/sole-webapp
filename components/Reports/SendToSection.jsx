import { useEffect, useState, Fragment } from "react";
import { Switch, Listbox, Transition } from "@headlessui/react";
import { useFormik } from "formik";
import ReportSchema from "../../schemas/Report.schema";
import { CheckIcon, SelectorIcon } from "@heroicons/react/solid";
import { sendReport } from "../../services/api/report.services";
import { InformationCircleIcon } from "@heroicons/react/outline";
import { getFinancialYears } from "../../services/api/taxCalculator.services";
import ReactTooltip from "react-tooltip";
import AlertDialog from "../AlertDialog";

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

export default function SendToSection(props) {
  const { title, description, reportType, userData, auditor } = props;
  const [financialYear, setFinancialYear] = useState();
  const [yearList, setyearList] = useState([]);
  const [openAlert, setOpenAlert] = useState(false);
  const [msg, setMsg] = useState("");
  const [alertType, setAlertType] = useState("Error");

  const getList = async () => {
    const form_data = new FormData();
    const result = await getFinancialYears(form_data);

    if (result.success) {
      let temp = [];
      result.data.financialYear.map((v) =>
        temp.push({ id: v.id, currentyear: v.start_year + "-" + v.end_year })
      );
      setyearList(temp);
      setFinancialYear(temp[temp.length - 1]);
    }
  };

  const formik = useFormik({
    initialValues: {
      sendTo: typeof userData.email !== "undefined" ? userData.email : "",
      accountantEnable: false,
      accountantName:
        auditor != null
          ? typeof auditor.accountant !== "undefined" &&
            typeof auditor.accountant.name !== "undefined"
            ? auditor.accountant.name
            : ""
          : "",
      accountantEmail:
        auditor != null
          ? typeof auditor.accountant !== "undefined" &&
            typeof auditor.accountant.email !== "undefined"
            ? auditor.accountant.email
            : ""
          : "",
      bookkeeperEnable: false,
      bookkeeperName:
        auditor != null
          ? typeof auditor.bookkeeper !== "undefined" &&
            typeof auditor.bookkeeper.name !== "undefined"
            ? auditor.bookkeeper.name
            : ""
          : "",
      bookkeeperEmail:
        auditor != null
          ? typeof auditor.bookkeeper !== "undefined" &&
            typeof auditor.bookkeeper.email !== "undefined"
            ? auditor.bookkeeper.email
            : ""
          : "",
    },
    validationSchema: ReportSchema,
    onSubmit: (values, { setSubmitting }) => {
      handleFormSubmit(values, setSubmitting);
    },
  });

  const renderErrorMessage = (field) => {
    return (
      formik.touched[field] && (
        <div className="error_text">{formik.errors[field]}</div>
      )
    );
  };

  const handleFormSubmit = async (values, setSubmitting) => {
    const formData = new FormData();
    if (reportType === "transaction-listing") {
      formData.append("report_type_id", 4);
      //add dates
      if (
        typeof financialYear === "undefined" ||
        typeof financialYear.id === "undefined"
      ) {
        setSubmitting(false);
        return;
      }
      formData.append("financial_year_id", financialYear.id);
    } else if (reportType === "assets") {
      formData.append("report_type_id", 3);
    } else if (reportType === "profit-loss") {
      formData.append("report_type_id", 1);
      //add dates
      if (
        typeof financialYear === "undefined" ||
        typeof financialYear.id === "undefined"
      ) {
        setSubmitting(false);
        return;
      }
      formData.append("financial_year_id", financialYear.id);
    } else if (reportType === "balance-sheet") {
      formData.append("report_type_id", 2);
    } else if (reportType === "gst-extract") {
      formData.append("report_type_id", 7);
      //add dates
      if (
        typeof financialYear === "undefined" ||
        typeof financialYear.id === "undefined"
      ) {
        setSubmitting(false);
        return;
      }
      formData.append("financial_year_id", financialYear.id);
    } else if (reportType === "tax-summary") {
      formData.append("report_type_id", 5);
    } else if (reportType === "tax-details") {
      formData.append("report_type_id", 6);
    } else if (reportType === "consolidated-report") {
      formData.append("report_type_id", 8);
    }
    formData.append("current_financial_year", 1);
    formData.append("send_to", values.sendTo);

    if (values.accountantEnable) {
      formData.append("accountant_name", values.accountantName);
      formData.append("accountant_email", values.accountantEmail);
    }

    if (values.bookkeeperEnable) {
      formData.append("bookkeeper_name", values.bookkeeperName);
      formData.append("bookkeeper_email", values.bookkeeperEmail);
    }

    let result = await sendReport(formData);

    if (result.success) {
      setSubmitting(false);
      formik.resetForm();

      setAlertType("Success");
      setMsg(result.message);
      setOpenAlert(true);
    } else {
      setSubmitting(false);

      setAlertType("Error");
      setMsg(result.message);
      setOpenAlert(true);
    }
  };

  useEffect(async () => {
    if (
      (reportType == "transaction-listing" ||
        reportType == "gst-extract" ||
        reportType === "profit-loss") &&
      yearList.length === 0
    ) {
      getList();
    }
    formik.initialValues.sendTo =
      typeof userData.email !== "undefined" ? userData.email : "";
    formik.initialValues.accountantName =
      auditor != null
        ? typeof auditor.accountant !== "undefined" &&
          typeof auditor.accountant.name !== "undefined"
          ? auditor.accountant.name
          : ""
        : "";
    formik.initialValues.accountantEmail =
      auditor != null
        ? typeof auditor.accountant !== "undefined" &&
          typeof auditor.accountant.email !== "undefined"
          ? auditor.accountant.email
          : ""
        : "";
    formik.initialValues.bookkeeperName =
      auditor != null
        ? typeof auditor.bookkeeper !== "undefined" &&
          typeof auditor.bookkeeper.name !== "undefined"
          ? auditor.bookkeeper.name
          : ""
        : "";
    formik.initialValues.bookkeeperEmail =
      auditor != null
        ? typeof auditor.bookkeeper !== "undefined" &&
          typeof auditor.bookkeeper.email !== "undefined"
          ? auditor.bookkeeper.email
          : ""
        : "";
  }, [auditor, userData, reportType]);

  return (
    <>
      <AlertDialog
        id="ringtone-menu2"
        title={alertType}
        message={msg}
        keepMounted
        open={openAlert}
        onClose={setOpenAlert}
      />
      <div className="flex-1 max-h-screen ">
        <div className="flex-1  max-h-screen hidden-scroll overflow-y-auto 2xl:mt-10">
          <div className="max-w-6xl overflow-hidden mx-auto pb-20 px-4  sm:px-6  lg:px-8">
            <form
              onSubmit={formik.handleSubmit}
              className="mt-6 space-y-8 divide-y-blue-gray-200"
            >
              <div className="grid grid-cols-2 gap-y-6 sm:grid-cols-6 sm:gap-x-6">
                <div className="sm:col-span-6">
                  <h1 className="text-3xl flex items-center justify-content-between font-extrabold text-gray-900">
                    {title}
                    <InformationCircleIcon
                      className="h-6 w-6 text-gray-400 ml-2  focus:outline-none"
                      data-for="info1"
                      data-tip
                    />
                  </h1>
                  <p className="mt-1 text-sm text-blue-gray-500">
                    {description}
                  </p>
                  <ReactTooltip
                    id="info1"
                    className="custom-tooltip bg-gray-900"
                    textColor="#ffffff"
                    backgroundColor="#111827"
                    effect="solid"
                    aria-haspopup="true"
                  >
                    <p className="w-64">
                      <b>Reporting Suite</b> <br></br>
                      <br></br>The reporting suite has been designed to help you
                      forecast your cash-flows and understand your business
                      finances. It is still recommended to use a qualified tax
                      agent to help you complete your tax returns and disucss
                      any major decisions with relevant experts.
                    </p>
                  </ReactTooltip>
                </div>
              </div>

              {(reportType == "transaction-listing" ||
                reportType == "gst-extract" ||
                reportType === "profit-loss") && (
                <div>
                  <div className="sm:col-span-2">
                    <label
                      htmlFor="sendTo"
                      className="block text-md tracking-wider font-medium text-gray-500"
                    >
                      Time Frame
                    </label>
                    <div className="mt-1">
                      <Listbox
                        value={financialYear}
                        onChange={setFinancialYear}
                      >
                        {({ open }) => (
                          <>
                            <div className="mt-1 relative">
                              <Listbox.Button className="bg-white  relative w-96 border-2 border-gray-300 text-gray-500 rounded-md shadow-sm pl-4 pr-11 py-3 text-left cursor-default focus:outline-none focus:ring-1 focus:ring-indigo-500 focus:border-indigo-500 sm:text-base">
                                <span className="block truncate">
                                  {financialYear
                                    ? financialYear.currentyear
                                    : ""}
                                </span>
                                <span className="absolute inset-y-0 right-0 flex items-center pr-2 pointer-events-none">
                                  <SelectorIcon
                                    className="h-5 w-5  text-gray-400"
                                    aria-hidden="true"
                                  />
                                </span>
                              </Listbox.Button>

                              <Transition
                                show={open}
                                as={Fragment}
                                leave="transition ease-in duration-100"
                                leaveFrom="opacity-100"
                                leaveTo="opacity-0"
                              >
                                <Listbox.Options
                                  static
                                  className="absolute z-10 mt-1 w-full bg-white shadow-lg max-h-60 rounded-md py-1 text-base ring-1 ring-black ring-opacity-5 overflow-auto focus:outline-none sm:text-md"
                                >
                                  {yearList.map((year) => (
                                    <Listbox.Option
                                      key={year.id}
                                      className={({ active }) =>
                                        classNames(
                                          active
                                            ? "text-white bg-brand-blue"
                                            : "text-gray-900",
                                          "cursor-default select-none relative py-2 pl-3 pr-9"
                                        )
                                      }
                                      value={year}
                                    >
                                      {({ selected, active }) => (
                                        <>
                                          <span
                                            className={classNames(
                                              selected
                                                ? "font-semibold"
                                                : "font-normal",
                                              "block truncate"
                                            )}
                                          >
                                            {year.currentyear}
                                          </span>

                                          {selected ? (
                                            <span
                                              className={classNames(
                                                active
                                                  ? "text-white"
                                                  : "text-brand-blue",
                                                "absolute inset-y-0 right-0 flex items-center pr-4"
                                              )}
                                            >
                                              <CheckIcon
                                                className="h-5 w-5"
                                                aria-hidden="true"
                                              />
                                            </span>
                                          ) : null}
                                        </>
                                      )}
                                    </Listbox.Option>
                                  ))}
                                </Listbox.Options>
                              </Transition>
                            </div>
                          </>
                        )}
                      </Listbox>
                    </div>
                  </div>
                </div>
              )}

              <div>
                <div className="sm:col-span-2">
                  <label
                    htmlFor="sendTo"
                    className="block text-md tracking-wider font-medium text-gray-500"
                  >
                    Send To
                  </label>
                  <div className="mt-1">
                    <input
                      type="email"
                      name="sendTo"
                      id="sendTo"
                      className={
                        "placeholder-gray-400 py-3 px-4 block w-full border-2 border-gray-200  focus:ring-indigo-400 focus:border-indigo-400 border-gray-100 rounded-md"
                      }
                      variant="outlined"
                      value={formik.values.sendTo}
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                      placeholder="Send To"
                    />
                    {renderErrorMessage("sendTo")}
                  </div>
                </div>
              </div>
              <div>
                <div className="mt-1">
                  <div className="bg-gray-100 p-1 mt-4 mb-2 relative shadow sm:rounded-lg">
                    <Switch.Group
                      as="div"
                      className="px-4 flex items-center justify-between py-5 sm:p-6"
                    >
                      <div>
                        <Switch.Label
                          as="h3"
                          className="text-lg leading-6 font-medium text-gray-700"
                          passive
                        >
                          Send this report to your accountant?
                        </Switch.Label>
                      </div>

                      <div className="flex items-center">
                        <Switch
                          checked={formik.values.accountantEnable}
                          onChange={() =>
                            formik.setFieldValue(
                              "accountantEnable",
                              !formik.values.accountantEnable
                            )
                          }
                          className={classNames(
                            formik.values.accountantEnable
                              ? "bg-green-600"
                              : "bg-gray-300",
                            "relative inline-flex flex-shrink-0 h-6 w-11 border-2 border-transparent rounded-full cursor-pointer transition-colors ease-in-out duration-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                          )}
                        >
                          <span
                            className={classNames(
                              formik.values.accountantEnable
                                ? "translate-x-5"
                                : "translate-x-0",
                              "pointer-events-none relative inline-block h-5 w-5 rounded-full bg-white shadow transform ring-0 transition ease-in-out duration-200"
                            )}
                          >
                            <span
                              className={classNames(
                                formik.values.accountantEnable
                                  ? "opacity-0 ease-out duration-100"
                                  : "opacity-100 ease-in duration-200",
                                "absolute inset-0 h-full w-full flex items-center justify-center transition-opacity pointer-events-none"
                              )}
                              aria-hidden="true"
                            >
                              <svg
                                className="h-3 w-3 text-gray-400"
                                fill="none"
                                viewBox="0 0 12 12"
                              >
                                <path
                                  d="M4 8l2-2m0 0l2-2M6 6L4 4m2 2l2 2"
                                  stroke="currentColor"
                                  strokeWidth={2}
                                  strokeLinecap="round"
                                  strokeLinejoin="round"
                                />
                              </svg>
                            </span>
                            <span
                              className={classNames(
                                formik.values.accountantEnable
                                  ? "opacity-100 ease-in duration-200"
                                  : "opacity-0 ease-out duration-100",
                                "absolute inset-0 h-full w-full flex items-center justify-center transition-opacity pointer-events-none"
                              )}
                              aria-hidden="true"
                            >
                              <svg
                                className="h-3 w-3 text-green-600"
                                fill="currentColor"
                                viewBox="0 0 12 12"
                              >
                                <path d="M3.707 5.293a1 1 0 00-1.414 1.414l1.414-1.414zM5 8l-.707.707a1 1 0 001.414 0L5 8zm4.707-3.293a1 1 0 00-1.414-1.414l1.414 1.414zm-7.414 2l2 2 1.414-1.414-2-2-1.414 1.414zm3.414 2l4-4-1.414-1.414-4 4 1.414 1.414z" />
                              </svg>
                            </span>
                          </span>
                        </Switch>
                      </div>
                    </Switch.Group>

                    <div className="lg:flex px-4">
                      {formik.values.accountantEnable ? (
                        <div className="px-2 pb-4 lg:w-6/12">
                          <div className="sm:col-span-2">
                            <label
                              htmlFor="accountantName"
                              className="block text-md tracking-wider font-medium text-gray-500"
                            >
                              Accountant Name
                            </label>
                            <div className="mt-1">
                              <input
                                type="text"
                                name="accountantName"
                                id="accountantName"
                                className={
                                  "placeholder-gray-400 py-3 px-4 block w-full border-2 border-gray-200  focus:ring-indigo-400 focus:border-indigo-400 border-gray-100 rounded-md"
                                }
                                variant="outlined"
                                value={formik.values.accountantName}
                                onChange={formik.handleChange}
                                onBlur={formik.handleBlur}
                                placeholder="Accountant Name"
                              />
                              {renderErrorMessage("accountantName")}
                            </div>
                          </div>
                        </div>
                      ) : null}

                      {formik.values.accountantEnable ? (
                        <div className="px-2 pb-4 lg:w-6/12">
                          <div className="sm:col-span-2">
                            <label
                              htmlFor="accountantEmail"
                              className="block text-md tracking-wider font-medium text-gray-500"
                            >
                              Accountant Email
                            </label>
                            <div className="mt-1">
                              <input
                                type="email"
                                name="accountantEmail"
                                id="accountantEmail"
                                className={
                                  "placeholder-gray-400 py-3 px-4 block w-full border-2 border-gray-200  focus:ring-indigo-400 focus:border-indigo-400 border-gray-100 rounded-md"
                                }
                                variant="outlined"
                                value={formik.values.accountantEmail}
                                onChange={formik.handleChange}
                                onBlur={formik.handleBlur}
                                placeholder="Accountant Email"
                              />
                              {renderErrorMessage("accountantEmail")}
                            </div>
                          </div>
                        </div>
                      ) : null}
                    </div>
                  </div>
                </div>
              </div>

              <div>
                <div className="mt-1">
                  <div className="bg-gray-100 p-1 mt-4 mb-2 relative shadow sm:rounded-lg">
                    <Switch.Group
                      as="div"
                      className="px-4 flex items-center justify-between py-5 sm:p-6"
                    >
                      <div>
                        <Switch.Label
                          as="h3"
                          className="text-lg leading-6 font-medium text-gray-700"
                          passive
                        >
                          Send this report to your bookkeeper?
                        </Switch.Label>
                      </div>

                      <div className="flex items-center">
                        <Switch
                          checked={formik.values.bookkeeperEnable}
                          onChange={() =>
                            formik.setFieldValue(
                              "bookkeeperEnable",
                              !formik.values.bookkeeperEnable
                            )
                          }
                          className={classNames(
                            formik.values.bookkeeperEnable
                              ? "bg-green-600"
                              : "bg-gray-300",
                            "relative inline-flex flex-shrink-0 h-6 w-11 border-2 border-transparent rounded-full cursor-pointer transition-colors ease-in-out duration-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                          )}
                        >
                          <span
                            className={classNames(
                              formik.values.bookkeeperEnable
                                ? "translate-x-5"
                                : "translate-x-0",
                              "pointer-events-none relative inline-block h-5 w-5 rounded-full bg-white shadow transform ring-0 transition ease-in-out duration-200"
                            )}
                          >
                            <span
                              className={classNames(
                                formik.values.bookkeeperEnable
                                  ? "opacity-0 ease-out duration-100"
                                  : "opacity-100 ease-in duration-200",
                                "absolute inset-0 h-full w-full flex items-center justify-center transition-opacity pointer-events-none"
                              )}
                              aria-hidden="true"
                            >
                              <svg
                                className="h-3 w-3 text-gray-400"
                                fill="none"
                                viewBox="0 0 12 12"
                              >
                                <path
                                  d="M4 8l2-2m0 0l2-2M6 6L4 4m2 2l2 2"
                                  stroke="currentColor"
                                  strokeWidth={2}
                                  strokeLinecap="round"
                                  strokeLinejoin="round"
                                />
                              </svg>
                            </span>
                            <span
                              className={classNames(
                                formik.values.bookkeeperEnable
                                  ? "opacity-100 ease-in duration-200"
                                  : "opacity-0 ease-out duration-100",
                                "absolute inset-0 h-full w-full flex items-center justify-center transition-opacity pointer-events-none"
                              )}
                              aria-hidden="true"
                            >
                              <svg
                                className="h-3 w-3 text-green-600"
                                fill="currentColor"
                                viewBox="0 0 12 12"
                              >
                                <path d="M3.707 5.293a1 1 0 00-1.414 1.414l1.414-1.414zM5 8l-.707.707a1 1 0 001.414 0L5 8zm4.707-3.293a1 1 0 00-1.414-1.414l1.414 1.414zm-7.414 2l2 2 1.414-1.414-2-2-1.414 1.414zm3.414 2l4-4-1.414-1.414-4 4 1.414 1.414z" />
                              </svg>
                            </span>
                          </span>
                        </Switch>
                      </div>
                    </Switch.Group>

                    <div className="lg:flex px-4">
                      {formik.values.bookkeeperEnable ? (
                        <div className="px-2 pb-4 lg:w-6/12">
                          <div className="sm:col-span-2">
                            <label
                              htmlFor="bookkeeperName"
                              className="block text-md tracking-wider font-medium text-gray-500"
                            >
                              Bookkeeper Name
                            </label>
                            <div className="mt-1">
                              <input
                                type="text"
                                name="bookkeeperName"
                                id="bookkeeperName"
                                className={
                                  "placeholder-gray-400 py-3 px-4 block w-full border-2 border-gray-200  focus:ring-indigo-400 focus:border-indigo-400 border-gray-100 rounded-md"
                                }
                                variant="outlined"
                                value={formik.values.bookkeeperName}
                                onChange={formik.handleChange}
                                onBlur={formik.handleBlur}
                                placeholder="Bookkeeper Name"
                              />
                              {renderErrorMessage("bookkeeperName")}
                            </div>
                          </div>
                        </div>
                      ) : null}

                      {formik.values.bookkeeperEnable ? (
                        <div className="px-2 pb-4 lg:w-6/12">
                          <div className="sm:col-span-2">
                            <label
                              htmlFor="bookkeeperEmail"
                              className="block text-md tracking-wider font-medium text-gray-500"
                            >
                              Bookkeeper Email
                            </label>
                            <div className="mt-1">
                              <input
                                type="email"
                                name="bookkeeperEmail"
                                id="bookkeeperEmail"
                                className={
                                  "placeholder-gray-400 py-3 px-4 block w-full border-2 border-gray-200  focus:ring-indigo-400 focus:border-indigo-400 border-gray-100 rounded-md"
                                }
                                variant="outlined"
                                value={formik.values.bookkeeperEmail}
                                onChange={formik.handleChange}
                                onBlur={formik.handleBlur}
                                placeholder="Bookkeeper Email"
                              />
                              {renderErrorMessage("bookkeeperEmail")}
                            </div>
                          </div>
                        </div>
                      ) : null}
                    </div>
                  </div>
                </div>
              </div>

              <div className="pt-8 flex justify-end">
                <button
                  type="submit"
                  className="ml-4 inline-flex justify-center py-2 px-4 tracking-wider border border-transparent uppercase shadow-sm  font-medium text-sm rounded-full text-white bg-green-600 hover:bg-green-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-500"
                  disabled={formik.isSubmitting}
                >
                  {formik.isSubmitting ? "Please wait..." : "Send Report"}
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </>
  );
}
