import { useState, useEffect } from 'react'
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Dialog from '@material-ui/core/Dialog';

export default function AlertDialog(props) {
    const [title, setTitle] = useState('Error');
    const [message, setMessage] = useState('Something going wrong');

    const handleClose = () => {
        props.onClose(false);
    };

    const navigateToProfile = () => {
        props.onClose(false);
        // navigate to /settings/profile screen
        // e.preventDefault();
        window.location.href = '/settings/profile';
    };

    useEffect(async () => {
        if (props.open) {
            setTitle(props.title || title);
            setMessage(props.message || message);
        }
    }, [props.open]);


    return (
        <Dialog
            sx={{ '& .MuiDialog-paper': { width: '80%', maxHeight: 435 } }}
            maxWidth="xs"
            disablePortal
            open={props.open}
        >
            <DialogTitle>{title}</DialogTitle>
            <DialogContent dividers>{message}</DialogContent>
            <DialogActions>
                {props.missingInfo === true ?
                    <>
                        <button onClick={navigateToProfile} className="btn-yes">Update Profile</button>
                        <button onClick={handleClose} className="btn-no">Later</button>
                    </>
                    :
                    <>
                        <button onClick={handleClose} className="btn-yes">Ok</button>
                    </>
                }
            </DialogActions>
        </Dialog>
    );
}