import React from "react";
import { Fragment, useState, useEffect } from "react";
import { Menu, Transition } from "@headlessui/react";
import { LogoutIcon, BellIcon, UserCircleIcon } from "@heroicons/react/outline";
import { ChevronDownIcon } from "@heroicons/react/solid";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Button from "@material-ui/core/Button";
import { useRouter } from "next/router";
import ExpireTrial from "../Subscription/ExpireTrial";
import FreeTrial from "../Subscription/FreeTrial";
import { logoutUser, notification } from "../../services/api/user.services";
import { getUserData } from "../../services/cookies.services";
import Link from "next/link";
``;
import NotificationSider from "../../components/Notification/NotificationSider";
import store from "../../store/store";
import { useSelector } from "react-redux";

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

export default function TopBar(props) {
  const router = useRouter();
  const storeData = store.store.getState();
  const [userData, setUserData] = useState(getUserData());
  const [dialogOpen, setDialogOpen] = useState(false);
  const [notificationCount, setNotificationCount] = useState(0);
  const [notifications, setNotifications] = useState(false);
  const { title, user = {} } = props;
  const [open, setOpen] = useState(false);
  const [updateId, setUpdateId] = useState("");
  const [subData, setSubData] = useState({});
  const refreshSubscription = useSelector(
    (state) => state.subscription.refreshSubscription
  );

  useEffect(() => {
    if (Object.keys(user).length > 0) {
      setUserData(getUserData());
    }
  }, [user]);

  useEffect(async () => {
    if (!notifications) {
      const res = await notification(`?page_size=6&page=1`);
      if (res.success) {
        setNotificationCount(res.data.notification_details.length);
        setNotifications(true);
      }
    }
  }, [notifications]);

  useEffect(async () => {
    if (
      typeof storeData.user_details !== "undefined" &&
      typeof storeData.user_details.subscription !== "undefined"
    ) {
      setSubData(storeData.user_details.subscription);
    }
  }, [storeData, refreshSubscription]);

  return (
    <div className="flex bg-gray-100">
      {/* Static TopBar */}
      <div className="flex-1 focus:outline-none">
        <div className="relative z-10 flex-shrink-0 flex h-16 bg-white border-b border-gray-200 lg:border-none">
          <div className="flex-1 px-2 flex justify-between  lg:mx-auto">
            <div className="flex-1 flex w-8/12">
              {/* <h1 className="text-2xl p-3 font-extrabold tracking-wide whitespace-nowrap text-gray-500 sm:text-3xl">
                {title}
              </h1> */}

              {typeof subData.subscription_type !== "undefined" &&
                subData.subscription_type === "NO_SUBSCRIPTION" && (
                  <>
                    {typeof subData.is_free !== "undefined" &&
                    subData.is_free ? (
                      <FreeTrial days={subData.free_day || 30} />
                    ) : (
                      <ExpireTrial />
                    )}
                  </>
                )}
            </div>

            <div className="ml-4 flex items-center md:ml-6">
              <button
                className="bg-white p-2 rounded-full text-gray-400 hover:bg-gray-100  focus:outline-none focus:ring-2 focus:ring-gray"
                onClick={() => {
                  setUpdateId("");
                  setOpen(!open);
                }}
              >
                <div className="relative">
                  <span className="sr-only">View notifications</span>
                  <BellIcon className="h-6 w-6" aria-hidden="true" />

                  {notificationCount > 0 && (
                    <span className="block w-1.5 h-1.5 rounded-full bg-red-500 absolute top-0 -right-0.5"></span>
                  )}
                </div>
              </button>

              {/* Profile dropdown */}
              <Menu as="div" className="ml-3 relative">
                {({ open }) => (
                  <>
                    <div>
                      <Menu.Button className="max-w-xs bg-white rounded-full flex items-center text-sm focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-cyan-500 lg:p-2 lg:rounded-md lg:hover:bg-gray-50">
                        {userData &&
                        userData.profile_pic !== "" &&
                        userData.profile_pic !== null ? (
                          <img
                            className="h-8 w-8 rounded-full"
                            src={
                              userData &&
                              userData.profile_pic !== "" &&
                              userData.profile_pic !== null
                                ? userData.profile_pic
                                : "/images/avatar/1.jpg"
                            }
                            alt="User"
                            width="34"
                            height="34"
                          />
                        ) : (
                          <UserCircleIcon
                            className={classNames(
                              "w-7 h-7 mr-2 text-gray-400 group-hover:text-white"
                            )}
                          />
                        )}
                        <span className="hidden ml-3 text-gray-700 text-sm font-medium lg:block">
                          <span className="sr-only">Open user menu for </span>
                          {userData ? userData.full_name : ""}
                        </span>
                        <ChevronDownIcon
                          className="hidden flex-shrink-0 ml-1 h-5 w-5 text-gray-400 lg:block"
                          aria-hidden="true"
                        />
                      </Menu.Button>
                    </div>
                    <Transition
                      show={open}
                      as={Fragment}
                      enter="transition ease-out duration-100"
                      enterFrom="transform opacity-0 scale-95"
                      enterTo="transform opacity-100 scale-100"
                      leave="transition ease-in duration-75"
                      leaveFrom="transform opacity-100 scale-100"
                      leaveTo="transform opacity-0 scale-95"
                    >
                      <Menu.Items
                        static
                        className="origin-top-right absolute right-0 mt-2 w-48 rounded-md shadow-lg p-1 bg-white ring-1 ring-black ring-opacity-5 focus:outline-none"
                      >
                        <Menu.Item>
                          {({ active }) => (
                            <Link href="/settings/profile">
                              <a
                                className={classNames(
                                  active ? "bg-brand-darkblue text-white" : "",
                                  " p-2.5 text-sm lex rounded-md flex items-center w-full text-gray-700 hover:bg-brand-blue hover:text-white"
                                )}
                              >
                                <span className="text-indigo-400">
                                  <UserCircleIcon
                                    className={classNames(
                                      active ? " text-white" : "",
                                      "w-6 h-6 mr-2 group-hover:text-white"
                                    )}
                                  />
                                </span>
                                Your Profile
                              </a>
                            </Link>
                          )}
                        </Menu.Item>
                        <Menu.Item>
                          {({ active }) => (
                            <a
                              href="#"
                              onClick={() => setDialogOpen(true)}
                              className={classNames(
                                active ? "bg-brand-darkblue text-white" : "",
                                " p-2.5 text-sm lex rounded-md flex items-center w-full text-gray-700 hover:bg-brand-blue hover:text-white"
                              )}
                            >
                              <span className="text-indigo-400">
                                <LogoutIcon
                                  className={classNames(
                                    active ? " text-white" : "",
                                    "w-6 h-6 mr-2 group-hover:text-white"
                                  )}
                                />
                              </span>
                              Log Out
                            </a>
                          )}
                        </Menu.Item>
                      </Menu.Items>
                    </Transition>
                  </>
                )}
              </Menu>
            </div>
          </div>
        </div>
      </div>
      <Dialog
        open={dialogOpen}
        onClose={(event, reason) => {
          if (reason !== "backdropClick") {
            setDialogOpen(false);
          }
        }}
        aria-describedby="alert-dialog-slide-description"
      >
        <div style={{ width: 300 }}>
          <DialogTitle>Log Out</DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-slide-description">
              Are you sure you want to log out from Sole?
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button
              onClick={async () => {
                setDialogOpen(false);
              }}
            >
              Cancel
            </Button>

            <Button
              onClick={async () => {
                const result = await logoutUser({
                  status: 0,
                });
                if (result.success) {
                  setDialogOpen(false);
                  router.push("/");
                }
              }}
            >
              Log Out
            </Button>
          </DialogActions>
        </div>
      </Dialog>

      <NotificationSider open={open} setOpen={setOpen} updateId={updateId} />
    </div>
  );
}
