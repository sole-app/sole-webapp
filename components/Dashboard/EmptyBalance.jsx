import Link from 'next/link';
import CircularProgress from '@material-ui/core/CircularProgress';

export default function EmptyView(props) {
  const { loader } = props;

  if (loader) {
    return (
      <div className="flex items-center justify-center shadow h-3/4 rounded-md bg-white p-7">
        <div className="text-center">
          <div className="box-center" style={{height: '335px'}}><CircularProgress /></div>
        </div>
      </div>
    )
  }

  return (
    <div className="flex items-center justify-center shadow h-3/4 rounded-md bg-white p-7">
      <div className="text-center">
        <img
          src="/images/icon/undraw_calculator_re_alsc.svg"
          width={200}
          height={200}
          alt="Connect Bank Account"
        />

        <h3 className="mt-2 text-2xl font-medium text-gray-900">Connect Bank Account</h3>
        <p className="mt-1 text-sm text-gray-500">
          Get started by linking your bank account to Sole and enable <br></br>
          a (secure) live bank feed to reconcile your transactions.
        </p>

        <div className="mt-8">
          <Link href="/bankaccounts">
            <a className='inline-flex items-center px-4 py-1.5 border border-indigo-400 text-sm font-medium  rounded-full text-brand-blue bg-gray-50 hover:bg-indigo-100 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-purple-500'>
              Connect Bank Account
            </a>
          </Link>
        </div>
      </div>
    </div>
  )
}
