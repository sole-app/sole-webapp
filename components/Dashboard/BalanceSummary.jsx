import React from "react";
import {
  getCurrentFY,
  formatCurrencyWithoutSymbol,
  formatCurrency,
} from "../../helpers/common";
import NumberFormat from "react-number-format";
import Link from "next/link";

export default function BalanceSummary(props) {
  const { summary = {}, loader, bank_balance } = props;

  let currentFY = getCurrentFY();
  currentFY = currentFY.replace("Current ", "");

  if (loader) {
    return null;
  }

  return (
    <div className="p-7 2xl:p-8">
      <dl className="w-full text-center xl:text-left">
        <dd>
          <Link href="/bankaccounts">
            <a>
              <span className="inline-flex items-center px-8 py-2 mt-5 rounded-full text-sm font-medium bg-red-100 text-red-500">
                {formatCurrencyWithoutSymbol(summary.reconcile_pending)}{" "}
                unmatched{" "}
                {summary.reconcile_pending > 1 ? "transactions" : "transaction"}
              </span>
            </a>
          </Link>
        </dd>

        <dd>
          <div className="text-4xl 2xl:text-6xl mt-10 mb-5 whitespace-nowrap font-bold text-gray-900">
            {/* <sup>$</sup> {formatCurrencyWithoutSymbol(bank_balance)} */}
            <NumberFormat
              value={parseFloat(bank_balance).toFixed(2)}
              displayType={"text"}
              thousandSeparator={true}
              prefix={"$"}
              renderText={(value) => <div>{value}</div>}
            />
          </div>
        </dd>
      </dl>

      <div className="h-40 overflow-y-auto relative sm:rounded-lg mt-8">
        <table className="table-auto min-w-full relative">
          <thead className="bg-gray-100 rounded">
            <tr>
              <th
                scope="col"
                className="px-3 py-3 text-left text-sm font-medium text-gray-500 uppercase tracking-wider"
              ></th>
              <th
                scope="col"
                className="px-3 py-3 text-left text-sm font-medium whitespace-nowrap  text-gray-500 uppercase tracking-wider"
              >
                income
              </th>
              <th
                scope="col"
                className="px-3 py-3 text-left text-sm font-medium text-gray-500 uppercase tracking-wider"
              >
                expenses
              </th>

              <th
                scope="col"
                className="px-3 py-3 text-left text-sm font-medium text-gray-500 uppercase tracking-wider"
              >
                Profit
              </th>
            </tr>
          </thead>
          <tbody className="bg-white">
            <tr>
              <td className="px-3 py-4 whitespace-nowrap">
                <div className="flex items-center">
                  <div className="text-md font-medium text-gray-900">
                    {currentFY}
                  </div>
                </div>
              </td>

              <td className="px-3 py-4 text-left whitespace-nowrap">
                <div className="text-md text-gray-900">
                  {formatCurrency(summary.fy_year.income)}
                </div>
              </td>

              <td className="px-3 py-4 whitespace-nowrap">
                <div className="text-md text-gray-900">
                  {formatCurrency(summary.fy_year.expense)}
                </div>
              </td>

              <td className="px-3 py-4 whitespace-nowrap">
                <div
                  className={
                    summary.fy_year.profit < 0
                      ? "text-md font-semibold text-red-600"
                      : "text-md font-semibold text-green-600"
                  }
                >
                  {formatCurrency(summary.fy_year.profit)}
                </div>
              </td>
            </tr>

            <tr>
              <td className="px-3 py-4 whitespace-nowrap">
                <div className="flex items-center">
                  <div className="text-md font-medium text-gray-900">
                    This Month
                  </div>
                </div>
              </td>

              <td className="px-3 py-4 text-left whitespace-nowrap">
                <div className="text-md text-gray-900">
                  {formatCurrency(summary.current_month.income)}
                </div>
              </td>

              <td className="px-3 py-4 whitespace-nowrap">
                <div className="text-md text-gray-900">
                  {formatCurrency(summary.current_month.expense)}
                </div>
              </td>

              <td className="px-3 py-4 whitespace-nowrap">
                <div
                  className={
                    summary.current_month.profit < 0
                      ? "text-md font-semibold text-red-600"
                      : "text-md font-semibold text-green-600"
                  }
                >
                  {formatCurrency(summary.current_month.profit)}
                </div>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  );
}
