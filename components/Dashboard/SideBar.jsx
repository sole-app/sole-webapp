import React from "react";
import Link from "next/link";
import { Disclosure } from "@headlessui/react";
import {
  CogIcon,
  CollectionIcon,
  ViewGridIcon,
  SupportIcon,
  QuestionMarkCircleIcon,
  UserGroupIcon,
  LogoutIcon,
  CalculatorIcon,
  NewspaperIcon,
  LibraryIcon,
  DocumentTextIcon,
  CurrencyDollarIcon,
  UserIcon,
  DocumentReportIcon,
} from "@heroicons/react/outline";
import { logoutUser } from "../../services/api/user.services";
import { BiSidebar } from "react-icons/bi";
import { useState, useEffect } from "react";
import ReactTooltip from "react-tooltip";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Button from "@material-ui/core/Button";
import { getUserName } from "../../services/cookies.services";
import { useRouter } from "next/router";

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

export default function SideDrawer(props) {
  const router = useRouter();
  const { title, user = {} } = props;
  const [open, setOpen] = useState(false);
  const [dialogOpen, setDialogOpen] = useState(false);
  const [profileName, setProfileName] = useState(false);

  const navigation = [
    {
      name: "Dashboard",
      href: "/dashboard",
      id: "dashboard-link",
      icon: ViewGridIcon,
      current: title === "dashboard",
    },
    {
      name: "Quotes",
      href: "/quotes",
      id: "quotes-link",
      icon: DocumentTextIcon,
      current: title === "quotes",
    },
    {
      name: "Invoices",
      href: "/invoices",
      id: "invoices-link",
      icon: NewspaperIcon,
      current: title === "invoices",
    },
    {
      name: "Expenses",
      href: "/expenses",
      id: "expenses-link",
      icon: CurrencyDollarIcon,
      current: title === "expenses",
    },
    {
      name: "Assets",
      href: "/assets",
      id: "assets-link",
      icon: CollectionIcon,
      current: title === "assets",
    },
    {
      name: "Contacts",
      href: "/clients",
      id: "clients-link",
      icon: UserIcon,
      current: title === "contacts",
    },
    {
      name: "Bank Transactions",
      href: "/bankaccounts",
      id: "bankaccounts-link",
      icon: LibraryIcon,
      current: title === "bankaccounts",
    },
    // { name: 'Integrations', href: '/integration', id: 'integration-link',icon: ChipIcon, current: (title === 'integration')},
  ];

  const otherNavigation = [
    {
      name: "Reporting Suite",
      href: "/reports/assets",
      id: "report-link",
      icon: DocumentReportIcon,
      current: title === "reports",
    },
    {
      name: "Tax Estimator",
      href: "/tax-estimator",
      id: "tax-estimator-link",
      icon: CalculatorIcon,
      current: title === "tax",
    },
    {
      name: "Partner Offers",
      href: "/partners",
      id: "sole-partners",
      icon: UserGroupIcon,
      current: title === "partners",
    },
  ];

  const manageNavigation = [
    {
      name: "Settings",
      href: "/settings/profile",
      id: "settings-link",
      icon: CogIcon,
      current: title === "settings",
    },
    {
      name: "Contact",
      href: "https://soleapp.atlassian.net/servicedesk/customer/portal/1",
      id: "contact-link",
      icon: SupportIcon,
    },
    {
      name: "Help",
      href: "https://soleapp.atlassian.net/servicedesk/customer/portal/1",
      id: "help-link",
      icon: QuestionMarkCircleIcon,
    },
    { name: "Log Out", href: "#", id: "logout-link", icon: LogoutIcon },
  ];

  useEffect(() => {
    setProfileName(getUserName());
  }, [profileName, user]);

  function opensidebar() {
    setOpen(!open);
  }

  return (
    <div className="h-screen flex overflow-hidden bg-gray-100">
      {/* Static sidebar for desktop */}
      <div className="  flex lg:flex-shrink-0">
        <div
          className={classNames(
            open ? "slide-out" : "slide-in w-64",
            "transition-all  duration-300 ease-in-out flex flex-col relative sidebar"
          )}
        >
          {/* Sidebar component, swap this element with another sidebar if you like */}
          <div className="flex flex-col flex-grow bg-white border-r ml-1 border-gray-100 pt-5 pb-4 overflow-x-hidden overflow-y-auto">
            <div
              className={classNames(
                open ? "px-0" : "lg:px-4",
                "flex items-center justify-between flex-shrink-0 py-2 "
              )}
            >
              <div className={classNames(open ? "hidden" : "block  sm-logo")}>
                <img
                  src="/images/Sole_logo.svg"
                  alt="Sole logo"
                  width="112"
                  height="28"
                  className="h-8 w-auto"
                />
              </div>

              <div className={classNames(open ? "block" : "hidden logo-icon")}>
                <img
                  src="/images/sole-icon.svg"
                  alt="Sole logo"
                  width="106"
                  height="24"
                  className="h-8 w-auto"
                />
              </div>

              <button
                onClick={opensidebar}
                className="side-icon pl-4 text-gray-400 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-cyan-500"
              >
                <span className="sr-only">Open sidebar</span>
                <BiSidebar
                  className="h-6 w-6 transition duration-300 ease-in-out"
                  aria-hidden="true"
                />
              </button>
            </div>

            <div className="user-card">
              <div className="p-4  my-5 mx-3 bg-gray-50 border border-gray-200 rounded shadow-sm">
                <a
                  href="/settings/profile"
                  className="flex-shrink-0 group block"
                >
                  <div className="flex items-center">
                    <div>
                      <p className="text-md font-semibold text-gray-800 group-hover:text-gray-900">
                        Profile: {profileName}
                      </p>
                    </div>
                  </div>
                </a>
              </div>
            </div>

            <nav
              className="mt-2 flex-1 flex flex-col hidden-scroll overflow-y-auto"
              aria-label="Sidebar"
            >
              <div className="px-2 space-y-1">
                <h3 className="menu-head text-gray-800 mb-2 ml-2 font-semibold">
                  Business
                </h3>
                {navigation.map((item, iKey) =>
                  !item.children ? (
                    <div key={iKey}>
                      <Link href={item.href}>
                        <a
                          className={classNames(
                            item.current
                              ? "bg-brand-navyblue text-white"
                              : "text-gray-400 hover:bg-gray-200 hover:text-gray-600",
                            "group flex items-center px-3 py-3 text-md font-medium rounded-md menu-link"
                          )}
                          data-tip={item.name}
                          data-for={item.id}
                        >
                          <item.icon
                            className={classNames(
                              item.current
                                ? "text-white"
                                : "text-gray-400 group-hover:text-gray-600",
                              "mr-3 flex-shrink-0 h-6 w-6"
                            )}
                            aria-hidden="true"
                          />
                          <span className="whitespace-nowrap">
                            {" "}
                            {item.name}{" "}
                          </span>
                        </a>
                      </Link>

                      <ReactTooltip
                        id={item.id}
                        className="custom-tooltip bg-gray-900"
                        place="right"
                        textColor="#ffffff"
                        backgroundColor="#111827"
                        effect="solid"
                      >
                        <span> {item.name} </span>
                      </ReactTooltip>
                    </div>
                  ) : (
                    <Disclosure as="div" key={item.name} className="space-y-1">
                      {({ open }) => (
                        <>
                          <Disclosure.Button
                            className={classNames(
                              item.current
                                ? "bg-gray-900 text-white"
                                : "text-gray-300  hover:bg-gray-700 hover:text-white",
                              "group w-full flex items-center pl-3 pr-1 py-3 text-left tracking-wide text-md font-medium rounded-md focus:outline-none focus:ring-2 focus:ring-indigo-500"
                            )}
                          >
                            <item.icon
                              className="mr-3 flex-shrink-0 h-6 w-6 text-gray-400 group-hover:text-gray-300"
                              aria-hidden="true"
                            />
                            <span className="flex-1">{item.name}</span>
                            <svg
                              className={classNames(
                                open
                                  ? "text-gray-400 rotate-90"
                                  : "text-gray-300",
                                "ml-3 flex-shrink-0 h-5 w-5 transform group-hover:text-gray-500 transition-colors ease-in-out duration-150"
                              )}
                              viewBox="0 0 20 20"
                              aria-hidden="true"
                            >
                              <path
                                d="M6 6L14 10L6 14V6Z"
                                fill="currentColor"
                              />
                            </svg>
                          </Disclosure.Button>
                          <Disclosure.Panel className="space-y-1">
                            {item.children.map((subItem, subItemKey) => (
                              <Link href={subItem.href} key={subItemKey}>
                                <a
                                  key={subItem.name}
                                  className="group w-full flex items-center pl-11 pr-2 py-2 text-md font-medium text-gray-400 rounded-md  hover:bg-gray-700 hover:text-white"
                                >
                                  {subItem.name}
                                </a>
                              </Link>
                            ))}
                          </Disclosure.Panel>
                        </>
                      )}
                    </Disclosure>
                  )
                )}
              </div>

              <div className="mt-4 pt-6 bottom-menu">
                <div className="px-2 space-y-1">
                  <h3 className="text-gray-800 mb-2 ml-2 font-semibold menu-head">
                    Other
                  </h3>
                  {otherNavigation.map((item, itemKey) => (
                    <div key={itemKey}>
                      <Link href={item.href} key={itemKey}>
                        <a
                          onClick={() => {
                            if (item.name === "Log Out") {
                              setDialogOpen(true);
                            }
                          }}
                          className={classNames(
                            item.current
                              ? "bg-brand-navyblue text-white"
                              : "text-gray-400 hover:bg-gray-200 hover:text-gray-600",
                            "group flex items-center px-3 py-3 text-md font-medium rounded-md menu-link"
                          )}
                          data-tip={item.name}
                          data-for={item.id}
                        >
                          <item.icon
                            className={classNames(
                              item.current
                                ? "text-white"
                                : "text-gray-400 group-hover:text-gray-600",
                              "mr-3 flex-shrink-0 h-6 w-6"
                            )}
                            aria-hidden="true"
                          />
                          <span className="whitespace-nowrap">
                            {" "}
                            {item.name}{" "}
                          </span>
                        </a>
                      </Link>

                      <ReactTooltip
                        id={item.id}
                        className="custom-tooltip bg-gray-900"
                        place="right"
                        textColor="#ffffff"
                        backgroundColor="#111827"
                        effect="solid"
                      >
                        <span> {item.name} </span>
                      </ReactTooltip>
                    </div>
                  ))}
                </div>
              </div>

              <div className="mt-4 pt-6 bottom-menu">
                <div className="px-2 space-y-1">
                  <h3 className="text-gray-800 mb-2 ml-2 font-semibold menu-head">
                    Manage
                  </h3>
                  {manageNavigation.map((item, itemKey) => (
                    <div key={itemKey}>
                      {item.name === "Help" || item.name === "Contact" ? (
                        <>
                          <Link href={item.href}>
                            <a
                              target="_blank"
                              className={classNames(
                                item.current
                                  ? "bg-brand-navyblue text-white"
                                  : "text-gray-400 hover:bg-gray-200 hover:text-gray-600",
                                "group flex items-center px-3 py-3 text-md font-medium rounded-md menu-link"
                              )}
                              data-tip={item.name}
                              data-for={item.id}
                            >
                              <item.icon
                                className={classNames(
                                  item.current
                                    ? "text-white"
                                    : "text-gray-400 group-hover:text-gray-600",
                                  "mr-3 flex-shrink-0 h-6 w-6"
                                )}
                                aria-hidden="true"
                              />
                              <span className="whitespace-nowrap">
                                {" "}
                                {item.name}{" "}
                              </span>
                            </a>
                          </Link>
                        </>
                      ) : (
                        <>
                          <Link href={item.href}>
                            <a
                              onClick={() => {
                                if (item.name === "Log Out") {
                                  setDialogOpen(true);
                                }
                              }}
                              className={classNames(
                                item.current
                                  ? "bg-brand-navyblue text-white"
                                  : "text-gray-400 hover:bg-gray-200 hover:text-gray-600",
                                "group flex items-center px-3 py-3 text-md font-medium rounded-md menu-link"
                              )}
                              data-tip={item.name}
                              data-for={item.id}
                            >
                              <item.icon
                                className={classNames(
                                  item.current
                                    ? "text-white"
                                    : "text-gray-400 group-hover:text-gray-600",
                                  "mr-3 flex-shrink-0 h-6 w-6"
                                )}
                                aria-hidden="true"
                              />
                              <span className="whitespace-nowrap">
                                {" "}
                                {item.name}{" "}
                              </span>
                            </a>
                          </Link>
                        </>
                      )}

                      <ReactTooltip
                        id={item.id}
                        className="custom-tooltip bg-gray-900"
                        place="right"
                        textColor="#ffffff"
                        backgroundColor="#111827"
                        effect="solid"
                      >
                        <span> {item.name} </span>
                      </ReactTooltip>
                    </div>
                  ))}
                </div>
              </div>
            </nav>
          </div>
        </div>
      </div>

      <Dialog
        open={dialogOpen}
        onClose={(event, reason) => {
          if (reason !== "backdropClick") {
            setDialogOpen(false);
          }
        }}
        aria-describedby="alert-dialog-slide-description"
      >
        <div style={{ width: 300 }}>
          <DialogTitle>Log Out</DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-slide-description">
              Are you sure you want to log out from Sole?
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button
              onClick={async () => {
                setDialogOpen(false);
              }}
            >
              Cancel
            </Button>
            <Button
              onClick={async () => {
                const result = await logoutUser({
                  status: 0,
                });
                if (result.success) {
                  setDialogOpen(false);
                  router.push("/");
                }
              }}
            >
              Log Out
            </Button>
          </DialogActions>
        </div>
      </Dialog>
    </div>
  );
}
