import React, { useEffect, useState } from 'react'
import ReactTooltip from 'react-tooltip';
import { InformationCircleIcon } from '@heroicons/react/outline'
import { Line } from 'react-chartjs-2';
import CircularProgress from '@material-ui/core/CircularProgress';

const plugin = {
  id: "increase-legend-spacing",
  beforeInit(chart) {
    // Get reference to the original fit function
    const originalFit = chart.legend.fit;

    // Override the fit function
    chart.legend.fit = function fit() {
      // Call original function and bind scope in order to use `this` correctly inside it
      originalFit.bind(chart.legend)();
      // Change the height as suggested in another answers
      this.height += 20;
    }
  }
};

const options = {
  maintainAspectRatio: false,
  legend: {
    display: true,
    position: 'top',
  },
  plugins: {
    title: {
      display: true,
      text: '',
    },

    legend: {
      display: true,
      align: 'center',
      borderRadius: 15,
      onClick: null,


      labels: {
        // This more specific font property overrides the global property
        font: {
          size: 13,
          weight: "bold",
          lineHeight: 2,
          padding: 20,
          family: "'Inter', sans-serif"
        },

        boxWidth: 20,
        boxHeight: 20,
        borderRadius: 15,
        padding: 20,
      }
    },

    tooltip: {
      callbacks: {
        label: function (context) {
          let label = context.dataset.label || '';

          if (label) {
              label += ': $';
          }
          if (context.formattedValue !== null) {
              label += context.formattedValue;
          }
          return label;
        }
      }
    },
  },

  layout: {
    padding: 10,
  },

  scales: {
    y: {
      title: {
        display: true,
        text: 'Amounts'
      },
      ticks: {
        // Include a dollar sign in the ticks
        callback: function (value, index, ticks) {
          return '$' + Math.round(((value/1000) + Number.EPSILON) * 100) / 100 + 'k';
        },
        font: {
          size: 12,
          weight: 500,
          family: "'Inter', sans-serif"
        },
        color: "#9ca3af",
      }
    },
    x: {
      title: {
        display: true,
        text: 'Months'
      },
      ticks: {
        font: {
          size: 12,
          weight: 500,
          family: "'Inter', sans-serif"
        },
        color: "#9ca3af",
      }
    }
  }

};

export default function IncomeExpenseChart(props) {
  const { ieData, hashIE, loader } = props;

  return (
    <div className="w-full mt-8">
      <div className="grid grid-cols-1 h-full">
        <div className="bg-white overflow-hidden shadow rounded-lg">
          <div className="mx-auto border-b border-grey-400 p-3 sm:p-4  xl:p-5  flex items-center justify-between">
            <h3 className="text-lg inline-flex items-center font-medium tracking-wide text-gray-900 sm:text-xl">
              Income & Expenses

              <InformationCircleIcon
                className="h-6 w-6 text-gray-400 ml-2  focus:outline-none"
                data-for='income-expense-help'
                data-tip='Income and expenses of the account'
              />
              <ReactTooltip id='income-expense-help' className='custom-tooltip bg-gray-900' border textColor='#ffffff' backgroundColor='#111827' effect='solid' >
              <p className="w-64">
                A quick view of your income and expenses over time.

                {/* Your income is derived from <INSERT BUSINESS RULE> */}
                {/* Your expenses are calculated from <INSERT BUSINESS RULE> */}
              </p>
              </ReactTooltip>
            </h3>

            <div className="mt-8 flex sm:mt-0 lg:flex-shrink-0">

            </div>
          </div>

          <div className="flex items-center w-full pt-6 justify-center  rounded-md">
            {(loader)
              ?
              <div className="text-center">
                <div className="box-center" style={{ height: '305px' }}><CircularProgress /></div>
              </div>
              :
              <>
                {hashIE && ieData && Object.keys(ieData).length > 0 ? (
                  <>
                    <Line
                      data={ieData}
                      width={610}
                      height={350}
                      plugins={[plugin]}
                      options={options}
                    />
                  </>
                ) : (
                  <div className='py-10 px-4' style={{margin: '10px'}}>
                    <h3 className="my-4 text-xl font-medium text-gray-700"> Please connect your bank to view graph</h3>
                  </div>
                )}
              </>
            }
          </div>
        </div>
      </div>
    </div>
  )
}