import { useState, useEffect } from 'react'
import { Line } from 'react-chartjs-2';

const plugin = {

  id: "increase-legend-spacing",
  beforeInit(chart) {
    // Get reference to the original fit function
    const originalFit = chart.legend.fit;

    // Override the fit function
    chart.legend.fit = function fit() {
      // Call original function and bind scope in order to use `this` correctly inside it
      originalFit.bind(chart.legend)();
      // Change the height as suggested in another answers
      this.height += 20;
    }
  }
};

const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

const options = {
  maintainAspectRatio: false,
  legend: {
    display: true,
    position: 'top',
  },
  
  plugins: {
    title: {
      display: true,
      text: '',
    },

    legend: {
      display: true,
      align: 'center',
      borderRadius: 15,
      onClick: null,

      labels: {
        // This more specific font property overrides the global property
        font: {
          size: 13,
          weight: "bold",
          lineHeight: 2,
          padding: 20,
          family: "'Inter', sans-serif"
        },

        boxWidth: 20,
        boxHeight: 20,
        borderRadius: 15,
        padding: 20,
      }
    },

    tooltip: {
      callbacks: {
        label: function (context) {
          let label = context.dataset.label || '';

          if (label) {
              label += ': $';
          }
          if (context.formattedValue !== null) {
              label += context.formattedValue;
          }
          return label;
        }
      }
    },
  },

  layout: {
    padding: 10,
  },

  scales: {
    y: {
      title: {
        display: true,
        text: 'Amounts'
      },
      ticks: {
        // Include a dollar sign in the ticks
        callback: function (value, index, ticks) {
          return '$' + Math.round(((value/1000) + Number.EPSILON) * 100) / 100 + 'k';
        },
        font: {
          size: 12,
          weight: 500,
          family: "'Inter', sans-serif"
        },
        color: "#9ca3af",
      }
    },
    x: {
      title: {
        display: true,
        text: 'Months'
      },
      ticks: {
        font: {
          size: 12,
          weight: 500,
          family: "'Inter', sans-serif"
        },
        color: "#9ca3af",
      }
    }
  }

};


export default function BalanceChart(props) {
  const { loader, credit, debit } = props;
  const [loadChart, setLoadChart] = useState(false);
  const [chart, setChart] = useState({
    labels: months,
    datasets: [
      {
        label: 'Incoming',
        // backgroundColor: '#d1fae5',
        // borderColor: '#34d399',
        // borderWidth: 3,
        fill: true,
        borderRadius: 10,
        data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        backgroundColor: [
					'rgba(147, 250, 165, 0.2)',
					'rgba(54, 162, 235, 0.2)',
					'rgba(255, 206, 86, 0.2)',
					'rgba(75, 192, 192, 0.2)',
					'rgba(153, 102, 255, 0.2)',
					'rgba(255, 159, 64, 0.2)'
				],
				borderColor: [
					'rgba(178, 222, 39, 1)',
					'rgba(178, 222, 39, 1)',
					'rgba(178, 222, 39, 1)',
					'rgba(178, 222, 39, 1)',
					'rgba(178, 222, 39, 1)',
					'rgba(178, 222, 39, 1)'
				],
				borderWidth: 1,
      },
      {
        label: 'Outgoing',
        // backgroundColor: '#fee2e2',
        // borderColor: '#f87171',
        // borderWidth: 3,
        backgroundColor: [
					'rgba(255, 99, 71, 0.2)',
					'rgba(255, 99, 71, 0.2)',
					'rgba(255, 99, 71, 0.2)',
					'rgba(255, 99, 71, 0.2)',
					'rgba(255, 99, 71, 0.2)',
					'rgba(255, 99, 71, 0.2)'
				],
				borderColor: [
					'rgba(255, 0, 0, 1)',
					'rgba(255, 0, 0, 1)',
					'rgba(255, 0, 0, 1)',
					'rgba(255, 0, 0, 1)',
					'rgba(255, 0, 0, 1)',
					'rgba(255, 0, 0, 1)'
				],
				borderWidth: 1,
        fill: true,
        borderRadius: 10,
        data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
      }
    ]
  });

  useEffect(() => {
    if (!loader) {
      return;
    }
    let cData = chart;
    let creditData = [];
    let debitData = [];
    let labels = [];
    for (let i = 0; i < credit.length; i++) {
      creditData[i] = credit[i].income;
      labels[i] = months[credit[i].month - 1] + ' ' + credit[i].year.toString().substring(2);
    }
    for (let i = 0; i < debit.length; i++) {
      debitData[i] = debit[i].income;
    }
    cData.datasets[0].data = creditData.reverse();
    cData.datasets[1].data = debitData.reverse();
    cData.labels = labels.reverse();
    setChart(cData);
    setLoadChart(true);
  }, [loader, credit, debit]);

  return (
    <div>
      <div className="grid grid-cols-1 h-full">
        <div>
          <div className="flex items-center w-full pt-6 justify-center  rounded-md">
            <div className="text-center  w-full">
              {(!loader && loadChart) &&
                <Line
                  data={chart}
                  height={400}
                  plugins={[plugin]}
                  options={options}
                />
              }
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}