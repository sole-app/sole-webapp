import { useState, useEffect } from "react";
import ReactTooltip from "react-tooltip";
import { InformationCircleIcon } from "@heroicons/react/outline";
import Chart from "react-chartjs-2";
import { Bar } from "react-chartjs-2";
import { getCashflow } from "../../services/api/bankTransaction.services";
import { getDateFromFYDates } from "../../helpers/common";
import { listAccounts } from "../../services/api/bankAccount.services";
import CircularProgress from "@material-ui/core/CircularProgress";

const plugin = {
  id: "increase-legend-spacing",
  beforeInit(chart) {
    // Get reference to the original fit function
    const originalFit = chart.legend.fit;

    // Override the fit function
    chart.legend.fit = function fit() {
      // Call original function and bind scope in order to use `this` correctly inside it
      originalFit.bind(chart.legend)();
      // Change the height as suggested in another answers
      this.height += 20;
    };
  },
};

const options = {
  maintainAspectRatio: false,
  legend: {
    display: true,
    position: "top",
  },
  plugins: {
    title: {
      display: true,
      text: "",
    },

    legend: {
      display: true,
      align: "center",
      borderRadius: 15,
      onClick: null,

      labels: {
        // This more specific font property overrides the global property
        font: {
          size: 13,
          weight: "bold",
          lineHeight: 2,
          padding: 20,
          family: "'Inter', sans-serif",
        },

        boxWidth: 20,
        boxHeight: 20,
        borderRadius: 15,
        padding: 20,
      },
    },

    tooltip: {
      callbacks: {
        label: function (context) {
          let label = context.dataset.label || "";

          if (label) {
            label += ": $";
          }
          if (context.formattedValue !== null) {
            label += context.formattedValue;
          }
          return label;
        },
      },
    },
  },

  layout: {
    padding: 10,
  },

  scales: {
    y: {
      title: {
        display: true,
        text: "Amounts",
      },
      ticks: {
        // Include a dollar sign in the ticks
        callback: function (value, index, ticks) {
          return (
            "$" + Math.round((value / 1000 + Number.EPSILON) * 100) / 100 + "k"
          );
        },
        font: {
          size: 12,
          weight: 500,
          family: "'Inter', sans-serif",
        },
        color: "#9ca3af",
      },
    },
    x: {
      title: {
        display: true,
        text: "Months",
      },
      ticks: {
        font: {
          size: 12,
          weight: 500,
          family: "'Inter', sans-serif",
        },
        color: "#9ca3af",
      },
    },
  },
};

const months = [
  "Jan",
  "Feb",
  "Mar",
  "Apr",
  "May",
  "Jun",
  "Jul",
  "Aug",
  "Sep",
  "Oct",
  "Nov",
  "Dec",
];

export default function Cashflow(props) {
  const [loader, setLoader] = useState(true);
  const [hasCashflow, setHasCashFlow] = useState(false);
  const [cashflowData, setCashflowData] = useState({
    labels: months,
    datasets: [
      {
        label: "Cash In",
        backgroundColor: "#14b8a6",
        borderColor: "#14b8a6",
        borderWidth: 1,
        barWidth: 1,
        borderRadius: 5,
        data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      },
      {
        label: "Cash Out",
        backgroundColor: "#fb7185",
        borderColor: "#fb7185",
        borderWidth: 1,
        barWidth: 1,
        borderRadius: 5,
        data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      },
    ],
  });

  useEffect(async () => {
    setLoader(true);
    await getData();
    setLoader(false);
  }, []);

  const getData = async () => {
    const accountList = await listAccounts();
    let hasAccount = false;
    if (accountList.success) {
      if (
        typeof accountList.data.banks_accounts !== "undefined" &&
        accountList.data.banks_accounts.length > 0
      ) {
        for (let i = 0; i < accountList.data.banks_accounts.length; i++) {
          hasAccount = true;
        }
      }
    }

    let data = {
      order_by: "asc",
      start_date: "",
      end_date: "",
    };

    let dates = getDateFromFYDates();
    data.start_date = dates.start_date;
    data.end_date = dates.end_date;

    const result = await getCashflow(data);
    if (result.success) {
      if (
        typeof result.data.cashflow !== "undefined" &&
        result.data.cashflow.length > 0
      ) {
        let cashIn = [];
        let cashOut = [];
        let labels = [];
        for (let i = 0; i < result.data.cashflow.length; i++) {
          cashIn[i] = result.data.cashflow[i].cash_in;
          cashOut[i] = result.data.cashflow[i].cash_out;
          labels[i] =
            months[result.data.cashflow[i].month - 1] +
            " " +
            result.data.cashflow[i].year.toString().substring(2);
        }
        let cData = cashflowData;
        cData.datasets[0].data = cashIn.reverse();
        cData.datasets[1].data = cashOut.reverse();
        cData.labels = labels.reverse();
        setCashflowData(cData);
      }
    }
    setHasCashFlow(hasAccount);
  };

  <ReactTooltip />;

  return (
    <div className="w-full mt-8">
      <div className="grid grid-cols-1 h-full">
        <div className="bg-white overflow-hidden shadow rounded-lg">
          <div className="mx-auto border-b border-grey-400 p-3 sm:p-4  xl:p-5  flex items-center justify-between">
            <h3 className="text-lg inline-flex items-center font-medium tracking-wide text-gray-900 sm:text-xl">
              Cashflow
              <InformationCircleIcon
                className="h-6 w-6 text-gray-400 ml-2  focus:outline-none"
                data-for="cashinfo"
                data-tip=""
              />
              <ReactTooltip
                id="cashinfo"
                className="custom-tooltip bg-gray-900"
                textColor="#ffffff"
                backgroundColor="#111827"
                effect="solid"
                aria-haspopup="true"
              >
                <p className="w-64">
                  The cashflow graph indicates the cash received (or cash
                  inflows) and the cash paid (or cash outflows). Think of it as
                  a water tank: water comes in at the top and drains out the
                  bottom. So to keep your tank nice and full: you want more
                  coming in than going out.
                </p>
              </ReactTooltip>
            </h3>
          </div>

          <div className="flex items-center w-full pt-6 justify-center  rounded-md">
            {loader ? (
              <div className="text-center">
                <div className="box-center" style={{ height: "130px" }}>
                  <CircularProgress />
                </div>
              </div>
            ) : (
              <>
                {hasCashflow ? (
                  <div className="text-center w-full">
                    <Bar
                      data={cashflowData}
                      plugins={[plugin]}
                      width={690}
                      height={380}
                      options={options}
                    />
                  </div>
                ) : (
                  <div className="py-10 px-4">
                    <h3 className="my-4 text-xl font-medium text-gray-700">
                      Please connect your bank to view graph
                    </h3>
                  </div>
                )}
              </>
            )}
          </div>
        </div>
      </div>
    </div>
  );
}
