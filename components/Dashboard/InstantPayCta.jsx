import React from 'react'

export default function InstantPayCta() {
  return (
    <div className="h-full">
      <div className="bg-green-600 rounded-md">
        <div className="max-w-full mx-auto py-10 px-4 sm:px-6 2xl:py-12 3xl:py-16 3xl:px-12 lg:flex lg:items-center lg:justify-between">
          <div className="flex justify-between items-center 2xl:w-auto">
            <div className="text-white mr-2 2xl:mr-4">
              <img
                src="/images/ip-new-white.svg"
                alt="Sole logo"

                className="w-60 h-32 2xl:w-72 2xl:h-36 3xl:w-96 3xl:w-72"
              />
            </div>

            <h2 className="text-2xl font-light  text-white lg:text-3xl 2xl:text-4xl 3xl:text-5xl">
              Get paid faster <span className="block">and easier with</span> <span className="block">Instant Pay </span>
            </h2>
          </div>
        </div>
      </div>
    </div>
  )
}
