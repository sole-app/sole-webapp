import React, { useEffect, useState } from "react";
import ReactTooltip from "react-tooltip";
import { InformationCircleIcon } from "@heroicons/react/outline";
import { Line } from "react-chartjs-2";
import { formatCurrency } from "../../helpers/common";
import CircularProgress from "@material-ui/core/CircularProgress";

let snapshotGraphData = {
  datasets: [
    {
      label: "Perfomance",
      // backgroundColor: '#d1d5db',
      // borderColor: '#4C4DFE',

      fill: true,
      // pointRadius: 0,
      // borderColor: "#0000ff",
      // backgroundColor: "rgba(255,10,13,255)",
      // borderWidth: 2,
      borderRadius: 3,
      data: [],
      backgroundColor: [
        "rgba(255, 99, 132, 0.2)",
        "rgba(54, 162, 235, 0.2)",
        "rgba(255, 206, 86, 0.2)",
        "rgba(75, 192, 192, 0.2)",
        "rgba(153, 102, 255, 0.2)",
        "rgba(255, 159, 64, 0.2)",
      ],
      borderColor: [
        "rgba(255, 99, 132, 1)",
        "rgba(255, 99, 132, 1)",
        "rgba(255, 99, 132, 1)",
        "rgba(255, 99, 132, 1)",
        "rgba(255, 99, 132, 1)",
        "rgba(255, 99, 132, 1)",
      ],
      borderWidth: 1,
    },
    {
      label: "Goal",
      fill: true,
      // pointRadius: 0,
      data: [],
      backgroundColor: [
        "rgba(255, 99, 132, 0.2)",
        "rgba(54, 162, 235, 0.2)",
        "rgba(255, 206, 86, 0.2)",
        "rgba(75, 192, 192, 0.2)",
        "rgba(153, 102, 255, 0.2)",
        "rgba(255, 159, 64, 0.2)",
      ],
      borderColor: [
        "rgba(54, 162, 235, 1)",
        "rgba(54, 162, 235, 1)",
        "rgba(54, 162, 235, 1)",
        "rgba(54, 162, 235, 1)",
        "rgba(54, 162, 235, 1)",
        "rgba(54, 162, 235, 1)",
      ],
      borderWidth: 1,
    },
  ],
};

const plugin = {
  id: "increase-legend-spacing",
  beforeInit(chart) {
    // Get reference to the original fit function
    const originalFit = chart.legend.fit;

    // Override the fit function
    chart.legend.fit = function fit() {
      // Call original function and bind scope in order to use `this` correctly inside it
      originalFit.bind(chart.legend)();
      // Change the height as suggested in another answers
      this.height += 40;
    };
  },
};

const options = {
  maintainAspectRatio: false,
  legend: {
    display: false,
    position: "top",
  },
  plugins: {
    title: {
      display: true,
      text: "",
    },

    legend: {
      display: false,
      align: "center",
      borderRadius: 15,
      onClick: null,

      labels: {
        // This more specific font property overrides the global property
        font: {
          size: 13,
          weight: "bold",
          lineHeight: 2,
          padding: 20,
          family: "'Inter', sans-serif",
        },

        boxWidth: 20,
        boxHeight: 20,
        borderRadius: 15,
        padding: 20,
      },
    },

    tooltip: {
      callbacks: {
        label: function (context) {
          let label = context.dataset.label || "";

          if (label) {
            label += ": $";
          }
          if (context.formattedValue !== null) {
            label += context.formattedValue;
          }
          return label;
        },
      },
    },
  },

  layout: {
    padding: 10,
  },

  scales: {
    y: {
      title: {
        display: true,
        text: "Amounts",
      },
      ticks: {
        // Include a dollar sign in the ticks
        callback: function (value, index, ticks) {
          return (
            "$" + Math.round((value / 1000 + Number.EPSILON) * 100) / 100 + "k"
          );
        },
        font: {
          size: 12,
          weight: 500,
          family: "'Inter', sans-serif",
        },
        color: "#9ca3af",
      },
    },
    x: {
      title: {
        display: true,
        text: "Months",
      },
      ticks: {
        font: {
          size: 12,
          weight: 500,
          family: "'Inter', sans-serif",
        },
        color: "#9ca3af",
      },
    },
  },
};

export default function SoleSnapshot({ snapshotdata, openSetGoal }) {
  const [loader, setLoader] = useState(true);
  const [labels, setLabels] = useState([]);
  const [dataPoints, setDataPoints] = useState([]);

  useEffect(() => {
    if (snapshotdata.snapshot_data) {
      let tempDM = [];
      let tempDP = [];
      let tempGoal = [];
      snapshotdata.snapshot_data.forEach((element) => {
        tempDM.push(element.dispaly_month);
        tempDP.push(element.performance);
        tempGoal.push(element.goal);
      });
      setLabels(tempDM);
      setDataPoints(tempDP);
      snapshotGraphData.labels = tempDM;
      snapshotGraphData.datasets[0].data = tempDP;
      snapshotGraphData.datasets[1].data = tempGoal;
      setLoader(false);
    }
  }, [snapshotdata]);

  const setGoal = () => {
    openSetGoal(snapshotdata.goal);
  };

  return (
    <div className="w-full mt-8">
      <div className="grid grid-cols-1 h-full">
        <div className="bg-white overflow-hidden shadow rounded-lg">
          <div className="mx-auto border-b border-grey-400 p-3 sm:p-4  xl:p-5  flex items-center justify-between">
            <h3 className="text-lg inline-flex items-center font-medium tracking-wide text-gray-900 sm:text-xl">
              Sole Snapshot
              <InformationCircleIcon
                className="h-6 w-6 text-gray-400 ml-2  focus:outline-none"
                data-for="solesnap"
                data-tip=""
              />
              <ReactTooltip
                id="solesnap"
                className="custom-tooltip bg-gray-900"
                textColor="#ffffff"
                backgroundColor="#111827"
                effect="solid"
                aria-haspopup="true"
              >
                <p className="w-64">
                  The Snapshot graph allows you to set your financial goal and
                  it compares your performance against this goal. It helps keep
                  you on track!
                  <br></br>
                  <br></br>
                  Your performance in the graph updates as you match
                  transactions in your bank account.
                </p>
              </ReactTooltip>
            </h3>

            <div className="mt-8 flex sm:mt-0 lg:flex-shrink-0"></div>

            <div className="mt-8 flex sm:mt-0 lg:flex-shrink-0">
              <button
                className="inline-flex items-center px-4 py-1.5 border border-indigo-400 text-sm font-medium  rounded-full text-brand-blue bg-gray-50 hover:bg-indigo-100 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-purple-500"
                onClick={setGoal}
              >
                Set Goal:{" "}
                {snapshotdata.goal > 0
                  ? formatCurrency(snapshotdata.goal)
                  : "-"}
              </button>
            </div>
          </div>

          <div className="flex items-center w-full pt-6 justify-center relative rounded-md">
            {loader ? (
              <div className="py-10 px-4">
                <div className="text-center">
                  <div className="box-center" style={{ height: "70px" }}>
                    <CircularProgress />
                  </div>
                </div>
              </div>
            ) : (
              <>
                {snapshotdata && Object.keys(snapshotdata).length > 0 ? (
                  <>
                    <div className="text-center w-full">
                      <Line
                        data={snapshotGraphData}
                        plugins={[plugin]}
                        width={690}
                        height={380}
                        options={options}
                      />
                    </div>
                  </>
                ) : (
                  <div className="py-10 px-4">
                    <h3 className="my-4 text-xl font-medium text-gray-700">
                      {" "}
                      Please connect your bank to view graph
                    </h3>
                  </div>
                )}
              </>
            )}
          </div>
        </div>
      </div>
    </div>
  );
}
