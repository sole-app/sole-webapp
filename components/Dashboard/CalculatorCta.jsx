import React from 'react'
import Link from 'next/link'

export default function CalculatorCta() {
  return (
    <div className="mt-8">
      <div className="bg-brand-navyblue rounded-md">
        <div className="max-w-full mx-auto py-10 px-4 sm:px-6 3xl:py-12 3xl:px-12 lg:flex lg:items-center lg:justify-between">
          <div className="flex justify-between items-center w-full">
            <div>
              <h2 className="text-2xl  font-light  text-white lg:text-3xl 2xl:text-4xl 3xl:text-5xl">
                <span className="block">Get an estimate on your </span>
                <span className="block"> taxable income today!</span>
              </h2>

              <div className="mt-2 xl:mt-5 2xl:mt-7 mr-8 flex  lg:flex-shrink-0">
                <div className="inline-flex rounded-md">
                  <Link href="/tax-estimator">
                    <a className="inline-flex items-center shadow justify-center px-5 py-3 border border-transparent text-md 2xl:text-xl font-semibold rounded-full text-gray-700 bg-white hover:bg-gray-100">
                      Go to Tax Estimator
                    </a>
                  </Link>
                </div>
              </div>
            </div>

            <div className="text-white ml-4 mr-2 xl:ml-12 2xl:ml-24 3xl:ml-28">
              <img
                src="/images/tax-income-calc.svg"
                alt="Sole logo"
                width="80"
                height="80"
                className="w-auto"
              />

              {/* <AiOutlineCalculator className="h-32 w-32" /> */}
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
