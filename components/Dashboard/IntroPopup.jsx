import { Fragment, useState, useEffect } from 'react'
import { Dialog, Transition } from '@headlessui/react'
import { XIcon } from '@heroicons/react/outline'
import Link from 'next/link';
import store from '../../store/store';

export default function IntroPopup() {
  const storeData = store.store.getState();
  const [openPopup, setOpenPopup] = useState(false);
  const [days, setDays] = useState(0);

  useEffect(async () => {
    if (localStorage.getItem('display-welcome-popup') === null) {
      if (typeof storeData.user_details !== 'undefined'
        && typeof storeData.user_details.subscription !== 'undefined'
        && typeof storeData.user_details.subscription.is_free !== 'undefined'
        && storeData.user_details.subscription.is_free
      ) {
        setDays(storeData.user_details.subscription.free_day);
        if (localStorage.getItem('intro-popup') !== null) {
          setOpenPopup(false);
        } else {
          setOpenPopup(true);
          localStorage.setItem('intro-popup', true)
        }
      }
    }
  }, [storeData]);

  return (
    <Transition.Root show={openPopup} as={Fragment}>
      <Dialog as="div" className="fixed z-10 inset-0 overflow-y-auto" onClose={setOpenPopup}>
        <div className="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <Dialog.Overlay className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity pointer-events-none" />
          </Transition.Child>

          {/* This element is to trick the browser into centering the modal contents. */}
          <span className="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">
            &#8203;
          </span>
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
            enterTo="opacity-100 translate-y-0 sm:scale-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100 translate-y-0 sm:scale-100"
            leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
          >
            <div className="relative inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-xl sm:w-full">
              <div className="hidden sm:block absolute top-0 right-0 pt-4 pr-4">
                <button
                  type="button"
                  className="bg-white rounded-full text-brand-blu hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                  onClick={() => setOpenPopup(false)}
                >
                  <span className="sr-only">Close</span>
                  <XIcon className="h-8 w-8 p-1" aria-hidden="true" />
                </button>
              </div>

              <div>
                <div className="text-center px-4 pt-10  sm:text-center">
                  <img
                    src="/images/subscription.png"
                    alt="Subscription"
                    width="200"
                    className="mx-auto"/>
                </div>

                <div className='pt-6 px-4'>
                  <Dialog.Title as="h3" className="text-2xl text-center leading-6 py-4 px-2 font-semibold text-brand-blue">
                    Enjoy your {days} days free trial
                  </Dialog.Title>
                  <p className='text-center text-black'>
                    At the end of your trial, you will be prompted to pay for a Sole subscription. You are able to stop using Sole at any time during the free trial and you will not be charged.
                    <br></br><br></br>
                    Need a quick refresher on how Sole works and what to do next? <br></br>Check out our <a target='blank' style={{textDecoration:'underline'}} href="https://soleapp.com.au/sole-bootcamp/"><span className='font-bold'>Bootcamp</span></a> or the <a target='blank' style={{textDecoration:'underline'}} href="https://soleapp.com.au/academy/"><span className='font-bold'>Sole Academy</span></a>.
                  </p>
                </div>
              </div>

              <div className="py-7 px-5 sm:flex justify-center space-x-4">
                <button
                  type="button"
                  className="w-full inline-flex justify-center rounded-full  shadow-sm px-4 py-2 tracking-wide text-base font-medium text-brand-blue border-2 border-brand-blue hover:text-white hover:bg-brand-darkblue focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-500 sm:w-auto sm:text-base"
                  onClick={() => setOpenPopup(false)}
                >
                  Continue With Trial
                </button>

                <Link href='/settings/subscription'>
                  <a
                    type="button"
                    className="w-full inline-flex justify-center rounded-full border border-transparent shadow-sm px-4 py-2 tracking-wide text-base font-medium text-white bg-brand-blue hover:bg-brand-darkblue focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-500 sm:w-auto sm:text-base"
                  >
                    Subscribe Now
                  </a>
                </Link>

              </div>
            </div>
          </Transition.Child>
        </div>
      </Dialog>
    </Transition.Root>
  )
}
