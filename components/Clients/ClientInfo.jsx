import { Fragment, useState, useEffect } from "react";
import { Dialog, Transition, Listbox, Tab } from "@headlessui/react";
import { XIcon } from "@heroicons/react/outline";
import { Info } from "../../services/api/client.services";
import Skeleton from "react-loading-skeleton";
import { List as ListInvoice } from "../../services/api/invoice.services";
import { getMatchTransactionsByClient } from "../../services/api/bankTransaction.services";
import NumberFormat from "react-number-format";
import { List as ListExpense } from "../../services/api/expense.services";
import { CheckIcon, SelectorIcon } from "@heroicons/react/solid";

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

const invcategory = [
  { name: "ALL" },
  { name: "Draft" },
  { name: "Due" },
  { name: "Overdue" },
  { name: "Paid" },
  { name: "Bank Matches" },
  { name: "Expenses" },
];

export default function ClientInfo(props) {
  const [infoData, setInfoData] = useState("");
  const [invoiceData, setInvoiceData] = useState([]);
  const [expenseData, setExpenseData] = useState([]);
  const [bankMatchData, setBankMatchData] = useState([]);
  const [loading, setLoading] = useState(true);
  const [selected, setSelected] = useState(invcategory[0]);
  const [tabValue, setTabValue] = useState(props.isClient);
  const [selectedTab, setSelectedTab] = useState(0);
  const [clientId, setClientId] = useState(0);

  const changeSelectedTab = async (select) => {
    setSelectedTab(select);

    if (select === 1) {
      // Bank Match
      // Get Bank Match Transactions
      getMatchTrans(props.updateId);
    } else if (select === 2) {
      // Expenses Data
      getExpenses(props.updateId);
    }
  };

  // Get Client Info Call
  useEffect(async () => {
    // Client or Supplier
    setTabValue(props.isClient);

    setClientId(props.updateId);

    setSelectedTab(0); // Default to Invoice Tab

    if (props.isInfoOpen && props.updateId > 0) {
      setLoading(true);
      const formData = new FormData();
      formData.append("id", props.updateId);
      formData.append("type", tabValue == 0 ? 1 : 2);

      const resultInfo = await Info(props.updateId);
      if (resultInfo.success) {
        setInfoData(resultInfo.data);
        setLoading(false);

        // Get list of all invoices
        getInvoiceList(props.updateId);
      } else {
        setLoading(false);
      }
    }
  }, [props.isInfoOpen]);

  const closeSlider = async () => {
    props.setIsInfoOpen; // Toggle status open/close
    setInvoiceData(null);
  };

  // Get All Invoice for this client
  const getInvoiceList = async (clientID) => {
    let formData = "?page=1";
    formData += "&filter_by[client_id]=" + clientID;
    formData += "&filter_by[status]=all";
    formData += "&sort_by[invoice_id]=desc";

    const result = await ListInvoice(formData);
    if (result.success) {
      setInvoiceData(result.data.invoice_details);
      setLoading(false);
    } else {
      setLoader(false);
    }
  };

  // Get All Match Transactyions
  const getMatchTrans = async (clientID) => {
    setLoading(true);

    const formData = new FormData();
    formData.append("client_id", clientID);

    const result = await getMatchTransactionsByClient(formData);
    if (result.success) {
      const isEmpty = Object.keys(result.data).length === 0;
      if (!isEmpty) {
        setBankMatchData(result.data.invoices.data);
        setLoading(false);
      } else {
        setLoading(false);
      }
    } else {
      setLoading(false);
    }
  };

  const getExpenses = async (clientID) => {
    setLoading(true);

    let formData = "?page=1";
    formData += "&filter_by[client_id]=" + clientID;
    formData += "&sort_by[expense_id]=desc";

    const result = await ListExpense(formData);
    if (result.success) {
      setExpenseData(result.data.expense_details);
      setLoading(false);
    } else {
      setLoading(false);
    }
  };

  const invStatusStyle = (status) => {
    switch (status) {
      case "draft":
        return (
          <span className="px-4 py-1 inline-flex text-xs leading-5 font-medium rounded-full bg-gray-200 text-gray-800">
            {status.toUpperCase()}
          </span>
        );
      case "overdue":
        return (
          <span className="px-4 py-1 inline-flex text-xs leading-5 font-medium rounded-full bg-red-100 text-red-500">
            {status.toUpperCase()}
          </span>
        );
      case "due":
        return (
          <span className="px-4 py-1 inline-flex text-xs leading-5 font-medium rounded-full bg-indigo-100 text-indigo-800">
            {status.toUpperCase()}
          </span>
        );
      case "paid":
        return (
          <span className="px-4 py-1 inline-flex text-xs leading-5 font-medium rounded-full bg-green-100 text-green-800">
            {status.toUpperCase()}
          </span>
        );
      default:
        return (
          <span className="px-4 py-1 inline-flex text-xs leading-5 font-medium rounded-full bg-gray-200 text-gray-800">
            {status.toUpperCase()}
          </span>
        );
    }
  };

  const getDisplayDate = (dt) => {
    var date = new Date(dt);
    return date.toISOString().substring(0, 10);
  };

  return (
    <Transition.Root show={props.isInfoOpen} as={Fragment}>
      <Dialog
        as="div"
        static
        className="fixed inset-0 overflow-hidden"
        onClose={props.setIsInfoOpen}
      >
        <div className="absolute inset-0 overflow-hidden">
          <Transition.Child
            as={Fragment}
            enter="ease-in-out duration-500"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in-out duration-500"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <Dialog.Overlay className="absolute inset-0 bg-gray-500 bg-opacity-75 transition-opacity pointer-events-none" />
          </Transition.Child>

          <div className="fixed inset-y-0 max-w-full right-0 flex">
            <Transition.Child
              as={Fragment}
              enter="transform transition ease-in-out duration-500 sm:duration-700"
              enterFrom="translate-x-full"
              enterTo="translate-x-0"
              leave="transform transition ease-in-out duration-500 sm:duration-700"
              leaveFrom="translate-x-0"
              leaveTo="translate-x-full"
            >
              <div className="w-screen lg:max-w-3xl bg-white">
                <div className="flex-1 pb-8 overflow-y-auto">
                  <div className="border-b border-gray-200 bg-gray-100 py-4 px-4 sm:px-6">
                    <div className="flex items-center justify-between">
                      <Dialog.Title className="text-xl font-medium text-gray-800">
                        {tabValue == 0 ? "Client" : "Supplier"} Information
                      </Dialog.Title>
                      <div className="ml-3 h-7 flex items-center">
                        <button
                          type="button"
                          className="rounded-full hover:bg-gray-300 p-2 text-gray-500 hover:text-gray-700 focus:outline-none focus:ring-2 focus:ring-white"
                          onClick={() => props.setIsInfoOpen(false)}
                        >
                          <span className="sr-only">Close panel</span>
                          <XIcon className="h-6 w-6" aria-hidden="true" />
                        </button>
                      </div>
                    </div>
                  </div>
                  <div className="flex-1 flex flex-col justify-between">
                    <div className=" divide-y divide-gray-200 h-100vh">
                      <div className="gap-y-6 px-4 2xl:px-6 border-b border-gray-200 bg-gray-50 pt-6 pb-7 grid grid-cols-1 sm:grid-cols-3">
                        {!loading ? (
                          infoData ? (
                            <>
                              <div>
                                <label className="info-label">Name</label>
                                <p>{infoData.name}</p>
                              </div>

                              <div>
                                <label className="info-label">
                                  Business Name
                                </label>
                                <p>
                                  {infoData.business_name
                                    ? infoData.business_name
                                    : "-"}
                                </p>
                              </div>

                              <div>
                                <label className="info-label">ABN</label>
                                <p>{infoData.ABN ? infoData.ABN : "-"}</p>
                              </div>

                              <div>
                                <label className="info-label">Email</label>
                                <p>{infoData.email ? infoData.email : "-"}</p>
                              </div>

                              <div>
                                <label className="info-label">
                                  Phone Number
                                </label>
                                <p>
                                  {infoData.mobile_no
                                    ? infoData.mobile_no
                                    : "-"}
                                </p>
                              </div>

                              <div>
                                <label className="info-label">Website</label>
                                <p>
                                  {infoData.website ? infoData.website : "-"}
                                </p>
                              </div>

                              <div>
                                <label className="info-label">Address</label>
                                <p>
                                  {infoData.address.address_line1
                                    ? infoData.address.address_line1
                                    : "-"}
                                </p>
                              </div>
                            </>
                          ) : (
                            <p>No records found!</p>
                          )
                        ) : (
                          <>
                            {Array.from(Array(2), (e, i) => {
                              return (
                                <div className="sm:col-span-2" key={i}>
                                  <label className="info-label">
                                    <Skeleton />
                                  </label>
                                  <p>
                                    <Skeleton />
                                  </p>
                                </div>
                              );
                            })}
                          </>
                        )}
                      </div>

                      <Tab.Group
                        selectedIndex={selectedTab}
                        onChange={(index) => changeSelectedTab(index)}
                      >
                        <Tab.List className="flex  space-x-1 border-b border-grey-400 bg-blue-900/20 ">
                          <Tab
                            className={classNames(
                              "py-6 px-6 text-base  leading-5 font-medium",
                              "focus:outline-none",
                              selectedTab === 0
                                ? "text-brand-blue border-b border-brand-blue"
                                : "text-grey-400 hover:bg-white/[0.12] "
                            )}
                          >
                            Invoices
                          </Tab>

                          <Tab
                            className={classNames(
                              "py-6 px-6 text-base  leading-5 font-medium",
                              "focus:outline-none",
                              selectedTab === 1
                                ? "text-brand-blue border-b border-brand-blue"
                                : "text-grey-400 hover:bg-white/[0.12] "
                            )}
                          >
                            Bank Matches
                          </Tab>

                          <Tab
                            className={classNames(
                              "py-6 px-6 text-base  leading-5 font-medium",
                              "focus:outline-none",
                              selectedTab === 2
                                ? "text-brand-blue border-b border-brand-blue"
                                : "text-grey-400 hover:bg-white/[0.12] "
                            )}
                          >
                            Expenses
                          </Tab>
                        </Tab.List>

                        <Tab.Panels className="h-full overflow-y-auto">
                          {/* Invoices */}
                          <Tab.Panel
                            key="0"
                            className={classNames(
                              "bg-white h-full  rounded-xl",
                              "focus:outline-none   ring-white ring-opacity-60"
                            )}
                          >
                            <div className="w-72 ml-auto mr-0 p-5">
                              <Listbox value={selected} onChange={setSelected}>
                                <div className="relative mt-1">
                                  <Listbox.Button className="bg-gray-100 relative w-full border border-gray-300 rounded-md shadow-sm pl-3 pr-10 py-3 text-left cursor-default focus:outline-none focus:ring-1 focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
                                    <span className="block truncate">
                                      {selected.name}
                                    </span>
                                    <span className="absolute inset-y-0 right-0 flex items-center pr-2 pointer-events-none">
                                      <SelectorIcon
                                        className="w-5 h-5 text-gray-400"
                                        aria-hidden="true"
                                      />
                                    </span>
                                  </Listbox.Button>
                                  <Transition
                                    as={Fragment}
                                    leave="transition ease-in duration-100"
                                    leaveFrom="opacity-100"
                                    leaveTo="opacity-0"
                                  >
                                    <Listbox.Options className="absolute w-full py-1 mt-1 overflow-auto text-base bg-white rounded-md shadow-lg max-h-60 ring-1 ring-black ring-opacity-5 focus:outline-none sm:text-sm">
                                      {invcategory.map((person, personIdx) => (
                                        <Listbox.Option
                                          key={personIdx}
                                          className={({ active }) =>
                                            `cursor-default select-none relative py-2 pl-10 pr-4 ${
                                              active
                                                ? "text-white bg-brand-bluetext-white bg-brand-blue text-amber-900 bg-amber-100"
                                                : "text-gray-900"
                                            }`
                                          }
                                          value={person}
                                        >
                                          {({ selected }) => (
                                            <>
                                              <span
                                                className={`block truncate ${
                                                  selected
                                                    ? "font-medium"
                                                    : "font-normal"
                                                }`}
                                              >
                                                {person.name}
                                              </span>
                                              {selected ? (
                                                <span className="absolute inset-y-0 left-0 flex items-center pl-3 text-amber-600">
                                                  <CheckIcon
                                                    className="w-5 h-5"
                                                    aria-hidden="true"
                                                  />
                                                </span>
                                              ) : null}
                                            </>
                                          )}
                                        </Listbox.Option>
                                      ))}
                                    </Listbox.Options>
                                  </Transition>
                                </div>
                              </Listbox>
                            </div>

                            <div className="h-full divide-y divide-gray-200 flex flex-col bg-white">
                              <div className="flex-1 h-0 pb-8 overflow-y-auto">
                                <div className="flex-1 flex flex-col justify-between">
                                  <div className="px-4 divide-y divide-gray-200 sm:px-6">
                                    <div className="shadow mt-2 bg-white  overflow-hidden border-b border-gray-200 sm:rounded-lg">
                                      <div className="overflow-y-auto overflow-x-auto">
                                        <table className="min-w-full  overflow-x-hidden divide-y hover-row divide-gray-200">
                                          <thead className="bg-gray-100">
                                            <tr>
                                              <th
                                                scope="col"
                                                className="px-7 py-3 text-left text-sm font-medium text-gray-500 uppercase tracking-wider"
                                              >
                                                Invoice #
                                              </th>
                                              <th
                                                scope="col"
                                                className="px-7 py-3  text-left text-sm font-medium text-gray-500 uppercase tracking-wider"
                                              >
                                                Client Name
                                              </th>
                                              <th
                                                scope="col"
                                                className="px-7 py-3 text-left text-sm font-medium text-gray-500 uppercase tracking-wider"
                                              >
                                                Status
                                              </th>
                                              <th
                                                scope="col"
                                                className="px-7 py-3 text-left text-sm font-medium text-gray-500 uppercase tracking-wider"
                                              >
                                                Total
                                              </th>
                                            </tr>
                                          </thead>

                                          <tbody className="bg-white divide-y divide-gray-200">
                                            {loading == false ? (
                                              invoiceData != null ? ( // records.length > 0 ?
                                                invoiceData.map((val, key) => (
                                                  <tr key={val.id}>
                                                    <td className="px-7 py-4 whitespace-nowrap">
                                                      <div className="text-md text-gray-900">
                                                        {val.invoice_no}
                                                      </div>
                                                      <span className="text-xs text-gray-500">
                                                        {val.invoice_date}
                                                      </span>
                                                    </td>

                                                    <td className="px-7 py-4 whitespace-nowrap">
                                                      <div className="text-md text-gray-900">
                                                        {val.description}
                                                      </div>
                                                    </td>

                                                    <td className="px-7 py-4 whitespace-nowrap text-md text-gray-500">
                                                      {invStatusStyle(
                                                        val.status.toLowerCase()
                                                      )}
                                                    </td>

                                                    <td className="px-7 py-4 whitespace-nowrap">
                                                      <div className="text-md text-gray-900">
                                                        <NumberFormat
                                                          value={val.total.toFixed(
                                                            2
                                                          )}
                                                          displayType={"text"}
                                                          thousandSeparator={
                                                            true
                                                          }
                                                          prefix={"$"}
                                                          renderText={(
                                                            value
                                                          ) => (
                                                            <div>{value}</div>
                                                          )}
                                                        />
                                                      </div>
                                                    </td>
                                                  </tr>
                                                ))
                                              ) : (
                                                <tr>
                                                  <td
                                                    colSpan="7"
                                                    align="center"
                                                  >
                                                    <span className="px-4">
                                                      <br></br>
                                                      No invoice found under
                                                      this{" "}
                                                      {tabValue == 0
                                                        ? "client"
                                                        : "supplier"}
                                                      <br></br>
                                                      <br></br>
                                                    </span>
                                                  </td>
                                                </tr>
                                              )
                                            ) : (
                                              ""
                                            )}
                                          </tbody>
                                        </table>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </Tab.Panel>

                          {/* Bank Matches */}
                          <Tab.Panel
                            key="1"
                            className={classNames(
                              "bg-white h-full  rounded-xl",
                              "focus:outline-none   ring-white ring-opacity-60"
                            )}
                          >
                            <div className="h-full divide-y divide-gray-200 flex flex-col bg-white">
                              <div className="flex-1 h-0 pb-8 overflow-y-auto">
                                <div className="flex-1 flex flex-col justify-between">
                                  <div className="px-4 divide-y divide-gray-200 sm:px-6">
                                    <div className="shadow mt-6 bg-white  overflow-hidden border-b border-gray-200 sm:rounded-lg">
                                      <div className="overflow-y-auto overflow-x-auto">
                                        <table className="min-w-full  overflow-x-hidden divide-y hover-row divide-gray-200">
                                          <thead className="bg-gray-100">
                                            <tr>
                                              <th
                                                scope="col"
                                                className="px-7 py-3 text-left text-sm font-medium text-gray-500 uppercase tracking-wider"
                                              >
                                                Status
                                              </th>
                                              <th
                                                scope="col"
                                                className="px-7 py-3 text-left text-sm font-medium text-gray-500 uppercase tracking-wider"
                                              >
                                                Transaction
                                              </th>
                                              <th
                                                scope="col"
                                                className="px-7 py-3 text-left text-sm font-medium text-gray-500 uppercase tracking-wider"
                                              ></th>
                                            </tr>
                                          </thead>

                                          <tbody className="bg-white divide-y divide-gray-200">
                                            {loading == false ? (
                                              bankMatchData != null &&
                                              bankMatchData.length > 0 ? (
                                                bankMatchData.map(
                                                  (val, key) => (
                                                    <tr key={val.id}>
                                                      <td className="px-7 py-4 whitespace-nowrap text-md text-gray-500">
                                                        {invStatusStyle(
                                                          val.status.toLowerCase()
                                                        )}{" "}
                                                        <br></br>
                                                        <span className="text-xs text-gray-500">
                                                          {getDisplayDate(
                                                            val.updated_at
                                                          )}
                                                        </span>
                                                      </td>

                                                      <td className="px-7 py-4 whitespace-nowrap">
                                                        <div className="text-md text-gray-900">
                                                          {val.invoice_no}
                                                        </div>
                                                      </td>

                                                      <td className="px-7 py-4 whitespace-nowrap">
                                                        <div className="text-md text-gray-900">
                                                          <NumberFormat
                                                            value={val.total.toFixed(
                                                              2
                                                            )}
                                                            displayType={"text"}
                                                            thousandSeparator={
                                                              true
                                                            }
                                                            prefix={"$"}
                                                            renderText={(
                                                              value
                                                            ) => (
                                                              <div>{value}</div>
                                                            )}
                                                          />
                                                        </div>
                                                      </td>
                                                    </tr>
                                                  )
                                                )
                                              ) : (
                                                <tr>
                                                  <td
                                                    colSpan="3"
                                                    align="center"
                                                  >
                                                    <span className="px-4">
                                                      <br></br>
                                                      No matched transactions
                                                      found under this{" "}
                                                      {tabValue == 0
                                                        ? "client"
                                                        : "supplier"}
                                                      .<br></br>
                                                      <br></br>
                                                    </span>
                                                  </td>
                                                </tr>
                                              )
                                            ) : (
                                              Array.from(Array(5), (e, i) => {
                                                return (
                                                  <tr key={i}>
                                                    <td className="px-7 py-4 whitespace-nowrap">
                                                      <Skeleton />
                                                    </td>
                                                    <td className="px-7 py-4 whitespace-nowrap">
                                                      <Skeleton />
                                                    </td>
                                                    <td className="px-7 py-4 whitespace-nowrap">
                                                      <Skeleton />
                                                    </td>
                                                  </tr>
                                                );
                                              })
                                            )}
                                          </tbody>
                                        </table>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </Tab.Panel>

                          {/* Expenses */}
                          <Tab.Panel
                            key="2"
                            className={classNames(
                              "bg-white h-full  rounded-xl",
                              "focus:outline-none   ring-white ring-opacity-60"
                            )}
                          >
                            <div className="h-full divide-y divide-gray-200 flex flex-col bg-white">
                              <div className="flex-1 h-0 pb-8 overflow-y-auto">
                                <div className="flex-1 flex flex-col justify-between">
                                  <div className="px-4 divide-y divide-gray-200 sm:px-6">
                                    <div className="shadow mt-2 bg-white  overflow-hidden border-b border-gray-200 sm:rounded-lg">
                                      <div className="overflow-y-auto overflow-x-auto">
                                        <table className="min-w-full hover-row divide-y divide-gray-200">
                                          <thead className="">
                                            <tr>
                                              <th
                                                scope="col"
                                                className="px-7 py-3 text-left text-sm font-medium text-gray-500 uppercase tracking-wider"
                                              >
                                                #
                                              </th>
                                              <th
                                                scope="col"
                                                className="px-7 py-3 text-left text-sm font-medium text-gray-500 uppercase tracking-wider"
                                              >
                                                Category
                                              </th>
                                              <th
                                                scope="col"
                                                className="px-7 py-3 text-left text-sm font-medium text-gray-500 uppercase tracking-wider"
                                              >
                                                Description
                                              </th>
                                              <th
                                                scope="col"
                                                className="px-7 py-3 text-left text-sm font-medium text-gray-500 uppercase tracking-wider"
                                              >
                                                Date
                                              </th>
                                              <th
                                                scope="col"
                                                className="px-7 py-3 text-left text-sm font-medium text-gray-500 uppercase tracking-wider"
                                              >
                                                Price
                                              </th>
                                            </tr>
                                          </thead>
                                          <tbody className="bg-white divide-y divide-gray-200">
                                            {loading == false ? (
                                              expenseData.length > 0 ? (
                                                expenseData.map((val, key) => (
                                                  <tr key={val.email}>
                                                    <td className="px-7 py-4 whitespace-nowrap">
                                                      <div className="flex  items-center">
                                                        <div className="text-md font-medium text-gray-900">
                                                          {rowsPerPage * page +
                                                            key +
                                                            1}
                                                        </div>
                                                      </div>
                                                    </td>

                                                    <td className="px-7 py-4 flex items-center whitespace-nowrap">
                                                      <span className="inline-flex items-center justify-center h-12 w-12 rounded-full bg-gray-100">
                                                        <img
                                                          src={
                                                            val
                                                              .accountSubcategory
                                                              .subcategory_icon
                                                          }
                                                          alt="Subcategory"
                                                          width="24"
                                                          height="24"
                                                          className="h-8 w-auto"
                                                        />
                                                      </span>
                                                      <div className="text-md pl-4 text-gray-900">
                                                        {
                                                          val.accountSubcategory
                                                            .name
                                                        }
                                                      </div>
                                                    </td>

                                                    <td className="px-7 py-4 whitespace-nowrap">
                                                      <div className="text-md text-gray-900">
                                                        {val.name}
                                                      </div>
                                                    </td>

                                                    <td className="px-7 py-4 whitespace-nowrap">
                                                      <div className="text-md text-gray-900">
                                                        {val.date}
                                                      </div>
                                                    </td>

                                                    <td className="px-7 py-4 whitespace-nowrap">
                                                      <div className="text-md font-semibold text-gray-900">
                                                        <NumberFormat
                                                          value={val.price.toFixed(
                                                            2
                                                          )}
                                                          displayType={"text"}
                                                          thousandSeparator={
                                                            true
                                                          }
                                                          prefix={"$"}
                                                          renderText={(
                                                            value
                                                          ) => (
                                                            <div>{value}</div>
                                                          )}
                                                        />
                                                      </div>
                                                    </td>
                                                  </tr>
                                                ))
                                              ) : (
                                                <tr>
                                                  <td
                                                    colspan="7"
                                                    className="py-4"
                                                    align="center"
                                                  >
                                                    <span>
                                                      No expense found under
                                                      this{" "}
                                                      {tabValue == 0
                                                        ? "client."
                                                        : "supplier."}
                                                    </span>
                                                  </td>
                                                </tr>
                                              )
                                            ) : (
                                              Array.from(Array(5), (e, i) => {
                                                return (
                                                  <tr key={i}>
                                                    <td className="px-6 py-4 whitespace-nowrap">
                                                      <Skeleton />
                                                    </td>
                                                    <td className="px-7 py-4 whitespace-nowrap">
                                                      <Skeleton />
                                                    </td>
                                                    <td className="px-7 py-4 whitespace-nowrap">
                                                      <Skeleton />
                                                    </td>
                                                    <td className="px-7 py-4 whitespace-nowrap">
                                                      <Skeleton />
                                                    </td>
                                                    <td className="px-7 py-4 whitespace-nowrap">
                                                      <Skeleton />
                                                    </td>
                                                    <td className="px-7 py-4 whitespace-nowrap text-right text-xsm font-medium">
                                                      <Skeleton />
                                                    </td>
                                                  </tr>
                                                );
                                              })
                                            )}
                                          </tbody>
                                        </table>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </Tab.Panel>
                        </Tab.Panels>
                      </Tab.Group>
                    </div>
                  </div>
                </div>
              </div>
            </Transition.Child>
          </div>
        </div>
      </Dialog>
    </Transition.Root>
  );
}
