/* This example requires Tailwind CSS v2.0+ */
import { IoIosAddCircleOutline } from "react-icons/io";

export default function EmptyView() {
    return (
        <div className="hidden flex items-center m-7 justify-center shadow h-3/4 rounded-md bg-white">
            <div className="text-center">
                <svg
                    className="mx-auto h-12 w-12 text-brand-blue"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                    aria-hidden="true"
                >
                    <path
                        vectorEffect="non-scaling-stroke"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth={2}
                        d="M9 13h6m-3-3v6m-9 1V7a2 2 0 012-2h6l2 2h6a2 2 0 012 2v8a2 2 0 01-2 2H5a2 2 0 01-2-2z"
                    />
                </svg>
                <h3 className="mt-2 text-xl font-medium text-gray-900">Add your first client</h3>
                <p className="mt-1 text-sm text-gray-500">Get started by adding a new Client.</p>
                <div className="mt-6">
                    <div className="inline-flex rounded-md">
                        <button
                            className="inline-flex items-center justify-center pr-5 pl-2 py-1 border border-transparent text-base font-medium rounded-full text-white bg-brand-blue hover:bg-brand-darkblue"
                        >
                            <IoIosAddCircleOutline className="w-10 h-10 pr-2 pl-0" /> Add a Client
                        </button>
                    </div>
                </div>
            </div>
        </div>
    )
}
