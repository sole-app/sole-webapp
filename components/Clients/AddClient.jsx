import { Fragment, useState, useEffect } from "react";
import { Dialog, Transition } from "@headlessui/react";
import { XIcon } from "@heroicons/react/outline";
import { ErrorMessage, Field, Formik } from "formik";
import ClientSchema from "../../schemas/Client.schema";
import { makeStyles } from "@material-ui/core/styles";
import { Update, Add, Info } from "../../services/api/client.services";
import { SNACKBAR_TYPE } from "../../components/Layout/Snackbar";
import store from "../../store/store";
import { snackbarOpen } from "../../reducers/slices/snackbarSlice";
import { CircularProgress } from "@material-ui/core";
import AddressAutoComplete from "../Address/AddressAutoComplete";
import AlertDialog from "../AlertDialog";

const useStyles = makeStyles((theme) => ({
  fControl: {
    marginTop: 0,
    marginBottom: theme.spacing(1),
    width: "100%",
  },
}));

var initialValues = {
  name: "",
  email: "",
  mobile_no: "",
  ABN: "",
  website: "",
  address: "",
  business_name: "",
  google_address: false,
};

export default function AddClient(props) {
  const [loader, setLoader] = useState(true);
  const classes = useStyles();
  const [isClient, setIsClient] = useState(true);
  const [openAlert, setOpenAlert] = useState(false);
  const [msg, setMsg] = useState("");
  const [alertType, setAlertType] = useState("Error");

  const [isVisible, setIsVisible] = useState();

  function closeModal() {
    setIsVisible(false);
  }

  function openModal() {
    setIsVisible(true);
  }

  const handleFormSubmit = async (values, setSubmitting) => {
    const formData = new FormData();
    formData.append("name", values.name);
    formData.append("email", values.email);
    formData.append("mobile_no", values.mobile_no);
    formData.append("website", values.website);
    formData.append("ABN", values.ABN);
    formData.append("business_name", values.business_name);
    formData.append("type", isClient === true ? 1 : 2);

    if (values.google_address) {
      var fullAddress = values.address.toString().split(",");
      if (fullAddress.length > 1) {
        const stateSuburb = fullAddress[1].trim().split(" ");

        formData.append("address_line1", fullAddress[0]); // Address line-1
        formData.append("suburb", stateSuburb[0]); // Address
        formData.append("city", stateSuburb[0]);
        formData.append("state", stateSuburb[stateSuburb.length - 1]);
        formData.append("country", "Australia");
      } else {
        formData.append("address_line1", fullAddress[0]);
        formData.append("suburb", "");
        formData.append("city", "");
        formData.append("state", "");
        formData.append("country", "Australia");
      }
    } else {
      formData.append("address_line1", values.address);
      formData.append("suburb", "");
      formData.append("city", "");
      formData.append("state", "");
      formData.append("country", "");
    }

    var result;
    if (props.updateId) {
      formData.append("id", props.updateId);
      result = await Update(formData);
    } else {
      result = await Add(formData);
    }

    if (result.success) {
      setSubmitting(false);
      props.setIsOpen(false);
      props.setIsListUpdate(true);
      setAlertType("Success");
      setMsg(result.message);
      setOpenAlert(true);
    } else {
      setSubmitting(false);
      setAlertType("Error");
      setMsg(result.message);
      setOpenAlert(true);
    }
  };

  useEffect(async () => {
    if (props.isOpen) {
      setLoader(true);

      if (props.updateId) {
        const resultInfo = await Info(props.updateId);
        if (resultInfo.success) {
          initialValues.name = resultInfo.data.name;
          initialValues.email = resultInfo.data.email;
          initialValues.mobile_no = resultInfo.data.mobile_no;

          initialValues.ABN =
            resultInfo.data.ABN !== null ? resultInfo.data.ABN : "";
          initialValues.website =
            resultInfo.data.website !== null ? resultInfo.data.website : "";

          initialValues.business_name =
            resultInfo.data.business_name !== null
              ? resultInfo.data.business_name
              : "";

          // make full address
          var fullAddress = "";

          if (
            typeof resultInfo.data.address.address_line1 === "string" &&
            resultInfo.data.address.address_line1.length !== 0
          ) {
            fullAddress +=
              resultInfo.data.address.address_line1 != null &&
              resultInfo.data.address.address_line1 != ""
                ? resultInfo.data.address.address_line1
                : "";
            fullAddress +=
              resultInfo.data.address.city != null &&
              resultInfo.data.address.city != ""
                ? ", " + resultInfo.data.address.city
                : "";
            fullAddress +=
              resultInfo.data.address.state != null &&
              resultInfo.data.address.state != ""
                ? ", " + resultInfo.data.address.state
                : "";
            fullAddress +=
              resultInfo.data.address.country != null &&
              resultInfo.data.address.country != ""
                ? ", " + resultInfo.data.address.country
                : "";
          }

          initialValues.address = fullAddress;

          setIsClient(resultInfo.data.type == 1 ? true : false);
          setLoader(false);
        }
      } else {
        initialValues.name = "";
        initialValues.email = "";
        initialValues.mobile_no = "";
        initialValues.ABN = "";
        initialValues.website = "";
        initialValues.business_name = "";
        initialValues.address = "";
        setLoader(false);
      }
    }
  }, [props.isOpen]);

  return (
    <>
      <Transition.Root show={props.isOpen} as={Fragment}>
        <Dialog
          as="div"
          static
          className="fixed inset-0 overflow-hidden"
          onClose={props.setIsOpen}
        >
          <div className="absolute inset-0 overflow-hidden">
            <Transition.Child
              as={Fragment}
              enter="ease-in-out duration-500"
              enterFrom="opacity-0"
              enterTo="opacity-100"
              leave="ease-in-out duration-500"
              leaveFrom="opacity-100"
              leaveTo="opacity-0"
            >
              <Dialog.Overlay className="absolute inset-0 bg-gray-500 bg-opacity-75 transition-opacity pointer-events-none" />
            </Transition.Child>

            <div className="fixed inset-y-0 max-w-full right-0 flex">
              <Transition.Child
                as={Fragment}
                enter="transform transition ease-in-out duration-500 sm:duration-700"
                enterFrom="translate-x-full"
                enterTo="translate-x-0"
                leave="transform transition ease-in-out duration-500 sm:duration-700"
                leaveFrom="translate-x-0"
                leaveTo="translate-x-full"
              >
                <div className="w-screen lg:max-w-4xl">
                  <Formik
                    initialValues={initialValues}
                    validateOnChange={false}
                    validateOnBlur={false}
                    validationSchema={ClientSchema}
                    onSubmit={(values, { setSubmitting }) => {
                      handleFormSubmit(values, setSubmitting);
                    }}
                  >
                    {({
                      values,
                      errors,
                      handleChange,
                      handleSubmit,
                      handleBlur,
                      isSubmitting,
                    }) => (
                      <form
                        onSubmit={handleSubmit}
                        className="h-full divide-y divide-gray-200 flex flex-col bg-white shadow-xl"
                      >
                        <div className="flex-1 h-0 pb-8 overflow-y-auto">
                          <div className="bg-gray-100 py-4 px-4 sm:px-6 border-b border-gray-200 ">
                            <div className="flex items-center justify-between">
                              <Dialog.Title className="text-lg font-medium text-gray-800">
                                {props.updateId ? "Edit" : "New"} Client /
                                Supplier
                                <p className="mt-1 font-normal text-sm text-gray-500">
                                  Fields marked with * are mandatory
                                </p>
                              </Dialog.Title>
                              <div className="ml-3 h-7 flex items-center">
                                <button
                                  type="button"
                                  className="rounded-full hover:bg-gray-300 p-2 text-gray-500 hover:text-gray-700 focus:outline-none focus:ring-2 focus:ring-white"
                                  onClick={() => props.setIsOpen(false)}
                                >
                                  <span className="sr-only">Close panel</span>
                                  <XIcon
                                    className="h-6 w-6"
                                    aria-hidden="true"
                                  />
                                </button>
                              </div>
                            </div>
                          </div>

                          <div className="flex-1 flex flex-col justify-between">
                            <div className="px-4 divide-y divide-gray-200 sm:px-6">
                              <div className="space-y-4 border-gray-200 mb-4 pt-6 pb-7 grid grid-cols-1 gap-y-2 sm:grid-cols-2 sm:gap-x-8">
                                {!loader ? (
                                  <>
                                    <div className="sm:col-span-2">
                                      <label
                                        htmlFor="last-name"
                                        className="block text-md tracking-wider font-medium text-gray-500"
                                      >
                                        Contact Type
                                      </label>

                                      <div className="mt-1 relative flex gap-2">
                                        <button
                                          disabled
                                          className="bg-gray-200 inline-flex items-center px-3 py-2 border border-transparent  text-sm leading-4  font-medium rounded-md text-blue-700   focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                                        >
                                          <input
                                            id="client"
                                            name="contact"
                                            type="radio"
                                            value="client"
                                            onChange={() => setIsClient(true)}
                                            className="focus:ring-brand-blue h-4 w-4 text-brand-blue border-gray-500"
                                            checked={isClient ? true : false}
                                          />
                                          <label
                                            htmlFor="client"
                                            className="ml-2 block text-base font-medium text-gray-800 "
                                          >
                                            Client
                                          </label>
                                        </button>

                                        <button
                                          disabled
                                          className="bg-gray-200 inline-flex items-center px-3 py-2 border border-transparent  text-sm leading-4  font-medium rounded-md text-blue-700   focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                                        >
                                          <input
                                            id="supplier"
                                            name="contact"
                                            type="radio"
                                            value="supplier"
                                            onChange={() => setIsClient(false)}
                                            checked={isClient ? false : true}
                                            className="focus:ring-brand-blue h-4 w-4 text-brand-blue border-gray-500"
                                          />
                                          <label
                                            htmlFor="supplier"
                                            className="ml-2 block text-base font-medium text-gray-800"
                                          >
                                            Supplier
                                          </label>
                                        </button>
                                      </div>
                                    </div>

                                    <div>
                                      <label
                                        htmlFor="company"
                                        className="block text-md tracking-wider font-medium text-gray-500"
                                      >
                                        Name*
                                      </label>
                                      <div className="mt-1">
                                        <input
                                          variant="outlined"
                                          value={values.name}
                                          onChange={handleChange}
                                          onBlur={handleBlur}
                                          type="text"
                                          name="name"
                                          id="name"
                                          placeholder="Enter contact name"
                                          className={
                                            "placeholder-gray-400 py-3 px-4 block w-full border-2 border-gray-200  focus:ring-indigo-400 focus:border-indigo-400 border-gray-100 rounded-md "
                                          }
                                        />
                                        <ErrorMessage
                                          name="name"
                                          component="span"
                                          className="error_text"
                                        />
                                      </div>
                                    </div>

                                    <div>
                                      <label
                                        htmlFor="first-name"
                                        className="block text-md tracking-wider font-medium text-gray-500"
                                      >
                                        Email*
                                      </label>
                                      <div className="mt-1">
                                        <input
                                          variant="outlined"
                                          value={values.email}
                                          onChange={handleChange}
                                          onBlur={handleBlur}
                                          type="email"
                                          name="email"
                                          id="email"
                                          placeholder="Enter a valid email address"
                                          className={
                                            "placeholder-gray-400 py-3 px-4 block w-full border-2 border-gray-200  focus:ring-indigo-400 focus:border-indigo-400 border-gray-100 rounded-md "
                                          }
                                        />
                                        <ErrorMessage
                                          name="email"
                                          component="span"
                                          className="error_text"
                                        />
                                      </div>
                                    </div>

                                    <div>
                                      <label
                                        htmlFor="last-name"
                                        className="block text-md tracking-wider font-medium text-gray-500"
                                      >
                                        Phone Number
                                      </label>
                                      <div className="mt-1">
                                        <div className="flex relative">
                                          <span className="inline-flex items-center px-3 rounded-l-md border-2 border-r-0 border-gray-300 bg-gray-50 text-gray-500 sm:text-md">
                                            +61
                                          </span>
                                          <input
                                            variant="outlined"
                                            value={values.mobile_no}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            type="text"
                                            name="mobile_no"
                                            id="mobile_no"
                                            placeholder="Enter a valid phone number"
                                            className="flex-1 min-w-0 block py-3 px-4 block w-full border-2 border-gray-200  focus:ring-indigo-400 focus:border-indigo-400 border-gray-100 rounded-none rounded-r-md"
                                          />
                                        </div>
                                        <ErrorMessage
                                          name="mobile_no"
                                          component="span"
                                          className="error_text"
                                        />
                                      </div>
                                    </div>

                                    <div>
                                      {/* <label htmlFor="last-name" className="block text-md tracking-wider font-medium text-gray-500">
                                    Address
                                  </label> */}

                                      <div>
                                        <label
                                          htmlFor="last-name"
                                          className="block text-md tracking-wider font-medium text-gray-500"
                                        >
                                          Address
                                        </label>
                                        <Field
                                          name="address"
                                          component={AddressAutoComplete}
                                          value={values.address}
                                          type="A"
                                        />
                                        <ErrorMessage
                                          name="address"
                                          component="span"
                                          className="error_text"
                                        />
                                      </div>
                                    </div>

                                    <div>
                                      <label className="block text-md tracking-wider font-medium text-gray-500">
                                        Website
                                      </label>
                                      <div className="mt-1 relative">
                                        <input
                                          variant="outlined"
                                          value={values.website}
                                          onChange={handleChange}
                                          onBlur={handleBlur}
                                          type="text"
                                          name="website"
                                          id="website"
                                          placeholder="Enter a valid website url"
                                          className={
                                            "placeholder-gray-400 py-3 px-4 block w-full border-2 border-gray-200  focus:ring-indigo-400 focus:border-indigo-400 border-gray-100 rounded-md " +
                                            classes.fControl
                                          }
                                        />
                                        <ErrorMessage
                                          name="website"
                                          component="span"
                                          className="error_text"
                                        />
                                      </div>
                                    </div>

                                    <div>
                                      <label className="block text-md tracking-wider font-medium text-gray-500">
                                        ABN
                                      </label>
                                      <div className="mt-1 relative">
                                        <input
                                          variant="outlined"
                                          value={values.ABN}
                                          onChange={handleChange}
                                          onBlur={handleBlur}
                                          type="text"
                                          name="ABN"
                                          id="ABN"
                                          placeholder="Enter your contacts ABN (11 digits, no spaces)"
                                          className={
                                            "placeholder-gray-400 py-3 px-4 block w-full border-2 border-gray-200  focus:ring-indigo-400 focus:border-indigo-400 border-gray-100 rounded-md " +
                                            classes.fControl
                                          }
                                        />
                                        <ErrorMessage
                                          name="ABN"
                                          component="span"
                                          className="error_text"
                                        />
                                      </div>
                                    </div>

                                    <div className="col-span-2">
                                      <label
                                        htmlFor="last-name"
                                        className="block text-md tracking-wider font-medium text-gray-500"
                                      >
                                        Business Name
                                      </label>
                                      <div className="mt-1 relative">
                                        <input
                                          variant="outlined"
                                          value={values.business_name}
                                          onChange={handleChange}
                                          onBlur={handleBlur}
                                          type="text"
                                          name="business_name"
                                          id="business_name"
                                          placeholder="Enter your contact business name"
                                          className={
                                            "placeholder-gray-400 py-3 px-4 block w-full border-2 border-gray-200  focus:ring-indigo-400 focus:border-indigo-400 border-gray-100 rounded-md "
                                          }
                                        />
                                        <ErrorMessage
                                          name="business_name"
                                          component="span"
                                          className="error_text"
                                        />
                                      </div>
                                    </div>
                                  </>
                                ) : (
                                  <div className="box-center">
                                    <CircularProgress />
                                  </div>
                                )}
                              </div>
                            </div>
                          </div>
                        </div>
                        {/* END */}

                        <div className="flex-shrink-0  px-4 py-4 flex justify-between">
                          {/*<div className=" inset-0 ">
                            <button
                              type="button"
                              onClick={openModal}
                              className="rounded-md bg-black bg-opacity-20 px-4 py-2 text-sm font-medium text-gray-700 hover:bg-opacity-30 focus:outline-none focus-visible:ring-2 focus-visible:ring-white focus-visible:ring-opacity-75"
                            >
                              Open Modal
                            </button>
                                </div> */}

                          <div>
                            <button
                              type="submit"
                              className="ml-4 inline-flex justify-center py-2 px-4 tracking-wider border border-transparent uppercase shadow-sm  font-medium text-sm rounded-full text-white bg-green-600 hover:bg-green-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-500"
                              disabled={isSubmitting ? true : false}
                            >
                              {isSubmitting ? "Please wait..." : "Save"}
                            </button>
                          </div>
                        </div>
                      </form>
                    )}
                  </Formik>

                  {/* Modal box   */}
                  <div>
                    <div
                      className={`${
                        isVisible
                          ? "visible opacity-100"
                          : "invisible opacity-0"
                      } fixed inset-0 transition duration-500 delay-100 bg-black bg-opacity-25`}
                      onClick={closeModal}
                    />

                    <div
                      className={`${
                        isVisible
                          ? "visible opacity-100"
                          : "invisible opacity-0"
                      } fixed modal transition duration-500 inset-0 overflow-y-auto`}
                    >
                      <div className="flex min-h-full items-center justify-center p-4 text-center">
                        <div className="w-full max-w-3xl transform overflow-hidden rounded-2xl bg-white text-left align-middle shadow-xl transition-all">
                          <div className="bg-gray-100 py-4 px-4 sm:px-6 border-b border-gray-200 ">
                            <div className="flex items-center justify-between">
                              <h3 className="text-lg font-medium text-gray-800">
                                {props.updateId ? "Edit" : "New"} Client /
                                Supplier
                                <p className="mt-1 font-normal text-sm text-gray-500">
                                  Fields marked with * are mandatory
                                </p>
                              </h3>

                              <div className="ml-3 h-7 flex items-center">
                                <button
                                  type="button"
                                  className="rounded-full hover:bg-gray-300 p-2 text-gray-500 hover:text-gray-700 focus:outline-none focus:ring-2 focus:ring-white"
                                  onClick={closeModal}
                                >
                                  <span className="sr-only">Close panel</span>
                                  <XIcon
                                    className="h-6 w-6"
                                    aria-hidden="true"
                                  />
                                </button>
                              </div>
                            </div>
                          </div>

                          <div className="p-6">
                            <h3 className="text-lg font-medium leading-6 text-gray-900">
                              Complete your order
                            </h3>

                            <div className="mt-2">
                              <p className="text-sm text-gray-500">
                                Your payment has been successfully submitted.
                                We’ve sent you an email with all of the details
                                of your order.
                              </p>
                            </div>

                            <div className="mt-4">
                              <button
                                type="button"
                                className="inline-flex justify-center mt-1 rounded-md border border-transparent bg-blue-100 px-4 py-2 text-sm font-medium text-blue-900 hover:bg-blue-200 focus:outline-none focus-visible:ring-2 focus-visible:ring-blue-500 focus-visible:ring-offset-2"
                                onClick={closeModal}
                              >
                                Got it
                              </button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </Transition.Child>
            </div>
          </div>

          <AlertDialog
            id="ringtone-menu2"
            title={alertType}
            message={msg}
            keepMounted
            open={openAlert}
            onClose={setOpenAlert}
          />
        </Dialog>
      </Transition.Root>
    </>
  );
}
