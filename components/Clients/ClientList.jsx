import { useState, useEffect } from "react";
import { SearchIcon } from "@heroicons/react/outline";
import { List, Delete } from "../../services/api/client.services";
import { SNACKBAR_TYPE } from "../../components/Layout/Snackbar";
import store from "../../store/store";
import { snackbarOpen } from "../../reducers/slices/snackbarSlice";
import Skeleton from "react-loading-skeleton";
import "react-loading-skeleton/dist/skeleton.css";
import ConfirmationDialog from "../ConfirmationDialog";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import Pagination from "../Pagination";
import ReactTooltip from "react-tooltip";

let globalSearch = "";
let apiRunning = false;

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

export default function ClientList(props) {
  const [loader, setLoader] = useState(true);
  const [records, setRecords] = useState([]);
  const [confirmation, setConfirmation] = useState(false);
  const [deleteId, setDeleteId] = useState("");
  const [activeKey, setActiveKey] = useState("");
  const [tabValue, setTabValue] = useState(0);
  const [searchName, setSearchName] = useState("");

  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [paginationObj, setpaginationObj] = useState([]);

  // GET ALL LIST
  const getList = async (
    type,
    newPage,
    newRowPerPage,
    filter = {},
    resultCheck = false
  ) => {
    setLoader(true);
    props.setIsListUpdate(false);

    let queryParam = "";
    let filterByName;

    queryParam = "?filter_by[type]=";
    queryParam += type == 0 ? "client" : "supplier";
    queryParam += "&sort_by[client_id]=DESC";
    queryParam += "&page=" + (newPage + 1);
    queryParam += "&page_size=" + newRowPerPage;
    if (typeof filter.name !== "undefined") {
      queryParam += "&filter_by[name]=" + filter.name;
      filterByName = filter.name;
    } else {
      queryParam += "&filter_by[name]=" + searchName;
      filterByName = searchName;
    }

    apiRunning = true;
    const result = await List(queryParam);
    if (result.success) {
      if (resultCheck) {
        if (filterByName === globalSearch) {
          setRecords(result.data.clients_details);
          apiRunning = false;
          setpaginationObj({
            total: result.data.total || 0,
          });
        }
      } else {
        setRecords(result.data.clients_details);
        apiRunning = false;
        setpaginationObj({
          total: result.data.total || 0,
        });
      }
    }
    if (!apiRunning) {
      setLoader(false);
    }
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
    getList(tabValue, newPage, rowsPerPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value));
    getList(tabValue, page, parseInt(event.target.value));
  };

  useEffect(() => {
    if (props.isListUpdate) {
      setLoader(true);
      getList(tabValue, page, rowsPerPage);
    }
  }, [props.isListUpdate]);

  useEffect(() => {
    getList(tabValue, page, rowsPerPage, {}, true);
  }, [searchName]);

  const handleConfirmationClose = async (id) => {
    if (id) {
      const formData = new FormData();
      formData.append("id", id);
      formData.append("type", tabValue == 0 ? 1 : 2);

      const result = await Delete(formData);
      if (result.success) {
        records.splice(activeKey, 1);
        setRecords(records);
        setConfirmation(false);

        store.store.dispatch(
          snackbarOpen({
            type: SNACKBAR_TYPE.SUCCESS,
            message: result.message,
          })
        );
      } else {
        setConfirmation(false);
      }
      setDeleteId("");
    } else {
      setDeleteId("");
      setConfirmation(false);
    }
  };

  const changeName = (event) => {
    globalSearch = event.target.value;
    setSearchName(event.target.value);
  };

  const handleChange = (event, newValue) => {
    setTabValue(newValue);
    getList(newValue, 0, rowsPerPage);
    setPage(0);
    props.setIsClient(newValue);
  };

  function a11yProps(index) {
    return {
      id: `simple-tab-${index}`,
      "aria-controls": `simple-tabpanel-${index}`,
      className: "tab-btn",
    };
  }

  return (
    <div className="mt-8 w-full flex flex-col">
      <ConfirmationDialog
        id="ringtone-menu"
        title="Remove Contact?"
        message="This contact will be permanently deleted."
        keepMounted
        open={confirmation}
        onClose={handleConfirmationClose}
        value={deleteId}
      />
      <div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
        <div className="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
          <div className="shadow bg-white overflow-hidden border-b border-gray-200 sm:rounded-lg">
            <div className="grid grid-cols-5 gap-5 p-7">
              <div className="mt-1 col-span-6 relative rounded-md">
                <div className="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
                  <SearchIcon
                    className="h-5 w-5 text-gray-500"
                    aria-hidden="true"
                  />
                </div>
                <input
                  type="text"
                  name="search"
                  id="search"
                  className="bg-gray-100 focus:ring-indigo-500 focus:border-indigo-500 block w-full pl-10 sm:text-sm p-3 border-gray-300 rounded-md"
                  placeholder={
                    tabValue === 0 ? "Search Client" : "Search Supplier"
                  }
                  onChange={changeName}
                />
              </div>
            </div>
            <Box
              sx={{ borderBottom: 1, borderColor: "divider" }}
              className="main-box-tab"
            >
              <Tabs
                value={tabValue}
                onChange={handleChange}
                aria-label="basic tabs example"
              >
                <Tab label="Client" {...a11yProps(0)} />
                <Tab label="Supplier" {...a11yProps(0)} />
              </Tabs>
            </Box>
            <TabPanel value={tabValue} index={0}>
              {tabValue === 0 ? (
                <div className="main-wrap-tab-pan">
                  <table className="min-w-full hover-row divide-y divide-gray-200">
                    <thead className="">
                      <tr>
                        <th
                          scope="col"
                          className="px-7 py-3 text-left text-sm font-medium text-gray-500 uppercase tracking-wider"
                        >
                          Client Name
                        </th>
                        <th
                          scope="col"
                          className="px-7 py-3 text-left text-sm font-medium text-gray-500 uppercase tracking-wider"
                        >
                          Email
                        </th>
                        <th
                          scope="col"
                          className="px-7 py-3 text-left text-sm font-medium text-gray-500 uppercase tracking-wider"
                        >
                          Phone no.
                        </th>
                        <th
                          scope="col"
                          className="px-7 py-3 text-left text-sm font-medium text-gray-500 uppercase tracking-wider"
                        >
                          Business Name
                        </th>
                        <th
                          scope="col"
                          className="px-7 py-3 text-right text-sm font-medium text-gray-500 uppercase tracking-wider"
                        >
                          Actions
                        </th>
                      </tr>
                    </thead>
                    <tbody className="bg-white divide-y divide-gray-200">
                      {loader == false ? (
                        records.length > 0 ? (
                          records.map((val, key) => (
                            <tr key={val.client_id}>
                              <td className="px-6 py-4 whitespace-nowrap">
                                <div className="flex items-center">
                                  <div className="text-md  text-gray-900">
                                    {val.name}
                                  </div>
                                </div>
                              </td>
                              <td className="px-7 py-4 whitespace-nowrap">
                                <div className="text-md text-gray-900">
                                  {val.email ? val.email : "-"}
                                </div>
                              </td>
                              <td className="px-7 py-4 whitespace-nowrap">
                                <div className="text-md text-gray-900">
                                  {val.mobile_no ? val.mobile_no : "-"}
                                </div>
                              </td>
                              <td className="px-7 py-4 whitespace-nowrap">
                                <div className="text-md text-gray-900">
                                  {val.business_name ? val.business_name : "-"}
                                </div>
                              </td>

                              <td className="px-7 py-4 whitespace-nowrap text-right text-xsm flex justify-end items-center font-medium">
                                <button
                                  className="text-gray-700 flex px-2 py-2 text-sm hover:text-brand-blue"
                                  onClick={() => {
                                    props.setUpdateId(val.client_id);
                                    props.setIsInfoOpen(true);
                                  }}
                                >
                                  <span data-for="view" data-tip="View">
                                    <svg
                                      xmlns="http://www.w3.org/2000/svg"
                                      className="h-6 w-6"
                                      fill="none"
                                      viewBox="0 0 24 24"
                                      stroke="currentColor"
                                      strokeWidth="2"
                                    >
                                      <path
                                        strokeLinecap="round"
                                        strokeLinejoin="round"
                                        d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"
                                      />
                                      <path
                                        strokeLinecap="round"
                                        strokeLinejoin="round"
                                        d="M2.458 12C3.732 7.943 7.523 5 12 5c4.478 0 8.268 2.943 9.542 7-1.274 4.057-5.064 7-9.542 7-4.477 0-8.268-2.943-9.542-7z"
                                      />
                                    </svg>
                                  </span>
                                  <ReactTooltip
                                    id="view"
                                    className="custom-tooltip bg-gray-900"
                                    textColor="#ffffff"
                                    backgroundColor="#111827"
                                    effect="solid"
                                  />
                                </button>

                                <button
                                  className="text-gray-700 flex px-2 py-2 text-sm hover:text-brand-blue"
                                  onClick={() => {
                                    props.setUpdateId(val.client_id);
                                    props.setIsOpen(true);
                                  }}
                                >
                                  <span data-for="edit" data-tip="Edit">
                                    <svg
                                      xmlns="http://www.w3.org/2000/svg"
                                      className="h-6 w-6"
                                      fill="none"
                                      viewBox="0 0 24 24"
                                      stroke="currentColor"
                                      strokeWidth="2"
                                    >
                                      <path
                                        strokeLinecap="round"
                                        strokeLinejoin="round"
                                        d="M15.232 5.232l3.536 3.536m-2.036-5.036a2.5 2.5 0 113.536 3.536L6.5 21.036H3v-3.572L16.732 3.732z"
                                      />
                                    </svg>
                                  </span>
                                  <ReactTooltip
                                    id="edit"
                                    className="custom-tooltip bg-gray-900"
                                    textColor="#ffffff"
                                    backgroundColor="#111827"
                                    effect="solid"
                                  />
                                </button>

                                <button
                                  className="text-gray-700 flex px-2 py-2 text-sm hover:text-brand-blue"
                                  onClick={() => {
                                    setDeleteId(val.client_id);
                                    setActiveKey(key);
                                    setConfirmation(true);
                                  }}
                                >
                                  <span data-for="delete" data-tip="Delete">
                                    <svg
                                      xmlns="http://www.w3.org/2000/svg"
                                      className="h-6 w-6"
                                      fill="none"
                                      viewBox="0 0 24 24"
                                      stroke="currentColor"
                                      strokeWidth="2"
                                    >
                                      <path
                                        strokeLinecap="round"
                                        strokeLinejoin="round"
                                        d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"
                                      />
                                    </svg>
                                  </span>
                                  <ReactTooltip
                                    id="delete"
                                    className="custom-tooltip bg-gray-900"
                                    textColor="#ffffff"
                                    backgroundColor="#111827"
                                    effect="solid"
                                  />
                                </button>
                              </td>
                            </tr>
                          ))
                        ) : (
                          <tr>
                            <td colspan="7" className="py-5" align="center">
                              <span>No records found.</span>
                            </td>
                          </tr>
                        )
                      ) : (
                        Array.from(Array(5), (e, i) => {
                          return (
                            <tr key={i}>
                              <td className="px-6 py-4 whitespace-nowrap">
                                <Skeleton />
                              </td>
                              <td className="px-7 py-4 whitespace-nowrap">
                                <Skeleton />
                              </td>
                              <td className="px-7 py-4 whitespace-nowrap">
                                <Skeleton />
                              </td>
                              <td className="px-7 py-4 whitespace-nowrap">
                                <Skeleton />
                              </td>
                              <td className="px-7 py-4 whitespace-nowrap">
                                <Skeleton />
                              </td>
                              <td className="px-7 py-4 whitespace-nowrap text-right text-xsm font-medium">
                                <Skeleton />
                              </td>
                            </tr>
                          );
                        })
                      )}
                    </tbody>
                  </table>

                  {loader == false ? (
                    <Pagination
                      total={paginationObj.total}
                      page={page}
                      onPageChange={handleChangePage}
                      rowsPerPage={rowsPerPage}
                      onRowsPerPageChange={handleChangeRowsPerPage}
                    />
                  ) : null}
                </div>
              ) : null}
            </TabPanel>

            <TabPanel value={tabValue} index={1}>
              {tabValue == 1 ? (
                <div className="main-wrap-tab-pan">
                  <table className="min-w-full hover-row divide-y divide-gray-200">
                    <thead className="">
                      <tr>
                        <th
                          scope="col"
                          className="px-7 py-3 text-left text-sm font-medium text-gray-500 uppercase tracking-wider"
                        >
                          Supplier Name
                        </th>
                        <th
                          scope="col"
                          className="px-7 py-3 text-left text-sm font-medium text-gray-500 uppercase tracking-wider"
                        >
                          Email
                        </th>
                        <th
                          scope="col"
                          className="px-7 py-3 text-left text-sm font-medium text-gray-500 uppercase tracking-wider"
                        >
                          Phone no.
                        </th>
                        <th
                          scope="col"
                          className="px-7 py-3 text-left text-sm font-medium text-gray-500 uppercase tracking-wider"
                        >
                          Business Name
                        </th>
                        <th
                          scope="col"
                          className="px-7 py-3 text-right text-sm font-medium text-gray-500 uppercase tracking-wider"
                        >
                          Actions
                        </th>
                      </tr>
                    </thead>
                    <tbody className="bg-white divide-y divide-gray-200">
                      {loader == false ? (
                        records.length > 0 ? (
                          records.map((val, key) => (
                            <tr key={val.client_id}>
                              <td className="px-6 py-4 whitespace-nowrap">
                                <div className="flex items-center">
                                  <div className="text-md  text-gray-900">
                                    {val.name}
                                  </div>
                                </div>
                              </td>
                              <td className="px-7 py-4 whitespace-nowrap">
                                <div className="text-md text-gray-900">
                                  {val.email ? val.email : "-"}
                                </div>
                              </td>
                              <td className="px-7 py-4 whitespace-nowrap">
                                <div className="text-md text-gray-900">
                                  {val.mobile_no ? val.mobile_no : "-"}
                                </div>
                              </td>
                              <td className="px-7 py-4 whitespace-nowrap">
                                <div className="text-md text-gray-900">
                                  {val.business_name ? val.business_name : "-"}
                                </div>
                              </td>

                              <td className="px-7 py-4 whitespace-nowrap text-right text-xsm flex justify-end items-center font-medium">
                                <button
                                  className="text-gray-700 flex px-2 py-2 text-sm hover:text-brand-blue"
                                  onClick={() => {
                                    props.setUpdateId(val.client_id);
                                    props.setIsInfoOpen(true);
                                  }}
                                >
                                  <span data-for="view" data-tip="View">
                                    <svg
                                      xmlns="http://www.w3.org/2000/svg"
                                      className="h-6 w-6"
                                      fill="none"
                                      viewBox="0 0 24 24"
                                      stroke="currentColor"
                                      strokeWidth="2"
                                    >
                                      <path
                                        strokeLinecap="round"
                                        strokeLinejoin="round"
                                        d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"
                                      />
                                      <path
                                        strokeLinecap="round"
                                        strokeLinejoin="round"
                                        d="M2.458 12C3.732 7.943 7.523 5 12 5c4.478 0 8.268 2.943 9.542 7-1.274 4.057-5.064 7-9.542 7-4.477 0-8.268-2.943-9.542-7z"
                                      />
                                    </svg>
                                  </span>
                                  <ReactTooltip
                                    id="view"
                                    className="custom-tooltip bg-gray-900"
                                    textColor="#ffffff"
                                    backgroundColor="#111827"
                                    effect="solid"
                                  />
                                </button>

                                <button
                                  className="text-gray-700 flex px-2 py-2 text-sm hover:text-brand-blue"
                                  onClick={() => {
                                    props.setUpdateId(val.client_id);
                                    props.setIsOpen(true);
                                  }}
                                >
                                  <span data-for="edit" data-tip="Edit">
                                    <svg
                                      xmlns="http://www.w3.org/2000/svg"
                                      className="h-6 w-6"
                                      fill="none"
                                      viewBox="0 0 24 24"
                                      stroke="currentColor"
                                      strokeWidth="2"
                                    >
                                      <path
                                        strokeLinecap="round"
                                        strokeLinejoin="round"
                                        d="M15.232 5.232l3.536 3.536m-2.036-5.036a2.5 2.5 0 113.536 3.536L6.5 21.036H3v-3.572L16.732 3.732z"
                                      />
                                    </svg>
                                  </span>
                                  <ReactTooltip
                                    id="edit"
                                    className="custom-tooltip bg-gray-900"
                                    textColor="#ffffff"
                                    backgroundColor="#111827"
                                    effect="solid"
                                  />
                                </button>

                                <button
                                  className="text-gray-700 flex px-2 py-2 text-sm hover:text-brand-blue"
                                  onClick={() => {
                                    setDeleteId(val.client_id);
                                    setActiveKey(key);
                                    setConfirmation(true);
                                  }}
                                >
                                  <span data-for="delete" data-tip="Delete">
                                    <svg
                                      xmlns="http://www.w3.org/2000/svg"
                                      className="h-6 w-6"
                                      fill="none"
                                      viewBox="0 0 24 24"
                                      stroke="currentColor"
                                      strokeWidth="2"
                                    >
                                      <path
                                        strokeLinecap="round"
                                        strokeLinejoin="round"
                                        d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"
                                      />
                                    </svg>
                                  </span>
                                  <ReactTooltip
                                    id="delete"
                                    className="custom-tooltip bg-gray-900"
                                    textColor="#ffffff"
                                    backgroundColor="#111827"
                                    effect="solid"
                                  />
                                </button>
                              </td>
                            </tr>
                          ))
                        ) : (
                          <tr>
                            <td colspan="7" className="py-5" align="center">
                              <span>No records found.</span>
                            </td>
                          </tr>
                        )
                      ) : (
                        Array.from(Array(5), (e, i) => {
                          return (
                            <tr key={i}>
                              <td className="px-6 py-4 whitespace-nowrap">
                                <Skeleton />
                              </td>
                              <td className="px-7 py-4 whitespace-nowrap">
                                <Skeleton />
                              </td>
                              <td className="px-7 py-4 whitespace-nowrap">
                                <Skeleton />
                              </td>
                              <td className="px-7 py-4 whitespace-nowrap">
                                <Skeleton />
                              </td>
                              <td className="px-7 py-4 whitespace-nowrap">
                                <Skeleton />
                              </td>
                              <td className="px-7 py-4 whitespace-nowrap text-right text-xsm font-medium">
                                <Skeleton />
                              </td>
                            </tr>
                          );
                        })
                      )}
                    </tbody>
                  </table>

                  {loader == false ? (
                    <Pagination
                      total={paginationObj.total}
                      page={page}
                      onPageChange={handleChangePage}
                      rowsPerPage={rowsPerPage}
                      onRowsPerPageChange={handleChangeRowsPerPage}
                    />
                  ) : null}
                </div>
              ) : null}
            </TabPanel>
          </div>
        </div>
      </div>
    </div>
  );
}
