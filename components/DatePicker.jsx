import { DatePicker } from 'react-nice-dates';
import { enGB } from 'date-fns/locale';
import { CalendarIcon } from '@heroicons/react/outline'
import 'react-nice-dates/build/style.css';

export default function DatePickerCustomize(props) {

    if (typeof props.minDate !== 'undefined') {
        return (
            <>
                <DatePicker
                    className="relative"
                    date={props.date}
                    onDateChange={props.onDateChange}
                    locale={enGB}
                    name={props.name}
                    minimumDate={props.minDate}
                    id={props.name}
                >
                    {({ inputProps, focused }) => (
                        <input
                            className={props.inputClassName + (focused ? ' -focused' : '')}
                            {...inputProps}
                            placeholder={props.placeholder}
                            disabled={props.disabled}
                        />
                    )}
                </DatePicker>
                <div className="absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-none">
                    <CalendarIcon className="h-5 w-5 text-blue-700" aria-hidden="true" />
                </div>
            </>
        );
    } else if (typeof props.maxDate !== 'undefined') {
        return (
            <>
                <DatePicker
                    className="relative"
                    date={props.date}
                    onDateChange={props.onDateChange}
                    locale={enGB}
                    name={props.name}
                    maximumDate={props.maxDate}
                    id={props.name}
                >
                    {({ inputProps, focused }) => (
                        <input
                            className={props.inputClassName + (focused ? ' -focused' : '')}
                            {...inputProps}
                            placeholder={props.placeholder}
                            disabled={props.disabled}
                        />
                    )}
                </DatePicker>
                <div className="absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-none">
                    <CalendarIcon className="h-5 w-5 text-blue-700" aria-hidden="true" />
                </div>
            </>
        );
    }else if (typeof props.minDate !== 'undefined' && typeof props.maxDate !== 'undefined') {
        return (
            <>
                <DatePicker
                    className="relative"
                    date={props.date}
                    onDateChange={props.onDateChange}
                    locale={enGB}
                    name={props.name}
                    minimumDate={props.minDate}
                    maximumDate={props.maxDate}
                    id={props.name}
                >
                    {({ inputProps, focused }) => (
                        <input
                            className={props.inputClassName + (focused ? ' -focused' : '')}
                            {...inputProps}
                            placeholder={props.placeholder}
                            disabled={props.disabled}
                        />
                    )}
                </DatePicker>
                <div className="absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-none">
                    <CalendarIcon className="h-5 w-5 text-blue-700" aria-hidden="true" />
                </div>
            </>
        );
    }

    return (
        <>
            <DatePicker
                className="relative"
                date={props.date}
                onDateChange={props.onDateChange}
                locale={enGB}
                name={props.name}
                id={props.name}
            >
                {({ inputProps, focused }) => (
                    <input
                        className={props.inputClassName + (focused ? ' -focused' : '')}
                        {...inputProps}
                        placeholder={props.placeholder}
                        disabled={props.disabled}
                    />
                )}
            </DatePicker>
            <div className="absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-none">
                <CalendarIcon className="h-5 w-5 text-blue-700" aria-hidden="true" />
            </div>
        </>
    );
}