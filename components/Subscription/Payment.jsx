import clsx from "clsx";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import { useState } from "react";
import { useEffect } from "react";
import { Formik, Form } from "formik";
import { ErrorMessage } from "formik";
import moment from "moment";
import { CardElement, useStripe, useElements } from "@stripe/react-stripe-js";
import Styles from "../../styles/Register.module.scss";
import {
  getUserData,
  updateSubscription,
} from "../../services/cookies.services";
import {
  addSubscription,
  validateCoupon,
} from "../../services/api/subscription.services";
import PaymentSchema from "../../schemas/Payment.schema";
import store from "../../store/store";

const useStyles = makeStyles((theme) => ({
  fControl: {
    width: "100%",
  },
}));

export default function Payment(props) {
  const classes = useStyles();
  const stripe = useStripe();
  const elements = useElements();

  const storeData = store.store.getState();
  const { subscriptionType } = props;
  const [errorMessage, setErrorMsg] = useState("");
  const [codeMessage, setCodeMsg] = useState("");
  const [hasCode, setHasCode] = useState(false);

  const [price, setPrice] = useState(0);
  const [gst, setGst] = useState(0);
  const [planId, setPlanID] = useState(1);

  const [couponId, setCouponId] = useState("");
  const [trialDays, setTrialDays] = useState(30);

  useEffect(() => {
    if (subscriptionType === 0) {
      setPrice(14.99);
      setGst(1.49);
      setPlanID(1);
    } else if (subscriptionType === 1) {
      setPrice(149.99);
      setGst(14.99);
      setPlanID(2);
    }
  }, [subscriptionType]);

  useEffect(async () => {
    if (
      typeof storeData.user_details !== "undefined" &&
      typeof storeData.user_details.subscription !== "undefined"
    ) {
      let sub = storeData.user_details.subscription;
      if (typeof sub.is_free !== "undefined") {
        setTrialDays(sub.free_day);
      }
    }
  }, [storeData]);

  // Step-1: Handle form submitevent & Create paymentMethodId
  const handleFormSubmit = async (values, setSubmitting) => {
    const result = await stripe.createPaymentMethod({
      type: "card",
      card: elements.getElement(CardElement),
      billing_details: {
        email: getUserData().email,
        name: getUserData().name,
        phone: getUserData().mobile_number,
      },
    });
    await handleStripePaymentMethod(result, setSubmitting);
  };

  // Step-2: Handle Stripe PaymentMethod & Create Customer on Stripe
  const handleStripePaymentMethod = async (result, setSubmitting) => {
    if (result.error) {
      setSubmitting(false);
      setErrorMsg(result.error.message);
    } else {
      const response = await fetch("/api/create-customer", {
        method: "POST",
        mode: "same-origin",
        body: JSON.stringify({
          paymentMethodId: result.paymentMethod.id,
          email: getUserData().email,
          phone: getUserData().mobile_number,
          coupon_code: couponId,
          planId: planId,
          trialDays: trialDays,
        }),
      });

      const subscription = await response.json();

      if (
        typeof subscription.statusCode !== "undefined" &&
        subscription.statusCode !== 200
      ) {
        setSubmitting(false);
        setErrorMsg(subscription.message);
      } else {
        handleSubscription(subscription, setSubmitting);
      }
    }
  };

  // Step-3: Handle Subscription &
  const handleSubscription = (subscription, setSubmitting) => {
    // const status = subscription['latest_invoice']['payment_intent']['status']
    // const client_secret = subscription['latest_invoice']['payment_intent']['client_secret']

    const { status, latest_invoice } = subscription;
    const { paid, payment_intent } = latest_invoice;

    if (payment_intent !== null) {
      const { client_secret, status } = payment_intent;

      if (status === "requires_action") {
        stripe.confirmCardPayment(client_secret).then(function (result) {
          if (result.error) {
            // The card was declined (i.e. insufficient funds, card has expired, etc)
            setErrorMsg(result.error.message);
            setSubmitting(false);
          } else {
            // addSubscription API call
            handleAddSubscription(subscription);
          }
        });
      } else {
        // addSubscription API call
        handleAddSubscription(subscription, setSubmitting);
      }
    } else if (status === "trialing" || status === "active") {
      handleAddSubscription(subscription, setSubmitting);
    } else {
      //console.log(`handleSubscription:: No payment information received!`);
    }
  };

  const handleAddSubscription = async (subscription, setSubmitting) => {
    const formData = new FormData();
    formData.append("platform", "WEBP");
    formData.append("subscription_data", subscription.id);
    formData.append("stripe_subscription_id", subscription.id);
    formData.append("plan_id", planId);
    formData.append("stripe_customer_id", subscription.customer);
    formData.append("stripe_plan", subscription.plan.id);
    formData.append(
      "ends_at",
      moment.unix(subscription.current_period_end).format("YYYY-MM-DD HH:mm:ss")
    );
    formData.append("quantity", 1);

    if (subscription.trial_end !== "" && subscription.trial_end !== null) {
      formData.append(
        "trial_ends_at",
        moment.unix(subscription.trial_end).format("YYYY-MM-DD HH:mm:ss")
      );
    }

    const result = await addSubscription(formData);

    if (result.success) {
      setSubmitting(false);
      //update subscription details on the store
      updateSubscription(result.data.subscription_detail);
      props.setOpen(false);
    } else {
      setSubmitting(false);
    }
  };

  // Styling for Stripe CardElement
  const CARD_ELEMENT_OPTIONS = {
    iconStyle: "solid",
    hidePostalCode: true,
    style: {
      base: {
        iconColor: "#c7c7c7",
        color: "#666666",
        fontSize: "14px",
        fontFamily: '"Open Sans", sans-serif',
        fontSmoothing: "antialiased",
        "::placeholder": {
          color: "#b1b1b1",
        },
      },
      invalid: {
        color: "#e5424d",
        ":focus": {
          color: "#303238",
        },
      },
    },
  };

  const handleCouponCode = async () => {
    // FIXME: Better way to read value from inputbox? GIGSUP4762, 'coupon' => 'free-period'
    const promo_code = document.getElementById("coupon_code").value;
    const result = await validateCoupon(
      document.getElementById("coupon_code").value
    );

    if (result.success) {
      setHasCode(true);

      setCouponId(result.data.coupon[0].stripe_coupon_id);
      setCodeMsg(result.data.coupon[0].description);

      // if (promo_code === 'GIGSUP4762') {
      //     setCodeMsg('The promotional code has been applied successfully. Subscribe now to get a 12 Months free trial of Sole App.');
      // } else if (promo_code === 'SOLEOF2021') {
      //     setCodeMsg('The promotional code has been applied successfully. Subscribe now to get a 90 days free trial of Sole App.');
      // } else {
      //     setCodeMsg(result.message);
      // }
    } else {
      setHasCode(false);
      setCodeMsg("");
      // Read coupon id
      setCouponId("");
    }
  };

  const initialValues = {
    coupon_code: "",
    card_holder_name: "",
  };

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={PaymentSchema}
      onSubmit={(values, { setSubmitting }) => {
        handleFormSubmit(values, setSubmitting);
      }}
    >
      {({
        values,
        errors,
        handleChange,
        handleSubmit,
        handleBlur,
        touched,
        isSubmitting,
      }) => (
        <Form onSubmit={handleSubmit}>
          <div className={Styles.payment}>
            <h4 className={Styles.topHead}>Payment</h4>

            <p className={Styles.guide}>
              You won't be charged for the duration of your free trial{" "}
            </p>

            <div
              className={clsx(
                Styles.dFlex,
                "align-items-baseline",
                "justify-content-center"
              )}
            >
              <div className={clsx(Styles.card)}>
                <div className={Styles.paymentDetail}>
                  <div className={clsx(Styles.dFlex, "p-row", "mb-block")}>
                    <div className={Styles.lAside}>
                      <h6 className={Styles.smTit}>Your Subscription</h6>
                    </div>

                    <div className={Styles.rAside}>
                      <div className={Styles.session}>
                        {/* SubscriptionType: 0: Monthly, 1: Yearly */}
                        {subscriptionType ? (
                          <p>
                            <span>Professional Yearly</span>
                          </p>
                        ) : (
                          <p>
                            <span>Professional Monthly</span>
                          </p>
                        )}
                      </div>
                    </div>
                  </div>

                  <div
                    className={clsx(
                      Styles.dFlex,
                      "p-row",
                      "mb-block",
                      "align-items-center"
                    )}
                  >
                    <div className={Styles.lAside}>
                      <h6 className={Styles.smTit}>Coupon Code</h6>
                    </div>

                    <div
                      className={clsx(
                        Styles.rAside,
                        "dFlex",
                        "justify-content-between"
                      )}
                    >
                      <div className={clsx(Styles.session, "w100")}>
                        <TextField
                          name="coupon_code"
                          id="coupon_code"
                          label="Do you have a coupon code?"
                          helperText=""
                          type="text"
                          variant="outlined"
                          onChange={handleChange}
                          size="small"
                          maxLength="10"
                          className={classes.fControl}
                          value={values.coupon_code}
                        />
                        <ErrorMessage
                          name="coupon_code"
                          component="span"
                          className={Styles.error_text}
                        />
                      </div>

                      <div>
                        <button
                          type="button"
                          className={Styles.applyCoupon}
                          onClick={handleCouponCode}
                        >
                          Apply
                        </button>
                      </div>
                    </div>
                  </div>

                  <div className={clsx(Styles.dFlex, "p-row", "mb-block")}>
                    <div className={Styles.lAside}>
                      <h6 className={Styles.smTit}>Subscription Cost</h6>
                    </div>

                    <div className={Styles.rAside}>
                      <div
                        className={clsx(
                          Styles.cost,
                          "dFlex",
                          "justify-content-between"
                        )}
                      >
                        <span>Sub total</span>
                        <span className={Styles.tAmount}>
                          ${(price - gst).toFixed(2)}
                        </span>
                      </div>

                      <div
                        className={clsx(
                          Styles.cost,
                          "dFlex",
                          "justify-content-between"
                        )}
                      >
                        <span>GST 10%</span>
                        <span>${gst.toFixed(2)}</span>
                      </div>

                      <div className={clsx(Styles.cost, "fsize9")}>
                        {hasCode ? (
                          <div className={clsx("coupon-note")}>
                            {" "}
                            {codeMessage}{" "}
                          </div>
                        ) : (
                          ""
                        )}
                      </div>

                      <div className={Styles.subtotal}>
                        <div
                          className={clsx(
                            Styles.ttl,
                            "dFlex",
                            "justify-content-between",
                            "align-items-center"
                          )}
                        >
                          <span>Total</span>
                          <span className={Styles.tAmount}>${price}</span>
                        </div>
                      </div>
                    </div>
                  </div>

                  {/* Payment Information */}
                  <div className={clsx(Styles.dFlex, "p-row", "m-btm10")}>
                    <div className={Styles.lAside}>
                      <h6 className={Styles.smTit}>Payment Information</h6>
                    </div>
                  </div>

                  <div className={clsx("p-row")}>
                    {/* FIXME: Remove this style from here and put in scss file. */}
                    <style jsx>{`
                      .mb10 {
                        margin-bottom: 10px;
                      }
                    `}</style>

                    <div className={clsx(Styles.w100)}>
                      <TextField
                        name="card_holder_name"
                        id="card_holder_name"
                        label="Name on card *"
                        helperText=""
                        type="text"
                        variant="outlined"
                        size="small"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.card_holder_name}
                        className={classes.fControl}
                      />
                      <ErrorMessage
                        name="card_holder_name"
                        component="span"
                        className={Styles.error_text}
                      />
                    </div>

                    <div className={clsx(Styles.cardStrip, "w100", "space10")}>
                      <CardElement options={CARD_ELEMENT_OPTIONS} />
                    </div>
                    <div className={Styles.error_text}>{errorMessage}</div>
                  </div>
                </div>
              </div>
            </div>

            <div
              className={clsx(
                Styles.formWizardButtons,
                "text-center",
                "space30"
              )}
            >
              <button
                type="submit"
                className={clsx(
                  Styles.roundBtn,
                  "blue-bg",
                  "mtop0",
                  " btn-next"
                )}
                disabled={isSubmitting}
              >
                <span>
                  {" "}
                  {isSubmitting ? "Please wait..." : "Subscribe Now"}{" "}
                </span>
              </button>
            </div>

            <div
              className={clsx(Styles.poweredStripe, "text-center", "space20")}
            >
              <h6 className={Styles.smTit}>Secured Payments by stripe</h6>
              <img
                src="/images/secure-stripe.png"
                alt="Mobile"
                width={250}
                height={42}
              />
            </div>
          </div>
        </Form>
      )}
    </Formik>
  );
}
