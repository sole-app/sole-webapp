import Link from 'next/link';

export default function ExpireTrial() {
  return (
    <div className="flex items-center  w-full justify-center md:justify-start ml-5">
      <Link href="/settings/subscription">
        <a className="bg-red-500  border-2 border-red-600 rounded-md flex py-2 px-4 text-white">
          Your subscription has expired. <span className='underline pl-5'>Subscribe now </span>

          <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6 ml-8" fill="none" viewBox="0 0 24 24" stroke="currentColor" strokeWidth="2">
            <path strokeLinecap="round" strokeLinejoin="round" d="M9 5l7 7-7 7" />
          </svg>
        </a>
      </Link>
    </div>
  )
}
