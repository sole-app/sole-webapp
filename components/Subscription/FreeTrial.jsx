import Link from 'next/link';

export default function FreeTrial(props) {
  const { days } = props;

  if (days <= 7) {
    return (
      <div className="flex items-center  w-full justify-center md:justify-start ml-5">
        <Link href="/settings/subscription">
          <a className="bg-red-500  border-2 border-red-600 rounded-md flex py-2 px-4 text-white">
            You have {days} days left in your free trial. <span className='underline pl-5'>Subscribe now </span>

            <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6 ml-8" fill="none" viewBox="0 0 24 24" stroke="currentColor" strokeWidth="2">
              <path strokeLinecap="round" strokeLinejoin="round" d="M9 5l7 7-7 7" />
            </svg>
          </a>
        </Link>
      </div>
    )
  }

  return (
    <div className="flex items-center ml-3.5 w-full justify-center md:justify-start">
      <Link href="/settings/subscription">
        <a className="bg-brand-navyblue  border-2  rounded-md flex py-2 px-4 text-white">
          You have {days} days left in your free trial. <span className='underline pl-5'>Subscribe now </span>

          <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6 ml-8" fill="none" viewBox="0 0 24 24" stroke="currentColor" strokeWidth="2">
            <path strokeLinecap="round" strokeLinejoin="round" d="M9 5l7 7-7 7" />
          </svg>
        </a>
      </Link>
    </div>
  )
}
