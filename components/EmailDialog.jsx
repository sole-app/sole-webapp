import { useState, useEffect } from 'react'
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Dialog from '@material-ui/core/Dialog';
import {Update} from '../services/api/client.services';

export default function EmailDialog(props) {
    const [type, setType] = useState(!!props.type ? props.type : 'quote');

    const updateClient = async () => {
        //update client with email id
        const formData2 = new FormData();
        formData2.append('email', props.email);
        formData2.append('type', 1);
        formData2.append('id', props.client.id ? props.client.id : '');
        formData2.append('name', props.client.label ? props.client.label : '');
        await Update(formData2);
    }

    const saveDraft = () => {
        props.setDraft(true);
        props.onClose(false);
        if (!!props?.client?.id && !!props?.client?.label && props.email) {
            updateClient();
        }
        document.getElementById("draft").click();
    };

    const send = async () => {
        if (props.error) {
            return;
        }
        props.setDraft(false);
        props.onClose(false);
        if (!!props?.client?.id && !!props?.client?.label && props.email) {
            updateClient();
        }
        document.getElementById("send").click();
    };

    const updateEmail = (value) => {
        props.setError(false);
        props.setEmail(value);
        var validRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

        if (!value.match(validRegex)) {
            props.setError(true);
        }
    }

    useEffect(async () => {
        if (props.open) {
            props.setError(false)
            props.setEmail('')
            setType(props.type || type);
        }
    }, [props.open]);


    return (
        <Dialog
            sx={{ '& .MuiDialog-paper': { width: '80%', maxHeight: 435 } }}
            maxWidth="xs"
            disablePortal
            open={props.open}
        >
            <DialogTitle>Missing Information</DialogTitle>
            <DialogContent dividers>
                Please provide your client's email address so that Sole can send the {type}. Alternatively, save it as a draft.
                <input
                    variant="outlined"
                    value={props.email}
                    onChange={(e) => updateEmail(e.target.value)}
                    type="email"
                    name="email"
                    id="email"
                    placeholder="Enter a valid email address"
                    className={"placeholder-gray-400 py-3 px-4 block w-full border-2 border-gray-200  focus:ring-indigo-400 focus:border-indigo-400 border-gray-100 rounded-md "}
                />
                {props.error && <span className="error_text">Invalid email address.</span>}

            </DialogContent>
            <DialogActions>
                <button onClick={saveDraft} className="btn-no">Save as Draft</button>
                <button onClick={send} className="btn-yes">Send</button>
            </DialogActions>
        </Dialog>
    );
}