import { useState } from 'react'
import AccountSetting from './Account';
import Accountant from './Accountant';
import Subscription from './Subscription';
import NotificationSettings from './NotificationSettings';
import Link from 'next/link';

import {
  BellIcon,
  CogIcon,
  KeyIcon,
  CashIcon

} from '@heroicons/react/outline'

function classNames(...classes) {
  return classes.filter(Boolean).join(' ')
}

export default function SettingSidebar(props) {

  const { settingType, setUser } = props;

  const subNavigation = [
    {
      name: 'Profile',
      description: 'Manage your profile.',
      href: 'profile',
      icon: CogIcon,
      current: (settingType === 'profile')
    },
    {
      name: 'Accountant / Bookkeeper',
      description: 'Your tax accountant & bookkeeper details.',
      href: 'accountant',
      icon: KeyIcon,
      current: (settingType === 'accountant')
    },
    {
      name: 'Subscription',
      description: 'Manage your subscription.',
      href: 'subscription',
      icon: CashIcon,
      current: (settingType === 'subscription')
    },
    {
      name: 'Notification Settings',
      description: 'Customise the type of push notifications you want to receive.',
      href: 'notification-settings',
      icon: BellIcon,
      current: (settingType === 'notification-settings')
    },
  ]

  function loadSettingViewByType() {
    if (settingType === 'profile') {
      return <AccountSetting setUser={setUser} />
    }
    else if (settingType === 'accountant') {
      return <Accountant />
    }
    else if (settingType === 'subscription') {
      return <Subscription />
    }
    else if (settingType === 'notification-settings') {
      return <NotificationSettings />
    }
    return <AccountSetting />
  }

  return (
    <div className="h-screen flex bg-blue-gray-50 overflow-hidden">

      <div className="flex-1 flex xl:overflow-hidden">
        {/* Secondary sidebar */}
        <nav
          aria-label="Sections"
          className=" flex-shrink-0 w-80 xl:w-96 bg-white border-r border-blue-gray-200 flex flex-col">

          <div className="flex-1 min-h-0 overflow-y-auto">
            {subNavigation.map((item, index) => (
              <Link href={item.href} key={index}>
                <a
                  className={classNames(
                    item.current ? 'bg-blue-50 bg-opacity-50' : 'hover:bg-blue-50 hover:bg-opacity-50',
                    'flex items-center p-6 border-b border-blue-gray-200'
                  )}
                  aria-current={item.current ? 'page' : undefined}
                >
                  <item.icon className="flex-shrink-0 -mt-0.5 h-6 w-6 text-blue-gray-400" aria-hidden="true" />
                  <div className="ml-3">
                    <p className="font-medium  text-md text-blue-gray-900">{item.name}</p>
                    <p className="mt-1 text-sm text-blue-gray-500">{item.description}</p>
                  </div>
                </a>
              </Link>
            ))}
          </div>
        </nav>

        {/* Identify view to load based on settingType passed in props */}
        {loadSettingViewByType()}

      </div>
    </div>
  )
}
