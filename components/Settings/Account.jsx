import { useState, useEffect } from "react";
import { ErrorMessage, Form, Formik, Field } from "formik";
import { setUserName, getUserData } from "../../services/cookies.services";
import {
  deleteAccount as deleteUserAccount,
  updateUserProfile,
} from "../../services/api/user.services";
import CircularProgress from "@mui/material/CircularProgress";
import { makeStyles } from "@material-ui/core/styles";
import clsx from "clsx";
import Styles from "../../styles/Register.module.scss";
import {
  MenuItem,
  FormHelperText,
  FormControl,
  Select,
} from "@material-ui/core";
import ReactTooltip from "react-tooltip";
import { InformationCircleIcon } from "@heroicons/react/outline";
import IndustryServices from "../../services/industry.services";
import AddressAutoComplete from "../Address/AddressAutoComplete";
import UpdateProfileSchema from "../../schemas/UpdateProfile.schema";
import { FilePond, registerPlugin } from "react-filepond";
import "filepond/dist/filepond.min.css";
import FilePondPluginImageExifOrientation from "filepond-plugin-image-exif-orientation";
import FilePondPluginImagePreview from "filepond-plugin-image-preview";
import "filepond-plugin-image-preview/dist/filepond-plugin-image-preview.css";
import ConfirmationDialog from "../ConfirmationDialog";
import { useRouter } from "next/router";
import store from "../../store/store";
import AlertDialog from "../AlertDialog";

const useStyles = makeStyles((theme) => ({
  fControl: {
    // marginTop: theme.spacing(2),
    width: "100%",
  },
  formControl: {
    width: "100%",
  },
  fSelection: {
    minWidth: "223px",
  },
  selectEmpty: {
    marginTop: theme.spacing(3),
  },
  maxHeight: {
    maxHeight: 100,
  },
}));

var initialValues = {
  full_name: "",
  email: "",
  mobile_no: "",
  address: "",
  abn: "",
  business_name: "",
  industry_id: "",
  bsb_number: "",
  bank_name: "",
  account_holder: "",
  account_number: "",
  profile_image: "",
  google_address: false,
};

const industryOptions = [];
Object.keys(IndustryServices).forEach(function (key) {
  industryOptions.push(
    <MenuItem value={key} key={key}>
      {IndustryServices[key]}
    </MenuItem>
  );
});

registerPlugin(FilePondPluginImageExifOrientation, FilePondPluginImagePreview);

export default function AccountSetting(props) {
  let storeData = store.store.getState();
  const router = useRouter();
  const [files, setFiles] = useState([]);
  const classes = useStyles();
  const [loader, setLoader] = useState(true);
  const [deleteLoader, setDeleteLoader] = useState(false);
  const [confirmation, setConfirmation] = useState(false);
  const [openAlert, setOpenAlert] = useState(false);
  const [msg, setMsg] = useState("");
  const [alertType, setAlertType] = useState("Error");

  const handleFormSubmit = async (values, setSubmitting) => {
    const flName = values.full_name.trim().split(" ");
    const fName = flName[0];
    const lName = Object.keys(flName).length > 1 ? flName[1] : "";

    const formData = new FormData();
    formData.append("first_name", fName);
    formData.append("last_name", lName);
    formData.append("email", values.email);
    formData.append("mobile_no", values.mobile_no);

    if (values.google_address) {
      // Full address, format in specific fields
      var fullAddress = values.address.toString().split(",");
      if (fullAddress.length > 1 && values.address_line1 !== "") {
        const stateSuburb = fullAddress[1].trim().split(" ");

        formData.append("address_line1", fullAddress[0]); // Address line-1
        formData.append("suburb", stateSuburb[0]); // Address
        formData.append("city", stateSuburb[0]);
        formData.append("state", stateSuburb[stateSuburb.length - 1]);
        formData.append("country", "Australia");
      } else {
        formData.append("address_line1", fullAddress[0]);
        formData.append("suburb", "");
        formData.append("city", "");
        formData.append("state", "");
        formData.append("country", "Australia");
      }
    } else {
      formData.append("address_line1", values.address);
      formData.append("suburb", "");
      formData.append("city", "");
      formData.append("state", "");
      formData.append("country", "");
    }

    //formData.append('tnf', values.tnf);   // Removed as change request

    formData.append("ABN", values.abn);
    formData.append("business_name", values.business_name);
    formData.append("industry_id", values.industry_id);
    formData.append("bsb", values.bsb_number);
    formData.append("bank_name", values.bank_name);
    formData.append("account_holder", values.account_holder);
    formData.append("account_number", values.account_number);
    if (
      typeof files[0] !== "undefined" &&
      typeof files[0].file !== "undefined"
    ) {
      formData.append("profile_pic", files[0].file);
    }

    const result = await updateUserProfile(formData);
    if (result.success) {
      setUserName(fName, lName);
      setSubmitting(false);
      props.setUser(getUserData());
    } else {
      setSubmitting(false);
      setAlertType("Error");
      setMsg(result.message);
      setOpenAlert(true);
    }
  };

  useEffect(async () => {
    let data = getUserData();
    initialValues.full_name = data.full_name;
    initialValues.email = data.email;
    initialValues.mobile_no = data.mobile_number;

    // make full address
    var fullAddress = data.address.address_line1;

    if (
      typeof data.address.address_line1 === "string" &&
      data.address.address_line1.length !== 0
    ) {
      fullAddress +=
        data.address.city != null && data.address.city != ""
          ? ", " + data.address.city
          : "";
      fullAddress +=
        data.address.state != null &&
        data.address.state != "" &&
        data.address.state != data.address.city
          ? ", " + data.address.state
          : "";
      fullAddress +=
        data.address.country != null &&
        data.address.country != "" &&
        data.address.country != data.address.state
          ? ", " + data.address.country
          : "";
    }

    initialValues.address = fullAddress;
    initialValues.tnf = data.tnf;
    initialValues.abn = data.abn;
    initialValues.business_name = data.business_name;
    initialValues.industry_id = data.industry_id > 0 ? data.industry_id : "";
    initialValues.bsb_number = data.bank.bsb;
    initialValues.bank_name = data.bank.bank_name;
    initialValues.account_holder = data.bank.account_holder;
    initialValues.account_number = data.bank.account_number;

    if (data.profile_pic !== "" && data.profile_pic !== null) {
      /*if (data.profile_pic.startsWith('http://static.soleapp.com.au/')) {
                setFiles([{ source: data.profile_pic.replace('http://static.soleapp.com.au/', 'https://s3.ap-southeast-2.amazonaws.com/static.soleapp.com.au/'), options: { type: 'remote' } }]);
            }*/
      setFiles([{ source: data.profile_pic, options: { type: "remote" } }]);
    }
    setLoader(false);
  }, []);

  const handleConfirmationClose = async (deleteAccount = false) => {
    if (deleteAccount) {
      setDeleteLoader(true);

      const result = await deleteUserAccount();
      if (result.success) {
        router.push("/");
      }
      setDeleteLoader(false);
    }
    setConfirmation(false);
  };

  return (
    <div className="flex-1 max-h-screen ">
      <AlertDialog
        id="ringtone-menu2"
        title={alertType}
        message={msg}
        keepMounted
        open={openAlert}
        onClose={setOpenAlert}
      />

      <ConfirmationDialog
        id="ringtone-menu"
        title="Delete Account?"
        message="Permanently remove your Sole Account and all of its contents from the Sole app. This action is not reversible, so please continue with caution."
        keepMounted
        open={confirmation}
        onClose={handleConfirmationClose}
        value={true}
      />

      <div className="flex-1  max-h-screen hidden-scroll overflow-y-auto 2xl:mt-10">
        <div className="max-w-6xl overflow-hidden mx-auto pb-20 px-4  sm:px-6  lg:px-8">
          <h1 className="text-3xl font-semibold text-gray-900">
            Manage your profile
          </h1>
          {!loader ? (
            <Formik
              initialValues={initialValues}
              validationSchema={UpdateProfileSchema}
              onSubmit={(values, { setSubmitting }) => {
                handleFormSubmit(values, setSubmitting);
              }}
            >
              {({
                values,
                errors,
                handleChange,
                handleSubmit,
                handleBlur,
                isSubmitting,
              }) => (
                <Form
                  onSubmit={handleSubmit}
                  className="mt-6 space-y-8 divide-y divide-y-blue-gray-200"
                >
                  <div className="lg:grid grid-cols-1 items-center gap-y-6 lg:grid-cols-6 sm:gap-x-6 space-y-4 lg:space-y-0">
                    <div className="sm:col-span-6">
                      <h2 className="text-xl font-medium text-gray-900">
                        Personal Information
                      </h2>
                      <p className="mt-1 text-sm text-blue-gray-500">
                        Fields marked with * are mandatory
                      </p>
                    </div>

                    <div className="lg:col-span-1 w-44 lg:w-auto">
                      <div className="mt-1 relative items-center">
                        <FilePond
                          files={files}
                          onupdatefiles={setFiles}
                          allowMultiple={true}
                          maxFiles={1}
                          imagePreviewHeight={170}
                          imageCropAspectRatio="1:1"
                          imageResizeTargetWidth={200}
                          imageResizeTargetHeight={200}
                          stylePanelLayout="compact circle"
                          styleLoadIndicatorPosition="center bottom"
                          styleProgressIndicatorPosition="right bottom"
                          styleButtonRemoveItemPosition="left bottom"
                          styleButtonProcessItemPosition="right bottom"
                          name="files"
                          labelIdle="Upload your business logo"
                        />

                        <InformationCircleIcon
                          className="h-8 w-8 text-gray-400 ml-2 absolute top-2 right-2 bg-white rounded-full p-1"
                          data-for="profile-image"
                          data-tip
                        />
                        <ReactTooltip
                          id="profile-image"
                          className="custom-tooltip bg-gray-900"
                          textColor="#ffffff"
                          backgroundColor="#111827"
                          effect="solid"
                          aria-haspopup="true"
                        >
                          <p className="w-64">
                            Please upload your business logo here. Sole app uses
                            your logo on all invoices. The best size is 500 x
                            500 pixels.
                          </p>
                        </ReactTooltip>
                      </div>
                    </div>

                    <div className="lg:col-span-3">
                      <label
                        htmlFor="company"
                        className="block text-md tracking-wider font-medium text-gray-500"
                      >
                        Name*
                      </label>
                      <div className="mt-1">
                        <input
                          type="text"
                          name="full_name"
                          id="full_name"
                          value={values.full_name}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          className="py-3 px-4 block w-full border-2 border-gray-200  focus:ring-indigo-400 focus:border-indigo-400 border-gray-100 rounded-md"
                        />
                        <ErrorMessage
                          name="full_name"
                          component="span"
                          className="error_text"
                        />
                      </div>
                    </div>

                    <div className="lg:col-span-2">
                      <label
                        htmlFor="company"
                        className="block text-md tracking-wider font-medium text-gray-500"
                      >
                        Email Address*
                      </label>
                      <div className="mt-1">
                        <input
                          type="text"
                          name="email"
                          id="email"
                          autoComplete="email"
                          value={values.email}
                          //onChange={handleChange}
                          //onBlur={handleBlur}
                          className="bg-gray-100 cursor-not-allowed py-3 px-4 block w-full border-2 border-gray-200  focus:ring-indigo-400 focus:border-indigo-400  rounded-md"
                          disabled
                        />
                        <ErrorMessage
                          name="email"
                          component="span"
                          className="error_text"
                        />
                      </div>
                    </div>

                    <div className="lg:col-span-3">
                      <label
                        htmlFor="company"
                        className="block text-md tracking-wider font-medium text-gray-500"
                      >
                        Mobile Number*
                      </label>
                      <div className="mt-1">
                        {storeData.user_details.signup_source === 1 ? (
                          <input
                            type="text"
                            name="mobile_no"
                            id="mobile_no"
                            value={values.mobile_no}
                            //onChange={handleChange}
                            //onBlur={handleBlur}
                            className="bg-gray-100 cursor-not-allowed  py-3 px-4 block w-full border-2 border-gray-200  focus:ring-indigo-400 focus:border-indigo-400 border-gray-100 rounded-md"
                            disabled
                          />
                        ) : (
                          <input
                            type="text"
                            name="mobile_no"
                            id="mobile_no"
                            value={values.mobile_no}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            className="py-3 px-4 block w-full border-2 border-gray-200  focus:ring-indigo-400 focus:border-indigo-400 border-gray-100 rounded-md"
                          />
                        )}
                        <ErrorMessage
                          name="mobile_no"
                          component="span"
                          className="error_text"
                        />
                      </div>
                    </div>

                    <div className="lg:col-span-3">
                      {/* <label htmlFor="company" className="block text-md tracking-wider font-medium text-gray-500">
                                             Address
                                             </label> */}
                      <div>
                        <label
                          htmlFor="last-name"
                          className="block text-md tracking-wider font-medium text-gray-500"
                        >
                          Address*
                        </label>
                        <Field
                          name="address"
                          component={AddressAutoComplete}
                          value={values.address}
                          type="A"
                        />
                        <ErrorMessage
                          name="address"
                          component="span"
                          className="error_text"
                        />
                      </div>
                    </div>
                  </div>

                  {/* Business Information */}
                  <div className="pt-8 lg:grid grid-cols-1 gap-y-6 sm:grid-cols-6 sm:gap-x-6 space-y-4 lg:space-y-0">
                    <div className="sm:col-span-6">
                      <h2 className="text-xl font-medium text-blue-gray-900">
                        Business Information
                      </h2>
                      <p className="mt-1 text-sm text-blue-gray-500"></p>
                    </div>

                    <div className="sm:col-span-3">
                      <label
                        htmlFor="company"
                        className="block text-md tracking-wider font-medium text-gray-500"
                      >
                        ABN Number
                      </label>
                      <div className="mt-1">
                        <input
                          type="number"
                          name="abn"
                          id="abn"
                          value={values.abn}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          placeholder="Enter your ABN Number (11 digits, no spaces)"
                          className="py-3 px-4 block w-full border-2 border-gray-200  focus:ring-indigo-400 focus:border-indigo-400  rounded-md"
                        />
                        <ErrorMessage
                          name="abn"
                          component="span"
                          className="error_text"
                        />
                      </div>
                    </div>

                    <div className="sm:col-span-3">
                      <label
                        htmlFor="company"
                        className="block text-md tracking-wider font-medium text-gray-500"
                      >
                        Business Name*
                      </label>
                      <div className="mt-1">
                        <input
                          type="text"
                          name="business_name"
                          id="business_name"
                          value={values.business_name}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          className="py-3 px-4 block w-full border-2  border-gray-200  focus:ring-indigo-400 focus:border-indigo-400  rounded-md"
                        />
                        <ErrorMessage
                          name="business_name"
                          component="span"
                          className="error_text"
                        />
                      </div>
                    </div>

                    <div
                      className={clsx(
                        Styles.formGroup,
                        "sm:col-span-3",
                        "w-full",
                        "f-selectbox"
                      )}
                    >
                      <label
                        htmlFor="company"
                        className="block text-md mb-2 tracking-wider font-medium text-gray-500"
                      >
                        Industry*
                      </label>
                      <FormControl
                        variant="outlined"
                        className={classes.formControl}
                      >
                        {/* <InputLabel id="demo-simple-select-outlined-label">Industry*</InputLabel> */}
                        <Select
                          name="industry_id"
                          id="industry_id"
                          labelId="demo-simple-select-outlined-label"
                          className={classes.fSelection}
                          value={values.industry_id}
                          onChange={handleChange}
                          onBlur={handleBlur}
                        >
                          <MenuItem value=""></MenuItem>
                          {industryOptions}
                        </Select>
                        <FormHelperText></FormHelperText>
                      </FormControl>
                      <ErrorMessage
                        name="industry_id"
                        component="span"
                        className={Styles.error_text}
                      />
                    </div>
                  </div>

                  <div className="pt-8 grid grid-cols-1 gap-y-6 sm:grid-cols-6 sm:gap-x-6">
                    <div className="sm:col-span-6">
                      <h2 className="text-xl font-medium text-blue-gray-900">
                        Invoices Paid to
                      </h2>
                      <p className="mt-1 text-sm text-blue-gray-500">
                        By default, invoice payment details will be populated by
                        primary bank account, unless specified below
                      </p>
                    </div>

                    <div className="sm:col-span-3">
                      <label
                        htmlFor="company"
                        className="block text-md tracking-wider font-medium text-gray-500"
                      >
                        BSB Number*
                      </label>
                      <div className="mt-1">
                        <input
                          type="text"
                          name="bsb_number"
                          id="bsb_number"
                          maxLength="6"
                          value={values.bsb_number}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          className="py-3 px-4 block w-full border-2 border-gray-200  focus:ring-indigo-400 focus:border-indigo-400 border-gray-100 rounded-md"
                        />
                        <ErrorMessage
                          name="bsb_number"
                          component="span"
                          className="error_text"
                        />
                      </div>
                    </div>

                    <div className="sm:col-span-3">
                      <label
                        htmlFor="company"
                        className="block text-md tracking-wider font-medium text-gray-500"
                      >
                        Bank Name*
                      </label>
                      <div className="mt-1">
                        <input
                          type="text"
                          name="bank_name"
                          id="bank_name"
                          value={values.bank_name}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          className="py-3 px-4 block w-full border-2 border-gray-200  focus:ring-indigo-400 focus:border-indigo-400 border-gray-100 rounded-md"
                        />
                        <ErrorMessage
                          name="bank_name"
                          component="span"
                          className="error_text"
                        />
                      </div>
                    </div>

                    <div className="sm:col-span-3">
                      <label
                        htmlFor="company"
                        className="block text-md tracking-wider font-medium text-gray-500"
                      >
                        Account Holder Name*
                      </label>
                      <div className="mt-1">
                        <input
                          type="text"
                          name="account_holder"
                          id="account_holder"
                          value={values.account_holder}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          className="py-3 px-4 block w-full border-2 border-gray-200  focus:ring-indigo-400 focus:border-indigo-400 border-gray-100 rounded-md"
                        />
                        <ErrorMessage
                          name="account_holder"
                          component="span"
                          className="error_text"
                        />
                      </div>
                    </div>

                    <div className="sm:col-span-3">
                      <label
                        htmlFor="company"
                        className="block text-md tracking-wider font-medium text-gray-500"
                      >
                        Account Number*
                      </label>
                      <div className="mt-1">
                        <input
                          type="text"
                          name="account_number"
                          id="account_number"
                          value={values.account_number}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          className="py-3 px-4 block w-full border-2 border-gray-200  focus:ring-indigo-400 focus:border-indigo-400 border-gray-100 rounded-md"
                        />
                        <ErrorMessage
                          name="account_number"
                          component="span"
                          className="error_text"
                        />
                      </div>
                    </div>

                    {/* <p className="text-sm text-blue-gray-500 sm:col-span-6">
                                         This account was created on{' '}
                                         <time dateTime="2020-01-05T20:35:40">January 5, 2020, 8:35:40 PM</time>.
                                         </p> */}
                  </div>

                  <div className="pt-8 flex justify-end">
                    {/* Delete */}
                    <button
                      type="button"
                      className="ml-4 inline-flex justify-center py-2 px-4 tracking-wider border border-transparent uppercase shadow-sm  font-medium text-sm rounded-full text-white bg-red-600 hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-500"
                      disabled={deleteLoader}
                      onClick={() => {
                        //setActiveKey(key);
                        setConfirmation(true);
                      }}
                    >
                      {deleteLoader ? "Deleting..." : "Delete Account"}
                    </button>

                    {/* Save  */}
                    <button
                      type="submit"
                      className="ml-4 inline-flex justify-center py-2 px-4 tracking-wider border border-transparent uppercase shadow-sm  font-medium text-sm rounded-full text-white bg-green-600 hover:bg-green-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-500"
                      disabled={isSubmitting}
                    >
                      {isSubmitting ? "Please wait..." : "Save & Update"}
                    </button>
                  </div>
                </Form>
              )}
            </Formik>
          ) : (
            <div
              style={{
                width: "100%",
                textAlign: "center",
                padding: "50px 0px",
              }}
            >
              <CircularProgress />
            </div>
          )}
        </div>
      </div>
    </div>
  );
}
