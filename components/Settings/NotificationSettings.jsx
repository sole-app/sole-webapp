import { useState, useEffect } from "react";
import { Form, Formik } from "formik";
import CircularProgress from "@mui/material/CircularProgress";
import { getNotifications, add } from "../../services/api/notification.services";
import { Switch } from "@headlessui/react";

var initialValues = {
  invoice_toggle: false,
  quote_toggle: false,
};
function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

export default function NotificationSettings() {
  const [invoiceEnabled, setInvoiceEnabled] = useState(false);
  const [quoteEnabled, setQuoteEnabled] = useState(false);

  const [loader, setLoader] = useState(false);

  useEffect(() => {
    getData();
  }, []);

  const getData = async () => {
    setLoader(true);
    const result = await getNotifications();
    if (result.success) {
      let data = result.data.notification_setting_details || [];
      for (let i = 0; i < data.length; i++) {
        if (typeof data[i].module !== "undefined") {
          if (data[i].module === "invoice" && data[i].is_email) {
            setInvoiceEnabled(true);
          }
          if (data[i].module === "quote" && data[i].is_email) {
            setQuoteEnabled(true);
          }
        }
      }
    }
    setLoader(false);
  };

  const changeData = async (type) => {
    let val = false;
    if (type === "invoice") {
      val = !invoiceEnabled;
      setInvoiceEnabled(val);
    } else {
      val = !quoteEnabled;
      setQuoteEnabled(val);
    }
    const formData = new FormData();
    formData.append("module", type);
    formData.append("is_email", val ? 1 : 0);
    formData.append("is_push_notification", val ? 1 : 0);
    const result = await add(formData);
  };

  return (
    <div className="flex-1 max-h-screen ">
      <div className="flex-1  max-h-screen hidden-scroll overflow-y-auto 2xl:mt-10">
        <div className="max-w-6xl overflow-hidden mx-auto pb-20 px-4 sm:px-6 lg:px-8">
          <h1 className="text-3xl font-semibold text-gray-900">
            Notification Settings
          </h1>
          {!loader ? (
            <Formik
              enableReinitialize
              initialValues={initialValues}
              onSubmit={(values, { setSubmitting }) => {
                handleFormSubmit(values, setSubmitting);
              }}
            >
              {({
                values,
                errors,
                handleChange,
                handleSubmit,
                handleBlur,
                touched,
                isSubmitting,
                setFieldValue,
              }) => (
                <Form
                  onSubmit={handleSubmit}
                  className="mt-6 space-y-8 divide-y divide-y-blue-gray-200"
                >
                  <div className="lg:grid grid-cols-1 gap-y-6 sm:gap-x-6">
                    <div className="sm:col-span-6">
                      <h2 className="text-xl font-medium text-gray-900">
                        Invoice & Quote Notification
                      </h2>
                      <p className="mt-1 text-sm text-blue-gray-500">
                        Customize the type of notification you want to receive
                      </p>
                    </div>

                    <div>
                      <div className="rounded-lg border border-gray-10 bg-white mt-8 shadow-md overflow-hidden  mx-auto">
                        <ul className="divide-y divide-gray-200">
                          <li className="p-2">
                            <div className="space-x-4">
                              <div>
                                <Switch.Group
                                  as="div"
                                  className="px-2  py-3 sm:p-4"
                                >
                                  <div className="inline-flex justify-between w-full">
                                    <Switch.Label
                                      as="h3"
                                      className="text-lg leading-6 font-medium text-gray-700"
                                      passive
                                    >
                                      Invoice
                                    </Switch.Label>
                                    <div className="mt-5 sm:mt-0 sm:ml-6 sm:flex-shrink-0 sm:flex sm:items-center">
                                      <Switch
                                        checked={invoiceEnabled}
                                        onChange={(e) => {
                                          changeData("invoice");
                                        }}
                                        className={classNames(
                                          invoiceEnabled
                                            ? "bg-green-600"
                                            : "bg-gray-300",
                                          "relative inline-flex flex-shrink-0 h-6 w-11 border-2 border-transparent rounded-full cursor-pointer transition-colors ease-in-out duration-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                                        )}
                                      >
                                        <span
                                          className={classNames(
                                            invoiceEnabled
                                              ? "translate-x-5"
                                              : "translate-x-0",
                                            "pointer-events-none relative inline-block h-5 w-5 rounded-full bg-white shadow transform ring-0 transition ease-in-out duration-200"
                                          )}
                                        >
                                          <span
                                            className={classNames(
                                              invoiceEnabled
                                                ? "opacity-0 ease-out duration-100"
                                                : "opacity-100 ease-in duration-200",
                                              "absolute inset-0 h-full w-full flex items-center justify-center transition-opacity pointer-events-none"
                                            )}
                                            aria-hidden="true"
                                          >
                                            <svg
                                              className="h-3 w-3 text-gray-400"
                                              fill="none"
                                              viewBox="0 0 12 12"
                                            >
                                              <path
                                                d="M4 8l2-2m0 0l2-2M6 6L4 4m2 2l2 2"
                                                stroke="currentColor"
                                                strokeWidth={2}
                                                strokeLinecap="round"
                                                strokeLinejoin="round"
                                              />
                                            </svg>
                                          </span>
                                          <span
                                            className={classNames(
                                              invoiceEnabled
                                                ? "opacity-100 ease-in duration-200"
                                                : "opacity-0 ease-out duration-100",
                                              "absolute inset-0 h-full w-full flex items-center justify-center transition-opacity pointer-events-none"
                                            )}
                                            aria-hidden="true"
                                          >
                                            <svg
                                              className="h-3 w-3 text-green-600"
                                              fill="currentColor"
                                              viewBox="0 0 12 12"
                                            >
                                              <path d="M3.707 5.293a1 1 0 00-1.414 1.414l1.414-1.414zM5 8l-.707.707a1 1 0 001.414 0L5 8zm4.707-3.293a1 1 0 00-1.414-1.414l1.414 1.414zm-7.414 2l2 2 1.414-1.414-2-2-1.414 1.414zm3.414 2l4-4-1.414-1.414-4 4 1.414 1.414z" />
                                            </svg>
                                          </span>
                                        </span>
                                      </Switch>
                                    </div>
                                  </div>

                                  <div className="mt-1 sm:flex sm:items-start sm:justify-between">
                                    <div className="max-w-xl text-sm text-gray-500">
                                      <Switch.Description>
                                        Would you like to receive a BCC email
                                        when you send an invoice to a customer?
                                      </Switch.Description>
                                    </div>
                                  </div>

                                  {/* <div onClick={togglenotify}>
                                                                     <ChevronDownIcon
                                                                     className={classNames(
                                                                     showNotify ? 'rotate-180' : '', "h-8 w-8 text-gray-400 cursor-pointer absolute top-5 right-5 transform  transition duration-300 ease-in-out"
                                                                     )}
                                                                     />
                                                                     </div> */}
                                </Switch.Group>
                              </div>
                            </div>
                          </li>

                          <li className="p-2">
                            <div className="space-x-4">
                              <div>
                                <Switch.Group
                                  as="div"
                                  className="px-2  py-3 sm:p-4"
                                >
                                  <div className="inline-flex justify-between w-full">
                                    <Switch.Label
                                      as="h3"
                                      className="text-lg leading-6 font-medium text-gray-700"
                                      passive
                                    >
                                      Quote
                                    </Switch.Label>
                                    <div className="mt-5 sm:mt-0 sm:ml-6 sm:flex-shrink-0 sm:flex sm:items-center">
                                      <Switch
                                        checked={quoteEnabled}
                                        onChange={(e) => {
                                          changeData("quote");
                                        }}
                                        className={classNames(
                                          quoteEnabled
                                            ? "bg-green-600"
                                            : "bg-gray-300",
                                          "relative inline-flex flex-shrink-0 h-6 w-11 border-2 border-transparent rounded-full cursor-pointer transition-colors ease-in-out duration-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                                        )}
                                      >
                                        <span
                                          className={classNames(
                                            quoteEnabled
                                              ? "translate-x-5"
                                              : "translate-x-0",
                                            "pointer-events-none relative inline-block h-5 w-5 rounded-full bg-white shadow transform ring-0 transition ease-in-out duration-200"
                                          )}
                                        >
                                          <span
                                            className={classNames(
                                              quoteEnabled
                                                ? "opacity-0 ease-out duration-100"
                                                : "opacity-100 ease-in duration-200",
                                              "absolute inset-0 h-full w-full flex items-center justify-center transition-opacity pointer-events-none"
                                            )}
                                            aria-hidden="true"
                                          >
                                            <svg
                                              className="h-3 w-3 text-gray-400"
                                              fill="none"
                                              viewBox="0 0 12 12"
                                            >
                                              <path
                                                d="M4 8l2-2m0 0l2-2M6 6L4 4m2 2l2 2"
                                                stroke="currentColor"
                                                strokeWidth={2}
                                                strokeLinecap="round"
                                                strokeLinejoin="round"
                                              />
                                            </svg>
                                          </span>
                                          <span
                                            className={classNames(
                                              quoteEnabled
                                                ? "opacity-100 ease-in duration-200"
                                                : "opacity-0 ease-out duration-100",
                                              "absolute inset-0 h-full w-full flex items-center justify-center transition-opacity pointer-events-none"
                                            )}
                                            aria-hidden="true"
                                          >
                                            <svg
                                              className="h-3 w-3 text-green-600"
                                              fill="currentColor"
                                              viewBox="0 0 12 12"
                                            >
                                              <path d="M3.707 5.293a1 1 0 00-1.414 1.414l1.414-1.414zM5 8l-.707.707a1 1 0 001.414 0L5 8zm4.707-3.293a1 1 0 00-1.414-1.414l1.414 1.414zm-7.414 2l2 2 1.414-1.414-2-2-1.414 1.414zm3.414 2l4-4-1.414-1.414-4 4 1.414 1.414z" />
                                            </svg>
                                          </span>
                                        </span>
                                      </Switch>
                                    </div>
                                  </div>

                                  <div className="mt-1 sm:flex sm:items-start sm:justify-between">
                                    <div className="max-w-xl text-sm text-gray-500">
                                      <Switch.Description>
                                        Would you like to receive a BCC email
                                        when you send a quote to a customer?
                                      </Switch.Description>
                                    </div>
                                  </div>
                                </Switch.Group>
                              </div>
                            </div>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </Form>
              )}
            </Formik>
          ) : (
            <div
              style={{
                width: "100%",
                textAlign: "center",
                padding: "50px 0px",
              }}
            >
              <CircularProgress />
            </div>
          )}
        </div>
      </div>
    </div>
  );
}
