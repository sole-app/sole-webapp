import { Fragment, useState, useEffect } from "react";
import { Dialog, Transition } from "@headlessui/react";
import { XIcon } from "@heroicons/react/outline";
import { CheckIcon } from "@heroicons/react/outline";
import Payment from "../Subscription/Payment";
import { Switch } from "@headlessui/react";
import { loadStripe } from "@stripe/stripe-js";
import { Elements } from "@stripe/react-stripe-js";
import ConfirmationDialog from "../ConfirmationDialog";
import store from "../../store/store";
import { getCurrentPeriod, formatDate } from "../../helpers/common";
import { cancel, resume } from "../../services/api/subscription.services";
import { updateSubscription } from "../../services/cookies.services";

export default function Subscription() {
  const storeData = store.store.getState();
  const [open, setOpen] = useState(false);
  const [enabled, setEnabled] = useState(false);
  const [payment, setPayment] = useState(false);
  const [confirmation, setConfirmation] = useState(false);
  const [resumeLoader, setResumeLoader] = useState(false);
  const [subData, setSubData] = useState({});
  //const stripePromise = loadStripe("pk_test_51IhkoFHXdXGlciB8Si1nWlxXANvUuOyqdIkZJCgk8MDtB47EcGbnaWg2pot4vW7G85gsF9XvCOmU07j1O6t2vpcL00rnnxcd1Z");
  const stripePromise = loadStripe(
    "pk_live_51IhkoFHXdXGlciB8izk34zNLk87lGF8pFGNU8YFNUh8aTFj9QsfyVXkhqSeNt3Hqi6uxAE7oB9wzGweoOkabJLMY00n3iC6foI"
  );

  useEffect(async () => {
    if (
      typeof storeData.user_details !== "undefined" &&
      typeof storeData.user_details.subscription !== "undefined"
    ) {
      let sub = storeData.user_details.subscription;
      setSubData(sub);

      if (
        typeof sub.subscription_type !== "undefined" &&
        sub.subscription_type === "NO_SUBSCRIPTION"
      ) {
        setOpen(true);
      }
    }
  }, [storeData]);

  const handleConfirmationClose = async (confirm) => {
    if (confirm) {
      await cancelSubscription();
    }
    setConfirmation(false);
  };

  const cancelSubscription = async () => {
    const result = await cancel();
    if (result.success) {
      updateSubscription(result.data.subscription_detail);
      setSubData(result.data.subscription_detail);
    }
  };

  const resumeSubscription = async () => {
    setResumeLoader(true);
    const result = await resume();
    if (result.success) {
      updateSubscription(result.data.subscription_detail);
      setSubData(result.data.subscription_detail);
    }
    setResumeLoader(false);
  };

  return (
    <>
      <div className="flex-1 max-h-screen ">
        <ConfirmationDialog
          id="ringtone-menu"
          title="Confirm"
          message="Are you sure you want to cancel the subscription?"
          keepMounted
          open={confirmation}
          onClose={handleConfirmationClose}
          value={1}
        />
        <div className="flex-1  max-h-screen hidden-scroll overflow-y-auto 2xl:mt-10">
          <div className="max-w-6xl overflow-hidden mx-auto pt-10 pb-20 px-4  sm:px-6  lg:px-8">
            <h1 className="text-3xl font-semibold text-center text-gray-900">
              Subscription Details
            </h1>
            <div className="rounded-lg border border-gray-10 bg-white mt-8 shadow-md overflow-hidden max-w-2xl mx-auto">
              {subData.subscription_type === "NO_SUBSCRIPTION" ? (
                <ul className="divide-y divide-gray-200">
                  <li className="p-4">
                    <div className="flex items-center justify-between space-x-4">
                      <div className="flex-shrink-0">
                        <span className="text-gray-600 font-semibold">
                          Status
                        </span>
                      </div>

                      <div>
                        <span className="inline-flex items-center shadow-sm px-2.5 py-0.5 border  text-sm leading-5 font-medium rounded-full text-red-700 bg-red-100">
                          NA
                        </span>
                      </div>
                    </div>
                  </li>

                  <li className="p-4">
                    <div className="flex items-center justify-between space-x-4">
                      <div className="flex-shrink-0">
                        <span className="text-gray-600 font-semibold">
                          Subscription
                        </span>
                      </div>

                      <div>
                        <span className="inline-flex items-center shadow-sm text-gray-500 leading-5 font-medium ">
                          -
                        </span>
                      </div>
                    </div>
                  </li>
                  <li className="p-4">
                    <div className="flex items-center justify-between space-x-4">
                      <div className="flex-shrink-0">
                        <span className="text-gray-600 font-semibold">
                          Current period
                        </span>
                      </div>

                      <div>
                        <span className="inline-flex items-center shadow-sm text-gray-500 leading-5 font-medium ">
                          -
                        </span>
                      </div>
                    </div>
                  </li>
                  <li className="p-4">
                    <div className="flex items-center justify-between space-x-4">
                      <div className="flex-shrink-0">
                        <span className="text-gray-600 font-semibold">
                          Next invoice
                        </span>
                      </div>

                      <div>
                        <span className="inline-flex items-center shadow-sm text-gray-500 leading-5 font-medium ">
                          -
                        </span>
                      </div>
                    </div>
                  </li>

                  <li className="p-4">
                    <div className="flex items-center justify-between space-x-4">
                      <div className="flex-shrink-0">
                        <span className="text-gray-600 font-semibold">
                          Payment frequency
                        </span>
                      </div>

                      <div>
                        <span className="inline-flex items-center shadow-sm text-gray-500 leading-5 font-medium ">
                          -
                        </span>
                      </div>
                    </div>
                  </li>
                </ul>
              ) : (
                <ul className="divide-y divide-gray-200">
                  <li className="p-4">
                    <div className="flex items-center justify-between space-x-4">
                      <div className="flex-shrink-0">
                        <span className="text-gray-600 font-semibold">
                          Status
                        </span>
                      </div>

                      <div>
                        {subData.subscription_expire ? (
                          <span className="inline-flex items-center shadow-sm px-2.5 py-0.5 border  text-sm leading-5 font-medium rounded-full text-red-700 bg-red-100">
                            Expired
                          </span>
                        ) : (
                          <span className="inline-flex items-center shadow-sm px-2.5 py-0.5 border text-sm leading-5 font-medium rounded-full text-green-700 bg-green-100">
                            Active
                          </span>
                        )}
                      </div>
                    </div>
                  </li>

                  <li className="p-4">
                    <div className="flex items-center justify-between space-x-4">
                      <div className="flex-shrink-0">
                        <span className="text-gray-600 font-semibold">
                          Subscription
                        </span>
                      </div>

                      <div>
                        <span className="inline-flex items-center shadow-sm text-gray-500 leading-5 font-medium ">
                          {subData.payment_interval == "MONTHLY" &&
                            "Professional Monthly"}
                          {subData.payment_interval == "YEARLY" &&
                            "Professional Yearly"}
                          {subData.payment_interval == "" && "-"}
                        </span>
                      </div>
                    </div>
                  </li>
                  <li className="p-4">
                    <div className="flex items-center justify-between space-x-4">
                      <div className="flex-shrink-0">
                        <span className="text-gray-600 font-semibold">
                          Current period
                        </span>
                      </div>

                      <div>
                        <span className="inline-flex items-center shadow-sm text-gray-500 leading-5 font-medium ">
                          {subData.subscription_due_date
                            ? getCurrentPeriod(
                                subData.subscription_due_date,
                                subData.payment_interval
                              )
                            : "-"}
                        </span>
                      </div>
                    </div>
                  </li>
                  <li className="p-4">
                    <div className="flex items-center justify-between space-x-4">
                      <div className="flex-shrink-0">
                        <span className="text-gray-600 font-semibold">
                          Next invoice
                        </span>
                      </div>

                      <div>
                        <span className="inline-flex items-center shadow-sm text-gray-500 leading-5 font-medium ">
                          {subData.subscription_due_date
                            ? formatDate(subData.subscription_due_date)
                            : "-"}
                        </span>
                      </div>
                    </div>
                  </li>

                  <li className="p-4">
                    <div className="flex items-center justify-between space-x-4">
                      <div className="flex-shrink-0">
                        <span className="text-gray-600 font-semibold">
                          Payment frequency
                        </span>
                      </div>

                      <div>
                        <span className="inline-flex items-center shadow-sm text-gray-500 leading-5 font-medium ">
                          {subData.payment_interval == "MONTHLY" && "Monthly"}
                          {subData.payment_interval == "YEARLY" && "Yearly"}
                          {subData.payment_interval == "" && "-"}
                        </span>
                      </div>
                    </div>
                  </li>
                </ul>
              )}

              {subData.subscription_type === "WEB_SUBSCRIPTION" && (
                <div className="flex items-center justify-center my-6 space-x-6">
                  {subData.subscription_expire ? (
                    <button
                      className="ml-4 inline-flex justify-center py-2 px-4 tracking-wider border border-transparent uppercase shadow-sm  font-medium text-sm rounded-full text-white bg-green-600 hover:bg-green-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-500"
                      onClick={() => setOpen(true)}
                    >
                      Add subscription
                    </button>
                  ) : (
                    <>
                      {subData.is_cancel_subscription ? (
                        <button
                          className="ml-4 inline-flex justify-center py-2 px-4 tracking-wider border border-transparent uppercase shadow-sm  font-medium text-sm rounded-full text-white bg-green-600 hover:bg-green-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-500"
                          onClick={() => resumeSubscription()}
                        >
                          {resumeLoader ? "Resume..." : "Resume subscription"}
                        </button>
                      ) : (
                        <button
                          className="bg-white py-2 px-4 border border-gray-300 rounded-full tracking-wider shadow-sm text-sm uppercase font-medium text-gray-700 bg-gray-100 hover:bg-gray-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                          onClick={() => setConfirmation(true)}
                        >
                          Cancel subscription
                        </button>
                      )}
                    </>
                  )}
                </div>
              )}

              {subData.subscription_type === "NO_SUBSCRIPTION" && (
                <div className="flex items-center justify-center my-6 space-x-6">
                  <button
                    className="ml-4 inline-flex justify-center py-2 px-4 tracking-wider border border-transparent uppercase shadow-sm  font-medium text-sm rounded-full text-white bg-green-600 hover:bg-green-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-500"
                    onClick={() => setOpen(true)}
                  >
                    Add subscription
                  </button>
                </div>
              )}

              {subData.subscription_type === "ANDR_SUBSCRIPTION" && (
                <div className="flex items-center justify-center my-6 space-x-6">
                  Your subscription is created from Google so you can't manage
                  from here.
                </div>
              )}

              {subData.subscription_type === "IOS_SUBSCRIPTION" && (
                <div className="flex items-center justify-center my-6 space-x-6">
                  Your subscription is created from Apple so you can't manage
                  from here.
                </div>
              )}
            </div>
          </div>
        </div>
      </div>

      <Transition.Root show={open} as={Fragment}>
        <Dialog
          as="div"
          className="fixed inset-0 overflow-hidden"
          onClose={setOpen}
        >
          <div className="absolute inset-0 overflow-hidden">
            <Dialog.Overlay className="absolute inset-0 bg-gray-500 opacity-70" />

            <div className="pointer-events-none fixed inset-y-0 right-0 flex max-w-full">
              <Transition.Child
                as={Fragment}
                enter="transform transition ease-in-out duration-500 sm:duration-700"
                enterFrom="translate-x-full"
                enterTo="translate-x-0"
                leave="transform transition ease-in-out duration-500 sm:duration-700"
                leaveFrom="translate-x-0"
                leaveTo="translate-x-full"
              >
                <div className="pointer-events-auto w-screen max-w-full">
                  <div className="flex h-full flex-col overflow-y-scroll bg-white py-6 shadow-xl">
                    <div className="px-4 sm:px-6">
                      <div className="flex items-start justify-between">
                        <div className="ml-3 flex h-7 items-center">
                          <div className={!payment ? "hidden" : "block"}>
                            <button
                              className="rounded-md bg-white text-gray-400 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
                              onClick={() => setPayment(false)}
                            >
                              <span className="sr-only">Back</span>
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                className="h-6 w-6"
                                fill="none"
                                viewBox="0 0 24 24"
                                stroke="currentColor"
                                strokeWidth="2"
                              >
                                <path
                                  strokeLinecap="round"
                                  strokeLinejoin="round"
                                  d="M7 16l-4-4m0 0l4-4m-4 4h18"
                                />
                              </svg>
                            </button>
                          </div>
                        </div>

                        <div className="ml-3 flex h-7 items-center">
                          <button
                            type="button"
                            className="rounded-md bg-white text-gray-400 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
                            onClick={() => setOpen(false)}
                          >
                            <span className="sr-only">Close panel</span>
                            <XIcon className="h-6 w-6" aria-hidden="true" />
                          </button>
                        </div>
                      </div>
                    </div>
                    <div className="relative mt-6 flex-1 px-4 sm:px-6">
                      <div className="flex-1 max-h-screen ">
                        <div className="flex-1  max-h-screen hidden-scroll overflow-y-auto">
                          <div className="max-w-6xl overflow-hidden mx-auto pt-10 pb-8 px-4  sm:px-6  lg:px-8">
                            <div>
                              <div className="pt-2">
                                <div className="max-w-7xl mx-auto text-center px-4 sm:px-6 lg:px-8">
                                  <div className="max-w-3xl mx-auto space-y-2 lg:max-w-none">
                                    <h2 className="text-lg leading-6 font-semibold text-gray-900  tracking-wider">
                                      Choose Your Subscription Plan
                                    </h2>
                                    <p className="text-xl font-normal text-gray-600 sm:text-2xl lg:text-3xl">
                                      {subData.is_free
                                        ? `You have ${subData.free_day} days left in your free trial.`
                                        : "Your subscription has expired."}
                                    </p>
                                  </div>
                                </div>
                              </div>
                              <div className="mt-3 sm:mt-3 pt-10 pb-10 lg:pb-10">
                                <div className="relative">
                                  <div className="absolute inset-0 h-3/4" />
                                  <div className="relative z-10 max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
                                    <div className="max-w-md mx-auto space-y-4 lg:max-w-5xl lg:grid lg:grid-cols-1 lg:gap-5 lg:space-y-0">
                                      <div className="flex mx-auto flex-col rounded-lg border border-gray-100  0 shadow-lg overflow-hidden">
                                        <div
                                          className={
                                            !payment ? "block" : "hidden"
                                          }
                                        >
                                          <div className="px-6 py-8 bg-white p-6 2xl:p-10 sm:pb-6">
                                            <Switch.Group>
                                              <div className="flex items-center justify-center">
                                                <Switch.Label className="mr-4">
                                                  Monthly
                                                </Switch.Label>
                                                <Switch
                                                  checked={enabled}
                                                  onChange={setEnabled}
                                                  className={`${
                                                    enabled
                                                      ? "bg-gray-300"
                                                      : "bg-gray-300"
                                                  } relative inline-flex flex-shrink-0 h-8 w-16 border-2 border-transparent rounded-full cursor-pointer transition-colors ease-in-out duration-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500`}
                                                >
                                                  <span
                                                    className={`${
                                                      enabled
                                                        ? "translate-x-8 bg-brand-blue"
                                                        : "translate-x-1 bg-brand-blue"
                                                    } inline-block mt-0.5 h-6 w-6 transform rounded-full  transition-transform`}
                                                  />
                                                </Switch>

                                                <Switch.Label className="ml-4">
                                                  Yearly (Save $30)
                                                </Switch.Label>
                                              </div>
                                            </Switch.Group>

                                            {/* <div>
                                                                            <h3
                                                                                className="inline-flex px-4 py-1 rounded-full text-sm font-semibold tracking-wide uppercase bg-indigo-100 text-indigo-600"
                                                                                id="tier-standard"
                                                                            >
                                                                                {tier.name}
                                                                            </h3>
                                                                        </div> */}

                                            <div className="mt-10 flex items-baseline text-brand-navyblue text-5xl font-normal">
                                              <span>
                                                ${" "}
                                                {!enabled ? "14.99" : "149.99"}
                                                <span className="ml-1 text-sm font-medium text-gray-500">
                                                  /{" "}
                                                  {!enabled
                                                    ? "monthly"
                                                    : "yearly"}
                                                </span>
                                              </span>
                                            </div>

                                            <p className="mt-5 text-md 2xl:text-lg text-gray-500">
                                              Pay by the{" "}
                                              {!enabled ? "month" : "year"}, no
                                              strings attached.
                                            </p>
                                          </div>

                                          <div className="flex-1 flex flex-col justify-between px-6 pt-6 pb-8 bg-gray-50 space-y-6 p-6 2xl:p-10 sm:pt-6">
                                            <ul className="space-y-4">
                                              <li className="flex items-start">
                                                <div className="flex-shrink-0">
                                                  <CheckIcon
                                                    className="h-6 w-6 text-green-500"
                                                    aria-hidden="true"
                                                  />
                                                </div>
                                                <p className="ml-3 text-base text-gray-700">
                                                  Unlimited professional quotes
                                                  & invoices within seconds and
                                                  on-the-go.
                                                </p>
                                              </li>

                                              <li className="flex items-start">
                                                <div className="flex-shrink-0">
                                                  <CheckIcon
                                                    className="h-6 w-6 text-green-500"
                                                    aria-hidden="true"
                                                  />
                                                </div>
                                                <p className="ml-3 text-base text-gray-700">
                                                  Never lose a receipt again
                                                  with seamless expense
                                                  management.
                                                </p>
                                              </li>

                                              <li className="flex items-start">
                                                <div className="flex-shrink-0">
                                                  <CheckIcon
                                                    className="h-6 w-6 text-green-500"
                                                    aria-hidden="true"
                                                  />
                                                </div>
                                                <p className="ml-3 text-base text-gray-700">
                                                  Track your business
                                                  performance at a glance and in
                                                  real time.
                                                </p>
                                              </li>

                                              <li className="flex items-start">
                                                <div className="flex-shrink-0">
                                                  <CheckIcon
                                                    className="h-6 w-6 text-green-500"
                                                    aria-hidden="true"
                                                  />
                                                </div>
                                                <p className="ml-3 text-base text-gray-700">
                                                  No more tax surprises - match
                                                  directly out of your bank
                                                  account & track your tax
                                                  estimate live.
                                                </p>
                                              </li>

                                              <li className="flex items-start">
                                                <div className="flex-shrink-0">
                                                  <CheckIcon
                                                    className="h-6 w-6 text-green-500"
                                                    aria-hidden="true"
                                                  />
                                                </div>
                                                <p className="ml-3 text-base text-gray-700">
                                                  Enjoy special partner offers
                                                  from Sole's extended business
                                                  network.
                                                </p>
                                              </li>
                                            </ul>
                                            <div className="rounded-md mt-6">
                                              <button
                                                className="flex items-center justify-center  text-center px-3 2xl:px-5 py-3 border border-transparent text-sm 2xl:text-base font-medium rounded-full text-white bg-brand-blue hover:bg-brand-darkblue"
                                                aria-describedby="tier-standard"
                                                onClick={() => setPayment(true)}
                                              >
                                                Subscribe now
                                              </button>
                                            </div>
                                          </div>
                                        </div>

                                        {/* Payment Component goes here */}

                                        <div
                                          className={
                                            !payment ? "hidden" : "block"
                                          }
                                        >
                                          {/* <h2 className="text-lg text-center py-10 leading-6 font-semibold text-gray-700  tracking-wider">Payment Method</h2> */}
                                          <Elements stripe={stripePromise}>
                                            <Payment
                                              subscriptionType={enabled ? 1 : 0}
                                              setOpen={setOpen}
                                            />
                                          </Elements>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div className="mt-4 relative max-w-7xl mx-auto px-4 sm:px-6 lg:px-8 lg:mt-12">
                                  <div className="max-w-md mx-auto lg:max-w-5xl">
                                    <div className="rounded-lg bg-gray-200 px-6 py-8 sm:p-10 lg:flex lg:items-center">
                                      <div className="flex-1">
                                        <div className="mt-1 text-sm text-gray-600">
                                          <p className="text-sm pt-2">
                                            By subscribing to Sole, you agree to
                                            Sole's{" "}
                                            <a
                                              href="https://soleapp.com.au/privacy/"
                                              target="_blank"
                                              className="underline"
                                            >
                                              Privacy Policy
                                            </a>{" "}
                                            and{" "}
                                            <a
                                              href="https://soleapp.com.au/terms/"
                                              target="_blank"
                                              className="underline"
                                            >
                                              Terms and Conditions.
                                            </a>
                                          </p>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </Transition.Child>
            </div>
          </div>
        </Dialog>
      </Transition.Root>
    </>
  );
}
