import { useState, useEffect } from "react";
import { ErrorMessage, Form, Formik } from "formik";
import CircularProgress from "@mui/material/CircularProgress";
import { getAuditorData } from "../../services/cookies.services";
import { getAuditor, addAuditor } from "../../services/api/auditor.services";
import AccountantSchema from "../../schemas/Accountant.schema";

var initialValues = {
  accountant_name: "",
  accountant_email: "",
  bookkeeper_name: "",
  bookkeeper_email: "",
};

export default function Accountant() {
  const [loader, setLoader] = useState(false);
  useEffect(async () => {
    if (getAuditorData() != null) {
      setAccountantBookkeeper();
    } else {
      getAuditorInfo();
    }
  }, []);

  // Fetch Audito Info
  const getAuditorInfo = async () => {
    setLoader(true);

    const formData = new FormData();
    const result = await getAuditor(formData);
    if (result.success) {
      setLoader(false);
      setAccountantBookkeeper();
    } else {
      setLoader(false);
    }
  };

  const setAccountantBookkeeper = async () => {
    const auditor = getAuditorData();

    if (auditor != null) {
      if (Object.keys(auditor.accountant).length > 0) {
        initialValues.accountant_name = auditor.accountant.name;
        initialValues.accountant_email = auditor.accountant.email;
      }

      if (Object.keys(auditor.bookkeeper).length > 0) {
        initialValues.bookkeeper_name =
          auditor.bookkeeper != null ? auditor.bookkeeper.name : "";
        initialValues.bookkeeper_email =
          auditor.bookkeeper != null ? auditor.bookkeeper.email : "";
      }
    }
  };

  const handleFormSubmit = async (values, setSubmitting) => {
    const formData = new FormData();
    formData.append("accountant_name", values.accountant_name);
    formData.append("accountant_email", values.accountant_email);
    formData.append("bookkeeper_name", values.bookkeeper_name);
    formData.append("bookkeeper_email", values.bookkeeper_email);

    const result = await addAuditor(formData);
    if (result.success) {
      setSubmitting(false);

      // Get AuditorInfo
      getAuditorInfo();
    } else {
      setSubmitting(false);
    }
  };

  return (
    <div className="flex-1 max-h-screen ">
      <div className="flex-1  max-h-screen hidden-scroll overflow-y-auto 2xl:mt-10">
        <div className="max-w-6xl overflow-hidden mx-auto pb-20 px-4 sm:px-6 lg:px-8">
          <h1 className="text-3xl font-semibold text-gray-900">
            Accountant & Bookkeeper Details
          </h1>
          {!loader ? (
            <Formik
              enableReinitialize
              initialValues={initialValues}
              validationSchema={AccountantSchema}
              onSubmit={(values, { setSubmitting }) => {
                handleFormSubmit(values, setSubmitting);
              }}
            >
              {({
                values,
                errors,
                handleChange,
                handleSubmit,
                handleBlur,
                touched,
                isSubmitting,
                setFieldValue,
              }) => (
                <Form
                  onSubmit={handleSubmit}
                  className="mt-6 space-y-8 divide-y divide-y-blue-gray-200"
                >
                  <div className="lg:grid grid-cols-1 gap-y-6 sm:grid-cols-6 sm:gap-x-6">
                    <div className="sm:col-span-6">
                      <h2 className="text-xl font-medium text-gray-900">
                        Accountant Information
                      </h2>
                      <p className="mt-1 text-sm text-blue-gray-500">
                        You can setup and manage your accountant and bookkeeper
                        details here to keep your reporting simple
                      </p>
                    </div>

                    <div className="sm:col-span-3">
                      <label
                        htmlFor="company"
                        className="block text-md tracking-wider font-medium text-gray-500"
                      >
                        Accountant Name
                      </label>
                      <div className="mt-1">
                        <input
                          type="text"
                          name="accountant_name"
                          id="accountant_name"
                          value={values.accountant_name}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          className="py-3 px-4 block w-full border-2 border-gray-200  focus:ring-indigo-400 focus:border-indigo-400 border-gray-100 rounded-md"
                        />
                        <ErrorMessage
                          name="accountant_name"
                          component="span"
                          className="error_text"
                        />
                      </div>
                    </div>

                    <div className="sm:col-span-3">
                      <label
                        htmlFor="company"
                        className="block text-md tracking-wider font-medium text-gray-500"
                      >
                        Accountant Email Address
                      </label>
                      <div className="mt-1">
                        <input
                          type="text"
                          name="accountant_email"
                          id="accountant_email"
                          autoComplete="email"
                          value={values.accountant_email}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          className="py-3 px-4 block w-full border-2 border-gray-200  focus:ring-indigo-400 focus:border-indigo-400 border-gray-100 rounded-md"
                        />
                        <ErrorMessage
                          name="accountant_email"
                          component="span"
                          className="error_text"
                        />
                      </div>
                    </div>
                  </div>

                  {/* Bookkeeper Information */}
                  <div className="pt-8 lg:grid grid-cols-1 gap-y-6 sm:grid-cols-6 sm:gap-x-6  space-y-4 lg:space-y-0">
                    <div className="sm:col-span-6">
                      <h2 className="text-xl font-medium text-blue-gray-900">
                        Bookkeeper Information
                      </h2>
                      <p className="mt-1 text-sm text-blue-gray-500"></p>
                    </div>

                    <div className="sm:col-span-3">
                      <label
                        htmlFor="company"
                        className="block text-md tracking-wider font-medium text-gray-500"
                      >
                        Bookkeeper Name
                      </label>
                      <div className="mt-1">
                        <input
                          type="text"
                          name="bookkeeper_name"
                          id="bookkeeper_name"
                          value={values.bookkeeper_name}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          className="py-3 px-4 block w-full border-2 border-gray-200  focus:ring-indigo-400 focus:border-indigo-400 border-gray-100 rounded-md"
                        />
                        <ErrorMessage
                          name="bookkeeper_name"
                          component="span"
                          className="error_text"
                        />
                      </div>
                    </div>

                    <div className="sm:col-span-3">
                      <label
                        htmlFor="company"
                        className="block text-md tracking-wider font-medium text-gray-500"
                      >
                        Bookkeeper Email Address
                      </label>
                      <div className="mt-1">
                        <input
                          type="text"
                          name="bookkeeper_email"
                          id="bookkeeper_email"
                          autoComplete="email"
                          value={values.bookkeeper_email}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          className="py-3 px-4 block w-full border-2 border-gray-200  focus:ring-indigo-400 focus:border-indigo-400 border-gray-100 rounded-md"
                        />
                        <ErrorMessage
                          name="bookkeeper_email"
                          component="span"
                          className="error_text"
                        />
                      </div>
                    </div>
                  </div>

                  <div className="pt-8 flex justify-end">
                    <button
                      type="submit"
                      className="ml-4 inline-flex justify-center py-2 px-10 tracking-wider border border-transparent uppercase shadow-sm  font-medium text-sm rounded-full text-white bg-green-600 hover:bg-green-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-500"
                      disabled={isSubmitting ? true : false}
                    >
                      {isSubmitting ? "Please wait..." : "Save & Update"}
                    </button>
                  </div>
                </Form>
              )}
            </Formik>
          ) : (
            <div
              style={{
                width: "100%",
                textAlign: "center",
                padding: "50px 0px",
              }}
            >
              <CircularProgress />
            </div>
          )}
        </div>
      </div>
    </div>
  );
}
