import { Switch } from '@headlessui/react'
import { InformationCircleIcon } from '@heroicons/react/outline'
import ReactTooltip from 'react-tooltip';

function classNames(...classes) {
    return classes.filter(Boolean).join('  ')
}

export default function SuperAnnuation(props) {
    return (
        <div style={props.currentStep !== 2 ? { display: 'none' } : {}}>
            <div id="superannuation" className="">
                <div className="bg-gray-100 p-2 2xl:p-4 mt-4 mb-2 relative shadow sm:rounded-lg">
                    <Switch.Group as="div" className=" flex items-center justify-between  p-3  2xl:p-0">
                        <div className="flex justify-between">
                            <Switch.Label as="h3" className="text-lg 2xl:w-96 leading-6 font-medium text-gray-700" passive>
                                Did you make any after-tax personal Super contributions this financial year?

                                <InformationCircleIcon
                                    className="h-6 w-6 text-gray-400 ml-2  focus:outline-none  inline-flex"
                                    data-for='superannuation_info'
                                    data-tip
                                />
                            </Switch.Label>


                            <ReactTooltip id='superannuation_info' className='custom-tooltip bg-gray-900' textColor='#ffffff' backgroundColor='#111827' effect='solid' aria-haspopup='true'>
                                <p className="w-64">Any personal super contributions coming from your take-home page: these contributions are in addition to any compulsory super contributions made and does not include super contributions made through a saalry-sacrifice arrangement.</p>
                            </ReactTooltip>
                        </div>

                        <div className="flex items-center">
                            <Switch
                                checked={props.formData['has_superannuation']}
                                onChange={() => {
                                    if (props.formData['has_superannuation']) {
                                        props.assignedVal('superannuation', 0);
                                    }
                                    props.assignedVal('has_superannuation', !props.formData['has_superannuation'])
                                }}
                                className={classNames(
                                    props.formData['has_superannuation'] ? 'bg-green-600' : 'bg-gray-300',
                                    'relative inline-flex flex-shrink-0 h-6 w-11 border-2 border-transparent rounded-full cursor-pointer transition-colors ease-in-out duration-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500'
                                )}
                            >
                                <span
                                    className={classNames(
                                        props.formData['has_superannuation'] ? 'translate-x-5' : 'translate-x-0',
                                        'pointer-events-none relative inline-block h-5 w-5 rounded-full bg-white shadow transform ring-0 transition ease-in-out duration-200'
                                    )}
                                >
                                    <span
                                        className={classNames(
                                            props.formData['has_superannuation'] ? 'opacity-0 ease-out duration-100' : 'opacity-100 ease-in duration-200',
                                            'absolute inset-0 h-full w-full flex items-center justify-center transition-opacity pointer-events-none'
                                        )}
                                        aria-hidden="true"
                                    >
                                        <svg className="h-3 w-3 text-gray-400" fill="none" viewBox="0 0 12 12">
                                            <path
                                                d="M4 8l2-2m0 0l2-2M6 6L4 4m2 2l2 2"
                                                stroke="currentColor"
                                                strokeWidth={2}
                                                strokeLinecap="round"
                                                strokeLinejoin="round"
                                            />
                                        </svg>
                                    </span>
                                    <span
                                        className={classNames(
                                            props.formData['has_superannuation'] ? 'opacity-100 ease-in duration-200' : 'opacity-0 ease-out duration-100',
                                            'absolute inset-0 h-full w-full flex items-center justify-center transition-opacity pointer-events-none'
                                        )}
                                        aria-hidden="true"
                                    >
                                        <svg className="h-3 w-3 text-green-600" fill="currentColor" viewBox="0 0 12 12">
                                            <path d="M3.707 5.293a1 1 0 00-1.414 1.414l1.414-1.414zM5 8l-.707.707a1 1 0 001.414 0L5 8zm4.707-3.293a1 1 0 00-1.414-1.414l1.414 1.414zm-7.414 2l2 2 1.414-1.414-2-2-1.414 1.414zm3.414 2l4-4-1.414-1.414-4 4 1.414 1.414z" />
                                        </svg>
                                    </span>
                                </span>
                            </Switch>
                        </div>
                    </Switch.Group>

                    <div className={classNames(props.formData['has_superannuation'] ? 'block' : 'hidden', "space-y-6  items-end grid gap-y-2 sm:pt-0 grid-cols-0 2xl:grid-cols-2 sm:gap-x-8")}>
                        <div className='mt-5'>
                            <div className="flex justify-between">
                                <label htmlFor="company" className="block text-md tracking-wider font-medium text-gray-500">
                                    Total Contributions*
                                </label>
                            </div>
                            <div className="flex justify-between mt-1 relative ">
                                <div className="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
                                    <span className="text-gray-500 font-semibold sm:text-md">$</span>
                                </div>
                                <input
                                    type="number"
                                    name="superannuation"
                                    id="superannuation"
                                    className="placeholder-gray-400 py-3 pl-7 select-none sm:text-md block w-full border-2 border-gray-200  focus:ring-indigo-400 focus:border-indigo-400 border-gray-100 rounded-md"
                                    placeholder="0.00"
                                    aria-describedby="price-currency"
                                    onChange={(val) => {
                                        if (val.target.value) {
                                            props.assignedVal('superannuation', val.target.value);
                                        } else {
                                            props.assignedVal('superannuation', 0);
                                        }
                                        //props.assignedVal('taxable_income',props.formData['diffrence'] - val.target.value);
                                    }}
                                    onBlur={(val) => {
                                        if (val.target.value) {
                                            val.target.value = parseFloat(val.target.value).toFixed(2);
                                            // props.assignedVal('superannuation', parseFloat(val.target.value).toFixed(2));
                                        }
                                    }}
                                    defaultValue={parseFloat(props.formData['superannuation']) > 0 ? props.formData['superannuation'] : null}
                                />
                            </div>
                        </div>
                    </div>

                    <div className="block space-y-6 items-end grid gap-y-2 sm:pt-0 grid-cols-0 2xl:grid-cols-3 sm:gap-x-8 mt-5 min-w-full">

                        <div className='mt-5'>
                            <div className="flex justify-between">
                                <label htmlFor="company" className="block text-md tracking-wider font-medium text-gray-500">
                                    Total Taxable Income
                                </label>
                            </div>

                            <div className="flex justify-between mt-1 relative ">
                                <div className="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
                                    <span className="text-gray-500 font-semibold sm:text-md">$</span>
                                </div>
                                <input
                                    type="text"
                                    name="taxable_income"
                                    id="taxable_income"
                                    className="placeholder-gray-400 py-3 pl-7 font-bold text-gray-700 border-indigo-400 bg-gray-100 select-none sm:text-lg block w-full border-2   focus:ring-green-700 focus:border-gray-500  rounded-md"
                                    placeholder="0.00"
                                    aria-describedby="price-currency"
                                    value={parseFloat(props.formData['taxable_income']).toFixed(2)}
                                    readOnly={true}
                                    disabled={true}
                                    onChange={(val) => props.assignedVal('taxable_income', val.target.value)}
                                />
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>

    );
}
