import { Switch } from '@headlessui/react'
import { PlusSmIcon, InformationCircleIcon } from '@heroicons/react/outline'
import ReactTooltip from 'react-tooltip';
import { XCircleIcon } from '@heroicons/react/solid'

function classNames(...classes) {
    return classes.filter(Boolean).join('  ')
}

export default function CapitalGain(props) {

    const calcualtion = () => {
        let total = 0;
        props.formData['asset_list'].map((v, k) => {
            if (v['owned_12_months']) {
                total += v['sale_price'] === "" ? 0 : v['sale_price'] - v['purchase_price'];
                if (total > 0) {
                    total = total / 2
                }
            } else {
                total += v['sale_price'] - v['purchase_price'];
            }
        });
        props.assignedVal('current_year_capital_gain', total);
        props.assignedVal('diffrence', (total - props.formData['previous_year_capital_loss']));
    }

    return (
        <div style={props.currentStep != 1 ? { display: 'none' } : {}}>
            <div id="capital-gain" className="mt-2">
                <div className="bg-gray-100 p-2 2xl:p-4 mt-4 mb-2 relative shadow sm:rounded-lg">
                    <Switch.Group as="div" className="flex items-center justify-between  p-3  2xl:p-0">
                        <div>
                            <Switch.Label as="h3" className="text-lg leading-6 w-80 2xl:w-full   font-medium text-gray-700">
                                Did you make any capital gains this financial year?*
                                <InformationCircleIcon
                                    className="h-6 w-6 text-gray-400 ml-2  focus:outline-none inline-flex focus:outline-none"
                                    data-for='info11'
                                    data-tip
                                />
                            </Switch.Label>

                            <ReactTooltip id='info11' className='custom-tooltip bg-gray-900' textColor='#ffffff' backgroundColor='#111827' effect='solid' aria-haspopup='true'>
                                <p className="w-64">You need to report capital gains and losses in your income tax return and pay tax on your capital gains.</p>
                            </ReactTooltip>
                        </div>

                        <div className="flex items-center">
                            <Switch
                                checked={props.formData['capital_gain_tax']}
                                onChange={() => props.assignedVal('capital_gain_tax', !props.formData['capital_gain_tax'])}
                                className={classNames(
                                    props.formData['capital_gain_tax'] ? 'bg-green-600' : 'bg-gray-300',
                                    'relative inline-flex flex-shrink-0 h-6 w-11 border-2 border-transparent rounded-full cursor-pointer transition-colors ease-in-out duration-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500'
                                )}
                            >
                                <span
                                    className={classNames(
                                        props.formData['capital_gain_tax'] ? 'translate-x-5' : 'translate-x-0',
                                        'pointer-events-none relative inline-block h-5 w-5 rounded-full bg-white shadow transform ring-0 transition ease-in-out duration-200'
                                    )}
                                >
                                    <span
                                        className={classNames(
                                            props.formData['capital_gain_tax'] ? 'opacity-0 ease-out duration-100' : 'opacity-100 ease-in duration-200',
                                            'absolute inset-0 h-full w-full flex items-center justify-center transition-opacity pointer-events-none'
                                        )}
                                        aria-hidden="true"
                                    >
                                        <svg className="h-3 w-3 text-gray-400" fill="none" viewBox="0 0 12 12">
                                            <path
                                                d="M4 8l2-2m0 0l2-2M6 6L4 4m2 2l2 2"
                                                stroke="currentColor"
                                                strokeWidth={2}
                                                strokeLinecap="round"
                                                strokeLinejoin="round"
                                            />
                                        </svg>
                                    </span>
                                    <span
                                        className={classNames(
                                            props.formData['capital_gain_tax'] ? 'opacity-100 ease-in duration-200' : 'opacity-0 ease-out duration-100',
                                            'absolute inset-0 h-full w-full flex items-center justify-center transition-opacity pointer-events-none'
                                        )}
                                        aria-hidden="true"
                                    >
                                        <svg className="h-3 w-3 text-green-600" fill="currentColor" viewBox="0 0 12 12">
                                            <path d="M3.707 5.293a1 1 0 00-1.414 1.414l1.414-1.414zM5 8l-.707.707a1 1 0 001.414 0L5 8zm4.707-3.293a1 1 0 00-1.414-1.414l1.414 1.414zm-7.414 2l2 2 1.414-1.414-2-2-1.414 1.414zm3.414 2l4-4-1.414-1.414-4 4 1.414 1.414z" />
                                        </svg>
                                    </span>
                                </span>
                            </Switch>
                        </div>
                    </Switch.Group>

                    <div className={classNames(props.formData['capital_gain_tax'] ? 'block' : 'hidden')}>
                        <div>
                            <table className="min-w-full mt-4 border-none">
                                <thead>
                                    <tr>
                                        {/* <th  scope="col" className="pr-7 py-3 w-8 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                                <span className="sr-only">Number</span>
                                            </th> */}
                                        <th scope="col" className="pr-3 2xl:pr-7 py-3 text-left text-sm font-medium text-gray-500  tracking-wider">
                                            Assets
                                        </th>
                                        {/* <th scope="col" className="pr-3 2xl:pr-7 py-3 text-left w-28 inline-flex items-center justify-center text-sm font-medium text-gray-500  " >   
                                                <span >12 Months Old?</span>
                                                <InformationCircleIcon
                                                    className="h-6 w-6 text-gray-400 ml-2  focus:outline-none inline-flex"
                                                    data-for='old-month'
                                                    data-tip
                                                />
                                                <ReactTooltip id='old-month' className='custom-tooltip bg-gray-900'   textColor='#ffffff' backgroundColor='#111827' effect='solid' aria-haspopup='true'>
                                                    <p className="w-64">Owned more than 12 months?  </p>
                                                </ReactTooltip>  
                                            </th> */}
                                        <th scope="col" className="pr-3 2xl:pr-7  py-3 w-40 text-left text-sm font-medium text-gray-500  tracking-wider">
                                            Purchase Price
                                        </th>
                                        <th scope="col" className="pr-3 2xl:pr-7 py-3 w-32 text-left text-sm font-medium text-gray-500  tracking-wider">
                                            Sale Price
                                        </th>
                                        <th scope="col" className="pr-7 py-3 w-32 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                            Gain/Loss
                                        </th>
                                        <th scope="col" className="pr-3 2xl:pr-7 py-3 text-left text-sm font-medium text-gray-500  tracking-wider">
                                            <span className="sr-only">Action</span>
                                        </th>
                                    </tr>
                                </thead>

                                <tbody>
                                    {props.formData['asset_list'].map((v, k) => {
                                        return <>
                                            <tr className='bg-white' key={k}>
                                                {/* <td className="pr-4 py-4 whitespace-nowrap">
                                                    <div className="text-gray-900 pr-4">{k + 1}</div>
                                                </td> */}

                                                <td className="px-4 py-4 whitespace-nowrap">

                                                    <div className="relative py-2.5 px-4 block w-full border-2 bg-white border-gray-200 focus-within:ring-indigo-400 focus-within:border-indigo-400 border-gray-100 rounded-md">
                                                        {/* <label htmlFor="name"  className="absolute -top-2 bg-white rounded-tl-md rounded-tr-md tracking-wide left-2 -mt-px inline-block pr-1 pl-2 text-sm font-medium text-gray-500">Description</label> */}
                                                        <input
                                                            type="text"
                                                            className="block w-40 2xl:w-full  border-0 p-0  text-gray-900 placeholder-gray-500 focus:ring-0 sm:text-sm"
                                                            placeholder="Enter asset description/type"
                                                            tabIndex="1"
                                                            value={props.formData['asset_list'][k]['description']}
                                                            onChange={(val) => props.assignedVal('asset_list', val.target.value, k, 'description')}
                                                        />
                                                    </div>
                                                </td>


                                                <td className="pr-4 py-4 whitespace-nowrap">
                                                    <div className="relative py-2 pr-4 block w-full border-2 bg-white  border-gray-200 focus-within:ring-indigo-400 focus-within:border-indigo-400 border-gray-100 rounded-md">
                                                        <div className="flex justify-between relative ">
                                                            <div className="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
                                                                <span className="text-gray-500 font-semibold sm:text-md">$</span>
                                                            </div>
                                                            <input
                                                                type="text"
                                                                className="placeholder-gray-400  pl-7 block w-full  border-0 p-0 text-gray-600  focus:ring-0 sm:text-md"
                                                                placeholder="0.00"
                                                                aria-describedby="price-currency"
                                                                tabIndex="2"
                                                                value={props.formData['asset_list'][k]['purchase_price']}
                                                                onChange={(val) => {
                                                                    props.assignedVal('asset_list', val.target.value, k, 'purchase_price');
                                                                    calcualtion();
                                                                }}
                                                                onBlur={(val) => {
                                                                    props.assignedVal('asset_list', val.target.value, k, 'purchase_price');
                                                                    calcualtion();
                                                                }}
                                                            />
                                                        </div>
                                                    </div>
                                                </td>

                                                <td className="pr-4 py-4 whitespace-nowrap">
                                                    <div className="relative py-2 pr-2 block w-full border-2 bg-white  border-gray-200 focus-within:ring-indigo-400 focus-within:border-indigo-400 border-gray-100 rounded-md">
                                                        <div className="flex justify-between relative ">
                                                            <div className="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
                                                                <span className="text-gray-500 font-semibold sm:text-md">$</span>
                                                            </div>
                                                            <input
                                                                type="text"
                                                                className="placeholder-gray-400  pl-7 block w-full  border-0 p-0 text-gray-600  focus:ring-0 sm:text-md"
                                                                placeholder="0.00"
                                                                tabIndex="3"
                                                                aria-describedby="price-currency"
                                                                value={props.formData['asset_list'][k]['sale_price']}
                                                                onChange={(val) => {
                                                                    props.assignedVal('asset_list', val.target.value, k, 'sale_price');
                                                                    calcualtion();
                                                                }}
                                                                onBlur={(val) => {
                                                                    props.assignedVal('asset_list', val.target.value, k, 'sale_price');
                                                                    calcualtion();
                                                                }}
                                                            />
                                                        </div>
                                                    </div>
                                                </td>

                                                <td className="pr-4 py-4 whitespace-nowrap">
                                                    <div className={(props.formData['asset_list'][k]['profit_loss'] === "") || (props.formData['asset_list'][k]['profit_loss'] < 0) ? "relative py-2 pr-2 block w-full border-2 bg-white  border-gray-200 focus-within:ring-indigo-400 focus-within:border-indigo-400 border-gray-100 rounded-md" : "relative py-2 pr-2 block w-full border-2 bg-white  border-red-200 focus-within:ring-indigo-400 focus-within:border-indigo-400 border-gray-100 rounded-md"}>
                                                        <div className="flex justify-between relative ">
                                                            <div className="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
                                                                <span className={ (props.formData['asset_list'][k]['profit_loss'] === "") || (props.formData['asset_list'][k]['profit_loss'] < 0) ? "text-gray-500 font-semibold sm:text-md" : "text-red-500 font-semibold sm:text-md"}>$</span>
                                                            </div>
                                                            <input
                                                                type="text"
                                                                className={(props.formData['asset_list'][k]['profit_loss'] === "") || (props.formData['asset_list'][k]['profit_loss'] < 0) ? "placeholder-gray-400  pl-7 block w-full  border-0 p-0 text-gray-600 focus:ring-0 sm:text-md" : "placeholder-red-400  pl-7 block w-full  border-0 p-0 text-red-600 focus:ring-0 sm:text-md"}
                                                                placeholder="0.00"
                                                                value={(props.formData['asset_list'][k]['profit_loss'] < 0) ? parseFloat(props.formData['asset_list'][k]['profit_loss'] + (Math.abs(props.formData['asset_list'][k]['profit_loss']) * 2)).toFixed(2) : parseFloat(Math.abs(props.formData['asset_list'][k]['profit_loss'] - (Math.abs(props.formData['asset_list'][k]['profit_loss']) * 2))).toFixed(2)}
                                                                readOnly={true}
                                                                disabled={true}
                                                            />
                                                        </div>
                                                    </div>
                                                </td>

                                                <td className="py-4 whitespace-nowrap">
                                                    <button
                                                        type="button"
                                                        className="rounded-md text-gray-400 hover:text-gray-700 focus:outline-none focus:ring-2 focus:ring-white"
                                                        onClick={() => {
                                                            props.removeAsset(k);
                                                            calcualtion();
                                                        }}
                                                    >
                                                        <span className="sr-only">Remove item</span>
                                                        <XCircleIcon className="h-6 w-6" aria-hidden="true" />
                                                    </button>
                                                </td>
                                            </tr>

                                            <tr className='bg-white border-b border-gray-200' key={`k${k}`}>
                                                <td className="px-4 pb-3 col-span-5" colSpan="5">
                                                    <div className="relative pt-1 pb-2  block w-full  focus-within:ring-gray-200 focus-within:border-gray-200  rounded-md">
                                                        <Switch.Group as="div" className="flex items-center justify-start">
                                                            <div className="flex items-center">
                                                                {/* <Switch.Label as="span" className="mr-1 2xl:mr-3">
                                                                <span className="text-sm font-medium text-gray-700">No</span>
                                                            </Switch.Label> */}
                                                                <Switch
                                                                    checked={props.formData['asset_list'][k]['owned_12_months']}
                                                                    onChange={() => {
                                                                        props.assignedVal('asset_list', !props.formData['asset_list'][k]['owned_12_months'], k, 'owned_12_months')
                                                                        calcualtion()
                                                                    }}
                                                                    className={classNames(
                                                                        props.formData['asset_list'][k]['owned_12_months'] ? 'bg-green-600' : 'bg-gray-300',
                                                                        'relative inline-flex flex-shrink-0 h-6 w-11 border-2 border-transparent rounded-full cursor-pointer transition-colors ease-in-out duration-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500'
                                                                    )}
                                                                >
                                                                    <span
                                                                        className={classNames(
                                                                            props.formData['asset_list'][k]['owned_12_months'] ? 'translate-x-5' : 'translate-x-0',
                                                                            'pointer-events-none relative inline-block h-5 w-5 rounded-full bg-white shadow transform ring-0 transition ease-in-out duration-200'
                                                                        )}
                                                                    >
                                                                        <span
                                                                            className={classNames(
                                                                                props.formData['asset_list'][k]['owned_12_months'] ? 'opacity-0 ease-out duration-100' : 'opacity-100 ease-in duration-200',
                                                                                'absolute inset-0 h-full w-full flex items-center justify-center transition-opacity pointer-events-none'
                                                                            )}
                                                                            aria-hidden="true"
                                                                        >
                                                                            <svg className="h-3 w-3 text-gray-400" fill="none" viewBox="0 0 12 12">
                                                                                <path
                                                                                    d="M4 8l2-2m0 0l2-2M6 6L4 4m2 2l2 2"
                                                                                    stroke="currentColor"
                                                                                    strokeWidth={2}
                                                                                    strokeLinecap="round"
                                                                                    strokeLinejoin="round"
                                                                                />
                                                                            </svg>
                                                                        </span>
                                                                        <span
                                                                            className={classNames(
                                                                                props.formData['asset_list'][k]['owned_12_months'] ? 'opacity-100 ease-in duration-200' : 'opacity-0 ease-out duration-100',
                                                                                'absolute inset-0 h-full w-full flex items-center justify-center transition-opacity pointer-events-none'
                                                                            )}
                                                                            aria-hidden="true"
                                                                        >
                                                                            <svg className="h-3 w-3 text-green-600" fill="currentColor" viewBox="0 0 12 12">
                                                                                <path d="M3.707 5.293a1 1 0 00-1.414 1.414l1.414-1.414zM5 8l-.707.707a1 1 0 001.414 0L5 8zm4.707-3.293a1 1 0 00-1.414-1.414l1.414 1.414zm-7.414 2l2 2 1.414-1.414-2-2-1.414 1.414zm3.414 2l4-4-1.414-1.414-4 4 1.414 1.414z" />
                                                                            </svg>
                                                                        </span>
                                                                    </span>
                                                                </Switch>


                                                                <Switch.Label as="span" className="ml-1 2xl:ml-3">
                                                                    <span className="text-sm font-medium text-gray-700"> Owned more than 12 months?</span>
                                                                </Switch.Label>
                                                            </div>
                                                        </Switch.Group>
                                                    </div>
                                                </td>

                                            </tr>
                                        </>
                                    })}
                                </tbody>
                            </table>

                            <button
                                type="button"
                                className="inline-flex items-center pl-6 py-2 border-none text-sm leading-4 font-medium  text-blue-700 hover:text-blue-800 focus:outline-none  focus:ring-blue-500"
                                onClick={() => props.addAsset()}
                            >
                                <PlusSmIcon className="-ml-0.5 mr-2 h-4 w-4" aria-hidden="true" />Add Capital Gain/Loss
                            </button>
                        </div>

                        <div className="bg-white p-6 pt-6 2xl:pt-0 mt-6 mb-2 relative  sm:rounded-lg">
                            <div className="space-y-6  items-end grid grid-cols-3 gap-y-2 sm:pt-0  sm:gap-x-8">
                                <div className='col-span-3 2xl:col-span-1'>
                                    <div className="flex justify-between">
                                        <label htmlFor="company" className="block text-md tracking-wider font-medium text-gray-500">
                                            Total current year capital {(props.formData['current_year_capital_gain'] < 0) ? 'loss*' : 'gain*'}
                                        </label>

                                        <InformationCircleIcon
                                            className="h-6 w-6 text-gray-400 ml-2  focus:outline-none focus:outline-none"
                                            data-for='info9'
                                            data-tip
                                        />
                                        <ReactTooltip id='info9' className='custom-tooltip bg-gray-900' textColor='#ffffff' backgroundColor='#111827' effect='solid' aria-haspopup='true'>
                                            <p className="w-64">A captial gain is when you sell an asset for more than you initially paid for it.  When your total capital gain for the year outweight your total capital losses, You will finish up with a next capital gain for the year. You pay tax on this amount.</p>
                                        </ReactTooltip>
                                    </div>
                                    <div className="flex justify-between mt-1 relative ">
                                        <div className="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
                                            <span className={(props.formData['current_year_capital_gain'] == "") || (props.formData['current_year_capital_gain'] >= 0) ? "text-gray-500 font-semibold sm:text-md" : "text-red-500 font-semibold sm:text-md"}>$</span>
                                        </div>
                                        <input
                                            type="number"
                                            name="current_year_capital_gain"
                                            id="current_year_capital_gain"
                                            className={(props.formData['current_year_capital_gain'] == "") || (props.formData['current_year_capital_gain'] >= 0) ? "placeholder-gray-400 py-3 pl-7 select-none sm:text-md block w-full border-2 border-gray-200 text-gray-500 focus:ring-indigo-400 focus:border-indigo-400 border-gray-100 rounded-md" : "placeholder-red-400 py-3 pl-7 select-none sm:text-md block w-full border-2 border-red-200 text-red-500 focus:ring-indigo-400 focus:border-indigo-400 border-red-100 rounded-md"}
                                            placeholder="0.00"
                                            aria-describedby="price-currency"
                                            value={(parseFloat(props.formData['current_year_capital_gain']) < 0) ? parseFloat(parseFloat(props.formData['current_year_capital_gain']) + (Math.abs(parseFloat(props.formData['current_year_capital_gain'])) * 2)).toFixed(2) : parseFloat(Math.abs(parseFloat(props.formData['current_year_capital_gain']) - (Math.abs(parseFloat(props.formData['current_year_capital_gain'])) * 2))).toFixed(2)}
                                            readOnly={true}
                                            disabled={true}
                                            onChange={(val) => props.assignedVal('current_year_capital_gain', val.target.value)}
                                        />
                                    </div>
                                </div>


                                <div className='col-span-2 2xl:col-span-1'>
                                    <div className="flex justify-between">
                                        <label htmlFor="company" className="block text-md tracking-wider font-medium text-gray-500">Previous year capital loss roll over*</label>
                                        <InformationCircleIcon
                                            className="h-6 w-6 text-gray-400 ml-2  focus:outline-none focus:outline-none"
                                            data-for='info10'
                                            data-tip
                                        />
                                        <ReactTooltip id='info10' className='custom-tooltip bg-gray-900' textColor='#ffffff' backgroundColor='#111827' effect='solid' aria-haspopup='true'>
                                            <p className="w-64">You are able to further reduce your current year captial gains by your unapplied net capital losses from earlier years.</p>
                                        </ReactTooltip>
                                    </div>
                                    <div className="flex justify-between mt-1 relative ">
                                        <div className="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
                                            <span className="text-gray-500 font-semibold sm:text-md">$</span>
                                        </div>

                                        <input
                                            type="number"
                                            name="previous_year_capital_loss"
                                            id="previous_year_capital_loss"
                                            className="placeholder-gray-400 py-3 pl-7 select-none sm:text-md block w-full border-2 border-gray-200  focus:ring-indigo-400 focus:border-indigo-400 border-gray-100 rounded-md"
                                            placeholder="0.00"
                                            tabIndex="4"
                                            aria-describedby="price-currency"
                                            value={parseFloat(props.formData['previous_year_capital_loss'])}
                                            onChange={(val) => {
                                                props.assignedVal('previous_year_capital_loss', val.target.value);
                                                props.assignedVal('diffrence', parseFloat(props.formData['current_year_capital_gain'] - val.target.value).toFixed(2));
                                            }}
                                            onBlur={(e) => {
                                                if (e.target.value){
                                                    e.target.value = parseFloat(e.target.value).toFixed(2);
                                                }
                                            }}
                                        />
                                    </div>
                                </div>

                                <div>
                                    <div className="flex justify-between">
                                        <label htmlFor="company" className="block text-md tracking-wider font-medium text-gray-500">Difference</label>
                                    </div>
                                    <div className="flex justify-between mt-1 relative ">
                                        <div className="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
                                            <span className={(props.formData['diffrence'] === "") || (parseFloat(props.formData['diffrence']) >= 0) ? "text-gray-500 font-semibold sm:text-md" : "text-red-500 font-semibold sm:text-md"}>$</span>
                                        </div>
                                        <input
                                            type="number"
                                            name="diffrence"
                                            id="diffrence"
                                            className={(props.formData['diffrence'] === "") || (parseFloat(props.formData['diffrence']) >= 0) ? "placeholder-gray-400 py-3 pl-7 select-none sm:text-md block w-full border-2 border-gray-200 text-gray-600 focus:ring-indigo-400 focus:border-indigo-400 border-gray-100 rounded-md" : "placeholder-red-400 py-3 pl-7 select-none sm:text-md block w-full border-2 border-red-200 text-red-600 focus:ring-indigo-400 focus:border-indigo-400 border-red-100 rounded-md"}
                                            placeholder="0.00"
                                            aria-describedby="price-currency"
                                            value={(parseFloat(props.formData['diffrence']) < 0) ? parseFloat(parseFloat(props.formData['diffrence']) + (Math.abs(parseFloat(props.formData['diffrence'])) * 2)).toFixed(2) : parseFloat(Math.abs(parseFloat(props.formData['diffrence']) - (Math.abs(parseFloat(props.formData['diffrence'])) * 2))).toFixed(2)}
                                            readOnly={true}
                                            disabled={true}
                                            onChange={(val) => props.assignedVal('diffrence', val.target.value)}
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>

    );
}
