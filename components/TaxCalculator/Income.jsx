import { InformationCircleIcon } from "@heroicons/react/outline";
import { useState } from "react";
import ReactTooltip from "react-tooltip";
import {
  getIncomeByFinYear,
  getExpenseByFinYear,
} from "../../services/api/taxCalculator.services";
import store from "../../store/store";
import { snackbarOpen } from "../../reducers/slices/snackbarSlice";
import { SNACKBAR_TYPE } from "../../components/Layout/Snackbar";
import { ImpulseSpinner } from "react-spinners-kit";

export default function Income(props) {
  const [isLoadingRevenue, setIsLoadingRevenue] = useState(false);
  const [isLoadingExpense, setIsLoadingExpense] = useState(false);

  const automateRevenue = async () => {
    if (props?.financialYear) {
      const form_data = new FormData();
      form_data.append("financial_year_id", props.financialYear.id);
      setIsLoadingRevenue(true);
      const result = await getIncomeByFinYear(form_data);

      if (result.success) {
        props.assignedVal("revenue", result.data.revenue.toFixed(2));
      }
      setIsLoadingRevenue(false);
    } else {
      store.store.dispatch(
        snackbarOpen({
          type: SNACKBAR_TYPE.ERROR,
          message: "Could not get Financial Year from props.",
        })
      );
      setIsLoadingRevenue(false);
    }
  };

  const automateExpenses = async () => {
    if (props?.financialYear) {
      const form_data = new FormData();
      form_data.append("financial_year_id", props.financialYear.id);
      setIsLoadingExpense(true);
      const result = await getExpenseByFinYear(form_data);

      if (result.success) {
        props.assignedVal("expenses", result.data.expenses.toFixed(2));
      }
      setIsLoadingExpense(false);
    } else {
      store.store.dispatch(
        snackbarOpen({
          type: SNACKBAR_TYPE.ERROR,
          message: "Could not get Financial Year from props.",
        })
      );
      setIsLoadingExpense(false);
    }
  };

  return (
    <div style={props.currentStep !== 0 ? { display: "none" } : {}}>
      <div id="income">
        <div className="space-y-6  mb-4 pt-6 pb-0 items-end grid grid-cols-2 gap-y-2 sm:pt-0 md:grid-cols-2 sm:gap-x-8">
          <div>
            <div className="flex justify-between">
              <label
                htmlFor="company"
                className="block text-md tracking-wider font-medium text-gray-500"
              >
                Revenue*
              </label>
              <InformationCircleIcon
                className="h-6 w-6 text-gray-400 ml-2  focus:outline-none"
                data-for="info1"
                data-tip
                tabIndex="-1"
              />

              <ReactTooltip
                id="info1"
                className="custom-tooltip bg-gray-900"
                textColor="#ffffff"
                backgroundColor="#111827"
                effect="solid"
                aria-haspopup="true"
              >
                <p className="w-64">
                  Your total forecasted income for this financial year: e.g.
                  salary & wages, interest, dividends, pensions or rent
                </p>
              </ReactTooltip>
            </div>

            <div className="flex justify-between mt-1 relative ">
              <div className="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
                <span className="text-gray-500 font-semibold sm:text-md">
                  $
                </span>
              </div>
              <input
                type="number"
                name="revenue"
                id="revenue"
                tabIndex="1"
                className="placeholder-gray-400 py-3 pl-7 sm:text-md block w-full border-2 border-gray-200  focus:ring-indigo-400 focus:border-indigo-400  rounded-md"
                placeholder="0.00"
                aria-describedby="price-currency"
                value={props.formData["revenue"]}
                onChange={(val) => {
                  props.assignedVal("revenue", val.target.value);
                }}
                onBlur={(val) =>
                  props.assignedVal(
                    "revenue",
                    `${parseFloat(val.target.value).toFixed(2)}`
                  )
                }
              />

              {/* Automate button */}
              <button
                type="button"
                tabIndex="-1"
                className="absolute right-3 top-3.5 inline-flex items-center uppercase px-2.5 py-1 border border-gray-400 shadow-sm text-xs tracking-wide font-semibold rounded text-gray-600 bg-gray-200 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-brand-blue"
                onClick={automateRevenue}
              >
                {isLoadingRevenue ? (
                  <ImpulseSpinner
                    frontColor={"#58c5a8"}
                    backColor="rgba(255,255,255, 0.5)"
                  />
                ) : (
                  "Automate"
                )}
              </button>
            </div>
          </div>

          <div>
            <div className="flex justify-between">
              <label
                htmlFor="company"
                className="block text-md tracking-wider font-medium text-gray-500"
              >
                Expenses*
              </label>
              <InformationCircleIcon
                className="h-6 w-6 text-gray-400 ml-2  focus:outline-none"
                data-for="info2"
                data-tip
                tabIndex="-1"
              />
              <ReactTooltip
                id="info2"
                className="custom-tooltip bg-gray-900"
                textColor="#ffffff"
                backgroundColor="#111827"
                effect="solid"
                aria-haspopup="true"
              >
                <p className="w-64">
                  Your total forecasted outgoings for this financial year: e.g.
                  inventory, etc.
                </p>
              </ReactTooltip>
            </div>

            <div className="flex justify-between mt-1 relative ">
              <div className="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
                <span className="text-gray-500 font-semibold sm:text-md">
                  $
                </span>
              </div>
              <input
                type="text"
                name="expenses"
                id="expenses"
                tabIndex="2"
                className="placeholder-gray-400 py-3 pl-7 sm:text-md block w-full border-2 border-gray-200  focus:ring-indigo-400 focus:border-indigo-400 border-gray-100 rounded-md"
                placeholder="0.00"
                aria-describedby="price-currency"
                value={props.formData["expenses"]}
                onChange={(val) =>
                  props.assignedVal("expenses", val.target.value)
                }
                onBlur={(val) =>
                  props.assignedVal(
                    "expenses",
                    `${parseFloat(val.target.value).toFixed(2)}`
                  )
                }
              />

              {/* Automate button */}
              <button
                type="button"
                className="absolute right-3 top-3.5 inline-flex items-center uppercase px-2.5 py-1 border border-gray-400 shadow-sm text-xs tracking-wide font-semibold rounded text-gray-600 bg-gray-200 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-brand-blue"
                onClick={automateExpenses}
              >
                {isLoadingExpense ? (
                  <ImpulseSpinner
                    frontColor={"#58c5a8"}
                    backColor="rgba(255,255,255, 0.5)"
                  />
                ) : (
                  "Automate"
                )}
              </button>
            </div>
          </div>

          <div>
            <div className="flex justify-between">
              <label
                htmlFor="company"
                className="block text-md tracking-wider font-medium text-gray-500"
              >
                Additional Income
              </label>
              <InformationCircleIcon
                className="h-6 w-6 text-gray-400 ml-2  focus:outline-none focus:outline-none"
                data-for="info3"
                data-tip
              />
              <ReactTooltip
                id="info3"
                className="custom-tooltip bg-gray-900"
                textColor="#ffffff"
                backgroundColor="#111827"
                effect="solid"
                aria-haspopup="true"
              >
                <p className="w-64">
                  Other income you need to declare: e.g. compensation payments,
                  prizes, etc.
                </p>
              </ReactTooltip>
            </div>

            <div className="flex justify-between mt-1 relative ">
              <div className="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
                <span className="text-gray-500 font-semibold sm:text-md">
                  $
                </span>
              </div>
              <input
                type="text"
                name="additional_income"
                id="additional_income"
                className="placeholder-gray-400 py-3 pl-7 sm:text-md block w-full border-2 border-gray-200  focus:ring-indigo-400 focus:border-indigo-400  rounded-md"
                placeholder="0.00"
                tabIndex="3"
                aria-describedby="price-currency"
                value={props.formData["additional_income"]}
                onChange={(val) =>
                  props.assignedVal("additional_income", val.target.value)
                }
                onBlur={(val) =>
                  props.assignedVal(
                    "additional_income",
                    `${parseFloat(val.target.value).toFixed(2)}`
                  )
                }
              />
            </div>
          </div>

          <div>
            <div className="flex justify-between">
              <label
                htmlFor="company"
                className="block text-md tracking-wider font-medium text-gray-500"
              >
                Deductions
              </label>
              <InformationCircleIcon
                className="h-6 w-6 text-gray-400 ml-2  focus:outline-none focus:outline-none"
                data-for="info4"
                data-tip
              />
              <ReactTooltip
                id="info4"
                className="custom-tooltip bg-gray-900"
                textColor="#ffffff"
                backgroundColor="#111827"
                effect="solid"
                aria-haspopup="true"
              >
                <p className="w-64">
                  Your total forecasted deductible expenss for this financial
                  year: e.g. home office expenses, etc.
                </p>
              </ReactTooltip>
            </div>

            <div className="flex justify-between mt-1 relative ">
              <div className="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
                <span className="text-gray-500 font-semibold sm:text-md">
                  $
                </span>
              </div>
              <input
                type="text"
                name="deductions"
                id="deductions"
                tabIndex="4"
                className="placeholder-gray-400 py-3 pl-7 sm:text-md block w-full border-2 border-gray-200  focus:ring-indigo-400 focus:border-indigo-400  rounded-md"
                placeholder="0.00"
                aria-describedby="price-currency"
                value={props.formData["deductions"]}
                onChange={(val) =>
                  props.assignedVal("deductions", val.target.value)
                }
                onBlur={(val) =>
                  props.assignedVal(
                    "deductions",
                    `${parseFloat(val.target.value).toFixed(2)}`
                  )
                }
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
