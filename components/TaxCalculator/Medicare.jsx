import { Switch } from '@headlessui/react'
import { InformationCircleIcon } from '@heroicons/react/outline'
import ReactTooltip from 'react-tooltip';

function classNames(...classes) {
    return classes.filter(Boolean).join('  ')
}

export default function Medicare(props) {

    return (
        <div style={props.currentStep !== 3 ? { display: 'none' } : {}}>
            <div id="medicare">
                <div>
                    <div className="bg-gray-100 p-2 2xl:p-4  mt-4 mb-2 relative shadow sm:rounded-lg">
                        <Switch.Group as="div" className="flex items-center justify-between  p-3  2xl:p-0">
                            <div className="flex justify-between">
                                <Switch.Label as="h3" className="text-lg leading-6 font-medium text-gray-700" passive>
                                    Did you have a spouse this Financial year?*
                                </Switch.Label>
                            </div>

                            <div className="flex items-center">

                                <Switch
                                    checked={props.formData['spouse_financial_year']}
                                    onChange={() => {
                                        if (props.formData['spouse_financial_year']) {
                                            props.assignedVal('spouse_income', 0)
                                        }
                                        props.assignedVal('spouse_financial_year', !props.formData['spouse_financial_year'])
                                    }}
                                    className={classNames(
                                        props.formData['spouse_financial_year'] ? 'bg-green-600' : 'bg-gray-300',
                                        'relative inline-flex flex-shrink-0 h-6 w-11 border-2 border-transparent rounded-full cursor-pointer transition-colors ease-in-out duration-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500'
                                    )}
                                >
                                    <span
                                        className={classNames(
                                            props.formData['spouse_financial_year'] ? 'translate-x-5' : 'translate-x-0',
                                            'pointer-events-none relative inline-block h-5 w-5 rounded-full bg-white shadow transform ring-0 transition ease-in-out duration-200'
                                        )}
                                    >
                                        <span
                                            className={classNames(
                                                props.formData['spouse_financial_year'] ? 'opacity-0 ease-out duration-100' : 'opacity-100 ease-in duration-200',
                                                'absolute inset-0 h-full w-full flex items-center justify-center transition-opacity pointer-events-none'
                                            )}
                                            aria-hidden="true"
                                        >
                                            <svg className="h-3 w-3 text-gray-400" fill="none" viewBox="0 0 12 12">
                                                <path
                                                    d="M4 8l2-2m0 0l2-2M6 6L4 4m2 2l2 2"
                                                    stroke="currentColor"
                                                    strokeWidth={2}
                                                    strokeLinecap="round"
                                                    strokeLinejoin="round"
                                                />
                                            </svg>
                                        </span>
                                        <span
                                            className={classNames(
                                                props.formData['spouse_financial_year'] ? 'opacity-100 ease-in duration-200' : 'opacity-0 ease-out duration-100',
                                                'absolute inset-0 h-full w-full flex items-center justify-center transition-opacity pointer-events-none'
                                            )}
                                            aria-hidden="true"
                                        >
                                            <svg className="h-3 w-3 text-green-600" fill="currentColor" viewBox="0 0 12 12">
                                                <path d="M3.707 5.293a1 1 0 00-1.414 1.414l1.414-1.414zM5 8l-.707.707a1 1 0 001.414 0L5 8zm4.707-3.293a1 1 0 00-1.414-1.414l1.414 1.414zm-7.414 2l2 2 1.414-1.414-2-2-1.414 1.414zm3.414 2l4-4-1.414-1.414-4 4 1.414 1.414z" />
                                            </svg>
                                        </span>
                                    </span>
                                </Switch>

                            </div>
                        </Switch.Group>

                        <div
                            className={classNames(props.formData['spouse_financial_year'] ? 'block' : 'hidden', "space-y-6 mt-4 items-end grid grid-cols-1 md:grid-cols-2 gap-y-2 sm:pt-0 sm:gap-x-8")}
                        >
                            <div>
                                <div className="flex justify-between">
                                    <label htmlFor="company" className="block text-md tracking-wider font-medium text-gray-500">
                                        How much did they earn?*
                                        <InformationCircleIcon
                                            className="h-6 w-6 text-gray-400 ml-2  focus:outline-none inline-flex focus:outline-none"
                                            data-for='spouse_income_info'
                                            data-tip
                                        />
                                    </label>


                                    <ReactTooltip id='spouse_income_info' className='custom-tooltip bg-gray-900' textColor='#ffffff' backgroundColor='#111827' effect='solid' aria-haspopup='true'>
                                        <p className="w-64">Your spouse's taxable income</p>
                                    </ReactTooltip>
                                </div>
                                <div className="flex justify-between mt-1 relative ">
                                    <div className="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
                                        <span className="text-gray-500 font-semibold sm:text-md">$</span>
                                    </div>
                                    <input
                                        type="number"
                                        name="spouse_income"
                                        id="spouse_income"
                                        className="placeholder-gray-400 py-3 pl-7 select-none sm:text-md block w-full border-2 border-gray-200  focus:ring-indigo-400 focus:border-indigo-400 border-gray-100 rounded-md"
                                        placeholder="0.00"
                                        aria-describedby="price-currency"
                                        // value={props.formData['spouse_income']}
                                        onChange={(val) => props.assignedVal('spouse_income', val.target.value)}
                                        onBlur={(val) => {
                                            val.target.value = parseFloat(val.target.value).toFixed(2);
                                        }}
                                        defaultValue={parseFloat(props.formData['spouse_income']) > 0 ? parseFloat(props.formData['spouse_income']).toFixed(2) : null}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>


                </div>

                <div>
                    <div className="bg-gray-100 p-2 2xl:p-4  mt-6 2xl:mt-6 mb-2 relative shadow sm:rounded-lg">
                        <Switch.Group as="div" className="flex items-center justify-between p-3  2xl:p-0">
                            <div className="flex justify-between">
                                <Switch.Label as="h3" className="text-lg leading-6 font-medium text-gray-700" passive>
                                    Were you eligble for Medicare Levy exemptions?
                                    <InformationCircleIcon
                                        className="h-6 w-6 text-gray-400 ml-2  focus:outline-none inline-flex focus:outline-none"
                                        data-for='medical_levy_examption_info'
                                        data-tip
                                    />
                                </Switch.Label>

                                <ReactTooltip id='medical_levy_examption_info' className='custom-tooltip bg-gray-900' textColor='#ffffff' backgroundColor='#111827' effect='solid' aria-haspopup='true'>
                                    <p className="w-64">The Medicare levy is 2% of your taxable income, in addition to the tax you pay on your taxable income.</p>
                                </ReactTooltip>
                            </div>

                            <div className="flex items-center">

                                <Switch
                                    checked={props.formData['medical_levy_examption']}
                                    onChange={() => {
                                        if (!props.formData['medical_levy_examption']) {
                                            props.assignedVal('has_private_insurance', false)
                                        }
                                        props.assignedVal('medical_levy_examption', !props.formData['medical_levy_examption'])
                                    }}
                                    className={classNames(
                                        props.formData['medical_levy_examption'] ? 'bg-green-600' : 'bg-gray-300',
                                        'relative inline-flex flex-shrink-0 h-6 w-11 border-2 border-transparent rounded-full cursor-pointer transition-colors ease-in-out duration-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500'
                                    )}
                                >
                                    <span
                                        className={classNames(
                                            props.formData['medical_levy_examption'] ? 'translate-x-5' : 'translate-x-0',
                                            'pointer-events-none relative inline-block h-5 w-5 rounded-full bg-white shadow transform ring-0 transition ease-in-out duration-200'
                                        )}
                                    >
                                        <span
                                            className={classNames(
                                                props.formData['medical_levy_examption'] ? 'opacity-0 ease-out duration-100' : 'opacity-100 ease-in duration-200',
                                                'absolute inset-0 h-full w-full flex items-center justify-center transition-opacity pointer-events-none'
                                            )}
                                            aria-hidden="true"
                                        >
                                            <svg className="h-3 w-3 text-gray-400" fill="none" viewBox="0 0 12 12">
                                                <path
                                                    d="M4 8l2-2m0 0l2-2M6 6L4 4m2 2l2 2"
                                                    stroke="currentColor"
                                                    strokeWidth={2}
                                                    strokeLinecap="round"
                                                    strokeLinejoin="round"
                                                />
                                            </svg>
                                        </span>
                                        <span
                                            className={classNames(
                                                props.formData['medical_levy_examption'] ? 'opacity-100 ease-in duration-200' : 'opacity-0 ease-out duration-100',
                                                'absolute inset-0 h-full w-full flex items-center justify-center transition-opacity pointer-events-none'
                                            )}
                                            aria-hidden="true"
                                        >
                                            <svg className="h-3 w-3 text-green-600" fill="currentColor" viewBox="0 0 12 12">
                                                <path d="M3.707 5.293a1 1 0 00-1.414 1.414l1.414-1.414zM5 8l-.707.707a1 1 0 001.414 0L5 8zm4.707-3.293a1 1 0 00-1.414-1.414l1.414 1.414zm-7.414 2l2 2 1.414-1.414-2-2-1.414 1.414zm3.414 2l4-4-1.414-1.414-4 4 1.414 1.414z" />
                                            </svg>
                                        </span>
                                    </span>
                                </Switch>

                            </div>
                        </Switch.Group>
                    </div>

                    <div
                        className={classNames(!props.formData['medical_levy_examption'] ? 'block' : 'hidden', "bg-gray-100  p-2 2xl:p-4  mt-8 2xl:mt-12 mb-2 relative shadow sm:rounded-lg")}
                    >
                        <Switch.Group as="div" className="flex items-center justify-between p-3  2xl:p-0">
                            <div className="flex justify-between">
                                <Switch.Label as="h3" className="text-lg leading-6 font-medium text-gray-700" passive>Did you have private health insurance?</Switch.Label>
                                <InformationCircleIcon
                                    className="h-6 w-6 text-gray-400 ml-2  focus:outline-none focus:outline-none"
                                    data-for='has_private_insurance_info'
                                    data-tip
                                />
                                <ReactTooltip id='has_private_insurance_info' className='custom-tooltip bg-gray-900' textColor='#ffffff' backgroundColor='#111827' effect='solid' aria-haspopup='true'>
                                    <p className="w-64">The estimatore assumes you hav held eligible health insurance for the entire year.</p>
                                </ReactTooltip>
                            </div>

                            <div className="flex items-center">
                                <Switch
                                    checked={props.formData['has_private_insurance']}
                                    onChange={() => props.assignedVal('has_private_insurance', !props.formData['has_private_insurance'])}
                                    className={classNames(
                                        props.formData['has_private_insurance'] ? 'bg-green-600' : 'bg-gray-300',
                                        'relative inline-flex flex-shrink-0 h-6 w-11 border-2 border-transparent rounded-full cursor-pointer transition-colors ease-in-out duration-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500'
                                    )}
                                >
                                    <span
                                        className={classNames(
                                            props.formData['has_private_insurance'] ? 'translate-x-5' : 'translate-x-0',
                                            'pointer-events-none relative inline-block h-5 w-5 rounded-full bg-white shadow transform ring-0 transition ease-in-out duration-200'
                                        )}
                                    >
                                        <span
                                            className={classNames(
                                                props.formData['has_private_insurance'] ? 'opacity-0 ease-out duration-100' : 'opacity-100 ease-in duration-200',
                                                'absolute inset-0 h-full w-full flex items-center justify-center transition-opacity pointer-events-none'
                                            )}
                                            aria-hidden="true"
                                        >
                                            <svg className="h-3 w-3 text-gray-400" fill="none" viewBox="0 0 12 12">
                                                <path
                                                    d="M4 8l2-2m0 0l2-2M6 6L4 4m2 2l2 2"
                                                    stroke="currentColor"
                                                    strokeWidth={2}
                                                    strokeLinecap="round"
                                                    strokeLinejoin="round"
                                                />
                                            </svg>
                                        </span>
                                        <span
                                            className={classNames(
                                                props.formData['has_private_insurance'] ? 'opacity-100 ease-in duration-200' : 'opacity-0 ease-out duration-100',
                                                'absolute inset-0 h-full w-full flex items-center justify-center transition-opacity pointer-events-none'
                                            )}
                                            aria-hidden="true"
                                        >
                                            <svg className="h-3 w-3 text-green-600" fill="currentColor" viewBox="0 0 12 12">
                                                <path d="M3.707 5.293a1 1 0 00-1.414 1.414l1.414-1.414zM5 8l-.707.707a1 1 0 001.414 0L5 8zm4.707-3.293a1 1 0 00-1.414-1.414l1.414 1.414zm-7.414 2l2 2 1.414-1.414-2-2-1.414 1.414zm3.414 2l4-4-1.414-1.414-4 4 1.414 1.414z" />
                                            </svg>
                                        </span>
                                    </span>
                                </Switch>

                            </div>
                        </Switch.Group>
                    </div>
                </div>
            </div>
        </div>

    );
}