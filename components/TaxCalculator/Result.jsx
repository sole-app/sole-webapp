import { InformationCircleIcon } from "@heroicons/react/outline";
import ReactTooltip from "react-tooltip";
import { formatCurrency } from "../../helpers/common";

export default function Result(props) {
  const { taxData = {} } = props;

  return (
    <div className="mb-2 relative  sm:rounded-lg">
      <div>
        <h4 className="bg-gray-50 font-semibold inline-flex w-full text-gray-500 text-lg p-4">
          Income
          <InformationCircleIcon
            className="h-6 w-6 text-gray-400 ml-2 focus:outline-none in line-flex  focus:outline-none"
            data-for="income"
            data-tip
          />
          <ReactTooltip
            id="income"
            className="custom-tooltip focus:outline-none bg-gray-900"
            textColor="#ffffff"
            backgroundColor="#111827"
            effect="solid"
            aria-haspopup="true"
          >
            <p className="w-64">
              Your total forecasted income for this financial year: e.g. salary
              & wages, interst ,dividends, pensions or rent, other income.
            </p>
          </ReactTooltip>
        </h4>

        <ul className="bg-white devide-y devide-gray-100">
          <li className="flex px-4 py-2 items-center justify-between">
            <span>Revenue</span>
            <span className="text-lg">
              {typeof taxData.revenue !== "undefined"
                ? formatCurrency(taxData.revenue)
                : formatCurrency(0)}
            </span>
          </li>
          <li className="flex  px-4 py-2 items-center justify-between">
            <span>Expenses</span>
            <span className="text-red-500 text-lg">
              {typeof taxData.expenses !== "undefined"
                ? formatCurrency(taxData.expenses)
                : formatCurrency(0)}
            </span>
          </li>
          <li className="flex  px-4 py-2 items-center justify-between">
            <span>Additional Income</span>
            <span className="text-lg">
              {typeof taxData.additional_income !== "undefined"
                ? formatCurrency(taxData.additional_income)
                : formatCurrency(0)}
            </span>
          </li>
          <li className="flex  px-4 py-2 items-center justify-between">
            <span>Deductions</span>
            <span className="text-red-500 text-lg ">
              {typeof taxData.deductions !== "undefined"
                ? formatCurrency(taxData.deductions)
                : formatCurrency(0)}
            </span>
          </li>
          <li className="flex  px-4 py-2 items-center justify-between">
            <span>Total capital gain</span>
            <span
              className={
                typeof taxData.total_capital_gains !== "undefined" &&
                taxData.total_capital_gains < 0
                  ? "text-red-500 text-lg"
                  : "text-lg"
              }
            >
              {typeof taxData.total_capital_gains !== "undefined"
                ? formatCurrency(Math.abs(taxData.total_capital_gains))
                : formatCurrency(0)}
            </span>
          </li>
        </ul>
      </div>
      {/* end income */}

      <div>
        <h4 className="bg-gray-50  font-semibold text-gray-500 text-lg p-4">
          Superannuation
          <InformationCircleIcon
            className="h-6 w-6 text-gray-400 ml-2  focus:outline-none inline-flex focus:outline-none"
            data-for="annuation"
            data-tip
          />
          <ReactTooltip
            id="annuation"
            className="custom-tooltip bg-gray-900"
            textColor="#ffffff"
            backgroundColor="#111827"
            effect="solid"
            aria-haspopup="true"
          >
            <p className="w-64">
              Any personal super contributions coming from your take-home pay:
              these contributions are in addition to any compulsory super
              contributions made and does not include super contributions made
              through a salary-sacrifice arrangement.
            </p>
          </ReactTooltip>
        </h4>

        <ul className=" bg-white devide-y devide-gray-100">
          <li className="flex px-4 py-2 items-center justify-between">
            <span>Total Contributions</span>
            <span className="text-red-500  text-lg">
              {typeof taxData.superannuation !== "undefined"
                ? formatCurrency(taxData.superannuation)
                : formatCurrency(0)}
            </span>
          </li>
        </ul>
      </div>
      {/* end superannuation */}

      <div>
        <h4 className="bg-gray-50  font-semibold text-gray-500 text-lg p-3">
          Taxable Income
          <InformationCircleIcon
            className="h-6 w-6 text-gray-400 ml-2  focus:outline-none inline-flex focus:outline-none"
            data-for="taxable"
            data-tip
          />
          <ReactTooltip
            id="taxable"
            className="custom-tooltip bg-gray-900"
            textColor="#ffffff"
            backgroundColor="#111827"
            effect="solid"
            aria-haspopup="true"
          >
            <p className="w-64">
              Taxable income is gross income less allowable deductions for the
              full income year.
            </p>
          </ReactTooltip>
        </h4>

        <ul className="bg-white devide-y devide-gray-100">
          <li className="flex px-4 py-2 items-center justify-between">
            <span className="font-semibold">Total Taxable Income</span>
            <span className="text-lg font-semibold">
              {typeof taxData.taxable_income !== "undefined"
                ? formatCurrency(taxData.taxable_income)
                : formatCurrency(0)}
            </span>
          </li>
          <li className="flex px-4 py-2 items-center justify-between">
            <span className="font-semibold">
              Tax on Taxable Income
              <span className="text-sm block">
                marginal tax rate:{" "}
                {typeof taxData.rate !== "undefined" ? taxData.rate : 0}%
              </span>
            </span>
            <span className="text-lg font-semibold">
              {typeof taxData.resident_tax !== "undefined"
                ? formatCurrency(taxData.resident_tax)
                : formatCurrency(0)}
            </span>
          </li>
        </ul>
      </div>
      {/* end taxable income */}

      <div>
        <h4 className="bg-gray-50  font-semibold text-gray-500 text-lg p-4">
          Medicare Levy
          <InformationCircleIcon
            className="h-6 w-6 text-gray-400 ml-2  focus:outline-none inline-flex focus:outline-none"
            data-for="medicare"
            data-tip
          />
          <ReactTooltip
            id="medicare"
            className="custom-tooltip bg-gray-900"
            textColor="#ffffff"
            backgroundColor="#111827"
            effect="solid"
            aria-haspopup="true"
          >
            <p className="w-64">
              The Medicare levy is 2% of your taxable income, in addition to the
              tax you pay on your taxable income.
            </p>
          </ReactTooltip>
        </h4>

        <ul className="bg-white devide-y devide-gray-100">
          <li className="flex px-4 py-2 items-center justify-between">
            <span>Spouse's Gross Income</span>
            <span className="text-lg">
              {typeof taxData.spouse_income !== "undefined"
                ? formatCurrency(taxData.spouse_income)
                : formatCurrency(0)}
            </span>
          </li>
          <li className="flex px-4 py-2 items-center justify-between">
            <span>Medicare Levy Payable</span>
            <span className="text-lg">
              {typeof taxData.medicare_levy !== "undefined"
                ? formatCurrency(taxData.medicare_levy)
                : formatCurrency(0)}
            </span>
          </li>
          <li className="flex px-4 py-2 items-center justify-between">
            <span>Medicare Levy Surcharge</span>
            <span className="text-lg">
              {typeof taxData.medicare_levy_surchage !== "undefined"
                ? formatCurrency(taxData.medicare_levy_surchage)
                : formatCurrency(0)}
            </span>
          </li>
        </ul>
      </div>
      {/* end medicare */}

      <div>
        <h4 className="bg-gray-50  font-semibold text-gray-500 text-lg p-4">
          Low Income Tax Offset
          <InformationCircleIcon
            className="h-6 w-6 text-gray-400 ml-2  focus:outline-none inline-flex focus:outline-none"
            data-for="offset"
            data-tip
          />
          <ReactTooltip
            id="offset"
            className="custom-tooltip bg-gray-900"
            textColor="#ffffff"
            backgroundColor="#111827"
            effect="solid"
            aria-haspopup="true"
          >
            <p className="w-64">
              The low and middle income tax offset amount is between $225 and
              $1,080.
            </p>
          </ReactTooltip>
        </h4>

        <ul className="bg-white devide-y devide-gray-100">
          <li className="flex px-3 py-2 items-center justify-between">
            <span>Total Amount of Tax Offset</span>
            <span className="text-lg">
              {typeof taxData.tax_offset !== "undefined"
                ? formatCurrency(taxData.tax_offset)
                : formatCurrency(0)}
            </span>
          </li>
        </ul>
      </div>
      {/* end  Low Income Tax Offset */}

      <div className="bg-gray-100 border-t border-gray-200 pt-3 pb-3">
        <ul className="devide-y devide-gray-100">
          <li className="flex px-4 py-2 items-center justify-between">
            <span className="font-semibold text-lg">
              {" "}
              Estimated Total Tax Payable*
            </span>
            <span className="text-4xl font-bold text-brand-blue">
              {typeof taxData.estimated_tax !== "undefined"
                ? formatCurrency(taxData.estimated_tax)
                : formatCurrency(0)}
            </span>
          </li>
        </ul>
      </div>
      {/* end  Total tax payable */}
    </div>
  );
}
