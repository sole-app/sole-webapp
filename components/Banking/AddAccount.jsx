import { Fragment, useState, useEffect } from "react";
import { Dialog, Transition } from "@headlessui/react";
import { XIcon } from "@heroicons/react/outline";
import {
  getBankLists,
  createCustomer,
  updateCustomer as updateBankCustomer,
  selectAccount as selectBankAccount,
  refreshAccountData,
  additionalAuth as additionalBankAuth,
} from "../../services/api/bankConnection.services";
import InstituteList from "../../components/Banking/InstituteList";
import AccountCredentials from "../../components/Banking/AccountCredentials";
import AccountCredentialsAdditional from "../../components/Banking/AccountCredentialsAdditional";
import AccountList from "../../components/Banking/AccountList";
import Connecting from "../../components/Banking/Connecting";
import AlertDialog from "../AlertDialog";

export default function AddAccount(props) {
  const {
    openSlider,
    setOpenSlider,
    setAccountAdded,
    openCloseReAuth,
    account,
    isReAuth,
    listAccounts,
    refresh,
    setRefresh,
  } = props;
  const [institutes, setInstitutes] = useState([]);
  const [list, setList] = useState([]);
  const [loader, setLoader] = useState(false);
  const [reAuthLoader, setReAuthLoader] = useState(false);
  const [institute, setInstitute] = useState({});
  const [customer, setCustomer] = useState({});
  const [section, setSection] = useState("list"); //list,account,list_accounts,account_reauth,refresh
  const [additionalInfo, setAdditionalInfo] = useState({});
  const [accountId, setAccountId] = useState(0);
  const [openModel, setOpenModel] = useState(false);
  const [openAlert, setOpenAlert] = useState(false);
  const [msg, setMsg] = useState("");

  const getAllInstitutes = async () => {
    setLoader(true);
    if (isReAuth) {
      setReAuthLoader(true);
    }
    const result = await getBankLists();
    if (result.success) {
      let institutesData = result.data.institutions;
      setInstitutes(institutesData);
      setList(institutesData);
      if (isReAuth) {
        for (let i = 0; i < institutesData.length; i++) {
          if (account.slug === institutesData[i].slug) {
            await selectInstitute(institutesData[i]);
            break;
          }
        }
      }
    }

    if (isReAuth) {
      setReAuthLoader(false);
    }
    setLoader(false);
  };

  const filterInstitutes = async (value) => {
    const filteredData = institutes.filter(function (item) {
      return item.name.toLowerCase().search(value.toLowerCase()) !== -1;
    });

    setList(filteredData);
  };

  const goBack = async () => {
    if (section === "account") {
      setSection("list");
    }
  };

  const selectInstitute = async (data) => {
    setInstitute(data);
    setSection("account");
  };

  const linkAccount = async (data) => {
    if (isReAuth) {
      await updateCustomer(data);
      return;
    }
    setLoader(true);
    const result = await createCustomer(data);
    if (result.success) {
      setCustomer(result.data);
      setSection("list_accounts");
    } else {
      setMsg(result.message);
      setOpenAlert(true);
    }
    setLoader(false);
  };

  const updateCustomer = async (data) => {
    data.append("bank_account_id", account.bank_account_id);
    setLoader(true);
    const result = await updateBankCustomer(data);
    if (result.success) {
      setAccountAdded(true);
      //setRefresh(!refresh);
      await listAccounts();
      openCloseReAuth();
    } else {
      setMsg(result.message);
      setOpenAlert(true);
    }
    setLoader(false);
  };

  const selectAccount = async (account_id) => {
    setAccountId(account_id);
    setLoader(true);
    const formData = new FormData();
    formData.append("account_id", account_id);
    formData.append("bank_connection_id", customer.bank_connection_id);
    const result = await selectBankAccount(formData);
    if (result.success) {
      setSection("refresh");
      refreshAccountData(formData);
      setTimeout(() => {
        setOpenSlider(false);
        setAccountAdded(true);
        setRefresh(!refresh);
      }, 5000);
    } else {
      if (result.message == "Additional Input Required") {
        //add extra auth factor step
        setOpenModel(true);
        setAdditionalInfo(result.data.additional_field);
      } else {
        setSection("list_accounts");
      }
    }
    setLoader(false);
  };

  const openReauth = async () => {
    setOpenModel(false);
    setSection("account_reauth");
  };

  const additionalAuth = async (formData) => {
    setLoader(true);
    const result = await additionalBankAuth(formData);
    if (result.success) {
      setSection("refresh");
      refreshAccountData(formData);
      setTimeout(() => {
        setOpenSlider(false);
        setAccountAdded(true);
        setRefresh(!refresh);
      }, 5000);
    } else {
      setSection("list_accounts");
    }
    setLoader(false);
  };

  useEffect(() => {
    if (openSlider) {
      getAllInstitutes();
    } else {
      setInstitute({});
      setSection("list");
    }
  }, [openSlider]);

  return (
    <>
      <Transition.Root show={openSlider} as={Fragment}>
        <Dialog
          as="div"
          className="fixed inset-0 overflow-hidden"
          onClose={() => {
            setOpenSlider(false);
            openCloseReAuth();
          }}
        >
          <div className="absolute inset-0 overflow-hidden">
            <Dialog.Overlay className="absolute inset-0" />

            <div className="pointer-events-none fixed inset-y-0 right-0 flex max-w-full">
              <Transition.Child
                as={Fragment}
                enter="transform transition ease-in-out duration-500 sm:duration-700"
                enterFrom="translate-x-full"
                enterTo="translate-x-0"
                leave="transform transition ease-in-out duration-500 sm:duration-700"
                leaveFrom="translate-x-0"
                leaveTo="translate-x-full"
              >
                <div className="pointer-events-auto w-screen max-w-full">
                  <div className="flex h-full flex-col overflow-y-scroll  bg-white shadow-xl">
                    <div className="py-6 px-4 sm:px-6">
                      <div className="flex items-center justify-end">
                        <div className="ml-3 flex h-7 items-center justify-end">
                          <button
                            type="button"
                            className="rounded-md  text-gray-500 hover:text-gray-700 focus:outline-none focus:ring-2 focus:ring-white"
                            onClick={() => setOpenSlider(false)}
                          >
                            <span className="sr-only">Close panel</span>
                            <XIcon className="h-6 w-6" aria-hidden="true" />
                          </button>
                        </div>
                      </div>
                    </div>
                    <div className="relative flex-1 py-6 px-4 sm:px-6">
                      {section === "list" ? (
                        <InstituteList
                          loader={loader}
                          reAuthLoader={reAuthLoader}
                          list={list}
                          filterInstitutes={filterInstitutes}
                          selectInstitute={selectInstitute}
                        />
                      ) : (
                        ""
                      )}

                      {section === "account" ? (
                        <AccountCredentials
                          loader={loader}
                          isReAuth={isReAuth}
                          institute={institute}
                          linkAccount={linkAccount}
                          goBack={goBack}
                        />
                      ) : (
                        ""
                      )}

                      {section === "list_accounts" ? (
                        <AccountList
                          loader={loader}
                          customer={customer}
                          selectAccount={selectAccount}
                        />
                      ) : (
                        ""
                      )}

                      {section === "account_reauth" ? (
                        <AccountCredentialsAdditional
                          loader={loader}
                          institute={institute}
                          accountId={accountId}
                          customer={customer}
                          additionalInfo={additionalInfo}
                          additionalAuth={additionalAuth}
                        />
                      ) : (
                        ""
                      )}

                      {section === "refresh" ? (
                        <Connecting loader={loader} institute={institute} />
                      ) : (
                        ""
                      )}

                      <div>
                        <div
                          className={`${
                            openModel
                              ? "visible opacity-100"
                              : "invisible opacity-0"
                          } fixed inset-0 transition duration-500 delay-100 bg-black bg-opacity-25`}
                          onClick={openReauth}
                        />

                        <div
                          className={`${
                            openModel
                              ? "visible opacity-100"
                              : "invisible opacity-0"
                          } fixed modal transition duration-500 inset-0 overflow-y-auto`}
                        >
                          <div className="flex min-h-full items-center justify-center p-4 text-center">
                            <div className="w-full max-w-md transform overflow-hidden rounded-2xl bg-white text-left align-middle shadow-xl transition-all">
                              <div className="bg-gray-100 py-4 px-4 sm:px-6 border-b border-gray-200 ">
                                <div className="flex items-center justify-between">
                                  <h3 className="text-lg font-medium text-gray-800">
                                    Important
                                  </h3>

                                  <div className="ml-3 h-7 flex items-center">
                                    <button
                                      type="button"
                                      className="rounded-full hover:bg-gray-300 p-2 text-gray-500 hover:text-gray-700 focus:outline-none focus:ring-2 focus:ring-white"
                                      onClick={openReauth}
                                    >
                                      <span className="sr-only">
                                        Close panel
                                      </span>
                                      <XIcon
                                        className="h-6 w-6"
                                        aria-hidden="true"
                                      />
                                    </button>
                                  </div>
                                </div>
                              </div>

                              <div className="p-6">
                                <div className="mt-2">
                                  <p className="text-sm text-gray-500">
                                    To ensure your security, your financial
                                    institution has requested a second MFA code
                                    before your account can be connected (note
                                    it may have changed since the previous code
                                    you provided).
                                  </p>
                                </div>

                                <div className="mt-4">
                                  <button
                                    type="button"
                                    className="inline-flex justify-center mt-1 rounded-md border border-transparent bg-blue-100 px-4 py-2 text-sm font-medium text-blue-900 hover:bg-blue-200 focus:outline-none focus-visible:ring-2 focus-visible:ring-blue-500 focus-visible:ring-offset-2"
                                    onClick={openReauth}
                                  >
                                    Next
                                  </button>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </Transition.Child>
            </div>
          </div>

          <AlertDialog
            id="ringtone-menu2"
            message={msg}
            keepMounted
            open={openAlert}
            onClose={setOpenAlert}
          />
        </Dialog>
      </Transition.Root>
    </>
  );
}
