export default function CredentialField(props) {
    const { field } = props;

    if (field.type === 'select') {
        //select
    }

    return (
        <input
            type={field.type}
            name={`credentials[${field.fieldID}]`}
            id={field.fieldID}
            className="placeholder-gray-400 mt-4 py-3 px-4 block w-full border-2 border-gray-200  focus:ring-indigo-400 focus:border-indigo-400 border-gray-100 rounded-md "
            placeholder={field.name}
            minLength={field.validation.minLength || 0}
            maxLength={field.validation.maxLength || 999}
        />
    )
}
