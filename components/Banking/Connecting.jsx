import bankIcons from "../../config/bankIcons";
import CircularProgress from '@material-ui/core/CircularProgress'

export default function Connecting(props) {
    const { institute } = props;

    let image = bankIcons.default;

    if (institute.slug !== '') {
        image = `https://test.bankstatements.com.au/images/institutions/${institute.slug}.png`;
    }

    return (
        <div className="inset-0 py-6 px-4 sm:px-6">
            <div className="flex items-center justify-center">
                <div>
                    <img
                        src="/images/Sole_logo.svg"
                        alt="Sole logo"
                        width="112"
                        height="28"
                        className="w-auto"
                    />
                </div>

                <div className='mx-5'>
                    <img
                        src="/images/icon/data-transfer.png"
                        alt="data-transfer"
                        width="50"
                        height="50"
                        className="w-auto"
                    />
                </div>


                <div>
                    <img
                        src={image}
                        alt="Bank logo"
                        width="158"
                        height="55"
                        className=" w-auto"
                    />
                </div>
            </div>

            <p className="note font-semibold text-center flex justify-center py-6 items-center">
                Connecting {institute.name}
            </p>


            <div className="flex items-center justify-center"><CircularProgress /></div>
        </div>
    )
}
