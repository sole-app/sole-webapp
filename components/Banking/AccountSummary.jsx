import Skeleton from "react-loading-skeleton";
import "react-loading-skeleton/dist/skeleton.css";
import { InformationCircleIcon } from "@heroicons/react/outline";
import ReactTooltip from "react-tooltip";
import { formatCurrency } from "../../helpers/common";
import { RiBankLine } from "react-icons/ri";
import { IconContext } from "react-icons";

export default function AccountSummary(props) {
  const { loader, accounts, openCloseReAuth, matchUnmatchTransactions } = props;

  if (loader) {
    return (
      <ul
        role="list"
        className="grid gap-6 grid-cols-1 lg:grid-cols-2 2xl:grid-cols-3 mt-8"
      >
        <li className="relative bg-white border-2 border-blue-800 col-span-1 rounded-lg shadow divide-y divide-gray-200">
          <div className="w-full   p-6">
            <div className="grid lg:flex items-start gap-5">
              <div className="bank-icon">
                <span className="w-14 h-14 rounded-full  bg-brand-blue flex items-center justify-center">
                  <IconContext.Provider
                    value={{ color: "#fff", className: "text-3xl" }}
                  >
                    <div>
                      <RiBankLine />
                    </div>
                  </IconContext.Provider>
                </span>
              </div>

              <div className="w-11/12">
                <div className="mt-1">
                  <div className="items-center">
                    <h3 className="text-gray-600 text-lg leading-5 font-semibold ">
                      <Skeleton />
                    </h3>
                    <span className="mt-2  py-0.5 block text-green-800 text-2xl font-semibold rounded-full">
                      <Skeleton />
                    </span>
                  </div>
                </div>
              </div>
            </div>

            <div className="flex items-end justify-between mt-3">
              <div className="items-center">
                <h3 className="text-gray-400 text-base leading-5 font-semibold">
                  <span className="mt-1 block py-0.5 text-red-800 text-xl font-semibold  rounded-full">
                    <Skeleton />
                  </span>
                  Matched/Unmatched <br></br>Transactions
                </h3>
              </div>

              <div>
                <p className="relative text-sm">
                  <Skeleton />
                </p>
              </div>
            </div>
          </div>
        </li>
      </ul>
    );
  }

  return (
    <ul
      role="list"
      className="grid  gap-6 grid-cols-1 lg:grid-cols-2 2xl:grid-cols-3 mt-8"
    >
      {accounts.map((account, index) => (
        <li
          key={index}
          className={
            account.is_primary > 0
              ? "bg-white border-2 relative border-blue-800 col-span-1 rounded-lg shadow divide-y divide-gray-200"
              : "col-span-1 relative bg-white rounded-lg shadow divide-y divide-gray-200"
          }
        >
          <div className="w-full   p-6">
            <div className="grid lg:flex items-start gap-5">
              <div className="bank-icon">
                <span className="w-14 h-14 rounded-full  bg-brand-blue flex items-center justify-center">
                  <IconContext.Provider
                    value={{ color: "#fff", className: "text-3xl" }}
                  >
                    <div>
                      <RiBankLine />
                    </div>
                  </IconContext.Provider>
                </span>
              </div>

              <div className="w-11/12">
                <div className="mt-1">
                  <div className="items-center">
                    <h3 className="text-gray-600 text-lg leading-5 font-semibold ">
                      {account.bank_name}
                    </h3>
                    <span className="mt-2  py-0.5 block text-green-800 text-2xl font-semibold rounded-full">
                      {formatCurrency(account.balance)}
                    </span>
                  </div>
                </div>
              </div>
            </div>

            <div className="flex items-end justify-between mt-3">
              <div className="items-center">
                <h3 className="text-gray-400 text-base leading-5 font-semibold">
                  <span className="mt-1 block py-0.5 text-red-800 text-xl font-semibold  rounded-full">
                    {account.bank_account_id > 0 &&
                    typeof matchUnmatchTransactions[account.bank_account_id] !==
                      "undefined"
                      ? matchUnmatchTransactions[account.bank_account_id]
                          .reconciled +
                        "/" +
                        matchUnmatchTransactions[account.bank_account_id]
                          .pending_reconciled
                      : 0}
                  </span>
                  Matched/Unmatched <br></br>Transactions
                </h3>
              </div>

              <div>
                {account.is_primary ? (
                  <span className="w-20 block ml-auto mr-0 mb-2 text-center px-2.5 py-0.5 rounded-full text-xs font-semibold tracking-wider bg-blue-100 text-blue-800">
                    Primary
                  </span>
                ) : (
                  ""
                )}
                <p className="relative text-sm text-right">
                  Account Type<br></br> {account.account_type_name}
                </p>
              </div>
            </div>
          </div>

          {account.bank_connection.connection_status ? (
            ""
          ) : (
            <div className="absolute h-full  w-full top-0 left-0">
              <div className="absolute right-2 top-2 z-20">
                <InformationCircleIcon
                  className="h-6 w-6 text-gray-400 ml-2  focus:outline-none"
                  data-for={"info_" + index}
                  data-tip={account.bank_connection.error_message || "Error"}
                />

                <ReactTooltip
                  id={"info_" + index}
                  className="custom-tooltip bg-gray-900"
                  textColor="#ffffff"
                  backgroundColor="#111827"
                  effect="solid"
                />
              </div>
              <div className="absolute w-full h-full blur bg-white opacity-80" />
              <div className="p-4 relative flex items-center justify-center z-10 h-full">
                <div className="flex items-center cursor-pointer bg-white py-1 px-4 border border-gray-300 rounded-full">
                  <div className="flex-shrink-0">
                    <svg
                      className="h-5 w-5 text-red-600"
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 20 20"
                      fill="currentColor"
                      aria-hidden="true"
                    >
                      <path
                        fillRule="evenodd"
                        d="M8.257 3.099c.765-1.36 2.722-1.36 3.486 0l5.58 9.92c.75 1.334-.213 2.98-1.742 2.98H4.42c-1.53 0-2.493-1.646-1.743-2.98l5.58-9.92zM11 13a1 1 0 11-2 0 1 1 0 012 0zm-1-8a1 1 0 00-1 1v3a1 1 0 002 0V6a1 1 0 00-1-1z"
                        clipRule="evenodd"
                      />
                    </svg>
                  </div>
                  <div className="ml-3">
                    <p
                      className="text-base font-semibold text-red-500"
                      onClick={() => openCloseReAuth(account)}
                    >
                      Feed disconnected, please re-authenticate
                    </p>
                  </div>
                </div>
              </div>
            </div>
          )}
        </li>
      ))}
    </ul>
  );
}
