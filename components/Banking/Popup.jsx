import { Fragment, useState } from 'react'
import { Dialog, Transition } from '@headlessui/react'
import { XIcon } from '@heroicons/react/outline'
import { useEffect } from 'react';

export default function Popup(props) {
  const { soleBalance, selected } = props;
  const [open, setOpen] = useState(false);

  useEffect(async () => {
    if (soleBalance < 0 && selected === 1) {
      if (!open) {
        if (localStorage.getItem('negative-balance-popup') !== null) {
          setOpen(false);
        } else {
          setOpen(true);
          localStorage.setItem('negative-balance-popup', true)
        }
      }
    }
  }, [soleBalance, selected]);

  return (
    <Transition.Root show={open} as={Fragment}>
      <Dialog as="div" className="fixed z-10 inset-0 overflow-y-auto" onClose={setOpen}>
        <div className="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <Dialog.Overlay className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity pointer-events-none" />
          </Transition.Child>

          {/* This element is to trick the browser into centering the modal contents. */}
          <span className="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">
            &#8203;
          </span>
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
            enterTo="opacity-100 translate-y-0 sm:scale-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100 translate-y-0 sm:scale-100"
            leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
          >
            <div className="relative inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-xl sm:w-full">
              <div className="hidden sm:block absolute top-0 right-0 pt-4 pr-4">
                <button
                  type="button"
                  className="bg-white rounded-full text-brand-blu hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                  onClick={() => setOpen(false)}
                >
                  <span className="sr-only">Close</span>
                  <XIcon className="h-8 w-8 p-1" aria-hidden="true" />
                </button>
              </div>

              <div>
                <div className='pt-6 px-4'>
                  <Dialog.Title as="h3" className="text-2xl text-center leading-6 py-4 px-2 font-semibold text-brand-blue">
                    Sole
                  </Dialog.Title>
                  <p className='text-center text-black'>
                    Your Sole balance is calculated based on the difference between your match income and expenses for the year --- no need to worry about the negative balance!
                  </p>
                </div>
              </div>

              <div className="py-7 px-5 sm:flex justify-center space-x-4">
                <button
                  type="button"
                  className="w-full inline-flex justify-center rounded-full  shadow-sm px-4 py-2 tracking-wide text-base font-medium text-brand-blue border-2 border-brand-blue hover:text-white hover:bg-brand-darkblue focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-500 sm:w-auto sm:text-base"
                  onClick={() => setOpen(false)}
                >
                  OK
                </button>

              </div>
            </div>
          </Transition.Child>
        </div>
      </Dialog>
    </Transition.Root>
  )
}
