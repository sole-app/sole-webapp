import { useState } from "react";
import { formatCurrencyWithoutSymbol } from "../../helpers/common";

export default function AccountList(props) {
  const { loader, customer, selectAccount } = props;
  const [accountId, setAccountId] = useState(0);

  return (
    <div className="absolute inset-0 py-6 px-4 sm:px-6">
      <div
        className="h-full max-w-xl m-auto border p-5 shadow-md border-gray-100 bank-accounts-type relative overflow-y-scroll"
        aria-hidden="true"
      >
        <div>
          <h2 className="text-3xl font-bold text-gray-700 text-center">
            Bank Accounts
          </h2>
          <p className="mt-4">
            Please select your business account from the list below. If you need
            to change the account any time, simply disconnect the active account
            and connect new one.
          </p>

          <div>
            {customer.accounts.map((account, index) => (
              <div
                key={index}
                onClick={() => setAccountId(account.id)}
                className={
                  account.id === accountId
                    ? "relative mt-10 rounded-lg border border-gray-300 bg-white px-6 py-5 shadow-sm flex items-center space-x-3 hover:border-gray-400 ring-2 ring-offset-2 ring-indigo-500"
                    : "relative mt-6 rounded-lg border border-gray-300 bg-white px-6 py-5 shadow-sm flex items-center space-x-3 hover:border-gray-400"
                }
              >
                <div className="flex-1 pl-2 min-w-0">
                  <a href="javascript:void(0);" className="focus:outline-none">
                    <span className="absolute inset-0" aria-hidden="true" />
                    <p className="text-lg font-semibold text-gray-900">
                      {account.name}
                    </p>
                    <p className="font-semibold text-gray-900">
                      {account.accountNumber} ({account.accountType}) $
                      {formatCurrencyWithoutSymbol(account.balance)}
                    </p>
                  </a>
                </div>
              </div>
            ))}
          </div>

          <button
            type="button"
            className="block mx-auto items-center mt-8 justify-center py-3 px-8 border border-transparent text-md font-medium rounded-full text-white bg-brand-blue hover:bg-brand-darkblue"
            onClick={() => selectAccount(accountId)}
            disabled={loader}
          >
            {loader ? "Processing..." : "Continue With Selected Account"}
          </button>
        </div>
      </div>
    </div>
  );
}
