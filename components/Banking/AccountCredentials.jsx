import bankIcons from "../../config/bankIcons";
import { HiChevronLeft } from "react-icons/hi";
import store from '../../store/store';
import { snackbarOpen } from '../../reducers/slices/snackbarSlice';
import { SNACKBAR_TYPE } from "../../components/Layout/Snackbar";

export default function AccountCredentials(props) {
    const { loader, institute, linkAccount, goBack, isReAuth } = props;

    let image = bankIcons.default;

    if (institute.slug !== '') {
        image = `https://test.bankstatements.com.au/images/institutions/${institute.slug}.png`;
    }

    const accountLink = async () => {
        const data = new FormData(document.getElementById('add-account'));
        if (typeof institute.credentials !== 'undefined' && institute.credentials.length > 0) {
            for (let i = 0; i < institute.credentials.length; i++) {
                if (document.getElementById(institute.credentials[i].fieldID).value == '') {
                    // check for optional param (like below example)
                        // description: ""
                        // fieldID: "token"
                        // isMFAField: true
                        // keyboardType: "default"
                        // name: "Token Code (if you have a security token or use the Suncorp Secured App)"
                        // optional: true
                        // type: "password"
                    if (!institute.credentials[i].optional) {
                        store.store.dispatch(snackbarOpen({
                            type: SNACKBAR_TYPE.ERROR,
                            message: `${institute.credentials[i].name} field is required.`
                        }));
                        return
                    }
                }
            }
        }
        await linkAccount(data);
    };

    return (
        <div className="absolute inset-0 py-6 px-4 sm:px-6">
            <div className=" max-w-xl m-auto border p-5 shadow-md border-gray-100"
                aria-hidden="true">
                {isReAuth ?
                    ''
                    :
                    <span className="go-back flex items-center cursor-pointer" onClick={() => goBack()}><HiChevronLeft />   Back</span>
                }

                <div>
                    <h3 className='font-bold pt-2 text-2xl text-gray-700 text-center'>Secure Connection</h3>

                    <div className="text-center w-72 mt-5 mx-auto p-5 justify-center rounded-md items-center bg-gray-50 border border-gray-100">
                        <img
                            className="text-center mx-auto"
                            src={image}
                            alt=""
                            width="120"
                        />

                        <h2 className='text-xl mt-4 font-bold text-gray-600 text-center'>{institute.name}</h2>
                    </div>

                    <form name="add-account" id="add-account">

                        <input
                            type="hidden"
                            name={`credentials[institution]`}
                            value={institute.slug}
                            className="mt-4"
                        />

                        {(typeof institute.credentials !== 'undefined' && institute.credentials.length > 0) ?
                            institute.credentials.map((inputBox, index) => (
                                <input
                                    key={index}
                                    type={inputBox.type}
                                    name={`credentials[${inputBox.fieldID}]`}
                                    id={inputBox.fieldID}
                                    autoComplete="new-password"
                                    className="placeholder-gray-400 mt-4 py-3 px-4 block w-full border-2 border-gray-200  focus:ring-indigo-400 focus:border-indigo-400 border-gray-100 rounded-md "
                                    placeholder={inputBox.name}
                                    minLength={inputBox.validation.minLength || 0}
                                    maxLength={inputBox.validation.maxLength || 999}
                                />
                            ))
                            :
                            ''
                        }

                        <button type="button"
                            className="block mx-auto items-center mt-8 justify-center py-3 px-10 border border-transparent text-md font-medium rounded-full text-white bg-brand-blue hover:bg-brand-darkblue"
                            onClick={() => accountLink()} disabled={loader}>
                            {loader ? "Linking..." : "Link Account"}
                        </button>
                    </form>
                </div>
                <p className="note text-center flex justify-center py-3 items-center">
                </p>
                <p className="note text-center flex justify-center items-center">
                    By providing your credentials, Sole establishes a secure connection with your bank to enable data display.
                </p>
            </div>
        </div>
    )
}
