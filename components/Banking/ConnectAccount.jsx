import { GrEmptyCircle } from "react-icons/gr";
import { IconContext } from "react-icons";

export default function ConnectAccount(props) {
    const { setOpenSlider } = props;

    return (
        <div className="relative shadow my-8 rounded-lg bg-white overflow-hidden">
            <div className="hidden sm:block sm:absolute sm:inset-y-0 sm:h-full sm:w-full" aria-hidden="true">
                <div className="relative h-full max-w-7xl mx-auto">

                    <svg
                        className="absolute left-full transform -translate-y-3/4 -translate-x-1/4 md:-translate-y-1/2 lg:-translate-x-1/2"
                        width={404}
                        height={784}
                        fill="none"
                        viewBox="0 0 404 784"
                    >
                        <defs>
                            <pattern
                                id="5d0dd344-b041-4d26-bec4-8d33ea57ec9b"
                                x={0}
                                y={0}
                                width={20}
                                height={20}
                                patternUnits="userSpaceOnUse"
                            >
                                <rect x={0} y={0} width={4} height={4} className="text-gray-200" fill="currentColor" />
                            </pattern>
                        </defs>
                        <rect width={404} height={784} fill="url(#5d0dd344-b041-4d26-bec4-8d33ea57ec9b)" />
                    </svg>
                </div>
            </div>

            <div className="relative">
                <main className=" mx-auto   p-10 ">
                    <div className='flex items-center'>
                        <div>
                            {/* <LlibraryIcon /> */}
                            <h1 className="text-4xl tracking-tight font-light text-gray-900 sm:text-5xl md:text-5xl">
                                <span className="block">Connect your bank account</span>{' '}
                                <span className="block text-brand-blue xl:inline">to match transactions</span>
                            </h1>
                            <p className="mt-3 max-w-md  text-base text-gray-500 sm:text-lg md:mt-5 md:text-xl md:max-w-3xl">
                                Link your bank account to Sole and enable a (secure) live bank feed to reconcile your transactions.
                            </p>

                            <div className='mt-5'>
                                <ul className='font-semibold  text-lg text-gray-500 space-y-4'>
                                    <li className="flex items-center">
                                        <span className="text-3xl font-bold  pr-3 relative">
                                            <IconContext.Provider value={{ className: "text-brand-blue" }}>
                                                <div>
                                                    <GrEmptyCircle />
                                                </div>
                                            </IconContext.Provider>
                                        </span>

                                        Connect a bank account to get started
                                    </li>
                                    <li className="flex items-center">
                                        <span className="text-3xl font-bold  pr-3 relative">
                                            <IconContext.Provider value={{ className: "text-brand-blue" }}>
                                                <div>
                                                    <GrEmptyCircle />
                                                </div>
                                            </IconContext.Provider>
                                        </span>
                                        Review and match your transactions
                                    </li>
                                    <li className="flex items-center">
                                        <span className="text-3xl font-bold  pr-3 relative">
                                            <IconContext.Provider value={{ className: "text-brand-blue" }}>
                                                <div>
                                                    <GrEmptyCircle />
                                                </div>
                                            </IconContext.Provider>
                                        </span>
                                        Track your performance
                                    </li>
                                </ul>
                            </div>

                            <div className="mt-5 max-w-md  md:mt-8">

                                <div>
                                    <button
                                        className="inline-flex items-center justify-center py-3 px-5 border border-transparent text-md font-medium rounded-full text-white bg-brand-blue hover:bg-brand-darkblue"
                                        onClick={() => setOpenSlider(true)}
                                    >
                                        Connect bank account
                                    </button>
                                </div>
                            </div>
                        </div>

                        <div className='relative 2xl:left-96'>

                            <img
                            src="/images/icon/undraw_calculator_re_alsc.svg"
                            width={300}
                            height={300}
                            alt="Connect bank "
                            />
                           
                        </div>
                    </div>
                </main>
            </div>
        </div>
    )
}
