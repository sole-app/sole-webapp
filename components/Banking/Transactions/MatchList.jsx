import { IoMdPaper } from "react-icons/io";
import Skeleton from "react-loading-skeleton";
import { formatCurrency, formatDate } from "../../../helpers/common";

export default function MatchList(props) {
  const {
    matchObj,
    listData,
    loader,
    matchLoader,
    multipleMatch,
    selectMultiple,
    expenseIds,
    assetIds,
    invoiceIds,
  } = props;

  if (typeof matchObj.type === "undefined") {
    return null;
  }

  if (matchObj.type === "debit") {
    //expense
    return (
      <div className="h-full divide-y divide-gray-200 flex flex-col bg-white">
        <div className="flex-1 h-0 pb-8 overflow-y-auto">
          <div className="flex-1 flex flex-col justify-between">
            <div className="px-4 divide-y divide-gray-200 sm:px-6">
              <div className="shadow mt-6 bg-white  overflow-hidden border-b border-gray-200 sm:rounded-lg">
                <div className="overflow-y-auto overflow-x-auto">
                  <table className="min-w-full  overflow-x-hidden divide-y hover-row divide-gray-200">
                    <thead className="">
                      <tr>
                        <th
                          scope="col"
                          className="px-6 py-3 w-12 text-left text-sm font-medium text-gray-500 uppercase tracking-wider"
                        >
                          <span className="sr-only">Select</span>
                        </th>
                        <th
                          scope="col"
                          className="px-6 py-3  text-left text-sm font-medium text-gray-500 uppercase tracking-wider"
                        >
                          Description
                        </th>
                        <th
                          scope="col"
                          className="px-6 py-3 text-left text-sm font-medium text-gray-500 uppercase tracking-wider"
                        >
                          Supplier
                        </th>
                        <th
                          scope="col"
                          className="px-6 py-3 text-center text-sm font-medium text-gray-500 uppercase tracking-wider"
                        >
                          Amount
                        </th>
                      </tr>
                    </thead>

                    <tbody className="bg-white divide-y divide-gray-200">
                      {!loader ? (
                        listData.length > 0 ? (
                          listData.map((item, index) => (
                            <>
                              {item.type === "asset" ? (
                                <tr key={index}>
                                  <td className="px-6 py-4 whitespace-nowrap">
                                    <div className="flex items-center">
                                      <div className="text-md font-medium text-gray-900">
                                        <input
                                          id={`expance_${item.asset_id}`}
                                          name={`expance_${item.asset_id}`}
                                          type="checkbox"
                                          className="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300 rounded"
                                          onClick={() =>
                                            selectMultiple(
                                              item.asset_id,
                                              item.type
                                            )
                                          }
                                          defaultChecked={
                                            assetIds.indexOf(item.asset_id) !==
                                            -1
                                          }
                                        />
                                      </div>
                                    </div>
                                  </td>

                                  <td className="px-6 py-4">
                                    <div className="flex items-center">
                                      <div className="text-md flex font-medium text-gray-900">
                                        <div className="circle-badge inline-flex items-center justify-center h-12 w-12 rounded-full bg-gray-100 text-gray-500 p-1.5 text-2xl">
                                          <IoMdPaper />
                                        </div>
                                        <div className="ml-4">
                                          <div className="text-md font-medium text-gray-500">
                                            <span className="font-semibold block text-gray-900">
                                              {item.name}
                                            </span>
                                            {item.account_subcategory.name ||
                                              ""}
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </td>

                                  <td className="px-6 py-4 whitespace-nowrap">
                                    <div className="flex items-center">
                                      <div className="text-md font-medium text-gray-900">
                                        {item?.client?.name}
                                      </div>
                                    </div>
                                  </td>

                                  <td className="px-6 py-4 whitespace-nowrap">
                                    <div className="flex justify-center items-center">
                                      <div className="text-md  font-medium text-gray-800">
                                        {formatCurrency(item.price)}
                                      </div>
                                    </div>
                                  </td>
                                </tr>
                              ) : (
                                <>
                                  {item.paid_with_cash ||
                                  item.bank_transaction_id > 0 ? (
                                    ""
                                  ) : (
                                    <tr key={`inner` + index}>
                                      <td className="px-6 py-4 whitespace-nowrap">
                                        <div className="flex items-center">
                                          <div className="text-md font-medium text-gray-900">
                                            <input
                                              id={`expance_${item.expense_id}`}
                                              name={`expance_${item.expense_id}`}
                                              type="checkbox"
                                              className="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300 rounded"
                                              onClick={() =>
                                                selectMultiple(
                                                  item.expense_id,
                                                  item.type
                                                )
                                              }
                                              defaultChecked={
                                                expenseIds.indexOf(
                                                  item.expense_id
                                                ) !== -1
                                              }
                                            />
                                          </div>
                                        </div>
                                      </td>

                                      <td className="px-6 py-4">
                                        <div className="flex items-center">
                                          <div className="text-md flex font-medium text-gray-900">
                                            <div className="circle-badge inline-flex items-center justify-center h-12 w-12 rounded-full bg-gray-100 text-gray-500 p-1.5 text-2xl">
                                              <IoMdPaper />
                                            </div>
                                            <div className="ml-4">
                                              <div className="text-md font-medium text-gray-500">
                                                <span className="font-semibold block text-gray-900">
                                                  {item.name}
                                                </span>
                                                {item.account_subcategory
                                                  .name || ""}
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </td>

                                      <td className="px-6 py-4 whitespace-nowrap">
                                        <div className="flex items-center">
                                          <div className="text-md font-medium text-gray-900">
                                            {item?.client?.name}
                                          </div>
                                        </div>
                                      </td>

                                      <td className="px-6 py-4 whitespace-nowrap">
                                        <div className="flex justify-center items-center">
                                          <div className="text-md  font-medium text-gray-800">
                                            {formatCurrency(item.price)}
                                          </div>
                                        </div>
                                      </td>
                                    </tr>
                                  )}
                                </>
                              )}
                            </>
                          ))
                        ) : (
                          <tr key="no_expense">
                            <td colSpan="4" align="center">
                              <span>No records found.</span>
                            </td>
                          </tr>
                        )
                      ) : (
                        Array.from(Array(3), (e, i) => {
                          return (
                            <tr key={i}>
                              <td className="px-6 py-3 whitespace-nowrap">
                                <Skeleton />
                              </td>
                              <td className="px-6 py-3 whitespace-nowrap">
                                <Skeleton />
                              </td>
                              <td className="px-6 py-3 whitespace-nowrap">
                                <Skeleton />
                              </td>
                              <td className="px-7 py-3 whitespace-nowrap">
                                <Skeleton />
                              </td>
                            </tr>
                          );
                        })
                      )}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="flex-shrink-0  px-4 py-4 flex justify-end">
          <button
            type="button"
            className="ml-4 inline-flex justify-center py-2 px-4 tracking-wider border border-transparent uppercase shadow-sm  font-medium text-sm rounded-full text-white bg-green-600 hover:bg-green-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-500"
            disabled={matchLoader}
            onClick={() => multipleMatch()}
          >
            {matchLoader ? "Matching..." : "Match Multiple"}
          </button>
        </div>
      </div>
    );
  } else if (matchObj.type === "credit") {
    //invoices
    return (
      <div className="h-full divide-y divide-gray-200 flex flex-col bg-white">
        <div className="flex-1 h-0 pb-8 overflow-y-auto">
          <div className="flex-1 flex flex-col justify-between">
            <div className="px-4 divide-y divide-gray-200 sm:px-6">
              <div className="shadow mt-6 bg-white  overflow-hidden border-b border-gray-200 sm:rounded-lg">
                <div className="overflow-y-auto overflow-x-auto">
                  <table className="min-w-full  overflow-x-hidden divide-y hover-row divide-gray-200">
                    <thead className="">
                      <tr>
                        <th
                          scope="col"
                          className="px-6 py-3 w-5 text-left text-sm font-medium text-gray-500 uppercase tracking-wider"
                        >
                          <span className="sr-only">Select</span>
                        </th>
                        <th
                          scope="col"
                          className="px-6 py-3 w-96 text-left text-sm font-medium text-gray-500 uppercase tracking-wider"
                        >
                          Description
                        </th>
                        <th
                          scope="col"
                          className="px-6 py-3 text-left text-sm font-medium text-gray-500 uppercase tracking-wider"
                        >
                          Date
                        </th>
                        <th
                          scope="col"
                          className="px-6 py-3 text-center text-sm font-medium text-gray-500 uppercase tracking-wider"
                        >
                          Amount
                        </th>
                      </tr>
                    </thead>

                    <tbody className="bg-white divide-y divide-gray-200">
                      {!loader ? (
                        listData.length > 0 ? (
                          listData.map((item, index) => (
                            <>
                              {item.paid_with_cash ||
                              item.bank_transaction_id > 0 ? (
                                ""
                              ) : (
                                <tr key={index}>
                                  <td className="px-6 py-4 whitespace-nowrap">
                                    <div className="flex items-center">
                                      <div className="text-md font-medium text-gray-900">
                                        <input
                                          id={`expance_${item.invoice_id}`}
                                          name={`expance_${item.invoice_id}`}
                                          type="checkbox"
                                          className="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300 rounded"
                                          onClick={() =>
                                            selectMultiple(item.invoice_id)
                                          }
                                          defaultChecked={
                                            invoiceIds.indexOf(
                                              item.invoice_id
                                            ) !== -1
                                          }
                                        />
                                      </div>
                                    </div>
                                  </td>

                                  <td className="px-6 py-4 whitespace-nowrap">
                                    <div className="flex items-center">
                                      <div className="text-md flex font-medium text-gray-900">
                                        <div className="ml-4">
                                          <div className="text-md font-medium text-gray-500">
                                            <span className="font-semibold block text-gray-900">
                                              {item.invoice_no}
                                            </span>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </td>

                                  <td className="px-6 py-4 whitespace-nowrap">
                                    <div className="flex items-center">
                                      <div className="text-md font-medium text-gray-900">
                                        {item.status}{" "}
                                        {formatDate(item.due_date)}
                                      </div>
                                    </div>
                                  </td>

                                  <td className="px-6 py-4 whitespace-nowrap">
                                    <div className="flex justify-center items-center">
                                      <div className="text-md  font-medium text-gray-800">
                                        {formatCurrency(item.total)}
                                      </div>
                                    </div>
                                  </td>
                                </tr>
                              )}
                            </>
                          ))
                        ) : (
                          <tr key="no_invoice">
                            <td colSpan="4" align="center">
                              <span>No Invoices Available.</span>
                            </td>
                          </tr>
                        )
                      ) : (
                        Array.from(Array(3), (e, i) => {
                          return (
                            <tr key={i}>
                              <td className="px-6 py-3 whitespace-nowrap">
                                <Skeleton />
                              </td>
                              <td className="px-6 py-3 whitespace-nowrap">
                                <Skeleton />
                              </td>
                              <td className="px-6 py-3 whitespace-nowrap">
                                <Skeleton />
                              </td>
                              <td className="px-7 py-3 whitespace-nowrap">
                                <Skeleton />
                              </td>
                            </tr>
                          );
                        })
                      )}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="flex-shrink-0  px-4 py-4 flex justify-end">
          <button
            type="button"
            className="ml-4 inline-flex justify-center py-2 px-4 tracking-wider border border-transparent uppercase shadow-sm  font-medium text-sm rounded-full text-white bg-green-600 hover:bg-green-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-500"
            disabled={matchLoader}
            onClick={() => multipleMatch()}
          >
            {matchLoader ? "Matching..." : "Match Multiple"}
          </button>
        </div>
      </div>
    );
  }

  return null;
}
