import { Fragment, useState, useEffect } from "react";
import { Dialog, Transition, Tab } from "@headlessui/react";
import { XIcon } from "@heroicons/react/outline";
import { Disclosure } from "@headlessui/react";
import { ChevronUpIcon } from "@heroicons/react/solid";
import { formatCurrency, formatDate } from "../../../helpers/common";
import {
  fetchPossibleMatchInvoices,
  setMatchExpenses,
  setMatchInvoices,
  getPossibleMatchExpenses,
  getPossibleMatchInvoices,
  setMatch,
} from "../../../services/api/bankTransaction.services";
import { Add as AddExpense } from "../../../services/api/expense.services";
import { Add } from "../../../services/api/asset.services";
import { List as ListInvoice } from "../../../services/api/invoice.services";
import {
  List as ListClient,
  Add as AddClient,
} from "../../../services/api/client.services";
import MatchList from "../../Banking/Transactions/MatchList";
import MatchForm from "../../Banking/Transactions/MatchForm";
import DetailedMatch from "../../Banking/Transactions/DetailedMatch";
import config from "../../../config/config";
import AlertDialog from "../../../components/AlertDialog";

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

export default function MatchTransaction(props) {
  const {
    matchOpen,
    setMatchOpen,
    matchObj,
    formType,
    matchDone,
    categories,
    setFormType,
  } = props;
  const [selectedTab, setSelectedTab] = useState(0);
  const [possibleMatch, setPossibleMatch] = useState([]);
  const [possibleMatchCount, setPossibleMatchCount] = useState(0);
  const [possibleMatchSelected, setPossibleMatchSelected] = useState(false);
  const [possibleMatchObj, setPossibleMatchObj] = useState({});
  const [possibleMatchLoader, setPossibleMatchLoader] = useState(false);
  const [possibleMatchingLoader, setPossibleMatchingLoader] = useState(false);
  const [updatePossibleMatch, setUpdatePossibleMatch] = useState(false);
  const [listData, setListData] = useState([]);
  const [listLoader, setListLoader] = useState(false);
  const [expenseIds, setExpenseIds] = useState([]);
  const [assetIds, setAssetds] = useState([]);
  const [invoiceIds, setInvoiceIds] = useState([]);
  const [matchLoader, setMatchLoader] = useState(false);
  const [clients, setClients] = useState([]);
  const [openAlert, setOpenAlert] = useState(false);
  const [alertMessage, setAlertMessage] = useState("");

  const [currentClient, setCurrentClient] = useState({});
  const [clientLoader, setClientLoader] = useState(false);

  useEffect(() => {
    setPossibleMatchSelected(false);
    setPossibleMatchObj({});
    setPossibleMatchingLoader(false);
    setUpdatePossibleMatch(false);
    if (typeof matchObj.type !== "undefined") {
      let callPossibleMatchApi = false;
      if (matchObj.type === "debit") {
        let count = 0;
        if (matchObj.possible_match_expense > 0) {
          count += matchObj.possible_match_expense;
        }
        if (matchObj.possible_match_asset > 0) {
          count += matchObj.possible_match_asset;
        }
        setPossibleMatchCount(count);
        if (count > 0) {
          callPossibleMatchApi = true;
        }
      } else if (matchObj.type === "credit" && matchObj.possible_match > 0) {
        callPossibleMatchApi = true;
        setPossibleMatchCount(matchObj.possible_match);
      }

      getListData();
      getClientOrSuppliers();

      if (callPossibleMatchApi) {
        getPossibleMatchData();
      } else {
        setPossibleMatchSelected(false);
        setPossibleMatchCount(0);
        setPossibleMatch([]);
      }
    }
  }, [matchObj]);

  const changeSelectedTab = async (select) => {
    setSelectedTab(select);
  };

  const getClientOrSuppliers = async () => {
    let formData = "?page_size=1000";
    if (matchObj.type === "debit") {
      formData += "&filter_by[type]=supplier";
    } else if (matchObj.type === "credit") {
      formData += "&filter_by[type]=client";
    }
    const result = await ListClient(formData);
    if (result.success) {
      setClients(result.data.clients_details);
    }
  };

  const getListData = async () => {
    setListLoader(true);
    let result = {};
    let formData = "";
    if (matchObj.type === "debit") {
      formData +=
        "?page_size=100&sort_by[date_of_purchase]=desc&filter_by[min_price]=0&filter_by[max_price]=" +
        matchObj.amount;
      result = await fetchPossibleMatchInvoices(formData);
    } else if (matchObj.type === "credit") {
      formData = "?page_size=100&sort_by[invoice_id]=desc";
      result = await ListInvoice(formData);
    }
    if (result.success) {
      if (matchObj.type === "debit") {
        setListData(
          typeof result.data.asset_expense ? result.data.asset_expense : []
        );
      } else if (matchObj.type === "credit") {
        setListData(
          typeof result.data.invoice_details ? result.data.invoice_details : []
        );
      }
      setListLoader(false);
    } else {
      setListLoader(false);
    }
  };

  const selectMultiple = (id, type = "expanse") => {
    if (type === "expense") {
      let ids = expenseIds;
      let index = ids.indexOf(id);
      if (index === -1) {
        ids.push(id);
        setExpenseIds(ids);
      } else {
        ids.splice(index, 1);
        setExpenseIds(ids);
      }
    } else if (type === "asset") {
      let ids = assetIds;
      let index = ids.indexOf(id);
      if (index === -1) {
        ids.push(id);
        setAssetds(ids);
      } else {
        ids.splice(index, 1);
        setAssetds(ids);
      }
    } else {
      let ids = invoiceIds;
      let index = ids.indexOf(id);
      if (index === -1) {
        ids.push(id);
        setInvoiceIds(ids);
      } else {
        ids.splice(index, 1);
        setInvoiceIds(ids);
      }
    }
  };

  const multipleMatch = async () => {
    let msg = "";
    if (matchObj.type === "debit" || matchObj.type === "credit") {
      let total = 0;
      let tranAmount = matchObj.amount;
      if (matchObj.type === "debit") {
        let hasSameSupplier = true;
        let clientId = "";
        for (let i = 0; i < listData.length; i++) {
          if (
            assetIds.indexOf(listData[i].asset_id) !== -1 &&
            listData[i].type === "asset"
          ) {
            if (clientId === "") {
              clientId = listData[i].client_id;
            } else {
              if (clientId !== listData[i].client_id) {
                hasSameSupplier = false;
              }
            }
            total += parseFloat(listData[i].price);
          }
          if (
            expenseIds.indexOf(listData[i].expense_id) !== -1 &&
            listData[i].type === "expense"
          ) {
            if (clientId === "") {
              clientId = listData[i].client_id;
            } else {
              if (clientId !== listData[i].client_id) {
                hasSameSupplier = false;
              }
            }
            total += parseFloat(listData[i].price);
          }
        }

        if (!hasSameSupplier) {
          //check diff supplier
          let msg = "The transactions selected are from different suppliers.";
          setAlertMessage(msg);
          setOpenAlert(true);
          return;
        }

        if (
          parseFloat(total).toFixed(2) === parseFloat(tranAmount).toFixed(2)
        ) {
          //call match apis
          setMatchLoader(true);
          const formData = new FormData();
          formData.append("bank_transaction_id", matchObj.bank_transacton_id);
          for (let i = 0; i < expenseIds.length; i++) {
            if (typeof expenseIds[i] !== "undefined" && expenseIds[i] !== "") {
              formData.append("expenses[]", expenseIds[i]);
            }
          }
          for (let i = 0; i < assetIds.length; i++) {
            if (typeof assetIds[i] !== "undefined" && assetIds[i] !== "") {
              formData.append("assets[]", assetIds[i]);
            }
          }
          const result = await setMatchExpenses(formData);
          if (result.success) {
            setMatchLoader(false);

            setMatchOpen(false);
            matchDone();
          } else {
            setMatchLoader(false);
          }
        } else {
          if (total > tranAmount) {
            msg =
              "Current selection is " +
              formatCurrency(total - tranAmount) +
              " over the selected balance";
          } else {
            msg =
              "Current selection is " +
              formatCurrency(tranAmount - total) +
              " under the selected balance";
          }
          setAlertMessage(msg);
          setOpenAlert(true);
        }
      } else {
        let hasSameSupplier = true;
        let clientId = "";

        for (let i = 0; i < listData.length; i++) {
          if (invoiceIds.indexOf(listData[i].invoice_id) !== -1) {
            if (clientId == "") {
              clientId = listData[i].client_id;
            } else {
              if (clientId !== listData[i].client_id) {
                hasSameSupplier = false;
              }
            }
            total += parseFloat(listData[i].total);
          }
        }

        if (!hasSameSupplier) {
          //check diff supplier
          let msg = "The transactions selected are from different clients.";
          setAlertMessage(msg);
          setOpenAlert(true);
          return;
        }

        if (
          parseFloat(total).toFixed(2) === parseFloat(tranAmount).toFixed(2)
        ) {
          //call match apis
          setMatchLoader(true);
          const formData = new FormData();
          formData.append("bank_transaction_id", matchObj.bank_transacton_id);
          for (let i = 0; i < invoiceIds.length; i++) {
            if (typeof invoiceIds[i] !== "undefined" && invoiceIds[i] !== "") {
              formData.append("invoices[]", invoiceIds[i]);
            }
          }
          const result = await setMatchInvoices(formData);
          if (result.success) {
            setMatchLoader(false);

            setMatchOpen(false);
            matchDone();
          } else {
            setMatchLoader(false);
          }
        } else {
          if (total > tranAmount) {
            msg =
              "Current selection is " +
              formatCurrency(total - tranAmount) +
              " over the balance to be matched";
          } else {
            msg =
              "Current selection is " +
              formatCurrency(tranAmount - total) +
              " under the balance to be matched";
          }
          setAlertMessage(msg);
          setOpenAlert(true);
        }
      }
    }
  };

  const updatePossibleMatchState = async (item) => {
    setPossibleMatchSelected(true);
    setPossibleMatchObj(item);
    setPossibleMatchingLoader(false);
    setFormType("MATCH");
    setUpdatePossibleMatch(!updatePossibleMatch);
  };

  const selectPossibleMatch = async (item) => {
    if (possibleMatchingLoader) {
      return;
    }
    setFormType("ADD");
    setPossibleMatchingLoader(true);
    if (!possibleMatchSelected) {
      await updatePossibleMatchState(item);
      return;
    } else {
      let add = false;
      if (matchObj.type === "debit") {
        if (
          possibleMatchObj.type === "asset" &&
          possibleMatchObj.asset_id !== item.asset_id
        ) {
          add = true;
        } else if (
          possibleMatchObj.type === "expense" &&
          possibleMatchObj.expense_id !== item.expense_id
        ) {
          add = true;
        }
      } else {
        if (possibleMatchObj.id !== item.id) {
          add = true;
        }
      }
      if (add) {
        await updatePossibleMatchState(item);
        return;
      }
    }

    if (matchObj.type === "debit" || matchObj.type === "credit") {
      if (matchObj.type === "debit") {
        //call match apis
        const formData = new FormData();
        formData.append("bank_transaction_id", matchObj.bank_transacton_id);
        if (possibleMatchObj.type === "asset") {
          formData.append("assets[]", possibleMatchObj.asset_id);
        } else {
          formData.append("expenses[]", possibleMatchObj.expense_id);
        }

        const result = await setMatchExpenses(formData);
        if (result.success) {
          setMatchOpen(false);
          setPossibleMatchingLoader(false);
          matchDone();
        } else {
          setPossibleMatchingLoader(false);
        }
      } else {
        //call match apis
        const formData = new FormData();
        formData.append("bank_transaction_id", matchObj.bank_transacton_id);
        formData.append("invoices[]", possibleMatchObj.id);

        const result = await setMatchInvoices(formData);
        if (result.success) {
          setMatchOpen(false);
          setPossibleMatchingLoader(false);
          matchDone();
        } else {
          setPossibleMatchingLoader(false);
        }
      }
    }
  };

  const getPossibleMatchData = async () => {
    if (matchObj.type === "debit" || matchObj.type === "credit") {
      setPossibleMatchLoader(true);
      const formData = new FormData();
      formData.append("bank_transaction_id", matchObj.bank_transacton_id);
      const result =
        matchObj.type === "debit"
          ? await getPossibleMatchExpenses(matchObj.bank_transacton_id)
          : await getPossibleMatchInvoices(formData);
      if (result.success) {
        let list =
          matchObj.type === "debit"
            ? result.data.asset_expense
            : result.data.invoices;
        setPossibleMatch(list);
        setPossibleMatchLoader(false);
      } else {
        setPossibleMatchLoader(false);
      }
    }
  };

  const matchManually = async (data) => {
    if (
      (matchObj.type === "credit" || matchObj.type === "debit") &&
      typeof data.client_id !== "undefined"
    ) {
      if (data.client_id === 0) {
        const formData = new FormData();
        formData.append("from_bank_match", 1);
        formData.append("name", data.client_name);
        formData.append("type", matchObj.type === "credit" ? 1 : 2);
        const result = await AddClient(formData);
        if (result.success) {
          data.client_id = result.data.id;
        }
      }

      if (data.client_id > 0) {
        if (formType === "MATCH") {
          if (matchObj.type === "debit") {
            if (possibleMatchObj.type === "asset") {
              const formData1 = new FormData();
              formData1.append(
                "bank_transaction_id",
                matchObj.bank_transacton_id
              );
              formData1.append("assets[]", data.asset_id);
              const result1 = await setMatchExpenses(formData1);
              if (result1.success) {
                const formData2 = new FormData();
                formData2.append(
                  "bank_transaction_id",
                  data.bank_transaction_id
                );
                formData2.append(
                  "account_subcategory_id",
                  data.account_subcategory_id
                );
                formData2.append("update_match", 1);
                formData2.append("client_id", data.client_id);
                formData2.append("user_description", data.user_description);
                const result2 = await setMatch(formData2);
                if (result2.success) {
                  setMatchOpen(false);
                  matchDone();
                }
              }
            } else {
              const formData1 = new FormData();
              formData1.append(
                "bank_transaction_id",
                matchObj.bank_transacton_id
              );
              formData1.append("expenses[]", data.expense_id);
              const result1 = await setMatchExpenses(formData1);
              if (result1.success) {
                const formData2 = new FormData();
                formData2.append(
                  "bank_transaction_id",
                  data.bank_transaction_id
                );
                formData2.append(
                  "account_subcategory_id",
                  data.account_subcategory_id
                );
                formData2.append("client_id", data.client_id);
                if (possibleMatchObj.type !== "asset") {
                  formData2.append("user_description", data.user_description);
                }
                const result2 = await setMatch(formData2);
                if (result2.success) {
                  setMatchOpen(false);
                  matchDone();
                }
              }
            }
          } else {
            //call match apis
            const formData1 = new FormData();
            formData1.append(
              "bank_transaction_id",
              matchObj.bank_transacton_id
            );
            formData1.append("invoice_id", data.invoice_id);
            formData1.append(
              "account_subcategory_id",
              data.account_subcategory_id
            );
            formData1.append("client_id", data.client_id);
            formData1.append("user_description", data.user_description);
            const result1 = await setMatch(formData1);
            if (result1.success) {
              setMatchOpen(false);
              matchDone();
            }
          }
        } else {
          if (matchObj.type === "credit" || formType === "EDIT") {
            const formData2 = new FormData();
            for (const [key, value] of Object.entries(data)) {
              formData2.append(key, value);
            }
            const result2 = await setMatch(formData2);
            if (result2.success) {
              setMatchOpen(false);
              matchDone();
            }
          } else if (matchObj.type === "debit") {
            const formData2 = new FormData();
            formData2.append("from_bank_match", 1);
            formData2.append("price", matchObj.amount);
            formData2.append("name", data.name);
            formData2.append("paid_with_cash", 0);
            formData2.append("gst", 1);
            formData2.append(
              "account_subcategory_id",
              data.account_subcategory_id
            );
            formData2.append("client_id", data.client_id);
            formData2.append("bank_transaction_id", data.bank_transaction_id);
            let result2;
            if (data.account_category_id === config.expanseCategoryId) {
              formData2.append("date", matchObj.transaction_date);
              formData2.append("is_asset", "no");
              formData2.append("user_description", data.user_description);
              result2 = await AddExpense(formData2);
            } else if (data.account_category_id === config.assetCategoryId) {
              formData2.append("date_of_purchase", matchObj.transaction_date);
              formData2.append("user_description", data.user_description);
              result2 = await Add(formData2);
            }
            if (result2.success) {
              setMatchOpen(false);
              matchDone();
            }
          }
        }
      }
    }
  };

  const matchDetailed = async (client_id) => {
    if (matchObj.type === "credit") {
      let account_subcategory_id = 0;
      if (
        typeof categories.income !== "undefined" &&
        categories.income.length > 0
      ) {
        account_subcategory_id = categories.income[0].account_subcategory_id;
      }
      const formData = new FormData();
      formData.append("bank_transaction_id", matchObj.bank_transacton_id);
      formData.append("account_subcategory_id", account_subcategory_id);
      formData.append("client_id", client_id);
      const result = await setMatch(formData);
      if (result.success) {
        setMatchOpen(false);
        matchDone();
      }
    } else {
      setMatchOpen(false);
      matchDone();
    }
  };

  const addClient = async (client_name) => {
    setClientLoader(true);
    const formData1 = new FormData();
    formData1.append("name", client_name);
    if (matchObj.type === "debit") {
      formData1.append("type", 2);
    } else if (matchObj.type === "credit") {
      formData1.append("type", 1);
    }
    const result1 = await AddClient(formData1);
    setClientLoader(false);
    if (result1.success) {
      let temp = clients;
      temp.push({
        name: client_name,
        client_id: result1.data.id,
      });
      setClients(temp);
      return {
        client_id: result1.data.id,
        name: client_name,
      };
    }
    return {};
  };

  if (Object.keys(matchObj).length === 0) {
    return null;
  }

  if (formType === "ADD" || formType === "MATCH") {
    return (
      <>
        <Transition.Root show={matchOpen} as={Fragment}>
          <Dialog
            as="div"
            static
            className="fixed inset-0 overflow-hidden"
            onClose={setMatchOpen}
          >
            <div className="absolute inset-0 overflow-hidden">
              <Transition.Child
                as={Fragment}
                enter="ease-in-out duration-500"
                enterFrom="opacity-0"
                enterTo="opacity-100"
                leave="ease-in-out duration-500"
                leaveFrom="opacity-100"
                leaveTo="opacity-0"
              >
                <Dialog.Overlay className="absolute inset-0 bg-gray-500 bg-opacity-75 transition-opacity pointer-events-none" />
              </Transition.Child>

              <div className="fixed inset-y-0 max-w-full right-0 flex">
                <Transition.Child
                  as={Fragment}
                  enter="transform transition ease-in-out duration-500 sm:duration-700"
                  enterFrom="translate-x-full"
                  enterTo="translate-x-0"
                  leave="transform transition ease-in-out duration-500 sm:duration-700"
                  leaveFrom="translate-x-0"
                  leaveTo="translate-x-full"
                >
                  <div className="w-screen lg:max-w-4xl">
                    <div className="h-full divide-y divide-gray-200 flex flex-col bg-white">
                      <div className="border-b border-gray-200 bg-gray-100 py-4 px-4 sm:px-6">
                        <div className="flex items-center justify-between">
                          <Dialog.Title className="text-xl font-medium text-gray-800">
                            Match Transaction
                            <p className="mt-1 font-normal text-sm text-gray-500">
                              Fields marked with * are mandatory
                            </p>
                          </Dialog.Title>
                          <div className="ml-3 h-7 flex items-center">
                            <button
                              type="button"
                              onClick={() => setMatchOpen(false)}
                              className="rounded-full hover:bg-gray-300 p-2 text-gray-500   hover:text-gray-700  focus:outline-none focus:ring-2 focus:ring-white"
                            >
                              <span className="sr-only">Close panel</span>
                              <XIcon className="h-6 w-6" aria-hidden="true" />
                            </button>
                          </div>
                        </div>
                      </div>

                      {/* Main Transaction */}
                      <div className="text-md py-3 px-6 bg-gray-50 flex font-medium items-center justify-between text-gray-900">
                        <div className="text-md font-medium text-gray-500">
                          <span className="font-medium block text-gray-900">
                            {matchObj.description}
                          </span>
                          <span className="text-xs">
                            {formatDate(matchObj.transaction_date)}
                          </span>
                        </div>

                        <div className="ml-4">
                          <span>{formatCurrency(matchObj.amount)}</span>
                        </div>
                      </div>

                      {/* Possible match transaction list */}
                      {possibleMatchCount > 0 && !possibleMatchLoader ? (
                        <div className="text-md bg-gray-50">
                          <Disclosure>
                            {({ open }) => (
                              <>
                                <Disclosure.Button className="flex justify-between py-6 px-6  w-full  text-md font-medium text-gray-700 rounded-lg focus:outline-none focus-visible:ring focus-visible:ring-purple-500 focus-visible:ring-opacity-75">
                                  <span>
                                    {possibleMatchCount} Possible Match
                                  </span>
                                  <ChevronUpIcon
                                    className={`${
                                      open ? "transform rotate-180" : ""
                                    } w-5 h-5 text-gray-500`}
                                  />
                                </Disclosure.Button>
                                <Disclosure.Panel className=" my-6 mx-6 divide-y divide-gray-200 rounded-md mt-3 border bg-white border-gray-200 text-gray-500">
                                  {possibleMatch.map((item, index) => (
                                    <>
                                      <div
                                        key={index}
                                        className={classNames(
                                          "flex  py-2 px-4 justify-between items-center",
                                          possibleMatchSelected &&
                                            (matchObj.type === "debit"
                                              ? item.type === "asset"
                                                ? possibleMatchObj.asset_id ===
                                                  item.asset_id
                                                : possibleMatchObj.expense_id ===
                                                  item.expense_id
                                              : possibleMatchObj.id === item.id)
                                            ? "bg-green-500 text-white"
                                            : ""
                                        )}
                                      >
                                        <div>
                                          <span
                                            className={classNames(
                                              "block text-base",
                                              possibleMatchSelected &&
                                                (matchObj.type === "debit"
                                                  ? item.type === "asset"
                                                    ? possibleMatchObj.asset_id ===
                                                      item.asset_id
                                                    : possibleMatchObj.expense_id ===
                                                      item.expense_id
                                                  : possibleMatchObj.id ===
                                                    item.id)
                                                ? "text-white"
                                                : "text-gray-600"
                                            )}
                                          >
                                            {matchObj.type === "debit"
                                              ? typeof item.client !==
                                                "undefined"
                                                ? item.name +
                                                  " (Supplier: " +
                                                  item.client.name +
                                                  ")"
                                                : item.name
                                              : item.invoice_no}
                                          </span>
                                          <span className="text-sm">
                                            {formatCurrency(
                                              matchObj.type === "debit"
                                                ? item.price
                                                : item.total
                                            )}
                                          </span>
                                          {matchObj.type === "debit" ? (
                                            <>
                                              &nbsp;&nbsp;
                                              <span className="inline-flex items-center px-2.5 pt-1 py-0.5 rounded-full text-xs font-medium bg-green-100 text-green-800">
                                                {item.type === "asset"
                                                  ? "Asset"
                                                  : "Expense"}
                                              </span>
                                            </>
                                          ) : (
                                            ""
                                          )}
                                        </div>

                                        <div>
                                          <span
                                            className={classNames(
                                              "cursor-pointer",
                                              possibleMatchSelected &&
                                                (matchObj.type === "debit"
                                                  ? item.type === "asset"
                                                    ? possibleMatchObj.asset_id ===
                                                      item.asset_id
                                                    : possibleMatchObj.expense_id ===
                                                      item.expense_id
                                                  : possibleMatchObj.id ===
                                                    item.id)
                                                ? "text-white"
                                                : "text-brand-blue"
                                            )}
                                            onClick={() =>
                                              selectPossibleMatch(item)
                                            }
                                          >
                                            {possibleMatchSelected &&
                                            (matchObj.type === "debit"
                                              ? item.type === "asset"
                                                ? possibleMatchObj.asset_id ===
                                                  item.asset_id
                                                : possibleMatchObj.expense_id ===
                                                  item.expense_id
                                              : possibleMatchObj.id === item.id)
                                              ? possibleMatchingLoader
                                                ? "Matching..."
                                                : "Match"
                                              : "Select"}
                                          </span>
                                        </div>
                                      </div>
                                    </>
                                  ))}
                                </Disclosure.Panel>
                              </>
                            )}
                          </Disclosure>
                        </div>
                      ) : (
                        ""
                      )}

                      <Tab.Group
                        selectedIndex={selectedTab}
                        onChange={(index) => changeSelectedTab(index)}
                      >
                        <Tab.List className="flex  space-x-1 border-b border-grey-400 bg-blue-900/20 ">
                          <Tab
                            className={classNames(
                              "py-6 px-6 text-base  leading-5 font-medium",
                              "focus:outline-none",
                              selectedTab === 0
                                ? "text-brand-blue border-b border-brand-blue"
                                : "text-grey-400 hover:bg-white/[0.12] "
                            )}
                          >
                            Create
                          </Tab>

                          <Tab
                            className={classNames(
                              "py-6 px-6 text-base  leading-5 font-medium",
                              "focus:outline-none",
                              selectedTab === 1
                                ? "text-brand-blue border-b border-brand-blue"
                                : "text-grey-400 hover:bg-white/[0.12] "
                            )}
                          >
                            Detailed Match
                          </Tab>

                          <Tab
                            className={classNames(
                              "py-6 px-6 text-base  leading-5 font-medium",
                              "focus:outline-none",
                              selectedTab === 2
                                ? "text-brand-blue border-b border-brand-blue"
                                : "text-grey-400 hover:bg-white/[0.12] "
                            )}
                          >
                            Assign to Multiple
                          </Tab>
                        </Tab.List>

                        <Tab.Panels className="h-full overflow-y-auto">
                          <Tab.Panel
                            key="0"
                            className={classNames(
                              "bg-white h-full  rounded-xl",
                              "focus:outline-none   ring-white ring-opacity-60"
                            )}
                          >
                            <MatchForm
                              formType={formType}
                              matchObj={matchObj}
                              clients={clients}
                              possibleMatchObj={possibleMatchObj}
                              updatePossibleMatch={updatePossibleMatch}
                              categories={
                                matchObj.type === "debit"
                                  ? categories.expanse
                                  : categories.income
                              }
                              clientLoader={clientLoader}
                              matchManually={matchManually}
                              setMatchOpen={setMatchOpen}
                              addClient={addClient}
                              setCurrentClient={setCurrentClient}
                            />
                          </Tab.Panel>

                          <Tab.Panel
                            key="1"
                            className={classNames(
                              "bg-white h-full  rounded-xl",
                              "focus:outline-none   ring-white ring-opacity-60"
                            )}
                          >
                            <DetailedMatch
                              matchObj={matchObj}
                              clients={clients}
                              accountCategories={categories.expanse}
                              matchDetailed={matchDetailed}
                            />
                          </Tab.Panel>

                          <Tab.Panel
                            key="2"
                            className={classNames(
                              "bg-white h-full  rounded-xl",
                              "focus:outline-none   ring-white ring-opacity-60"
                            )}
                          >
                            <MatchList
                              matchObj={matchObj}
                              listData={listData}
                              loader={listLoader}
                              matchLoader={matchLoader}
                              expenseIds={expenseIds}
                              assetIds={assetIds}
                              invoiceIds={invoiceIds}
                              multipleMatch={multipleMatch}
                              selectMultiple={selectMultiple}
                            />
                          </Tab.Panel>
                        </Tab.Panels>
                      </Tab.Group>
                    </div>
                  </div>
                </Transition.Child>
              </div>
              <>
                <AlertDialog
                  id="alert-menu"
                  open={openAlert}
                  onClose={setOpenAlert}
                  title="Message"
                  message={alertMessage}
                />
              </>
            </div>
          </Dialog>
        </Transition.Root>
      </>
    );
  } else {
    return (
      <>
        <Transition.Root show={matchOpen} as={Fragment}>
          <Dialog
            as="div"
            static
            className="fixed inset-0 overflow-hidden"
            onClose={setMatchOpen}
          >
            <div className="absolute inset-0 overflow-hidden">
              <Transition.Child
                as={Fragment}
                enter="ease-in-out duration-500"
                enterFrom="opacity-0"
                enterTo="opacity-100"
                leave="ease-in-out duration-500"
                leaveFrom="opacity-100"
                leaveTo="opacity-0"
              >
                <Dialog.Overlay className="absolute inset-0 bg-gray-500 bg-opacity-75 transition-opacity pointer-events-none" />
              </Transition.Child>

              <div className="fixed inset-y-0 max-w-full right-0 flex">
                <Transition.Child
                  as={Fragment}
                  enter="transform transition ease-in-out duration-500 sm:duration-700"
                  enterFrom="translate-x-full"
                  enterTo="translate-x-0"
                  leave="transform transition ease-in-out duration-500 sm:duration-700"
                  leaveFrom="translate-x-0"
                  leaveTo="translate-x-full"
                >
                  <div className="w-screen lg:max-w-4xl">
                    <div className="h-full divide-y divide-gray-200 flex flex-col bg-white">
                      <div className="border-b border-gray-200 bg-gray-100 py-4 px-4 sm:px-6">
                        <div className="flex items-center justify-between">
                          <Dialog.Title className="text-xl font-medium text-gray-800">
                            Match Transaction
                            <p className="mt-1 font-normal text-sm text-gray-500">
                              Fields marked with * are mandatory
                            </p>
                          </Dialog.Title>
                          <div className="ml-3 h-7 flex items-center">
                            <button
                              type="button"
                              onClick={() => setMatchOpen(false)}
                              className="rounded-full hover:bg-gray-300 p-2 text-gray-500   hover:text-gray-700  focus:outline-none focus:ring-2 focus:ring-white"
                            >
                              <span className="sr-only">Close panel</span>
                              <XIcon className="h-6 w-6" aria-hidden="true" />
                            </button>
                          </div>
                        </div>
                      </div>

                      {/* Main Transaction */}
                      <div className="text-md py-3 px-6 bg-gray-50 flex font-medium items-center justify-between text-gray-900">
                        <div className="text-md font-medium text-gray-500">
                          <span className="font-medium block text-gray-900">
                            {matchObj.description}
                          </span>
                          <span className="text-xs">
                            {formatDate(matchObj.transaction_date)}
                          </span>
                        </div>

                        <div className="ml-4">
                          <span>{formatCurrency(matchObj.amount)}</span>
                        </div>
                      </div>

                      <div className="text-md py-3 px-6 bg-gray-50 flex font-medium items-center justify-between text-gray-900">
                        <div className="text-md font-medium text-gray-500">
                          <span className="block">
                            {matchObj.type === "debit"
                              ? "To change Supplier details please undo match on the Sole Balance and start fresh."
                              : "To change Client details please undo match on the Sole Balance and start fresh."}
                          </span>
                        </div>
                      </div>

                      <div className="h-full overflow-y-auto">
                        <div className="bg-white h-full rounded-xl focus:outline-none ring-white ring-opacity-60 mt-4">
                          <MatchForm
                            formType={formType}
                            matchObj={matchObj}
                            clients={clients}
                            possibleMatchObj={possibleMatchObj}
                            updatePossibleMatch={updatePossibleMatch}
                            categories={
                              matchObj.type === "debit"
                                ? categories.expanse
                                : categories.income
                            }
                            clientLoader={clientLoader}
                            matchManually={matchManually}
                            setMatchOpen={setMatchOpen}
                            addClient={addClient}
                            setCurrentClient={setCurrentClient}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </Transition.Child>
              </div>
              <>
                <AlertDialog
                  id="alert-menu"
                  open={openAlert}
                  onClose={setOpenAlert}
                  message={alertMessage}
                />
              </>
            </div>
          </Dialog>
        </Transition.Root>
      </>
    );
  }
}
