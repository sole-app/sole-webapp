import { useState, useEffect } from "react";
import { Tab } from "@headlessui/react";
import {
  fetchAllBalance,
  soleTransactions as getSoleTransactions,
  bankTransaction,
  markPersonal as markTransactionPersonal,
  undo as undoTransaction,
  undoCashInvoice,
} from "../../../services/api/bankTransaction.services";
import MatchTransaction from "../../../components/Banking/Transactions/MatchTransaction";
import TransactionList from "../../../components/Banking/Transactions/TransactionList";
import AddExpenses from "../../../components/Expenses/AddExpenses";
import AddAssets from "../../../components/Assets/AddAssets";
import BankBalanceFilter from "../../../components/Banking/Transactions/BankBalanceFilter";
import SoleBalanceFilter from "../../../components/Banking/Transactions/SoleBalanceFilter";
import YearFilter from "../../../components/Banking/Transactions/YearFilter";
import Popup from "../../../components/Banking/Popup";
import { CheckIcon, SelectorIcon } from "@heroicons/react/solid";
import { AdjustmentsIcon } from "@heroicons/react/outline";
import {
  getFYList,
  getCurrentFY,
  getDateFromFYYears,
  formatCurrency,
} from "../../../helpers/common";
import store from "../../../store/store";
import { snackbarOpen } from "../../../reducers/slices/snackbarSlice";
import { SNACKBAR_TYPE } from "../../../components/Layout/Snackbar";

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

export default function BankTransactions(props) {
  const [open, setOpen] = useState(false);
  const [openSole, setOpenSole] = useState(false);

  const [updateId, setUpdateId] = useState("");

  const { categories, accounts } = props;

  const [displayConfirm, setDisplayConfirm] = useState(true);
  const [bankLoader, setBankLoader] = useState(false);
  const [bankTransactions, setBankTransactions] = useState([]);
  const [bankPage, setBankPage] = useState(0);
  const [bankPageSize, setBankPageSize] = useState(25);
  const [bankTotalTransaction, setBankTotalTransaction] = useState([]);
  const [soleLoader, setSoleLoader] = useState(false);
  const [soleTransactions, setSoleTransactions] = useState([]);
  const [solePage, setSolePage] = useState(0);
  const [solePageSize, setSolePageSize] = useState(25);
  const [soleTotalTransaction, setSoleTotalTransaction] = useState([]);
  const [matchOpen, setMatchOpen] = useState(false);
  const [matchObj, setMatchObj] = useState({});
  const [formType, setFormType] = useState("ADD"); //ADD, EDIT, MATCH
  const [selected, setSelected] = useState(0);
  const [confirmOpen, setConfirmOpen] = useState(false);
  const [selectedId, setSelectedId] = useState(0);
  const [confirmType, setConfirmType] = useState("DELETE"); //UNDO, DELETE, EDITEXPANSE, EDITASSET
  const [bankFilters, setBankFilters] = useState({});
  const [soleFilters, setSoleFilters] = useState({});
  const [balances, setBalances] = useState({
    sole_balance: 0,
    bank_balance: 0,
  });

  const [financialYear, setFinancialYear] = useState(getCurrentFY());
  const [yearDates, setYearDates] = useState({});
  const [yearList] = useState(getFYList());

  useEffect(() => {
    changeSelected(selected);
  }, [props.refresh]);

  const setYear = async (year) => {
    let yearData = getDateFromFYYears(year);
    setYearDates(yearData);
    setFinancialYear(year);

    let filters = {
      yearDates: yearData,
    };
    if (selected === 0) {
      await getBankTransactionsList(filters);
    } else if (selected === 1) {
      await getSoleTransactionsList(filters);
    }
  };

  const getAllBalance = async (filter = {}) => {
    let payload = "";
    if (
      typeof filter.yearDates !== "undefined" &&
      typeof filter.yearDates.start_date !== "undefined" &&
      typeof filter.yearDates.end_date !== "undefined"
    ) {
      payload += "?filter_by[start_date]=" + filter.yearDates.start_date;
      payload += "&filter_by[end_date]=" + filter.yearDates.end_date;
    } else if (
      typeof yearDates !== "undefined" &&
      typeof yearDates.start_date !== "undefined" &&
      typeof yearDates.end_date !== "undefined"
    ) {
      payload += "?filter_by[start_date]=" + yearDates.start_date;
      payload += "&filter_by[end_date]=" + yearDates.end_date;
    }

    const result = await fetchAllBalance(payload);
    setBalances({
      sole_balance: 0,
      bank_balance: 0,
    });
    if (result.success) {
      setBalances(result.data.balances);

      if (
        typeof result.data.balances !== "undefined" &&
        typeof result.data.balances.bank_account !== "undefined" &&
        result.data.balances.bank_account.length > 0
      ) {
        let data = {};
        for (let i = 0; i < result.data.balances.bank_account.length; i++) {
          data[result.data.balances.bank_account[i].bank_account_id] =
            result.data.balances.bank_account[i];
        }
        props.setMatchUnmatchTransactions(data);
      }
    }
  };

  const changeSelected = async (select) => {
    setSelected(select);
    if (select === 0 && !bankLoader) {
      setBankLoader(true);
      await getBankTransactionsList();
    } else if (select === 1 && !soleLoader) {
      setSoleLoader(true);
      await getSoleTransactionsList();
    }
  };

  const setFilter = async (type = "bank", filter = {}) => {
    if (type == "bank") {
      setBankFilters(filter);
      await getBankTransactionsList(filter);
    } else if (type == "sole") {
      setSoleFilters(filter);
      await getSoleTransactionsList(filter);
    }
  };

  const getSoleTransactionsList = async (filter = {}) => {
    setSoleLoader(true);

    let payload =
      "?page=" +
      (typeof filter.page !== "undefined" ? filter.page + 1 : solePage + 1);
    payload +=
      "&page_size=" +
      (typeof filter.pageSize !== "undefined" ? filter.pageSize : solePageSize);

    if (
      typeof filter.yearDates !== "undefined" &&
      typeof filter.yearDates.start_date !== "undefined" &&
      typeof filter.yearDates.end_date !== "undefined"
    ) {
      payload += "&filter_by[start_date]=" + filter.yearDates.start_date;
      payload += "&filter_by[end_date]=" + filter.yearDates.end_date;
    } else if (
      typeof yearDates !== "undefined" &&
      typeof yearDates.start_date !== "undefined" &&
      typeof yearDates.end_date !== "undefined"
    ) {
      payload += "&filter_by[start_date]=" + yearDates.start_date;
      payload += "&filter_by[end_date]=" + yearDates.end_date;
    }

    let fromFilter = false;
    if (typeof filter.fromFilter !== "undefined" && filter.fromFilter) {
      fromFilter = true;
    }

    let catIds = [];
    if (
      typeof filter.account_subcategory_id !== "undefined" &&
      filter.account_subcategory_id.length > 0 &&
      fromFilter
    ) {
      catIds = filter.account_subcategory_id;
    } else if (
      typeof bankFilters.account_subcategory_id !== "undefined" &&
      bankFilters.account_subcategory_id.length > 0 &&
      !fromFilter
    ) {
      catIds = bankFilters.account_subcategory_id;
    }
    for (let i = 0; i < catIds.length; i++) {
      payload += "&filter_by[account_subcategory_id][]=" + catIds[i];
    }

    if (
      typeof filter.cash !== "undefined" &&
      filter.cash !== "" &&
      fromFilter
    ) {
      payload += "&filter_by[cash]=" + filter.cash;
    } else if (
      typeof bankFilters.cash !== "undefined" &&
      bankFilters.cash !== "" &&
      !fromFilter
    ) {
      payload += "&filter_by[cash]=" + bankFilters.cash;
    }

    if (
      typeof filter.personal !== "undefined" &&
      filter.personal !== "" &&
      fromFilter
    ) {
      payload += "&filter_by[personal]=" + filter.personal;
    } else if (
      typeof bankFilters.cash !== "undefined" &&
      bankFilters.personal !== "" &&
      !fromFilter
    ) {
      payload += "&filter_by[personal]=" + bankFilters.personal;
    }

    if (
      typeof filter.start_date !== "undefined" &&
      filter.start_date !== "" &&
      fromFilter
    ) {
      payload += "&filter_by[start_date]=" + filter.start_date;
    } else if (
      typeof bankFilters.start_date !== "undefined" &&
      bankFilters.start_date !== "" &&
      !fromFilter
    ) {
      payload += "&filter_by[start_date]=" + bankFilters.start_date;
    }

    if (
      typeof filter.end_date !== "undefined" &&
      filter.end_date !== "" &&
      fromFilter
    ) {
      payload += "&filter_by[end_date]=" + filter.end_date;
    } else if (
      typeof bankFilters.end_date !== "undefined" &&
      bankFilters.end_date !== "" &&
      !fromFilter
    ) {
      payload += "&filter_by[end_date]=" + bankFilters.end_date;
    }

    if (
      typeof filter.min_price !== "undefined" &&
      filter.min_price !== "" &&
      fromFilter
    ) {
      payload += "&filter_by[min_price]=" + filter.min_price;
    } else if (
      typeof bankFilters.min_price !== "undefined" &&
      bankFilters.min_price !== "" &&
      !fromFilter
    ) {
      payload += "&filter_by[min_price]=" + bankFilters.min_price;
    }

    if (
      typeof filter.max_price !== "undefined" &&
      filter.max_price !== "" &&
      fromFilter
    ) {
      payload += "&filter_by[max_price]=" + filter.max_price;
    } else if (
      typeof bankFilters.max_price !== "undefined" &&
      bankFilters.max_price !== "" &&
      !fromFilter
    ) {
      payload += "&filter_by[max_price]=" + bankFilters.max_price;
    }

    const result = await getSoleTransactions(payload);
    if (result.success) {
      setSoleTransactions([]);
      if (
        typeof result.data.bank_transactions.sole_transaction_details !==
          "undefined" &&
        result.data.bank_transactions.sole_transaction_details.length > 0
      ) {
        setSoleTransactions(
          result.data.bank_transactions.sole_transaction_details
        );
        setSoleTotalTransaction(result.data.bank_transactions.total);
      } else {
        //props.setUnMatchTransaction(0);
      }
      setSoleLoader(false);
    } else {
      //props.setUnMatchTransaction(0);
      setSoleLoader(false);
    }

    await getAllBalance(filter);
  };

  const handleSoleChangePage = async (event, newPage) => {
    await setSolePage(newPage);
    await getSoleTransactionsList({ page: newPage });
  };

  const handleSoleChangeRowsPerPage = async (event) => {
    await setSolePageSize(parseInt(event.target.value));
    await getSoleTransactionsList({ pageSize: parseInt(event.target.value) });
  };

  const getBankTransactionsList = async (filter = {}) => {
    setBankLoader(true);
    let payload =
      "?page=" +
      (typeof filter.page !== "undefined" ? filter.page + 1 : bankPage + 1);
    payload +=
      "&page_size=" +
      (typeof filter.pageSize !== "undefined" ? filter.pageSize : bankPageSize);

    if (
      typeof filter.yearDates !== "undefined" &&
      typeof filter.yearDates.start_date !== "undefined" &&
      typeof filter.yearDates.end_date !== "undefined"
    ) {
      payload += "&filter_by[start_date]=" + filter.yearDates.start_date;
      payload += "&filter_by[end_date]=" + filter.yearDates.end_date;
    } else if (
      typeof yearDates !== "undefined" &&
      typeof yearDates.start_date !== "undefined" &&
      typeof yearDates.end_date !== "undefined"
    ) {
      payload += "&filter_by[start_date]=" + yearDates.start_date;
      payload += "&filter_by[end_date]=" + yearDates.end_date;
    }

    let fromFilter = false;
    if (typeof filter.fromFilter !== "undefined" && filter.fromFilter) {
      fromFilter = true;
    }

    let bankIds = [];
    if (
      typeof filter.bank_account_id !== "undefined" &&
      filter.bank_account_id.length > 0 &&
      fromFilter
    ) {
      bankIds = filter.bank_account_id;
    } else if (
      typeof bankFilters.bank_account_id !== "undefined" &&
      bankFilters.bank_account_id.length > 0 &&
      !fromFilter
    ) {
      bankIds = bankFilters.bank_account_id;
    }
    for (let i = 0; i < bankIds.length; i++) {
      payload += "&filter_by[bank_account_id][]=" + bankIds[i];
    }

    if (
      typeof filter.type !== "undefined" &&
      filter.type !== "" &&
      fromFilter
    ) {
      payload += "&filter_by[type]=" + filter.type;
    } else if (
      typeof bankFilters.type !== "undefined" &&
      bankFilters.type !== "" &&
      !fromFilter
    ) {
      payload += "&filter_by[type]=" + bankFilters.type;
    }

    if (
      typeof filter.start_date !== "undefined" &&
      filter.start_date !== "" &&
      fromFilter
    ) {
      payload += "&filter_by[start_date]=" + filter.start_date;
    } else if (
      typeof bankFilters.start_date !== "undefined" &&
      bankFilters.start_date !== "" &&
      !fromFilter
    ) {
      payload += "&filter_by[start_date]=" + bankFilters.start_date;
    }

    if (
      typeof filter.end_date !== "undefined" &&
      filter.end_date !== "" &&
      fromFilter
    ) {
      payload += "&filter_by[end_date]=" + filter.end_date;
    } else if (
      typeof bankFilters.end_date !== "undefined" &&
      bankFilters.end_date !== "" &&
      !fromFilter
    ) {
      payload += "&filter_by[end_date]=" + bankFilters.end_date;
    }

    if (
      typeof filter.min_price !== "undefined" &&
      filter.min_price !== "" &&
      fromFilter
    ) {
      payload += "&filter_by[min_price]=" + filter.min_price;
    } else if (
      typeof bankFilters.min_price !== "undefined" &&
      bankFilters.min_price !== "" &&
      !fromFilter
    ) {
      payload += "&filter_by[min_price]=" + bankFilters.min_price;
    }

    if (
      typeof filter.max_price !== "undefined" &&
      filter.max_price !== "" &&
      fromFilter
    ) {
      payload += "&filter_by[max_price]=" + filter.max_price;
    } else if (
      typeof bankFilters.max_price !== "undefined" &&
      bankFilters.max_price !== "" &&
      !fromFilter
    ) {
      payload += "&filter_by[max_price]=" + bankFilters.max_price;
    }

    const result = await bankTransaction(payload);
    if (result.success) {
      setBankTransactions([]);
      if (
        typeof result.data.bank_transactions.bank_transaction_details !==
          "undefined" &&
        result.data.bank_transactions.bank_transaction_details.length > 0
      ) {
        setBankTransactions(
          result.data.bank_transactions.bank_transaction_details
        );
        setBankTotalTransaction(result.data.bank_transactions.total);
      }
      setBankLoader(false);
    } else {
      setBankLoader(false);
    }

    await getAllBalance(filter);
  };

  const handleBankChangePage = async (event, newPage) => {
    await setBankPage(newPage);
    await getBankTransactionsList({ page: newPage });
  };

  const handleBankChangeRowsPerPage = async (event) => {
    await setBankPageSize(parseInt(event.target.value));
    await getBankTransactionsList({ pageSize: parseInt(event.target.value) });
  };

  const markPersonal = async (id) => {
    setBankLoader(true);
    const formData = new FormData();
    formData.append("id", id);
    const result = await markTransactionPersonal(formData);
    if (result.success) {
      store.store.dispatch(
        snackbarOpen({
          type: SNACKBAR_TYPE.SUCCESS,
          message: "The transaction has been marked as personal.",
        })
      );
      await getBankTransactionsList();
    } else {
      setBankLoader(false);
    }
  };

  const undo = async (id) => {
    setSoleLoader(true);
    const formData = new FormData();
    if (confirmType === "UNDOCASHINVOICE") {
      formData.append("id", id);
    } else {
      formData.append("bank_transaction_id", id);
    }

    const result =
      confirmType === "UNDOCASHINVOICE"
        ? await undoCashInvoice(formData)
        : await undoTransaction(formData);
    if (result.success) {
      await getSoleTransactionsList();
    } else {
      setSoleLoader(false);
    }
  };

  const handleConfirmationOpen = async (id, type) => {
    if (type === "PERSONAL" && !displayConfirm) {
      await markPersonal(id);
      return;
    }
    setDisplayConfirm(false);
    setConfirmType(type);
    setSelectedId(id);
    setConfirmOpen(true);
  };

  const handleConfirmationClose = async (id) => {
    if (typeof id !== "undefined" && parseInt(id) > 0) {
      if (confirmType === "UNDO" || confirmType === "UNDOCASHINVOICE") {
        await undo(id);
      } else if (confirmType === "PERSONAL") {
        await markPersonal(id);
      }
    }
    setConfirmOpen(false);
    setSelectedId(0);
  };

  const openMatch = async (obj = {}, type = "ADD") => {
    setFormType(type);
    setMatchOpen(true);
    setMatchObj(obj);
  };

  const matchDone = async () => {
    setMatchOpen(false);
    setMatchObj({});
    if (formType === "ADD" || formType === "MATCH") {
      await changeSelected(0);
    } else if (
      formType === "EDIT" ||
      formType === "EDITEXPANSE" ||
      formType === "EDITASSET"
    ) {
      await changeSelected(1);
    }
    setFormType("ADD");
  };

  return (
    <div className="mt-8 w-full flex flex-col">
      <div className="-my-2 w-full shadow bg-white overflow-hidden border-b border-gray-200 sm:rounded-lg">
        <div className="pt-2 w-full align-middle inline-block min-w-full">
          <div className="relative">
            <Tab.Group
              selectedIndex={selected}
              onChange={(index) => changeSelected(index)}
            >
              <Tab.List className="flex relative space-x-1 border-b border-grey-400 ">
                <Tab
                  className={classNames(
                    "py-6 px-6 text-base  leading-5 font-medium",
                    "focus:outline-none",
                    selected === 0
                      ? "text-brand-blue border-b border-brand-blue"
                      : "text-grey-400 hover:bg-white/[0.12] "
                  )}
                >
                  Bank Balance
                  <br />
                  <span>{formatCurrency(balances.bank_balance)}</span>
                  <br />
                  <br />
                  <span className="border inline-flex border-blue-600 items-center px-2.5 py-0.5 rounded-full text-sm font-medium bg-gray-50 text-gray-600">
                    {selected === 0 ? "Viewing Bank Feed" : "See Bank Feed"}
                  </span>
                </Tab>

                <Tab
                  className={classNames(
                    "py-6 px-6 text-base  leading-5 font-medium",
                    "focus:outline-none",
                    selected === 1
                      ? "text-brand-blue border-b border-brand-blue"
                      : "text-grey-400 hover:bg-white/[0.12] "
                  )}
                >
                  Sole Balance
                  <br />
                  <span>{formatCurrency(balances.sole_balance)}</span>
                  <br />
                  <br />
                  <span className="border inline-flex border-blue-600 items-center px-2.5 py-0.5 rounded-full text-sm font-medium bg-gray-50 text-gray-600">
                    {selected === 1 ? "Viewing History" : "See History"}
                  </span>
                </Tab>
              </Tab.List>

              <Tab.Panels>
                {/*Bank Transactions*/}
                <Tab.Panel
                  className={classNames(
                    "bg-white rounded-xl",
                    "focus:outline-none   ring-white ring-opacity-60"
                  )}
                >
                  <TransactionList
                    key="0"
                    selected={selected}
                    loader={bankLoader}
                    bankTransactions={bankTransactions}
                    soleTransactions={[]}
                    page={bankPage}
                    total={bankTotalTransaction}
                    rowsPerPage={bankPageSize}
                    confirmOpen={confirmOpen}
                    selectedId={selectedId}
                    openMatch={openMatch}
                    onPageChange={handleBankChangePage}
                    onRowsPerPageChange={handleBankChangeRowsPerPage}
                    handleConfirmationOpen={handleConfirmationOpen}
                    handleConfirmationClose={handleConfirmationClose}
                  />

                  {/* add year filter */}
                  <div className="mt-1 absolute right-40 top-8">
                    <YearFilter
                      yearList={yearList}
                      financialYear={financialYear}
                      setYear={setYear}
                    />
                  </div>

                  {/* Bank balance filter */}
                  <div className="mt-1 absolute right-7 top-9">
                    <button
                      className="bg-gray-100 relative w-full border border-gray-300 rounded-md shadow-sm pl-10 pr-10 py-3 text-left cursor-default focus:outline-none focus:ring-1 focus:ring-indigo-500 focus:border-indigo-500 sm:text-base"
                      onClick={() => {
                        setUpdateId("");
                        setOpen(!open);
                      }}
                    >
                      <span className="block truncate">Filter by</span>
                      <span className="absolute inset-y-0 right-0 flex items-center pr-2 pointer-events-none">
                        <SelectorIcon
                          className="h-5 w-5 text-gray-400"
                          aria-hidden="true"
                        />
                      </span>

                      <div className="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
                        <AdjustmentsIcon
                          className="h-5 w-5 z-10 text-gray-500"
                          aria-hidden="true"
                        />
                      </div>
                    </button>
                  </div>
                </Tab.Panel>

                {/*Sole Transactions*/}
                <Tab.Panel
                  className={classNames(
                    "bg-white rounded-xl",
                    "focus:outline-none   ring-white ring-opacity-60"
                  )}
                >
                  <TransactionList
                    key="1"
                    selected={selected}
                    loader={soleLoader}
                    bankTransactions={[]}
                    soleTransactions={soleTransactions}
                    page={solePage}
                    total={soleTotalTransaction}
                    rowsPerPage={solePageSize}
                    confirmOpen={confirmOpen}
                    selectedId={selectedId}
                    openMatch={openMatch}
                    onPageChange={handleSoleChangePage}
                    onRowsPerPageChange={handleSoleChangeRowsPerPage}
                    handleConfirmationOpen={handleConfirmationOpen}
                    handleConfirmationClose={handleConfirmationClose}
                  />

                  {/* add year filter */}
                  <div className="mt-1 absolute right-40 top-8">
                    <YearFilter
                      yearList={yearList}
                      financialYear={financialYear}
                      setYear={setYear}
                    />
                  </div>

                  {/* Sole balance filter */}
                  <div className="mt-1 absolute right-7 top-9">
                    <button
                      className="bg-gray-100 relative w-full border border-gray-300 rounded-md shadow-sm pl-10 pr-10 py-3 text-left cursor-default focus:outline-none focus:ring-1 focus:ring-indigo-500 focus:border-indigo-500 sm:text-base"
                      onClick={() => {
                        setOpenSole(!openSole);
                      }}
                    >
                      <span className="block truncate">Filter by</span>
                      <span className="absolute inset-y-0 right-0 flex items-center pr-2 pointer-events-none">
                        <SelectorIcon
                          className="h-5 w-5 text-gray-400"
                          aria-hidden="true"
                        />
                      </span>

                      <div className="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
                        <AdjustmentsIcon
                          className="h-5 w-5 z-10 text-gray-500"
                          aria-hidden="true"
                        />
                      </div>
                    </button>
                  </div>
                </Tab.Panel>
              </Tab.Panels>
            </Tab.Group>
          </div>
        </div>
      </div>

      {formType === "EDITEXPANSE" && (
        <AddExpenses
          isOpen={matchOpen}
          accountCategories={categories.expanseOnly}
          setIsListUpdate={matchDone}
          setIsOpen={setMatchOpen}
          updateId={matchObj.expense_id || ""}
        />
      )}

      {formType === "EDITASSET" && (
        <AddAssets
          isOpen={matchOpen}
          accountCategories={categories.assetOnly}
          setIsListUpdate={matchDone}
          setIsOpen={setMatchOpen}
          updateId={matchObj.asset_id || ""}
        />
      )}

      {formType !== "EDITEXPANSE" && formType !== "EDITASSET" && (
        <MatchTransaction
          formType={formType}
          categories={categories}
          matchOpen={matchOpen}
          matchObj={matchObj}
          setFormType={setFormType}
          setMatchOpen={setMatchOpen}
          matchDone={matchDone}
        />
      )}

      <BankBalanceFilter
        open={open}
        accounts={accounts}
        bankFilters={bankFilters}
        setOpen={setOpen}
        setFilter={setFilter}
      />

      <SoleBalanceFilter
        openSole={openSole}
        soleFilters={soleFilters}
        setOpenSole={setOpenSole}
        setFilter={setFilter}
      />

      <Popup selected={selected} soleBalance={balances.sole_balance} />
    </div>
  );
}
