import { Fragment, useState, useEffect } from 'react'
import { Transition, Listbox } from '@headlessui/react';
import { CheckIcon, SelectorIcon } from '@heroicons/react/solid';
import config from '../../../config/config';

function classNames(...classes) {
    return classes.filter(Boolean).join(' ')
}

export default function CategoryDropdown(props) {
    const { category, setCategory, categories, type } = props;
    const [hasExpense, setHasExpense] = useState(false);
    const [hasAsset, setHasAsset] = useState(false);

    useEffect(() => {
        setHasExpense(false);
        setHasAsset(false);

        for (let i = 0; i < categories.length; i++) {
            if (categories[i].account_category_id == config.expanseCategoryId) {
                setHasExpense(true);
            }
            if (categories[i].account_category_id == config.assetCategoryId) {
                setHasAsset(true);
            }
        }
    }, [categories]);

    if (type === 'INCOME') {
        return (
            <Listbox value={category} onChange={setCategory}>
                {({ open }) => (
                    <>
                        <div className="mt-1 relative">
                            <Listbox.Button
                                className="py-3 px-4 block relative text-left bg-white cursor-default  w-full  text-md tracking-wider font-medium text-gray-500 border-2 border-gray-200  focus:ring-indigo-400 focus:border-indigo-400  rounded-md">
                                <span className="block truncate">{category.name}</span>
                                <span
                                    className="absolute inset-y-0 right-0 flex items-center pr-2 pointer-events-none">
                                    <SelectorIcon className="h-5 w-5 text-gray-400"
                                        aria-hidden="true" />
                                </span>
                            </Listbox.Button>

                            <Transition
                                show={open}
                                as={Fragment}
                                leave="transition ease-in duration-100"
                                leaveFrom="opacity-100"
                                leaveTo="opacity-0"
                            >
                                <Listbox.Options
                                    static
                                    className="absolute z-10 mt-1 w-full bg-white shadow-lg max-h-60 rounded-md py-1 text-base ring-1 ring-black ring-opacity-5 overflow-auto focus:outline-none sm:text-sm"
                                >
                                    <Listbox.Button>
                                        <div className='font-bold py-3 px-2  text-left w-full text-md'>Income</div>
                                    </Listbox.Button>
                                    {categories.map((cat) => (
                                        <Listbox.Option
                                            key={cat.id}
                                            className={({ active }) =>
                                                classNames(
                                                    active ? 'text-white bg-brand-blue' : 'text-gray-900',
                                                    'cursor-default select-none relative py-2 pl-3 pr-9'
                                                )
                                            }
                                            value={cat}
                                        >
                                            {({ category, active }) => (
                                                <>
                                                    <span
                                                        className={classNames(category ? 'font-semibold' : 'font-normal', 'block truncate')}>
                                                        {cat.name}
                                                    </span>

                                                    {category ? (
                                                        <span
                                                            className={classNames(
                                                                active ? 'text-white' : 'text-brand-blue',
                                                                'absolute inset-y-0 right-0 flex items-center pr-4'
                                                            )}
                                                        >
                                                            <CheckIcon className="h-5 w-5" aria-hidden="true" />
                                                        </span>
                                                    ) : null}
                                                </>
                                            )}
                                        </Listbox.Option>
                                    ))}
                                </Listbox.Options>
                            </Transition>
                        </div>
                    </>
                )}
            </Listbox>
        )
    }

    return (
        <Listbox value={category} onChange={setCategory}>
            {({ open }) => (
                <>
                    <div className="mt-1 relative">
                        <Listbox.Button
                            className="py-3 px-4 block relative text-left bg-white cursor-default  w-full  text-md tracking-wider font-medium text-gray-500 border-2 border-gray-200  focus:ring-indigo-400 focus:border-indigo-400  rounded-md">
                            <span className="block truncate">{category.name}</span>
                            <span
                                className="absolute inset-y-0 right-0 flex items-center pr-2 pointer-events-none">
                                <SelectorIcon className="h-5 w-5 text-gray-400"
                                    aria-hidden="true" />
                            </span>
                        </Listbox.Button>

                        <Transition
                            show={open}
                            as={Fragment}
                            leave="transition ease-in duration-100"
                            leaveFrom="opacity-100"
                            leaveTo="opacity-0"
                        >
                            <Listbox.Options
                                static
                                className="absolute z-10 mt-1 w-full bg-white shadow-lg max-h-60 rounded-md py-1 text-base ring-1 ring-black ring-opacity-5 overflow-auto focus:outline-none sm:text-sm"
                            >
                                {hasExpense &&
                                    <Listbox.Button>
                                        <div className='font-bold py-3 px-2  text-left w-full text-md'>Expenses</div>
                                    </Listbox.Button>
                                }
                                {categories.map((cat) => (
                                    <>
                                        {(cat.account_category_id == 1) &&
                                            <Listbox.Option
                                                key={cat.id}
                                                className={({ active }) =>
                                                    classNames(
                                                        active ? 'text-white bg-brand-blue' : 'text-gray-900',
                                                        'cursor-default select-none relative py-2 pl-3 pr-9'
                                                    )
                                                }
                                                value={cat}
                                            >
                                                {({ category, active }) => (
                                                    <>
                                                        <span
                                                            className={classNames(category ? 'font-semibold' : 'font-normal', 'block truncate')}>
                                                            {cat.name}
                                                        </span>

                                                        {category ? (
                                                            <span
                                                                className={classNames(
                                                                    active ? 'text-white' : 'text-brand-blue',
                                                                    'absolute inset-y-0 right-0 flex items-center pr-4'
                                                                )}
                                                            >
                                                                <CheckIcon className="h-5 w-5" aria-hidden="true" />
                                                            </span>
                                                        ) : null}
                                                    </>
                                                )}
                                            </Listbox.Option>
                                        }
                                    </>
                                ))}

                                {hasAsset &&
                                    <Listbox.Button>
                                        <div className='font-bold py-3 px-2  text-left w-full text-md'>Assets</div>
                                    </Listbox.Button>
                                }
                                {categories.map((cat) => (
                                    <>
                                        {(cat.account_category_id == 6) &&
                                            <Listbox.Option
                                                key={cat.id}
                                                className={({ active }) =>
                                                    classNames(
                                                        active ? 'text-white bg-brand-blue' : 'text-gray-900',
                                                        'cursor-default select-none relative py-2 pl-3 pr-9'
                                                    )
                                                }
                                                value={cat}
                                            >
                                                {({ category, active }) => (
                                                    <>
                                                        <span
                                                            className={classNames(category ? 'font-semibold' : 'font-normal', 'block truncate')}>
                                                            {cat.name}
                                                        </span>

                                                        {category ? (
                                                            <span
                                                                className={classNames(
                                                                    active ? 'text-white' : 'text-brand-blue',
                                                                    'absolute inset-y-0 right-0 flex items-center pr-4'
                                                                )}
                                                            >
                                                                <CheckIcon className="h-5 w-5" aria-hidden="true" />
                                                            </span>
                                                        ) : null}
                                                    </>
                                                )}
                                            </Listbox.Option>
                                        }
                                    </>
                                ))}
                            </Listbox.Options>
                        </Transition>
                    </div>
                </>
            )}
        </Listbox>
    )
}