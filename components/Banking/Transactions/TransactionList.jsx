import Skeleton from 'react-loading-skeleton';
import Pagination from '../../Pagination';
import ConfirmationDialog from '../../ConfirmationDialog';
import Transaction from '../../../components/Banking/Transactions/Transaction';

export default function TransactionList(props) {
    const {
        selected,
        loader,
        bankTransactions,
        soleTransactions,
        page,
        total,
        rowsPerPage,
        onPageChange,
        onRowsPerPageChange,

        openMatch,
        confirmOpen,
        selectedId,
        handleConfirmationOpen,
        handleConfirmationClose
    } = props;

    //sole transactions
    if (selected === 1) {
        return (
            <>
                <ConfirmationDialog
                    id="ringtone-menu"
                    keepMounted
                    title="Undo Match?"
                    message="This will undo your previously matched transaction."
                    processing="Undoing..."
                    open={confirmOpen}
                    onClose={handleConfirmationClose}
                    value={selectedId}
                />

                <div className="overflow-y-auto overflow-x-auto">
                    <table
                        className="min-w-full  overflow-x-hidden divide-y hover-row divide-gray-200">
                        <thead className="">
                            <tr>
                                <th scope="col"
                                    className="px-6 py-3 w-60 text-left text-sm font-medium text-gray-500 uppercase tracking-wider">
                                    Date
                                </th>
                                <th scope="col"
                                    className="px-6 py-3  2xl:w-auto text-left text-sm font-medium text-gray-500 uppercase tracking-wider">
                                    Category
                                </th>
                                <th scope="col"
                                    className="px-6 py-3 w-96 2xl:w-auto text-left text-sm font-medium text-gray-500 uppercase tracking-wider">
                                    Description
                                </th>
                                <th scope="col"
                                    className="px-6 py-3 w-5 text-left text-sm font-medium text-gray-500 uppercase tracking-wider">
                                    <span className='sr-only'>Type</span>
                                </th>
                                <th scope="col"
                                    className="px-6 py-3 text-left text-sm font-medium text-gray-500 uppercase tracking-wider">
                                    Amount
                                </th>
                                <th scope="col"
                                    className="px-6 py-3 w-40 text-center text-sm font-medium text-gray-500 uppercase tracking-wider">
                                    Actions
                                </th>
                            </tr>
                        </thead>

                        <tbody className="bg-white divide-y divide-gray-200">
                            {!loader ?
                                soleTransactions.length > 0 ?
                                    soleTransactions.map((transaction, key) => (
                                        <Transaction
                                            key={key}
                                            transaction={transaction.data}
                                            type={transaction.type}
                                            openMatch={openMatch}
                                            handleConfirmationOpen={handleConfirmationOpen}
                                        />
                                    ))
                                    : <tr><td colSpan="5" align="center"><span>
                                        <br></br>
                                        You haven't matched any transactions, get matching!
                                        <br></br>
                                        </span></td></tr>
                                :
                                Array.from(Array(5), (e, i) => {
                                    return <tr key={i}>
                                        <td className="px-6 py-4 whitespace-nowrap"><Skeleton /></td>
                                        <td className="px-6 py-4 whitespace-nowrap"><Skeleton /></td>
                                        <td className="px-7 py-4 whitespace-nowrap"><Skeleton /></td>
                                        <td className="px-7 py-4 whitespace-nowrap"><Skeleton /></td>
                                        <td className="px-7 py-4 whitespace-nowrap"><Skeleton /></td>
                                        <td className="px-7 py-4 whitespace-nowrap text-right text-xsm font-medium"><Skeleton /></td>
                                    </tr>
                                })
                            }
                        </tbody>
                    </table>
                </div>

                {!loader ?
                    <Pagination
                        total={total}
                        page={page}
                        onPageChange={onPageChange}
                        rowsPerPage={rowsPerPage}
                        onRowsPerPageChange={onRowsPerPageChange}
                    />
                    : null}
            </>
        )
    }

    //bank transactions
    return (
        <>
            <ConfirmationDialog
                id="ringtone-menu"
                keepMounted
                title="Mark Personal?"
                message="This will mark your transaction as personal."
                processing="Processing..."
                open={confirmOpen}
                onClose={handleConfirmationClose}
                value={selectedId}
            />

            <div className="overflow-y-auto w-full overflow-x-auto">
                <table
                    className="min-w-full  overflow-x-hidden divide-y hover-row divide-gray-200">
                    <thead className="">
                        <tr>
                            <th scope="col"
                                className="px-6 py-3 w-80 text-left text-sm font-medium text-gray-500 uppercase tracking-wider">
                                Date
                            </th>
                            <th scope="col"
                                className="px-6 py-3 min-w-96 2xl:w-auto text-left text-sm font-medium text-gray-500 uppercase tracking-wider">
                                Description
                            </th>
                            <th scope="col"
                                className="px-6 py-3 text-left text-sm font-medium text-gray-500 uppercase tracking-wider">
                                Type
                            </th>
                            <th scope="col"
                                className="px-6 py-3 text-left text-sm font-medium text-gray-500 uppercase tracking-wider">
                                Amount
                            </th>
                            <th scope="col"
                                className="px-6 py-3 w-40 text-center text-sm font-medium text-gray-500 uppercase tracking-wider">
                                Actions
                            </th>
                        </tr>
                    </thead>

                    <tbody className="bg-white divide-y divide-gray-200">
                        {!loader ?
                            bankTransactions.length > 0 ?
                                bankTransactions.map((transaction, key) => (
                                    <Transaction
                                        key={key}
                                        transaction={transaction}
                                        type="Transaction"
                                        openMatch={openMatch}
                                        handleConfirmationOpen={handleConfirmationOpen}
                                    />
                                ))
                                : <tr><td colSpan={5} className="p-5" align="center"><span>No Transaction Available.</span></td></tr>
                            :
                            Array.from(Array(5), (e, i) => {
                                return <tr key={i}>
                                    <td className="px-6 py-4 whitespace-nowrap"><Skeleton /></td>
                                    <td className="px-6 py-4 whitespace-nowrap"><Skeleton /></td>
                                    <td className="px-7 py-4 whitespace-nowrap"><Skeleton /></td>
                                    <td className="px-7 py-4 whitespace-nowrap"><Skeleton /></td>
                                    <td className="px-7 py-4 whitespace-nowrap text-right text-xsm font-medium"><Skeleton /></td>
                                </tr>
                            })
                        }
                    </tbody>
                </table>
            </div>

            {!loader
                ?
                <Pagination
                    total={total}
                    page={page}
                    onPageChange={onPageChange}
                    rowsPerPage={rowsPerPage}
                    onRowsPerPageChange={onRowsPerPageChange}
                />
                :
                null
            }
        </>
    )
}
