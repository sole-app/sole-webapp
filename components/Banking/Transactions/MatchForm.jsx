import {useState, useEffect} from 'react'
import {ErrorMessage, Form, Formik} from 'formik';
import TextField from '@mui/material/TextField';
import Stack from '@mui/material/Stack';  
import CircularProgress from '@material-ui/core/CircularProgress';
import Autocomplete, { createFilterOptions } from '@mui/material/Autocomplete';
import MatchExpenseSchema from "../../../schemas/MatchExpense.schema";
import MatchIncomeSchema from "../../../schemas/MatchIncome.schema";
import { IconContext } from "react-icons";
import { FiPlusCircle } from "react-icons/fi";
import CategoryDropdown from "../../../components/Banking/Transactions/CategoryDropdown";

const initialValuesExpense = {
    client_id: '',
    client_name: '',
    name: '',
    category: null,
    description: '',
    client: null,
};

const initialValuesIncome = {
    client_id: '',
    client_name: '',
    category: null,
    description: '',
    client: null,
};

const initialValuesEdit = {
    client_name: '',
    category: null,
    description: '',
    client: null,
};

export default function MatchForm(props) {
    const { formType, matchObj, clients, categories, matchManually, possibleMatchObj, updatePossibleMatch,
        setCurrentClient, addClient, clientLoader
    } = props;
    const [category, setCategory] = useState(categories[0]);
    const [updated, setUpdated] = useState(false);
    const [allCategories, setAllCategories] = useState(categories);
    const [catType, setCatType] = useState('ALL'); //ALL,EXPENSE,ASSET
    const filter = createFilterOptions();

    useEffect(() => {
        setAllCategories(categories);
        setUpdated(!updated);
        initialValuesExpense.client_id = '';
        initialValuesExpense.client_name = '';
        initialValuesExpense.name = '';
        initialValuesExpense.category = null;
        initialValuesExpense.description = '';
        initialValuesIncome.client_id = '';
        initialValuesIncome.client_name = '';
        initialValuesIncome.category = null;
        initialValuesIncome.description = '';
        initialValuesEdit.client_name = '';
        initialValuesEdit.category = null;
        initialValuesEdit.description = '';
        if (formType === 'EDIT') {
            let accountCatId = 0;
            for (let i=0;i<categories.length;i++) {
                if (typeof matchObj.account_subcategory[0] !== 'undefined' && typeof matchObj.account_subcategory[0].account_subcategory_id !== 'undefined' && matchObj.account_subcategory[0].account_subcategory_id === categories[i].account_subcategory_id) {
                    setCategory(categories[i]);
                    accountCatId = matchObj.account_subcategory[0].account_category_id;
                    setCatType(categories[i].parent_category);
                    break;
                }
            }

            if (accountCatId > 0) {
                let cats = [];
                for (let i=0;i<categories.length;i++) {
                    if (accountCatId === categories[i].account_category_id) {
                        cats.push(categories[i]);
                    }
                }
                setAllCategories(cats);
            }

            initialValuesEdit.client_name = matchObj.client.client_name || '';
            if (matchObj.type === 'credit') {
                initialValuesEdit.description = matchObj.user_description;
            } else {
                initialValuesEdit.description = matchObj.user_description;
            }
        } else if (formType === 'MATCH') {
            if (typeof possibleMatchObj.account_subcategory_id !== 'undefined' && possibleMatchObj.account_subcategory_id !== '') {
                for (let i=0;i<categories.length;i++) {
                    if (possibleMatchObj.account_subcategory_id === categories[i].account_subcategory_id) {
                        setCategory(categories[i]);
                        break;
                    }
                }
            }

            if (matchObj.type === 'debit') {
                initialValuesExpense.client_id = possibleMatchObj.client_id;
                initialValuesExpense.client_name = (typeof possibleMatchObj.client != 'undefined' && typeof possibleMatchObj.client.name != 'undefined') ? possibleMatchObj.client.name : '';
                initialValuesExpense.name = possibleMatchObj.name;
            } else {
                initialValuesIncome.client_id = possibleMatchObj.client_id;
                initialValuesIncome.client_name = (typeof possibleMatchObj.client != 'undefined' && typeof possibleMatchObj.client.name != 'undefined') ? possibleMatchObj.client.name : '';
                initialValuesIncome.description = possibleMatchObj.description;
            }
        }
    }, [formType, possibleMatchObj, updatePossibleMatch]);

    if (typeof matchObj.type === 'undefined') {
        return null;
    }

    const handleFormSubmit = async (values, setSubmitting) => {
        let formData = {};
        if (formType === 'EDIT') {
            let id = ''
            if (typeof matchObj.client_id !== 'undefined') {
                id = matchObj.client_id
            } else if (typeof matchObj.client !== 'undefined' && typeof matchObj.client.client_id !== 'undefined') {
                id = matchObj.client.client_id
            } 
            formData = {
                bank_transaction_id: matchObj.bank_transacton_id,
                account_subcategory_id: category.account_subcategory_id,
                account_category_id: category.account_category_id,
                update_match: 1,
                user_description: values.description,
                client_id: id,
            };
        } else {
            formData = {
                bank_transaction_id: matchObj.bank_transacton_id,
                account_subcategory_id: category.account_subcategory_id,
                account_category_id: category.account_category_id,
                user_description: values.description,
                client_id: values.client_id,
                client_name: values.client_name,
            };

            if (matchObj.type === 'debit') {
                formData.name = values.name;
            }

            if (formType === 'MATCH') {
                if (matchObj.type === 'debit') {
                    if (possibleMatchObj.type === 'expense') {
                        formData.expense_id = possibleMatchObj.expense_id;
                    } else {
                        formData.asset_id = possibleMatchObj.asset_id;
                    }
                } else {
                    formData.invoice_id = possibleMatchObj.id;
                }
            }
        }

        await matchManually(formData);
        setSubmitting(false);
    }

    if (formType === 'ADD' || formType === 'MATCH') {
        if (matchObj.type === 'debit') {
            //expense
            return (
                <Formik
                    initialValues={initialValuesExpense}
                    validationSchema={MatchExpenseSchema}
                    onSubmit={(values, {setSubmitting}) => {
                        handleFormSubmit(values, setSubmitting);
                    }}>
                    {({values, errors, handleChange, handleSubmit, handleBlur, touched, isSubmitting, setFieldValue}) => (
                        <Form onSubmit={handleSubmit} className="h-full divide-y divide-gray-200 flex flex-col bg-white shadow-xl">
                            <div className="h-full divide-y divide-gray-200 flex flex-col bg-white">
                                <div className="flex-1 h-0 pb-8 overflow-y-scroll">
                                    <div className="flex-1 flex flex-col justify-between">
                                        <div className="px-4 pt-5 divide-y divide-gray-200 sm:px-6">
                                            <div
                                                className="space-y-6  mb-4 pt-6 pb-7 items-baseline grid grid-cols-1 sm:pt-0 md:grid-cols-2 sm:gap-x-8"
                                            >
                                                <input type='hidden' value={updated} />

                                                <div className='sm:col-span-2'>
                                                    <label htmlFor="company"
                                                           className="block text-md tracking-wider font-medium text-gray-500">
                                                        Choose Account*
                                                    </label>
                                                    <div className="mt-1">
                                                        <CategoryDropdown
                                                            type="EXPENSE"
                                                            categories={allCategories}
                                                            category={category}
                                                            setCategory={setCategory}
                                                        />
                                                    </div>
                                                </div>

                                                <div>
                                                    <label htmlFor="company"
                                                           className="block text-md tracking-wider font-medium text-gray-500">
                                                        Name of Supplier*
                                                    {clientLoader &&
                                                    <div style={{marginRight:'13px', float: 'right'}}>
                                                        <CircularProgress size={15} />
                                                    </div>
                                                    }
                                                    </label>

                                                    <div className="mt-1">
                                                        <Stack spacing={2}>
                                                            <Autocomplete
                                                                disablePortal
                                                                id="client_name"
                                                                name="client_name"
                                                                value={values.client_name}
                                                                onBlur={handleBlur}
                                                                freeSolo    
                                                                className="p-0"
                                                                options={clients}
                                                                getOptionLabel={option => {
                                                                    if (option?.value === "Add new Supplier"){
                                                                        return <div className='py-2 flex gap-1'>Add New Supplier&nbsp;&nbsp; 
                                                                        <IconContext.Provider value={{ color: "#4d4dff"  , className: "global-class-name h-6 w-6" }}>
                                                                        <div>
                                                                            <FiPlusCircle />
                                                                        </div>
                                                                        </IconContext.Provider></div>                                                                                        
                                                                    } else{
                                                                        if (typeof option.name !== 'undefined') {
                                                                            return option.name
                                                                        } else if (typeof option !== 'undefined') {
                                                                            return option
                                                                        } else {
                                                                            return ''
                                                                        }
                                                                    }
                                                                }}
                                                                onChange={async (e, value) => {
                                                                    if (value?.value == 'Add new Supplier'){
                                                                        setFieldValue("client_name", value.label);
                                                                        setFieldValue("client", {
                                                                            label: value.label,
                                                                        });
                                                                        setCurrentClient({
                                                                            label: value.label,
                                                                        })
                                                                        let client_details = await addClient(value.label);
                                                                        if (!!client_details?.client_id){
                                                                            setFieldValue("client_id", client_details.client_id);
                                                                            setFieldValue("client_name", client_details.name);
                                                                            setFieldValue("client", {
                                                                                label: client_details.name,
                                                                                id: client_details.client_id,
                                                                            });
                                                                            setCurrentClient({
                                                                                label: client_details.name,
                                                                                id: client_details.client_id,
                                                                            })
                                                                        }
                                                                    } else{
                                                                        if (typeof value !== 'undefined' && value !== null && Object.keys(value).length > 0) {
                                                                            if (typeof(value) === "object"){
                                                                                setFieldValue("client_id", value.client_id);
                                                                                let name = (typeof value.label !== 'undefined') ? value.label : ''
                                                                                name = (typeof value.name !== 'undefined') ? value.name : ''
                                                                                setFieldValue("client_name", name);
                                                                            } else{
                                                                                setFieldValue("client_id", '');
                                                                                setFieldValue("client_name", '');
                                                                                setFieldValue("client", {
                                                                                    label: '',
                                                                                });
                                                                            }
                                                                        } else {
                                                                            setFieldValue("client_id", '');
                                                                            setFieldValue("client_name", '');
                                                                        }
                                                                    }
                                                                }}
                                                                renderInput={(params) => (
                                                                    <TextField {...params} variant="outlined" className="p-0 border-2 border-gray-200 form-control" />
                                                                )}
                                                                filterOptions={(options, params) => {
                                                                    const filtered = filter(options, params);
                                                                    // Suggest the creation of a new value
                                                                    if (params.inputValue !== '') {
                                                                      filtered.push({
                                                                        label: params.inputValue,
                                                                        value: `Add new Supplier`,                                                                                      
                                                                      });
                                                                    }
                                                                    return filtered;
                                                                }}
                                                            />
                                                        </Stack>
                                                    </div>
                                                    <ErrorMessage name="client_name" component="span" className="error_text"/>
                                                </div>

                                                <div>
                                                    <label htmlFor="company"
                                                           className="block text-md tracking-wider font-medium text-gray-500">
                                                        Name of Expense/Asset*
                                                    </label>

                                                    <div className="mt-1">
                                                        <input
                                                            type="text"
                                                            name="name"
                                                            id="name"
                                                            value={values.name}
                                                            onChange={handleChange}
                                                            onBlur={handleBlur}
                                                            className="placeholder-gray-400 py-3 px-4 block w-full border-2  focus:ring-indigo-400 focus:border-indigo-400 border-gray-200 rounded-md "
                                                        />
                                                    </div>
                                                    <ErrorMessage name="name" component="span" className="error_text"/>
                                                </div>

                                                <div className='sm:col-span-2'>
                                                    <label htmlFor="company"
                                                           className="block text-md tracking-wider font-medium text-gray-500">
                                                        Description
                                                    </label>

                                                    <div className="mt-1">
                                                <textarea
                                                    id="description"
                                                    name="description"
                                                    rows={4}
                                                    value={values.description}
                                                    onChange={handleChange}
                                                    onBlur={handleBlur}
                                                    className="max-w-fullpy-3 px-4 block w-full border-2  focus:ring-indigo-400 focus:border-indigo-400 border-gray-200 rounded-md "
                                                />
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="flex-shrink-0  px-4 py-4 flex justify-between">
                                <p className=" w-96 font-normal text-sm text-gray-500">
                                    This match is GST inclusive. If your transaction is GST exclusive please utilise the
                                    "Detailed Match" functionality.
                                </p>

                                <div>
                                    <button
                                        type="submit"
                                        className="ml-4 inline-flex justify-center py-2 px-4 tracking-wider border border-transparent uppercase shadow-sm  font-medium text-sm rounded-full text-white bg-green-600 hover:bg-green-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-500"
                                        disabled={isSubmitting}
                                    >
                                        {isSubmitting ? "Please wait..." : "Save"}
                                    </button>
                                </div>
                            </div>
                        </Form>
                    )}
                </Formik>
            )
        } else if (matchObj.type === 'credit') {
            //income
            return (
                <Formik
                    initialValues={initialValuesIncome}
                    validationSchema={MatchIncomeSchema}
                    onSubmit={(values, {setSubmitting}) => {
                        handleFormSubmit(values, setSubmitting);
                    }}>
                    {({values, errors, handleChange, handleSubmit, handleBlur, touched, isSubmitting, setFieldValue}) => (
                        <Form onSubmit={handleSubmit} className="h-full divide-y divide-gray-200 flex flex-col bg-white shadow-xl">
                            <div className="h-full divide-y divide-gray-200 flex flex-col bg-white">
                                <div className="flex-1 h-0 pb-8 overflow-y-scroll">
                                    <div className="flex-1 flex flex-col justify-between">
                                        <div className="px-4 divide-y divide-gray-200 sm:px-6">
                                            <div
                                                className="space-y-6  mb-4 pt-6 pb-7 items-baseline grid grid-cols-1 sm:pt-0 md:grid-cols-2 sm:gap-x-8"
                                            >
                                                <input type='hidden' value={updated} />
                                                
                                                <div>
                                                    <label htmlFor="company"
                                                           className="block text-md tracking-wider font-medium text-gray-500">
                                                        Choose Account*
                                                    </label>
                                                    <div className="mt-1">
                                                        <CategoryDropdown
                                                            type="INCOME"
                                                            categories={allCategories}
                                                            category={category}
                                                            setCategory={setCategory}
                                                        />
                                                    </div>
                                                </div>

                                                <div>
                                                    <label htmlFor="company"
                                                           className="block text-md tracking-wider font-medium text-gray-500">
                                                        Name of Client*
                                                    {clientLoader &&
                                                    <div style={{marginRight:'13px', float: 'right'}}>
                                                        <CircularProgress size={15} />
                                                    </div>
                                                    }
                                                    </label>

                                                    <div className="mt-1">
                                                        <Stack spacing={2} >
                                                            <Autocomplete
                                                                disablePortal
                                                                id="client_name"
                                                                name="client_name"
                                                                value={values.client_name}
                                                                onBlur={handleBlur}
                                                                freeSolo
                                                                options={clients}
                                                                className="p-0"
                                                                getOptionLabel={option => {
                                                                    if (option?.value === "Add new Client"){
                                                                        return <div className='py-2 flex gap-1'>Add New Client&nbsp;&nbsp; 
                                                                        <IconContext.Provider value={{ color: "#4d4dff"  , className: "global-class-name h-6 w-6" }}>
                                                                        <div>
                                                                            <FiPlusCircle />
                                                                        </div>
                                                                        </IconContext.Provider></div>                                                                                        
                                                                    } else{
                                                                        if (typeof option.name !== 'undefined') {
                                                                            return option.name
                                                                        } else if (typeof option !== 'undefined') {
                                                                            return option
                                                                        } else {
                                                                            return ''
                                                                        }
                                                                    }
                                                                }}
                                                                onChange={async (e, value) => {
                                                                    if (value?.value == 'Add new Client'){
                                                                        setFieldValue("client_name", value.label);
                                                                        setFieldValue("client", {
                                                                            label: value.label,
                                                                        });
                                                                        setCurrentClient({
                                                                            label: value.label,
                                                                        })
                                                                        let client_details = await addClient(value.label);
                                                                        if (!!client_details?.client_id){
                                                                            setFieldValue("client_id", client_details.client_id);
                                                                            setFieldValue("client_name", client_details.name);
                                                                            setFieldValue("client", {
                                                                                label: client_details.name,
                                                                                id: client_details.client_id,
                                                                            });
                                                                            setCurrentClient({
                                                                                label: client_details.name,
                                                                                id: client_details.client_id,
                                                                            })
                                                                        }
                                                                    } else{
                                                                        if (typeof value !== 'undefined' && value !== null && Object.keys(value).length > 0) {
                                                                            if (typeof(value) === "object"){
                                                                                setFieldValue("client_id", value.client_id);
                                                                                let name = (typeof value.label !== 'undefined') ? value.label : ''
                                                                                name = (typeof value.name !== 'undefined') ? value.name : ''
                                                                                setFieldValue("client_name", name);
                                                                            } else{
                                                                                setFieldValue("client_id", '');
                                                                                setFieldValue("client_name", '');
                                                                                setFieldValue("client", {
                                                                                    label: '',
                                                                                });
                                                                            }
                                                                        } else {
                                                                            setFieldValue("client_id", '');
                                                                            setFieldValue("client_name", '');
                                                                        }
                                                                    }
                                                                }}
                                                                renderInput={(params) => (
                                                                    <TextField {...params} variant="outlined" className="p-0 border-2 border-gray-200 form-control" />
                                                                )}
                                                                filterOptions={(options, params) => {
                                                                    const filtered = filter(options, params);
                                                                    // Suggest the creation of a new value
                                                                    if (params.inputValue !== '') {
                                                                      filtered.push({
                                                                        label: params.inputValue,
                                                                        value: `Add new Client`,                                                                                      
                                                                      });
                                                                    }
                                                                    return filtered;
                                                                }}
                                                            />
                                                        </Stack>
                                                    </div>
                                                    <ErrorMessage name="client_name" component="span" className="error_text"/>
                                                </div>

                                                <div className='sm:col-span-2'>
                                                    <label htmlFor="company"
                                                           className="block text-md tracking-wider font-medium text-gray-500">
                                                        Description
                                                    </label>

                                                    <div className="mt-1">
                                                <textarea
                                                    id="description"
                                                    name="description"
                                                    rows={4}
                                                    value={values.description}
                                                    onChange={handleChange}
                                                    onBlur={handleBlur}
                                                    className="max-w-fullpy-3 px-4 block w-full border-2  focus:ring-indigo-400 focus:border-indigo-400 border-gray-200 rounded-md "
                                                />
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div className="flex-shrink-0  px-4 py-4 flex justify-between">
                                <p className=" w-96 font-normal text-sm text-gray-500">
                                    This match is GST inclusive. If your transaction is GST exclusive please utilise the
                                    "Detailed Match" functionality.
                                </p>

                                <div>
                                    <button
                                        type="submit"
                                        className="ml-4 inline-flex justify-center py-2 px-4 tracking-wider border border-transparent uppercase shadow-sm  font-medium text-sm rounded-full text-white bg-green-600 hover:bg-green-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-500"
                                        disabled={isSubmitting}
                                    >
                                        {isSubmitting ? "Please wait..." : "Save"}
                                    </button>
                                </div>
                            </div>
                        </Form>
                    )}
                </Formik>
            )
        }
    } else {
        if (matchObj.type === 'debit') {
            //expense
            return (
                <Formik
                    initialValues={initialValuesEdit}
                    onSubmit={(values, {setSubmitting}) => {
                        handleFormSubmit(values, setSubmitting);
                    }}>
                    {({values, errors, handleChange, handleSubmit, handleBlur, touched, isSubmitting, setFieldValue}) => (
                        <Form onSubmit={handleSubmit} className="h-full divide-y divide-gray-200 flex flex-col bg-white shadow-xl">
                            <div className="h-full divide-y divide-gray-200 flex flex-col bg-white">
                                <div className="flex-1 h-0 pb-8 overflow-y-scroll">
                                    <div className="flex-1 flex flex-col justify-between">
                                        <div className="px-4 divide-y divide-gray-200 sm:px-6">
                                            <div
                                                className="space-y-6  mb-4 pt-6 pb-7 items-baseline grid grid-cols-1 sm:pt-0 md:grid-cols-2 sm:gap-x-8"
                                            >
                                                <input type='hidden' value={updated} />
                                                
                                                <div>
                                                    <label htmlFor="company"
                                                           className="block text-md tracking-wider font-medium text-gray-500">
                                                        Name of Supplier*
                                                    </label>

                                                    <div className="mt-1">
                                                        <input
                                                            type="text"
                                                            name="client_name"
                                                            id="client_name"
                                                            value={values.client_name}
                                                            readOnly={true}
                                                            disabled={true}
                                                            className="placeholder-gray-400 py-3 px-4 block w-full border-2  focus:ring-indigo-400 focus:border-indigo-400 border-gray-200 rounded-md "
                                                        />
                                                    </div>
                                                </div>

                                                <div className='sm:col-span-2'>
                                                    <label htmlFor="company"
                                                           className="block text-md tracking-wider font-medium text-gray-500">
                                                        Choose Account*
                                                    </label>
                                                    <div className="mt-1">
                                                        <CategoryDropdown
                                                            type="EXPENSE"
                                                            categories={allCategories}
                                                            category={category}
                                                            setCategory={setCategory}
                                                        />
                                                    </div>
                                                </div>

                                                <div className='sm:col-span-2'>
                                                    <label htmlFor="company"
                                                           className="block text-md tracking-wider font-medium text-gray-500">
                                                        Description
                                                    </label>

                                                    <div className="mt-1">
                                                        <textarea
                                                            id="description"
                                                            name="description"
                                                            rows={4}
                                                            value={values.description}
                                                            onChange={handleChange}
                                                            onBlur={handleBlur}
                                                            className="max-w-fullpy-3 px-4 block w-full border-2  focus:ring-indigo-400 focus:border-indigo-400 border-gray-200 rounded-md "
                                                        />
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="flex-shrink-0  px-4 py-4 flex justify-between">
                                <div>
                                    <button
                                        type="submit"
                                        className="ml-4 inline-flex justify-center py-2 px-4 tracking-wider border border-transparent uppercase shadow-sm  font-medium text-sm rounded-full text-white bg-green-600 hover:bg-green-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-500"
                                        disabled={isSubmitting}
                                    >
                                        {isSubmitting ? "Please wait..." : "Update"}
                                    </button>
                                </div>
                            </div>
                        </Form>
                    )}
                </Formik>
            )
        } else if (matchObj.type === 'credit') {
            //expense
            return (
                <Formik
                    initialValues={initialValuesEdit}
                    onSubmit={(values, {setSubmitting}) => {
                        handleFormSubmit(values, setSubmitting);
                    }}>
                    {({values, errors, handleChange, handleSubmit, handleBlur, touched, isSubmitting, setFieldValue}) => (
                        <Form onSubmit={handleSubmit} className="h-full divide-y divide-gray-200 flex flex-col bg-white shadow-xl">
                            <div className="h-full divide-y divide-gray-200 flex flex-col bg-white">
                                <div className="flex-1 h-0 pb-8 overflow-y-scroll">
                                    <div className="flex-1 flex flex-col justify-between">
                                        <div className="px-4 divide-y divide-gray-200 sm:px-6">
                                            <div
                                                className="space-y-6  mb-4 pt-6 pb-7 items-baseline grid grid-cols-1 sm:pt-0 md:grid-cols-2 sm:gap-x-8"
                                            >
                                                <input type='hidden' value={updated} />

                                                <div className="sm:col-span-2">
                                                    <label htmlFor="company"
                                                           className="block text-md tracking-wider font-medium text-gray-500">
                                                        Name of Client*
                                                    </label>

                                                    <div className="mt-1">
                                                        <input
                                                            type="text"
                                                            name="client_name"
                                                            id="client_name"
                                                            value={values.client_name}
                                                            readOnly={true}
                                                            disabled={true}
                                                            className="placeholder-gray-400 py-3 px-4 block w-full border-2  focus:ring-indigo-400 focus:border-indigo-400 border-gray-200 rounded-md "
                                                        />
                                                    </div>
                                                </div>

                                                <div className='sm:col-span-2'>
                                                    <label htmlFor="company"
                                                           className="block text-md tracking-wider font-medium text-gray-500">
                                                        Choose Account*
                                                    </label>
                                                    <div className="mt-1">
                                                        <CategoryDropdown
                                                            type="INCOME"
                                                            categories={allCategories}
                                                            category={category}
                                                            setCategory={setCategory}
                                                        />
                                                    </div>
                                                </div>

                                                <div className='sm:col-span-2'>
                                                    <label htmlFor="company"
                                                           className="block text-md tracking-wider font-medium text-gray-500">
                                                        Description
                                                    </label>

                                                    <div className="mt-1">
                                                        <textarea
                                                            id="description"
                                                            name="description"
                                                            rows={4}
                                                            value={values.description}
                                                            onChange={handleChange}
                                                            onBlur={handleBlur}
                                                            className="max-w-fullpy-3 px-4 block w-full border-2  focus:ring-indigo-400 focus:border-indigo-400 border-gray-200 rounded-md "
                                                        />
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div className="flex-shrink-0  px-4 py-4 flex justify-between">
                                <div>
                                    <button
                                        type="submit"
                                        className="ml-4 inline-flex justify-center py-2 px-4 tracking-wider border border-transparent uppercase shadow-sm  font-medium text-sm rounded-full text-white bg-green-600 hover:bg-green-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-500"
                                        disabled={isSubmitting}
                                    >
                                        {isSubmitting ? "Please wait..." : "Update"}
                                    </button>
                                </div>
                            </div>
                        </Form>
                    )}
                </Formik>
            )
        }
    }

    return null;
}

