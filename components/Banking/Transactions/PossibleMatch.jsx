export default function PossibleMatch(props) {
    const { transaction, openMatch } = props;

    if (transaction.type === 'credit') {
        if (transaction.possible_match > 0) {
            return (
                <a
                    href="javascript:void(0)"
                    className="block text-brand-blue text-sm underline"
                    onClick={() => openMatch(transaction)}
                >
                    {transaction.possible_match} Possible Match
                </a>
            )
        }

        if (transaction.possible_cash_match > 0) {
            return (
                <a
                    href="javascript:void(0)"
                    className="block text-brand-blue text-sm underline"
                    onClick={() => openMatch(transaction)}
                >
                    {transaction.possible_cash_match} Possible Cash Match
                </a>
            )
        }
    }

    if (transaction.type === 'debit') {
        let count = 0
        let cashCount = 0
        if (transaction.possible_match_expense > 0) {
            count += transaction.possible_match_expense
        }
        if (transaction.possible_match_asset > 0) {
            count += transaction.possible_match_asset
        }
        if (transaction.possible_cash_match_expense > 0) {
            cashCount += transaction.possible_cash_match_expense
        }
        if (transaction.possible_cash_match_asset > 0) {
            cashCount += transaction.possible_cash_match_asset
        }
        if (count > 0) {
            return (
                <a
                    href="javascript:void(0)"
                    className="block text-brand-blue text-sm underline"
                    onClick={() => openMatch(transaction)}
                >
                    {count} Possible Match
                </a>
            )
        }

        if (cashCount > 0) {
            return (
                <a
                    href="javascript:void(0)"
                    className="block text-brand-blue text-sm underline"
                    onClick={() => openMatch(transaction)}
                >
                    {cashCount} Possible Cash Match
                </a>
            )
        }
    }

    return ''

}
