import { Fragment, useState } from "react";
import { Dialog, Transition, Switch } from "@headlessui/react";
import { XIcon } from "@heroicons/react/outline";
import { Form, Formik } from "formik";
import { enGB } from "date-fns/locale";
import Styles from "../../../styles/Invoices.module.scss";
import { formatCurrency, formatDateToDB } from "../../../helpers/common";
import DateTimePicker from "../../DateTimePicker";

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

export default function BankBalanceFilter(props) {
  const { accounts, bankFilters, setFilter, setOpen, open } = props;

  const [accIds, setAccIds] = useState(
    typeof bankFilters.bank_account_id !== "undefined"
      ? bankFilters.bank_account_id
      : []
  );
  const [dateRange, setDateRange] = useState([null, null]);
  const [startDate, endDate] = dateRange;
  const [from, setFrom] = useState("");
  const [to, setTo] = useState("");
  const [income, setIncome] = useState(false);
  const [expense, setExpense] = useState(false);

  const [showAccount, setShowAccount] = useState(false);
  const [showType, setShowType] = useState(false);
  const [showDate, setShowDate] = useState(false);
  const [showRange, setShowRange] = useState(false);

  const togglebyaccount = () => {
    if (showAccount) {
      setAccIds([]);
    }
    setShowAccount(!showAccount);
  };

  const togglebytype = () => {
    setShowType(!showType);
  };

  const togglebydate = () => {
    setShowDate(!showDate);
  };

  const togglebyrange = () => {
    if (showRange) {
      setFrom("");
      setTo("");
    }
    setShowRange(!showRange);
  };

  const changeAccount = (id) => {
    let ids = accIds;
    let index = ids.indexOf(id);
    if (index === -1) {
      //add
      ids.push(id);
    } else {
      //remove
      ids.splice(index, 1);
    }
    setAccIds(ids);
  };

  const apply = () => {
    let filter = {};
    if (showAccount) {
      filter.bank_account_id = accIds;
    }

    if (showDate) {
      if (startDate !== "") {
        filter.start_date = formatDateToDB(startDate);
      }
      if (endDate !== "") {
        filter.end_date = formatDateToDB(endDate);
      }
    }

    if (showRange) {
      if (from != "") {
        filter.min_price = from;
      }
      if (to != "") {
        filter.max_price = to;
      }
    }

    if (showType) {
      if (income && expense) {
        filter.type = "income | expense";
      } else if (income && !expense) {
        filter.type = "income";
      } else if (!income && expense) {
        filter.type = "expense";
      }
    }

    filter.fromFilter = true;

    setFilter("bank", filter);
    setOpen(false);
  };

  const reset = () => {
    setAccIds([]);
    setDateRange([null, null]);
    setFrom("");
    setTo("");
    setIncome(false);
    setExpense(false);
    setShowAccount(false);
    setShowType(false);
    setShowDate(false);
    setShowRange(false);
    let filter = {};
    filter.fromFilter = true;
    setFilter("bank", filter);
    setOpen(false);
  };

  return (
    <Transition.Root show={open} as={Fragment}>
      <Dialog
        as="div"
        static
        className="fixed inset-0 overflow-hidden"
        onClose={setOpen}
      >
        <div className="absolute inset-0 overflow-hidden">
          <Transition.Child
            as={Fragment}
            enter="ease-in-out duration-500"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in-out duration-500"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <Dialog.Overlay className="absolute inset-0 bg-gray-500 bg-opacity-75 transition-opacity pointer-events-none" />
          </Transition.Child>

          <div className="fixed inset-y-0 max-w-3xl right-0 flex">
            <Transition.Child
              as={Fragment}
              enter="transform transition ease-in-out duration-500 sm:duration-700"
              enterFrom="translate-x-full"
              enterTo="translate-x-0"
              leave="transform transition ease-in-out duration-500 sm:duration-700"
              leaveFrom="translate-x-0"
              leaveTo="translate-x-full"
            >
              <div className="w-screen lg:max-w-6xl">
                <Formik>
                  <Form className="h-full divide-y divide-gray-200 flex flex-col bg-white shadow-xl">
                    <div className="flex-1 h-0 pb-8 overflow-y-auto">
                      <div className="border-b border-gray-200 bg-gray-100 py-6 px-4 sm:px-6">
                        <div className="flex items-center justify-between">
                          <Dialog.Title className="text-xl font-medium text-gray-800">
                            Bank Balance Filter
                          </Dialog.Title>
                          <div className="ml-3 h-7 flex items-center">
                            <button
                              type="button"
                              className="rounded-full hover:bg-gray-300 p-2 text-gray-500 hover:text-gray-700 focus:outline-none focus:ring-2 focus:ring-white"
                              onClick={() => setOpen(false)}
                            >
                              <span className="sr-only">Close panel</span>
                              <XIcon className="h-6 w-6" aria-hidden="true" />
                            </button>
                          </div>
                        </div>
                      </div>

                      <div className="flex-1 flex flex-col justify-between">
                        <div className="px-4 divide-y divide-gray-200 sm:px-6">
                          <div className="space-y-6 border-gray-200 mb-4 pt-6 pb-7 items-baseline grid grid-cols-1 gap-y-2  sm:gap-x-8">
                            <div className="bg-gray-50 relative shadow sm:rounded-lg">
                              <Switch.Group
                                as="div"
                                className="px-4  py-5 sm:p-6"
                              >
                                <div className="inline-flex justify-between w-full">
                                  <Switch.Label
                                    as="h3"
                                    className="text-lg leading-6 font-medium text-gray-700"
                                  >
                                    Account
                                  </Switch.Label>
                                  <div className="mt-5 sm:mt-0 sm:ml-6 sm:flex-shrink-0 sm:flex sm:items-center">
                                    <Switch
                                      checked={showAccount}
                                      onChange={(e) => {
                                        setShowAccount(!showAccount);
                                        togglebyaccount();
                                      }}
                                      className={classNames(
                                        showAccount
                                          ? "bg-green-600"
                                          : "bg-gray-300",
                                        "relative inline-flex flex-shrink-0 h-6 w-11 border-2 border-transparent rounded-full cursor-pointer transition-colors ease-in-out duration-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                                      )}
                                    >
                                      <span
                                        className={classNames(
                                          showAccount
                                            ? "translate-x-5"
                                            : "translate-x-0",
                                          "pointer-events-none relative inline-block h-5 w-5 rounded-full bg-white shadow transform ring-0 transition ease-in-out duration-200"
                                        )}
                                      >
                                        <span
                                          className={classNames(
                                            showAccount
                                              ? "opacity-0 ease-out duration-100"
                                              : "opacity-100 ease-in duration-200",
                                            "absolute inset-0 h-full w-full flex items-center justify-center transition-opacity pointer-events-none"
                                          )}
                                          aria-hidden="true"
                                        >
                                          <svg
                                            className="h-3 w-3 text-gray-400"
                                            fill="none"
                                            viewBox="0 0 12 12"
                                          >
                                            <path
                                              d="M4 8l2-2m0 0l2-2M6 6L4 4m2 2l2 2"
                                              stroke="currentColor"
                                              strokeWidth={2}
                                              strokeLinecap="round"
                                              strokeLinejoin="round"
                                            />
                                          </svg>
                                        </span>
                                        <span
                                          className={classNames(
                                            showAccount
                                              ? "opacity-100 ease-in duration-200"
                                              : "opacity-0 ease-out duration-100",
                                            "absolute inset-0 h-full w-full flex items-center justify-center transition-opacity pointer-events-none"
                                          )}
                                          aria-hidden="true"
                                        >
                                          <svg
                                            className="h-3 w-3 text-green-600"
                                            fill="currentColor"
                                            viewBox="0 0 12 12"
                                          >
                                            <path d="M3.707 5.293a1 1 0 00-1.414 1.414l1.414-1.414zM5 8l-.707.707a1 1 0 001.414 0L5 8zm4.707-3.293a1 1 0 00-1.414-1.414l1.414 1.414zm-7.414 2l2 2 1.414-1.414-2-2-1.414 1.414zm3.414 2l4-4-1.414-1.414-4 4 1.414 1.414z" />
                                          </svg>
                                        </span>
                                      </span>
                                    </Switch>
                                  </div>
                                </div>
                              </Switch.Group>

                              <div
                                className={classNames(
                                  showAccount ? "block" : "hidden",
                                  "collapse px-4 py-5 sm:p-6 border-t border-gray-200"
                                )}
                              >
                                <div className="border-gray-200 border relative rounded-md shadow-sm divide-y divide-gray-200 items-center">
                                  {accounts.map((account, key) => (
                                    <div
                                      className="flex justify-between p-4  w-full items-center relative"
                                      key={`acc_${key}`}
                                    >
                                      <div>
                                        <span className="block">
                                          {account.account_type_name}
                                        </span>
                                        <span className="block">
                                          ACC:{account.account_number} (
                                          {formatCurrency(account.balance)}){" "}
                                        </span>
                                      </div>

                                      <div className="mb-5">
                                        <input
                                          id={`acc_${key}`}
                                          name={`acc_${key}`}
                                          type="checkbox"
                                          value={
                                            account.bank_connection
                                              .bank_connection_id
                                          }
                                          className="focus:ring-brand-blue h-4 w-4 text-brand-blue border-gray-300 rounded"
                                          onClick={(e) =>
                                            changeAccount(
                                              account.bank_connection
                                                .bank_connection_id
                                            )
                                          }
                                          defaultChecked={
                                            accIds.indexOf(
                                              account.bank_connection
                                                .bank_connection_id
                                            ) !== -1
                                          }
                                        />
                                      </div>

                                      {account.is_primary ? (
                                        <span className="absolute right-3 top-8 w-20 block ml-auto mt-4 mb-auto text-center px-2.5 py-0.5 rounded-full text-xs font-semibold tracking-wider bg-blue-100 text-blue-800">
                                          Primary
                                        </span>
                                      ) : (
                                        ""
                                      )}
                                    </div>
                                  ))}
                                </div>
                              </div>
                            </div>

                            {/* end for account */}

                            <div className="bg-gray-50 relative shadow sm:rounded-lg">
                              <Switch.Group
                                as="div"
                                className="px-4  py-5 sm:p-6"
                              >
                                <div className="inline-flex justify-between w-full">
                                  <Switch.Label
                                    as="h3"
                                    className="text-lg leading-6 font-medium text-gray-700"
                                  >
                                    Type
                                  </Switch.Label>
                                  <div className="mt-5 sm:mt-0 sm:ml-6 sm:flex-shrink-0 sm:flex sm:items-center">
                                    <Switch
                                      checked={showType}
                                      onChange={(e) => {
                                        setShowType(!showType);
                                        togglebytype();
                                      }}
                                      className={classNames(
                                        showType
                                          ? "bg-green-600"
                                          : "bg-gray-300",
                                        "relative inline-flex flex-shrink-0 h-6 w-11 border-2 border-transparent rounded-full cursor-pointer transition-colors ease-in-out duration-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                                      )}
                                    >
                                      <span
                                        className={classNames(
                                          showType
                                            ? "translate-x-5"
                                            : "translate-x-0",
                                          "pointer-events-none relative inline-block h-5 w-5 rounded-full bg-white shadow transform ring-0 transition ease-in-out duration-200"
                                        )}
                                      >
                                        <span
                                          className={classNames(
                                            showType
                                              ? "opacity-0 ease-out duration-100"
                                              : "opacity-100 ease-in duration-200",
                                            "absolute inset-0 h-full w-full flex items-center justify-center transition-opacity pointer-events-none"
                                          )}
                                          aria-hidden="true"
                                        >
                                          <svg
                                            className="h-3 w-3 text-gray-400"
                                            fill="none"
                                            viewBox="0 0 12 12"
                                          >
                                            <path
                                              d="M4 8l2-2m0 0l2-2M6 6L4 4m2 2l2 2"
                                              stroke="currentColor"
                                              strokeWidth={2}
                                              strokeLinecap="round"
                                              strokeLinejoin="round"
                                            />
                                          </svg>
                                        </span>
                                        <span
                                          className={classNames(
                                            showType
                                              ? "opacity-100 ease-in duration-200"
                                              : "opacity-0 ease-out duration-100",
                                            "absolute inset-0 h-full w-full flex items-center justify-center transition-opacity pointer-events-none"
                                          )}
                                          aria-hidden="true"
                                        >
                                          <svg
                                            className="h-3 w-3 text-green-600"
                                            fill="currentColor"
                                            viewBox="0 0 12 12"
                                          >
                                            <path d="M3.707 5.293a1 1 0 00-1.414 1.414l1.414-1.414zM5 8l-.707.707a1 1 0 001.414 0L5 8zm4.707-3.293a1 1 0 00-1.414-1.414l1.414 1.414zm-7.414 2l2 2 1.414-1.414-2-2-1.414 1.414zm3.414 2l4-4-1.414-1.414-4 4 1.414 1.414z" />
                                          </svg>
                                        </span>
                                      </span>
                                    </Switch>
                                  </div>
                                </div>
                              </Switch.Group>

                              <div
                                className={classNames(
                                  showType ? "block" : "hidden",
                                  "collapse px-4 py-5 sm:p-6 border-t border-gray-200"
                                )}
                              >
                                <div className="border-gray-200 border relative rounded-md shadow-sm divide-y divide-gray-200 items-center">
                                  <div className="flex justify-between p-4 w-full items-center relative">
                                    <div>
                                      <span className="block">Income</span>
                                    </div>

                                    <div className="">
                                      <input
                                        id="income"
                                        name="income"
                                        value="income"
                                        type="checkbox"
                                        className="focus:ring-brand-blue h-4 w-4 text-brand-blue border-gray-300 rounded"
                                        onClick={(e) => setIncome(!income)}
                                        defaultChecked={income}
                                      />
                                    </div>
                                  </div>

                                  <div className="flex justify-between  w-full items-center p-4  relative">
                                    <div>
                                      <span className="block">Expenses</span>
                                    </div>

                                    <div className="">
                                      <input
                                        id="expense"
                                        name="expense"
                                        value="expense"
                                        type="checkbox"
                                        className="focus:ring-brand-blue h-4 w-4 text-brand-blue border-gray-300 rounded"
                                        onClick={(e) => setExpense(!expense)}
                                        defaultChecked={expense}
                                      />
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>

                            {/* end for type    */}

                            {/* Date filter */}
                            <div className="bg-gray-50 relative shadow sm:rounded-lg">
                              <Switch.Group
                                as="div"
                                className="px-4  py-5 sm:p-6"
                              >
                                <div className="inline-flex justify-between w-full">
                                  <Switch.Label
                                    as="h3"
                                    className="text-lg leading-6 font-medium text-gray-700"
                                  >
                                    Date
                                  </Switch.Label>
                                  <div className="mt-5 sm:mt-0 sm:ml-6 sm:flex-shrink-0 sm:flex sm:items-center">
                                    <Switch
                                      checked={showDate}
                                      onChange={(e) => {
                                        setShowDate(!showDate);
                                        togglebydate();
                                      }}
                                      className={classNames(
                                        showDate
                                          ? "bg-green-600"
                                          : "bg-gray-300",
                                        "relative inline-flex flex-shrink-0 h-6 w-11 border-2 border-transparent rounded-full cursor-pointer transition-colors ease-in-out duration-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                                      )}
                                    >
                                      <span
                                        className={classNames(
                                          showDate
                                            ? "translate-x-5"
                                            : "translate-x-0",
                                          "pointer-events-none relative inline-block h-5 w-5 rounded-full bg-white shadow transform ring-0 transition ease-in-out duration-200"
                                        )}
                                      >
                                        <span
                                          className={classNames(
                                            showDate
                                              ? "opacity-0 ease-out duration-100"
                                              : "opacity-100 ease-in duration-200",
                                            "absolute inset-0 h-full w-full flex items-center justify-center transition-opacity pointer-events-none"
                                          )}
                                          aria-hidden="true"
                                        >
                                          <svg
                                            className="h-3 w-3 text-gray-400"
                                            fill="none"
                                            viewBox="0 0 12 12"
                                          >
                                            <path
                                              d="M4 8l2-2m0 0l2-2M6 6L4 4m2 2l2 2"
                                              stroke="currentColor"
                                              strokeWidth={2}
                                              strokeLinecap="round"
                                              strokeLinejoin="round"
                                            />
                                          </svg>
                                        </span>
                                        <span
                                          className={classNames(
                                            showDate
                                              ? "opacity-100 ease-in duration-200"
                                              : "opacity-0 ease-out duration-100",
                                            "absolute inset-0 h-full w-full flex items-center justify-center transition-opacity pointer-events-none"
                                          )}
                                          aria-hidden="true"
                                        >
                                          <svg
                                            className="h-3 w-3 text-green-600"
                                            fill="currentColor"
                                            viewBox="0 0 12 12"
                                          >
                                            <path d="M3.707 5.293a1 1 0 00-1.414 1.414l1.414-1.414zM5 8l-.707.707a1 1 0 001.414 0L5 8zm4.707-3.293a1 1 0 00-1.414-1.414l1.414 1.414zm-7.414 2l2 2 1.414-1.414-2-2-1.414 1.414zm3.414 2l4-4-1.414-1.414-4 4 1.414 1.414z" />
                                          </svg>
                                        </span>
                                      </span>
                                    </Switch>
                                  </div>
                                </div>
                              </Switch.Group>

                              <div
                                className={classNames(
                                  showDate ? "block" : "hidden",
                                  "collapse px-4 py-5 sm:p-6 border-t border-gray-200"
                                )}
                              >
                                <div className="grid grid-cols-1 mt-0  date-picker bg-white relative border-2 focus:ring-indigo-400 focus:border-indigo-400 border-gray-100 rounded-md">
                                  <div className={Styles.calendar}>
                                    <DateTimePicker
                                      range={true}
                                      startDate={startDate}
                                      endDate={endDate}
                                      onDateChange={(val) => {
                                        setDateRange(val);
                                      }}
                                      inputClassName={
                                        "input py-3 px-4 block w-full border-0"
                                      }
                                      maxDate={new Date()}
                                      locale={enGB}
                                    />
                                  </div>
                                </div>
                              </div>
                            </div>
                            {/* end date filter            */}

                            {/* Range filter */}
                            <div className="bg-gray-50 relative shadow sm:rounded-lg">
                              <Switch.Group
                                as="div"
                                className="px-4  py-5 sm:p-6"
                              >
                                <div className="inline-flex justify-between w-full">
                                  <Switch.Label
                                    as="h3"
                                    className="text-lg leading-6 font-medium text-gray-700"
                                  >
                                    Range ($)
                                  </Switch.Label>
                                  <div className="mt-5 sm:mt-0 sm:ml-6 sm:flex-shrink-0 sm:flex sm:items-center">
                                    <Switch
                                      checked={showRange}
                                      onChange={(e) => {
                                        setShowRange(!showRange);
                                        togglebyrange();
                                      }}
                                      className={classNames(
                                        showRange
                                          ? "bg-green-600"
                                          : "bg-gray-300",
                                        "relative inline-flex flex-shrink-0 h-6 w-11 border-2 border-transparent rounded-full cursor-pointer transition-colors ease-in-out duration-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                                      )}
                                    >
                                      <span
                                        className={classNames(
                                          showRange
                                            ? "translate-x-5"
                                            : "translate-x-0",
                                          "pointer-events-none relative inline-block h-5 w-5 rounded-full bg-white shadow transform ring-0 transition ease-in-out duration-200"
                                        )}
                                      >
                                        <span
                                          className={classNames(
                                            showRange
                                              ? "opacity-0 ease-out duration-100"
                                              : "opacity-100 ease-in duration-200",
                                            "absolute inset-0 h-full w-full flex items-center justify-center transition-opacity pointer-events-none"
                                          )}
                                          aria-hidden="true"
                                        >
                                          <svg
                                            className="h-3 w-3 text-gray-400"
                                            fill="none"
                                            viewBox="0 0 12 12"
                                          >
                                            <path
                                              d="M4 8l2-2m0 0l2-2M6 6L4 4m2 2l2 2"
                                              stroke="currentColor"
                                              strokeWidth={2}
                                              strokeLinecap="round"
                                              strokeLinejoin="round"
                                            />
                                          </svg>
                                        </span>
                                        <span
                                          className={classNames(
                                            showRange
                                              ? "opacity-100 ease-in duration-200"
                                              : "opacity-0 ease-out duration-100",
                                            "absolute inset-0 h-full w-full flex items-center justify-center transition-opacity pointer-events-none"
                                          )}
                                          aria-hidden="true"
                                        >
                                          <svg
                                            className="h-3 w-3 text-green-600"
                                            fill="currentColor"
                                            viewBox="0 0 12 12"
                                          >
                                            <path d="M3.707 5.293a1 1 0 00-1.414 1.414l1.414-1.414zM5 8l-.707.707a1 1 0 001.414 0L5 8zm4.707-3.293a1 1 0 00-1.414-1.414l1.414 1.414zm-7.414 2l2 2 1.414-1.414-2-2-1.414 1.414zm3.414 2l4-4-1.414-1.414-4 4 1.414 1.414z" />
                                          </svg>
                                        </span>
                                      </span>
                                    </Switch>
                                  </div>
                                </div>
                              </Switch.Group>

                              <div
                                className={classNames(
                                  showRange ? "block" : "hidden",
                                  "collapse px-4 py-5 sm:p-6 border-t border-gray-200"
                                )}
                              >
                                <div className="grid grid-cols-1 mt-0">
                                  <div>
                                    <div className="flex gap-x-2">
                                      <div className="w-6/12">
                                        <span className="block mb-2">From</span>
                                        <input
                                          type="text"
                                          className="input w-full rounded-md border-2 border-gray-200 py-3 px-4 mr-2 text-sm font-medium bg-white"
                                          onChange={(e) =>
                                            setFrom(e.target.value)
                                          }
                                          value={from}
                                        />
                                      </div>

                                      <div className="w-6/12">
                                        <span className="block mb-2">To</span>
                                        <input
                                          type="text"
                                          className="input w-full rounded-md border-2 border-gray-200 py-3 px-4 mr-2 text-sm font-medium bg-white"
                                          onChange={(e) =>
                                            setTo(e.target.value)
                                          }
                                          value={to}
                                        />
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>

                            {/* end range filter */}
                          </div>
                        </div>
                      </div>
                    </div>

                    <div className="flex-shrink-0  px-4 py-4 flex justify-between">
                      <div>
                        <button
                          type="button"
                          className="ml-4 inline-flex justify-center py-2 px-4 tracking-wider border border-transparent uppercase shadow-sm  font-medium text-sm rounded-full text-white bg-red-600 hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-500"
                          onClick={reset}
                        >
                          Reset filter
                        </button>
                      </div>
                      <div>
                        <button
                          type="button"
                          className="ml-4 inline-flex justify-center py-2 px-4 tracking-wider border border-transparent uppercase shadow-sm  font-medium text-sm rounded-full text-white bg-green-600 hover:bg-green-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-500"
                          onClick={apply}
                        >
                          Apply filter
                        </button>
                      </div>
                    </div>
                  </Form>
                </Formik>
              </div>
            </Transition.Child>
          </div>
        </div>
      </Dialog>
    </Transition.Root>
  );
}
