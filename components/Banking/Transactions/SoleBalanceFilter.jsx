import { Fragment, useState, useEffect } from "react";
import { Dialog, Transition, Switch } from "@headlessui/react";
import { XIcon } from "@heroicons/react/outline";
import { Form, Formik } from "formik";
import { enGB } from "date-fns/locale";
import Styles from "../../../styles/Invoices.module.scss";
import { formatDateToDB } from "../../../helpers/common";
import { getAllCategories } from "../../../services/api/accountCategory.services";
import DateTimePicker from "../../DateTimePicker";

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

export default function SoleBalanceFilter(props) {
  const { soleFilters, setFilter, setOpenSole, openSole } = props;

  const [allCategories, setAllCategories] = useState({});
  const [dateRange, setDateRange] = useState([null, null]);
  const [startDate, endDate] = dateRange;
  const [from, setFrom] = useState("");
  const [to, setTo] = useState("");
  const [showCategory, setShowCategory] = useState(false);
  const [showDate, setShowDate] = useState(false);
  const [showRange, setShowRange] = useState(false);
  const [personal, setPersonal] = useState(false);
  const [cash, setCash] = useState(false);
  const [catIds, setCatIds] = useState(
    typeof soleFilters.account_subcategory_id !== "undefined"
      ? soleFilters.account_subcategory_id
      : []
  );

  useEffect(async () => {
    let data = await getAllCategories();
    setAllCategories(data);
  }, []);

  const togglebycategory = () => {
    setShowCategory(!showCategory);
  };

  const togglebydate = () => {
    setShowDate(!showDate);
  };

  const togglebyrange = () => {
    if (showRange) {
      setFrom("");
      setTo("");
    }
    setShowRange(!showRange);
  };

  const changeCat = (id) => {
    let ids = catIds;
    let index = ids.indexOf(id);
    if (index === -1) {
      //add
      ids.push(id);
    } else {
      //remove
      ids.splice(index, 1);
    }
    setCatIds(ids);
  };

  const apply = () => {
    let filter = {};

    if (showDate) {
      if (startDate !== "") {
        filter.start_date = formatDateToDB(startDate);
      }
      if (endDate !== "") {
        filter.end_date = formatDateToDB(endDate);
      }
    }

    if (showRange) {
      if (from != "") {
        filter.min_price = from;
      }
      if (to != "") {
        filter.max_price = to;
      }
    }

    if (showCategory && cash) {
      filter.cash = 1;
    }

    if (showCategory && personal) {
      filter.personal = 1;
    }

    if (showCategory && catIds.length > 0) {
      filter.account_subcategory_id = catIds;
    }

    filter.fromFilter = true;

    setFilter("sole", filter);
    setOpenSole(false);
  };

  const reset = () => {
    setCatIds([]);
    setDateRange([null, null]);
    setFrom("");
    setTo("");
    setShowCategory(false);
    setShowDate(false);
    setShowRange(false);
    setCash(false);
    setPersonal(false);
    let filter = {};
    filter.fromFilter = true;
    setFilter("sole", filter);
    setOpenSole(false);
  };

  return (
    <Transition.Root show={openSole} as={Fragment}>
      <Dialog
        as="div"
        static
        className="fixed inset-0 overflow-hidden"
        onClose={setOpenSole}
      >
        <div className="absolute inset-0 overflow-hidden">
          <Transition.Child
            as={Fragment}
            enter="ease-in-out duration-500"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in-out duration-500"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <Dialog.Overlay className="absolute inset-0 bg-gray-500 bg-opacity-75 transition-opacity pointer-events-none" />
          </Transition.Child>

          <div className="fixed inset-y-0 max-w-3xl right-0 flex">
            <Transition.Child
              as={Fragment}
              enter="transform transition ease-in-out duration-500 sm:duration-700"
              enterFrom="translate-x-full"
              enterTo="translate-x-0"
              leave="transform transition ease-in-out duration-500 sm:duration-700"
              leaveFrom="translate-x-0"
              leaveTo="translate-x-full"
            >
              <div className="w-screen lg:max-w-6xl">
                <Formik>
                  <Form className="h-full divide-y divide-gray-200 flex flex-col bg-white shadow-xl">
                    <div className="flex-1 h-0 pb-8 overflow-y-auto">
                      <div className="border-b border-gray-200 bg-gray-100 py-6 px-4 sm:px-6">
                        <div className="flex items-center justify-between">
                          <Dialog.Title className="text-xl font-medium text-gray-800">
                            Sole Balance Filter
                          </Dialog.Title>
                          <div className="ml-3 h-7 flex items-center">
                            <button
                              type="button"
                              className="rounded-full hover:bg-gray-300 p-2 text-gray-500 hover:text-gray-700 focus:outline-none focus:ring-2 focus:ring-white"
                              onClick={() => setOpenSole(false)}
                            >
                              <span className="sr-only">Close panel</span>
                              <XIcon className="h-6 w-6" aria-hidden="true" />
                            </button>
                          </div>
                        </div>
                      </div>

                      <div className="flex-1 flex flex-col justify-between">
                        <div className="px-4 divide-y divide-gray-200 sm:px-6">
                          <div className="space-y-6 border-gray-200 mb-4 pt-6 pb-7 items-baseline grid grid-cols-1 gap-y-2  sm:gap-x-8">
                            <div className="bg-gray-50 relative shadow sm:rounded-lg">
                              <Switch.Group
                                as="div"
                                className="px-4  py-5 sm:p-6"
                              >
                                <div className="inline-flex justify-between w-full">
                                  <Switch.Label
                                    as="h3"
                                    className="text-lg leading-6 font-medium text-gray-700"
                                  >
                                    Category
                                  </Switch.Label>
                                  <div className="mt-5 sm:mt-0 sm:ml-6 sm:flex-shrink-0 sm:flex sm:items-center">
                                    <Switch
                                      checked={showCategory}
                                      onChange={(e) => {
                                        setShowCategory(!showCategory);
                                        togglebycategory();
                                      }}
                                      className={classNames(
                                        showCategory
                                          ? "bg-green-600"
                                          : "bg-gray-300",
                                        "relative inline-flex flex-shrink-0 h-6 w-11 border-2 border-transparent rounded-full cursor-pointer transition-colors ease-in-out duration-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                                      )}
                                    >
                                      <span
                                        className={classNames(
                                          showCategory
                                            ? "translate-x-5"
                                            : "translate-x-0",
                                          "pointer-events-none relative inline-block h-5 w-5 rounded-full bg-white shadow transform ring-0 transition ease-in-out duration-200"
                                        )}
                                      >
                                        <span
                                          className={classNames(
                                            showCategory
                                              ? "opacity-0 ease-out duration-100"
                                              : "opacity-100 ease-in duration-200",
                                            "absolute inset-0 h-full w-full flex items-center justify-center transition-opacity pointer-events-none"
                                          )}
                                          aria-hidden="true"
                                        >
                                          <svg
                                            className="h-3 w-3 text-gray-400"
                                            fill="none"
                                            viewBox="0 0 12 12"
                                          >
                                            <path
                                              d="M4 8l2-2m0 0l2-2M6 6L4 4m2 2l2 2"
                                              stroke="currentColor"
                                              strokeWidth={2}
                                              strokeLinecap="round"
                                              strokeLinejoin="round"
                                            />
                                          </svg>
                                        </span>
                                        <span
                                          className={classNames(
                                            showCategory
                                              ? "opacity-100 ease-in duration-200"
                                              : "opacity-0 ease-out duration-100",
                                            "absolute inset-0 h-full w-full flex items-center justify-center transition-opacity pointer-events-none"
                                          )}
                                          aria-hidden="true"
                                        >
                                          <svg
                                            className="h-3 w-3 text-green-600"
                                            fill="currentColor"
                                            viewBox="0 0 12 12"
                                          >
                                            <path d="M3.707 5.293a1 1 0 00-1.414 1.414l1.414-1.414zM5 8l-.707.707a1 1 0 001.414 0L5 8zm4.707-3.293a1 1 0 00-1.414-1.414l1.414 1.414zm-7.414 2l2 2 1.414-1.414-2-2-1.414 1.414zm3.414 2l4-4-1.414-1.414-4 4 1.414 1.414z" />
                                          </svg>
                                        </span>
                                      </span>
                                    </Switch>
                                  </div>
                                </div>
                              </Switch.Group>

                              <div
                                className={classNames(
                                  showCategory ? "block" : "hidden",
                                  "collapse px-4 py-5 sm:p-6 border-t border-gray-200"
                                )}
                              >
                                <div className="grid grid-cols-1 gap-3 mt-0">
                                  {allCategories.length > 0 &&
                                    allCategories.map((cat, key) => (
                                      <div
                                        className="bg-white rounded-bl-md rounded-br-md"
                                        key={`cat_${key}`}
                                      >
                                        <h3 className="text-lg bg-gray-200 rounded-tl-md rounded-tr-md p-3 border-b border-gray-300 text-gray-700">
                                          {cat.name}
                                        </h3>

                                        <div className="divide-y divide-gray-100">
                                          {cat.account_subcategories.map(
                                            (subCat, subKey) => (
                                              <div
                                                className="relative flex items-center py-2"
                                                key={`subcat_${subKey}`}
                                              >
                                                <div className="min-w-0  flex-1 text-sm">
                                                  <label className="font-medium p-4  text-gray-700 select-none">
                                                    {subCat.name}
                                                  </label>
                                                </div>

                                                <div className="ml-3 flex items-center p-4 h-5">
                                                  <input
                                                    id={`subcatid_${subCat.account_subcategory_id}`}
                                                    name={`subcatid_${subCat.account_subcategory_id}`}
                                                    type="checkbox"
                                                    className="focus:ring-indigo-500 h-4 w-4 text-brand-blue rounded-full border-0 bg-gray-300"
                                                    onClick={(e) =>
                                                      changeCat(
                                                        subCat.account_subcategory_id
                                                      )
                                                    }
                                                    defaultChecked={
                                                      catIds.indexOf(
                                                        subCat.account_subcategory_id
                                                      ) !== -1
                                                    }
                                                  />
                                                </div>
                                              </div>
                                            )
                                          )}
                                        </div>
                                      </div>
                                    ))}

                                  {/* Cash and Personal */}
                                  <div className="bg-white rounded-bl-md rounded-br-md">
                                    <div className="divide-y divide-gray-100">
                                      <div className="relative flex items-center py-2">
                                        <div className="min-w-0  flex-1 text-sm">
                                          <label className="font-medium p-4  text-gray-700 select-none">
                                            Cash
                                          </label>
                                        </div>

                                        <div className="ml-3 flex items-center p-4 h-5">
                                          <input
                                            id="cash"
                                            name="cash"
                                            type="checkbox"
                                            className="focus:ring-indigo-500 h-4 w-4 text-brand-blue rounded-full border-0 bg-gray-300"
                                            onClick={(e) => setCash(!cash)}
                                            defaultChecked={cash}
                                          />
                                        </div>
                                      </div>

                                      <div className="relative flex items-center py-2">
                                        <div className="min-w-0  flex-1 text-sm">
                                          <label className="font-medium p-4  text-gray-700 select-none">
                                            Personal
                                          </label>
                                        </div>

                                        <div className="ml-3 flex items-center p-4 h-5">
                                          <input
                                            id="personal"
                                            name="personal"
                                            type="checkbox"
                                            className="focus:ring-indigo-500 h-4 w-4 text-brand-blue rounded-full border-0 bg-gray-300"
                                            onClick={(e) =>
                                              setPersonal(!personal)
                                            }
                                            defaultChecked={personal}
                                          />
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>

                            {/* end for account */}

                            {/* Date filter */}
                            <div className="bg-gray-50 relative shadow sm:rounded-lg">
                              <Switch.Group
                                as="div"
                                className="px-4  py-5 sm:p-6"
                              >
                                <div className="inline-flex justify-between w-full">
                                  <Switch.Label
                                    as="h3"
                                    className="text-lg leading-6 font-medium text-gray-700"
                                  >
                                    Date
                                  </Switch.Label>
                                  <div className="mt-5 sm:mt-0 sm:ml-6 sm:flex-shrink-0 sm:flex sm:items-center">
                                    <Switch
                                      checked={showDate}
                                      onChange={(e) => {
                                        setShowDate(!showDate);
                                        togglebydate();
                                      }}
                                      className={classNames(
                                        showDate
                                          ? "bg-green-600"
                                          : "bg-gray-300",
                                        "relative inline-flex flex-shrink-0 h-6 w-11 border-2 border-transparent rounded-full cursor-pointer transition-colors ease-in-out duration-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                                      )}
                                    >
                                      <span
                                        className={classNames(
                                          showDate
                                            ? "translate-x-5"
                                            : "translate-x-0",
                                          "pointer-events-none relative inline-block h-5 w-5 rounded-full bg-white shadow transform ring-0 transition ease-in-out duration-200"
                                        )}
                                      >
                                        <span
                                          className={classNames(
                                            showDate
                                              ? "opacity-0 ease-out duration-100"
                                              : "opacity-100 ease-in duration-200",
                                            "absolute inset-0 h-full w-full flex items-center justify-center transition-opacity pointer-events-none"
                                          )}
                                          aria-hidden="true"
                                        >
                                          <svg
                                            className="h-3 w-3 text-gray-400"
                                            fill="none"
                                            viewBox="0 0 12 12"
                                          >
                                            <path
                                              d="M4 8l2-2m0 0l2-2M6 6L4 4m2 2l2 2"
                                              stroke="currentColor"
                                              strokeWidth={2}
                                              strokeLinecap="round"
                                              strokeLinejoin="round"
                                            />
                                          </svg>
                                        </span>
                                        <span
                                          className={classNames(
                                            showDate
                                              ? "opacity-100 ease-in duration-200"
                                              : "opacity-0 ease-out duration-100",
                                            "absolute inset-0 h-full w-full flex items-center justify-center transition-opacity pointer-events-none"
                                          )}
                                          aria-hidden="true"
                                        >
                                          <svg
                                            className="h-3 w-3 text-green-600"
                                            fill="currentColor"
                                            viewBox="0 0 12 12"
                                          >
                                            <path d="M3.707 5.293a1 1 0 00-1.414 1.414l1.414-1.414zM5 8l-.707.707a1 1 0 001.414 0L5 8zm4.707-3.293a1 1 0 00-1.414-1.414l1.414 1.414zm-7.414 2l2 2 1.414-1.414-2-2-1.414 1.414zm3.414 2l4-4-1.414-1.414-4 4 1.414 1.414z" />
                                          </svg>
                                        </span>
                                      </span>
                                    </Switch>
                                  </div>
                                </div>
                              </Switch.Group>

                              <div
                                className={classNames(
                                  showDate ? "block" : "hidden",
                                  "collapse px-4 py-5 sm:p-6 border-t border-gray-200"
                                )}
                              >
                                <div className="grid grid-cols-1 mt-0 date-picker bg-white relative border-2 focus:ring-indigo-400 focus:border-indigo-400 border-gray-100 rounded-md">
                                  <div>
                                    <DateTimePicker
                                      range={true}
                                      startDate={startDate}
                                      endDate={endDate}
                                      onDateChange={(val) => {
                                        setDateRange(val);
                                      }}
                                      inputClassName={
                                        "input py-3 px-4 block w-full border-0"
                                      }
                                      maxDate={new Date()}
                                      locale={enGB}
                                    />
                                  </div>
                                </div>
                              </div>
                            </div>
                            {/* end date filter            */}

                            {/* Range filter */}
                            <div className="bg-gray-50 relative shadow sm:rounded-lg">
                              <Switch.Group
                                as="div"
                                className="px-4  py-5 sm:p-6"
                              >
                                <div className="inline-flex justify-between w-full">
                                  <Switch.Label
                                    as="h3"
                                    className="text-lg leading-6 font-medium text-gray-700"
                                  >
                                    Range ($)
                                  </Switch.Label>
                                  <div className="mt-5 sm:mt-0 sm:ml-6 sm:flex-shrink-0 sm:flex sm:items-center">
                                    <Switch
                                      checked={showRange}
                                      onChange={(e) => {
                                        setShowRange(!showRange);
                                        togglebyrange();
                                      }}
                                      className={classNames(
                                        showRange
                                          ? "bg-green-600"
                                          : "bg-gray-300",
                                        "relative inline-flex flex-shrink-0 h-6 w-11 border-2 border-transparent rounded-full cursor-pointer transition-colors ease-in-out duration-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                                      )}
                                    >
                                      <span
                                        className={classNames(
                                          showRange
                                            ? "translate-x-5"
                                            : "translate-x-0",
                                          "pointer-events-none relative inline-block h-5 w-5 rounded-full bg-white shadow transform ring-0 transition ease-in-out duration-200"
                                        )}
                                      >
                                        <span
                                          className={classNames(
                                            showRange
                                              ? "opacity-0 ease-out duration-100"
                                              : "opacity-100 ease-in duration-200",
                                            "absolute inset-0 h-full w-full flex items-center justify-center transition-opacity pointer-events-none"
                                          )}
                                          aria-hidden="true"
                                        >
                                          <svg
                                            className="h-3 w-3 text-gray-400"
                                            fill="none"
                                            viewBox="0 0 12 12"
                                          >
                                            <path
                                              d="M4 8l2-2m0 0l2-2M6 6L4 4m2 2l2 2"
                                              stroke="currentColor"
                                              strokeWidth={2}
                                              strokeLinecap="round"
                                              strokeLinejoin="round"
                                            />
                                          </svg>
                                        </span>
                                        <span
                                          className={classNames(
                                            showRange
                                              ? "opacity-100 ease-in duration-200"
                                              : "opacity-0 ease-out duration-100",
                                            "absolute inset-0 h-full w-full flex items-center justify-center transition-opacity pointer-events-none"
                                          )}
                                          aria-hidden="true"
                                        >
                                          <svg
                                            className="h-3 w-3 text-green-600"
                                            fill="currentColor"
                                            viewBox="0 0 12 12"
                                          >
                                            <path d="M3.707 5.293a1 1 0 00-1.414 1.414l1.414-1.414zM5 8l-.707.707a1 1 0 001.414 0L5 8zm4.707-3.293a1 1 0 00-1.414-1.414l1.414 1.414zm-7.414 2l2 2 1.414-1.414-2-2-1.414 1.414zm3.414 2l4-4-1.414-1.414-4 4 1.414 1.414z" />
                                          </svg>
                                        </span>
                                      </span>
                                    </Switch>
                                  </div>
                                </div>
                              </Switch.Group>

                              <div
                                className={classNames(
                                  showRange ? "block" : "hidden",
                                  "collapse px-4 py-5 sm:p-6 border-t border-gray-200"
                                )}
                              >
                                <div className="grid grid-cols-1 mt-0">
                                  <div>
                                    <div className="flex gap-x-2">
                                      <div className="w-6/12">
                                        <span className="block mb-2">From</span>
                                        <input
                                          type="text"
                                          className="input w-full rounded-md border-2 border-gray-200 py-3 px-4 mr-2 text-sm font-medium bg-white"
                                          onChange={(e) =>
                                            setFrom(e.target.value)
                                          }
                                          value={from}
                                        />
                                      </div>

                                      <div className="w-6/12">
                                        <span className="block mb-2">To</span>
                                        <input
                                          type="text"
                                          className="input w-full rounded-md border-2 border-gray-200 py-3 px-4 mr-2 text-sm font-medium bg-white"
                                          onChange={(e) =>
                                            setTo(e.target.value)
                                          }
                                          value={to}
                                        />
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>

                            {/* end range filter */}
                          </div>
                        </div>
                      </div>
                    </div>

                    <div className="flex-shrink-0  px-4 py-4 flex justify-between">
                      <div>
                        <button
                          type="button"
                          className="ml-4 inline-flex justify-center py-2 px-4 tracking-wider border border-transparent uppercase shadow-sm  font-medium text-sm rounded-full text-white bg-red-600 hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-500"
                          onClick={reset}
                        >
                          Reset filter
                        </button>
                      </div>
                      <div>
                        <button
                          type="button"
                          className="ml-4 inline-flex justify-center py-2 px-4 tracking-wider border border-transparent uppercase shadow-sm  font-medium text-sm rounded-full text-white bg-green-600 hover:bg-green-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-500"
                          onClick={apply}
                        >
                          Apply filter
                        </button>
                      </div>
                    </div>
                  </Form>
                </Formik>
              </div>
            </Transition.Child>
          </div>
        </div>
      </Dialog>
    </Transition.Root>
  );
}
