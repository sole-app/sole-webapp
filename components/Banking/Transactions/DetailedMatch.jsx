import { Fragment, useState, useEffect } from "react";
import { Transition, Switch, Listbox } from "@headlessui/react";
import { ErrorMessage, Field, Formik } from "formik";
import ClientSchema from "../../../schemas/Client.schema";
import DetailedMatchExpenseSchema from "../../../schemas/DetailedMatchExpense.schema";
import { makeStyles } from "@material-ui/core/styles";
import {
  Update as UpdateClient,
  Add as AddClient,
} from "../../../services/api/client.services";
import { Add } from "../../../services/api/asset.services";
import { Add as AddExpense } from "../../../services/api/expense.services";
import { SNACKBAR_TYPE } from "../../../components/Layout/Snackbar";
import store from "../../../store/store";
import { snackbarOpen } from "../../../reducers/slices/snackbarSlice";
import Stack from "@mui/material/Stack";
import Autocomplete from "@mui/material/Autocomplete";
import TextField from "@mui/material/TextField";
import AddressAutoComplete from "../../Address/AddressAutoComplete";
import { FilePond, registerPlugin } from "react-filepond";
import "filepond/dist/filepond.min.css";
import FilePondPluginImageExifOrientation from "filepond-plugin-image-exif-orientation";
import FilePondPluginImagePreview from "filepond-plugin-image-preview";
import "filepond-plugin-image-preview/dist/filepond-plugin-image-preview.css";
import DateTimePicker from "../../DateTimePicker";
import config from "../../../config/config";
import CategoryDropdown from "../../../components/Banking/Transactions/CategoryDropdown";

registerPlugin(FilePondPluginImageExifOrientation, FilePondPluginImagePreview);

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}
const useStyles = makeStyles((theme) => ({
  fControl: {
    marginTop: 0,
    marginBottom: theme.spacing(1),
    width: "100%",
  },
}));

var initialValues = {
  client_id: "",
  name: "",
  email: "",
  mobile_no: "",
  ABN: "",
  website: "",
  address: "",
  business_name: "",
  google_address: false,
};

var initialExpenseValues = {
  client_id: "",
  name: "",
  price: "",
  date: "",
  description: "",
};

export default function DetailedMatch(props) {
  const { matchObj, clients, accountCategories, matchDetailed } = props;
  const classes = useStyles();
  const [gst, setGst] = useState(false);
  const [image, setImages] = useState([]);
  const [accountCategory, setAccountCategory] = useState(accountCategories[0]);

  useEffect(() => {
    if (typeof matchObj.type !== "undefined" && matchObj.type === "debit") {
      initialExpenseValues.price = matchObj.amount;
      initialExpenseValues.date = new Date(matchObj.transaction_date);
      initialExpenseValues.description = matchObj.description;
    }
  }, [matchObj]);

  const handleClientSubmit = async (values, setSubmitting) => {
    const formData = new FormData();
    formData.append("name", values.name);
    formData.append("email", values.email);
    formData.append("mobile_no", values.mobile_no);
    formData.append("website", values.website);
    formData.append("ABN", values.ABN);
    formData.append("business_name", values.business_name);
    formData.append("type", 1);
    formData.append("from_bank_match", 1);

    if (values.google_address) {
      var fullAddress = values.address.toString().split(",");

      if (fullAddress.length > 1 && values.address !== "") {
        const stateSuburb = fullAddress[1].trim().split(" ");

        formData.append("address_line1", fullAddress[0]);
        formData.append("suburb", stateSuburb[0]);
        formData.append("city", stateSuburb[0]);
        formData.append("state", stateSuburb[stateSuburb.length - 1]);
        formData.append("country", "Australia");
      }
    } else {
      formData.append("address_line1", values.address);
      formData.append("suburb", "");
      formData.append("city", "");
      formData.append("state", "");
      formData.append("country", "");
    }

    var result;
    if (values.client_id > 0) {
      formData.append("id", values.client_id);
      result = await UpdateClient(formData);
    } else {
      result = await AddClient(formData);
    }

    if (result.success) {
      setSubmitting(false);
      matchDetailed(result.data.id);
    } else {
      setSubmitting(false);
      store.store.dispatch(
        snackbarOpen({
          type: SNACKBAR_TYPE.SUCCESS,
          message: result.message,
        })
      );
    }
  };

  const handleSubmitExpense = async (values, setSubmitting) => {
    let formattedDate = new Date(values.date);
    let d = formattedDate.getDate();
    let m = formattedDate.getMonth();
    let y = formattedDate.getFullYear();

    const formData = new FormData();
    if (values.client_id > 0) {
      formData.append("client_id", values.client_id ? values.client_id : "");
    } else {
      const formData1 = new FormData();
      formData1.append("from_bank_match", 1);
      formData1.append("name", values.client_name);
      formData1.append("type", 2);
      const result1 = await AddClient(formData1);
      if (result1.success) {
        formData.append("client_id", result1.data.id || "");
      }
    }

    formData.append("date", y + "-" + (m + 1) + "-" + d);
    formData.append("bank_transaction_id", matchObj.bank_transacton_id);
    formData.append("from_bank_match", 1);
    formData.append("price", values.price);
    formData.append("name", values.description);
    formData.append("gst", gst ? 1 : 0);
    formData.append(
      "account_subcategory_id",
      accountCategory.account_subcategory_id
    );
    formData.append("paid_with_cash", 0);
    if (typeof image[0] !== "undefined") {
      formData.append("image", image[0].file);
    }

    let result;
    if (accountCategory.account_category_id === config.expanseCategoryId) {
      formData.append("is_asset", "no");
      result = await AddExpense(formData);
    } else if (accountCategory.account_category_id === config.assetCategoryId) {
      formData.append("date_of_purchase", matchObj.transaction_date);
      formData.append("user_description", "");
      result = await Add(formData);
    }

    if (result.success) {
      matchDetailed();
    }

    setSubmitting(false);
  };

  const getAdd = (address) => {
    var fullAddress = address.address_line1;

    if (
      typeof address.address_line1 === "string" &&
      address.address_line1.length !== 0
    ) {
      fullAddress +=
        address.city != null && address.city != "" ? ", " + address.city : "";
      fullAddress +=
        address.state != null &&
        address.state != "" &&
        address.state != address.city
          ? ", " + address.state
          : "";
      fullAddress +=
        address.country != null &&
        address.country != "" &&
        address.country != address.state
          ? ", " + address.country
          : "";
    }
    return fullAddress != null ? fullAddress : "";
  };

  if (Object.keys(matchObj).length === 0) {
    return null;
  }

  if (matchObj.type === "credit") {
    return (
      <Formik
        initialValues={initialValues}
        validateOnChange={false}
        validateOnBlur={false}
        validationSchema={ClientSchema}
        onSubmit={(values, { setSubmitting }) => {
          handleClientSubmit(values, setSubmitting);
        }}
      >
        {({
          values,
          errors,
          handleChange,
          handleSubmit,
          handleBlur,
          setFieldValue,
          isSubmitting,
        }) => (
          <form
            onSubmit={handleSubmit}
            className="h-full divide-y divide-gray-200 flex flex-col bg-white shadow-xl"
          >
            <div className="flex-1 h-0 pb-8 overflow-y-auto">
              <div className="flex-1 flex flex-col justify-between">
                <div className="px-4 divide-y divide-gray-200 sm:px-6">
                  <div className="border-gray-200 mb-4 pt-6 pb-7 grid gap-y-2 sm:grid-cols-2 sm:gap-x-8">
                    <div>
                      <label
                        htmlFor="company"
                        className="block text-md tracking-wider font-medium text-gray-500"
                      >
                        Name*
                      </label>

                      <div className="mt-1">
                        <Stack spacing={2}>
                          <Autocomplete
                            disablePortal
                            id="name"
                            name="name"
                            value={values.name}
                            onBlur={handleBlur}
                            freeSolo
                            className="p-0"
                            options={clients}
                            getOptionLabel={(option) => {
                              if (typeof option.name !== "undefined") {
                                return option.name;
                              } else if (typeof option !== "undefined") {
                                return option;
                              } else {
                                return "";
                              }
                            }}
                            onChange={(e, value) => {
                              if (
                                typeof value !== "undefined" &&
                                value !== null &&
                                Object.keys(value).length > 0
                              ) {
                                let add = value.address
                                  ? getAdd(value.address)
                                  : "";
                                setFieldValue("client_id", value.client_id);
                                setFieldValue("name", value.name);
                                setFieldValue(
                                  "email",
                                  value.email ? value.email : ""
                                );
                                setFieldValue(
                                  "mobile_no",
                                  typeof value.mobile_no !== "undefined"
                                    ? value.mobile_no
                                    : ""
                                );
                                setFieldValue(
                                  "ABN",
                                  value.ABN ? value.ABN : ""
                                );
                                setFieldValue(
                                  "website",
                                  value.website ? value.website : ""
                                );
                                setFieldValue("address", add);
                                setFieldValue(
                                  "business_name",
                                  value.business_name ? value.business_name : ""
                                );
                              } else {
                                setFieldValue("client_id", "");
                                setFieldValue("name", "");
                                setFieldValue("email", "");
                                setFieldValue("mobile_no", "");
                                setFieldValue("ABN", "");
                                setFieldValue("website", "");
                                setFieldValue("address", "");
                                setFieldValue("business_name", "");
                              }
                            }}
                            renderInput={(params) => (
                              <TextField
                                {...params}
                                onChange={(e) => {
                                  setFieldValue("client_id", 0);
                                  setFieldValue("name", e.target.value);
                                  setFieldValue("email", "");
                                  setFieldValue("mobile_no", "");
                                  setFieldValue("ABN", "");
                                  setFieldValue("website", "");
                                  setFieldValue("address", "");
                                  setFieldValue("business_name", "");
                                }}
                                className="p-0 border-2 border-gray-200 form-control"
                              />
                            )}
                          />
                        </Stack>
                      </div>
                      <ErrorMessage
                        name="name"
                        component="span"
                        className="error_text"
                      />
                    </div>

                    <div>
                      <label
                        htmlFor="first-name"
                        className="block text-md tracking-wider font-medium text-gray-500"
                      >
                        Email*
                      </label>
                      <div className="mt-1">
                        <input
                          variant="outlined"
                          value={values.email}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          type="email"
                          name="email"
                          id="email"
                          placeholder="Enter a valid email address"
                          className={
                            "placeholder-gray-400 py-3 px-4 block w-full border-2 border-gray-200  focus:ring-indigo-400 focus:border-indigo-400 border-gray-100 rounded-md "
                          }
                        />
                        <ErrorMessage
                          name="email"
                          component="span"
                          className="error_text"
                        />
                      </div>
                    </div>

                    <div>
                      <label
                        htmlFor="last-name"
                        className="block text-md tracking-wider font-medium text-gray-500"
                      >
                        Phone Number
                      </label>
                      <div className="mt-1">
                        <div className="flex relative">
                          <span className="inline-flex items-center px-3 rounded-l-md border-2 border-r-0 border-gray-300 bg-gray-50 text-gray-500 sm:text-md">
                            +61
                          </span>
                          <input
                            variant="outlined"
                            value={values.mobile_no}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            type="text"
                            name="mobile_no"
                            id="mobile_no"
                            placeholder="Enter a valid phone number"
                            className="flex-1 min-w-0 block py-3 px-4 block w-full border-2 border-gray-200  focus:ring-indigo-400 focus:border-indigo-400 border-gray-100 rounded-none rounded-r-md"
                          />
                        </div>
                        <ErrorMessage
                          name="mobile_no"
                          component="span"
                          className="error_text"
                        />
                      </div>
                    </div>

                    <div>
                      <div>
                        <label
                          htmlFor="last-name"
                          className="block text-md tracking-wider font-medium text-gray-500"
                        >
                          Address
                        </label>
                        <Field
                          name="address"
                          component={AddressAutoComplete}
                          value={values.address}
                          type="A"
                        />
                        <ErrorMessage
                          name="address"
                          component="span"
                          className="error_text"
                        />
                      </div>
                    </div>

                    <div>
                      <label className="block text-md tracking-wider font-medium text-gray-500">
                        Website
                      </label>
                      <div className="mt-1 relative">
                        <input
                          variant="outlined"
                          value={values.website}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          type="text"
                          name="website"
                          id="website"
                          placeholder="Enter a valid website url"
                          className={
                            "placeholder-gray-400 py-3 px-4 block w-full border-2 border-gray-200  focus:ring-indigo-400 focus:border-indigo-400 border-gray-100 rounded-md " +
                            classes.fControl
                          }
                        />
                        <ErrorMessage
                          name="website"
                          component="span"
                          className="error_text"
                        />
                      </div>
                    </div>

                    <div>
                      <label className="block text-md tracking-wider font-medium text-gray-500">
                        ABN
                      </label>
                      <div className="mt-1 relative">
                        <input
                          variant="outlined"
                          value={values.ABN}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          type="text"
                          name="ABN"
                          id="ABN"
                          placeholder="Enter your ABN (11 digits, no spaces)"
                          className={
                            "placeholder-gray-400 py-3 px-4 block w-full border-2 border-gray-200  focus:ring-indigo-400 focus:border-indigo-400 border-gray-100 rounded-md " +
                            classes.fControl
                          }
                        />
                        <ErrorMessage
                          name="ABN"
                          component="span"
                          className="error_text"
                        />
                      </div>
                    </div>

                    <div className="col-span-2">
                      <label
                        htmlFor="last-name"
                        className="block text-md tracking-wider font-medium text-gray-500"
                      >
                        Business Name
                      </label>
                      <div className="mt-1 relative">
                        <input
                          variant="outlined"
                          value={values.business_name}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          type="text"
                          name="business_name"
                          id="business_name"
                          placeholder="Enter your contact business name"
                          className={
                            "placeholder-gray-400 py-3 px-4 block w-full border-2 border-gray-200  focus:ring-indigo-400 focus:border-indigo-400 border-gray-100 rounded-md "
                          }
                        />
                        <ErrorMessage
                          name="business_name"
                          component="span"
                          className="error_text"
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {/* END */}

            <div className="flex-shrink-0  px-4 py-4 flex justify-between">
              <p className=" w-96 font-normal text-sm text-gray-500"></p>

              <div>
                <button
                  type="submit"
                  className="ml-4 inline-flex justify-center py-2 px-4 tracking-wider border border-transparent uppercase shadow-sm  font-medium text-sm rounded-full text-white bg-green-600 hover:bg-green-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-500"
                  disabled={isSubmitting}
                >
                  {isSubmitting ? "Please wait..." : "Save"}
                </button>
              </div>
            </div>
          </form>
        )}
      </Formik>
    );
  }

  return (
    <Formik
      initialValues={initialExpenseValues}
      validateOnChange={false}
      validateOnBlur={false}
      validationSchema={DetailedMatchExpenseSchema}
      onSubmit={(values, { setSubmitting }) => {
        handleSubmitExpense(values, setSubmitting);
      }}
    >
      {({
        values,
        errors,
        handleChange,
        handleSubmit,
        handleBlur,
        setFieldValue,
        isSubmitting,
      }) => (
        <form
          onSubmit={handleSubmit}
          className="h-full divide-y divide-gray-200 flex flex-col bg-white shadow-xl"
        >
          <div className="flex-1 h-0 pb-8 overflow-y-auto">
            <div className="flex-1 flex flex-col justify-between">
              <div className="px-4 divide-y divide-gray-200 sm:px-6">
                <div className="border-gray-200 mb-4 pt-6 pb-7 grid gap-y-2 sm:grid-cols-2 sm:gap-x-8">
                  <div className="App filepond sm:col-span-2">
                    <FilePond
                      files={image}
                      onupdatefiles={setImages}
                      allowMultiple={false}
                      maxFiles={1}
                      allowFileTypeValidation={true}
                      acceptedFileTypes={["image/png", "image/jpeg"]}
                      allowFileSizeValidation={true}
                      maxFileSize={1}
                      labelMaxFileSizeExceeded={"File is too large"}
                      name="files"
                      className="App sm:col-span-2"
                      labelIdle='<strong>Add photo of your receipt</strong> <br> Drag & Drop your file or <span class="filepond--label-action">Browse</span>'
                    />
                  </div>

                  <div className="sm:col-span-2">
                    <label
                      htmlFor="first-name"
                      className="inline-flex items-center text-md tracking-wider font-medium text-gray-500"
                    >
                      Category*
                    </label>
                    <CategoryDropdown
                      categories={accountCategories}
                      category={accountCategory}
                      setCategory={setAccountCategory}
                    />
                  </div>

                  <div>
                    <label
                      htmlFor="company"
                      className="block text-md tracking-wider font-medium text-gray-500"
                    >
                      Expense/Asset Description*
                    </label>
                    <div className="mt-1">
                      <input
                        variant="outlined"
                        value={values.description}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        type="text"
                        name="description"
                        id="description"
                        placeholder=""
                        className={
                          "placeholder-gray-400 py-3 px-4 block w-full border-2 border-gray-200  focus:ring-indigo-400 focus:border-indigo-400 border-gray-100 rounded-md " +
                          classes.fControl
                        }
                      />
                      <ErrorMessage
                        name="description"
                        component="span"
                        className="error_text"
                      />
                    </div>
                  </div>

                  <div>
                    <label
                      htmlFor="company"
                      className="block text-md tracking-wider font-medium text-gray-500"
                    >
                      Price*
                    </label>
                    <div className="mt-1">
                      <input
                        variant="outlined"
                        value={values.price}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        type="number"
                        name="price"
                        id="price"
                        placeholder=""
                        className={
                          "placeholder-gray-400 py-3 px-4 block w-full border-2 border-gray-200  focus:ring-indigo-400 focus:border-indigo-400 border-gray-100 rounded-md " +
                          classes.fControl
                        }
                      />
                      <ErrorMessage
                        name="price"
                        component="span"
                        className="error_text"
                      />
                    </div>
                  </div>

                  <div>
                    <label
                      htmlFor="last-name"
                      className="block text-md tracking-wider font-medium text-gray-500"
                    >
                      Date*
                    </label>
                    <div className="mt-1 relative border-2 focus:ring-indigo-400 focus:border-indigo-400 border-gray-100 rounded-md">
                      <DateTimePicker
                        date={values.date}
                        onDateChange={(val) => {
                          setFieldValue("date", val);
                        }}
                        name="date"
                        id="date"
                        inputClassName={
                          "input py-3 px-4 block w-full border-0"
                        }
                        placeholder=""
                      />
                      <ErrorMessage
                        name="date"
                        component="span"
                        className="error_text"
                      />
                    </div>
                  </div>

                  <div>
                    <label
                      htmlFor="first-name"
                      className="inline-flex items-center text-md tracking-wider font-medium text-gray-500"
                    >
                      Supplier*
                    </label>
                    <div className="mt-1">
                      <Stack spacing={2}>
                        <Autocomplete
                          disablePortal
                          id="name"
                          name="name"
                          value={values.name}
                          onBlur={handleBlur}
                          freeSolo
                          className="p-0 form-control"
                          options={clients}
                          getOptionLabel={(option) => {
                            if (typeof option.name !== "undefined") {
                              return option.name;
                            } else if (typeof option !== "undefined") {
                              return option;
                            } else {
                              return "";
                            }
                          }}
                          onChange={(e, value) => {
                            if (
                              typeof value !== "undefined" &&
                              value !== null &&
                              Object.keys(value).length > 0
                            ) {
                              setFieldValue("client_id", value.client_id);
                              setFieldValue("name", value.name);
                            } else {
                              setFieldValue("client_id", "");
                              setFieldValue("name", "");
                            }
                          }}
                          renderInput={(params) => (
                            <TextField
                              {...params}
                              onChange={(e) => {
                                setFieldValue("client_id", 0);
                                setFieldValue("name", e.target.value);
                              }}
                            />
                          )}
                        />
                      </Stack>
                    </div>
                    <ErrorMessage
                      name="name"
                      component="span"
                      className="error_text"
                    />
                  </div>

                  <div>
                    <label
                      htmlFor="company"
                      className="block text-md tracking-wider font-medium text-gray-500"
                    >
                      Include GST (10%)
                    </label>
                    <div className="mt-1">
                      <Switch
                        checked={gst}
                        onChange={setGst}
                        className={classNames(
                          gst ? "bg-green-600" : "bg-gray-300",
                          "relative inline-flex flex-shrink-0 h-6 w-11 border-2 border-transparent rounded-full cursor-pointer transition-colors ease-in-out duration-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                        )}
                      >
                        <span className="sr-only">GST Enable/Disable</span>
                        <span
                          className={classNames(
                            gst ? "translate-x-5" : "translate-x-0",
                            "pointer-events-none relative inline-block h-5 w-5 rounded-full bg-white shadow transform ring-0 transition ease-in-out duration-200"
                          )}
                        >
                          <span
                            className={classNames(
                              gst
                                ? "opacity-0 ease-out duration-100"
                                : "opacity-100 ease-in duration-200",
                              "absolute inset-0 h-full w-full flex items-center justify-center transition-opacity pointer-events-none"
                            )}
                            aria-hidden="true"
                          >
                            <svg
                              className="h-3 w-3 text-gray-400"
                              fill="none"
                              viewBox="0 0 12 12"
                            >
                              <path
                                d="M4 8l2-2m0 0l2-2M6 6L4 4m2 2l2 2"
                                stroke="currentColor"
                                strokeWidth={2}
                                strokeLinecap="round"
                                strokeLinejoin="round"
                              />
                            </svg>
                          </span>
                          <span
                            className={classNames(
                              gst
                                ? "opacity-100 ease-in duration-200"
                                : "opacity-0 ease-out duration-100",
                              "absolute inset-0 h-full w-full flex items-center justify-center transition-opacity pointer-events-none"
                            )}
                            aria-hidden="true"
                          >
                            <svg
                              className="h-3 w-3 text-green-600"
                              fill="currentColor"
                              viewBox="0 0 12 12"
                            >
                              <path d="M3.707 5.293a1 1 0 00-1.414 1.414l1.414-1.414zM5 8l-.707.707a1 1 0 001.414 0L5 8zm4.707-3.293a1 1 0 00-1.414-1.414l1.414 1.414zm-7.414 2l2 2 1.414-1.414-2-2-1.414 1.414zm3.414 2l4-4-1.414-1.414-4 4 1.414 1.414z" />
                            </svg>
                          </span>
                        </span>
                      </Switch>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="flex-shrink-0  px-4 py-4 flex justify-between">
            <p className=" w-96 font-normal text-sm text-gray-500"></p>

            <div>
              <button
                type="submit"
                className="ml-4 inline-flex justify-center py-2 px-4 tracking-wider border border-transparent uppercase shadow-sm  font-medium text-sm rounded-full text-white bg-green-600 hover:bg-green-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-500"
                disabled={isSubmitting}
              >
                {isSubmitting ? "Please wait..." : "Save"}
              </button>
            </div>
          </div>
        </form>
      )}
    </Formik>
  );
}
