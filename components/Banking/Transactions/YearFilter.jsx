import { Fragment } from 'react'
import { Listbox, Transition } from '@headlessui/react'
import { CheckIcon, SelectorIcon } from '@heroicons/react/solid';

function classNames(...classes) {
    return classes.filter(Boolean).join(' ')
}

export default function YearFilter(props) {
    const {financialYear, setYear, yearList} = props;

    return (
        <Listbox 
            style={{marginRight: '10px'}}
            value={financialYear} 
            onChange={setYear}
        >
            {({ open }) => (
                <>
                <div className="mt-1 relative mr-6">
                    <Listbox.Button className="bg-white  relative w-80 border border-gray-300 text-gray-500 rounded-md shadow-sm pl-4 pr-11 py-3 text-left cursor-default focus:outline-none focus:ring-1 focus:ring-indigo-500 focus:border-indigo-500 sm:text-base">
                    <span className="block truncate">{financialYear}</span>
                    <span className="absolute inset-y-0 right-0 flex items-center pr-2 pointer-events-none">
                        <SelectorIcon className="h-5 w-5  text-gray-400" aria-hidden="true" />
                    </span>
                    </Listbox.Button>

                    <Transition
                        show={open}
                        as={Fragment}
                        leave="transition ease-in duration-100"
                        leaveFrom="opacity-100"
                        leaveTo="opacity-0"
                    >
                    <Listbox.Options
                        static
                        className="absolute z-10 mt-1 w-full bg-white shadow-lg max-h-60 rounded-md py-1 text-base ring-1 ring-black ring-opacity-5 overflow-auto focus:outline-none sm:text-md"
                    >
                        {yearList.map((year) => (
                            <Listbox.Option
                                key={year}
                                className={({ active }) =>
                                    classNames(
                                        active ? 'text-white bg-brand-blue' : 'text-gray-900',
                                        'cursor-default select-none relative py-2 pl-3 pr-9'
                                    )
                                }
                                value={year}
                            >
                                {({ selected, active }) => (
                                <>
                                    <span className={classNames(selected ? 'font-semibold' : 'font-normal', 'block truncate')}>
                                    {year}
                                    </span>

                                    {selected ? (
                                    <span
                                        className={classNames(
                                        active ? 'text-white' : 'text-brand-blue',
                                        'absolute inset-y-0 right-0 flex items-center pr-4'
                                        )}
                                    >
                                        <CheckIcon className="h-5 w-5" aria-hidden="true" />
                                    </span>
                                    ) : null}
                                </>
                                )}
                            </Listbox.Option>
                        ))}
                    </Listbox.Options>
                    </Transition>
                </div>
                </>
            )}
        </Listbox>
    )
}
