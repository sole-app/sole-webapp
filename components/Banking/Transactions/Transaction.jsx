import { IconContext } from "react-icons";
import { MdCallMade, MdCallReceived } from "react-icons/md";
import {formatDate, formatCurrency} from "../../../helpers/common";
import config from "../../../config/config";
import PossibleMatch from "../../../components/Banking/Transactions/PossibleMatch";
import { useSelector } from 'react-redux';

export default function Transaction(props) {
    const {
        type,
        transaction,
        openMatch,
        handleConfirmationOpen
    } = props;

    const primaryAccount = useSelector(state => state.bankTransaction.primaryAccount)
    const highlightPrimary = useSelector(state => state.bankTransaction.highlightPrimary)

    const isPrimaryTransaction = () => {
        if (highlightPrimary && primaryAccount > 0) {
            if (transaction.account_id == primaryAccount) {
                return 'primary-account-transaction'
            }
        }
        return ''
    };

    if (type === 'Expense') {
        //for sole transaction
        return (
            <tr key={transaction.bank_transacton_id} className={isPrimaryTransaction()}>

                <td className="px-6 py-4 whitespace-nowrap">
                    <div className="flex items-center">
                        <div className="text-md font-medium text-gray-900">
                            {formatDate(transaction.date)}
                        </div>
                    </div>
                </td>

                <td className="px-6 py-4">
                    <div className=" items-center">
                        {(typeof transaction.account_subcategory[0] !== 'undefined' && typeof transaction.account_subcategory[0].account_subcategory_id !== 'undefined' && transaction.account_subcategory[0].account_subcategory_id === config.personalCategoryId)
                            ?
                            <div className=" whitespace-nowrap ">
                                <span className="inline-flex border border-gray-400 items-center px-2.5 py-0.5 rounded-full text-sm font-medium bg-gray-50 text-gray-600">{transaction.account_subcategory[0].name}</span>
                            </div>
                            :
                            <div className="mt-2  whitespace-nowrap ">
                                <span className="border border-red-600 items-center px-2.5 py-0.5 rounded-full text-sm font-medium bg-gray-50 text-gray-600"> {transaction.account_subcategory[0].name}</span>
                            </div>
                        }

                        <div className="inline-flex  mt-2  border whitespace-nowrap  items-center px-2.5 py-0.5 rounded-full text-sm font-medium bg-blue-100 text-brand-darkblue">
                            Paid with Cash
                        </div>
                    </div>
                </td>

                <td className="px-6 py-4 min-w-96">
                    <div className="flex items-center">
                        <div className="text-md font-medium text-gray-900">
                            {transaction.name}
                        </div>
                    </div>
                </td>
                <td className="px-6 py-4 whitespace-nowrap">
                    <div className="flex items-center mr-4">
                        <div className="text-md font-medium text-gray-900">
                            <span className="inline-flex items-center px-2.5 pt-1 py-0.5 rounded-full text-xs font-medium bg-gray-100 text-gray-600">
                                <IconContext.Provider value={{ className: "global-class-name text-gray-600 mr-1 -ml-0.5" }}>
                                    <div><MdCallMade /></div>
                                </IconContext.Provider> Debit
                            </span>
                        </div>
                    </div>
                </td>

                <td className="px-6 py-4 whitespace-nowrap">
                    <div className="flex items-center">


                        <div className="text-md  font-medium text-gray-800">
                            {formatCurrency(transaction.price)}
                        </div>
                    </div>
                </td>

                <td className="px-7 py-4 whitespace-nowrap text-right text-xsm font-medium">
                    <span
                        className="ml-4 flex-shrink-0 flex items-center justify-end space-x-4">
                        <button
                            type="button"
                            className="rounded-md font-medium text-brand-blue hover:text-gray-600 focus:outline-none focus:ring-2"
                            onClick={() => openMatch(transaction, 'EDITEXPANSE')}
                        >
                            Edit
                        </button>
                    </span>
                </td>
            </tr>
        )
    } else if (type === 'Asset') {
        //for sole transaction
        return (
            <tr key={transaction.bank_transacton_id} className={isPrimaryTransaction()}>

                <td className="px-6 py-4 whitespace-nowrap">
                    <div className="flex items-center">
                        <div className="text-md font-medium text-gray-900">
                            {formatDate(transaction.sole_transaction_date)}
                        </div>
                    </div>
                </td>

                <td className="px-6 py-4">
                    <div className=" items-center">
                        {(typeof transaction.account_subcategory[0] !== 'undefined' && typeof transaction.account_subcategory[0].account_subcategory_id !== 'undefined' && transaction.account_subcategory[0].account_subcategory_id === config.personalCategoryId)
                            ?
                            <div className=" whitespace-nowrap ">
                                <span className="inline-flex border border-gray-400 items-center px-2.5 py-0.5 rounded-full text-sm font-medium bg-gray-50 text-gray-600">{transaction.account_subcategory[0].name}</span>
                            </div>
                            :
                            <div className="mt-2  whitespace-nowrap ">
                                <span className="border border-red-600 items-center px-2.5 py-0.5 rounded-full text-sm font-medium bg-gray-50 text-gray-600"> {transaction.account_subcategory[0].name}</span>
                            </div>
                        }

                        <div className="inline-flex  mt-2  border whitespace-nowrap  items-center px-2.5 py-0.5 rounded-full text-sm font-medium bg-blue-100 text-brand-darkblue">
                            Paid with Cash
                        </div>
                    </div>
                </td>

                <td className="px-6 py-4 min-w-96">
                    <div className="flex items-center">
                        <div className="text-md font-medium text-gray-900">
                            {transaction.name}
                        </div>
                    </div>
                </td>
                <td className="px-6 py-4 whitespace-nowrap">
                    <div className="flex items-center mr-4">
                        <div className="text-md font-medium text-gray-900">
                            <span className="inline-flex items-center px-2.5 pt-1 py-0.5 rounded-full text-xs font-medium bg-gray-100 text-gray-600">
                                <IconContext.Provider value={{ className: "global-class-name text-gray-600 mr-1 -ml-0.5" }}>
                                    <div><MdCallMade /></div>
                                </IconContext.Provider> Debit
                            </span>
                        </div>
                    </div>
                </td>

                <td className="px-6 py-4 whitespace-nowrap">
                    <div className="flex items-center">


                        <div className="text-md  font-medium text-gray-800">
                            {formatCurrency(transaction.price)}
                        </div>
                    </div>
                </td>

                <td className="px-7 py-4 whitespace-nowrap text-right text-xsm font-medium">
                    <span
                        className="ml-4 flex-shrink-0 flex items-center justify-end space-x-4">
                        <button
                            type="button"
                            className="rounded-md font-medium text-brand-blue hover:text-gray-600 focus:outline-none focus:ring-2"
                            onClick={() => openMatch(transaction, 'EDITASSET')}
                        >
                            Edit
                        </button>
                    </span>
                </td>
            </tr>
        )
    } else if (type === 'Invoice') {
        //for sole transaction
        return (
            <tr key={transaction.bank_transacton_id} className={isPrimaryTransaction()}>

                <td className="px-6 py-4 whitespace-nowrap">
                    <div className="flex items-center">
                        <div className="text-md font-medium text-gray-900">
                            {formatDate(transaction.invoice_date)}
                        </div>
                    </div>
                </td>

                <td className="px-6 py-4 whitespace-nowrap">
                    <div className="flex items-center gap-1">
                        <div className="inline-flex border border-gray-400 items-center px-2.5 py-0.5 rounded-full text-sm font-medium bg-gray-50 text-gray-600">
                            Paid by Cash
                        </div>
                    </div>
                </td>

                <td className="px-6 py-4 w-96">
                    <div className="flex items-center">
                        <div className="text-md font-medium text-gray-900">
                            {transaction.description}
                        </div>
                    </div>
                </td>

                <td className="px-6 py-4 whitespace-nowrap">
                    <div className="flex items-center mr-4">
                        <div className="text-md font-medium text-gray-900">
                            <span className="inline-flex items-center px-2.5 pt-1 py-0.5 rounded-full text-xs font-medium bg-gray-50 text-gray-600">
                                <IconContext.Provider value={{ className: "global-class-name text-gray-600 mr-1 -ml-0.5" }}>
                                    <div><MdCallReceived /></div>
                                </IconContext.Provider> Credit
                            </span>
                        </div>
                    </div>
                </td>

                <td className="px-6 py-4 whitespace-nowrap">
                    <div className="flex items-center">


                        <div className="text-md  font-medium text-gray-800">
                            {formatCurrency(transaction.total)}
                        </div>
                    </div>
                </td>

                <td className="px-7 py-4 whitespace-nowrap text-right text-xsm font-medium">
                    <span
                        className="ml-4 flex-shrink-0 flex items-center justify-end space-x-4">
                        <button
                            type="button"
                            className="rounded-md font-medium text-brand-blue hover:text-gray-600  focus:outline-none"
                            onClick={() => handleConfirmationOpen(transaction.id, 'UNDOCASHINVOICE')}
                        >
                            Undo
                        </button>
                    </span>
                </td>
            </tr>
        )
    } else if (type === 'BankTransaction') {
        //for sole transaction
        return (
            <tr key={transaction.bank_transacton_id} className={isPrimaryTransaction()}>

                <td className="px-6 py-4 whitespace-nowrap">
                    <div className="flex items-center">
                        <div className="text-md font-medium text-gray-900">
                            {formatDate(transaction.transaction_date)}
                        </div>
                    </div>
                </td>

                <td className="px-6 py-4">
                    <div className="flex items-center gap-1">
                        {(typeof transaction.account_subcategory[0] !== 'undefined' && typeof transaction.account_subcategory[0].account_subcategory_id !== 'undefined')
                            ?
                            <>
                                {transaction.account_subcategory[0].account_subcategory_id === config.personalCategoryId
                                    ?
                                    <div className="inline-flex border whitespace-nowrap border-gray-400 items-center px-2.5 py-0.5 rounded-full text-sm font-medium bg-gray-50 text-gray-600">
                                        {transaction.account_subcategory[0].name}
                                    </div>
                                    :
                                    <>
                                        {transaction.type === 'credit'
                                            ?
                                            <div className="inline-flex border  whitespace-nowrap border-green-600 items-center px-2.5 py-0.5 rounded-full text-sm font-medium bg-gray-50 text-gray-600">
                                                {transaction.account_subcategory[0].name}
                                            </div>
                                            :
                                            (typeof transaction.account_subcategory !== 'undefined' && transaction.account_subcategory[0] !== null)
                                                ?
                                                <div className="  whitespace-nowrap ">
                                                    <span className="border inline-flex border-red-600 items-center px-2.5 py-0.5 rounded-full text-sm font-medium bg-gray-50 text-gray-600">{transaction.account_subcategory[0].name || ''}</span>
                                                </div>
                                                : ''
                                        }
                                    </>
                                }
                            </>
                            :
                            ''
                        }
                    </div>
                </td>

                <td className="px-6 py-4  min-w-96">
                    <div className="flex items-center">
                        <div className="text-md font-medium text-gray-900">
                            {transaction.description}
                        </div>
                    </div>
                </td>

                <td className="px-6 py-4 whitespace-nowrap">
                    <div className="flex items-center">
                        <div className="flex items-center mr-4">
                            <div className="text-md font-medium text-gray-900">
                                {transaction.type === 'credit'
                                    ?
                                    <>
                                        {(typeof transaction.account_subcategory[0] !== 'undefined' && typeof transaction.account_subcategory[0].account_subcategory_id !== 'undefined' && transaction.account_subcategory[0].account_subcategory_id === config.personalCategoryId)
                                            ?
                                            <span className="inline-flex items-center px-2.5 pt-1 py-0.5 rounded-full text-xs font-medium bg-gray-50 text-gray-600">
                                                <IconContext.Provider value={{ className: "global-class-name text-gray-600 mr-1 -ml-0.5" }}>
                                                    <div><MdCallReceived /></div>
                                                </IconContext.Provider> Credit
                                            </span>
                                            :
                                            <span className="inline-flex items-center px-2.5 pt-1 py-0.5 rounded-full text-xs font-medium bg-green-100 text-green-800">
                                                <IconContext.Provider value={{ color: "#4caf50", className: "global-class-name mr-1 -ml-0.5" }}>
                                                    <div>  <MdCallReceived /> </div>
                                                </IconContext.Provider>  Credit
                                            </span>
                                        }
                                    </>
                                    :
                                    <>
                                        {(typeof transaction.account_subcategory[0] !== 'undefined' && typeof transaction.account_subcategory[0].account_subcategory_id !== 'undefined' && transaction.account_subcategory[0].account_subcategory_id === config.personalCategoryId)
                                            ?
                                            <span className="inline-flex items-center px-2.5 pt-1 py-0.5 rounded-full text-xs font-medium bg-gray-50 text-gray-600">
                                                <IconContext.Provider value={{ className: "global-class-name text-gray-600 mr-1 -ml-0.5" }}>
                                                    <div><MdCallMade /></div>
                                                </IconContext.Provider> Debit
                                            </span>
                                            :
                                            <span className="inline-flex items-center px-2.5 pt-1 py-0.5 rounded-full text-xs font-medium bg-red-100 text-red-800">
                                                <IconContext.Provider value={{ color: "#e53935", className: "global-class-name mr-1 -ml-0.5" }}>
                                                    <div><MdCallMade /></div>
                                                </IconContext.Provider> Debit
                                            </span>
                                        }
                                    </>
                                }
                            </div>
                        </div>
                    </div>
                </td>

                <td className="px-6 py-4 whitespace-nowrap">
                    <div className="flex items-center">
                        <div className="text-md  font-medium text-gray-800">
                            {formatCurrency(transaction.amount)}
                        </div>
                    </div>
                </td>

                <td className="px-7 py-4 whitespace-nowrap text-right text-xsm font-medium">
                    <span
                        className="ml-4 flex-shrink-0 flex items-center justify-end space-x-4">
                        {(typeof transaction.account_subcategory[0] !== 'undefined' && typeof transaction.account_subcategory[0].account_subcategory_id !== 'undefined' && transaction.account_subcategory[0].account_subcategory_id !== config.personalCategoryId)
                            ?
                            <>
                                <button
                                    type="button"
                                    className="rounded-md font-medium text-brand-blue hover:text-gray-600 focus:outline-none focus:ring-2"
                                    onClick={() => openMatch(transaction, 'EDIT')}
                                >
                                    Edit
                                </button>
                                <span className="text-gray-300" aria-hidden="true">
                                    |
                                </span>
                            </>
                            :
                            ''
                        }
                        <button
                            type="button"
                            className="rounded-md font-medium text-brand-blue hover:text-gray-600  focus:outline-none"
                            onClick={() => handleConfirmationOpen(transaction.bank_transacton_id, 'UNDO')}
                        >
                            Undo
                        </button>
                    </span>
                </td>
            </tr>
        )
    } else {
        //for bank transaction
        return (
            <tr key={transaction.bank_transacton_id} className={isPrimaryTransaction()}>

                <td className="px-6 py-4 whitespace-nowrap">
                    <div className="flex items-center">
                        <div className="text-md  text-gray-900">
                            {formatDate(transaction.transaction_date)}
                        </div>
                    </div>
                </td>

                <td className="px-6 py-4  min-w-96">
                    <div className="flex items-center">
                        <div className="text-md text-gray-900">
                            {transaction.description}

                            <PossibleMatch transaction={transaction} openMatch={openMatch} />
                        </div>
                    </div>
                </td>

                <td className="px-6 py-4 w-96 whitespace-nowrap">
                    <div className="flex items-center">
                        <div className="flex items-center mr-4">
                            <div className="text-md text-gray-900">
                                {transaction.type === 'credit'
                                    ?
                                    <>
                                        {(typeof transaction.account_subcategory[0] !== 'undefined' && typeof transaction.account_subcategory[0].account_subcategory_id !== 'undefined' && transaction.account_subcategory[0].account_subcategory_id === config.personalCategoryId)
                                            ?
                                            <span className="inline-flex items-center px-2.5 pt-1 py-0.5 rounded-full text-xs font-medium bg-gray-50 text-gray-600">
                                                <IconContext.Provider value={{ className: "global-class-name text-gray-600 mr-1 -ml-0.5" }}>
                                                    <div><MdCallReceived /></div>
                                                </IconContext.Provider> Credit
                                            </span>
                                            :
                                            <span className="inline-flex items-center px-2.5 pt-1 py-0.5 rounded-full text-xs font-medium bg-green-100 text-green-800">
                                                <IconContext.Provider value={{ color: "#4caf50", className: "global-class-name mr-1 -ml-0.5" }}>
                                                    <div>  <MdCallReceived /> </div>
                                                </IconContext.Provider>  Credit
                                            </span>
                                        }
                                    </>
                                    :
                                    <>
                                        {(typeof transaction.account_subcategory[0] !== 'undefined' && typeof transaction.account_subcategory[0].account_subcategory_id !== 'undefined' && transaction.account_subcategory[0].account_subcategory_id === config.personalCategoryId)
                                            ?
                                            <span className="inline-flex items-center px-2.5 pt-1 py-0.5 rounded-full text-xs font-medium bg-gray-50 text-gray-600">
                                                <IconContext.Provider value={{ className: "global-class-name text-gray-600 mr-1 -ml-0.5" }}>
                                                    <div><MdCallMade /></div>
                                                </IconContext.Provider> Debit
                                            </span>
                                            :
                                            <span className="inline-flex items-center px-2.5 pt-1 py-0.5 rounded-full text-xs font-medium bg-red-100 text-red-800">
                                                <IconContext.Provider value={{ color: "#e53935", className: "global-class-name mr-1 -ml-0.5" }}>
                                                    <div><MdCallMade /></div>
                                                </IconContext.Provider> Debit
                                            </span>
                                        }
                                    </>
                                }
                            </div>
                        </div>
                    </div>
                </td>

                <td className="px-6 py-4 whitespace-nowrap">
                    <div className="flex items-center">
                        <div className="text-md  font-medium text-gray-800">
                            {formatCurrency(transaction.amount)}
                        </div>
                    </div>
                </td>

                <td className="px-7 py-4 whitespace-nowrap text-right text-xsm font-medium">
                    <span
                        className="ml-4 flex-shrink-0 flex items-center justify-end space-x-4">
                        <button
                            type="button"
                            className="rounded-md font-medium text-brand-blue hover:text-gray-600 focus:outline-none focus:ring-2"
                            onClick={() => handleConfirmationOpen(transaction.bank_transacton_id, 'PERSONAL')}
                        >
                            Personal
                        </button>
                        <span className="text-gray-300" aria-hidden="true">
                            |
                        </span>
                        <button
                            type="button"
                            className="rounded-md font-medium text-brand-blue hover:text-gray-600  focus:outline-none"
                            onClick={() => openMatch(transaction)}
                        >
                            Match
                        </button>
                    </span>
                </td>
            </tr>
        )
    }
}
