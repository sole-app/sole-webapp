import {SearchIcon} from '@heroicons/react/outline'
import CircularProgress from '@material-ui/core/CircularProgress'
import bankIcons from "../../config/bankIcons"

export default function InstituteList(props) {
    const {loader, list, filterInstitutes, selectInstitute, reAuthLoader} = props;

    const getImage = (institute) => {
        let image = bankIcons.default;        

        if (institute.slug !== '') {            
            image = `https://test.bankstatements.com.au/images/institutions/${institute.slug}.png`;
        }

        return image;
    };

    return (
        <div className="absolute inset-0 py-6 px-4 sm:px-6">
            <div className="max-w-xl m-auto border p-5 shadow-md border-gray-100"
                 aria-hidden="true">

                {
                    reAuthLoader ?
                        ''
                        :
                        <div>
                            <h2 className='text-3xl font-bold text-gray-700'>Let's connect your bank accounts</h2>
                            <p>Connect your bank accounts to sync your transactions.</p>

                            <div className="mt-4 relative rounded-md">
                                <div
                                    className="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
                                    <SearchIcon className="h-5 w-5 text-gray-500"
                                                aria-hidden="true"/>
                                </div>
                                <input
                                    type="text"
                                    name="search"
                                    id="search"
                                    className="bg-gray-100 focus:ring-indigo-500 focus:border-indigo-500 block w-full pl-10 sm:text-sm p-3 border-gray-300 rounded-md"
                                    placeholder="Enter your bank name"
                                    onChange={(event) => filterInstitutes(event.target.value)}
                                />
                            </div>
                        </div>
                }

                {
                    loader
                        ?
                        <div className="box-center bank-list mt-4 overflow-y-scroll"><CircularProgress/></div>
                        :
                        <div className='bank-list mt-4 h-96 overflow-y-scroll'>
                            {
                                list.map((item, index) => (
                                    <div
                                        key={index}
                                        onClick={() => selectInstitute(item)}
                                        className="relative mt-6 rounded-lg border border-gray-300 bg-white  shadow-sm flex items-center space-x-3 hover:border-gray-400"
                                    >
                                        <div
                                            className="flex items-center bg-gray-50 p-4 rounded-tl-lg rounded-bl-lg h-16 w-32">
                                            <img
                                                src={getImage(item)}
                                                height="100%"
                                                alt="Bank Logo"
                                            />
                                        </div>

                                        <div className="flex-1 pl-2 min-w-0">
                                            <a href="#" className="focus:outline-none">
                                                <span className="absolute inset-0" aria-hidden="true"/>
                                                <p className="text-lg font-semibold text-gray-900">
                                                    {item.name}
                                                </p>
                                            </a>
                                        </div>
                                    </div>
                                ))
                            }
                        </div>
                }
            </div>

            <p className="note text-center flex justify-center py-6 items-center">
                <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6 mr-2"
                     fill="none" viewBox="0 0 24 24" stroke="currentColor"
                     strokeWidth={2}>
                    <path strokeLinecap="round" strokeLinejoin="round"
                          d="M12 15v2m-6 4h12a2 2 0 002-2v-6a2 2 0 00-2-2H6a2 2 0 00-2 2v6a2 2 0 002 2zm10-10V7a4 4 0 00-8 0v4h8z"/>
                </svg>
                At Sole, the privacy and security of your information is our top priority. Sole establishes a secure connection with your bank. <br></br>                
            </p>
            <p className="note text-center flex justify-center items-center">
                <a href='https://soleapp.com.au/privacy/' target='_blank'>Privacy Policy </a>&nbsp;|&nbsp;<a target='_blank' href='https://soleapp.com.au/terms/'>Terms & Conditions</a>
            </p>
        </div>
    )
}
