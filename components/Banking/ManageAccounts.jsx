/* This example requires Tailwind CSS v2.0+ */
import { Fragment, useState, useEffect } from 'react'
import { Dialog, Transition } from '@headlessui/react'
import { XIcon, InformationCircleIcon } from '@heroicons/react/outline'
import { FiTrash2 } from "react-icons/fi";
import {formatCurrency} from "../../helpers/common";
import bankIcons from "../../config/bankIcons"
import ReactTooltip from 'react-tooltip';
import { useSelector } from 'react-redux';
import store from '../../store/store';
import { setHighlightPrimary } from '../../store/actions';
import { Switch } from '@headlessui/react';

function classNames(...classes) {
    return classes.filter(Boolean).join(' ')
}

export default function ManageAccounts(props) {
    const { loader, setOpenSlider, accounts, confirmDelete, makePrimary, open, setOpen, openCloseReAuth } = props;
    const highlightPrimary = useSelector(state => state.bankTransaction.highlightPrimary);
    const [highlight, setHighlight] = useState(highlightPrimary);

    const changeHighList = () => {
        setHighlight(!highlight);
        store.store.dispatch(setHighlightPrimary(!highlight));
    };

    const getImage = (institute) => {
        let image = bankIcons.default;

        if (institute.slug !== '') {
            image = `https://test.bankstatements.com.au/images/institutions/${institute.slug}.png`;
        }

        return image;
    };

    return (
        <Transition.Root show={open} as={Fragment}>
            <Dialog as="div" className="fixed inset-0 overflow-hidden" onClose={setOpen}>
                <div className="absolute inset-0 overflow-hidden">
                    <Dialog.Overlay className="absolute inset-0 bg-gray-500 bg-opacity-75 transition-opacity pointer-events-none" />

                    <div className="pointer-events-none fixed inset-y-0 right-0 flex max-w-full ">
                        <Transition.Child
                            as={Fragment}
                            enter="transform transition ease-in-out duration-500 sm:duration-700"
                            enterFrom="translate-x-full"
                            enterTo="translate-x-0"
                            leave="transform transition ease-in-out duration-500 sm:duration-700"
                            leaveFrom="translate-x-0"
                            leaveTo="translate-x-full"
                        >
                            <div className="pointer-events-auto w-screen lg:max-w-3xl">
                                <div className="flex h-full flex-col overflow-y-scroll bg-white shadow-xl">
                                    <div className="relativeborder-b border-gray-200 bg-gray-100 py-4 px-4 sm:px-6">
                                        <div className="flex items-center justify-between">
                                            <Dialog.Title className="text-xl font-medium text-gray-800">
                                                Manage Bank Accounts

                                            </Dialog.Title>
                                            <div className="ml-3 h-7 flex items-center">
                                                <button
                                                    type="button"
                                                    onClick={() => setOpen(false)}
                                                    className="rounded-full hover:bg-gray-300 p-2 text-gray-500   hover:text-gray-700  focus:outline-none focus:ring-2 focus:ring-white"
                                                >
                                                    <span className="sr-only">Close panel</span>
                                                    <XIcon className="h-6 w-6" aria-hidden="true" />
                                                </button>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="relative  flex-1">
                                        <div
                                            className="inset-0 px-6 py-4 border-b border-gray-200 flex items-center justify-between">
                                            <h3 className='text-lg text-gray-600'>Connected Accounts</h3>
                                                <button type="button"
                                                    className="inline-flex items-center px-4 py-2 border border-transparent text-sm font-medium rounded-full shadow-sm text-white bg-brand-blue hover:bg-brand-darkblue  focus:outline-none focus:ring-2 focus:ring-offset-2 focus:bg-brand-darkblue"
                                                    onClick={() => setOpenSlider(true)}>
                                                    Add Bank Account
                                                </button>                                            
                                        </div>

                                        <div className="border-b border-gray-200 high-ligh-switch">
                                            <div className="space-x-4">
                                                <div>
                                                    <Switch.Group as="div"
                                                        className="px-2  py-3 sm:p-4">
                                                        <div
                                                            className="inline-flex justify-between w-full">
                                                            <Switch.Label as="h3"
                                                                className="text-lg leading-6 font-medium text-gray-700"
                                                                passive>
                                                                Highlight primary account over other account?
                                                            </Switch.Label>
                                                            <div
                                                                className="mt-5 sm:mt-0 sm:ml-6 sm:flex-shrink-0 sm:flex sm:items-center">
                                                                <Switch
                                                                    checked={highlight}
                                                                    onChange={(e) => changeHighList()}
                                                                    className={classNames(
                                                                        highlight ? 'bg-green-600' : 'bg-gray-300',
                                                                        'relative inline-flex flex-shrink-0 h-6 w-11 border-2 border-transparent rounded-full cursor-pointer transition-colors ease-in-out duration-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500'
                                                                    )}
                                                                >
                                                                    <span
                                                                        className={classNames(
                                                                            highlight ? 'translate-x-5' : 'translate-x-0',
                                                                            'pointer-events-none relative inline-block h-5 w-5 rounded-full bg-white shadow transform ring-0 transition ease-in-out duration-200'
                                                                        )}
                                                                    >
                                                                        <span
                                                                            className={classNames(
                                                                                highlight ? 'opacity-0 ease-out duration-100' : 'opacity-100 ease-in duration-200',
                                                                                'absolute inset-0 h-full w-full flex items-center justify-center transition-opacity pointer-events-none'
                                                                            )}
                                                                            aria-hidden="true"
                                                                        >
                                                                            <svg className="h-3 w-3 text-gray-400"
                                                                                fill="none"
                                                                                viewBox="0 0 12 12">
                                                                                <path
                                                                                    d="M4 8l2-2m0 0l2-2M6 6L4 4m2 2l2 2"
                                                                                    stroke="currentColor"
                                                                                    strokeWidth={2}
                                                                                    strokeLinecap="round"
                                                                                    strokeLinejoin="round"
                                                                                />
                                                                            </svg>
                                                                        </span>
                                                                        <span
                                                                            className={classNames(
                                                                                highlight ? 'opacity-100 ease-in duration-200' : 'opacity-0 ease-out duration-100',
                                                                                'absolute inset-0 h-full w-full flex items-center justify-center transition-opacity pointer-events-none'
                                                                            )}
                                                                            aria-hidden="true"
                                                                        >
                                                                            <svg className="h-3 w-3 text-green-600"
                                                                                fill="currentColor"
                                                                                viewBox="0 0 12 12">
                                                                                <path
                                                                                    d="M3.707 5.293a1 1 0 00-1.414 1.414l1.414-1.414zM5 8l-.707.707a1 1 0 001.414 0L5 8zm4.707-3.293a1 1 0 00-1.414-1.414l1.414 1.414zm-7.414 2l2 2 1.414-1.414-2-2-1.414 1.414zm3.414 2l4-4-1.414-1.414-4 4 1.414 1.414z" />
                                                                            </svg>
                                                                        </span>
                                                                    </span>
                                                                </Switch>
                                                            </div>
                                                        </div>

                                                    </Switch.Group>
                                                </div>
                                            </div>
                                        </div>

                                        <div className='px-4 2xl:px-6 mt-6'>

                                            {
                                                accounts.map((account, index) => (
                                                    <div
                                                        key={index}
                                                        className={(index === 0) ? 'border-gray-200 border relative rounded-md shadow-sm flex items-center' : 'border-gray-200 relative border rounded-md mt-5 shadow-sm flex items-center'}
                                                    >
                                                        <div
                                                            className="flex items-center bg-gray-50 p-2 rounded-tl-lg rounded-bl-lg  w-32">
                                                            <img
                                                                src={getImage(account)}
                                                                /*src="/images/banks/default.png"*/
                                                                height="100%"
                                                                alt="Bank Logo"
                                                            />
                                                        </div>

                                                        <div className='flex justify-between w-full items-end relative'>
                                                            <div className='border-l border-gray-200 p-4 '>
                                                                <h5 className='text-lg text-gray-800 font-semibold'>
                                                                    {account.bank_name}
                                                                </h5>
                                                                <span className='block'>{account.account_type_name}</span>
                                                                <span className='block'>ACC:{account.account_number} ({formatCurrency(account.balance)})</span>
                                                            </div>

                                                            {(account.is_primary > 0)
                                                                ?
                                                                <span className="absolute right-3 top-0 w-20 block ml-auto mt-4 mb-auto text-center px-2.5 py-0.5 rounded-full text-xs font-semibold tracking-wider bg-blue-100 text-blue-800">
                                                                    Primary
                                                                </span>
                                                                :
                                                                ''
                                                            }

                                                            <div className='flex items-center relative z-50 py-2'>
                                                                <button
                                                                    className="inline-flex items-center px-3 py-1 border border-transparent  text-sm leading-4  font-medium rounded-md text-blue-700   focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                                                                >
                                                                    {(account.is_primary > 0)
                                                                        ?
                                                                        ''
                                                                        :
                                                                        <>
                                                                            <input
                                                                                id={`primary${index}`}
                                                                                name="primary"
                                                                                type="radio"
                                                                                value='primaryaccount'
                                                                                className="focus:ring-brand-blue h-4 w-4 text-brand-blue border-gray-500"
                                                                                onClick={() => makePrimary(account.id)}
                                                                                checked={(account.is_primary > 0)}
                                                                            />
                                                                            <label htmlFor={`primary${index}`}
                                                                                className="ml-2 block text-sm font-medium text-blue-700 "
                                                                            >
                                                                                Mark as Primary
                                                                            </label>
                                                                        </>
                                                                    }
                                                                </button>

                                                                <button
                                                                    className="inline-flex items-center px-3 py-1 border border-transparent text-sm leading-4  font-medium rounded-md text-red-700  focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                                                                    onClick={() => confirmDelete(account.id)}>
                                                                    <FiTrash2 className="-ml-0.5 mr-2 h-4 w-4"
                                                                        aria-hidden="true" />
                                                                </button>
                                                            </div>
                                                        </div>

                                                        {account.bank_connection.connection_status
                                                            ?
                                                            ''
                                                            :
                                                            <div className='absolute h-full  w-full top-0 left-0'>
                                                                <div className='absolute right-2 top-2 z-20'>
                                                                    <InformationCircleIcon
                                                                        className="h-6 w-6 text-gray-400 ml-2  focus:outline-none"
                                                                        data-for={'info_' + index}
                                                                        data-tip={account.bank_connection.error_message || 'Error'}
                                                                    />

                                                                    <ReactTooltip id={'info_' + index}
                                                                        className='custom-tooltip bg-gray-900'
                                                                        textColor='#ffffff'
                                                                        backgroundColor='#111827'
                                                                        effect='solid' />
                                                                </div>


                                                                <div className="absolute w-full h-full blur bg-white opacity-80" />
                                                                <div className="p-4 relative flex items-center justify-center z-10 h-full">
                                                                    <div className="flex items-center cursor-pointer bg-white py-1 px-4 border border-gray-300 rounded-full">
                                                                        <div className="flex-shrink-0">
                                                                            <svg className="h-5 w-5 text-red-600" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                                                                <path fill-rule="evenodd" d="M8.257 3.099c.765-1.36 2.722-1.36 3.486 0l5.58 9.92c.75 1.334-.213 2.98-1.742 2.98H4.42c-1.53 0-2.493-1.646-1.743-2.98l5.58-9.92zM11 13a1 1 0 11-2 0 1 1 0 012 0zm-1-8a1 1 0 00-1 1v3a1 1 0 002 0V6a1 1 0 00-1-1z" clip-rule="evenodd" />
                                                                            </svg>
                                                                        </div>
                                                                        <div className="ml-3">
                                                                            <p className="text-base font-semibold text-red-500" onClick={() => openCloseReAuth(account)}>
                                                                                Feed disconnected, please re-authenticate
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        }
                                                    </div>
                                                ))
                                            }
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </Transition.Child>
                    </div>
                </div>
            </Dialog>
        </Transition.Root>
    )
}
