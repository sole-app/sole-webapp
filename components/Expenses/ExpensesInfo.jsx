import { Fragment, useState, useEffect } from "react";
import { Dialog, Transition } from "@headlessui/react";
import { XIcon } from "@heroicons/react/outline";
import Skeleton from "react-loading-skeleton";
import { Info } from "../../services/api/expense.services";
import NumberFormat from "react-number-format";

export default function ExpensesInfo(props) {
  const [info, setInfo] = useState("");
  const [loading, setLoading] = useState(true);

  useEffect(async () => {
    if (props.isInfoOpen && props.updateId) {
      const resultInfo = await Info(props.updateId);
      if (resultInfo.success) {
        setInfo(resultInfo.data);
        setLoading(false);
      } else {
        setLoading(false);
      }
    }
  }, [props.isInfoOpen]);

  return (
    <Transition.Root show={props.isInfoOpen} as={Fragment}>
      <Dialog
        as="div"
        static
        className="fixed inset-0 overflow-hidden"
        onClose={props.setIsInfoOpen}
      >
        <div className="absolute inset-0 overflow-hidden">
          <Transition.Child
            as={Fragment}
            enter="ease-in-out duration-500"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in-out duration-500"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <Dialog.Overlay className="absolute inset-0 bg-gray-500 bg-opacity-75 transition-opacity pointer-events-none" />
          </Transition.Child>

          <div className="fixed inset-y-0 max-w-full right-0 flex">
            <Transition.Child
              as={Fragment}
              enter="transform transition ease-in-out duration-500 sm:duration-700"
              enterFrom="translate-x-full"
              enterTo="translate-x-0"
              leave="transform transition ease-in-out duration-500 sm:duration-700"
              leaveFrom="translate-x-0"
              leaveTo="translate-x-full"
            >
              <div className="w-screen lg:max-w-2xl bg-white">
                <div className="flex-1 pb-8 overflow-y-auto">
                  <div className="border-b border-gray-200 bg-gray-100 py-4 px-4 sm:px-6">
                    <div className="flex items-center justify-between">
                      <Dialog.Title className="text-xl font-medium text-gray-800">
                        Expense Information
                      </Dialog.Title>
                      <div className="ml-3 h-7 flex items-center">
                        <button
                          type="button"
                          className="rounded-full hover:bg-gray-300 p-2 text-gray-500 hover:text-gray-700 focus:outline-none focus:ring-2 focus:ring-white"
                          onClick={() => props.setIsInfoOpen(false)}
                        >
                          <span className="sr-only">Close panel</span>
                          <XIcon className="h-6 w-6" aria-hidden="true" />
                        </button>
                      </div>
                    </div>
                  </div>
                  <div className="flex-1 flex flex-col justify-between">
                    <div className="px-4 divide-y divide-gray-200 sm:px-6 h-100vh">
                      <div className="space-y-6 border-gray-200 mb-4 pt-6 pb-7 grid grid-cols-1 gap-y-2 sm:grid-cols-1">
                        {!loading ? (
                          info ? (
                            <>
                              {info.image && info.image != "" ? (
                                <div className="sm:col-span-2">
                                  <img src={info.image}></img>
                                </div>
                              ) : null}

                              <div className="sm:col-span-2">
                                <label className="info-label">Name</label>
                                <p>{info.name}</p>
                              </div>

                              <div className="sm:col-span-2">
                                <label className="info-label">Date</label>
                                <p>{info.date}</p>
                              </div>

                              <div className="sm:col-span-2">
                                <label className="info-label">Category</label>
                                <p>
                                  {info.account_subcategory
                                    ? info.account_subcategory.name
                                    : "-"}
                                </p>
                              </div>

                              <div className="sm:col-span-2">
                                <label className="info-label">Supplier</label>
                                <p>{info.client ? info.client.name : "-"}</p>
                              </div>

                              <div className="sm:col-span-2">
                                <label className="info-label">Sub Total</label>
                                <p>
                                  {info.sub_total ? (
                                    <NumberFormat
                                      value={parseFloat(info.sub_total).toFixed(
                                        2
                                      )}
                                      displayType={"text"}
                                      thousandSeparator={true}
                                      prefix={"$"}
                                      renderText={(value) => <div>{value}</div>}
                                    />
                                  ) : (
                                    "-"
                                  )}
                                </p>
                              </div>

                              <div className="sm:col-span-2">
                                <label className="info-label">GST</label>
                                <p>
                                  {info.total_gst ? (
                                    <NumberFormat
                                      value={parseFloat(info.total_gst).toFixed(
                                        2
                                      )}
                                      displayType={"text"}
                                      thousandSeparator={true}
                                      prefix={"$"}
                                      renderText={(value) => <div>{value}</div>}
                                    />
                                  ) : (
                                    "-"
                                  )}
                                </p>
                              </div>

                              <div className="sm:col-span-2">
                                <label className="info-label">Price</label>
                                <p>
                                  {info.price ? (
                                    <NumberFormat
                                      value={parseFloat(info.price).toFixed(2)}
                                      displayType={"text"}
                                      thousandSeparator={true}
                                      prefix={"$"}
                                      renderText={(value) => <div>{value}</div>}
                                    />
                                  ) : (
                                    "-"
                                  )}
                                </p>
                              </div>

                              <div className="sm:col-span-2">
                                <label className="info-label">
                                  Paid With Cash
                                </label>
                                <p>{info.paid_with_cash ? "Yes" : "No"}</p>
                              </div>
                            </>
                          ) : (
                            <p>No records found.</p>
                          )
                        ) : (
                          <>
                            {Array.from(Array(8), (e, i) => {
                              return (
                                <div key={i} className="sm:col-span-2">
                                  <label className="info-label">
                                    <Skeleton />
                                  </label>
                                  <p>
                                    <Skeleton />
                                  </p>
                                </div>
                              );
                            })}
                          </>
                        )}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </Transition.Child>
          </div>
        </div>
      </Dialog>
    </Transition.Root>
  );
}
