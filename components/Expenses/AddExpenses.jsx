import { Fragment, useState, useEffect } from "react";
import { Dialog, Transition, Switch, Listbox } from "@headlessui/react";
import { XIcon } from "@heroicons/react/outline";
import { ErrorMessage, Form, Formik } from "formik";
import ExpenseSchema from "../../schemas/Expense.schema";
import { makeStyles } from "@material-ui/core/styles";
import {
  Update as UpdateExpense,
  Add as AddExpense,
  Info as InfoExpense,
} from "../../services/api/expense.services";
import { SNACKBAR_TYPE } from "../../components/Layout/Snackbar";
import store from "../../store/store";
import { snackbarOpen } from "../../reducers/slices/snackbarSlice";
import CircularProgress from "@material-ui/core/CircularProgress";
import { CheckIcon, SelectorIcon } from "@heroicons/react/solid";
import FormControlLabel from "@mui/material/FormControlLabel";
import Checkbox from "@mui/material/Checkbox";
import DateTimePicker from "../DateTimePicker";
import Stack from "@mui/material/Stack";
import Autocomplete, { createFilterOptions } from "@mui/material/Autocomplete";
import TextField from "@mui/material/TextField";
import {
  Add as AddClient,
  List as ListClient,
} from "../../services/api/client.services";
import { FilePond, registerPlugin } from "react-filepond";
import "filepond/dist/filepond.min.css";
import FilePondPluginImageExifOrientation from "filepond-plugin-image-exif-orientation";
import FilePondPluginImagePreview from "filepond-plugin-image-preview";
import "filepond-plugin-image-preview/dist/filepond-plugin-image-preview.css";
import { FiPlusCircle } from "react-icons/fi";
import { IconContext } from "react-icons";
import AlertDialog from "../AlertDialog";

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

const useStyles = makeStyles((theme) => ({
  fControl: {
    width: "100%",
    paddingLeft: "1.50rem",
  },
  categoryHeading: {
    padding: "1em",
    paddingLeft: "0.5em",
    fontSize: "1em",
    fontWeight: "900",
  },
}));

let initialValues = {
  description: "",
  price: "",
  date: "",
  supplier: null,
  client_name: "",
  client_id: "",
  category: null,
};

registerPlugin(FilePondPluginImageExifOrientation, FilePondPluginImagePreview);

export default function AddExpenses(props) {
  const { accountCategories, setRefreshSummary, refreshSummary } = props;
  const [payment, setPayment] = useState(false);
  const [accountCategory, setAccountCategory] = useState(accountCategories[0]);
  const [gst, setGst] = useState(false);
  const [image, setImages] = useState([]);
  const [loader, setLoader] = useState(true);
  const [clientList, setClientList] = useState([]);
  const classes = useStyles();
  const [openAlert, setOpenAlert] = useState(false);
  const [msg, setMsg] = useState("");
  const [alertType, setAlertType] = useState("Error");
  const filter = createFilterOptions();

  const handleFormSubmit = async (values, setSubmitting) => {
    let formattedDate = new Date(values.date);
    let d = formattedDate.getDate();
    let m = formattedDate.getMonth();
    let y = formattedDate.getFullYear();

    const formData = new FormData();

    if (values.client_id === 0 && values.client_name !== "") {
      const formData1 = new FormData();
      formData1.append("from_bank_match", 1);
      formData1.append("name", values.client_name);
      formData1.append("type", 2);
      const result1 = await AddClient(formData1);
      if (result1.success) {
        let temp = clientList;
        temp.push({
          label: values.client_name,
          id: result1.data.id,
        });
        setClientList(temp);
        formData.append("client_id", result1.data.id || "");
      }
    } else {
      formData.append("client_id", values.client_id ? values.client_id : "");
    }

    formData.append("name", values.description);
    formData.append("price", values.price);
    formData.append("date", y + "-" + (m + 1) + "-" + d);
    formData.append("gst", gst ? 1 : 0);
    formData.append(
      "account_subcategory_id",
      accountCategory.account_subcategory_id
    );

    formData.append("paid_with_cash", payment ? 1 : 0);
    formData.append("is_asset", "no");
    if (typeof image[0] !== "undefined") {
      formData.append("image", image[0].file);
    }

    let result;
    // INSRET OR UPDATE
    if (props.updateId) {
      formData.append("id", props.updateId);
      result = await UpdateExpense(formData);
    } else {
      result = await AddExpense(formData);
    }

    if (result.success) {
      setSubmitting(false);
      props.setIsOpen(false);
      props.setIsListUpdate(true);
      if (props.updateId === "") {
        props.setEmptyView(false);
      }
      setAlertType("Success");
      setMsg(result.message);
      setOpenAlert(true);
    } else {
      setSubmitting(false);
      setAlertType("Error");
      setMsg(result.message);
      setOpenAlert(true);
    }
    setRefreshSummary(!refreshSummary);
  };

  const AddSupplier = async (client_name) => {
    const formData1 = new FormData();
    formData1.append("from_bank_match", 1);
    formData1.append("name", client_name);
    formData1.append("type", 2);
    const result1 = await AddClient(formData1);
    if (result1.success) {
      let temp = clientList;
      temp.push({
        label: client_name,
        id: result1.data.id,
      });
      setClientList(temp);
      return {
        client_id: result1.data.id,
        client_name: client_name,
      };
    }
    return {};
  };

  useEffect(async () => {
    if (props.isOpen) {
      setLoader(true);
      const formData = "?filter_by[type]=supplier&page_size=1000";

      // GET CLIENT DATA
      const resultInfo = await ListClient(formData);
      if (resultInfo.success) {
        let temp = [];
        resultInfo.data.clients_details.map((v) => {
          temp.push({
            label: v.name,
            id: v.client_id,
          });
        });
        setClientList(temp);

        if (props.updateId) {
          const resultInfo = await InfoExpense(props.updateId);
          if (resultInfo.success) {
            initialValues.description = resultInfo.data.name;
            initialValues.price = resultInfo.data.price;
            initialValues.date = new Date(resultInfo.data.date);
            if (resultInfo.data.client) {
              initialValues.supplier = {
                id: resultInfo.data.client.id,
                label: resultInfo.data.client.name,
              };
            }
            setPayment(resultInfo.data.paid_with_cash);
            setGst(resultInfo.data.gst);
            setAccountCategory({
              account_subcategory_id: resultInfo.data.account_subcategory_id,
              name: resultInfo.data.account_subcategory.name,
            });

            if (
              resultInfo.data.image !== "" &&
              resultInfo.data.image !== null
            ) {
              setImages([
                { source: resultInfo.data.image, options: { type: "remote" } },
              ]);
            }
            setLoader(false);
          }
        } else {
          setPayment(false);
          setGst(false);
          setAccountCategory(accountCategories[0]);
          setImages([]);
          setLoader(false);
        }
      }
    }
  }, [props.isOpen]);

  return (
    <>
      <Transition.Root show={props.isOpen} as={Fragment}>
        <Dialog
          as="div"
          static
          className="fixed inset-0 overflow-hidden"
          onClose={props.setIsOpen}
        >
          <div className="absolute inset-0 overflow-hidden">
            <Transition.Child
              as={Fragment}
              enter="ease-in-out duration-500"
              enterFrom="opacity-0"
              enterTo="opacity-100"
              leave="ease-in-out duration-500"
              leaveFrom="opacity-100"
              leaveTo="opacity-0"
            >
              <Dialog.Overlay className="absolute inset-0 bg-gray-500 bg-opacity-75 transition-opacity pointer-events-none" />
            </Transition.Child>

            <div className="fixed inset-y-0 max-w-full right-0 flex">
              <Transition.Child
                as={Fragment}
                enter="transform transition ease-in-out duration-500 sm:duration-700"
                enterFrom="translate-x-full"
                enterTo="translate-x-0"
                leave="transform transition ease-in-out duration-500 sm:duration-700"
                leaveFrom="translate-x-0"
                leaveTo="translate-x-full"
              >
                <div className="w-screen lg:max-w-3xl">
                  <Formik
                    initialValues={initialValues}
                    validationSchema={ExpenseSchema}
                    onSubmit={(values, { setSubmitting }) => {
                      handleFormSubmit(values, setSubmitting);
                    }}
                  >
                    {({
                      values,
                      errors,
                      handleChange,
                      handleSubmit,
                      handleBlur,
                      touched,
                      isSubmitting,
                      setFieldValue,
                    }) => (
                      <Form
                        onSubmit={handleSubmit}
                        className="h-full divide-y divide-gray-200 flex flex-col bg-white shadow-xl"
                      >
                        <div className="flex-1 h-0 pb-8 overflow-y-auto">
                          <div className="border-b border-gray-200 bg-gray-100 py-4 px-4 sm:px-6">
                            <div className="flex items-center justify-between">
                              <Dialog.Title className="text-xl font-medium text-gray-800">
                                {props.updateId ? "Edit" : "New"} Expense
                                <p className="mt-1 font-normal text-sm text-gray-500">
                                  Fields marked with * are mandatory
                                </p>
                              </Dialog.Title>
                              <div className="ml-3 h-7 flex items-center">
                                <button
                                  type="button"
                                  className="rounded-full hover:bg-gray-300 p-2 text-gray-500 hover:text-gray-700 focus:outline-none focus:ring-2 focus:ring-white"
                                  onClick={() => props.setIsOpen(false)}
                                >
                                  <span className="sr-only">Close panel</span>
                                  <XIcon
                                    className="h-6 w-6"
                                    aria-hidden="true"
                                  />
                                </button>
                              </div>
                            </div>
                          </div>

                          <div className="flex-1 flex flex-col justify-between">
                            <div className="px-4 divide-y divide-gray-200 sm:px-6">
                              <div className="space-y-4 border-gray-200 mb-4 pt-6 pb-7 grid grid-cols-1 gap-y-2 sm:grid-cols-2 sm:gap-x-8">
                                {!loader ? (
                                  <>
                                    <div className="App filepond sm:col-span-2">
                                      <FilePond
                                        files={image}
                                        onupdatefiles={setImages}
                                        allowMultiple={false}
                                        maxFiles={1}
                                        allowFileTypeValidation={true}
                                        acceptedFileTypes={[
                                          "image/png",
                                          "image/jpeg",
                                        ]}
                                        allowFileSizeValidation={true}
                                        maxFileSize={1}
                                        labelMaxFileSizeExceeded={
                                          "File is too large"
                                        }
                                        name="files" /* sets the file input name, it's filepond by default */
                                        className="App sm:col-span-2"
                                        labelIdle='<strong>Add photo of your receipt</strong> <br> Drag & Drop your file or <span class="filepond--label-action">Browse</span>'
                                      />
                                    </div>

                                    <div className="sm:col-span-2">
                                      <label
                                        htmlFor="company"
                                        className="block text-md tracking-wider font-medium text-gray-500"
                                      >
                                        Expense Description*
                                      </label>
                                      <div className="mt-1">
                                        <input
                                          variant="outlined"
                                          value={values.description}
                                          onChange={handleChange}
                                          onBlur={handleBlur}
                                          type="text"
                                          name="description"
                                          id="description"
                                          placeholder=""
                                          className={
                                            "placeholder-gray-400 py-3 px-4 block w-full border-2 border-gray-200  focus:ring-indigo-400 focus:border-indigo-400 border-gray-100 rounded-md " +
                                            classes.fControl
                                          }
                                        />
                                        <ErrorMessage
                                          name="description"
                                          component="span"
                                          className="error_text"
                                        />
                                      </div>
                                    </div>

                                    <div>
                                      <label
                                        htmlFor="company"
                                        className="block text-md tracking-wider font-medium text-gray-500"
                                      >
                                        Price*
                                      </label>
                                      <div className="flex justify-between mt-1 relative ">
                                        <div className="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
                                          <span className="text-gray-500 font-semibold sm:text-md">
                                            $
                                          </span>
                                        </div>
                                        <input
                                          variant="outlined"
                                          value={values.price}
                                          onChange={handleChange}
                                          onBlur={(e) => {
                                            setFieldValue(
                                              "price",
                                              parseFloat(
                                                e.target.value
                                              ).toFixed(2)
                                            );
                                          }}
                                          type="number"
                                          name="price"
                                          id="price"
                                          placeholder="0.00"
                                          className={
                                            "placeholder-gray-400 py-3 px-4 block w-full border-2 border-gray-200  focus:ring-indigo-400 focus:border-indigo-400 border-gray-100 rounded-md " +
                                            classes.fControl
                                          }
                                        />
                                        <ErrorMessage
                                          name="price"
                                          component="span"
                                          className="error_text"
                                        />
                                      </div>
                                    </div>

                                    <div>
                                      <label
                                        htmlFor="last-name"
                                        className="block text-md tracking-wider font-medium text-gray-500"
                                      >
                                        Date*
                                      </label>
                                      <div className="mt-1 relative  border-2  focus:ring-indigo-400 focus:border-indigo-400 border-gray-100 rounded-md">
                                        <DateTimePicker
                                          date={values.date}
                                          onDateChange={(val) => {
                                            setFieldValue("date", val);
                                          }}
                                          name="date"
                                          id="date"
                                          maxDate={new Date()}
                                          inputClassName={
                                            "input py-3 px-4 block w-full border-0"
                                          }
                                          placeholder=""
                                        />
                                        <ErrorMessage
                                          name="date"
                                          component="span"
                                          className="error_text"
                                        />
                                      </div>
                                    </div>

                                    <div className="sm:col-span-2">
                                      <label
                                        htmlFor="first-name"
                                        className="inline-flex items-center text-md tracking-wider font-medium text-gray-500"
                                      >
                                        Supplier
                                      </label>
                                      <div className="mt-1">
                                        <Stack spacing={2}>
                                          <Autocomplete
                                            disablePortal
                                            id="client_name"
                                            name="client_name"
                                            value={values.supplier}
                                            onBlur={handleBlur}
                                            freeSolo
                                            className="p-0 form-control"
                                            options={clientList}
                                            getOptionLabel={(option) => {
                                              if (
                                                option?.value ===
                                                "Add new Supplier"
                                              ) {
                                                return (
                                                  <div className="py-2 flex gap-1">
                                                    Add New Supplier&nbsp;&nbsp;
                                                    <IconContext.Provider
                                                      value={{
                                                        color: "#4d4dff",
                                                        className:
                                                          "global-class-name h-6 w-6",
                                                      }}
                                                    >
                                                      <div>
                                                        <FiPlusCircle />
                                                      </div>
                                                    </IconContext.Provider>
                                                  </div>
                                                );
                                              } else {
                                                return option.label;
                                              }
                                              // return option.label;
                                            }}
                                            onChange={async (e, value) => {
                                              if (
                                                value?.value ==
                                                "Add new Supplier"
                                              ) {
                                                setFieldValue(
                                                  "client_name",
                                                  value.label
                                                );
                                                setFieldValue("supplier", {
                                                  label: value.label,
                                                });
                                                let client_details =
                                                  await AddSupplier(
                                                    value.label
                                                  );
                                                if (
                                                  !!client_details?.client_id
                                                ) {
                                                  setFieldValue(
                                                    "client_id",
                                                    client_details.client_id
                                                  );
                                                  setFieldValue(
                                                    "client_name",
                                                    client_details.client_name
                                                  );
                                                  setFieldValue("supplier", {
                                                    label:
                                                      client_details.client_name,
                                                    id: client_details.client_id,
                                                  });
                                                }
                                              } else {
                                                if (
                                                  typeof value !==
                                                    "undefined" &&
                                                  value !== null &&
                                                  Object.keys(value).length > 0
                                                ) {
                                                  if (
                                                    typeof value === "object"
                                                  ) {
                                                    setFieldValue(
                                                      "client_id",
                                                      value.id
                                                    );
                                                    setFieldValue(
                                                      "client_name",
                                                      value.label
                                                    );
                                                  } else {
                                                    setFieldValue(
                                                      "client_id",
                                                      ""
                                                    );
                                                    setFieldValue(
                                                      "client_name",
                                                      ""
                                                    );
                                                    setFieldValue("supplier", {
                                                      label: "",
                                                    });
                                                  }
                                                } else {
                                                  setFieldValue(
                                                    "client_id",
                                                    ""
                                                  );
                                                  setFieldValue(
                                                    "client_name",
                                                    ""
                                                  );
                                                }
                                              }
                                            }}
                                            renderInput={(params) => (
                                              <TextField
                                                {...params}
                                                variant="outlined"
                                              />
                                            )}
                                            filterOptions={(
                                              options,
                                              params
                                            ) => {
                                              const filtered = filter(
                                                options,
                                                params
                                              );
                                              // Suggest the creation of a new value
                                              if (params.inputValue !== "") {
                                                filtered.push({
                                                  label: params.inputValue,
                                                  value: `Add new Supplier`,
                                                });
                                              }
                                              return filtered;
                                            }}
                                            onInputChange={(e) => {
                                              if (
                                                e !== null &&
                                                typeof e.target !=
                                                  "undefined" &&
                                                e.target !== null &&
                                                typeof e.target.value !=
                                                  "undefined"
                                              ) {
                                                setFieldValue("client_id", 0);
                                                setFieldValue(
                                                  "client_name",
                                                  e.target.value
                                                );
                                              }
                                            }}
                                          />
                                        </Stack>
                                      </div>
                                    </div>

                                    <div>
                                      <label
                                        htmlFor="first-name"
                                        className="inline-flex items-center text-md tracking-wider font-medium text-gray-500"
                                      >
                                        Category*
                                      </label>
                                      <Listbox
                                        value={accountCategory}
                                        onChange={setAccountCategory}
                                      >
                                        {({ open }) => (
                                          <>
                                            <div className="mt-1 relative">
                                              <Listbox.Button className="py-3 px-4 block relative text-left cursor-default  w-full border-2 border-gray-200  focus:ring-indigo-400 focus:border-indigo-400 border-gray-100 rounded-md">
                                                <span className="block truncate">
                                                  {accountCategory.name}
                                                </span>
                                                <span className="absolute inset-y-0 right-0 flex items-center pr-2 pointer-events-none">
                                                  <SelectorIcon
                                                    className="h-5 w-5 text-gray-400"
                                                    aria-hidden="true"
                                                  />
                                                </span>
                                              </Listbox.Button>
                                              <Transition
                                                show={open}
                                                as={Fragment}
                                                leave="transition ease-in duration-100"
                                                leaveFrom="opacity-100"
                                                leaveTo="opacity-0"
                                              >
                                                <Listbox.Options
                                                  static
                                                  className="absolute z-10 mt-1 w-full bg-white shadow-lg max-h-60 rounded-md py-1 text-base ring-1 ring-black ring-opacity-5 overflow-auto focus:outline-none sm:text-sm"
                                                >
                                                  <Listbox.Button>
                                                    <div
                                                      className={
                                                        classes.categoryHeading
                                                      }
                                                    >
                                                      Expenses
                                                    </div>
                                                  </Listbox.Button>
                                                  {accountCategories.map(
                                                    (terms, tIndex) => (
                                                      <Listbox.Option
                                                        key={tIndex}
                                                        className={({
                                                          active,
                                                        }) =>
                                                          classNames(
                                                            active
                                                              ? "text-white bg-brand-blue"
                                                              : "text-gray-900",
                                                            "cursor-default select-none relative py-2 pl-3 pr-9"
                                                          )
                                                        }
                                                        value={terms}
                                                      >
                                                        {({
                                                          selected,
                                                          active,
                                                        }) => (
                                                          <>
                                                            <span
                                                              className={classNames(
                                                                selected
                                                                  ? "font-semibold"
                                                                  : "font-normal",
                                                                "block truncate"
                                                              )}
                                                            >
                                                              {terms.name}
                                                            </span>

                                                            {selected ? (
                                                              <span
                                                                className={classNames(
                                                                  active
                                                                    ? "text-white"
                                                                    : "text-brand-blue",
                                                                  "absolute inset-y-0 right-0 flex items-center pr-4"
                                                                )}
                                                              >
                                                                <CheckIcon
                                                                  className="h-5 w-5"
                                                                  aria-hidden="true"
                                                                />
                                                              </span>
                                                            ) : null}
                                                          </>
                                                        )}
                                                      </Listbox.Option>
                                                    )
                                                  )}
                                                </Listbox.Options>
                                              </Transition>
                                            </div>
                                          </>
                                        )}
                                      </Listbox>
                                    </div>

                                    <div>
                                      <label
                                        htmlFor="company"
                                        className="block text-md tracking-wider font-medium text-gray-500"
                                      >
                                        Payment
                                      </label>
                                      <FormControlLabel
                                        control={
                                          <Checkbox
                                            checked={payment}
                                            onChange={(e) =>
                                              setPayment(e.target.checked)
                                            }
                                            id="payment"
                                            name="payment"
                                            sx={{
                                              "& .MuiSvgIcon-root": {
                                                fontSize: 22,
                                              },
                                            }}
                                          />
                                        }
                                        className="checkbox"
                                        label="Paid with cash?"
                                      />
                                    </div>

                                    <div>
                                      <label
                                        htmlFor="company"
                                        className="block text-md tracking-wider font-medium text-gray-500"
                                      >
                                        Include GST (10%)
                                      </label>
                                      <div className="mt-1">
                                        <Switch
                                          checked={gst}
                                          onChange={setGst}
                                          className={classNames(
                                            gst
                                              ? "bg-green-600"
                                              : "bg-gray-300",
                                            "relative inline-flex flex-shrink-0 h-6 w-11 border-2 border-transparent rounded-full cursor-pointer transition-colors ease-in-out duration-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                                          )}
                                        >
                                          <span className="sr-only">
                                            GST Enable/Disable
                                          </span>
                                          <span
                                            className={classNames(
                                              gst
                                                ? "translate-x-5"
                                                : "translate-x-0",
                                              "pointer-events-none relative inline-block h-5 w-5 rounded-full bg-white shadow transform ring-0 transition ease-in-out duration-200"
                                            )}
                                          >
                                            <span
                                              className={classNames(
                                                gst
                                                  ? "opacity-0 ease-out duration-100"
                                                  : "opacity-100 ease-in duration-200",
                                                "absolute inset-0 h-full w-full flex items-center justify-center transition-opacity pointer-events-none"
                                              )}
                                              aria-hidden="true"
                                            >
                                              <svg
                                                className="h-3 w-3 text-gray-400"
                                                fill="none"
                                                viewBox="0 0 12 12"
                                              >
                                                <path
                                                  d="M4 8l2-2m0 0l2-2M6 6L4 4m2 2l2 2"
                                                  stroke="currentColor"
                                                  strokeWidth={2}
                                                  strokeLinecap="round"
                                                  strokeLinejoin="round"
                                                />
                                              </svg>
                                            </span>
                                            <span
                                              className={classNames(
                                                gst
                                                  ? "opacity-100 ease-in duration-200"
                                                  : "opacity-0 ease-out duration-100",
                                                "absolute inset-0 h-full w-full flex items-center justify-center transition-opacity pointer-events-none"
                                              )}
                                              aria-hidden="true"
                                            >
                                              <svg
                                                className="h-3 w-3 text-green-600"
                                                fill="currentColor"
                                                viewBox="0 0 12 12"
                                              >
                                                <path d="M3.707 5.293a1 1 0 00-1.414 1.414l1.414-1.414zM5 8l-.707.707a1 1 0 001.414 0L5 8zm4.707-3.293a1 1 0 00-1.414-1.414l1.414 1.414zm-7.414 2l2 2 1.414-1.414-2-2-1.414 1.414zm3.414 2l4-4-1.414-1.414-4 4 1.414 1.414z" />
                                              </svg>
                                            </span>
                                          </span>
                                        </Switch>
                                      </div>
                                    </div>
                                  </>
                                ) : (
                                  <div className="box-center col-span-2 text-center">
                                    <CircularProgress />
                                  </div>
                                )}
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="flex-shrink-0  px-4 py-4 flex justify-between">
                          <div></div>
                          <div>
                            <button
                              type="submit"
                              className="ml-4 inline-flex justify-center py-2 px-4 tracking-wider border border-transparent uppercase shadow-sm  font-medium text-sm rounded-full text-white bg-green-600 hover:bg-green-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-500"
                              disabled={isSubmitting ? true : false}
                            >
                              {isSubmitting ? "Please wait..." : "Save"}
                            </button>
                          </div>
                        </div>
                      </Form>
                    )}
                  </Formik>
                </div>
              </Transition.Child>
            </div>
          </div>

          <AlertDialog
            id="ringtone-menu2"
            title={alertType}
            message={msg}
            keepMounted
            open={openAlert}
            onClose={setOpenAlert}
          />
        </Dialog>
      </Transition.Root>
    </>
  );
}
