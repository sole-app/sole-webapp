import { Fragment, useState, useEffect } from "react";
import { Listbox, Transition } from "@headlessui/react";
import { CheckIcon, SelectorIcon } from "@heroicons/react/solid";
import { SearchIcon, AdjustmentsIcon } from "@heroicons/react/outline";
import { download as downloadExpense, Delete, List } from "../../services/api/expense.services";
import Skeleton from "react-loading-skeleton";
import "react-loading-skeleton/dist/skeleton.css";
import ConfirmationDialog from "../ConfirmationDialog";
import AlertDialog from "../AlertDialog";
import Filter from "../../components/Expenses/Filter";
import Pagination from "../Pagination";
import NumberFormat from "react-number-format";
import ReactTooltip from "react-tooltip";
import { List as ListClient } from "../../services/api/client.services";
import { getFileName, formatShortDate } from "../../helpers/common";
import { FiDownload } from "react-icons/fi";
import store from "../../store/store";
import { snackbarOpen } from "../../reducers/slices/snackbarSlice";
import { SNACKBAR_TYPE } from "../../components/Layout/Snackbar";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import Dialog from "@material-ui/core/Dialog";
import { ImpulseSpinner } from "react-spinners-kit";

// Filter Options
const filterOptions = [
  { id: 1, name: "All Status" },
  { id: 2, name: "Draft" },
  { id: 3, name: "Sent" },
];

// Sorting Options
const sortOptions = [
  { id: "date_of_purchase", name: "Default (By Created Date)", order: "DESC" },
  { id: "name", name: "Name", order: "ASC" },
  { id: "price", name: "Price - Low -> High", order: "ASC" },
  { id: "price", name: "Price - High -> Low", order: "DESC" },
];

let globalSearch = "";
let apiRunning = false;

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

export default function ExpensesList(props) {
  const { setRefreshSummary, refreshSummary } = props;
  const [open, setOpen] = useState(false);
  const [updateId, setUpdateId] = useState("");
  const [loader, setLoader] = useState(true);
  const [downloadLoader, setDownloadLoader] = useState(false);
  const [expensesIds, setExpensesIds] = useState([]);
  const [records, setRecords] = useState([]);
  const [confirmation, setConfirmation] = useState(false);
  const [alert, setAlert] = useState(false);
  const [deleteType, setDeleteType] = useState("NORMAL");
  const [deleteId, setDeleteId] = useState("");
  const [activeKey, setActiveKey] = useState("");
  const [sortSelected, setSortSelected] = useState(sortOptions[0]);
  const [filterObj, setFilterObj] = useState({});
  const [showDownloadDialog, setShowDownloadDialog] = useState(false);
  const [allExpenses, setAllExpenses] = useState(false);

  //const [filterSelected, setFilterSelected] = useState(filterOptions[0]);
  const [searchName, setSearchName] = useState("");

  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [paginationObj, setpaginationObj] = useState({});

  const [clients, setClients] = useState([]);

  // GET ALL LIST
  const getList = async (
    newPage,
    newRowPerPage,
    filter = {},
    resultCheck = false
  ) => {
    props.setIsListUpdate(false);
    setLoader(true);

    let formData = "";
    let filterByName;
    formData += "?page_size=" + newRowPerPage;
    formData += "&page=" + (newPage + 1);
    if (typeof filter.name !== "undefined") {
      formData += "&filter_by[name]=" + filter.name;
      filterByName = filter.name;
    } else {
      formData += "&filter_by[name]=" + searchName;
      filterByName = searchName;
    }

    if (typeof filter.sort !== "undefined") {
      formData +=
        "&sort_by[" + filter.sort.id + "]=" + filter.sort.order.toLowerCase();
    } else {
      formData +=
        "&sort_by[" + sortSelected.id + "]=" + sortSelected.order.toLowerCase();
    }

    let fromFilter = false;
    if (typeof filter.fromFilter !== "undefined" && filter.fromFilter) {
      fromFilter = true;
    }

    if (
      typeof filter.paid_with_cash !== "undefined" &&
      filter.paid_with_cash &&
      fromFilter
    ) {
      formData += "&filter_by[paid_with_cash]=" + 1;
    } else if (
      typeof filterObj.paid_with_cash !== "undefined" &&
      filterObj.paid_with_cash &&
      !fromFilter
    ) {
      formData += "&filter_by[paid_with_cash]=" + 1;
    }

    if (typeof filter.start_date !== "undefined" && fromFilter) {
      formData += "&filter_by[start_date]=" + filter.start_date;
    } else if (typeof filterObj.start_date !== "undefined" && !fromFilter) {
      formData += "&filter_by[start_date]=" + filterObj.start_date;
    }

    if (typeof filter.end_date !== "undefined" && fromFilter) {
      formData += "&filter_by[end_date]=" + filter.end_date;
    } else if (typeof filterObj.end_date !== "undefined" && !fromFilter) {
      formData += "&filter_by[end_date]=" + filterObj.end_date;
    }

    if (typeof filter.min_price !== "undefined" && fromFilter) {
      formData += "&filter_by[min_price]=" + filter.min_price;
    } else if (typeof filterObj.min_price !== "undefined" && !fromFilter) {
      formData += "&filter_by[min_price]=" + filterObj.min_price;
    }

    if (typeof filter.max_price !== "undefined" && fromFilter) {
      formData += "&filter_by[max_price]=" + filter.max_price;
    } else if (typeof filterObj.max_price !== "undefined" && !fromFilter) {
      formData += "&filter_by[max_price]=" + filterObj.max_price;
    }

    let cIds = [];
    if (typeof filter.category_id !== "undefined" && fromFilter) {
      cIds = filter.category_id;
    } else if (typeof filterObj.category_id !== "undefined" && !fromFilter) {
      cIds = filterObj.category_id;
    }
    if (cIds.length > 0) {
      for (let i = 0; i < cIds.length; i++) {
        formData +=
          "&filter_by[account_subcategory_id][" + (i + 1) + "]=" + cIds[i];
      }
    }

    cIds = [];
    if (typeof filter.client_id !== "undefined" && fromFilter) {
      cIds = filter.client_id;
    } else if (typeof filterObj.client_id !== "undefined" && !fromFilter) {
      cIds = filterObj.client_id;
    }
    if (cIds.length > 0) {
      for (let i = 0; i < cIds.length; i++) {
        formData += "&filter_by[client_id][" + (i + 1) + "]=" + cIds[i];
      }
    }
    apiRunning = true;
    const result = await List(formData);
    if (result.success) {
      if (resultCheck) {
        if (filterByName === globalSearch) {
          setRecords(result.data.expense_details);
          apiRunning = false;
          setpaginationObj({
            total: result.data.total || 0,
          });
        }
      } else {
        setRecords(result.data.expense_details);
        apiRunning = false;
        setpaginationObj({
          total: result.data.total || 0,
        });
      }
    }
    if (!apiRunning) {
      setLoader(false);
    }
  };

  const changeName = (event) => {
    // let filter = {
    //     name: event.target.value
    // };
    // setSearchName(event.target.value);
    // getList(page,rowsPerPage, filter);

    globalSearch = event.target.value;
    setSearchName(event.target.value);
  };

  const changeSort = (sortData) => {
    let filter = {
      sort: sortData,
    };
    setSortSelected(sortData);
    getList(page, rowsPerPage, filter);
  };

  const applyFilter = (filter) => {
    setFilterObj(filter);
    filter.fromFilter = true;
    getList(page, rowsPerPage, filter);
    setOpen(false);
  };

  const getClients = async () => {
    const formData = "?filter_by[type]=supplier&page_size=1000";

    const resultInfo = await ListClient(formData);
    if (resultInfo.success) {
      let temp = [];
      resultInfo.data.clients_details.map((v) => {
        temp.push({
          id: v.client_id,
          name: v.name,
        });
      });
      setClients(temp);
    }
  };

  useEffect(() => {
    if (props.isListUpdate) {
      setLoader(true);
      getList(0, rowsPerPage);
      getClients();
    }
  }, [props.isListUpdate]);

  useEffect(() => {
    getList(page, rowsPerPage, {}, true);
  }, [searchName]);

  const handleConfirmationClose = async (id) => {
    if (id) {
      const formData = new FormData();
      formData.append("id", id);

      const result = await Delete(formData);
      if (result.success) {
        records.splice(activeKey, 1);
        if (records.length === 0) {
          props.setEmptyView(true);
        }
        setRecords(records);
        setConfirmation(false);
      } else {
        setConfirmation(false);
      }
      setDeleteId("");
      setRefreshSummary(!refreshSummary);
    } else {
      setDeleteId("");
      setConfirmation(false);
    }
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
    getList(newPage, rowsPerPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value));
    getList(page, parseInt(event.target.value));
  };

  const changeAlert = (type = "") => {
    setAlert(!alert);
  };

  const changeCheckbox = (id) => {
    let index = expensesIds.indexOf(id);
    if (index === -1) {
      //add
      setExpensesIds([...expensesIds, ...[id]]);
    } else {
      //remove
      expensesIds.splice(index, 1);
      setExpensesIds([...expensesIds]);
    }
  };

  const download = async () => {
    if (expensesIds.length <= 0) {
      store.store.dispatch(
        snackbarOpen({
          type: SNACKBAR_TYPE.ERROR,
          message: "Please select expense to download",
        })
      );
      return;
    } else {
      if (showDownloadDialog) {
        setDownloadLoader(true);
        let formData = new FormData();
        for (let i = 0; i < expensesIds.length; i++) {
          formData.append("expenses[]", expensesIds[i]);
        }

        const result = await downloadExpense(formData);
        if (result.success) {
          let data = result.data.zip;
          if (window.navigator && window.navigator.msSaveOrOpenBlob) {
            let byteCharacters = atob(data);
            let byteNumbers = new Array(byteCharacters.length);
            for (let i = 0; i < byteCharacters.length; i++) {
              byteNumbers[i] = byteCharacters.charCodeAt(i);
            }
            let byteArray = new Uint8Array(byteNumbers);
            let blob = new Blob([byteArray], {
              type: " application/zip",
            });
            let fileName = "invoices.zip";
            window.navigator.msSaveOrOpenBlob(blob, fileName);
          } else {
            // decode base64 string, remove space for IE compatibility
            let binary = atob(data.replace(/\s/g, ""));
            let len = binary.length;
            let buffer = new ArrayBuffer(len);
            let view = new Uint8Array(buffer);
            for (let i = 0; i < len; i++) {
              view[i] = binary.charCodeAt(i);
            }

            // create the blob object with content-type " application/zip"
            let blob = new Blob([view], { type: " application/zip" });
            window.URL = window.URL || window.webkitURL;
            let url = window.URL.createObjectURL(blob);
            window.open(url);
          }
        }

        setDownloadLoader(false);
        setShowDownloadDialog(false);
        setExpensesIds([]);
        setAllExpenses(false);
      } else {
        setShowDownloadDialog(true);
      }
    }
  };

  const cancelDownloadExpenses = () => {
    setShowDownloadDialog(false);
  };

  const selectAllExpenses = (event) => {
    if (event.target.checked) {
      setAllExpenses(true);
      let ids = expensesIds;
      records.forEach((value, index) => {
        let each_expense_id = value.expense_id;
        let each_expense_index = ids.indexOf(each_expense_id);
        if (each_expense_index === -1) {
          ids.push(each_expense_id);
        }
        setExpensesIds(ids);
      });
    } else {
      setAllExpenses(false);
      setExpensesIds([]);
    }
  };

  return (
    <div className="mt-8 w-full flex flex-col">
      <Dialog
        sx={{ "& .MuiDialog-paper": { width: "80%", maxHeight: 435 } }}
        maxWidth="xs"
        disablePortal
        open={showDownloadDialog}
      >
        <DialogTitle>Download Expenses</DialogTitle>
        <DialogContent dividers>
          Do you want to download the selected expenses? <br></br>
          <br></br>
          Depending on how many expenses you have selected, this process may
          take up to a few minutes. Please do not navigate away from this page.
        </DialogContent>
        <DialogActions>
          <button onClick={cancelDownloadExpenses} className="btn-no">
            No
          </button>
          <button onClick={download} className="btn-yes">
            {downloadLoader ? (
              <span className="w-full flex align-items-center justify-content-center">
                Downloading...&nbsp;
                <ImpulseSpinner
                  frontColor={"white"}
                  backColor="rgba(255,255,255, 0.5)"
                />
              </span>
            ) : (
              <span>Yes</span>
            )}
          </button>
        </DialogActions>
      </Dialog>
      <ConfirmationDialog
        id="ringtone-menu"
        title="Remove Expense?"
        message={
          deleteType === "NORMAL"
            ? "This expense will be permanently deleted."
            : "This expense is already reconciled, are you sure you want to delete the expense and undo the reconcilation?"
        }
        keepMounted
        open={confirmation}
        onClose={handleConfirmationClose}
        value={deleteId}
      />
      <AlertDialog
        id="ringtone-menu2"
        title="Note"
        message="The expense is already reconciled and can't be edited. You can open in view mode only."
        keepMounted
        open={alert}
        onClose={changeAlert}
      />
      <div className="-my-2 w-full shadow bg-white overflow-hidden border-b border-gray-200 sm:rounded-lg ">
        <div className="pt-2 w-full align-middle inline-block min-w-full">
          <div>
            <div className="flex w-full gap-5 p-7">
              <div className="download w-[3%] mt-1">
                <span
                  onClick={() => download()}
                  data-for="download"
                  data-tip=""
                  className="bg-gray-100 hover:bg-gray-200 flex justify-center relative w-full border border-gray-300 rounded-md shadow-sm px-3 py-small text-left cursor-pointer focus:outline-none"
                >
                  <FiDownload />
                  <ReactTooltip
                    id="download"
                    className="custom-tooltip bg-gray-900"
                    border
                    textColor="#ffffff"
                    backgroundColor="#111827"
                    effect="solid"
                  >
                    <p className="w-20">Download</p>
                  </ReactTooltip>
                </span>
              </div>

              <div className="w-[63%]  mt-1 relative rounded-md">
                <div className="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
                  <SearchIcon
                    className="h-5 w-5 text-gray-500"
                    aria-hidden="true"
                  />
                </div>
                <input
                  type="text"
                  name="search"
                  id="search"
                  className="bg-gray-100 focus:ring-indigo-500 focus:border-indigo-500 block w-full pl-10 sm:text-sm p-3 border-gray-300 rounded-md"
                  placeholder="Search"
                  onChange={changeName}
                />
              </div>

              {/* Status dropdown */}
              <div className="w-[17%] mt-1">
                <Listbox value={sortSelected} onChange={changeSort}>
                  {({ open }) => (
                    <>
                      <div className="relative">
                        <div className="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
                          <AdjustmentsIcon
                            className="h-5 w-5 z-10 text-gray-500"
                            aria-hidden="true"
                          />
                        </div>

                        <Listbox.Button className="bg-gray-100 relative w-full border border-gray-300 rounded-md shadow-sm pl-10 pr-10 py-3 text-left cursor-default focus:outline-none focus:ring-1 focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
                          <span className="block truncate">
                            {sortSelected.name}
                          </span>
                          <span className="absolute inset-y-0 right-0 flex items-center pr-2 pointer-events-none">
                            <SelectorIcon
                              className="h-5 w-5 text-gray-400"
                              aria-hidden="true"
                            />
                          </span>
                        </Listbox.Button>

                        <Transition
                          show={open}
                          as={Fragment}
                          leave="transition ease-in duration-100"
                          leaveFrom="opacity-100"
                          leaveTo="opacity-0"
                        >
                          <Listbox.Options
                            static
                            className="absolute z-10 mt-1 w-full bg-white shadow-lg max-h-60 rounded-md py-1 text-base ring-1 ring-black ring-opacity-5 overflow-auto focus:outline-none sm:text-sm"
                          >
                            {sortOptions.map((status) => (
                              <Listbox.Option
                                key={status.id}
                                className={({ active }) =>
                                  classNames(
                                    active
                                      ? "text-white bg-brand-blue"
                                      : "text-gray-900",
                                    "cursor-default select-none relative py-2 pl-3 pr-9"
                                  )
                                }
                                value={status}
                              >
                                {({ selected, active }) => (
                                  <>
                                    <span
                                      className={classNames(
                                        selected
                                          ? "font-semibold"
                                          : "font-normal",
                                        "block truncate"
                                      )}
                                    >
                                      {status.name}
                                    </span>

                                    {selected ? (
                                      <span
                                        className={classNames(
                                          active
                                            ? "text-white"
                                            : "text-brand-blue",
                                          "absolute inset-y-0 right-0 flex items-center pr-4"
                                        )}
                                      >
                                        <CheckIcon
                                          className="h-5 w-5"
                                          aria-hidden="true"
                                        />
                                      </span>
                                    ) : null}
                                  </>
                                )}
                              </Listbox.Option>
                            ))}
                          </Listbox.Options>
                        </Transition>
                      </div>
                    </>
                  )}
                </Listbox>
              </div>

              {/* Status dropdown */}
              <div className="mt-1 w-[17%]">
                <button
                  className="bg-gray-100 relative w-full border border-gray-300 rounded-md shadow-sm pl-10 pr-10 py-3 text-left cursor-default focus:outline-none focus:ring-1 focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                  onClick={() => {
                    setUpdateId("");
                    setOpen(!open);
                  }}
                >
                  <span className="block truncate">Filter by</span>
                  <span className="absolute inset-y-0 right-0 flex items-center pr-2 pointer-events-none">
                    <SelectorIcon
                      className="h-5 w-5 text-gray-400"
                      aria-hidden="true"
                    />
                  </span>

                  <div className="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
                    <AdjustmentsIcon
                      className="h-5 w-5 z-10 text-gray-500"
                      aria-hidden="true"
                    />
                  </div>
                </button>
              </div>
            </div>

            <div className="overflow-y-auto w-full overflow-x-auto">
              <table className="min-w-full hover-row divide-y divide-gray-200">
                <thead className="">
                  <tr>
                    <th
                      scope="col"
                      className="px-7 py-3 text-left text-sm font-medium text-gray-500 uppercase tracking-wider"
                    >
                      <input
                        id={`all_expenses`}
                        name={`all_expenses`}
                        type="checkbox"
                        value={allExpenses}
                        onClick={(e) => selectAllExpenses(e)}
                        className="focus:ring-indigo-500 h-4 w-4 text-brand-blue  border-1 border-gray-300 rounded-xs bg-white"
                        defaultChecked={false}
                        checked={allExpenses}
                      />
                    </th>
                    <th
                      scope="col"
                      className="px-7 py-3 text-left text-sm font-medium text-gray-500 uppercase tracking-wider"
                    >
                      #
                    </th>
                    <th
                      scope="col"
                      className="px-7 py-3 text-left text-sm font-medium text-gray-500 uppercase tracking-wider"
                    >
                      Category
                    </th>
                    <th
                      scope="col"
                      className="px-7 py-3 min-w-96 text-left text-sm font-medium text-gray-500 uppercase tracking-wider"
                    >
                      Description
                    </th>
                    <th
                      scope="col"
                      className="px-7 py-3 text-left text-sm font-medium text-gray-500 uppercase tracking-wider"
                    >
                      Supplier
                    </th>
                    <th
                      scope="col"
                      className="px-7 py-3 text-left text-sm font-medium text-gray-500 uppercase tracking-wider"
                    >
                      Date
                    </th>
                    <th
                      scope="col"
                      className="px-7 py-3 text-left text-sm font-medium text-gray-500 uppercase tracking-wider"
                    >
                      Price
                    </th>
                    <th
                      scope="col"
                      className="px-7 py-3 text-right text-sm font-medium text-gray-500 uppercase tracking-wider"
                    >
                      Actions
                    </th>
                  </tr>
                </thead>
                <tbody className="bg-white divide-y divide-gray-200">
                  {loader == false ? (
                    records.length > 0 ? (
                      records.map((val, key) => (
                        <tr key={key}>
                          <td className="px-7 py-4 whitespace-nowrap">
                            <input
                              id={`expense_id_${key}`}
                              name={`expense_name_${key}`}
                              type="checkbox"
                              value={val.expense_id}
                              onClick={() => changeCheckbox(val.expense_id)}
                              className="focus:ring-indigo-500 h-4 w-4 text-brand-blue  border-1 border-gray-300 rounded-xs bg-white"
                              defaultChecked={
                                expensesIds.indexOf(val.expense_id) !== -1
                              }
                              checked={
                                expensesIds.indexOf(val.expense_id) !== -1
                              }
                            />
                          </td>
                          <td className="px-7 py-4 whitespace-nowrap">
                            <div className="flex  items-center">
                              <div className="text-md font-medium text-gray-900">
                                {rowsPerPage * page + key + 1}
                              </div>
                            </div>
                          </td>

                          <td className="px-7 py-4 flex items-center whitespace-nowrap">
                            <span className="inline-flex items-center justify-center h-12 w-12 rounded-full bg-gray-100">
                              <img
                                src={`/images/production/subcategory_pic/${getFileName(
                                  val.account_subcategory.subcategory_icon
                                )}`}
                                alt="Subcategory"
                                width="24"
                                height="24"
                                className="h-8 w-auto"
                              />
                            </span>
                            <div className="text-md pl-4 text-gray-900">
                              {val.account_subcategory.name}
                            </div>
                          </td>

                          <td className="px-7 py-4 min-w-96">
                            <div className="text-md text-gray-900">
                              {val.name}
                            </div>
                          </td>

                          <td className="px-7 py-4 whitespace-nowrap">
                            <div className="text-md text-gray-900">
                              {val.client ? val.client.name : "-"}
                            </div>
                          </td>

                          <td className="px-7 py-4 whitespace-nowrap">
                            <div className="text-md text-gray-900">
                              {formatShortDate(val.date)}
                            </div>
                          </td>

                          <td className="px-7 py-4 whitespace-nowrap">
                            <div className="text-md font-semibold text-gray-900">
                              <NumberFormat
                                value={val.price.toFixed(2)}
                                displayType={"text"}
                                thousandSeparator={true}
                                prefix={"$"}
                                renderText={(value) => <div>{value}</div>}
                              />
                            </div>
                          </td>

                          <td className="px-7 py-4 whitespace-nowrap text-right text-xsm flex justify-end items-center font-medium">
                            <button
                              className="text-gray-700 flex px-2 py-2 text-sm hover:text-brand-blue"
                              onClick={() => {
                                props.setIsInfoOpen(true);
                                props.setUpdateId(val.expense_id);
                              }}
                            >
                              <span data-for="view" data-tip="View">
                                <svg
                                  xmlns="http://www.w3.org/2000/svg"
                                  className="h-6 w-6"
                                  fill="none"
                                  viewBox="0 0 24 24"
                                  stroke="currentColor"
                                  strokeWidth="2"
                                >
                                  <path
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                    d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"
                                  />
                                  <path
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                    d="M2.458 12C3.732 7.943 7.523 5 12 5c4.478 0 8.268 2.943 9.542 7-1.274 4.057-5.064 7-9.542 7-4.477 0-8.268-2.943-9.542-7z"
                                  />
                                </svg>
                              </span>
                              <ReactTooltip
                                id="view"
                                className="custom-tooltip bg-gray-900"
                                textColor="#ffffff"
                                backgroundColor="#111827"
                                effect="solid"
                              />
                            </button>

                            <button
                              className="text-gray-700 flex px-2 py-2 text-sm hover:text-brand-blue"
                              onClick={() => {
                                if (
                                  val.bank_transaction_id !== null &&
                                  parseInt(val.bank_transaction_id) > 0
                                ) {
                                  changeAlert("EDIT");
                                } else if (
                                  parseInt(val.from_bank_match) === 1
                                ) {
                                  changeAlert("EDIT");
                                } else {
                                  props.setUpdateId(val.expense_id);
                                  props.setIsOpen(true);
                                }
                              }}
                            >
                              <span data-for="edit" data-tip="Edit">
                                <svg
                                  xmlns="http://www.w3.org/2000/svg"
                                  className="h-6 w-6"
                                  fill="none"
                                  viewBox="0 0 24 24"
                                  stroke="currentColor"
                                  strokeWidth="2"
                                >
                                  <path
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                    d="M15.232 5.232l3.536 3.536m-2.036-5.036a2.5 2.5 0 113.536 3.536L6.5 21.036H3v-3.572L16.732 3.732z"
                                  />
                                </svg>
                              </span>
                              <ReactTooltip
                                id="edit"
                                className="custom-tooltip bg-gray-900"
                                textColor="#ffffff"
                                backgroundColor="#111827"
                                effect="solid"
                              />
                            </button>

                            <button
                              className="text-gray-700 flex px-2 py-2 text-sm hover:text-brand-blue"
                              onClick={() => {
                                if (
                                  val.bank_transaction_id !== null &&
                                  parseInt(val.bank_transaction_id) > 0
                                ) {
                                  setDeleteType("RECONCILE");
                                } else if (
                                  parseInt(val.from_bank_match) === 1
                                ) {
                                  setDeleteType("RECONCILE");
                                } else {
                                  setDeleteType("NORMAL");
                                }
                                setDeleteId(val.expense_id);
                                setActiveKey(key);
                                setConfirmation(true);
                              }}
                            >
                              <span data-for="delete" data-tip="Delete">
                                <svg
                                  xmlns="http://www.w3.org/2000/svg"
                                  className="h-6 w-6"
                                  fill="none"
                                  viewBox="0 0 24 24"
                                  stroke="currentColor"
                                  strokeWidth="2"
                                >
                                  <path
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                    d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"
                                  />
                                </svg>
                              </span>
                              <ReactTooltip
                                id="delete"
                                className="custom-tooltip bg-gray-900"
                                textColor="#ffffff"
                                backgroundColor="#111827"
                                effect="solid"
                              />
                            </button>
                          </td>
                        </tr>
                      ))
                    ) : (
                      <tr>
                        <td colspan="7" className="py-4" align="center">
                          <span>No records found.</span>
                        </td>
                      </tr>
                    )
                  ) : (
                    Array.from(Array(5), (e, i) => {
                      return (
                        <tr key={i}>
                          <td className="px-6 py-4 whitespace-nowrap">
                            <Skeleton />
                          </td>
                          <td className="px-7 py-4 whitespace-nowrap">
                            <Skeleton />
                          </td>
                          <td className="px-7 py-4 whitespace-nowrap">
                            <Skeleton />
                          </td>
                          <td className="px-7 py-4 whitespace-nowrap">
                            <Skeleton />
                          </td>
                          <td className="px-7 py-4 whitespace-nowrap">
                            <Skeleton />
                          </td>
                          <td className="px-7 py-4 whitespace-nowrap">
                            <Skeleton />
                          </td>
                          <td className="px-7 py-4 whitespace-nowrap text-right text-xsm font-medium">
                            <Skeleton />
                          </td>
                        </tr>
                      );
                    })
                  )}
                </tbody>
              </table>
            </div>
            {loader == false ? (
              <Pagination
                total={paginationObj.total}
                page={page}
                onPageChange={handleChangePage}
                rowsPerPage={rowsPerPage}
                onRowsPerPageChange={handleChangeRowsPerPage}
              />
            ) : null}
          </div>
        </div>
      </div>

      <Filter
        open={open}
        setOpen={setOpen}
        updateId={updateId}
        clients={clients}
        categories={props.accountCategories}
        filterObj={filterObj}
        applyFilter={applyFilter}
      />
    </div>
  );
}
