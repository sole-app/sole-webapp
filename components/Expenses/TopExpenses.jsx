import { useState, useEffect } from "react";
import Styles from "../../styles/Invoices.module.scss";
import ReactTooltip from "react-tooltip";
import { InformationCircleIcon } from "@heroicons/react/outline";
import { Doughnut, Bar } from "react-chartjs-2";
import { Summary } from "../../services/api/expense.services";
import { getDateFromFYDates } from "../../helpers/common";
import YearMonthFilter from "../Common/YearMonthFilter";

const plugin = {
  id: "increase-legend-spacing",
  beforeInit(chart) {
    // Get reference to the original fit function
    const originalFit = chart.legend.fit;

    // Override the fit function
    chart.legend.fit = function fit() {
      // Call original function and bind scope in order to use `this` correctly inside it
      originalFit.bind(chart.legend)();
      // Change the height as suggested in another answers
      this.height += 20;
    };
  },
};

const months = [
  "Jan",
  "Feb",
  "Mar",
  "Apr",
  "May",
  "Jun",
  "Jul",
  "Aug",
  "Sep",
  "Oct",
  "Nov",
  "Dec",
];

const options = {
  maintainAspectRatio: false,
  plugins: {
    title: {
      display: true,
      text: "",
    },

    legend: {
      display: true,
      position: "right",
      align: "center",
      margin: 20,
      onClick: null,
      borderRadius: 15,

      labels: {
        // This more specific font property overrides the global property
        font: {
          size: 13,
          weight: "bold",
          lineHeight: 2,
          padding: 20,
          family: "'Inter', sans-serif",
        },
        boxWidth: 20,
        boxHeight: 20,
        borderRadius: 15,
        padding: 20,
      },
    },

    tooltip: {
      callbacks: {
        label: function (context) {
          let label = context.label || "";

          if (label) {
            label += ": $";
          }
          if (context.formattedValue !== null) {
            label += context.formattedValue;
          }
          return label;
        },
      },
    },
  },
};

export default function TopExpenses(props) {
  const [type, setType] = useState("YEAR"); //YEAR, MONTH
  const [loader, setLoader] = useState(true);
  const [hasData, setHasData] = useState(false);
  const [donutData, setDonutData] = useState({
    labels: ["Cat 1", "Cat 2", "Cat 3"],
    datasets: [
      {
        data: [0, 0, 0],
        backgroundColor: [
          "#d1d5db",
          "#4C4DFE",
          "#2ef8ff",
          "#191944",
          "#e0ee4f",
        ],
        hoverBackgroundColor: [
          "#d1d5db",
          "#4C4DFE",
          "#2ef8ff",
          "#191944",
          "#e0ee4f",
        ],
      },
    ],
  });

  const getData = async (filterType) => {
    setType(filterType);
    let apiRunning = true;
    setLoader(apiRunning);
    setHasData(false);

    let dates = getDateFromFYDates(filterType);
    let formData = "";
    formData += "?start_date=" + dates.start_date;
    formData += "&end_date=" + dates.end_date;

    const res = await Summary(formData);
    if (res.success) {
      const expenseSummary = res.data.expense_details;
      let ddata = donutData;
      let data = [];
      let labels = [];
      if (expenseSummary.length > 0) {
        for (let i = 0; i < expenseSummary.length; i++) {
          data[i] = expenseSummary[i].total;
          labels[i] = expenseSummary[i].account_category;
        }
      }
      ddata.datasets[0].data = data;
      ddata.labels = labels;
      setDonutData(ddata);
      apiRunning = false;
      if (res.data.expenses_total > 0) {
        setHasData(true);
      }
    }
    if (!apiRunning) {
      setLoader(false);
    }
  };

  useEffect(() => {
    getData(type);
  }, [props.refreshSummary]);

  return (
    <div className="mt-8 w-full xl:w-1/2">
      <div className="grid grid-cols-1 h-full">
        {/* Top Expenses */}
        <div className="bg-white overflow-hidden shadow rounded-lg top-expenses">
          <div className="mx-auto border-b border-grey-400 p-3 sm:p-4  xl:p-5 lg:flex lg:items-center lg:justify-between">
            <h3 className="text-lg inline-flex font-medium tracking-wide text-gray-900 sm:text-xl">
              Top Expenses
              <InformationCircleIcon
                className="h-6 w-6 text-gray-400 ml-2  focus:outline-none"
                data-for="help"
                data-tip="These are the cost categories you spend most on in the selected time period."
              />
              <ReactTooltip
                id="help"
                className="custom-tooltip bg-gray-900"
                border
                textColor="#ffffff"
                backgroundColor="#111827"
                effect="solid"
              >
                <p className="w-64">
                  These are the cost categories you spent the most on for the
                  selected time period.
                </p>
              </ReactTooltip>
            </h3>

            <div className="mt-8 flex lg:mt-0 lg:flex-shrink-0">
              <YearMonthFilter type={type} setType={getData} />
            </div>
          </div>

          {/* Chart */}
          <div className={Styles.earnerChart}>
            <div className="flex">
              <div className="w-full">
                {!loader && (
                  <>
                    {hasData ? (
                      <Doughnut
                        data={donutData}
                        options={options}
                        width={400}
                        height={280}
                      />
                    ) : (
                      <div
                        className="flex items-center h-full text-xl justify-center font-medium text-gray-700"
                        style={{ padding: "2em" }}
                      >
                        No expenses found for this{" "}
                        {type === "YEAR" ? "year" : "month"}
                      </div>
                    )}
                  </>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
