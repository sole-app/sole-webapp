export default function EmptyView() {
  return (
    <div className="flex items-center m-7 justify-center shadow h-3/4 rounded-md bg-white">
      <div className="text-center">
        <img
          src="/images/icon/sole_mobile_payments_re_7udl.svg"
          width={200}
          height={200}
          alt="Create your first Expense"
        />

        <h3 className="mt-2 text-2xl font-medium text-gray-900">Create your first Expense</h3>
        <p className="mt-1 text-sm text-gray-500">Get started by creating a new Expense.</p>
      </div>
    </div>
  )
}
