import { useState, useEffect } from "react";
import { Summary as SummaryExpense } from "../../services/api/expense.services";
import { getDateFromFYDates, formatCurrency } from "../../helpers/common";
import Skeleton from "react-loading-skeleton";
import "react-loading-skeleton/dist/skeleton.css";
import YearMonthFilter from "../Common/YearMonthFilter";

export default function ExpensesAnalytic(props) {
  const [loader, setLoader] = useState(true);
  const [type, setType] = useState("YEAR"); //YEAR, MONTH
  const [total, setTotal] = useState(0);
  const [summary, setSummary] = useState([]);

  const getData = async (filterType) => {
    setType(filterType);
    let apiRunning = true;
    setLoader(apiRunning);

    let dates = getDateFromFYDates(filterType);
    let formData = "";
    formData += "?start_date=" + dates.start_date;
    formData += "&end_date=" + dates.end_date;

    const res = await SummaryExpense(formData);
    if (res.success) {
      setTotal(res.data.expenses_total);
      setSummary(res.data.expense_details);
      apiRunning = false;
    }
    if (!apiRunning) {
      setLoader(false);
    }
  };

  useEffect(() => {
    getData(type);
  }, [props.refreshSummary]);

  return (
    <div className="mt-8  xl:pr-8   w-full xl:w-1/2 2xl:w-1/2">
      <div className="grid grid-cols-1 h-full">
        <div className="bg-white overflow-hidden shadow rounded-lg">
          <div className="p-7 xl:p-8 h-full">
            <div className="hidden sm:block">
              <YearMonthFilter type={type} setType={getData} />
            </div>

            {loader ? (
              <div className="block justify-between mt-16  flex-1 2xl:flex">
                <dl className="pr-8 w-full text-center xl:text-left 2xl:w-1/2">
                  <dt className="text-lg tracking-wide pb-2 font-medium text-gray-400 truncate">
                    Total Expenses
                  </dt>
                  <dd>
                    <div className="text-3xl 2xl:text-4xl whitespace-nowrap font-bold text-gray-900">
                      <Skeleton />
                    </div>
                  </dd>
                </dl>

                <div className="right-aside mt-5 w-full 2xl:w-1/2 2xl:mt-0">
                  {Array.from(Array(5), (e, i) => {
                    return (
                      <dl className="flex pb-2 justify-between" key={i}>
                        <dt className="text-md tracking-wide pb-1 font-medium text-gray-400 truncate">
                          <Skeleton />
                        </dt>
                        <dd>
                          <div className="whitespace-nowrap font-bold text-gray-800 text-lg">
                            <Skeleton />
                          </div>
                        </dd>
                      </dl>
                    );
                  })}
                </div>
              </div>
            ) : (
              <>
                {total > 0 ? (
                  <div className="block justify-between mt-16  flex-1 2xl:flex">
                    <dl className="pr-8 w-full text-center xl:text-left 2xl:w-1/2">
                      <dt className="text-lg tracking-wide pb-2 font-medium text-gray-400 truncate">
                        Total Expenses
                      </dt>
                      <dd>
                        <div className="text-3xl 2xl:text-4xl whitespace-nowrap font-bold text-gray-900">
                          {formatCurrency(total)}
                        </div>
                      </dd>
                    </dl>

                    <div className="right-aside mt-5 w-full 2xl:w-1/2 2xl:mt-0">
                      {summary.map((card, cIndex) => (
                        <dl className="flex pb-2 justify-between" key={cIndex}>
                          <dt className="text-md tracking-wide pb-1 font-medium text-gray-400 truncate">
                            {card.account_category}
                          </dt>
                          <dd>
                            <div className="whitespace-nowrap font-bold text-gray-800 text-lg">
                              {formatCurrency(card.total)}
                            </div>
                          </dd>
                        </dl>
                      ))}
                    </div>
                  </div>
                ) : (
                  <div
                    className="flex items-center h-full text-xl justify-center font-medium text-gray-700"
                    style={{ padding: "2em" }}
                  >
                    No expenses found for this{" "}
                    {type === "YEAR" ? "year" : "month"}
                  </div>
                )}
              </>
            )}
          </div>
        </div>
      </div>
    </div>
  );
}
