import { Fragment, useState, useEffect } from "react";
import { Dialog, Transition } from "@headlessui/react";
import { XIcon } from "@heroicons/react/outline";
import { notification } from "../../services/api/user.services";
import CircularProgress from "@material-ui/core/CircularProgress";
import { notificationData } from "../../helpers/common";

export default function NotificationList(props) {
  const [loader, setLoader] = useState(true);
  const [loadingMore, setLoadingMore] = useState(false);
  const [hasMore, setHasMore] = useState(false);
  const [pageSize, setPageSize] = useState(6);
  const [page, setPage] = useState(1);
  const [notifications, setNotifications] = useState([]);

  const getData = async () => {
    let apiRunning = true;
    setLoader(apiRunning);
    let formData = `?page_size=${pageSize}&page=1`;
    const res = await notification(formData);
    if (res.success) {
      setNotifications(res.data.notification_details);
      if (res.data.next_page > 1) {
        setHasMore(true);
      } else {
        setHasMore(false);
      }
      setPage(2);
      apiRunning = false;
    }
    if (!apiRunning) {
      setLoader(false);
    }
  };

  const loadMore = async () => {
    let apiRunning = true;
    setLoadingMore(apiRunning);
    let formData = `?page_size=${pageSize}&page=${page}`;
    const res = await notification(formData);
    if (res.success) {
      setNotifications((prev) => [...prev, ...res.data.notification_details]);
      if (res.data.next_page > page) {
        setHasMore(true);
      } else {
        setHasMore(false);
      }
      setPage(page + 1);
      apiRunning = false;
    }
    if (!apiRunning) {
      setLoadingMore(false);
    }
  };

  useEffect(() => {
    if (props.open) {
      getData();
    }
  }, [props.open]);

  return (
    <Transition.Root show={props.open} as={Fragment}>
      <Dialog
        as="div"
        static
        className="fixed inset-0 overflow-hidden"
        onClose={props.setOpen}
      >
        <div className="absolute inset-0 overflow-hidden">
          <Transition.Child
            as={Fragment}
            enter="ease-in-out duration-500"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in-out duration-500"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <Dialog.Overlay className="absolute inset-0 bg-gray-500 bg-opacity-75 transition-opacity pointer-events-none" />
          </Transition.Child>

          <div className="fixed inset-y-0 max-w-full right-0 flex">
            <Transition.Child
              as={Fragment}
              enter="transform transition ease-in-out duration-500 sm:duration-700"
              enterFrom="translate-x-full"
              enterTo="translate-x-0"
              leave="transform transition ease-in-out duration-500 sm:duration-700"
              leaveFrom="translate-x-0"
              leaveTo="translate-x-full"
            >
              <div className="w-screen lg:max-w-xl">
                <div className="h-full divide-y divide-gray-200 flex flex-col bg-white shadow-xl">
                  <div className="flex-1 h-0 pb-8 overflow-y-auto">
                    <div className="border-b border-gray-200 bg-gray-100 py-4 px-4 sm:px-6">
                      <div className="flex items-center justify-between">
                        <Dialog.Title className="text-xl font-medium text-gray-800">
                          Notifications
                        </Dialog.Title>
                        <div className="ml-3 h-7 flex items-center">
                          <button
                            type="button"
                            className="rounded-full hover:bg-gray-300 p-2 text-gray-500 hover:text-gray-700 focus:outline-none focus:ring-2 focus:ring-white"
                            onClick={() => props.setOpen(false)}
                          >
                            <span className="sr-only">Close panel</span>
                            <XIcon className="h-6 w-6" aria-hidden="true" />
                          </button>
                        </div>
                      </div>
                    </div>

                    <div className=" flex-1 flex flex-col justify-between">
                      {loader ? (
                        <div className="text-center">
                          <div className="box-center">
                            <CircularProgress />
                          </div>
                        </div>
                      ) : (
                        <ul className="text-gray-800 text-base  divide-y divide-gray-200">
                          {notifications.length > 0 ? (
                            <>
                              {notifications.map((notification, key) => (
                                <li className="flex items-start p-4" key={key}>
                                  <svg
                                    className="-ml-0.5 mr-1.5 h-2 w-2 text-brand-blue relative top-2"
                                    fill="currentColor"
                                    viewBox="0 0 8 8"
                                  >
                                    <circle cx={4} cy={4} r={3} />
                                  </svg>

                                  <div className="w-full">
                                    <h6 className="font-semibold">
                                      {notification.title}
                                    </h6>
                                    <p>{notification.message}</p>
                                    <span className="text-gray-500 text-xs text-right mt-2 block font-normal">
                                      {notificationData(
                                        notification.notification_date
                                      )}
                                    </span>
                                  </div>
                                </li>
                              ))}
                              {loadingMore ? (
                                <div className="text-center mt-2">
                                  <div
                                    className="box-center"
                                    style={{ height: "50px" }}
                                  >
                                    <CircularProgress />
                                  </div>
                                </div>
                              ) : (
                                <>
                                  {hasMore && (
                                    <div className="text-center mt-2">
                                      <div
                                        className="box-center"
                                        style={{ height: "50px" }}
                                      >
                                        <div
                                          className="cursor-pointer text-brand-blue font-medium text-sm"
                                          onClick={() => loadMore()}
                                        >
                                          Load More
                                        </div>
                                      </div>
                                    </div>
                                  )}
                                </>
                              )}
                            </>
                          ) : (
                            <div className="text-center mt-10">
                              No notification found
                            </div>
                          )}
                        </ul>
                      )}
                    </div>
                  </div>
                </div>
              </div>
            </Transition.Child>
          </div>
        </div>
      </Dialog>
    </Transition.Root>
  );
}
