import Link from 'next/link';
import axios from 'axios';
import { useEffect, useState } from 'react';

export default function SolePartners() {
    const [partners, setPartners] = useState([]);
    const getPartners = async () => {
        const partnersResults = await (await axios.get('https://soleapp.com.au/wp-json/wp/v2/partner?partner_category_slug=benefit-partner&per_page=100'));
        setPartners(partnersResults.data);
    }

    useEffect(() => {
        if (partners.length === 0) {
            getPartners();
        }
    }, [partners])

    return (
        <div className="lg:grid grid-cols-2 mt-5  gap-8">
            {partners.map((partner, k) => {
                return (
                    <div key={k} className="bg-white border-gray-200 relative border rounded-md  shadow-sm flex items-center hover:border-gray-300">
                        <Link href={partner.meta_box.partner_offer_url}>
                            <a target='_blank' className='flex items-center w-full'>
                                <div className="flex items-center justify-center bg-gray-50 px-6 rounded-tl-lg rounded-bl-lg h-64 w-104">
                                    <img
                                        src={!!partner?.meta_box?.partner_logo?.full_url ? partner?.meta_box?.partner_logo?.full_url : ''}
                                        alt="Info Tech"
                                        height="100%"
                                        style={{maxHeight: '100%'}}
                                    />
                                </div>

                                <div className='px-5 py-1 w-85'>
                                    <h5 className='text-3xl text-gray-800 font-semibold'>{partner?.title?.rendered}</h5>
                                    <p className='text-gray-500 pt-5'>{partner?.meta_box?.sole_app_description}</p>
                                    <br />
                                    <i>
                                        {!!partner?.meta_box?.sole_app_discount_code_description ? partner?.meta_box?.sole_app_discount_code_description : ""}
                                        {!!partner?.meta_box?.sole_app_discount_code ? (` (${partner?.meta_box?.sole_app_discount_code})`) : ""}
                                    </i>
                                </div>
                            </a>
                        </Link>
                    </div>
                )
            })}

        </div>

    )
}
