import store from "../store/store";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import Dialog from "@material-ui/core/Dialog";
import { deleteAll } from "../services/cookies.services";
import { setTokenExpired } from "../store/actions";
import { useSelector } from "react-redux";

export default function TokenExpiredDialog() {
  const tokenExpired = useSelector(
    (state) => state.bankTransaction.tokenExpired
  );

  const handleClose = () => {
    store.store.dispatch(setTokenExpired(false));
    deleteAll();
    localStorage.clear();
    location.reload();
  };

  return (
    <>
      {tokenExpired && (
        <Dialog
          sx={{ "& .MuiDialog-paper": { width: "80%", maxHeight: 435 } }}
          maxWidth="xs"
          disablePortal
          open={true}
        >
          <DialogTitle>Session Expired</DialogTitle>
          <DialogContent dividers>
            Your session has been expired. You need to log in again to continue
            using the app.
          </DialogContent>
          <DialogActions>
            <button onClick={handleClose} className="btn-yes">
              Ok
            </button>
          </DialogActions>
        </Dialog>
      )}
    </>
  );
}
