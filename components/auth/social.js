import Image from 'next/image';
import clsx from 'clsx';
import Styles from '../../styles/Home.module.scss';
import { useEffect, useState } from "react";
import { GoogleLogin } from 'react-google-login';
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props';

import store from '../../store/store';
import { snackbarOpen } from '../../store/slices/snackbarSlice';
import Snackbar, { SNACKBAR_TYPE } from "../../components/Layout/Snackbar";
import AuthServices from "../../services/api/auth.services";
//import AppleLogin from 'react-apple-login';
import CookiesServices from "../../services/cookies.services";

const process = "Processing...";

import { useRouter } from 'next/router';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

export default function Social(props) {
    const router = useRouter();
    const [dialogOpen, setDialogOpen] = useState(false);
    const [dialogMsg, setDialogMsg] = useState('');

    const apiCall = async (formData) => {
        setDialogMsg(process);
        setDialogOpen(true);

        var result = null;

        formData.append('device_id', CookiesServices.getDeviceId());
        if (props.type == 'register') {
            result = await AuthServices.signUp(formData, 'yes');
        }
        else {
            result = await AuthServices.login(formData, 'yes');
        }
        if (result.success) {
            /*store.dispatch(snackbarOpen({
                type: SNACKBAR_TYPE.SUCCESS,
                message: 'Your action has been completed. For access more feature please use Mobile APP.',
            }));*/

            if (props.type == 'register') {
                CookiesServices.setLoginResponse(result.data.user);
                CookiesServices.setUserEmail(result.data.user.email);

                setDialogMsg('');
                setDialogOpen(false);

                router.push({
                    pathname: '/register',
                })
            }
            else {

            }
        }
        else {
            /*store.dispatch(snackbarOpen({
                type: SNACKBAR_TYPE.ERROR,
                message: result.message,
            }));*/            
            setDialogMsg(result.message);
            setDialogOpen(true);
        }
    }

    const responseGoogle = async (response) => {
        if (response) {
            if (response.error == "popup_closed_by_user")
                return false;

            if (response.profileObj && response.profileObj.googleId != '') {
                if (response.profileObj.email != '') {
                    const formData = new FormData();
                    var name = response.profileObj.name.split(' ');
                    formData.append('email', response.profileObj.email);
                    formData.append('signup_source', 3);
                    formData.append('first_name', name[0] ? name[0] : '');
                    formData.append('last_name', name[1] ? name[1] : '');

                    apiCall(formData);
                }
                else {
                    store.dispatch(snackbarOpen({
                        type: SNACKBAR_TYPE.ERROR,
                        message: 'Email is required.',
                    }));
                }
            }
            else {
                store.dispatch(snackbarOpen({
                    type: SNACKBAR_TYPE.ERROR,
                    message: response.details || 'Something went wrong, please try again later.',
                }));
            }
        }
    }

    const responseFacebook = async (response) => {
        if (response) {
            if (response.id != '') {
                if (response.email != '') {
                    const formData = new FormData();
                    var name = response.name.split(' ');
                    formData.append('email', response.email);
                    formData.append('signup_source', 2);
                    formData.append('first_name', name[0] ? name[0] : '');
                    formData.append('last_name', name[1] ? name[1] : '');

                    apiCall(formData);
                }
                else {
                    store.dispatch(snackbarOpen({
                        type: SNACKBAR_TYPE.ERROR,
                        message: 'Email is required.',
                    }));
                }
            }
            else {
                store.dispatch(snackbarOpen({
                    type: SNACKBAR_TYPE.ERROR,
                    message: 'Something went wrong, please try again later.',
                }));
            }
        }
    }

    const responseApple = async (response) => {
        if (response) {
            if (response.user) {
                if (response.email != '') {
                    const formData = new FormData();
                    formData.append('email', response.user.email);
                    formData.append('signup_source', 4);
                    formData.append('first_name', response.user.name.firstName);
                    formData.append('last_name', response.user.name.lastName);

                    apiCall(formData);
                }
                else {
                    store.dispatch(snackbarOpen({
                        type: SNACKBAR_TYPE.ERROR,
                        message: 'Email is required.',
                    }));
                }
            }
            else {
                store.dispatch(snackbarOpen({
                    type: SNACKBAR_TYPE.ERROR,
                    message: 'Something went wrong, please try again later.',
                }));
            }
        }
    }

    useEffect(async() => {
        CookiesServices.deleteAll();        
    },[]);
    
    return (
        <>
            <GoogleLogin
                //clientId="940579618961-8ms2bd9lcqfpca39pvr8fk3gt0ieh3ae.apps.googleusercontent.com"
                clientId="202415546798-f79sc453se03bv1ltg0l5utmgbk59f7n.apps.googleusercontent.com"
                render={renderProps => (
                    <button onClick={renderProps.onClick} disabled={renderProps.disabled} className={clsx(Styles.roundBtn, "grey-bg")}>
                        <Image
                            src="/images/icon/google.png"
                            alt="Google"
                            width={19}
                            height={19}
                        />
                    </button>
                )}
                onSuccess={(res) => responseGoogle(res)}
                //onFailure={responseGoogle}
                onFailure={err => { return false }}
                isSignedIn={false}
            />
            <FacebookLogin
                //appId="595673998182244"
                appId="862252904296668"
                autoLoad={false}
                callback={(res) => responseFacebook(res)}
                fields="name,email,picture"
                render={renderProps => (
                    <button onClick={renderProps.onClick} className={clsx(Styles.roundBtn, "grey-bg")}>
                        <Image src="/images/icon/fb.png" alt="Facebook" width={19} height={19} />
                    </button>
                )}
            />
            {/* <AppleLogin
                clientId="com.au.app.soleapp"
                redirectURI="https://demo-webapp-soleapp.netlify.app/"
                callback={(res) => responseApple(res)}
                render={renderProps => (
                    <button onClick={renderProps.onClick} className={clsx(Styles.roundBtn, "grey-bg")}>
                        <Image src="/images/icon/apple.webp" alt="Apple" width={19} height={19} />
                    </button>
                )}
            /> */}

            <Dialog
                open={dialogOpen}
                disableBackdropClick 
                onClose={()=> {
                    setDialogOpen(false);
                    CookiesServices.deleteAll();
                }}
                aria-describedby="alert-dialog-slide-description"                
            >
                <div style={{width:300}}>
                    <DialogTitle>Sole App</DialogTitle>
                    <DialogContent>
                        <DialogContentText id="alert-dialog-slide-description">
                            {dialogMsg != process ?  dialogMsg :  <p style={{textAlign:'center',marginTop:0}}>{dialogMsg}</p> }
                        </DialogContentText>
                    </DialogContent>
                    {dialogMsg != process ?
                        <DialogActions>
                            <Button onClick={()=> {
                                setDialogOpen(false);
                                CookiesServices.deleteAll();
                            }}>Ok</Button>
                        </DialogActions> 
                    : null}
                </div>
            </Dialog>
        </>
    );
}