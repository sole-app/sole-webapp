import Autocomplete from '@mui/material/Autocomplete';
import TextField from '@mui/material/TextField';

export default function AutocompleteCustomize(props) {
    return (
        <Autocomplete
            disablePortal
            id={props.id}
            name={props.name}
            options={props.options}
            value={props.value}
            className="p-0"
            onChange={props.onChange}
            renderInput={(params) =>
                <TextField
                    {...params}
                    label={props.label}
                    classes={props.inputClassName}
                    className="p-0 border-2 border-gray-200 form-control"
                    fullWidth={true}
                />
            }
        />
    );
}