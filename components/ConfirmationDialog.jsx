import { useState, useEffect } from 'react'
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Dialog from '@material-ui/core/Dialog';

export default function ConfirmationDialog(props) {
    const [loader, setLoader] = useState(false);
    const [title, setTitle] = useState(props.title);
    const [message, setMessage] = useState(props.message);
    const [processing, setProcessing] = useState('Processing...');

    const handleCancel = () => {
        props.onClose();
    };

    const handleOk = () => {
        setLoader(true);
        props.onClose(props.value);
    };

    useEffect(async () => {
        if (props.open) {
            setTitle(props.title || title);
            setMessage(props.message || message);
            setProcessing(props.processing || processing);
            setLoader(false);
        }
    }, [props.open]);


    return (
        <Dialog
            sx={{ '& .MuiDialog-paper': { width: '80%', maxHeight: 435 } }}
            maxWidth="xs"
            open={props.open}
        >
            <DialogTitle>{title}</DialogTitle>
            <DialogContent dividers>{message}</DialogContent>
            <DialogActions>
                <button onClick={handleCancel} className="btn-no">No</button>
                <button onClick={handleOk} className="btn-yes" disabled={loader}>{!loader ? 'Yes' : processing}</button>
            </DialogActions>
        </Dialog>
    );
}