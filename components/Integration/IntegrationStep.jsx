import { Fragment, useState } from 'react'
import { enGB } from 'date-fns/locale'
//import { DatePicker } from 'react-nice-dates'
import { Listbox, Transition } from '@headlessui/react'
//import 'react-nice-dates/build/style.css'
import ReactTooltip from 'react-tooltip';
import { InformationCircleIcon, GlobeAltIcon, CalendarIcon } from '@heroicons/react/outline'
import { CheckIcon, SelectorIcon } from '@heroicons/react/solid'


// For License Issued dropdown  
const issuedby = [
    { id: 1, name: 'New South Wales' },
    { id: 2, name: 'Queensland' },
]

function classNames(...classes) {
    return classes.filter(Boolean).join(' ')
}


export default function IntegrationStep() {
    const [dobdate, setDobDate] = useState()  //For Due date calendar
    const [expdate, setExpDate] = useState()  //For License Expiry calendar
    const [selected, setSelected] = useState(issuedby[0])

    return (
        <div className="p-10 bg-white rounded">
            <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
                <div className="flex justify-center items-center text-center">
                    <div className="border-r border-gray-400 pr-3">
                        <img
                            src="/images/logo.png"
                            alt="Sole logo"
                            width="182"
                            height="58"
                            className=""
                        />
                    </div>

                    <div>
                        <img
                            src="/images/ip-new-colour.svg"
                            alt="Instant Pay logo"
                            className="w-60 h-32 2xl:w-72 2xl:h-36 3xl:w-96 3xl:w-72"
                        />
                    </div>
                </div>

                <div className="mt-10">
                    <div className="space-y-10 md:space-y-0 md:grid md:grid-cols-2 md:gap-x-8 md:gap-y-10">
                        <div>
                            <p>
                                To help you get paid faster and easier, safely and securely integrate your Sole amount with <span className="font-bold">instant Pay </span>
                                (Australia's fastest and best priced digital e-wallet).
                            </p>

                            <p className="mt-4">
                                Seamless integrationmeans that Sole App will reconsile the transaction for you, getting you ready for tax time without
                                needing lift a  finger.
                            </p>

                            <p className="mt-4 text-sm">
                                Any questions? Refer to these <a href="#" className="underline text-blue-600">FAQ's.</a>
                            </p>

                            <div className="bg-gray-100 p-4 mt-4 mb-2 relative shadow sm:rounded-lg">
                                <p className="text-sm">
                                    <span className="font-bold">Note:</span> You will have the option to use Instant Pay or the
                                    normal invoicing process for each transaction
                                    (<a href="#" className="underline font-bold">Sole does not take any commission</a> for either payment method)
                                </p>
                            </div>
                        </div>

                        <dl className="space-y-10 grid-cols-1 md:space-y-0 md:grid md:gap-y-10">
                            <div className="relative">
                                <dt>
                                    <div className="absolute flex items-center justify-center h-12 w-12 rounded-md bg-brand-blue text-white">
                                        <GlobeAltIcon className="h-6 w-6" aria-hidden="true" />
                                    </div>
                                    <p className="ml-16 text-lg leading-6 font-medium text-gray-900">Step 1</p>
                                </dt>
                                <dd className="mt-1 ml-16 text-base text-gray-500">
                                    Make a sale , create an invoice in Sole.
                                </dd>
                            </div>

                            <div className="relative">
                                <dt>
                                    <div className="absolute flex items-center justify-center h-12 w-12 rounded-md bg-brand-blue text-white">
                                        <GlobeAltIcon className="h-6 w-6" aria-hidden="true" />
                                    </div>
                                    <p className="ml-16 text-lg leading-6 font-medium text-gray-900">Step 2</p>
                                </dt>
                                <dd className="mt-1 ml-16 text-base text-gray-500">
                                    Get paid instantly and safely using Instant Pay for only $1.50
                                </dd>
                            </div>

                            <div className="relative">
                                <dt>
                                    <div className="absolute flex items-center justify-center h-12 w-12 rounded-md bg-brand-blue text-white">
                                        <GlobeAltIcon className="h-6 w-6" aria-hidden="true" />
                                    </div>
                                    <p className="ml-16 text-lg leading-6 font-medium text-gray-900">Step 3</p>
                                </dt>
                                <dd className="mt-1 ml-16 text-base text-gray-500">
                                    Sole reconciles the transaction for you.
                                </dd>
                            </div>
                        </dl>
                    </div>

                    <div className="shadow bg-gray-50 rounded p-7 mt-6">
                        <h4 className="flex items-center text-2xl text-gray-800 font-medium">
                            Instant Pay

                            <InformationCircleIcon
                                className="h-6 w-6 text-gray-400 ml-2  focus:outline-none"
                                data-for='info'
                                data-tip='Do you need any help? '
                            />
                            <ReactTooltip id='info' className='custom-tooltip bg-gray-900' textColor='#ffffff' backgroundColor='#111827' effect='solid' />
                        </h4>

                        <p className="mt-2">
                            Fill out the form below to create your account on Instant Pay & get paid instantly.
                            <span className="block">All below fields are mandatory
                                to meet Instant Pay requirements.</span>
                        </p>

                        <form action="#" className="mt-4 flex flex-col">
                            <div className=" divide-y divide-gray-200">
                                <div className="flex-1 py-4">
                                    <h5 className="flex items-center text-lg text-gray-800 font-medium">
                                        Your Details

                                        <InformationCircleIcon
                                            className="h-6 w-6 text-gray-400 ml-2  focus:outline-none"
                                            data-for='detail'
                                            data-tip='Do you need any help? '
                                        />
                                        <ReactTooltip id='detail' className='custom-tooltip bg-gray-900' textColor='#ffffff' backgroundColor='#111827' effect='solid' />
                                    </h5>

                                    <div className="space-y-4 pt-6 pb-7 items-baseline grid grid-cols-1 gap-y-2 sm:pt-0 lg:grid-cols-3 sm:gap-x-8">
                                        <div>
                                            <div className="flex justify-between">
                                                <label htmlFor="company" className="block text-md tracking-wider font-medium text-gray-500">
                                                    First Name
                                                </label>
                                            </div>

                                            <div className="mt-1">
                                                <input
                                                    type="text"
                                                    name="First Name"
                                                    id="fname"
                                                    placeholder="e.g. demo@soleapp.com.au"
                                                    className="placeholder-gray-400 py-3 px-4 block w-full border-2 border-gray-200  focus:ring-indigo-400 focus:border-indigo-400 border-gray-100 rounded-md"
                                                />
                                            </div>
                                        </div>

                                        <div>
                                            <div className="flex justify-between">
                                                <label htmlFor="company" className="block text-md tracking-wider font-medium text-gray-500">
                                                    Middle Name
                                                </label>
                                            </div>

                                            <div className="mt-1">
                                                <input
                                                    type="text"
                                                    name="Middle Name"
                                                    id="mname"
                                                    placeholder=""
                                                    className="placeholder-gray-400 py-3 px-4 block w-full border-2 border-gray-200  focus:ring-indigo-400 focus:border-indigo-400 border-gray-100 rounded-md"
                                                />
                                            </div>
                                        </div>

                                        <div>
                                            <div className="flex justify-between">
                                                <label htmlFor="company" className="block text-md tracking-wider font-medium text-gray-500">
                                                    Last Name
                                                </label>
                                            </div>

                                            <div className="mt-1">
                                                <input
                                                    type="text"
                                                    name="Last Name"
                                                    id="lname"
                                                    placeholder=""
                                                    className="placeholder-gray-400 py-3 px-4 block w-full border-2 border-gray-200  focus:ring-indigo-400 focus:border-indigo-400 border-gray-100 rounded-md"
                                                />
                                            </div>
                                        </div>

                                        <div>
                                            <div className="flex justify-between">
                                                <label htmlFor="company" className="block text-md tracking-wider font-medium text-gray-500">
                                                    Email Address
                                                </label>
                                            </div>

                                            <div className="mt-1">
                                                <input
                                                    type="text"
                                                    name="Emaill address"
                                                    id="eaddress"
                                                    placeholder=""
                                                    className="placeholder-gray-400 py-3 px-4 block w-full border-2 border-gray-200  focus:ring-indigo-400 focus:border-indigo-400 border-gray-100 rounded-md"
                                                />
                                            </div>
                                        </div>

                                        <div>
                                            <div className="flex justify-between">
                                                <label htmlFor="company" className="block text-md tracking-wider font-medium text-gray-500">
                                                    Mobile Number
                                                </label>
                                            </div>

                                            <div className="mt-1">
                                                <input
                                                    type="text"
                                                    name="Mobile Number"
                                                    id="mnumber"
                                                    placeholder=""
                                                    className="placeholder-gray-400 py-3 px-4 block w-full border-2 border-gray-200  focus:ring-indigo-400 focus:border-indigo-400 border-gray-100 rounded-md"
                                                />
                                            </div>
                                        </div>

                                        <div>
                                            <div className="flex justify-between">
                                                <label htmlFor="company" className="block text-md tracking-wider font-medium text-gray-500">
                                                    Date of Birth
                                                </label>
                                            </div>

                                            <div className="mt-1 relative">
                                                {/*<DatePicker className="relative" date={dobdate} onDateChange={setDobDate} locale={enGB}>
                                                    {({ inputProps, focused }) => (
                                                        <input
                                                            className={'input py-3 px-4 block w-full border-2 border-red-200  focus:ring-indigo-400 focus:border-indigo-400 border-gray-100 rounded-md' + (focused ? ' -focused' : '')}
                                                            {...inputProps}
                                                            placeholder=''
                                                        />
                                                    )}
                                                </DatePicker>*/}
                                                <div className="absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-none">
                                                    <CalendarIcon className="h-5 w-5 text-blue-700" aria-hidden="true" />
                                                </div>
                                            </div>

                                            <p className="mt-2 text-sm text-red-600" id="email-error">
                                                Please select Date of Birthday.
                                            </p>
                                        </div>

                                        <div>
                                            <div className="flex justify-between">
                                                <label htmlFor="company" className="block text-md tracking-wider font-medium text-gray-500">
                                                    Business Name
                                                </label>
                                            </div>

                                            <div className="mt-1">
                                                <input
                                                    type="text"
                                                    name="Business Name"
                                                    id="businessname"
                                                    placeholder=""
                                                    className="placeholder-gray-400 py-3 px-4 block w-full border-2 border-gray-200  focus:ring-indigo-400 focus:border-indigo-400 border-gray-100 rounded-md"
                                                />
                                            </div>
                                        </div>

                                        <div>
                                            <div className="flex justify-between">
                                                <label htmlFor="company" className="block text-md tracking-wider font-medium text-gray-500">
                                                    Business Address
                                                </label>
                                            </div>

                                            <div className="mt-1">
                                                <input
                                                    type="text"
                                                    name="Business Address"
                                                    id="businessadd"
                                                    placeholder=""
                                                    className="placeholder-gray-400 py-3 px-4 block w-full border-2 border-gray-200  focus:ring-indigo-400 focus:border-indigo-400 border-gray-100 rounded-md"
                                                />
                                            </div>
                                        </div>

                                        <div>
                                            <div className="flex justify-between">
                                                <label htmlFor="company" className="block text-md tracking-wider font-medium text-gray-500">
                                                    Business Type
                                                </label>
                                            </div>

                                            <div className="mt-1">
                                                <input
                                                    type="text"
                                                    name="Business Type"
                                                    id="businesstype"
                                                    placeholder=""
                                                    className="placeholder-gray-400 py-3 px-4 block w-full border-2 border-gray-200  focus:ring-indigo-400 focus:border-indigo-400 border-gray-100 rounded-md"
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="flex-1 pt-8 pb-4">
                                    < h5 className="flex items-center text-lg text-gray-800 font-medium">
                                        Driving License Info

                                        <InformationCircleIcon
                                            className="h-6 w-6 text-gray-400 ml-2  focus:outline-none"
                                            data-for='license'
                                            data-tip='Do you need any help? '
                                        />
                                        <ReactTooltip id='license' className='custom-tooltip bg-gray-900' textColor='#ffffff' backgroundColor='#111827' effect='solid' />
                                    </h5>

                                    <div className="space-y-4  pt-6 pb-7 items-baseline grid grid-cols-1 gap-y-2 sm:pt-0 lg:grid-cols-3 sm:gap-x-8">
                                        <div>
                                            <div className="flex justify-between">
                                                <label htmlFor="company" className="block text-md tracking-wider font-medium text-gray-500">
                                                    Driving License Number
                                                </label>
                                            </div>

                                            <div className="mt-1">
                                                <input
                                                    type="text"
                                                    name="Driving license number"
                                                    id="licenseno"
                                                    placeholder="e.g. demo@soleapp.com.au"
                                                    className="placeholder-gray-400 py-3 px-4 block w-full border-2 border-gray-200  focus:ring-indigo-400 focus:border-indigo-400 border-gray-100 rounded-md"
                                                />
                                            </div>
                                        </div>

                                        <div>
                                            <div className="flex justify-between">
                                                <label htmlFor="company" className="block text-md tracking-wider font-medium text-gray-500">
                                                    Driving License Exp Date
                                                </label>
                                            </div>

                                            <div className="mt-1 relative">
                                                {/*<DatePicker className="relative" date={expdate} onDateChange={setExpDate} locale={enGB}>
                                                    {({ inputProps, focused }) => (
                                                        <input
                                                            className={'input py-3 px-4 block w-full border-2 border-gray-200  focus:ring-indigo-400 focus:border-indigo-400 border-gray-100 rounded-md' + (focused ? ' -focused' : '')}
                                                            {...inputProps}
                                                            placeholder=''
                                                        />
                                                    )}
                                                    </DatePicker>*/}
                                                <div className="absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-none">
                                                    <CalendarIcon className="h-5 w-5 text-blue-700" aria-hidden="true" />
                                                </div>
                                            </div>
                                        </div>

                                        <div>
                                            <div className="flex justify-between">
                                                <label htmlFor="company" className="block text-md tracking-wider font-medium text-gray-500">
                                                    License Issued By
                                                </label>
                                            </div>

                                            <div className="mt-1">
                                                <Listbox value={selected} onChange={setSelected}>
                                                    {({ open }) => (
                                                        <>
                                                            <div className="mt-1 relative">
                                                                <Listbox.Button className="py-3 px-4 block relative text-left cursor-default  w-full border-2 border-gray-200  focus:ring-indigo-400 focus:border-indigo-400 border-gray-100 rounded-md">
                                                                    <span className="block truncate">{selected.name}</span>
                                                                    <span className="absolute inset-y-0 right-0 flex items-center pr-2 pointer-events-none">
                                                                        <SelectorIcon className="h-5 w-5 text-gray-400" aria-hidden="true" />
                                                                    </span>
                                                                </Listbox.Button>
                                                                <Transition
                                                                    show={open}
                                                                    as={Fragment}
                                                                    leave="transition ease-in duration-100"
                                                                    leaveFrom="opacity-100"
                                                                    leaveTo="opacity-0"
                                                                >
                                                                    <Listbox.Options
                                                                        static
                                                                        className="absolute z-10 mt-1 w-full bg-white shadow-lg max-h-60 rounded-md py-1 text-base ring-1 ring-black ring-opacity-5 overflow-auto focus:outline-none sm:text-sm"
                                                                    >
                                                                        {issuedby.map((terms) => (
                                                                            <Listbox.Option
                                                                                key={terms.id}
                                                                                className={({ active }) =>
                                                                                    classNames(
                                                                                        active ? 'text-white bg-brand-blue' : 'text-gray-900',
                                                                                        'cursor-default select-none relative py-2 pl-3 pr-9'
                                                                                    )
                                                                                }
                                                                                value={terms}
                                                                            >
                                                                                {({ selected, active }) => (
                                                                                    <>
                                                                                        <span className={classNames(selected ? 'font-semibold' : 'font-normal', 'block truncate')}>
                                                                                            {terms.name}
                                                                                        </span>

                                                                                        {selected ? (
                                                                                            <span
                                                                                                className={classNames(
                                                                                                    active ? 'text-white' : 'text-brand-blue',
                                                                                                    'absolute inset-y-0 right-0 flex items-center pr-4'
                                                                                                )}
                                                                                            >
                                                                                                <CheckIcon className="h-5 w-5" aria-hidden="true" />
                                                                                            </span>
                                                                                        ) : null}
                                                                                    </>
                                                                                )}
                                                                            </Listbox.Option>
                                                                        ))}
                                                                    </Listbox.Options>
                                                                </Transition>
                                                            </div>
                                                        </>
                                                    )}
                                                </Listbox>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="flex-1 pt-8 pb-4">
                                    <h5 className="flex items-center text-lg text-gray-800 font-medium">
                                        Bank Account

                                        <InformationCircleIcon
                                            className="h-6 w-6 text-gray-400 ml-2  focus:outline-none"
                                            data-for='bankac'
                                            data-tip='Do you need any help? '
                                        />
                                        <ReactTooltip id='bankac' className='custom-tooltip bg-gray-900' textColor='#ffffff' backgroundColor='#111827' effect='solid' />
                                    </h5>

                                    <div className="space-y-4  pt-6 pb-7 items-baseline grid grid-cols-1 gap-y-2 sm:pt-0 lg:grid-cols-3 sm:gap-x-8">
                                        <div>
                                            <div className="flex justify-between">
                                                <label htmlFor="company" className="block text-md tracking-wider font-medium text-gray-500">
                                                    Bank Account Name
                                                </label>
                                            </div>

                                            <div className="mt-1">
                                                <input
                                                    type="text"
                                                    name="Account Name"
                                                    id="acname"
                                                    placeholder="e.g. demo@soleapp.com.au"
                                                    className="placeholder-gray-400 py-3 px-4 block w-full border-2 border-gray-200  focus:ring-indigo-400 focus:border-indigo-400 border-gray-100 rounded-md"
                                                />
                                            </div>
                                        </div>

                                        <div>
                                            <div className="flex justify-between">
                                                <label htmlFor="company" className="block text-md tracking-wider font-medium text-gray-500">
                                                    Bank Account Number
                                                </label>
                                            </div>

                                            <div className="mt-1 relative">
                                                <input
                                                    type="text"
                                                    name="Bank Account Number"
                                                    id="acnumber"
                                                    placeholder=""
                                                    className="placeholder-gray-400 py-3 px-4 block w-full border-2 border-gray-200  focus:ring-indigo-400 focus:border-indigo-400 border-gray-100 rounded-md"
                                                />
                                            </div>
                                        </div>

                                        <div>
                                            <div className="flex justify-between">
                                                <label htmlFor="company" className="block text-md tracking-wider font-medium text-gray-500">
                                                    Bank Account BSB
                                                </label>
                                            </div>

                                            <div className="mt-1">
                                                <input
                                                    type="text"
                                                    name="Bank Account BSB"
                                                    id="bsb"
                                                    placeholder=""
                                                    className="placeholder-gray-400 py-3 px-4 block w-full border-2 border-gray-200  focus:ring-indigo-400 focus:border-indigo-400 border-gray-100 rounded-md"
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="flex justify-center">
                                <button
                                    type="submit"
                                    className="ml-4 inline-flex justify-center py-2 px-4 tracking-wider border border-transparent uppercase shadow-sm  font-medium text-sm rounded-full text-white bg-green-600 hover:bg-green-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-500"
                                >
                                    Submit
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
}
