import TablePagination from '@material-ui/core/TablePagination';

export default function Pagination(props) {

    return (
        <TablePagination
            component="div"
            count={props.total}
            page={props.page}
            rowsPerPageOptions={[10, 25, 50, 100]}
            onPageChange={props.onPageChange}
            rowsPerPage={props.rowsPerPage}
            onRowsPerPageChange={props.onRowsPerPageChange}
        />
    );
}