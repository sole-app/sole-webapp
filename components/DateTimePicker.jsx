import DatePicker from "react-datepicker";
import { CalendarIcon } from "@heroicons/react/outline";
import { range } from "../helpers/common";
import "react-datepicker/dist/react-datepicker.css";
import { getYear, getMonth } from "date-fns";

export default function DateTimePicker(props) {
  const years = range(1990, getYear(new Date()) + 1 + 1, 1);
  const months = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];

  if (typeof props.range !== "undefined" && props.range) {
    return (
      <>
        <DatePicker
          renderCustomHeader={({
            date,
            changeYear,
            changeMonth,
            decreaseMonth,
            increaseMonth,
            prevMonthButtonDisabled,
            nextMonthButtonDisabled,
          }) => (
            <div
              style={{
                margin: 10,
                display: "flex",
                justifyContent: "center",
              }}
            >
              <button
                onClick={decreaseMonth}
                disabled={prevMonthButtonDisabled}
                className="mr-1"
              >
                {<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-4 h-4">
                  <path stroke-linecap="round" stroke-linejoin="round" d="M15.75 19.5L8.25 12l7.5-7.5" />
                </svg>
                }
              </button>
              <select
                value={getYear(date)}
                onChange={({ target: { value } }) => changeYear(value)}
                className="border-2 focus:ring-indigo-400 focus:border-indigo-400 border-gray-200 rounded-md mr-1"
              >
                {years.map((option) => (
                  <option key={option} value={option}>
                    {option}
                  </option>
                ))}
              </select>

              <select
                value={months[getMonth(date)]}
                onChange={({ target: { value } }) =>
                  changeMonth(months.indexOf(value))
                }
                className="border-2 focus:ring-indigo-400 focus:border-indigo-400 border-gray-200 rounded-md ml-1"
              >
                {months.map((option) => (
                  <option key={option} value={option}>
                    {option}
                  </option>
                ))}
              </select>

              <button
                onClick={increaseMonth}
                disabled={nextMonthButtonDisabled}
                className="ml-1"
              >
                {<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-4 h-4">
                  <path stroke-linecap="round" stroke-linejoin="round" d="M8.25 4.5l7.5 7.5-7.5 7.5" />
                </svg>
                }
              </button>
            </div>
          )}
          startDate={props.startDate}
          endDate={props.endDate}
          onChange={props.onDateChange}
          name={props.name}
          id={props.name}
          className={props.inputClassName}
          dateFormat="dd/MM/yyyy"
          minDate={props.minDate}
          maxDate={props.maxDate}
          placeholderText={props.placeholder}
          selectsRange={true}
          isClearable={true}
          disabled={
            typeof props.disabled !== "undefined" ? props.disabled : false
          }
        />
        <div className="absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-none">
          <CalendarIcon className="h-5 w-5 text-blue-700" aria-hidden="true" />
        </div>
      </>
    );
  }

  return (
    <>
      <DatePicker
        renderCustomHeader={({
          date,
          changeYear,
          changeMonth,
          decreaseMonth,
          increaseMonth,
          prevMonthButtonDisabled,
          nextMonthButtonDisabled,
        }) => (
          <div
            style={{
              margin: 10,
              display: "flex",
              justifyContent: "center",
            }}
          >
            <button onClick={decreaseMonth} disabled={prevMonthButtonDisabled}   className="mr-1">
              {<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-4 h-4">
                <path stroke-linecap="round" stroke-linejoin="round" d="M15.75 19.5L8.25 12l7.5-7.5" />
              </svg>
              }
            </button>
            <select
              value={getYear(date)}
              onChange={({ target: { value } }) => changeYear(value)}
              className="border-2 focus:ring-indigo-400 focus:border-indigo-400 border-gray-200 rounded-md mr-1"
            >
              {years.map((option) => (
                <option key={option} value={option}>
                  {option}
                </option>
              ))}
            </select>

            <select
              value={months[getMonth(date)]}
              onChange={({ target: { value } }) =>
                changeMonth(months.indexOf(value))
              }
              className="border-2 focus:ring-indigo-400 focus:border-indigo-400 border-gray-200 rounded-md ml-1"
            >
              {months.map((option) => (
                <option key={option} value={option}>
                  {option}
                </option>
              ))}
            </select>

            <button onClick={increaseMonth} disabled={nextMonthButtonDisabled}   className="ml-1">
              {<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-4 h-4">
                <path stroke-linecap="round" stroke-linejoin="round" d="M8.25 4.5l7.5 7.5-7.5 7.5" />
              </svg>
              }
            </button>
          </div>
        )}
        selected={props.date}
        //onChange={(date) => setStartDate(date)}
        onChange={props.onDateChange}
        name={props.name}
        id={props.name}
        className={props.inputClassName}
        dateFormat="dd/MM/yyyy"
        minDate={props.minDate}
        maxDate={props.maxDate}
        placeholderText={props.placeholder}
        disabled={
          typeof props.disabled !== "undefined" ? props.disabled : false
        }
      />
      <div className="absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-none">
        <CalendarIcon className="h-5 w-5 text-blue-700" aria-hidden="true" />
      </div>
    </>
  );
}
