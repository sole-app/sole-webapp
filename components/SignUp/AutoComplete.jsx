import React, { Component } from "react";
import PlacesAutocomplete  from "react-places-autocomplete";
import { TextField } from '@material-ui/core';
import clsx from 'clsx';
import Styles from '../../styles/Register.module.scss';
import { IconContext } from "react-icons";
import { IoIosPin } from "react-icons/io";
import { HiOutlineLocationMarker } from "react-icons/hi";

const searchOptions = {
    componentRestrictions: { country: ['au'] },
};

class AutoComplete extends Component {
    constructor(props) {
        super(props);

        this.state = {
            name: props.field.name,
            address: props.value || ""
        };
    }

    handleError = error => {
        this.props.form.setFieldError(this.state.name, error);
    };

    handleChange = address => {
        this.setState(() => {
            this.props.form.setFieldTouched(`${this.state.name}.value`);
            this.props.form.setFieldTouched(`${this.state.name}.address`);
            this.props.form.setFieldValue(this.state.name, address);
            return { address };
        });
    };

    handleSelect = address => {
        this.setState(() => {
            this.props.form.setFieldValue(this.state.name, address);
            return { address };
        });
    };

    render() {
        const {
            field: { name, ...field }, // { name, value, onChange, onBlur }
            form: { touched, errors }, // also values, setXXXX, handleXXXX, dirty, isValid, status, etc.
            classes,
            label,
            ...props
        } = this.props;

        const error = errors[name];
        const touch = touched[name];
        //console.log(this.state);
        return (
            <PlacesAutocomplete
                searchOptions={searchOptions}
                name={name}
                id={name}
                {...props}
                value={this.state.address}
                onChange={this.handleChange}
                onSelect={this.handleSelect}
                onError={this.handleError}
            >
                {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
                    <div className={clsx(Styles.formGroup, "w100", " p-rel")}>
                        <TextField
                            {...getInputProps({
                                placeholder: "Search Places ...",
                                className: "location-search-input form-control"
                            })}
                            label="Address"
                            name="address_line1"
                            id="address_line1"
                            type="text"
                            variant="outlined"
                            className="w100"
                        />
                        <div className="autocomplete-dropdown-container">
                            {loading && <div>Loading...</div>}
                            {suggestions.map(suggestion => {
                                const className = suggestion.active
                                    ? "suggestion-item--active"
                                    : "suggestion-item";
                                // inline style for demonstration purpose
                                const style = suggestion.active
                                    ? { backgroundColor: "#fafafa", cursor: "pointer" }
                                    : { backgroundColor: "#ffffff", cursor: "pointer" };
                                return (
                                    <div
                                        {...getSuggestionItemProps(suggestion, {
                                            className,
                                            style
                                        })}
                                        className="dFlex"
                                    >
                                        <IconContext.Provider value={{ color: "#b2b2b2", className: 'p-rel', style: { top: '2px', fontSize: "14px" } }}>
                                             <IoIosPin/>
                                       </IconContext.Provider> <span className="place">{suggestion.description}</span>
                                    </div>
                                );
                            })}
                        </div>
                    </div>
                )}
            </PlacesAutocomplete>
        );
    }
}

export default AutoComplete;
