import { Fragment, useState } from 'react'
import { Dialog, Transition } from '@headlessui/react'
import { XIcon } from '@heroicons/react/outline'

export default function Feedback() {
  const [openPopup, setOpenPopup] = useState(true)

  return (
    <Transition.Root show={openPopup} as={Fragment}>
      <Dialog as="div" className="fixed z-10 inset-0 overflow-y-auto" onClose={setOpenPopup}>
        <div className="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <Dialog.Overlay className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity pointer-events-none" />
          </Transition.Child>

          {/* This element is to trick the browser into centering the modal contents. */}
          <span className="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">
            &#8203;
          </span>
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
            enterTo="opacity-100 translate-y-0 sm:scale-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100 translate-y-0 sm:scale-100"
            leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
          >
            <div className="relative inline-block align-bottom bg-white rounded-lg px-4 pt-5 pb-4 text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-xl sm:w-full sm:p-6">
              <div className="hidden sm:block absolute top-0 right-0 pt-4 pr-4">
                <button
                  type="button"
                  className="bg-white rounded-md text-gray-400 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                  onClick={() => setOpenPopup(false)}
                >
                  <span className="sr-only">Close</span>
                  <XIcon className="h-6 w-6" aria-hidden="true" />
                </button>
              </div>
              <div className="sm:flex sm:items-start">

                <div className="mt-4 text-center sm:mt-0 sm:ml-4 sm:text-left">
                  <Dialog.Title as="h3" className="text-lg leading-6 font-medium text-gray-900">
                    Your Input is valuable to us
                  </Dialog.Title>
                  <div className="mt-1">
                    <p className="text-sm text-gray-500">
                      Please take  moment to answer these questions.
                    </p>
                  </div>

                  <div className="mt-5">
                    <p className="text-sm font-semibold text-gray-700">
                      How likely are you to recommend Sole to your friends?
                    </p>
                  </div>

                  <div className="mt-5">
                    <div className='flex items-center justify-between'>
                      <span className='text-sm'>Not Likely</span>

                      <span className='text-sm'>Extremely Likely</span>
                    </div>
                  </div>

                  <div className='mt-3'>
                    <div className="rating">
                      <input type="radio" name="rating" id="rata10" /><label for="rata10">10</label>
                      <input type="radio" name="rating" id="rata9" /><label for="rata9">9</label>
                      <input type="radio" name="rating" id="rata8" /><label for="rata8">8</label>
                      <input type="radio" name="rating" id="rata7" /><label for="rata7">7</label>
                      <input type="radio" name="rating" id="rata6" /><label for="rata6">6</label>
                      <input type="radio" name="rating" id="rata5" /><label for="rata5">5</label>
                      <input type="radio" name="rating" id="rata4" /><label for="rata4">4</label>
                      <input type="radio" name="rating" id="rata3" /><label for="rata3">3</label>
                      <input type="radio" name="rating" id="rata2" /><label for="rata2">2</label>
                      <input type="radio" name="rating" id="rata1" /><label for="rata1">1</label>
                    </div>
                  </div>
                  <div className="mt-5">
                    <label htmlFor="company" className="block text-md tracking-wider font-medium text-gray-500">
                      Why do you feel that way?
                    </label>

                    <div className="mt-1">
                      <textarea
                        id="description"
                        name="description"
                        rows={3}
                        className="max-w-fullpy-3 px-4 block w-full border-2  focus:ring-indigo-400 focus:border-indigo-400 border-gray-200 rounded-md "
                        defaultValue={''}
                      />
                    </div>
                  </div>
                </div>
              </div>

              <div className="mt-6 sm:mt-6 justify-center sm:flex sm:flex-row-reverse">
                <button
                  type="button"
                  className="w-full inline-flex justify-center rounded-full border border-transparent shadow-sm px-4 py-2 tracking-wide text-base font-medium text-white bg-brand-blue hover:bg-brand-darkblue focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-500 sm:w-auto sm:text-base"
                >
                  Submit
                </button>

              </div>
            </div>
          </Transition.Child>
        </div>
      </Dialog>
    </Transition.Root>
  )
}
