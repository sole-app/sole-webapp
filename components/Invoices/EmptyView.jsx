export default function EmptyView() {
  return (
    <div className="flex items-center m-7 justify-center shadow h-3/4 rounded-md bg-white">
      <div className="text-center">
        <img
          src="/images/icon/undraw_receipt_re_fre3.svg"
          width={200}
          height={200}
          alt="Create your first Invoice"
        />
        
        <h3 className="mt-2 text-2xl font-medium text-gray-900">Create your first Invoice</h3>
        <p className="mt-1 text-sm text-gray-500">Get started by creating a new Invoice.</p>

      </div>
    </div>
  )
}
