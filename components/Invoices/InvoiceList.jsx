import { Fragment, useState, useEffect } from "react";
import { Menu, Listbox, Transition } from "@headlessui/react";
import { CheckIcon, SelectorIcon } from "@heroicons/react/solid";
import { SearchIcon, DotsVerticalIcon } from "@heroicons/react/outline";
import Skeleton from "react-loading-skeleton";
import "react-loading-skeleton/dist/skeleton.css";
import ConfirmationDialog from "../ConfirmationDialog";
import AlertDialog from "../AlertDialog";
import {
  List,
  Delete,
  PaidWithCash as PaidWithCashInvoice,
  download as downloadInvoice,
} from "../../services/api/invoice.services";
import Pagination from "../Pagination";
import config from "../../config/config";
import NumberFormat from "react-number-format";
import { formatShortDate } from "../../helpers/common";
import ReactTooltip from "react-tooltip";
import { FiDownload } from "react-icons/fi";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import Dialog from "@material-ui/core/Dialog";
import { ImpulseSpinner } from "react-spinners-kit";

const statusStyles = {
  draft: "bg-gray-200 text-gray-800",
  due: "bg-indigo-200 text-indigo-800",
  overdue: "bg-red-200 text-red-800",
  paid: "bg-green-200 text-green-800",
};

// For status dropdown
const statuses = [
  { id: "all", name: "All Status" },
  { id: "draft", name: "Draft" },
  { id: "due", name: "Due" },
  { id: "overdue", name: "Overdue" },
  { id: "paid", name: "Paid" },
];

let globalSearch = "";
let apiRunning = false;

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

export default function InvoiceList(props) {
  const { setRefreshSummary, refreshSummary } = props;
  const [loader, setLoader] = useState(true);
  const [downloadLoader, setDownloadLoader] = useState(false);
  const [invoiceIds, setInvoiceIds] = useState([]);
  const [records, setRecords] = useState([]);
  const [confirmation, setConfirmation] = useState(false);
  const [deleteId, setDeleteId] = useState("");
  const [activeKey, setActiveKey] = useState("");
  const [selected, setSelected] = useState(statuses[0]);
  const [search, setSearch] = useState("");
  const [alert, setAlert] = useState(false);
  const [showDownloadDialog, setShowDownloadDialog] = useState(false);
  const [deleteType, setDeleteType] = useState("NORMAL");
  const [allInvoices, setAllInvoices] = useState(false);

  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [paginationObj, setpaginationObj] = useState([]);
  const [openAlert, setOpenAlert] = useState(false);
  const [msg, setMsg] = useState("");
  const [alertType, setAlertType] = useState("Error");

  // GET ALL LIST
  const getList = async (
    newPage,
    newRowPerPage,
    filter = {},
    resultCheck = false
  ) => {
    props.setIsListUpdate(false);

    setLoader(true);
    let formData = "";
    let filterByName;
    formData += "?page_size=" + newRowPerPage;
    formData += "&page=" + (newPage + 1);
    formData += "&sort_by[invoice_id]=desc";

    if (typeof filter.search !== "undefined") {
      formData += "&filter_by[search]=" + filter.search;
      filterByName = filter.search;
    } else {
      formData += "&filter_by[search]=" + search;
      filterByName = search;
    }

    if (typeof filter.status !== "undefined") {
      formData += "&filter_by[status]=" + filter.status.id;
    } else {
      formData += "&filter_by[status]=" + selected.id;
    }

    apiRunning = true;
    const result = await List(formData);
    if (result.success) {
      if (resultCheck) {
        if (filterByName === globalSearch) {
          setRecords(result.data.invoice_details);
          apiRunning = false;
          setpaginationObj({
            total: result.data.total || 0,
          });
        }
      } else {
        setRecords(result.data.invoice_details);
        apiRunning = false;
        setpaginationObj({
          total: result.data.total || 0,
        });
      }
    }
    if (!apiRunning) {
      setLoader(false);
    }
  };

  const changeSearch = (event) => {
    globalSearch = event.target.value;
    setSearch(event.target.value);
  };

  const changeStatus = (statusData) => {
    let filter = {
      status: statusData,
    };
    setSelected(statusData);
    getList(page, rowsPerPage, filter);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
    getList(newPage, rowsPerPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value));
    getList(page, parseInt(event.target.value));
  };

  useEffect(() => {
    if (props.isListUpdate) {
      setLoader(true);
      getList(0, rowsPerPage);
    }
  }, [props.isListUpdate]);

  useEffect(() => {
    getList(page, rowsPerPage, {}, true);
  }, [search]);

  const handleConfirmationClose = async (id) => {
    if (id) {
      const formData = new FormData();
      formData.append("id", id);

      const result = await Delete(formData);
      if (result.success) {
        records.splice(activeKey, 1);
        if (records.length === 0) {
          props.setEmptyView(true);
        }
        setRecords(records);
        setConfirmation(false);
      } else {
        setConfirmation(false);
      }
      setDeleteId("");
      setRefreshSummary(!refreshSummary);
    } else {
      setDeleteId("");
      setConfirmation(false);
    }
  };

  const paidWidthCash = async (id) => {
    if (id) {
      const formData = new FormData();
      formData.append("invoice_id", id);

      const result = await PaidWithCashInvoice(formData);
      if (result.success) {
        getList(page, rowsPerPage, {});
        setConfirmation(false);
      } else {
        setConfirmation(false);
      }
      setDeleteId("");
    } else {
      setDeleteId("");
      setConfirmation(false);
    }
  };

  const changeAlert = (type = "") => {
    setAlert(!alert);
  };

  const invStatusStyle = (status) => {
    switch (status) {
      case "draft":
        return (
          <span className="px-4 py-1 inline-flex text-xs leading-5 font-medium rounded-full bg-gray-200 text-gray-800">
            {status.toUpperCase()}
          </span>
        );
      case "overdue":
        return (
          <span className="px-4 py-1 inline-flex text-xs leading-5 font-medium rounded-full bg-red-100 text-red-500">
            {status.toUpperCase()}
          </span>
        );
      case "due":
        return (
          <span className="px-4 py-1 inline-flex text-xs leading-5 font-medium rounded-full bg-indigo-100 text-indigo-800">
            {status.toUpperCase()}
          </span>
        );
      case "paid":
        return (
          <span className="px-4 py-1 inline-flex text-xs leading-5 font-medium rounded-full bg-green-100 text-green-800">
            {status.toUpperCase()}
          </span>
        );
      default:
        return (
          <span className="px-4 py-1 inline-flex text-xs leading-5 font-medium rounded-full bg-gray-200 text-gray-800">
            {status.toUpperCase()}
          </span>
        );
    }
  };

  const changeCheckbox = (id) => {
    let index = invoiceIds.indexOf(id);
    if (index === -1) {
      //add
      setInvoiceIds([...invoiceIds, ...[id]]);
    } else {
      //remove
      invoiceIds.splice(index, 1);
      setInvoiceIds([...invoiceIds]);
    }
  };

  const download = async () => {
    if (invoiceIds.length <= 0) {
      setAlertType("Error");
      setMsg("Please select invoice to download");
      setOpenAlert(true);
      return;
    } else {
      if (showDownloadDialog) {
        setDownloadLoader(true);
        let formData = new FormData();
        for (let i = 0; i < invoiceIds.length; i++) {
          formData.append("invoices[]", invoiceIds[i]);
        }

        const result = await downloadInvoice(formData);
        if (result.success) {
          let data = result.data.zip;
          if (window.navigator && window.navigator.msSaveOrOpenBlob) {
            let byteCharacters = atob(data);
            let byteNumbers = new Array(byteCharacters.length);
            for (let i = 0; i < byteCharacters.length; i++) {
              byteNumbers[i] = byteCharacters.charCodeAt(i);
            }
            let byteArray = new Uint8Array(byteNumbers);
            let blob = new Blob([byteArray], {
              type: " application/zip",
            });
            let fileName = "invoices.zip";
            window.navigator.msSaveOrOpenBlob(blob, fileName);
          } else {
            // decode base64 string, remove space for IE compatibility
            let binary = atob(data.replace(/\s/g, ""));
            let len = binary.length;
            let buffer = new ArrayBuffer(len);
            let view = new Uint8Array(buffer);
            for (let i = 0; i < len; i++) {
              view[i] = binary.charCodeAt(i);
            }

            // create the blob object with content-type " application/zip"
            let blob = new Blob([view], { type: " application/zip" });
            window.URL = window.URL || window.webkitURL;
            let url = window.URL.createObjectURL(blob);
            window.open(url);
          }
        }

        setDownloadLoader(false);
        setShowDownloadDialog(false);
        setInvoiceIds([]);
        setAllInvoices(false);
      } else {
        setShowDownloadDialog(true);
      }
    }
  };

  const cancelDownloadInvoices = () => {
    setShowDownloadDialog(false);
  };

  const selectAllInvoices = (event) => {
    if (event.target.checked) {
      setAllInvoices(true);
      let ids = invoiceIds;
      records.forEach((value, index) => {
        let each_invoice_id = value.invoice_id;
        let each_invoice_index = ids.indexOf(each_invoice_id);
        if (each_invoice_index === -1) {
          ids.push(each_invoice_id);
        }
        setInvoiceIds(ids);
      });
    } else {
      setAllInvoices(false);
      setInvoiceIds([]);
    }
  };

  return (
    <div className="mt-8 w-full flex flex-col">
      <Dialog
        sx={{ "& .MuiDialog-paper": { width: "80%", maxHeight: 435 } }}
        maxWidth="xs"
        disablePortal
        open={showDownloadDialog}
      >
        <DialogTitle>Download Invoices</DialogTitle>
        <DialogContent dividers>
          Do you want to download the selected invoices? <br></br>
          <br></br>
          Depending on how many invoices you have selected, this process may
          take up to a few minutes. Please do not navigate away from this page.
        </DialogContent>
        <DialogActions>
          <button onClick={cancelDownloadInvoices} className="btn-no">
            No
          </button>
          <button onClick={download} className="btn-yes">
            {downloadLoader ? (
              <span className="w-full flex align-items-center justify-content-center">
                Downloading...&nbsp;
                <ImpulseSpinner
                  frontColor={"white"}
                  backColor="rgba(255,255,255, 0.5)"
                />
              </span>
            ) : (
              <span>Yes</span>
            )}
          </button>
        </DialogActions>
      </Dialog>
      <ConfirmationDialog
        id="ringtone-menu"
        title="Remove Invoice?"
        message={
          deleteType === "NORMAL"
            ? "This invoice will be permanently deleted."
            : "This invoice is already reconciled, are you sure you want to delete and undo the reconcilation?"
        }
        keepMounted
        open={confirmation}
        onClose={handleConfirmationClose}
        value={deleteId}
      />
      <AlertDialog
        id="ringtone-menu2"
        title="Note"
        message="This invoice is already reconciled and can't be edited. You can open in view mode only."
        keepMounted
        open={alert}
        onClose={changeAlert}
      />
      <AlertDialog
        id="ringtone-menu2"
        title={alertType}
        message={msg}
        keepMounted
        open={openAlert}
        onClose={setOpenAlert}
      />
      <div className="-my-2 w-full shadow bg-white overflow-hidden border-b border-gray-200 sm:rounded-lg ">
        <div className="pt-2 w-full align-middle inline-block min-w-full">
          <div>
            <div className="flex w-full gap-5 p-4">
              <div className="download w-[3%] mt-1">
                <span
                  onClick={() => download()}
                  data-for="download"
                  data-tip=""
                  className="bg-gray-100 hover:bg-gray-200 flex justify-center relative w-full border border-gray-300 rounded-md shadow-sm px-3 py-3 text-left cursor-pointer focus:outline-none"
                >
                  <FiDownload />
                  <ReactTooltip
                    id="download"
                    className="custom-tooltip bg-gray-900"
                    border
                    textColor="#ffffff"
                    backgroundColor="#111827"
                    effect="solid"
                  >
                    <p className="w-20">Download</p>
                  </ReactTooltip>
                </span>
              </div>

              <div className="mt-1 w-[80%] relative rounded-md">
                <div className="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
                  <SearchIcon
                    className="h-5 w-5 text-gray-500"
                    aria-hidden="true"
                  />
                </div>
                <input
                  type="text"
                  name="search"
                  id="search"
                  className="bg-gray-100 focus:ring-indigo-500 focus:border-indigo-500 block w-full pl-10 sm:text-sm p-3 border-gray-300 rounded-md"
                  placeholder="Invoice # or Client"
                  onChange={changeSearch}
                />
              </div>

              {/* Status dropdown */}
              <div className="w-[17%]">
                <Listbox value={selected} onChange={changeStatus}>
                  {({ open }) => (
                    <>
                      <div className="mt-1 relative">
                        <Listbox.Button className="bg-gray-100 relative w-full border border-gray-300 rounded-md shadow-sm pl-3 pr-10 py-3 text-left cursor-default focus:outline-none focus:ring-1 focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
                          <span className="block truncate">
                            {selected.name}
                          </span>
                          <span className="absolute inset-y-0 right-0 flex items-center pr-2 pointer-events-none">
                            <SelectorIcon
                              className="h-5 w-5 text-gray-400"
                              aria-hidden="true"
                            />
                          </span>
                        </Listbox.Button>

                        <Transition
                          show={open}
                          as={Fragment}
                          leave="transition ease-in duration-100"
                          leaveFrom="opacity-100"
                          leaveTo="opacity-0"
                        >
                          <Listbox.Options
                            static
                            className="absolute z-10 mt-1 w-full bg-white shadow-lg max-h-60 rounded-md py-1 text-base ring-1 ring-black ring-opacity-5 overflow-auto focus:outline-none sm:text-sm"
                          >
                            {statuses.map((status) => (
                              <Listbox.Option
                                key={status.id}
                                className={({ active }) =>
                                  classNames(
                                    active
                                      ? "text-white bg-brand-blue"
                                      : "text-gray-900",
                                    "cursor-default select-none relative py-2 pl-3 pr-9"
                                  )
                                }
                                value={status}
                              >
                                {({ selected, active }) => (
                                  <>
                                    <span
                                      className={classNames(
                                        selected
                                          ? "font-semibold"
                                          : "font-normal",
                                        "block truncate"
                                      )}
                                    >
                                      {status.name}
                                    </span>

                                    {selected ? (
                                      <span
                                        className={classNames(
                                          active
                                            ? "text-white"
                                            : "text-brand-blue",
                                          "absolute inset-y-0 right-0 flex items-center pr-4"
                                        )}
                                      >
                                        <CheckIcon
                                          className="h-5 w-5"
                                          aria-hidden="true"
                                        />
                                      </span>
                                    ) : null}
                                  </>
                                )}
                              </Listbox.Option>
                            ))}
                          </Listbox.Options>
                        </Transition>
                      </div>
                    </>
                  )}
                </Listbox>
              </div>
            </div>

            <div className="overflow-y-auto w-full overflow-x-auto">
              <table className="min-w-full  divide-y hover-row divide-gray-200">
                <thead className="">
                  <tr>
                    <th
                      scope="col"
                      className="px-7 py-3 text-left text-sm font-medium text-gray-500 uppercase tracking-wider"
                    >
                      <input
                        id={`all_invoices`}
                        name={`all_invoices`}
                        type="checkbox"
                        value={allInvoices}
                        onClick={(e) => selectAllInvoices(e)}
                        className="focus:ring-indigo-500 h-4 w-4 text-brand-blue  border-1 border-gray-300 rounded-xs bg-white"
                        defaultChecked={false}
                        checked={allInvoices}
                      />
                    </th>
                    <th
                      scope="col"
                      className="px-7 py-3 text-left text-sm font-medium text-gray-500 uppercase tracking-wider"
                    >
                      #
                    </th>
                    <th
                      scope="col"
                      className="px-7 py-3 text-left text-sm font-medium text-gray-500 uppercase tracking-wider"
                    >
                      Invoice #
                    </th>
                    <th
                      scope="col"
                      className="px-7 py-3  text-left text-sm font-medium text-gray-500 uppercase tracking-wider"
                    >
                      Client Name
                    </th>
                    <th
                      scope="col"
                      className="px-7 py-3 min-w-96 text-left text-sm font-medium text-gray-500 uppercase tracking-wider"
                    >
                      Description
                    </th>
                    {/* <th scope="col" className="px-7 py-3 text-left text-sm font-medium text-gray-500 uppercase tracking-wider">Invoice Date</th> */}
                    <th
                      scope="col"
                      className="px-7 py-3 text-left text-sm font-medium text-gray-500 uppercase tracking-wider"
                    >
                      Payment Terms
                    </th>
                    <th
                      scope="col"
                      className="px-7 py-3 text-left text-sm font-medium text-gray-500 uppercase tracking-wider"
                    >
                      Due Date
                    </th>
                    <th
                      scope="col"
                      className="px-7 py-3 text-left text-sm font-medium text-gray-500 uppercase tracking-wider"
                    >
                      Total
                    </th>
                    <th
                      scope="col"
                      className="px-7 py-3 text-left text-sm font-medium text-gray-500 uppercase tracking-wider"
                    >
                      Status
                    </th>
                    <th
                      scope="col"
                      className="px-7 py-3 text-right text-sm font-medium text-gray-500 uppercase tracking-wider"
                    >
                      Actions
                    </th>
                  </tr>
                </thead>
                <tbody className="bg-white divide-y divide-gray-200">
                  {loader == false ? (
                    records.length > 0 ? (
                      records.map((val, key) => (
                        <tr key={val.invoice_id}>
                          <td className="px-7 py-4 whitespace-nowrap">
                            <input
                              id={`invoice_id_${key}`}
                              name={`invoice_name_${key}`}
                              type="checkbox"
                              value={val.invoice_id}
                              onClick={() => changeCheckbox(val.invoice_id)}
                              className="focus:ring-indigo-500 h-4 w-4 text-brand-blue  border-1 border-gray-300 rounded-xs bg-white"
                              defaultChecked={
                                invoiceIds.indexOf(val.invoice_id) !== -1
                              }
                              checked={
                                invoiceIds.indexOf(val.invoice_id) !== -1
                              }
                            />
                          </td>

                          <td className="px-6 py-4 whitespace-nowrap">
                            <div className="flex items-center">
                              <div className="text-md font-medium text-gray-900">
                                {rowsPerPage * page + key + 1}
                              </div>
                            </div>
                          </td>

                          <td className="px-7 py-4 whitespace-nowrap">
                            <div className="text-md text-gray-900">
                              {val.invoice_no}
                            </div>
                          </td>

                          <td className="px-7 py-4 whitespace-nowrap">
                            <div className="text-md text-gray-900">
                              {val.client.name}
                            </div>
                          </td>

                          <td className="px-7 py-4 min-w-96">
                            <div className="text-md text-gray-900">
                              {val.description}
                            </div>
                          </td>

                          {/* <td className="px-7 py-4 whitespace-nowrap">
                                                        <div className="text-md text-gray-900">{formatShortDate(val.invoice_date)}</div>
                                                    </td> */}

                          <td className="px-7 py-4 whitespace-nowrap">
                            <div className="text-md text-gray-900">
                              {config.paytermList[val.payment_term]}
                            </div>
                          </td>

                          <td className="px-7 py-4 whitespace-nowrap">
                            <div className="text-md  text-gray-900">
                              {formatShortDate(val.due_date)}
                            </div>
                          </td>

                          <td className="px-7 py-4 whitespace-nowrap">
                            <div className="text-md text-gray-900">
                              <NumberFormat
                                value={val.total.toFixed(2)}
                                displayType={"text"}
                                thousandSeparator={true}
                                prefix={"$"}
                                renderText={(value) => <div>{value}</div>}
                              />
                            </div>
                          </td>

                          <td className="px-7 py-4 whitespace-nowrap text-md text-gray-500">
                            {invStatusStyle(val.status.toLowerCase())}
                          </td>

                          <td className="px-7 py-4 whitespace-nowrap text-right text-xsm flex justify-end items-center font-medium">
                            <button
                              className="text-gray-700 flex px-2 py-2 text-sm hover:text-brand-blue"
                              onClick={() => {
                                props.setUpdateId(val.invoice_id);
                                props.setIsInfoOpen(true);
                              }}
                            >
                              <span data-for="view" data-tip="View">
                                <svg
                                  xmlns="http://www.w3.org/2000/svg"
                                  className="h-6 w-6"
                                  fill="none"
                                  viewBox="0 0 24 24"
                                  stroke="currentColor"
                                  strokeWidth="2"
                                >
                                  <path
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                    d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"
                                  />
                                  <path
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                    d="M2.458 12C3.732 7.943 7.523 5 12 5c4.478 0 8.268 2.943 9.542 7-1.274 4.057-5.064 7-9.542 7-4.477 0-8.268-2.943-9.542-7z"
                                  />
                                </svg>
                              </span>
                              <ReactTooltip
                                id="view"
                                className="custom-tooltip bg-gray-900"
                                textColor="#ffffff"
                                backgroundColor="#111827"
                                effect="solid"
                              />
                            </button>

                            <button
                              className="text-gray-700 flex px-2 py-2 text-sm hover:text-brand-blue"
                              onClick={() => {
                                if (
                                  val.bank_transaction_id !== null &&
                                  parseInt(val.bank_transaction_id) > 0
                                ) {
                                  changeAlert("EDIT");
                                } else {
                                  props.setUpdateId(val.invoice_id);
                                  props.setIsOpen(true);
                                }
                              }}
                            >
                              <span data-for="edit" data-tip="Edit">
                                <svg
                                  xmlns="http://www.w3.org/2000/svg"
                                  className="h-6 w-6"
                                  fill="none"
                                  viewBox="0 0 24 24"
                                  stroke="currentColor"
                                  strokeWidth="2"
                                >
                                  <path
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                    d="M15.232 5.232l3.536 3.536m-2.036-5.036a2.5 2.5 0 113.536 3.536L6.5 21.036H3v-3.572L16.732 3.732z"
                                  />
                                </svg>
                              </span>
                              <ReactTooltip
                                id="edit"
                                className="custom-tooltip bg-gray-900"
                                textColor="#ffffff"
                                backgroundColor="#111827"
                                effect="solid"
                              />
                            </button>

                            <button
                              className="text-gray-700 flex px-2 py-2 text-sm hover:text-brand-blue"
                              onClick={() => {
                                if (
                                  val.bank_transaction_id !== null &&
                                  parseInt(val.bank_transaction_id) > 0
                                ) {
                                  setDeleteType("RECONCILE");
                                } else {
                                  setDeleteType("NORMAL");
                                }
                                setDeleteId(val.invoice_id);
                                setActiveKey(key);
                                setConfirmation(true);
                              }}
                            >
                              <span data-for="delete" data-tip="Delete">
                                <svg
                                  xmlns="http://www.w3.org/2000/svg"
                                  className="h-6 w-6"
                                  fill="none"
                                  viewBox="0 0 24 24"
                                  stroke="currentColor"
                                  strokeWidth="2"
                                >
                                  <path
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                    d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"
                                  />
                                </svg>
                              </span>
                              <ReactTooltip
                                id="delete"
                                className="custom-tooltip bg-gray-900"
                                textColor="#ffffff"
                                backgroundColor="#111827"
                                effect="solid"
                              />
                            </button>

                            <Menu
                              as="div"
                              className="relative inline-block text-left ml-2"
                            >
                              {({ open }) => (
                                <>
                                  <div>
                                    <Menu.Button className="-m-2 p-2 rounded flex items-center  text-gray-800 hover:text-brand-blue">
                                      <span className="sr-only">
                                        Open options
                                      </span>
                                      <DotsVerticalIcon
                                        className="h-5 w-5"
                                        aria-hidden="true"
                                      />
                                    </Menu.Button>
                                  </div>

                                  <Transition
                                    show={open}
                                    as={Fragment}
                                    enter="transition ease-out duration-100"
                                    enterFrom="transform opacity-0 scale-95"
                                    enterTo="transform opacity-100 scale-100"
                                    leave="transition ease-in duration-75"
                                    leaveFrom="transform opacity-100 scale-100"
                                    leaveTo="transform opacity-0 scale-95"
                                  >
                                    <Menu.Items
                                      static
                                      className="origin-top-right z-10 px-4 absolute devider-y devider-gray-200 -top-6 right-8 mt-3  rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5 focus:outline-none"
                                    >
                                      <div className="py-1 flex items-center space-x-3">
                                        {val.paid_with_cash == 0 && (
                                          <Menu.Item>
                                            <button
                                              className="text-gray-700 font-medium flex py-2 text-sm hover:text-brand-blue"
                                              onClick={() => {
                                                paidWidthCash(val.invoice_id);
                                              }}
                                            >
                                              <span>Paid With Cash</span>
                                            </button>
                                          </Menu.Item>
                                        )}

                                        {val.paid_with_cash == 0 && (
                                          <Menu.Item>
                                            <button className="text-gray-700 font-medium flex py-2 text-sm">
                                              <span>|</span>
                                            </button>
                                          </Menu.Item>
                                        )}

                                        <Menu.Item>
                                          <button
                                            className="text-gray-700 font-medium  flex  py-2 text-sm hover:text-brand-blue"
                                            onClick={() => {
                                              props.setUpdateId(val.invoice_id);
                                              props.setIsOpen(true);
                                              props.setIsDuplicate(true);
                                            }}
                                          >
                                            <span>Duplicate</span>
                                          </button>
                                        </Menu.Item>
                                      </div>
                                    </Menu.Items>
                                  </Transition>
                                </>
                              )}
                            </Menu>
                          </td>
                        </tr>
                      ))
                    ) : (
                      <tr>
                        <td colspan="9" className="p-6" align="center">
                          <span>No records found.</span>
                        </td>
                      </tr>
                    )
                  ) : (
                    Array.from(Array(5), (e, i) => {
                      return (
                        <tr key={i}>
                          <td className="px-6 py-4 whitespace-nowrap">
                            <Skeleton />
                          </td>
                          <td className="px-7 py-4 whitespace-nowrap">
                            <Skeleton />
                          </td>
                          <td className="px-7 py-4 whitespace-nowrap">
                            <Skeleton />
                          </td>
                          <td className="px-7 py-4 whitespace-nowrap">
                            <Skeleton />
                          </td>
                          <td className="px-7 py-4 whitespace-nowrap">
                            <Skeleton />
                          </td>
                          <td className="px-6 py-4 whitespace-nowrap">
                            <Skeleton />
                          </td>
                          <td className="px-7 py-4 whitespace-nowrap">
                            <Skeleton />
                          </td>
                          <td className="px-7 py-4 whitespace-nowrap">
                            <Skeleton />
                          </td>
                          <td className="px-7 py-4 whitespace-nowrap text-right text-xsm font-medium">
                            <Skeleton />
                          </td>
                        </tr>
                      );
                    })
                  )}
                </tbody>
              </table>
            </div>

            {loader == false ? (
              <Pagination
                total={paginationObj.total}
                page={page}
                onPageChange={handleChangePage}
                rowsPerPage={rowsPerPage}
                onRowsPerPageChange={handleChangeRowsPerPage}
              />
            ) : null}
          </div>
        </div>
      </div>
    </div>
  );
}
