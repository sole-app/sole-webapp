import Skeleton from 'react-loading-skeleton';
import 'react-loading-skeleton/dist/skeleton.css';
import {formatCurrency} from "../../helpers/common";
import NumberFormat from 'react-number-format';

export default function InvoiceSummary(props) {
    const { invoices = {}, loader } = props;

    return (
        <>
            <div className="px-5 pt-8 pb-4">
                {loader &&
                    <ul className="space-y-3 overflow-y-auto h-44">
                        {Array.from(Array(5), (e, i) => {
                            return <li className="grid grid-cols-3" key={i}>
                                <div className="font-semibold  w-40">
                                    <Skeleton />
                                </div>

                                <div className='mr-3'>
                                    <Skeleton />
                                </div>

                                <div className="font-semibold text-right text-base pr-2">
                                    <Skeleton />
                                </div>
                            </li>
                        })
                        }
                    </ul>
                }
                {(!loader) &&
                    <>
                        {(typeof invoices.invoices !== 'undefined' && invoices.invoices != null && invoices.invoices.length > 0) ?
                            <>
                            <ul className="space-y-3 overflow-y-auto h-44">
                                {
                                    invoices.invoices.map((val, key) => (
                                        <li className="grid grid-cols-3" key={key}>
                                            <div className="font-semibold  w-40">
                                                {val.invoice_no}
                                            </div>

                                            <div>
                                                {val.description}
                                            </div>

                                            <div className="font-semibold text-right text-base pr-2">
                                                {formatCurrency(val.total)}
                                            </div>
                                        </li>
                                    ))
                                }
                            </ul>

                            <div className="grid grid-cols-3 text-center mt-2 pt-5 pb-0 border-t border-grey-400">
                                <dl>
                                    <dt className="text-lg tracking-wide pb-1 font-semibold text-gray-400 truncate">
                                        Due
                                    </dt>
                                    <dd>
                                        <div className="text-xl 2xl:text-3xl whitespace-nowrap font-bold text-brand-blue">
                                            {/* <sup className="text-gray-400">$</sup>  */}
                                            {!!invoices?.total_due_amount ? (<NumberFormat
                                                          value={parseFloat(invoices?.total_due_amount).toFixed(2)}
                                                          displayType={"text"}
                                                          thousandSeparator={true}
                                                          prefix={"$"}
                                                          renderText={value => <div>{value}</div>}
                                                        />) : "$0.00"}
                                        </div>
                                    </dd>
                                    <dd>
                                        <div className="text-gray-400">{invoices.due_invoices_count || 0} Invoice</div>
                                    </dd>
                                </dl>


                                <dl>
                                    <dt className="text-lg tracking-wide pb-1 font-semibold text-gray-400 truncate">
                                        Overdue
                                    </dt>
                                    <dd>
                                        <div className="text-xl 2xl:text-3xl whitespace-nowrap font-bold text-red-600">
                                            {/* <sup className="text-gray-400">$</sup>  */}
                                            {!!invoices?.total_overdue_amount ? (<NumberFormat
                                                          value={parseFloat(invoices?.total_overdue_amount).toFixed(2)}
                                                          displayType={"text"}
                                                          thousandSeparator={true}
                                                          prefix={"$"}
                                                          renderText={value => <div>{value}</div>}
                                                        />) : "$0.00"}
                                        </div>
                                    </dd>
                                    <dd>
                                        <div className="text-gray-400">{invoices.overdue_invoices_count || 0} Invoices</div>
                                    </dd>
                                </dl>

                                <dl>
                                    <dt className="text-lg tracking-wide pb-1 font-semibold text-gray-400 truncate">
                                        Draft
                                    </dt>
                                    <dd>
                                        <div className="text-xl 2xl:text-3xl whitespace-nowrap font-bold text-gray-500">
                                            {/* <sup className="text-gray-400">$</sup>  */}
                                            {!!invoices?.total_draft_amount ? (<NumberFormat
                                                          value={parseFloat(invoices?.total_draft_amount).toFixed(2)}
                                                          displayType={"text"}
                                                          thousandSeparator={true}
                                                          prefix={"$"}
                                                          renderText={value => <div>{value}</div>}
                                                        />) : "$0.00"}
                                        </div>
                                    </dd>
                                    <dd>
                                        <div className="text-gray-400">{invoices.draft_invoices_count || 0} Invoices</div>
                                    </dd>
                                </dl>
                            </div>
                            </>
                            :
                            <>
                                <div className="text-center py-10">
                                    <img
                                        src="/images/icon/undraw_receipt_re_fre3.svg"
                                        width={150}
                                        height={150}
                                        alt="Create your first Invoice"
                                        className="mx-auto"
                                    />

                                    <h3 className="mt-2 text-2xl font-medium text-gray-700">Create your first Invoice</h3>
                                </div>
                            </>
                        }
                    </>
                }
            </div>

            
        </>
    )
}
