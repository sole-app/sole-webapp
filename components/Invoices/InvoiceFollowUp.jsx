import { Fragment, useState, useEffect } from "react";
import { Dialog, Transition } from "@headlessui/react";
import { XIcon } from "@heroicons/react/outline";
import { followUpReminder } from "../../services/api/invoice.services";
import Skeleton from "react-loading-skeleton";

export default function InvoiceFollowUp(props) {
  const [link, setLink] = useState("");
  const [loading, setLoading] = useState(true);

  useEffect(async () => {
    if (props.isFollowUp) {
      const resultInfo = await followUpReminder(props.notify);
      if (resultInfo.success) {
        setLink(resultInfo.data.link);
        setLoading(false);
      } else {
        setLoading(false);
      }
    }
  }, [props.isFollowUp]);

  return (
    <Transition.Root show={props.isFollowUp} as={Fragment}>
      <Dialog
        as="div"
        static
        className="fixed inset-0 overflow-hidden"
        onClose={(e) => props.closeNotify(e)}
      >
        <div className="absolute inset-0 overflow-hidden">
          <Transition.Child
            as={Fragment}
            enter="ease-in-out duration-500"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in-out duration-500"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <Dialog.Overlay className="absolute inset-0 bg-gray-500 bg-opacity-75 transition-opacity pointer-events-none" />
          </Transition.Child>

          <div className="fixed inset-y-0 max-w-full right-0 flex">
            <Transition.Child
              as={Fragment}
              enter="transform transition ease-in-out duration-500 sm:duration-700"
              enterFrom="translate-x-full"
              enterTo="translate-x-0"
              leave="transform transition ease-in-out duration-500 sm:duration-700"
              leaveFrom="translate-x-0"
              leaveTo="translate-x-full"
            >
              <div className="w-screen max-w-6xl bg-white">
                <div className="flex-1 pb-8 overflow-y-auto">
                  <div className="border-b border-gray-200 bg-gray-100 py-4 px-4 sm:px-6">
                    <div className="flex items-center justify-between">
                      <Dialog.Title className="text-xl font-medium text-gray-800">
                        Invoice Follow Up Preview
                      </Dialog.Title>
                      <div className="ml-3 h-7 flex items-center">
                        <button
                          type="button"
                          className="rounded-full hover:bg-gray-300 p-2 text-gray-500 hover:text-gray-700 focus:outline-none focus:ring-2 focus:ring-white"
                          onClick={(e) => props.closeNotify(e)}
                        >
                          <span className="sr-only">Close panel</span>
                          <XIcon className="h-6 w-6" aria-hidden="true" />
                        </button>
                      </div>
                    </div>
                  </div>
                  <div className="flex-1 flex flex-col justify-between">
                    <div className="px-4 divide-y divide-gray-200 sm:px-6 h-100vh pd-0">
                      {!loading ? (
                        <iframe
                          src={link + "#toolbar=0"}
                          height="95%"
                          width="100%"
                        ></iframe>
                      ) : (
                        <div className="space-y-6 border-gray-200 mb-4 pt-6 pb-7 grid grid-cols-1 gap-y-2 sm:grid-cols-1">
                          {Array.from(Array(1), (e, i) => {
                            return (
                              <div className="sm:col-span-2" key={i}>
                                <label className="info-label">
                                  <Skeleton />
                                </label>
                                <p>
                                  <Skeleton />
                                </p>
                              </div>
                            );
                          })}
                        </div>
                      )}
                    </div>
                  </div>
                </div>
              </div>
            </Transition.Child>
          </div>
        </div>
      </Dialog>
    </Transition.Root>
  );
}
