import { useState, useEffect } from "react";
import { Summary as SummaryInvoice } from "../../services/api/invoice.services";
import { getDateFromFYDates, formatCurrency } from "../../helpers/common";
import Skeleton from "react-loading-skeleton";
import "react-loading-skeleton/dist/skeleton.css";
import YearMonthFilter from "../Common/YearMonthFilter";

export default function InvoiceAnalytics(props) {
  const [loader, setLoader] = useState(true);
  const [type, setType] = useState("YEAR"); //YEAR, MONTH
  const [summary, setSummary] = useState([]);

  const getData = async (filterType) => {
    setType(filterType);
    let apiRunning = true;
    setLoader(apiRunning);

    let formData = "";
    let dates = getDateFromFYDates(filterType);
    formData += "?start_date=" + dates.start_date;
    formData += "&end_date=" + dates.end_date;

    const res = await SummaryInvoice(formData);
    if (res.success) {
      setSummary(res.data);
      apiRunning = false;
    }
    if (!apiRunning) {
      setLoader(false);
    }
  };

  useEffect(() => {
    getData(type);
  }, [props.refreshSummary]);

  return (
    <div className="mt-8 xl:pr-8 w-full  xl:w-1/2">
      <div className="grid grid-cols-1 h-full">
        <div className="bg-white overflow-hidden shadow rounded-lg">
          <div className="p-7 xl:p-8">
            <div className="hidden sm:block">
              <YearMonthFilter type={type} setType={getData} />
            </div>

            <div className=" 2xl:flex justify-between mt-12  flex-1">
              {/* Total Amount */}
              <dl className="pr-8 pt-5 w-full text-center 2xl:text-left 2xl:w-1/2">
                <dt className="text-lg tracking-wide pb-2 font-medium text-gray-400 truncate">
                  Total Paid
                </dt>
                <dd>
                  <div className="text-3xl 2xl:text-4xl whitespace-nowrap font-bold text-gray-900">
                    {loader ? (
                      <Skeleton />
                    ) : (
                      formatCurrency(summary.total_paid_amount)
                    )}
                  </div>
                </dd>

                <dd>
                  <span className="inline-flex items-center px-8 py-2 mt-5 rounded-full text-sm font-medium bg-green-100 text-green-800">
                    {loader ? (
                      <Skeleton />
                    ) : (
                      <>
                        {summary.has_previous_data ? (
                          <>
                            {summary.invoice_gain < 0 ? "-" : "+"}
                            {summary.invoice_gain}% Since Last{" "}
                            {type == "YEAR" ? "Year" : "Month"}
                          </>
                        ) : (
                          <>
                            {`No comparison for last ${
                              type == "YEAR" ? "year" : "month"
                            }`}
                          </>
                        )}
                      </>
                    )}
                  </span>
                </dd>
              </dl>

              <div className="right-aside text-center 2xl:text-left border-t border-gray-100 pt-4 2xl:border-0 w-full mt-4 2xl:w-1/2 2xl:mt-0">
                <div className="flex justify-between">
                  {/* All Amount */}
                  <dl className="pr-6 w-1/2">
                    <dt className="text-md tracking-wide pb-1 font-medium text-gray-400 truncate">
                      All
                    </dt>
                    <dd>
                      <div className="whitespace-nowrap font-bold text-gray-900 text-lg 2xl:text-xl">
                        {loader ? (
                          <Skeleton />
                        ) : (
                          formatCurrency(
                            summary.total_paid_amount +
                              summary.total_due_amount +
                              summary.total_overdue_amount +
                              summary.total_draft_amount
                          )
                        )}
                      </div>
                    </dd>
                  </dl>

                  {/* Draft Amount */}
                  <dl className="w-1/2">
                    <dt className="text-md tracking-wide pb-1 font-medium text-gray-400 truncate">
                      Draft
                    </dt>
                    <dd>
                      <div className="text-lg 2xl:text-xl whitespace-nowrap font-bold text-gray-700">
                        {loader ? (
                          <Skeleton />
                        ) : (
                          formatCurrency(summary.total_draft_amount)
                        )}
                      </div>
                    </dd>
                  </dl>
                </div>

                <div className="flex mt-6 justify-between">
                  {/* Due Amount */}
                  <dl className="pr-6 w-1/2">
                    <dt className="text-md tracking-wide pb-1 font-medium text-gray-400 truncate">
                      Due
                    </dt>
                    <dd>
                      <div className="text-lg 2xl:text-xl whitespace-nowrap font-bold text-brand-blue">
                        {loader ? (
                          <Skeleton />
                        ) : (
                          formatCurrency(summary.total_due_amount)
                        )}
                      </div>
                    </dd>
                  </dl>

                  {/* Overdue Amount */}
                  <dl className="w-1/2">
                    <dt className="text-md tracking-wide pb-1 font-medium text-gray-400 truncate">
                      Overdue
                    </dt>
                    <dd>
                      <div className="text-lg 2xl:text-xl whitespace-nowrap font-bold text-red-600">
                        {loader ? (
                          <Skeleton />
                        ) : (
                          formatCurrency(summary.total_overdue_amount)
                        )}
                      </div>
                    </dd>
                  </dl>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
