import { Fragment, useState, useEffect } from "react";
import { ErrorMessage, Form, Formik, FieldArray } from "formik";
import { Dialog, Listbox, Transition, Switch } from "@headlessui/react";
import { CheckIcon, SelectorIcon } from "@heroicons/react/solid";
import ReactTooltip from "react-tooltip";
import {
  XIcon,
  InformationCircleIcon,
  XCircleIcon,
  PlusSmIcon,
} from "@heroicons/react/outline";
import InvoiceSchema from "../../schemas/Invoice.schema";
import DateTimePicker from "../DateTimePicker";
import {
  Add as AddClientData,
  List as ListClient,
} from "../../services/api/client.services";
import {
  Update as UpdateInvoice,
  Add as AddInvoiceData,
  followUpReminder,
  Info as InfoInvoice,
} from "../../services/api/invoice.services";
import { getUserData } from "../../services/cookies.services";
import NumberFormat from "react-number-format";
import Radio from "@mui/material/Radio";
import RadioGroup from "@mui/material/RadioGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import CircularProgress from "@material-ui/core/CircularProgress";
import Stack from "@mui/material/Stack";
import { makeStyles } from "@material-ui/core/styles";
import Autocomplete, { createFilterOptions } from "@mui/material/Autocomplete";
import TextField from "@mui/material/TextField";
import { preview } from "../../services/api/preview.services";
import Skeleton from "react-loading-skeleton";
import EmailDialog from "../../components/EmailDialog";
import { FiPlusCircle } from "react-icons/fi";
import { IconContext } from "react-icons";
import { lastSentDate } from "../../helpers/common";
import AlertDialog from "../AlertDialog";

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

const useStyles = makeStyles((theme) => ({
  dollarPrice: {
    paddingLeft: "0.60rem",
  },
}));
// For Paymentterm dropdown
const paytermList = [
  { id: 0, name: "Select Payment Terms" },
  { id: 7, name: "7 Days" },
  { id: 14, name: "14 Days" },
  { id: 30, name: "30 Days" },
  { id: 60, name: "60 Days" },
  { id: 90, name: "90 Days" },
  { id: 120, name: "120 Days" },
];

// For Frequency
const frequencyList = [
  { id: "select", name: "Select frequency" },
  { id: "day", name: "Once Off" },
  { id: "weekly", name: "Weekly" },
  { id: "fortnightly", name: "Fortnightly" },
  { id: "monthly", name: "Monthly" },
  { id: "quarterly", name: "Quarterly" },
  { id: "halfyearly", name: "Half Yearly" },
  { id: "yearly", name: "Yearly" },
];

let initialValues = {
  client: null,
  client_name: "",
  client_id: "",
  invoiceDate: null,
  dueDate: null,
  description: "",
  items: [
    {
      description: "",
      qty: "",
      disc: "",
      price: "",
    },
  ],
  notifyMeOn: null,
  reminder: "",
  fllowReminder: "only",
  isInstantpay: 1,
  noOfOccurance: 1,
  onOccurance: 1,
  startDate: null,
  notes: "",
  tag_line: "",
};

export default function AddInvoice(props) {
  const { setRefreshSummary, refreshSummary } = props;
  const user = getUserData();
  const gst_percentage = 10;
  const [clientList, setClientList] = useState([]);
  const [notifyEnabled, setNotifyEnabled] = useState(false);
  const [isInstantpay, setIsInstantpay] = useState(false);
  const [isRecurrentInvoice, setIsRecurrentInvoice] = useState(false);
  const [paymentTerms, setPaymentTerms] = useState(paytermList[0]);
  const [frequency, setFrequency] = useState(frequencyList[0]);
  const [showMe, setShowMe] = useState(false);
  const [showNotify, setShowNotify] = useState(false);
  const [gstEnabled, setGstEnabled] = useState(false);
  const [totAmount, setTotAmount] = useState(0);
  const [subtotAmount, setSubtotAmount] = useState(0);
  const [discount, setDiscount] = useState(0);
  const [gstAmount, setGstAmount] = useState(0);
  const [loader, setLoader] = useState(true);
  const [draft, setDraft] = useState(false);
  const [preview, setPreview] = useState(false);
  const [invStatus, setInvStatus] = useState("DRAFT");
  const [btnType, setBtnType] = useState("SAVE"); //SAVE/SEND/DRAFT
  const [onceOff, setOnceOff] = useState(false);
  const [minDate, setMinDate] = useState(new Date());
  const [isVisible, setIsVisible] = useState();
  const [link, setLink] = useState("");
  const [invoiceType, setInvoiceType] = useState("NORMAL"); //FOLLOWUP
  const [loading, setLoading] = useState(false);
  const [openEmailDialog, setOpenEmailDialog] = useState(false);
  const [email, setEmail] = useState("");
  const [error, setError] = useState(false);
  const [currentClient, setCurrentClient] = useState({});
  const [clientLoader, setClientLoader] = useState(false);
  const [lastSent, setLastSent] = useState("");
  const [userData, setUserData] = useState(getUserData());
  const [openAlert, setOpenAlert] = useState(false);
  const [msg, setMsg] = useState("");
  const [alertType, setAlertType] = useState("Error");
  const filter = createFilterOptions();

  const closeSlider = () => {
    setIsVisible(false);
  };

  const classes = useStyles();

  const openSlider = async (invoice_id) => {
    setLoading(true);
    setIsVisible(true);
    const formData = new FormData();
    formData.append("id", invoice_id);
    formData.append("type", "invoice");

    const resultInfo = await preview(formData);
    if (resultInfo.success) {
      setLink(resultInfo.data.link);
      setLoading(false);
    } else {
      setLoading(false);
    }
  };

  function toggle() {
    setShowMe(!showMe);
  }

  function togglenotify() {
    setShowNotify(!showNotify);
  }

  const handleFormSubmit = async (values, setSubmitting) => {
    let i_date = values.invoiceDate;
    let d_date = values.dueDate;

    const formData = new FormData();

    formData.append("client_id", values.client_id ? values.client_id : "");
    formData.append(
      "invoice_date",
      i_date.getFullYear() +
        "-" +
        (i_date.getMonth() + 1) +
        "-" +
        i_date.getDate()
    );
    formData.append(
      "due_date",
      d_date.getFullYear() +
        "-" +
        (d_date.getMonth() + 1) +
        "-" +
        d_date.getDate()
    );
    formData.append("description", values.description);
    formData.append("payment_term", paymentTerms.id);

    if (notifyEnabled) {
      let notify_date = values.notifyMeOn;
      formData.append("is_notify_future_work", notifyEnabled ? 1 : 0);
      formData.append(
        "notify_future_work_date",
        notify_date.getFullYear() +
          "-" +
          (notify_date.getMonth() + 1) +
          "-" +
          notify_date.getDate()
      );
      formData.append("notify_future_work_description", values.reminder);
      formData.append(
        "notify_future_work_me_only",
        values.fllowReminder == "only" ? 1 : 0
      );
      formData.append(
        "notify_future_work_both",
        values.fllowReminder == "only" ? 0 : 1
      );
    }

    formData.append("is_instantpay", isInstantpay ? 1 : 0);

    if (isRecurrentInvoice) {
      let s_date = values.startDate;
      formData.append("is_recurring_invoice", isRecurrentInvoice ? 1 : 0);
      formData.append("on_occurance", values.onOccurance);
      formData.append("frequency", frequency.id);
      formData.append("tag_line", values.tag_line);
      formData.append("no_of_occurance", values.noOfOccurance);
      formData.append(
        "start_date",
        s_date.getFullYear() +
          "-" +
          (s_date.getMonth() + 1) +
          "-" +
          s_date.getDate()
      );
    }

    values.items.map((v, k) => {
      formData.append("item[" + k + "][description]", v.description);
      formData.append("item[" + k + "][price]", v.price);
      formData.append("item[" + k + "][qty]", v.qty);
      formData.append(
        "item[" + k + "][discount_percentage]",
        v.disc === "" ? 0 : v.disc
      );
    });

    if (draft) {
      formData.append("save", 0);
    } else {
      if (btnType == "SENT") {
        formData.append("send", 1);
      } else {
        formData.append("send", 2);
      }
    }
    if (!draft && btnType == "SENT") {
      formData.append("send", 1);
    }

    if (gstEnabled) {
      formData.append("gst_percentage", gst_percentage);
    }
    formData.append("notes", values.notes !== "" ? values.notes : "");

    let result;
    // INSRET OR UPDATE
    if (props.updateId && !props.isDuplicate) {
      formData.append("id", props.updateId);
      result = await UpdateInvoice(formData);
    } else {
      result = await AddInvoiceData(formData);
    }

    if (result.success) {
      let invoice_id =
        props.updateId === "" ? result.data.invoice_id : props.updateId;
      setSubmitting(false);
      props.setIsDuplicate(false);

      if (!preview) {
        props.setIsOpen(false);
      }
      props.setIsListUpdate(true);
      if (props.updateId === "") {
        props.setEmptyView(false);
      }
      if (preview) {
        if (invoiceType == "NORMAL") {
          props.setUpdateId(invoice_id);
          await openSlider(invoice_id);
        } else {
          props.setUpdateId(invoice_id);
        }
      }
      setEmail("");
      setAlertType("Success");
      setMsg(result.message);
      setOpenAlert(true);
    } else {
      setAlertType("Error");
      setMsg(result.message);
      setOpenAlert(true);
      setSubmitting(false);
    }
    setRefreshSummary(!refreshSummary);
  };

  const AddClient = async (client_name) => {
    setClientLoader(true);
    const formData1 = new FormData();
    formData1.append("type", 1);
    formData1.append("name", client_name);
    formData1.append("type", 1);
    const result1 = await AddClientData(formData1);
    setClientLoader(false);
    if (result1.success) {
      setEmail("");
      let temp = clientList;
      temp.push({
        label: client_name,
        id: result1.data.id,
        email: "",
      });
      setClientList(temp);
      return {
        client_id: result1.data.id,
        client_name: client_name,
      };
    }
    return {};
  };

  useEffect(async () => {
    setLastSent("");
    if (props.isOpen) {
      setEmail("");
      setLoader(true);
      setOnceOff(false);
      setNotifyEnabled(false);
      setShowNotify(false);
      setIsRecurrentInvoice(false);
      setShowMe(false);
      setSubtotAmount(0);
      setDiscount(0);
      setGstEnabled(false);
      setGstAmount(0);
      setTotAmount(0);
      setPaymentTerms(paytermList[0]);
      setFrequency(frequencyList[0]);
      let d = new Date();
      d.setDate(d.getDate() + 1);
      setMinDate(d);
      let date = new Date();
      date.setDate(date.getDate() + 7);
      initialValues.dueDate = null;
      initialValues.dueDate = date;
      const formData = "?filter_by[type]=client&page_size=1000";

      // GET CLIENT DATA
      const resultInfoClients = await ListClient(formData);
      if (resultInfoClients.success) {
        let temp = [];
        resultInfoClients.data.clients_details.map((v) => {
          temp.push({
            email: v.email,
            label: v.name,
            id: v.client_id,
          });
        });
        setClientList(temp);

        // RESET VALUE
        initialValues.client = null;
        initialValues.invoiceDate = null;
        //initialValues.dueDate = null;
        initialValues.description = "";
        initialValues.items = [
          {
            description: "",
            qty: "",
            disc: "",
            price: "",
          },
        ];
        initialValues.notifyMeOn = null;
        initialValues.reminder = "";
        initialValues.fllowReminder = "only";
        initialValues.isInstantpay = 1;
        initialValues.noOfOccurance = "";
        initialValues.onOccurance = 1;
        initialValues.startDate = null;
        initialValues.notes = "";

        if (props.updateId) {
          const resultInfo = await InfoInvoice(props.updateId);
          if (resultInfo.success) {
            let res = resultInfo.data;
            if (res.sent_at !== "") {
              setLastSent(res.sent_at);
            }

            if (res.client) {
              let selectedClient = {
                client_id: res.client.client_id,
                name: res.client.name,
                email: "",
              };

              resultInfoClients.data.clients_details.map((v) => {
                if (v.client_id == selectedClient.client_id) {
                  selectedClient.email = v.email;
                }
              });

              initialValues.client = {
                id: selectedClient.client_id,
                label: selectedClient.name,
                email: selectedClient.email,
              };
              initialValues.client_id = selectedClient.client_id;
              initialValues.client_name = selectedClient.name;
              setEmail(selectedClient.email);
              setCurrentClient({
                label: res.client.name,
                id: res.client.client_id,
              });
            }
            initialValues.description = res.description;
            initialValues.invoiceDate = new Date(res.invoice_date);
            initialValues.dueDate = new Date(res.due_date);

            // Set Invoice Status
            setInvStatus(res.status);

            if (res.payment_term !== "" && res.payment_term !== null) {
              if (
                [0, 7, 14, 30, 60, 90, 120].indexOf(res.payment_term) !== -1
              ) {
                setPaymentTerms(
                  paytermList.filter((v) => v.id === res.payment_term)[0]
                );
              }
            }

            temp = [];
            res.invoice_item.map((vv, kk) => {
              temp.push({
                description: vv.description,
                qty: vv.qty,
                disc: vv.discount_percentage,
                price: vv.price,
              });
            });
            initialValues.items = temp;

            if (res.is_notify_future_work === 1) {
              if (res.notify_future_work) {
                initialValues.notifyMeOn = res.notify_future_work
                  ? new Date(res.notify_future_work.notify_future_work_date)
                  : null;
                initialValues.reminder =
                  res.notify_future_work?.notify_future_work_description;
                initialValues.fllowReminder =
                  res.notify_future_work.notify_future_work;
                setNotifyEnabled(true);
              }
              setShowNotify(true);
            }

            if (res.is_recurring_invoice === 1) {
              initialValues.noOfOccurance =
                res.invoice_recurring.no_of_occurance;
              initialValues.onOccurance = res.invoice_recurring.on_occurance;
              initialValues.startDate = res.invoice_recurring
                ? new Date(res.invoice_recurring.start_date)
                : null;
              initialValues.tag_line = res.invoice_recurring.tag_line;
              setFrequency(
                frequencyList.filter(
                  (v) => v.id === res.invoice_recurring.frequency
                )[0]
              );
              setIsRecurrentInvoice(true);
              setShowMe(true);
            }

            setIsInstantpay(res.is_instantpay === 1 ? true : false);
            initialValues.notes =
              res.notes && res.notes !== "" ? res.notes : "";

            if (res.total_gst > 0) {
              setGstEnabled(true);
              calculation(initialValues.items, "", "", "", true);
            } else {
              calculation(initialValues.items);
            }
            setLoader(false);
          }
        } else {
          setLoader(false);
        }
      }
    }
  }, [props.isOpen]);

  const notifyEmail = async (values) => {
    if (values.reminder && values.notifyMeOn) {
      setInvoiceType("FOLLOWUP");
      //props.setNotify('');
      let formData = new FormData();
      formData.append("notify_future_work_description", values.reminder);
      formData.append("invoice_total", totAmount);
      if (paymentTerms.id != "undefined") {
        formData.append("payment_term", paymentTerms.id);
      }
      formData.append("invoice_description", values.description);
      formData.append("client_id", values.client_id);
      let i_date = values.invoiceDate;
      let d_date = values.dueDate;
      formData.append(
        "invoice_date",
        i_date.getFullYear() +
          "-" +
          (i_date.getMonth() + 1) +
          "-" +
          i_date.getDate()
      );
      formData.append(
        "due_date",
        d_date.getFullYear() +
          "-" +
          (d_date.getMonth() + 1) +
          "-" +
          d_date.getDate()
      );
      //props.setNotify(formData);
      //props.setIsFollowUp(true);

      setLoading(true);
      setIsVisible(true);
      const resultInfo = await followUpReminder(formData);
      if (resultInfo.success) {
        setLink(resultInfo.data.link);
        setLoading(false);
      } else {
        setLoading(false);
      }
    } else {
      setAlertType("Error");
      setMsg("Please complete mandatory fields in order to preview.");
      setOpenAlert(true);
    }
  };

  const calculation = (data, index, key, val, g = gstEnabled) => {
    let sub_total = 0;
    let gst_total = 0;
    let total = 0;
    let dis = 0;
    data.map((vv, kk) => {
      if (val === "" && kk === index && key !== "disc") {
        return false;
      } else {
        let temp_p = vv.price;
        let temp_q = vv.qty;
        let temp_d = vv.disc;

        if (index === kk) {
          if (key === "price") temp_p = val;
          else if (key === "qty") temp_q = val;
          else if (key === "disc") temp_d = val;
        }
        if (isNaN(temp_d) || parseInt(temp_d) < 1 || temp_d === "") {
          temp_d = 0;
        }

        if (temp_p && temp_q) {
          dis += temp_d ? temp_p * temp_q * (temp_d / 100) : 0;
          sub_total += temp_p * temp_q;
        }
      }
    });
    gst_total = g === true ? (sub_total - dis) * (gst_percentage / 100) : 0;
    total = sub_total + gst_total - dis;

    setGstAmount(gst_total);
    setSubtotAmount(sub_total);
    setTotAmount(total);
    setDiscount(dis);
  };

  const changeDate = (data, setFieldValue, values, type) => {
    if (type === 0) {
      //invoice date
      setFieldValue("invoiceDate", data);
      let date = new Date(data);
      date.setDate(date.getDate() + paymentTerms.id);
      setFieldValue("dueDate", date);
    } else {
      //terms
      setPaymentTerms(data);
      let date = new Date();
      if (initialValues.invoiceDate !== null) {
        date = new Date(initialValues.invoiceDate);
      }
      if (values.invoiceDate !== null) {
        date = new Date(values.invoiceDate);
      }
      date.setDate(date.getDate() + data.id);
      setFieldValue("dueDate", date);
    }
  };

  const handleFrequencyChange = (selectedValue, setFieldValue) => {
    setFrequency(selectedValue);

    // Business rules, for Once Off
    if (selectedValue.id === "day") {
      initialValues.noOfOccurance = 1;
      setOnceOff(true);
      setFieldValue("noOfOccurance", 1);
      setFieldValue("onOccurance", "2");
    } else {
      setOnceOff(false);
      setFieldValue("onOccurance", "1");
      setFieldValue("noOfOccurance", 0);
      initialValues.noOfOccurance = 0;
    }
  };

  return (
    <>
      <Transition.Root show={props.isOpen} as={Fragment}>
        <Dialog
          as="div"
          static
          className="fixed inset-0 overflow-hidden"
          onClose={props.setIsOpen}
        >
          <div className="absolute inset-0 overflow-hidden">
            <Transition.Child
              as={Fragment}
              enter="ease-in-out duration-500"
              enterFrom="opacity-0"
              enterTo="opacity-100"
              leave="ease-in-out duration-500"
              leaveFrom="opacity-100"
              leaveTo="opacity-0"
            >
              <Dialog.Overlay className="absolute inset-0 bg-gray-500 bg-opacity-75 transition-opacity pointer-events-none" />
            </Transition.Child>

            <div className="fixed inset-y-0 max-w-full right-0 flex">
              <Transition.Child
                as={Fragment}
                enter="transform transition ease-in-out duration-500 sm:duration-700"
                enterFrom="translate-x-full"
                enterTo="translate-x-0"
                leave="transform transition ease-in-out duration-500 sm:duration-700"
                leaveFrom="translate-x-0"
                leaveTo="translate-x-full"
              >
                <div className="w-screen xl:max-w-7xl">
                  <Formik
                    initialValues={initialValues}
                    validationSchema={InvoiceSchema}
                    onSubmit={(values, { setSubmitting }) => {
                      if (notifyEnabled) {
                        if (!values.notifyMeOn) {
                          setError(true);
                          setSubmitting(false);
                          setAlertType("Error");
                          setMsg("Notify On field is required.");
                          setOpenAlert(true);
                          return;
                        }
                      }
                      if (isRecurrentInvoice) {
                        if (!values.startDate) {
                          setError(true);
                          setSubmitting(false);
                          setAlertType("Error");
                          setMsg("Start Date is required.");
                          setOpenAlert(true);
                          return;
                        }
                      }
                      if (
                        !draft &&
                        email == "" &&
                        !error &&
                        btnType == "SENT"
                      ) {
                        setSubmitting(false);
                        setOpenEmailDialog(true);
                        return;
                      }
                      if (!email && !error && btnType == "SENT") {
                        setSubmitting(false);
                        setOpenEmailDialog(true);
                        return;
                      }
                      handleFormSubmit(values, setSubmitting);
                    }}
                  >
                    {({
                      values,
                      errors,
                      handleChange,
                      handleSubmit,
                      handleBlur,
                      touched,
                      isSubmitting,
                      setFieldValue,
                    }) => (
                      <Form
                        autoComplete="off"
                        id="invoice-form"
                        onSubmit={handleSubmit}
                        className="h-full flex flex-col bg-white shadow-xl"
                      >
                        <div className="flex-1 h-0 pb-8 overflow-y-auto">
                          <div className="border-b border-gray-200 bg-gray-100 py-4 px-4 sm:px-6">
                            <div className="flex items-center justify-between">
                              <Dialog.Title className="text-xl font-medium text-gray-800">
                                {props.updateId && !props.isDuplicate
                                  ? "Edit "
                                  : "New "}
                                Invoice
                                <p className="mt-1 font-normal text-sm text-gray-500">
                                  Fields marked with * are mandatory
                                </p>
                              </Dialog.Title>
                              <div className="ml-3 h-7 flex items-center">
                                <button
                                  type="button"
                                  className="inline-flex items-center mr-2 px-4 py-1.5 border border-indigo-400 text-sm font-medium  rounded-full text-brand-blue bg-gray-50 hover:bg-indigo-100 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-purple-500"
                                  disabled={isSubmitting}
                                  onClick={() => {
                                    setBtnType("PREVIEW");
                                    setInvoiceType("NORMAL");
                                    setPreview(true);
                                    setDraft(true);
                                    handleSubmit();
                                  }}
                                >
                                  {isSubmitting && btnType == "PREVIEW"
                                    ? "Please wait..."
                                    : "Preview"}
                                </button>

                                <button
                                  type="button"
                                  className="rounded-full hover:bg-gray-300 p-2 text-gray-500   hover:text-gray-700  focus:outline-none focus:ring-2 focus:ring-white"
                                  onClick={() => props.setIsOpen(false)}
                                >
                                  <span className="sr-only">Close panel</span>
                                  <XIcon
                                    className="h-6 w-6"
                                    aria-hidden="true"
                                  />
                                </button>
                              </div>
                            </div>
                          </div>

                          <div className="flex-1 flex flex-col justify-between">
                            <div className="px-4 sm:px-6">
                              {!loader ? (
                                <>
                                  <div className="border-b border-gray-200 mb-4 pt-6 pb-7 ">
                                    <div className="flex items-center justify-between border-b border-gray-200 pb-7">
                                      <div className="mt-1 flex rounded-md shadow-sm pointer-events-none">
                                        <span className="inline-flex items-center px-3 rounded-l-md border-2 border-r-0 font-semibold border-gray-200 bg-gray-100 text-gray-500 text-lg">
                                          {props.updateId
                                            ? invStatus.toUpperCase()
                                            : "DRAFT"}
                                        </span>
                                        <input
                                          type="text"
                                          name="invoice-name"
                                          id="invoice-name"
                                          readOnly
                                          tab={-1}
                                          className="input py-3 px-4 block w-full text-xl border-2  text-gray-800 focus:ring-indigo-400 focus:border-indigo-400 border-gray-200 rounded-tr-md rounded-br-md"
                                          value="Tax Invoice"
                                        />
                                      </div>

                                      <div className="h-32 w-32 flex items-center justify-center rounded-md border-2 border-gray-100 relative">
                                        {userData &&
                                        userData.profile_pic !== "" &&
                                        userData.profile_pic !== null ? (
                                          <img
                                            className="h-8 w-8 rounded-full"
                                            src={
                                              userData &&
                                              userData.profile_pic !== "" &&
                                              userData.profile_pic !== null
                                                ? userData.profile_pic
                                                : "/images/avatar/1.jpg"
                                            }
                                            alt="User"
                                            width="80"
                                            height="80"
                                          />
                                        ) : (
                                          <img
                                            src="/images/icon/dummy-logo.jpg"
                                            alt="Sole logo"
                                            width="180"
                                            height="180"
                                            className="object-cover"
                                          />
                                        )}

                                        <InformationCircleIcon
                                          className="h-8 w-8 text-gray-400 ml-2 absolute -top-2 -right-2 bg-white rounded-full p-1"
                                          data-for="profile-image"
                                          data-tip
                                        />
                                        <ReactTooltip
                                          id="profile-image"
                                          className="custom-tooltip bg-gray-900"
                                          textColor="#ffffff"
                                          backgroundColor="#111827"
                                          effect="solid"
                                          aria-haspopup="true"
                                        >
                                          <p className="w-64">
                                            Please upload your business logo
                                            from your profile under settings.
                                            Sole app uses your logo on all
                                            invoices. The best suggested size is
                                            500 x 500 pixel.
                                          </p>
                                        </ReactTooltip>
                                      </div>
                                    </div>

                                    <div className="items-baseline space-y-6  grid grid-cols-1 gap-y-2 sm:pt-0 md:grid-cols-2 sm:gap-x-8">
                                      <div className="from-address">
                                        <p>
                                          <strong className="font-bold block mb-2">
                                            From
                                          </strong>
                                          <span className="uppercase">
                                            {!!user?.business_name
                                              ? user.business_name
                                              : user?.full_name}
                                          </span>{" "}
                                          <br></br>
                                          {user?.address?.address_line1
                                            .split(",")
                                            .map((str) => (
                                              <p key={str}>{str}</p>
                                            ))}
                                          {user?.address?.suburb
                                            .split(",")
                                            .map((str) => (
                                              <p key={str}>{str}</p>
                                            ))}
                                          {user?.address?.state
                                            .split(",")
                                            .map((str) => (
                                              <p key={str}>
                                                {str} {user?.address?.country}
                                              </p>
                                            ))}
                                        </p>
                                      </div>

                                      <div className="client-name mt-10">
                                        <div className="flex justify-between">
                                          <strong className="font-bold block mb-2">
                                            To
                                          </strong>
                                        </div>

                                        <div className="flex justify-between">
                                          <label
                                            htmlFor="company"
                                            className="block text-md tracking-wider font-medium text-gray-500"
                                          >
                                            Client Name*
                                          </label>
                                          {clientLoader && (
                                            <div
                                              style={{ marginRight: "13px" }}
                                            >
                                              <CircularProgress size={15} />
                                            </div>
                                          )}
                                        </div>

                                        <div className="mt-1">
                                          <Stack spacing={2}>
                                            <Autocomplete
                                              disablePortal
                                              id="client_name"
                                              name="client_name"
                                              value={values.client}
                                              onBlur={handleBlur}
                                              freeSolo
                                              className="p-0 form-control"
                                              options={clientList}
                                              getOptionLabel={(option) => {
                                                if (
                                                  option?.value ===
                                                  "Add new Client"
                                                ) {
                                                  return (
                                                    <div className="py-2 flex gap-1">
                                                      Add New Client&nbsp;&nbsp;
                                                      <IconContext.Provider
                                                        value={{
                                                          color: "#4d4dff",
                                                          className:
                                                            "global-class-name h-6 w-6",
                                                        }}
                                                      >
                                                        <div>
                                                          <FiPlusCircle />
                                                        </div>
                                                      </IconContext.Provider>
                                                    </div>
                                                  );
                                                } else {
                                                  return option.label;
                                                }
                                              }}
                                              onChange={async (e, value) => {
                                                if (
                                                  value?.value ==
                                                  "Add new Client"
                                                ) {
                                                  setFieldValue(
                                                    "client_name",
                                                    value.label
                                                  );
                                                  setFieldValue("client", {
                                                    label: value.label,
                                                  });
                                                  setCurrentClient({
                                                    label: value.label,
                                                  });
                                                  let client_details =
                                                    await AddClient(
                                                      value.label
                                                    );
                                                  if (
                                                    !!client_details?.client_id
                                                  ) {
                                                    setFieldValue(
                                                      "client_id",
                                                      client_details.client_id
                                                    );
                                                    setFieldValue(
                                                      "client_name",
                                                      client_details.client_name
                                                    );
                                                    setFieldValue("client", {
                                                      label:
                                                        client_details.client_name,
                                                      id: client_details.client_id,
                                                    });
                                                    setCurrentClient({
                                                      label:
                                                        client_details.client_name,
                                                      id: client_details.client_id,
                                                    });
                                                  }
                                                } else {
                                                  if (
                                                    typeof value !==
                                                      "undefined" &&
                                                    value !== null &&
                                                    Object.keys(value).length >
                                                      0
                                                  ) {
                                                    if (
                                                      typeof value === "object"
                                                    ) {
                                                      setFieldValue(
                                                        "client_id",
                                                        value.id
                                                      );
                                                      setFieldValue(
                                                        "client_name",
                                                        value.label
                                                      );
                                                      setEmail(value.email);
                                                    } else {
                                                      setFieldValue(
                                                        "client_id",
                                                        ""
                                                      );
                                                      setFieldValue(
                                                        "client_name",
                                                        ""
                                                      );
                                                      setFieldValue("client", {
                                                        label: "",
                                                      });
                                                      setEmail("");
                                                    }
                                                  } else {
                                                    setFieldValue(
                                                      "client_id",
                                                      ""
                                                    );
                                                    setFieldValue(
                                                      "client_name",
                                                      ""
                                                    );
                                                    setEmail("");
                                                  }
                                                }
                                              }}
                                              renderInput={(params) => (
                                                <TextField
                                                  {...params}
                                                  variant="outlined"
                                                />
                                              )}
                                              filterOptions={(
                                                options,
                                                params
                                              ) => {
                                                const filtered = filter(
                                                  options,
                                                  params
                                                );
                                                // Suggest the creation of a new value
                                                if (params.inputValue !== "") {
                                                  filtered.push({
                                                    label: params.inputValue,
                                                    value: `Add new Client`,
                                                  });
                                                }
                                                return filtered;
                                              }}
                                            />
                                            <ErrorMessage
                                              name="client_name"
                                              component="span"
                                              className="error_text"
                                            />
                                          </Stack>
                                        </div>
                                      </div>
                                    </div>

                                    <div className="items-baseline space-y-6 mt-4 grid grid-cols-1 gap-y-2 sm:pt-0 md:grid-cols-3 sm:gap-x-8">
                                      <div>
                                        <label
                                          htmlFor="last-name"
                                          className="block text-base tracking-wider font-medium text-gray-500"
                                        >
                                          Invoice Date*
                                        </label>
                                        <div className="mt-1 relative date-picker bg-white border-2 focus:ring-indigo-400 focus:border-indigo-400 border-gray-100 rounded-md">
                                          <DateTimePicker
                                            date={values.invoiceDate}
                                            onDateChange={(val) => {
                                              changeDate(
                                                val,
                                                setFieldValue,
                                                values,
                                                0
                                              );
                                            }}
                                            name="invoiceDate"
                                            id="invoiceDate"
                                            inputClassName={
                                              "input py-3 px-4 block w-full border-0"
                                            }
                                            placeholder=""
                                          />
                                          <ErrorMessage
                                            name="invoiceDate"
                                            component="span"
                                            className="error_text"
                                          />
                                        </div>
                                      </div>

                                      <div>
                                        <label
                                          htmlFor="first-name"
                                          className="inline-flex items-center text-md tracking-wider font-medium text-gray-500"
                                        >
                                          Payment Terms
                                        </label>
                                        <Listbox
                                          value={paymentTerms}
                                          onChange={(val) =>
                                            changeDate(
                                              val,
                                              setFieldValue,
                                              values,
                                              1
                                            )
                                          }
                                        >
                                          {({ open }) => (
                                            <>
                                              <div className="mt-1 relative">
                                                <Listbox.Button className="py-3 px-4 block relative text-left cursor-default  w-full border-2 border-gray-200  focus:ring-indigo-400 focus:border-indigo-400 border-gray-100 rounded-md">
                                                  <span className="block truncate">
                                                    {paymentTerms.name || ""}
                                                  </span>
                                                  <span className="absolute inset-y-0 right-0 flex items-center pr-2 pointer-events-none">
                                                    <SelectorIcon
                                                      className="h-5 w-5 text-gray-400"
                                                      aria-hidden="true"
                                                    />
                                                  </span>
                                                </Listbox.Button>
                                                <Transition
                                                  show={open}
                                                  as={Fragment}
                                                  leave="transition ease-in duration-100"
                                                  leaveFrom="opacity-100"
                                                  leaveTo="opacity-0"
                                                >
                                                  <Listbox.Options
                                                    static
                                                    className="absolute z-10 mt-1 w-full bg-white shadow-lg max-h-60 rounded-md py-1 text-base ring-1 ring-black ring-opacity-5 overflow-auto focus:outline-none sm:text-sm"
                                                  >
                                                    {paytermList.map(
                                                      (terms) => (
                                                        <Listbox.Option
                                                          key={terms.id}
                                                          className={({
                                                            active,
                                                          }) =>
                                                            classNames(
                                                              active
                                                                ? "text-white bg-brand-blue"
                                                                : "text-gray-900",
                                                              "cursor-default select-none relative py-2 pl-3 pr-9"
                                                            )
                                                          }
                                                          value={terms}
                                                        >
                                                          {({
                                                            selected,
                                                            active,
                                                          }) => (
                                                            <>
                                                              <span
                                                                className={classNames(
                                                                  selected
                                                                    ? "font-semibold"
                                                                    : "font-normal",
                                                                  "block truncate"
                                                                )}
                                                              >
                                                                {terms.name}
                                                              </span>

                                                              {selected ? (
                                                                <span
                                                                  className={classNames(
                                                                    active
                                                                      ? "text-white"
                                                                      : "text-brand-blue",
                                                                    "absolute inset-y-0 right-0 flex items-center pr-4"
                                                                  )}
                                                                >
                                                                  <CheckIcon
                                                                    className="h-5 w-5"
                                                                    aria-hidden="true"
                                                                  />
                                                                </span>
                                                              ) : null}
                                                            </>
                                                          )}
                                                        </Listbox.Option>
                                                      )
                                                    )}
                                                  </Listbox.Options>
                                                </Transition>
                                              </div>
                                            </>
                                          )}
                                        </Listbox>
                                      </div>

                                      <div>
                                        <label
                                          htmlFor="last-name"
                                          className="block text-md tracking-wider font-medium text-gray-500"
                                        >
                                          Due Date*
                                        </label>
                                        <div className="mt-1 relative date-picker bg-white border-2 focus:ring-indigo-400 focus:border-indigo-400 border-gray-100 rounded-md">
                                          <DateTimePicker
                                            date={values.dueDate}
                                            onDateChange={(val) => {
                                              setFieldValue("dueDate", val);
                                            }}
                                            name="dueDate"
                                            id="dueDate"
                                            minDate={
                                              values.invoiceDate !== null
                                                ? values.invoiceDate
                                                : new Date()
                                            }
                                            inputClassName={
                                              "input py-3 px-4 block w-full border-0"
                                            }
                                            placeholder=""
                                          />
                                          <ErrorMessage
                                            name="dueDate"
                                            component="span"
                                            className="error_text"
                                          />
                                        </div>
                                      </div>

                                      <div className="sm:col-span-3">
                                        <label
                                          htmlFor="company"
                                          className="block text-md tracking-wider font-medium text-gray-500"
                                        >
                                          Invoice Description*
                                        </label>
                                        <div className="mt-1">
                                          <input
                                            variant="outlined"
                                            value={values.description}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            type="text"
                                            name="description"
                                            id="description"
                                            placeholder=""
                                            className={
                                              "placeholder-gray-400 py-3 px-4 block w-full border-2 border-gray-200  focus:ring-indigo-400 focus:border-indigo-400 border-gray-100 rounded-md "
                                            }
                                          />
                                          <ErrorMessage
                                            name="description"
                                            component="span"
                                            className="error_text"
                                          />
                                        </div>
                                      </div>
                                    </div>
                                  </div>

                                  <table className="min-w-full border-none">
                                    <thead>
                                      <tr>
                                        <th
                                          scope="col"
                                          className="pr-7 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                                        >
                                          Items
                                        </th>
                                        <th
                                          scope="col"
                                          className="pr-7 w-40 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                                        >
                                          Qty
                                        </th>
                                        <th
                                          scope="col"
                                          className="pr-7 py-3 w-40 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                                        >
                                          Price
                                        </th>
                                        <th
                                          scope="col"
                                          className="pr-7 py-3 w-24 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                                        >
                                          Disc
                                        </th>
                                        <th
                                          scope="col"
                                          className="pr-7 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                                        >
                                          <span className="sr-only">
                                            Action
                                          </span>
                                        </th>
                                      </tr>
                                    </thead>

                                    <FieldArray
                                      name="items"
                                      render={({ insert, remove, push }) => (
                                        <tbody className="bg-white divide-y divide-gray-200">
                                          {values.items.length > 0 &&
                                            values.items.map((vv, kk) => (
                                              <tr
                                                key={kk}
                                                className="align-top"
                                              >
                                                <td className="pr-4 py-4">
                                                  <div className=" flex items-center whitespace-nowrap">
                                                    <div className="text-gray-900 pr-4">
                                                      {kk + 1}
                                                    </div>
                                                    <div className="relative py-3 px-4 block w-full border-2 border-gray-200 focus-within:ring-indigo-400 focus-within:border-indigo-400  rounded-md">
                                                      <label
                                                        htmlFor="name"
                                                        className="absolute -top-2 bg-white tracking-wide left-2 -mt-px inline-block px-1  text-sm font-medium text-gray-500"
                                                      >
                                                        Item Description*
                                                      </label>
                                                      <input
                                                        type="text"
                                                        className="block w-full border-0 p-0 text-gray-900 placeholder-gray-500 focus:ring-0 sm:text-sm"
                                                        placeholder=""
                                                        variant="outlined"
                                                        name={`items.${kk}.description`}
                                                        required={true}
                                                        value={vv.description}
                                                        onChange={handleChange}
                                                        onBlur={handleBlur}
                                                      />
                                                    </div>
                                                  </div>
                                                  <ErrorMessage
                                                    name={`items.${kk}.description`}
                                                    component="span"
                                                    className="error_text"
                                                  />
                                                </td>

                                                <td className="pr-4 py-4 whitespace-nowrap">
                                                  <div className="relative py-3 px-4 block w-full border-2 border-gray-200 focus-within:ring-indigo-400 focus-within:border-indigo-400 border-gray-100 rounded-md">
                                                    <label
                                                      htmlFor="name"
                                                      className="absolute -top-2 bg-white tracking-wide left-2 -mt-px inline-block px-1  text-sm font-medium text-gray-500"
                                                    >
                                                      Qty*
                                                    </label>
                                                    <input
                                                      type="number"
                                                      className="block w-full  border-0 p-0 text-gray-900 placeholder-gray-500 focus:ring-0 sm:text-sm"
                                                      placeholder=""
                                                      required
                                                      name={`items.${kk}.qty`}
                                                      value={vv.qty}
                                                      onChange={async (e) => {
                                                        handleChange(e);
                                                        calculation(
                                                          values.items,
                                                          kk,
                                                          "qty",
                                                          e.target.value
                                                        );
                                                      }}
                                                      onBlur={handleBlur}
                                                      min={0}
                                                    />
                                                  </div>
                                                  <ErrorMessage
                                                    name={`items.${kk}.qty`}
                                                    component="span"
                                                    className="error_text"
                                                  />
                                                </td>

                                                <td className="pr-4 py-4 whitespace-nowrap">
                                                  <div className="relative py-3 px-4 block w-full border-2 border-gray-200 focus-within:ring-indigo-400 focus-within:border-indigo-400 border-gray-100 rounded-md">
                                                    <label
                                                      htmlFor="name"
                                                      className="absolute -top-2 bg-white tracking-wide left-2 -mt-px inline-block px-1  text-sm font-medium text-gray-500"
                                                    >
                                                      Price per unit*
                                                    </label>
                                                    <div className="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
                                                      <span className="text-gray-500 font-semibold sm:text-md">
                                                        $
                                                      </span>
                                                    </div>
                                                    <input
                                                      type="number"
                                                      className={
                                                        "block w-full  border-0 p-0 text-gray-900 placeholder-gray-500 focus:ring-0 sm:text-sm " +
                                                        classes.dollarPrice
                                                      }
                                                      placeholder="0.00"
                                                      name={`items.${kk}.price`}
                                                      value={vv.price}
                                                      onChange={(e) => {
                                                        handleChange(e);
                                                        calculation(
                                                          values.items,
                                                          kk,
                                                          "price",
                                                          e.target.value
                                                        );
                                                      }}
                                                      onBlur={(e) => {
                                                        let temp = values.items;
                                                        temp[kk].price =
                                                          parseFloat(
                                                            e.target.value
                                                          ).toFixed(2);
                                                        setFieldValue(
                                                          "items",
                                                          temp
                                                        );
                                                      }}
                                                      min={0}
                                                    />
                                                  </div>
                                                  <ErrorMessage
                                                    name={`items.${kk}.price`}
                                                    component="span"
                                                    className="error_text"
                                                  />
                                                </td>

                                                <td className="pr-4 py-4 whitespace-nowrap">
                                                  <div className="relative py-3 px-4 block w-full border-2 border-gray-200 focus-within:ring-indigo-400 focus-within:border-indigo-400 border-gray-100 rounded-md">
                                                    <label
                                                      htmlFor="name"
                                                      className="absolute -top-2 bg-white tracking-wide left-2 -mt-px inline-block px-1  text-sm font-medium text-gray-500"
                                                    >
                                                      Disc %
                                                    </label>
                                                    <input
                                                      type="number"
                                                      className="block w-full  border-0 p-0 text-gray-900 placeholder-gray-500 focus:ring-0 sm:text-sm"
                                                      placeholder=""
                                                      name={`items.${kk}.disc`}
                                                      value={vv.disc}
                                                      onChange={(e) => {
                                                        handleChange(e);
                                                        calculation(
                                                          values.items,
                                                          kk,
                                                          "disc",
                                                          e.target.value
                                                        );
                                                      }}
                                                      onBlur={handleBlur}
                                                      min={0}
                                                    />
                                                  </div>
                                                </td>

                                                <td className="pr-4 py-4 whitespace-nowrap">
                                                  {values.items.length > 1 ? (
                                                    <button
                                                      type="button"
                                                      className="rounded-md text-gray-500 hover:text-gray-700 focus:outline-none focus:ring-2 focus:ring-white"
                                                      onClick={() => {
                                                        calculation(
                                                          values.items,
                                                          kk,
                                                          "",
                                                          ""
                                                        );
                                                        remove(kk);
                                                      }}
                                                    >
                                                      <span className="sr-only">
                                                        Remove item
                                                      </span>
                                                      <XCircleIcon
                                                        className="h-6 w-6"
                                                        aria-hidden="true"
                                                      />
                                                    </button>
                                                  ) : null}
                                                </td>
                                              </tr>
                                            ))}
                                          <tr>
                                            <td>
                                              <button
                                                type="button"
                                                className="inline-flex items-center pl-6 py-2 border-none text-sm leading-4 font-medium  text-blue-700 hover:text-blue-800 focus:outline-none  focus:ring-blue-500"
                                                onClick={() =>
                                                  push({
                                                    description: "",
                                                    qty: "",
                                                    disc: "",
                                                    price: "",
                                                  })
                                                }
                                              >
                                                <PlusSmIcon
                                                  className="-ml-0.5 mr-2 h-4 w-4"
                                                  aria-hidden="true"
                                                />
                                                Add New Item
                                              </button>
                                            </td>
                                          </tr>
                                        </tbody>
                                      )}
                                    />
                                  </table>

                                  <div className="grid grid-cols-1 items-start mt-4 gap-4 lg:grid-cols-2">
                                    <div className="bg-gray-100 relative shadow sm:rounded-lg">
                                      <Switch.Group
                                        as="div"
                                        className="px-4  py-5 sm:p-6"
                                      >
                                        <div className="inline-flex justify-between w-full">
                                          <Switch.Label
                                            as="h3"
                                            className="text-lg flex leading-6 font-medium text-gray-700"
                                            passive
                                          >
                                            Follow up work reminder
                                            <a
                                              target="_blank"
                                              href="https://soleapp.com.au/academy_article/follow-up-work-reminders-2/"
                                            >
                                              <InformationCircleIcon
                                                className="h-6 w-6 text-gray-400 ml-2  focus:outline-none"
                                                data-for=""
                                                data-tip=""
                                              />
                                            </a>
                                          </Switch.Label>

                                          <div className="mt-5 sm:mt-0 sm:ml-6 sm:flex-shrink-0 sm:flex sm:items-center">
                                            <Switch
                                              checked={notifyEnabled}
                                              onChange={(e) => {
                                                setNotifyEnabled(e);
                                                togglenotify();
                                              }}
                                              className={classNames(
                                                notifyEnabled
                                                  ? "bg-green-600"
                                                  : "bg-gray-300",
                                                "relative inline-flex flex-shrink-0 h-6 w-11 border-2 border-transparent rounded-full cursor-pointer transition-colors ease-in-out duration-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                                              )}
                                            >
                                              <span
                                                className={classNames(
                                                  notifyEnabled
                                                    ? "translate-x-5"
                                                    : "translate-x-0",
                                                  "pointer-events-none relative inline-block h-5 w-5 rounded-full bg-white shadow transform ring-0 transition ease-in-out duration-200"
                                                )}
                                              >
                                                <span
                                                  className={classNames(
                                                    notifyEnabled
                                                      ? "opacity-0 ease-out duration-100"
                                                      : "opacity-100 ease-in duration-200",
                                                    "absolute inset-0 h-full w-full flex items-center justify-center transition-opacity pointer-events-none"
                                                  )}
                                                  aria-hidden="true"
                                                >
                                                  <svg
                                                    className="h-3 w-3 text-gray-400"
                                                    fill="none"
                                                    viewBox="0 0 12 12"
                                                  >
                                                    <path
                                                      d="M4 8l2-2m0 0l2-2M6 6L4 4m2 2l2 2"
                                                      stroke="currentColor"
                                                      strokeWidth={2}
                                                      strokeLinecap="round"
                                                      strokeLinejoin="round"
                                                    />
                                                  </svg>
                                                </span>
                                                <span
                                                  className={classNames(
                                                    notifyEnabled
                                                      ? "opacity-100 ease-in duration-200"
                                                      : "opacity-0 ease-out duration-100",
                                                    "absolute inset-0 h-full w-full flex items-center justify-center transition-opacity pointer-events-none"
                                                  )}
                                                  aria-hidden="true"
                                                >
                                                  <svg
                                                    className="h-3 w-3 text-green-600"
                                                    fill="currentColor"
                                                    viewBox="0 0 12 12"
                                                  >
                                                    <path d="M3.707 5.293a1 1 0 00-1.414 1.414l1.414-1.414zM5 8l-.707.707a1 1 0 001.414 0L5 8zm4.707-3.293a1 1 0 00-1.414-1.414l1.414 1.414zm-7.414 2l2 2 1.414-1.414-2-2-1.414 1.414zm3.414 2l4-4-1.414-1.414-4 4 1.414 1.414z" />
                                                  </svg>
                                                </span>
                                              </span>
                                            </Switch>
                                          </div>
                                        </div>

                                        <div className="mt-1 sm:flex sm:items-start sm:justify-between">
                                          <div className="max-w-xl text-sm text-gray-500">
                                            <Switch.Description>
                                              Be notified and/or send your
                                              client a follow up email for
                                              follow up work
                                              <br></br>(e.g.if maintenance may
                                              be required).
                                            </Switch.Description>
                                          </div>
                                        </div>

                                        {/* <div onClick={togglenotify}>
                                                                         <ChevronDownIcon
                                                                         className={classNames(
                                                                         showNotify ? 'rotate-180' : '', "h-8 w-8 text-gray-400 cursor-pointer absolute top-5 right-5 transform  transition duration-300 ease-in-out"
                                                                         )}
                                                                         />
                                                                         </div> */}
                                      </Switch.Group>

                                      <div
                                        className={classNames(
                                          showNotify ? "block" : "hidden",
                                          "collapse px-4 py-5 sm:p-6 border-t border-gray-200"
                                        )}
                                      >
                                        <div className="mb-4">
                                          <div>
                                            <label
                                              htmlFor="first-name"
                                              className="inline-flex items-center text-md tracking-wider font-medium text-gray-500"
                                            >
                                              Notify on*
                                            </label>

                                            <div className="mt-1 date-picker relative bg-white border-2 focus:ring-indigo-400 focus:border-indigo-400 border-gray-200 rounded-md">
                                              <DateTimePicker
                                                date={values.notifyMeOn}
                                                onDateChange={(val) => {
                                                  setFieldValue(
                                                    "notifyMeOn",
                                                    val
                                                  );
                                                }}
                                                minDate={minDate}
                                                name="notifyMeOn"
                                                id="notifyMeOn"
                                                inputClassName={
                                                  "input py-3 px-4 block w-full border-0"
                                                }
                                                placeholder=""
                                              />
                                            </div>
                                          </div>
                                        </div>

                                        <div className="mb-4">
                                          <div>
                                            <div className="flex justify-between">
                                              <label
                                                htmlFor="first-name"
                                                className="inline-flex items-center text-md tracking-wider font-medium text-gray-500"
                                              >
                                                Reminder*
                                              </label>
                                              <InformationCircleIcon
                                                className="h-6 w-6 text-gray-400 ml-2  focus:outline-none"
                                                data-for="notifyme"
                                                data-tip="The reminder wording will be used as the body of the email or notification."
                                              />
                                              <ReactTooltip
                                                id="notifyme"
                                                className="custom-tooltip bg-gray-900"
                                                textColor="#ffffff"
                                                backgroundColor="#111827"
                                                effect="solid"
                                              />
                                            </div>

                                            <div className="mt-1 relative">
                                              <textarea
                                                type="text"
                                                rows={3}
                                                name="reminder"
                                                id="reminder"
                                                className="placeholder-gray-400 py-3 px-4 block w-full border-2 border-gray-200  focus:ring-indigo-400 focus:border-indigo-400 border-gray-100 rounded-md"
                                                // className={"placeholder-gray-400 py-3 px-4 block w-full border-2 border-gray-200  focus:ring-indigo-400 focus:border-indigo-400 border-gray-100 rounded-md " + classes.fControl}
                                                placeholder="Enter your personalised message here..."
                                                variant="outlined"
                                                value={values.reminder}
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                              />
                                            </div>
                                          </div>
                                        </div>

                                        <div className="flex justify-between items-center pl-1">
                                          <RadioGroup
                                            row
                                            id="fllowReminder"
                                            name="fllowReminder"
                                            value={values.fllowReminder}
                                            onChange={handleChange}
                                          >
                                            <FormControlLabel
                                              value="only"
                                              control={<Radio />}
                                              label="&nbsp;Notify me only"
                                            />

                                            <FormControlLabel
                                              value="both"
                                              control={<Radio />}
                                              label="&nbsp;Notify client & me"
                                            />
                                          </RadioGroup>

                                          {values.fllowReminder === "both" ? (
                                            <>
                                              <button
                                                type="button"
                                                onClick={() => {
                                                  setInvoiceType("FOLLOWUP");
                                                  setPreview(true);
                                                  setDraft(true);
                                                  if (
                                                    Object.keys(errors)
                                                      .length == 0
                                                  ) {
                                                    notifyEmail(values);
                                                  } else {
                                                    if (
                                                      Object.keys(errors)
                                                        .length == 1 &&
                                                      Object.keys(
                                                        errors
                                                      ).indexOf("items") !== -1
                                                    ) {
                                                      notifyEmail(values);
                                                    } else {
                                                      setAlertType("Error");
                                                      setMsg(
                                                        "Please complete mandatory fields in order to preview."
                                                      );
                                                      setOpenAlert(true);
                                                    }
                                                  }
                                                }}
                                                className="inline-flex items-center mr-2 px-4 py-1.5 border border-indigo-400 text-sm font-medium  rounded-full text-brand-blue bg-gray-50 hover:bg-indigo-100 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-purple-500"
                                              >
                                                Preview
                                              </button>
                                            </>
                                          ) : (
                                            ""
                                          )}
                                        </div>
                                      </div>
                                    </div>

                                    {/* end Follow up work reminder */}

                                    <div className="bg-gray-100  mb-2 relative shadow hidden sm:rounded-lg">
                                      <Switch.Group
                                        as="div"
                                        className="px-4 flex items-center justify-between py-5 sm:p-6"
                                      >
                                        <div>
                                          <Switch.Label
                                            as="h3"
                                            className="text-lg leading-6 font-medium text-gray-700"
                                            passive
                                          >
                                            Instant Pay
                                          </Switch.Label>

                                          <div className="mt-1 sm:flex sm:items-start sm:justify-between">
                                            <div className="max-w-xl text-sm text-gray-500">
                                              <Switch.Description>
                                                Your subtitle goes here.
                                              </Switch.Description>
                                            </div>
                                          </div>
                                        </div>

                                        <Switch
                                          checked={isInstantpay}
                                          onChange={setIsInstantpay}
                                          className={classNames(
                                            isInstantpay
                                              ? "bg-green-600"
                                              : "bg-gray-300",
                                            "relative inline-flex flex-shrink-0 h-6 w-11 border-2 border-transparent rounded-full cursor-pointer transition-colors ease-in-out duration-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                                          )}
                                        >
                                          <span
                                            className={classNames(
                                              isInstantpay
                                                ? "translate-x-5"
                                                : "translate-x-0",
                                              "pointer-events-none relative inline-block h-5 w-5 rounded-full bg-white shadow transform ring-0 transition ease-in-out duration-200"
                                            )}
                                          >
                                            <span
                                              className={classNames(
                                                isInstantpay
                                                  ? "opacity-0 ease-out duration-100"
                                                  : "opacity-100 ease-in duration-200",
                                                "absolute inset-0 h-full w-full flex items-center justify-center transition-opacity pointer-events-none"
                                              )}
                                              aria-hidden="true"
                                            >
                                              <svg
                                                className="h-3 w-3 text-gray-400"
                                                fill="none"
                                                viewBox="0 0 12 12"
                                              >
                                                <path
                                                  d="M4 8l2-2m0 0l2-2M6 6L4 4m2 2l2 2"
                                                  stroke="currentColor"
                                                  strokeWidth={2}
                                                  strokeLinecap="round"
                                                  strokeLinejoin="round"
                                                />
                                              </svg>
                                            </span>
                                            <span
                                              className={classNames(
                                                isInstantpay
                                                  ? "opacity-100 ease-in duration-200"
                                                  : "opacity-0 ease-out duration-100",
                                                "absolute inset-0 h-full w-full flex items-center justify-center transition-opacity pointer-events-none"
                                              )}
                                              aria-hidden="true"
                                            >
                                              <svg
                                                className="h-3 w-3 text-green-600"
                                                fill="currentColor"
                                                viewBox="0 0 12 12"
                                              >
                                                <path d="M3.707 5.293a1 1 0 00-1.414 1.414l1.414-1.414zM5 8l-.707.707a1 1 0 001.414 0L5 8zm4.707-3.293a1 1 0 00-1.414-1.414l1.414 1.414zm-7.414 2l2 2 1.414-1.414-2-2-1.414 1.414zm3.414 2l4-4-1.414-1.414-4 4 1.414 1.414z" />
                                              </svg>
                                            </span>
                                          </span>
                                        </Switch>
                                      </Switch.Group>
                                    </div>

                                    {/* end Instant PayInstant Pay */}

                                    <div className="bg-gray-100  relative shadow sm:rounded-lg">
                                      <Switch.Group
                                        as="div"
                                        className="px-4  py-5 sm:p-6"
                                      >
                                        <div className="inline-flex  justify-between w-full">
                                          <Switch.Label
                                            as="h3"
                                            className="text-lg flex leading-6 font-medium text-gray-700"
                                            passive
                                          >
                                            Schedule / Repeating Invoice
                                            <a
                                              target="_blank"
                                              href="https://soleapp.com.au/academy_article/scheduled-or-repeating-invoices/"
                                            >
                                              <InformationCircleIcon
                                                className="h-6 w-6 text-gray-400 ml-2  focus:outline-none"
                                                data-for=""
                                                data-tip=""
                                              />
                                            </a>
                                          </Switch.Label>

                                          <div className="mt-5 sm:mt-0 sm:ml-6 sm:flex-shrink-0 sm:flex sm:items-center">
                                            <Switch
                                              checked={isRecurrentInvoice}
                                              onChange={(e) => {
                                                setIsRecurrentInvoice(e);
                                                toggle(e);
                                              }}
                                              className={classNames(
                                                isRecurrentInvoice
                                                  ? "bg-green-600"
                                                  : "bg-gray-300",
                                                "relative inline-flex flex-shrink-0 h-6 w-11 border-2 border-transparent rounded-full cursor-pointer transition-colors ease-in-out duration-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                                              )}
                                            >
                                              <span
                                                className={classNames(
                                                  isRecurrentInvoice
                                                    ? "translate-x-5"
                                                    : "translate-x-0",
                                                  "pointer-events-none relative inline-block h-5 w-5 rounded-full bg-white shadow transform ring-0 transition ease-in-out duration-200"
                                                )}
                                              >
                                                <span
                                                  className={classNames(
                                                    isRecurrentInvoice
                                                      ? "opacity-0 ease-out duration-100"
                                                      : "opacity-100 ease-in duration-200",
                                                    "absolute inset-0 h-full w-full flex items-center justify-center transition-opacity pointer-events-none"
                                                  )}
                                                  aria-hidden="true"
                                                >
                                                  <svg
                                                    className="h-3 w-3 text-gray-400"
                                                    fill="none"
                                                    viewBox="0 0 12 12"
                                                  >
                                                    <path
                                                      d="M4 8l2-2m0 0l2-2M6 6L4 4m2 2l2 2"
                                                      stroke="currentColor"
                                                      strokeWidth={2}
                                                      strokeLinecap="round"
                                                      strokeLinejoin="round"
                                                    />
                                                  </svg>
                                                </span>
                                                <span
                                                  className={classNames(
                                                    isRecurrentInvoice
                                                      ? "opacity-100 ease-in duration-200"
                                                      : "opacity-0 ease-out duration-100",
                                                    "absolute inset-0 h-full w-full flex items-center justify-center transition-opacity pointer-events-none"
                                                  )}
                                                  aria-hidden="true"
                                                >
                                                  <svg
                                                    className="h-3 w-3 text-green-600"
                                                    fill="currentColor"
                                                    viewBox="0 0 12 12"
                                                  >
                                                    <path d="M3.707 5.293a1 1 0 00-1.414 1.414l1.414-1.414zM5 8l-.707.707a1 1 0 001.414 0L5 8zm4.707-3.293a1 1 0 00-1.414-1.414l1.414 1.414zm-7.414 2l2 2 1.414-1.414-2-2-1.414 1.414zm3.414 2l4-4-1.414-1.414-4 4 1.414 1.414z" />
                                                  </svg>
                                                </span>
                                              </span>
                                            </Switch>
                                          </div>
                                        </div>

                                        <div className="mt-1 sm:flex sm:items-start sm:justify-between">
                                          <div className="max-w-xl text-sm text-gray-500">
                                            <Switch.Description>
                                              Send this invoice at a future date
                                              or send multiple emails <br></br>
                                              (automatically or review prior to
                                              sending)
                                            </Switch.Description>
                                          </div>
                                        </div>
                                      </Switch.Group>

                                      <div
                                        className={classNames(
                                          showMe ? "block" : "hidden",
                                          "collapse px-4 py-5 sm:p-6 border-t border-gray-200"
                                        )}
                                      >
                                        <div className="pl-1">
                                          <RadioGroup
                                            id="onOccurance"
                                            name="onOccurance"
                                            value={values.onOccurance}
                                            onChange={(e) => {
                                              if (!onceOff) {
                                                setFieldValue(
                                                  "onOccurance",
                                                  e.target.value
                                                );
                                              }
                                            }}
                                          >
                                            <FormControlLabel
                                              value="1"
                                              control={<Radio />}
                                              label="&nbsp;Save invoice as a draft on each occurrence and alert me"
                                            />
                                            <FormControlLabel
                                              value="2"
                                              control={<Radio />}
                                              label="&nbsp;Send automatically to the client on each occurrence"
                                            />
                                          </RadioGroup>
                                        </div>

                                        <div className="grid grid-cols-1 gap-y-2 mt-5 sm:grid-cols-2 sm:gap-x-8">
                                          <div>
                                            {/* onChange={setFrequency} */}
                                            <label
                                              htmlFor="first-name"
                                              className="block text-md tracking-wider font-medium text-gray-500"
                                            >
                                              Frequency
                                            </label>
                                            <Listbox
                                              value={frequency}
                                              onChange={(e) =>
                                                handleFrequencyChange(
                                                  e,
                                                  setFieldValue
                                                )
                                              }
                                            >
                                              {({ open }) => (
                                                <>
                                                  <div className="mt-1 relative">
                                                    <Listbox.Button className="py-3 px-4 block relative text-left bg-white cursor-default  w-full border-2 border-gray-200  focus:ring-indigo-400 focus:border-indigo-400 border-gray-100 rounded-md">
                                                      <span className="block truncate">
                                                        {frequency.name}
                                                      </span>
                                                      <span className="absolute inset-y-0 right-0 flex items-center pr-2 pointer-events-none">
                                                        <SelectorIcon
                                                          className="h-5 w-5 text-gray-400"
                                                          aria-hidden="true"
                                                        />
                                                      </span>
                                                    </Listbox.Button>
                                                    <Transition
                                                      show={open}
                                                      as={Fragment}
                                                      leave="transition ease-in duration-100"
                                                      leaveFrom="opacity-100"
                                                      leaveTo="opacity-0"
                                                    >
                                                      <Listbox.Options
                                                        static
                                                        className="absolute z-10 mt-1 w-full bg-white shadow-lg max-h-60 rounded-md py-1 text-base ring-1 ring-black ring-opacity-5 overflow-auto focus:outline-none sm:text-sm"
                                                      >
                                                        {frequencyList.map(
                                                          (action) => (
                                                            <Listbox.Option
                                                              key={action.id}
                                                              className={({
                                                                active,
                                                              }) =>
                                                                classNames(
                                                                  active
                                                                    ? "text-white bg-brand-blue"
                                                                    : "text-gray-900",
                                                                  "cursor-default select-none relative py-2 pl-3 pr-9"
                                                                )
                                                              }
                                                              value={action}
                                                            >
                                                              {({
                                                                selected,
                                                                active,
                                                              }) => (
                                                                <>
                                                                  <span
                                                                    className={classNames(
                                                                      selected
                                                                        ? "font-semibold"
                                                                        : "font-normal",
                                                                      "block truncate"
                                                                    )}
                                                                  >
                                                                    {
                                                                      action.name
                                                                    }
                                                                  </span>

                                                                  {selected ? (
                                                                    <span
                                                                      className={classNames(
                                                                        active
                                                                          ? "text-white"
                                                                          : "text-brand-blue",
                                                                        "absolute inset-y-0 right-0 flex items-center pr-4"
                                                                      )}
                                                                    >
                                                                      <CheckIcon
                                                                        className="h-5 w-5"
                                                                        aria-hidden="true"
                                                                      />
                                                                    </span>
                                                                  ) : null}
                                                                </>
                                                              )}
                                                            </Listbox.Option>
                                                          )
                                                        )}
                                                      </Listbox.Options>
                                                    </Transition>
                                                  </div>
                                                </>
                                              )}
                                            </Listbox>
                                          </div>

                                          <div>
                                            <label
                                              htmlFor="company"
                                              className="block text-md tracking-wider font-medium text-gray-500"
                                            >
                                              Number of occurrences
                                            </label>
                                            <div className="mt-1">
                                              <input
                                                name="noOfOccurance"
                                                id="noOfOccurance"
                                                type="number"
                                                readOnly={onceOff}
                                                className="py-3 px-4 block w-full border-2 border-gray-200  focus:ring-indigo-400 focus:border-indigo-400 border-gray-100 rounded-md"
                                                placeholder="Occurrences to repeat"
                                                value={values.noOfOccurance}
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                              />
                                            </div>
                                            <label className="block text-xs tracking-wider pt-1 text-gray-400">
                                              Enter number of occurrences to
                                              repeat. You may enter 0 to repeat
                                              indefinitely.
                                            </label>
                                          </div>

                                          {values.noOfOccurance > 1 ? (
                                            <div className="sm:col-span-2">
                                              <label
                                                htmlFor="company"
                                                className="block text-md tracking-wider font-medium text-gray-500"
                                              >
                                                Milestone Reference
                                              </label>
                                              <div className="mt-1">
                                                <input
                                                  name=""
                                                  id="tag_line"
                                                  type="text"
                                                  className="py-3 px-4 block w-full border-2 border-gray-200  focus:ring-indigo-400 focus:border-indigo-400 border-gray-100 rounded-md"
                                                  placeholder="Milestone Reference"
                                                  value={values.tag_line}
                                                  onChange={handleChange}
                                                  onBlur={handleBlur}
                                                />
                                              </div>
                                            </div>
                                          ) : (
                                            ""
                                          )}

                                          <div className="sm:col-span-2">
                                            <label
                                              htmlFor="last-name"
                                              className="block text-md tracking-wider font-medium text-gray-500"
                                            >
                                              Start Date
                                            </label>
                                            <div className="mt-1 relative date-picker bg-white border-2 focus:ring-indigo-400 focus:border-indigo-400 border-gray-200 rounded-md">
                                              <DateTimePicker
                                                date={values.startDate}
                                                onDateChange={(val) => {
                                                  setFieldValue(
                                                    "startDate",
                                                    val
                                                  );
                                                }}
                                                minDate={minDate}
                                                name="startDate"
                                                id="startDate"
                                                inputClassName={
                                                  "input py-3 px-4 block w-full border-0"
                                                }
                                                placeholder=""
                                              />
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>

                                    {/* end Recurring / Schedule Invoice? */}
                                  </div>

                                  <div className="grid grid-cols-2 gap-4 mt-4 pt-2">
                                    <div className="col-span-2 ml-auto w-full lg:w-1/2">
                                      <ul className="-my-4">
                                        <li className="flex items-center py-3 space-x-3">
                                          <div className="min-w-0 flex-1">
                                            <p className="text-md font-medium text-gray-700">
                                              Include GST (10%)
                                            </p>
                                          </div>

                                          <div className="flex-shrink-0">
                                            <Switch
                                              checked={gstEnabled}
                                              onChange={() => {
                                                setGstEnabled(!gstEnabled);
                                                calculation(
                                                  values.items,
                                                  "",
                                                  "",
                                                  "",
                                                  !gstEnabled
                                                );
                                              }}
                                              className={classNames(
                                                gstEnabled
                                                  ? "bg-green-600"
                                                  : "bg-gray-300",
                                                "relative inline-flex flex-shrink-0 h-6 w-11 border-2 border-transparent rounded-full cursor-pointer transition-colors ease-in-out duration-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                                              )}
                                            >
                                              <span className="sr-only">
                                                GST Enable/Disable
                                              </span>
                                              <span
                                                className={classNames(
                                                  gstEnabled
                                                    ? "translate-x-5"
                                                    : "translate-x-0",
                                                  "pointer-events-none relative inline-block h-5 w-5 rounded-full bg-white shadow transform ring-0 transition ease-in-out duration-200"
                                                )}
                                              >
                                                <span
                                                  className={classNames(
                                                    gstEnabled
                                                      ? "opacity-0 ease-out duration-100"
                                                      : "opacity-100 ease-in duration-200",
                                                    "absolute inset-0 h-full w-full flex items-center justify-center transition-opacity pointer-events-none"
                                                  )}
                                                  aria-hidden="true"
                                                >
                                                  <svg
                                                    className="h-3 w-3 text-gray-400"
                                                    fill="none"
                                                    viewBox="0 0 12 12"
                                                  >
                                                    <path
                                                      d="M4 8l2-2m0 0l2-2M6 6L4 4m2 2l2 2"
                                                      stroke="currentColor"
                                                      strokeWidth={2}
                                                      strokeLinecap="round"
                                                      strokeLinejoin="round"
                                                    />
                                                  </svg>
                                                </span>
                                                <span
                                                  className={classNames(
                                                    gstEnabled
                                                      ? "opacity-100 ease-in duration-200"
                                                      : "opacity-0 ease-out duration-100",
                                                    "absolute inset-0 h-full w-full flex items-center justify-center transition-opacity pointer-events-none"
                                                  )}
                                                  aria-hidden="true"
                                                >
                                                  <svg
                                                    className="h-3 w-3 text-green-600"
                                                    fill="currentColor"
                                                    viewBox="0 0 12 12"
                                                  >
                                                    <path d="M3.707 5.293a1 1 0 00-1.414 1.414l1.414-1.414zM5 8l-.707.707a1 1 0 001.414 0L5 8zm4.707-3.293a1 1 0 00-1.414-1.414l1.414 1.414zm-7.414 2l2 2 1.414-1.414-2-2-1.414 1.414zm3.414 2l4-4-1.414-1.414-4 4 1.414 1.414z" />
                                                  </svg>
                                                </span>
                                              </span>
                                            </Switch>
                                          </div>
                                        </li>

                                        <li className="flex items-center py-3 space-x-3">
                                          <div className="min-w-0 flex-1">
                                            <p className="text-md font-medium text-gray-700">
                                              Subtotal
                                            </p>
                                          </div>

                                          <div className="flex-shrink-0">
                                            <p className="text-md font-medium text-gray-600">
                                              <NumberFormat
                                                value={subtotAmount.toFixed(2)}
                                                displayType={"text"}
                                                thousandSeparator={true}
                                                prefix={"$"}
                                                renderText={(value) => (
                                                  <div>{value}</div>
                                                )}
                                              />
                                            </p>
                                          </div>
                                        </li>

                                        <li className="flex items-center py-3 space-x-3">
                                          <div className="min-w-0 flex-1">
                                            <p className="text-md font-medium text-gray-700">
                                              Discount
                                            </p>
                                          </div>

                                          <div className="flex-shrink-0">
                                            <p className="text-md font-medium text-gray-600">
                                              <NumberFormat
                                                value={discount.toFixed(2)}
                                                displayType={"text"}
                                                thousandSeparator={true}
                                                prefix={"$"}
                                                renderText={(value) => (
                                                  <div>{value}</div>
                                                )}
                                              />
                                            </p>
                                          </div>
                                        </li>

                                        <li className="flex items-center py-3 space-x-3">
                                          <div className="min-w-0 flex-1">
                                            <p className="text-md font-medium text-gray-700">
                                              GST
                                            </p>
                                          </div>

                                          <div className="flex-shrink-0">
                                            <p className="text-md font-medium text-gray-600">
                                              <NumberFormat
                                                value={gstAmount.toFixed(2)}
                                                displayType={"text"}
                                                thousandSeparator={true}
                                                prefix={"$"}
                                                renderText={(value) => (
                                                  <div>{value}</div>
                                                )}
                                              />
                                            </p>
                                          </div>
                                        </li>

                                        <li className="flex items-center border-t border-gray-200 py-3 space-x-3">
                                          <div className="min-w-0 flex-1">
                                            <p className="text-xl font-semibold text-gray-900">
                                              Total
                                            </p>
                                          </div>

                                          <div className="flex-shrink-0">
                                            <p className="text-lg font-semibold text-gray-800">
                                              <NumberFormat
                                                value={totAmount.toFixed(2)}
                                                displayType={"text"}
                                                thousandSeparator={true}
                                                prefix={"$"}
                                                renderText={(value) => (
                                                  <div>{value}</div>
                                                )}
                                              />
                                            </p>
                                          </div>
                                        </li>
                                      </ul>
                                    </div>
                                  </div>

                                  <div className="space-y-6 mt-5  border-t border-gray-200 mb-4 pt-6 pb-7 items-baseline grid grid-cols-1 gap-y-2 sm:pt-5 md:grid-cols-3 sm:gap-x-8">
                                    <div className="sm:col-span-3">
                                      <label
                                        htmlFor="company"
                                        className="block text-md tracking-wider font-medium text-gray-500"
                                      >
                                        Invoice notes
                                      </label>
                                      <div className="mt-1">
                                        <textarea
                                          type="text"
                                          rows={3}
                                          name="notes"
                                          id="notes"
                                          className="placeholder-gray-400 py-3 px-4 block w-full border-2 border-gray-200  focus:ring-indigo-400 focus:border-indigo-400 border-gray-100 rounded-md"
                                          placeholder="Enter your invoice note here..."
                                          variant="outlined"
                                          value={values.notes}
                                          onChange={handleChange}
                                          onBlur={handleBlur}
                                        />
                                      </div>
                                    </div>
                                  </div>
                                </>
                              ) : (
                                <div className="box-center">
                                  <CircularProgress />
                                </div>
                              )}
                            </div>
                          </div>
                        </div>

                        <div className="flex-shrink-0 border-t border-gray-200 px-4 py-4 flex justify-between">
                          <div>
                            {props.updateId && (
                              <span>
                                Last Invoice Sent:{" "}
                                {lastSent == "" || lastSent == null
                                  ? "----"
                                  : lastSentDate(lastSent)}
                              </span>
                            )}
                          </div>

                          <div>
                            <button
                              type="button"
                              id="draft"
                              className="bg-white py-2 px-4 border border-gray-300 rounded-full tracking-wider shadow-sm text-sm uppercase font-medium text-gray-700 bg-gray-100 hover:bg-gray-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                              disabled={isSubmitting ? true : false}
                              onClick={() => {
                                setBtnType("DRAFT");
                                setPreview(false);
                                setDraft(true);
                                handleSubmit();
                              }}
                            >
                              {isSubmitting && draft && btnType == "DRAFT"
                                ? "Please wait..."
                                : "Save Draft"}
                            </button>
                            <button
                              type="button"
                              id="save"
                              className="ml-4 inline-flex justify-center py-2 px-4 tracking-wider border border-transparent uppercase shadow-sm  font-medium text-sm rounded-full text-white bg-green-600 hover:bg-green-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-500"
                              disabled={isSubmitting ? true : false}
                              onClick={() => {
                                setBtnType("SAVE");
                                setPreview(false);
                                setDraft(false);
                                handleSubmit();
                              }}
                            >
                              {isSubmitting && !draft && btnType == "SAVE"
                                ? "Please wait..."
                                : "Save"}
                            </button>
                            <button
                              type="button"
                              id="send"
                              className="ml-4 inline-flex justify-center py-2 px-4 tracking-wider border border-transparent uppercase shadow-sm  font-medium text-sm rounded-full text-white bg-green-600 hover:bg-green-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-500"
                              disabled={isSubmitting ? true : false}
                              onClick={() => {
                                setBtnType("SENT");
                                setPreview(false);
                                setDraft(false);
                                handleSubmit();
                              }}
                            >
                              {isSubmitting && !draft && btnType == "SENT"
                                ? "Please wait..."
                                : "Save & Send"}
                            </button>
                          </div>
                        </div>
                      </Form>
                    )}
                  </Formik>

                  {/* Preview Slider   */}
                  <div>
                    <div
                      className={`${
                        isVisible ? "translate-x-0" : "  translate-x-full"
                      } "w-screen lg:max-w-7xl absolute preview transition transform ease-in-out duration-500 inset-0 overflow-y-auto`}
                    >
                      <div className="h-full items-center justify-center  text-center bg-white shadow-xl">
                        <div className="bg-gray-100 py-4 px-4 sm:px-6 border-b border-gray-200 ">
                          <div className="flex items-center justify-between">
                            <h3 className="text-lg font-medium text-left text-gray-800">
                              {invoiceType == "NORMAL"
                                ? "Invoice Preview"
                                : "Invoice Follow Up Preview"}
                            </h3>

                            <div className="ml-3 h-7 flex items-center">
                              <button
                                type="button"
                                className="rounded-full hover:bg-gray-300 p-2 text-gray-500 hover:text-gray-700 focus:outline-none focus:ring-2 focus:ring-white"
                                onClick={closeSlider}
                              >
                                <span className="sr-only">Close panel</span>
                                <XIcon className="h-6 w-6" aria-hidden="true" />
                              </button>
                            </div>
                          </div>
                        </div>

                        {!loading ? (
                          <iframe
                            src={link + "#toolbar=0"}
                            height="95%"
                            width="100%"
                          ></iframe>
                        ) : (
                          <div className="space-y-6 border-gray-200 mb-4 pt-6 pb-7 grid grid-cols-1 gap-y-2 sm:grid-cols-1">
                            {Array.from(Array(1), (e, i) => {
                              return (
                                <div className="sm:col-span-2" key={i}>
                                  <label className="info-label">
                                    <Skeleton />
                                  </label>
                                  <p>
                                    <Skeleton />
                                  </p>
                                </div>
                              );
                            })}
                          </div>
                        )}
                      </div>
                    </div>
                  </div>

                  <EmailDialog
                    id="email-dialog"
                    open={openEmailDialog}
                    email={email}
                    client={currentClient}
                    error={error}
                    type="invoice"
                    setDraft={setDraft}
                    setEmail={setEmail}
                    setError={setError}
                    onClose={setOpenEmailDialog}
                  />
                </div>
              </Transition.Child>
            </div>
          </div>
          <AlertDialog
            id="ringtone-menu2"
            title={alertType}
            message={msg}
            keepMounted
            open={openAlert}
            onClose={setOpenAlert}
          />
        </Dialog>
      </Transition.Root>
    </>
  );
}
