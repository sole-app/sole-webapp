import { useState, useEffect } from "react";
import Styles from "../../styles/Invoices.module.scss";
import ReactTooltip from "react-tooltip";
import { InformationCircleIcon } from "@heroicons/react/outline";
import { Doughnut, Bar } from "react-chartjs-2";
import { TopEarners as TopEarnersData } from "../../services/api/invoice.services";
import store from "../../store/store";
import CircularProgress from "@material-ui/core/CircularProgress";
import { getDateFromFYDates } from "../../helpers/common";
import YearMonthFilter from "../Common/YearMonthFilter";

const months = [
  "Jan",
  "Feb",
  "Mar",
  "Apr",
  "May",
  "Jun",
  "Jul",
  "Aug",
  "Sep",
  "Oct",
  "Nov",
  "Dec",
];

const plugin = {
  id: "increase-legend-spacing",
  beforeInit(chart) {
    // Get reference to the original fit function
    const originalFit = chart.legend.fit;

    // Override the fit function
    chart.legend.fit = function fit() {
      // Call original function and bind scope in order to use `this` correctly inside it
      originalFit.bind(chart.legend)();
      // Change the height as suggested in another answers
      this.height += 20;
    };
  },
};

const options = {
  maintainAspectRatio: false,
  plugins: {
    title: {
      display: true,
      text: "",
    },

    legend: {
      display: true,
      position: "right",
      align: "center",
      margin: 20,
      onClick: null,
      borderRadius: 15,

      labels: {
        // This more specific font property overrides the global property
        font: {
          size: 13,
          weight: "bold",
          lineHeight: 2,
          family: "'Inter', sans-serif",
        },
        boxWidth: 20,
        boxHeight: 20,
        borderRadius: 15,
        padding: 20,
      },
    },

    tooltip: {
      callbacks: {
        label: function (context) {
          let label = context.label || "";

          if (label) {
            label += ": $";
          }
          if (context.formattedValue !== null) {
            label += context.formattedValue;
          }
          return label;
        },
      },
    },
  },

  layout: {
    padding: 20,
  },
};

export default function TopEarners(props) {
  const storeData = store.store.getState();
  const [type, setType] = useState("YEAR"); //YEAR, MONTH
  const [foundData, setFoundData] = useState(false);
  const [loader, setLoader] = useState(true);
  const [donutData, setDonutData] = useState({
    labels: ["Red", "Green", "Yellow"],
    datasets: [
      {
        data: [0, 0, 0],
        backgroundColor: ["#d1d5db", "#4C4DFE", "#2ef8ff"],
        hoverBackgroundColor: ["#d1d5db", "#4C4DFE", "#2ef8ff"],
      },
    ],
  });
  const [barData, setBarData] = useState({
    labels: ["Jan", "Feb", "Mar"],
    datasets: [
      {
        label: "Invoices",
        backgroundColor: "#198754",
        borderColor: "#1b6f46",
        borderWidth: 1,
        barWidth: 1,
        data: [0, 0, 0],
      },
    ],
  });

  const getData = async (filterType) => {
    let apiRunning = true;
    setLoader(apiRunning);
    setType(filterType);

    let formData = "";
    let dates = getDateFromFYDates(filterType);
    formData += "?start_date=" + dates.start_date;
    formData += "&end_date=" + dates.end_date;

    const res = await TopEarnersData(formData);
    let hasData = false;
    if (res.success) {
      let bdata = barData;
      if (
        typeof res.data.invoices_total !== "undefined" &&
        res.data.invoices_total.length > 0
      ) {
        hasData = true;
        let data = [];
        let labels = [];
        let totals = res.data.invoices_total;
        for (let i = 0; i < totals.length; i++) {
          data[i] = totals[i].total;
          labels[i] = months[totals[i].month - 1];
        }
        bdata.datasets[0].data = data;
        bdata.labels = labels;
        setBarData(bdata);
      }
      let ddata = donutData;
      if (
        typeof res.data.top_three_clients !== "undefined" &&
        res.data.top_three_clients.length > 0
      ) {
        let data = [];
        let labels = [];
        let clients = res.data.top_three_clients;
        for (let i = 0; i < clients.length; i++) {
          data[i] = clients[i].total;
          labels[i] = clients[i].client_name;
        }
        ddata.datasets[0].data = data;
        ddata.labels = labels;
        setDonutData(ddata);
      }
      apiRunning = false;
    }
    if (!apiRunning) {
      setLoader(false);
    }
    if (props.from === "dashboard" && hasData) {
      setFoundData(true);
    }
  };

  useEffect(() => {
    getData(type);
  }, [props.refreshSummary]);

  return (
    <div
      className={
        props.from === "dashboard" ? "mt-8 w-full" : "mt-8 w-full xl:w-1/2"
      }
    >
      <div className="grid grid-cols-1 h-full">
        {/* Top Earners */}
        <div className="bg-white overflow-hidden shadow rounded-lg">
          <div className="mx-auto border-b border-grey-400 p-3 sm:p-4  xl:p-5 lg:flex lg:items-center lg:justify-between">
            <h3 className="text-lg inline-flex font-medium tracking-wide text-gray-900 sm:text-xl">
              Top Earners
              <InformationCircleIcon
                className="h-6 w-6 text-gray-400 ml-2  focus:outline-none"
                data-for="help"
                data-tip=""
              />
              <ReactTooltip
                id="help"
                className="custom-tooltip bg-gray-900"
                border
                textColor="#ffffff"
                backgroundColor="#111827"
                effect="solid"
              >
                <p className="w-64">
                  Your top earners are calculated based on the invoices you have
                  sent for revenue won. Keep an eye on your best clients to
                  identify where to focus your energy!
                </p>
              </ReactTooltip>
            </h3>

            <div className="mt-8 flex lg:mt-0 lg:flex-shrink-0">
              {props.from !== "dashboard" && (
                <YearMonthFilter type={type} setType={getData} />
              )}
            </div>
          </div>

          {/* Chart */}
          <div className={Styles.earnerChart}>
            <div className="flex">
              {props.from === "dashboard" ? (
                <>
                  {loader ? (
                    <div className="flex items-center w-full pt-6 justify-center  rounded-md">
                      <div className="text-center">
                        <div className="box-center" style={{ height: "286px" }}>
                          <CircularProgress />
                        </div>
                      </div>
                    </div>
                  ) : (
                    <>
                      {!foundData ? (
                        <div className="flex items-center w-full pt-6 justify-center  rounded-md">
                          <div className="py-10 px-4">
                            <h3 className="my-4 text-xl font-medium text-gray-700">
                              {" "}
                              No data found
                            </h3>
                          </div>
                        </div>
                      ) : (
                        <>
                          <div className="w-full">
                            <Doughnut
                              data={donutData}
                              plugins={[plugin]}
                              options={options}
                              width={400}
                              height={350}
                            />
                          </div>

                          <div className="w-1/2 hidden">
                            <Bar
                              data={barData}
                              options={{
                                maintainAspectRatio: false,
                                title: {
                                  display: true,
                                  text: "Average Rainfall per month",
                                  fontSize: 20,
                                },
                                legend: {
                                  display: true,
                                  x: "left",
                                  borderRadius: "5px",
                                },
                              }}
                            />
                          </div>
                        </>
                      )}
                    </>
                  )}
                </>
              ) : (
                <>
                  <div className="w-full">
                    {!loader && (
                      <Doughnut
                        data={donutData}
                        options={options}
                        width={400}
                        height={250}
                      />
                    )}
                  </div>

                  <div className="w-1/2 hidden">
                    {!loader && (
                      <Bar
                        data={barData}
                        options={{
                          maintainAspectRatio: false,
                          title: {
                            display: true,
                            text: "Average Rainfall per month",
                            fontSize: 20,
                          },
                          legend: {
                            display: true,
                            x: "left",
                            borderRadius: "5px",
                          },
                        }}
                      />
                    )}
                  </div>
                </>
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
