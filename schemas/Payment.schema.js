import * as Yup from 'yup';

const PaymentSchema = Yup.object().shape({
  card_holder_name: Yup.string()
    .required('Please enter cardholder name').max(50)
});

export default PaymentSchema;
