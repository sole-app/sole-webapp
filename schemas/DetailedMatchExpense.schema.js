import * as Yup from 'yup';

const DetailedMatchExpenseSchema = Yup.object().shape({
  price: Yup.number()
    .required('Price is required.')
    .positive('Price is invalid.'),
  date: Yup.date()
    .required('Date is required.')
    .nullable()
    .default(null),
  description: Yup.string()
    .required('Description is required'),
  name: Yup.string()
    .required('Supplier is required'),
});

export default DetailedMatchExpenseSchema;
