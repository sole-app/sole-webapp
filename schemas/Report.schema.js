import * as Yup from 'yup';

const ReportSchema = Yup.object().shape({
    sendTo: Yup.string()
        .required('Send to is required.')
        .email('Enter a valid email address.')
        .max(150, 'Sent to should be of maximum 150 characters length.'),
    accountantEnable: Yup.boolean(),
    accountantName: Yup.string().when('accountantEnable', (accountantEnable) => {
        if (accountantEnable) {
            return Yup.string()
                .required('Accountant name is required.')
                .max(150, 'Accountant name should be of maximum 150 characters length.')
                .nullable()
                .default(null)
        } else {
            return Yup.string()
                .max(150, 'Accountant name should be of maximum 150 characters length.')
                .nullable()
                .default(null)
        }
    }),
    accountantEmail: Yup.string().when('accountantEnable', (accountantEnable) => {
        if (accountantEnable) {
            return Yup.string()
                .required('Accountant email is required.')
                .email('Enter a valid accountant email address')
                .max(150, 'Accountant email should be of maximum 150 characters length.')
                .nullable()
                .default(null)
        } else {
            return Yup.string()
                .email('Enter a valid accountant email address')
                .max(150, 'Accountant email should be of maximum 150 characters length.')
                .nullable()
                .default(null)
        }
    }),
    bookkeeperEnable: Yup.boolean(),
    bookkeeperName: Yup.string().when('bookkeeperEnable', (bookkeeperEnable) => {
        if (bookkeeperEnable) {
            return Yup.string()
                .required('Bookkeeper name is required.')
                .max(150, 'Bookkeeper name should be of maximum 150 characters length.')
                .nullable()
                .default(null)
        } else {
            return Yup.string()
                .max(150, 'Bookkeeper name should be of maximum 150 characters length.')
                .nullable()
                .default(null)
        }
    }),
    bookkeeperEmail: Yup.string().when('bookkeeperEnable', (bookkeeperEnable) => {
        if (bookkeeperEnable) {
            return Yup.string()
                .required('Bookkeeper email is required.')
                .email('Enter a valid accountant email address')
                .max(150, 'Bookkeeper email should be of maximum 150 characters length.')
                .nullable()
                .default(null)
        } else {
            return Yup.string()
                .email('Enter a valid accountant email address')
                .max(150, 'Bookkeeper email should be of maximum 150 characters length.')
                .nullable()
                .default(null)
        }
    }),
});

export default ReportSchema;