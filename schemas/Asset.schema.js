import * as Yup from 'yup';

const AssetSchema = Yup.object().shape({
  assetName: Yup.string()
    .required('Asset name is required.'),
  assetValue: Yup.number()
    .required('Asset value is required.')
    .positive('Asset value is invlaid.'),
  datePurchased: Yup.date()
    .required('Date of purchased is required.')
    .nullable()
    .default(null),
});

export default AssetSchema;
