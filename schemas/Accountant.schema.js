import * as Yup from 'yup';

const AccountantSchema = Yup.object().shape({
    accountant_name: Yup.string().ensure().when('accountant_email', {
        is: (accountant_email) => {
            return accountant_email.length > 0 ? true : false
        },
        then: Yup.string().required('Accountant name is required')
    }),
    accountant_email: Yup.string().ensure().when('accountant_name', {
        is: (accountant_name) => {
            return accountant_name?.length > 0 ? true : false
        },
        then: Yup.string().required('Accountant email is required')
    }),
    bookkeeper_name: Yup.string().ensure().when('bookkeeper_email', {
        is: (bookkeeper_email) => {
            return bookkeeper_email.length > 0 ? true : false
        },
        then: Yup.string().required('Bookkeeper name is required')
    }),
    bookkeeper_email: Yup.string().ensure().when('bookkeeper_name', {
        is: (bookkeeper_name) => {
            return bookkeeper_name?.length > 0 ? true : false
        },
        then: Yup.string().required('Bookkeeper email is required')
    }),
}, [
    ['accountant_email', 'accountant_name'],
    ['bookkeeper_email', 'bookkeeper_name']
]);

export default AccountantSchema;
