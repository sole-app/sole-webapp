import * as Yup from 'yup';

const mobileRegex = /^(?:\+?(61))? ?(?:\((?=.*\)))?(0?[2-57-8])\)? ?(\d\d(?:[- ](?=\d{3})|(?!\d\d[- ]?\d[- ]))\d\d[- ]?\d[- ]?\d{3})$/;

const UpdateProfileSchema = Yup.object().shape({
  full_name: Yup.string().required('Full name is required.'),
  industry_id: Yup.string().required('Industry is required.'),
  
  mobile_no: Yup.string()
    .required('Mobile number is required')
    .min(10).max(16)
    .matches(mobileRegex, "Enter a valid mobile number"),
  
  address: Yup.string()
    .required('Address is required'),    

  abn: Yup.string()
    .typeError('ABN is required')
    .min(11, "Please enter a valid 11 digit ABN number")
    .max(11, "Please enter a valid 11 digit ABN number"),

  business_name: Yup.string()
    .required('Business name is required.')
    .typeError('Business name is required')
    .max(200, "Maximum characters allowed for business name is 200"),

  bsb_number: Yup.string()
    .required('BSB number is required')
    .typeError('Please provide a valid BSB number'),

  bank_name: Yup.string()
    .required('Bank name is required'),

  account_holder: Yup.string()
    .required('Account holder name is required'),

  account_number: Yup.string()
    .required('Account number is required'),

});

export default UpdateProfileSchema;
