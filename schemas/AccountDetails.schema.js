import * as Yup from 'yup';

const mobileRegex = /^(?:\+?(61))? ?(?:\((?=.*\)))?(0?[2-57-8])\)? ?(\d\d(?:[- ](?=\d{3})|(?!\d\d[- ]?\d[- ]))\d\d[- ]?\d[- ]?\d{3})$/;

const AccountDetailsSchema = Yup.object().shape({
  first_name: Yup.string().required('First name is required'),
  last_name: Yup.string().required('Last name is required'),
  mobile_no: Yup.string()
    .required('Mobile number is required')
    .min(10).max(16)
    .matches(mobileRegex, "Enter a valid mobile number"),
  ABN: Yup.string()
    .typeError('ABN is required')
    .min(11, "Please enter a valid 11 digit ABN number")
    .max(11, "Please enter a valid 11 digit ABN number"),    
  industry_id: Yup.number().typeError('Select industry').min(1, 'Select industry'),
});

export default AccountDetailsSchema;
