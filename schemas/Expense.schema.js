import * as Yup from 'yup';

const ExpenseSchema = Yup.object().shape({
  price: Yup.number()
    .required('Price is required.')
    .positive('Price is invalid.'),
  date: Yup.date()
    .required('Date is required.')
    .nullable()
    .default(null),
  description: Yup.string()
    .required('Description is required'),
});

export default ExpenseSchema;
