import * as Yup from 'yup';

const MatchExpenseSchema = Yup.object().shape({
    client_name: Yup.string()
        .required('Supplier is required.'),
    name: Yup.string()
        .required('Name is required.'),
});

export default MatchExpenseSchema;
