import * as Yup from 'yup';

const mobileRegex = /^(?:\+?(61))? ?(?:\((?=.*\)))?(0?[2-57-8])\)? ?(\d\d(?:[- ](?=\d{3})|(?!\d\d[- ]?\d[- ]))\d\d[- ]?\d[- ]?\d{3})$/;
const passwordRegex = /^.*(?=.{8,})((?=.*[!@#$%^&*()\-_=+{};:,<.>]){1})(?=.*\d)((?=.*[a-z]){1})((?=.*[A-Z]){1}).*$/;

const CreateAccountSchema = Yup.object().shape({
  email: Yup.string()
    .required('Email is required')
    .email('Enter a valid email address'),
  mobile_no: Yup.string()
    .required('Mobile number is required')
    .min(10).max(16)
    .matches(mobileRegex, "Enter a valid mobile number"),
  password: Yup.string()
    .required('Password is required')
    .min(8, 'Password should be of minimum 8 characters length')
    .matches(passwordRegex, "Password not matching criteria."),
  confirm_password: Yup.string()
    .when("password", {
      is: val => (val && val.length > 0 ? true : false),
      then: Yup.string().oneOf(
        [Yup.ref("password")], "Both password need to be the same"
      )
    })
    .required('Confirm Password is required')
});

export default CreateAccountSchema;
