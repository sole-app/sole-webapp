import * as Yup from 'yup';

const QuoteSchema = Yup.object().shape({
  client_name: Yup.string()
    .required('Client is required.')
    .nullable(),
  quoteDate: Yup.date()
    .required('Quote date is required.')
    .nullable()
    .default(null),
  quoteExpDate: Yup.date()
    .required('Quote expiry date is required.')
    .nullable()
    .default(null),
  description: Yup.string()
    .required('Description is required'),
  items: Yup.array()
    .of(
      Yup.object().shape({
        price: Yup.number().required("Price is required"),
        description: Yup.string().required("Description is required"),
        qty: Yup.number().required("Qty is required"),
      })
    ),
});

export default QuoteSchema;
