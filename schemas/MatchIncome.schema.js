import * as Yup from 'yup';

const MatchIncomeSchema = Yup.object().shape({
    client_name: Yup.string()
        .required('Client is required.'),
});

export default MatchIncomeSchema;
