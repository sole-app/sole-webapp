import * as Yup from 'yup';

const InvoiceSchema = Yup.object().shape({
  client_name: Yup.string()
    .required('Client is required.')
    .nullable(),
  invoiceDate: Yup.date()
    .required('Invoice date is required.')
    .nullable()
    .default(null),
  dueDate: Yup.date()
    .required('Due date is required.')
    .nullable()
    .default(null),
  description: Yup.string()
    .required('Description is required'),
  items: Yup.array()
    .of(
      Yup.object().shape({
        price: Yup.number().required("Price is required").nullable(),
        description: Yup.string().required("Description is required"),
        qty: Yup.number().required("Qty is required"),
      })
    ),
});

export default InvoiceSchema;
