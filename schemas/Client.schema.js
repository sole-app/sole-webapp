import * as Yup from 'yup';

const numberRegex = '^[0-9]*$';
const websiteRegex = /^((http|https):\/\/)?(www.)?(?!.*(http|https|www.))[a-zA-Z0-9_-]+(\.[a-zA-Z]+)+(\/)?.([\w\?[a-zA-Z-_%\/@?]+)*([^\/\w\?[a-zA-Z0-9_-]+=\w+(&[a-zA-Z0-9_]+=\w+)*)?$/;


const ClientSchema = Yup.object().shape({
  name: Yup.string()
    .required('Please enter the client/supplier name'),
  email: Yup.string()
    .required('Please provide a valid email address.')
    .email('Enter a valid email address'),

  mobile_no: Yup.string()
    .min(8, "Enter a valid phone number").max(10, "Enter a valid phone number")
    .matches(numberRegex, "Enter a valid phone number"),

  website: Yup.string()
    .matches(websiteRegex, "Enter a valid website URL"),

  ABN: Yup.string()
    .nullable()
    .min(11, "Please enter a valid 11 digit ABN number").max(11, "Please enter a valid 11 digit ABN number")
    .matches(numberRegex, "Please enter a valid 11 digit ABN number"),

  //address: Yup.string().required('Address is required'),    

});

export default ClientSchema;
