export const addUser = (user_details) => {
  return {
    type: "ADD_USER_DETAILS",
    payload: user_details
  }
};

export const removeUser = () => {
  return {
    type: "REMOVE_USER_DETAILS"
  }
};

export const addSubscription = (subscription) => {
  return {
    type: "ADD_SUBSCRIPTION",
    payload: subscription
  }
};

export const setRefreshSubscription = () => {
  return {
    type: "REFRESH_SUBSCRIPTION"
  }
};

export const setPrimaryAccount = (account) => {
  return {
    type: "SET_PRIMARY_ACCOUNT",
    payload: account
  }
};

export const setHighlightPrimary = (highlight) => {
  return {
    type: "SET_HIGHTLIGHT_PRIMARY_ACCOUNT",
    payload: highlight
  }
};

export const setTokenExpired = (payload) => {
  return {
    type: "SET_TOKEN_EXPIRED",
    payload: payload
  }
};