const securityHeaders = [
    {
        key: 'X-DNS-Prefetch-Control',
        value: 'on'
    },
    {
        key: 'Strict-Transport-Security',
        value: 'max-age=63072000; includeSubDomains; preload'
    },
    {
        key: 'X-XSS-Protection',
        value: '1; mode=block'
    },
    {
        key: 'X-Frame-Options',
        value: 'SAMEORIGIN'
    },
    {
        key: 'Permissions-Policy',
        value: 'geolocation=()'
    },
    {
        key: 'X-Content-Type-Options',
        value: 'nosniff'
    },
    {
        key: 'Referrer-Policy',
        value: 'origin-when-cross-origin'
    },
    /*{
        key: 'Content-Security-Policy',
        value: "default-src * data: 'unsafe-inline' blob:"
    }*/
];

const webpack = require('webpack');
module.exports = {
    reactStrictMode: process.env.NODE_ENV === "production",
    webpack: (config, { buildId, dev, isServer, defaultLoaders, webpack }) => {
        return config;
    },
    images: {
        domains: [
            'soleapp.com.au',
            'api.soleapp.com.au',
            'static.soleapp.com.au',
            'sole-webapp-clone.vercel.app',
            'staging.soleapp.com.au',
            'test.bankstatements.com.au'
        ],
    },
    async headers() {
        return [
            {
                // Apply these headers to all routes in your application.
                source: '/:path*',
                headers: securityHeaders,
            },
            {
                // Apply these headers to the home page
                source: '/',
                headers: securityHeaders,
            },
        ]
    },
}