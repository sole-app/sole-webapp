import config from "./config";

const apiRoutes = {
    //APIS
    device: {
        register: config.baseUrl + 'v1/device/register',
    },
    auth: {
        login: config.baseUrl + 'v2/user/login',
        signUp: config.baseUrl + 'v2/user/signup',
        verifyEmail: config.baseUrl + 'v1/user/verifyEmail',
        deviceAddUpdate: config.baseUrl + 'v1/device/register',
        forgotPassword: config.baseUrl + 'v1/user/forgotPassword',
        resendVerificationCode: config.baseUrl + 'v1/user/resend_verification_code',
        accountActivate: config.baseUrl + 'v1/user/account/activate',
    },
    user: {
        updateUserProfile: config.baseUrl + 'v1/user/update',
        userLogout: config.baseUrl + 'v1/user/logout',
        deleteAccount: config.baseUrl + 'v2/user/delete-account',
        notification: config.baseUrl + 'v2/user/notification'
    },
    subscription: {
        add: config.baseUrl + 'v1/addSubscription',
        validateCoupon: config.baseUrl + 'v1/coupon/validate',
        cancel: config.baseUrl + 'v2/stripe/subscription/cancel',
        resume: config.baseUrl + 'v2/stripe/subscription/resume',
    },
    accountCategory: {
        list: config.baseUrl + 'v2/account-categories',
    },
    dashboard: {
        get: config.baseUrl + 'v1/web-app/dashboard',
    },
    goal: {
        set: config.baseUrl + 'v1/dashboard/setGoal'

    },
    asset: {
        list: config.baseUrl + 'v2/assets',
        add: config.baseUrl + 'v2/asset',
        delete: config.baseUrl + 'v1/asset/remove',
        update: config.baseUrl + 'v1/asset/edit',
        info: config.baseUrl + 'v2/asset/',
    },
    quote: {
        list: config.baseUrl + 'v2/quotes',
        add: config.baseUrl + 'v1/quote/add',
        delete: config.baseUrl + 'v1/quote/remove',
        update: config.baseUrl + 'v1/quote/edit',
        info: config.baseUrl + 'v2/quote/',
        summary: config.baseUrl + 'v2/quote-summary',
        topEarners: config.baseUrl + 'v2/quote-top-earners',
    },
    client: {
        listwithPagination: config.baseUrl + 'v1/client/getAll',
        list: config.baseUrl + 'v1/client/get',
        listV2: config.baseUrl + 'v2/clients',
        add: config.baseUrl + 'v1/client/add',
        delete: config.baseUrl + 'v1/client/remove',
        update: config.baseUrl + 'v1/client/edit',
        info: config.baseUrl + 'v2/client/',
    },
    preview: {
        view: config.baseUrl + 'v1/preview/pdf',
    },
    expense: {
        list: config.baseUrl + 'v2/expenses',
        add: config.baseUrl + 'v2/expense',
        delete: config.baseUrl + 'v1/expense/remove',
        update: config.baseUrl + 'v1/expense/edit',
        info: config.baseUrl + 'v2/expense/',
        summary: config.baseUrl + 'v2/expense-summary',
        download: config.baseUrl + 'v2/expense/download',
    },
    invoice: {
        list: config.baseUrl + 'v2/invoices',
        add: config.baseUrl + 'v1/invoice/add',
        delete: config.baseUrl + 'v1/invoice/remove',
        update: config.baseUrl + 'v1/invoice/edit',
        info: config.baseUrl + 'v2/invoice/',
        paidWithCash: config.baseUrl + 'v1/invoice/paid/cash',
        convertToInvoice: config.baseUrl + 'v1/invoice/quoteToInvoice',
        followUpReminder: config.baseUrl + 'v1/invoice/notify-future-work/email/preview',
        summary: config.baseUrl + 'v2/invoice-summary',
        topEarners: config.baseUrl + 'v2/invoice-top-earners',
        download: config.baseUrl + 'v2/invoice/download',
    },
    report: {
        send: config.baseUrl + 'v1/report/send',
    },
    tax: {
        getFinancialYears: config.baseUrl + 'v1/tax/getFinancialYears',
        getTaxCalculation: config.baseUrl + 'v1/tax/getTaxCalculation',
        income: config.baseUrl + 'v1/tax/income',
        expenses: config.baseUrl + 'v1/tax/expenses',
        calculateTax: config.baseUrl + 'v1/tax/calculateTax',
    },
    bankConnection: {
        getInstituteList: config.baseUrl + 'v1/bank_feeds/get_institutions',
        createCustomer: config.baseUrl + 'v1/bank_feeds/create_customer',
        listAccounts: config.baseUrl + 'v1/bank_feeds/list_customer_accounts',
        selectAccount: config.baseUrl + 'v1/bank_feeds/select_customer_account',
        refreshAccountData: config.baseUrl + 'v1/bank_feeds/refresh_customer_account_data',
        updateCustomer: config.baseUrl + 'v2/bank-feeds/update-customer',
        additionalAuth: config.baseUrl + 'v1/bank-feeds/follow-up-request',
    },
    auditor: {  // Accountant & Bookkeepper
        add: config.baseUrl + 'v1/auditor/add',
        update: config.baseUrl + 'v1/auditor/update',
        get: config.baseUrl + 'v1/auditor/get',
    },
    bankAccount: {
        list: config.baseUrl + 'v2/bank_account/getAll',
        delete: config.baseUrl + 'v1/bank_account/remove',
        makePrimary: config.baseUrl + 'v1/bank_account/primary',
    },
    bankTransaction: {
        bankTransactions: config.baseUrl + 'v2/bank/bank-transactions',
        soleTransactions: config.baseUrl + 'v2/bank/sole-transactions',
        undo: config.baseUrl + 'v2/bank-transaction/undo',
        getCashflow: config.baseUrl + 'v1/bank_transaction/getMonthlyCashflow',

        markPersonal: config.baseUrl + 'v1/bank_transaction/markPersonal',
        undoCashInvoice: config.baseUrl + 'v1/bank_transaction/undoCashInvoice',

        setMatchExpenses: config.baseUrl + 'v2/bank-transaction/match-asset-expense',
        getPossibleMatchInvoices: config.baseUrl + 'v1/bank_transaction/getPossibleMatchInvoices',
        setMatchInvoices: config.baseUrl + 'v2/bank-transaction/match-invoice',
        setMatch: config.baseUrl + 'v2/bank-transaction/set-match',
        getMatchTransactionsByClient: config.baseUrl + 'v1/bank_transaction/getMatchTransactionsByClient',
        getPossibleMatchExpenses: config.baseUrl + 'v2/bank-transaction/possible-match-assets-expenses/',
        fetchPossibleMatchExpenses: config.baseUrl + 'v2/bank-transaction/assets-expenses',
        fetchAllBalance: config.baseUrl + 'v2/bank/all-balances',
    },
    notification: {
        get: config.baseUrl + 'v2/notification-settings',
        add: config.baseUrl + 'v2/notification-setting',
    }
};

export default apiRoutes;