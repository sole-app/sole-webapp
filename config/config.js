//local   = local environment
//staging = develop / stating environment
//live    = live / production environment

const env = `${process.env.NEXT_PUBLIC_API_ENV}`;

const versionData = {
    v1: 'v1/',
    v2: 'v2/',
};

//local
const localData = {
    //baseUrl: 'https://api.soleapp.com.au/production/api/',
    //baseUrl: 'https://staging.soleapp.com.au/staging1_1/api/',
    baseUrl: 'https://prod.soleapp.com.au/prod1_1/api/',
    stripeKey: 'pk_test_51IhkoFHXdXGlciB8Si1nWlxXANvUuOyqdIkZJCgk8MDtB47EcGbnaWg2pot4vW7G85gsF9XvCOmU07j1O6t2vpcL00rnnxcd1Z',
    apiKey: 'UjNRNeGyyN5K6z3CwCUYv9G4rD5EzLK5',
    priceMonthly: 'price_1JC8TRHXdXGlciB8FSQ4rB8Z',
    priceYearly: 'price_1JC8TRHXdXGlciB8ADvdmZqF',
    sessionTimeOut: 60, /*in minutes*/
    googlePlaceApi: 'AIzaSyAOYCFnbRzjmQJIzXlvOmpQ_TRhl1uPXD8',
    gtmCode: 'G-G5R1EKRX6J',
};

//stage
const stageData = {
    baseUrl: 'https://staging.soleapp.com.au/staging1_1/api/',
    stripeKey: 'pk_test_51IhkoFHXdXGlciB8Si1nWlxXANvUuOyqdIkZJCgk8MDtB47EcGbnaWg2pot4vW7G85gsF9XvCOmU07j1O6t2vpcL00rnnxcd1Z',
    apiKey: 'UjNRNeGyyN5K6z3CwCUYv9G4rD5EzLK5',
    priceMonthly: 'price_1JC8TRHXdXGlciB8FSQ4rB8Z',
    priceYearly: 'price_1JC8TRHXdXGlciB8ADvdmZqF',
    sessionTimeOut: 60, /*in minutes*/
    googlePlaceApi: 'AIzaSyAOYCFnbRzjmQJIzXlvOmpQ_TRhl1uPXD8',
    gtmCode: 'G-G5R1EKRX6J',
};

//live
const liveData = {
    baseUrl: 'https://staging.soleapp.com.au/staging1_1/api/',
    stripeKey: 'pk_test_51IhkoFHXdXGlciB8Si1nWlxXANvUuOyqdIkZJCgk8MDtB47EcGbnaWg2pot4vW7G85gsF9XvCOmU07j1O6t2vpcL00rnnxcd1Z',
    apiKey: 'UjNRNeGyyN5K6z3CwCUYv9G4rD5EzLK5',
    priceMonthly: 'price_1JC8TRHXdXGlciB8FSQ4rB8Z',
    priceYearly: 'price_1JC8TRHXdXGlciB8ADvdmZqF',
    sessionTimeOut: 60, /*in minutes*/
    googlePlaceApi: 'AIzaSyAOYCFnbRzjmQJIzXlvOmpQ_TRhl1uPXD8',
    gtmCode: 'G-G5R1EKRX6J',
};

const serverData = (env === 'live') ? liveData : (env === 'staging') ? stageData : localData;

const config = {
    baseUrl: serverData.baseUrl,
    stripeKey: serverData.stripeKey,
    apiKey: serverData.apiKey,
    priceMonthly: serverData.priceMonthly,
    priceYearly: serverData.priceYearly,
    sessionTimeOut: serverData.sessionTimeOut, /*in minutes*/
    deviceInfo: {
        platform: 'WEBP',
        deviceId: (typeof window !== "undefined") ? localStorage.key('deviceInfo.deviceId') : null,
    },
    gtmCode: serverData.gtmCode,
    gtmId: "GTM-PDK3H3T",

    paytermList: {
        7: '7 Days',
        14: '14 Days',
        30: '30 Days',
        60: '60 Days',
        90: '90 Days',
        120: '120 Days'
    },
    googlePlaceApi: serverData.googlePlaceApi,

    personalCategoryId: 133,
    incomeCategoryId: 4,
    expanseCategoryId: 1,
    assetCategoryId: 6,
    liabilitiesCategoryId: 2,
    capitalCategoryId: 3,
};

export default config;
