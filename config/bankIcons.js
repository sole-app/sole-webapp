const bankIcons = {
    adcu: '/images/banks/adcu.png',
    anz: '/images/banks/anz.png',
    stgeorge: '/images/banks/stgeorge.png',
    westpac: '/images/banks/westpac.png',
    cba: '/images/banks/cba.svg',
    nab: '/images/banks/nab.png',

    default: '/images/banks/default.png',
};

export default bankIcons;