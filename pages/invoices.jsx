import Head from "next/head";
import { useState, useEffect } from "react";
import TopBar from "../components/Dashboard/TopBar";
import SideDrawer from "../components/Dashboard/SideBar";
import InvoiceAnalytics from "../components/Invoices/InvoiceAnalytic";
import TopEarners from "../components/Invoices/TopEarners";
import InvoiceList from "../components/Invoices/InvoiceList";
import AddInvoice from "../components/Invoices/AddInvoice";
import EmptyView from "../components/Invoices/EmptyView";
import { IoIosAddCircleOutline } from "react-icons/io";
import Snackbar, { SNACKBAR_TYPE } from "../components/Layout/Snackbar";
import cookies from "next-cookies";
import InvoiceInfo from "../components/Invoices/InvoiceInfo";
import InvoiceFollowUp from "../components/Invoices/InvoiceFollowUp";
import { List } from "../services/api/invoice.services";
import AlertDialog from "../components/AlertDialog";
import CircularProgress from "@material-ui/core/CircularProgress";
import store from "../store/store";
import { listAccounts } from "../services/api/bankAccount.services";
import { useRouter } from "next/router";
import { snackbarClose } from "../reducers/slices/snackbarSlice";
import TokenExpiredDialog from "../components/TokenExpiredDialog";

export async function getServerSideProps(context) {
  var data = cookies(context);

  if (!data.token && !data.userData) {
    return {
      redirect: {
        permanent: false,
        destination: "/",
      },
    };
  } else {
    return {
      props: data,
    };
  }
}

export default function Invoices() {
  const router = useRouter();
  let storeData = store.store.getState();
  const [loader, setLoader] = useState(false);
  const [isOpen, setIsOpen] = useState(false);
  const [isListUpdate, setIsListUpdate] = useState(true);
  const [isInfoOpen, setIsInfoOpen] = useState(false);
  const [isFollowUp, setIsFollowUp] = useState(false);
  const [updateId, setUpdateId] = useState("");
  const [isDuplicate, setIsDuplicate] = useState(false);
  const [isEmptyView, setEmptyView] = useState(true);
  const [openAlert, setOpenAlert] = useState(false);
  const [notify, setNotify] = useState("");
  const [checkAddInvoice, setCheckAddInvoice] = useState(true);
  const [refreshSummary, setRefreshSummary] = useState(false);

  useEffect(() => {
    store.store.dispatch(snackbarClose());
    getInvoiceList();
  }, []);

  useEffect(async () => {
    if (
      typeof router.query !== "undefined" &&
      typeof router.query.add !== "undefined" &&
      router.query.add
    ) {
      await canAddInvoice();
    }
  }, [router.query]);

  const getInvoiceList = async () => {
    setLoader(true);
    const formData = "?page_size=1";

    const result = await List(formData);
    if (result.success) {
      if (Object.keys(result.data.invoice_details).length > 0) {
        setEmptyView(false);
        setCheckAddInvoice(false);
      }
    }
    setLoader(false);
  };

  const canAddInvoice = async () => {
    if (checkAddInvoice) {
      // check bank connection and account information
      // FIXME: Need to check below parameters added
      let canAdd = false;
      // if ((typeof storeData.user_details.bank !== 'undefined' && typeof storeData.user_details.bank.account_holder !== 'undefined' && storeData.user_details.bank.account_holder !== '')) {
      // 	canAdd = true;
      // }

      if (
        typeof storeData.user_details.bank !== "undefined" &&
        typeof storeData.user_details.bank.account_holder !== "undefined" &&
        storeData.user_details.bank.account_holder !== "" &&
        storeData.user_details.business_name &&
        storeData.user_details.email &&
        storeData.user_details.full_name &&
        storeData.user_details.mobile_number &&
        (storeData.user_details.address.address_line1 ||
          storeData.user_details.address.address_line2 ||
          storeData.user_details.address.city ||
          storeData.user_details.address.country ||
          storeData.user_details.address.postcode ||
          storeData.user_details.address.state ||
          storeData.user_details.address.suburb)
      ) {
        canAdd = true;
      }

      if (!canAdd) {
        const result = await listAccounts();
        if (result.success) {
          if (
            typeof result.data.banks_accounts !== "undefined" &&
            result.data.banks_accounts.length > 0
          ) {
            canAdd = false;
          }
        }
      }

      if (canAdd) {
        setUpdateId("");
        setIsOpen(!isOpen);
      } else {
        setOpenAlert(true);
      }
    } else {
      setUpdateId("");
      setIsOpen(!isOpen);
    }
  };

  const closeNotify = (e) => {
    e.stopPropagation();
    e.preventDefault();
    setNotify("");
    setIsFollowUp(false);
  };

  return (
    <div className="h-screen flex overflow-hidden bg-gray-100">
      <Snackbar type={SNACKBAR_TYPE} />

      <Head>
        <title> Invoices | Sole Accounting made easy</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      {/* For sideBar */}
      <SideDrawer title="invoices" key={"invoices"} />

      <TokenExpiredDialog />

      <main className="flex-1 relative invoices pb-8 z-0 overflow-y-auto">
        {/* Top Bar */}
        <TopBar title="Invoices" />

        {/* Page header */}
        <div className="mt-8">
          <div className="mx-auto  px-4 sm:px-6 lg:px-8 md:flex md:items-center md:justify-between">
            <div>
              <h2 className="text-3xl font-extrabold tracking-wide text-gray-900 sm:text-4xl">
                Invoices
              </h2>
              {/* <p className="mt-1 font-normal text-sm text-gray-500">
								List of all your Invoices.
							</p> */}
            </div>
            <div className="mt-8 flex md:mt-0 lg:flex-shrink-0">
              <div className="inline-flex rounded-md">
                <button
                  className="inline-flex items-center justify-center uppercase tracking-wide pr-5 pl-2 py-1 border border-transparent text-sm  rounded-full text-white bg-brand-blue hover:bg-brand-darkblue"
                  onClick={() => {
                    canAddInvoice();
                  }}
                >
                  <IoIosAddCircleOutline className="w-9 h-9 pr-2 pl-0" /> New
                  Invoice
                </button>
              </div>
            </div>
          </div>
        </div>

        {loader ? (
          <div className="box-center">
            <CircularProgress />
          </div>
        ) : isEmptyView ? (
          <>
            <EmptyView />
          </>
        ) : (
          <>
            <div className="block xl:flex justify-between flex-1 px-4 sm:px-6 lg:px-8">
              <InvoiceAnalytics refreshSummary={refreshSummary} />
              <TopEarners from="invoice" refreshSummary={refreshSummary} />
            </div>

            <div className="block  px-4 sm:px-6 lg:px-8">
              <InvoiceList
                isListUpdate={isListUpdate}
                refreshSummary={refreshSummary}
                setIsListUpdate={setIsListUpdate}
                setIsOpen={setIsOpen}
                setUpdateId={setUpdateId}
                setIsInfoOpen={setIsInfoOpen}
                setEmptyView={setEmptyView}
                setIsDuplicate={setIsDuplicate}
                setRefreshSummary={setRefreshSummary}
              />
            </div>
          </>
        )}

        {/* Slider: Add Invoice	 */}
        <AddInvoice
          isOpen={isOpen}
          refreshSummary={refreshSummary}
          setIsListUpdate={setIsListUpdate}
          setIsOpen={setIsOpen}
          updateId={updateId}
          setEmptyView={setEmptyView}
          isDuplicate={isDuplicate}
          setIsDuplicate={setIsDuplicate}
          setIsInfoOpen={setIsInfoOpen}
          setUpdateId={setUpdateId}
          setIsFollowUp={setIsFollowUp}
          setNotify={setNotify}
          setRefreshSummary={setRefreshSummary}
        />

        <InvoiceInfo
          isInfoOpen={isInfoOpen}
          setIsInfoOpen={setIsInfoOpen}
          updateId={updateId}
        />

        <InvoiceFollowUp
          isFollowUp={isFollowUp}
          closeNotify={closeNotify}
          setIsFollowUp={setIsFollowUp}
          notify={notify}
        />

        <AlertDialog
          id="alert-menu"
          keepMounted
          open={openAlert}
          onClose={setOpenAlert}
          missingInfo={true}
          title={"Missing Information"}
          message={
            "We are missing important information for your invoice. Please update your details."
          }
        />
      </main>
    </div>
  );
}
