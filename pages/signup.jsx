import cookies from 'next-cookies';
import AuthContent from '../components/Auth/AuthContent';

export async function getServerSideProps(context) {
	var data = cookies(context);
	if (data.token && data.userData) {
		return {
			redirect: {
				permanent: false,
				destination: "/dashboard",
			},
		}
	}
	else {
		return {
			props: data,
		}
	}
}

export default function Signup() {
	return (
		<AuthContent type="signup" />
	);
}