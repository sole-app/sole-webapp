import Head from 'next/head';
import { useState, useEffect } from 'react'
import TopBar from '../components/Dashboard/TopBar';
import SideDrawer from '../components/Dashboard/SideBar';
import ClientList from '../components/Clients/ClientList';
import ClientInfo from '../components/Clients/ClientInfo';
import AddClient from '../components/Clients/AddClient';
import { IoIosAddCircleOutline } from "react-icons/io";
import Snackbar, { SNACKBAR_TYPE } from "../components/Layout/Snackbar";
import cookies from 'next-cookies';
import { useRouter } from 'next/router';
import { snackbarClose } from '../reducers/slices/snackbarSlice';
import store from '../store/store';
import TokenExpiredDialog from '../components/TokenExpiredDialog';

export async function getServerSideProps(context) {
	var data = cookies(context);

	if (!data.token && !data.userData) {
		return {
			redirect: {
				permanent: false,
				destination: "/",
			},
		}
	}
	else {
		return {
			props: data,
		}
	}
}

export default function Clients() {
	const router = useRouter();
	const [isOpen, setIsOpen] = useState(false);
	const [isListUpdate, setIsListUpdate] = useState(true);
	const [isInfoOpen, setIsInfoOpen] = useState(false);
	const [updateId, setUpdateId] = useState('');
	const [isClient, setIsClient] = useState(0);

	useEffect(() => {
		store.store.dispatch(snackbarClose());
		if (typeof router.query !== 'undefined' && typeof router.query.add !== 'undefined' && router.query.add) {
			setIsOpen(true);
		}
	}, [router.query]);

	return (
		<div className="h-screen flex overflow-hidden bg-gray-100">
			<Snackbar type={SNACKBAR_TYPE}></Snackbar>

			<Head>
				<title> Clients | Sole Accounting made easy</title>
				<link rel='icon' href='/favicon.ico' />
			</Head>

			{/* For sideBar */}
			<SideDrawer title="contacts" />

			<TokenExpiredDialog />

			<main className="flex-1 relative invoices pb-8 z-0 overflow-y-auto">
				{/* Top Bar */}
				<TopBar title="Contacts" />

				{/* Page header */}
				<div className="mt-8">
					<div className="mx-auto  px-4 sm:px-6 lg:px-8 md:flex md:items-center md:justify-between">
						<div>
							<h2 className="text-3xl font-extrabold tracking-wide text-gray-900 sm:text-4xl">
								Contacts
							</h2>
							{/* <p className="mt-1 font-normal text-sm text-gray-500">
								List of all of all your clients and suppliers.
							</p> */}
						</div>

						<div className="mt-8 flex md:mt-0 lg:flex-shrink-0">
							<div className="inline-flex rounded-md">
								<button className="inline-flex items-center justify-center uppercase tracking-wide pr-5 pl-2 py-1 border border-transparent text-sm  rounded-full text-white bg-brand-blue hover:bg-brand-darkblue" onClick={() => { setUpdateId(''); setIsOpen(!isOpen) }}>
									<IoIosAddCircleOutline className="w-9 h-9 pr-2 pl-0" /> New Contact
								</button>
							</div>
						</div>
					</div>
				</div>

				{/* <EmptyView /> */}

				<div className="block  px-4 sm:px-6 lg:px-8">
					<ClientList
						isListUpdate={isListUpdate}
						setIsListUpdate={setIsListUpdate}
						setIsOpen={setIsOpen}
						setUpdateId={setUpdateId}
						setIsInfoOpen={setIsInfoOpen}
						setIsClient={setIsClient}
					/>
				</div>

				{/* Sideover for Add/Edit Client */}
				<AddClient
					isOpen={isOpen}
					setIsListUpdate={setIsListUpdate}
					setIsOpen={setIsOpen}
					updateId={updateId}
					isClient={isClient}
				/>

				<ClientInfo
					isInfoOpen={isInfoOpen}
					setIsInfoOpen={setIsInfoOpen}
					updateId={updateId}
					isClient={isClient}
				/>

			</main>

		</div>
	);
}
