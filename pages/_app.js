import '../styles/globals.scss';
import 'tailwindcss/tailwind.css';
import store from '../store/store';
import { Provider } from "react-redux";
import config from "../config/config";
import { PersistGate } from "redux-persist/lib/integration/react";
import { RouteGuard } from '../components/RouteGuard';
import Head from 'next/head';

const apiKEY = "https://maps.googleapis.com/maps/api/js?key=" + config.googlePlaceApi + "&libraries=places";
const gaUrl = "https://www.googletagmanager.com/gtag/js?id=" + config.gtmCode;
function MyApp({ Component, pageProps }) {
  return (
    <>
      <script async src={gaUrl}></script>
      <script
        dangerouslySetInnerHTML={{
          __html: `window.dataLayer =window.dataLayer || [];
                function gtag(){dataLayer.push(arguments)}
                gtag('js', new Date());
                gtag('config', '${config.gtmCode}');`
        }}
      />

      {config.gtmId !== "" &&
        <>
          <Head>
            <script
              type="text/javascript"
              async
              dangerouslySetInnerHTML={{
                __html: `
          (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
          new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
          j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
          'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
          })(window,document,'script','dataLayer','${config.gtmId}');`,
              }}
            />
          </Head>

          {/* Google Tag Manager (noscript) */}
          <noscript>
            <iframe
              title="Google Tag Manager"
              src={`https://www.googletagmanager.com/ns.html?id=${config.gtmId}`}
              height="0"
              width="0"
              style={{ display: "none", visibility: "hidden" }}
            ></iframe>
          </noscript>
          {/* End Google Tag Manager (noscript) */}
        </>
      }

      <script src={apiKEY} />
      <Provider store={store.store} session={pageProps.session} >
        <PersistGate persistor={store.persistor}>
          <RouteGuard>
            <Component {...pageProps} />
          </RouteGuard>
        </PersistGate>
      </Provider>
    </>
  );
}

export default MyApp
