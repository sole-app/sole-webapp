import Head from "next/head";
import { useState, useEffect } from "react";
import TopBar from "../components/Dashboard/TopBar";
import SideDrawer from "../components/Dashboard/SideBar";
import ConnectAccount from "../components/Banking/ConnectAccount";
import AddAccount from "../components/Banking/AddAccount";
import AccountSummary from "../components/Banking/AccountSummary";
import { IoIosAddCircleOutline } from "react-icons/io";
import BankTransactions from "../components/Banking/Transactions/BankTransactions";
import { getSpecificCategories } from "../services/api/accountCategory.services";
import cookies from "next-cookies";
import {
  listAccounts as listBankAccounts,
  deleteAccount as deleteBankAccount,
  makePrimary as makeBankPrimary,
} from "../services/api/bankAccount.services";
import { setBank } from "../services/cookies.services";
import ConfirmationDialog from "../components/ConfirmationDialog";
import ManageAccounts from "../components/Banking/ManageAccounts";
import config from "../config/config";
import Snackbar, { SNACKBAR_TYPE } from "../components/Layout/Snackbar";
import store from "../store/store";
import { snackbarClose } from "../reducers/slices/snackbarSlice";
import { setPrimaryAccount } from "../store/actions";
import TokenExpiredDialog from "../components/TokenExpiredDialog";
import CircularProgress from "@material-ui/core/CircularProgress";

export async function getServerSideProps(context) {
  let data = cookies(context);

  if (!data.token && !data.userData) {
    return {
      redirect: {
        permanent: false,
        destination: "/",
      },
    };
  } else {
    return {
      props: data,
    };
  }
}

export default function BankAccounts(user) {
  let storeData = store.store.getState();

  const [open, setOpen] = useState(false);
  const [openSlider, setOpenSlider] = useState(false);
  const [displayAddToBank, setDisplayAddToBank] = useState(true);
  const [accounts, setAccounts] = useState([]);
  const [loader, setLoader] = useState(false);
  const [bankAccountLoader, setBankAccountLoader] = useState(false);
  const [confirmation, setConfirmation] = useState(false);
  const [deleteId, setDeleteId] = useState("");
  const [accountAdded, setAccountAdded] = useState(false);
  const [refresh, setRefresh] = useState(false);
  const [categories, setCategories] = useState({
    income: [],
    expanse: [],
    expanseOnly: [],
  });
  const [refreshTransaction, setRefreshTransaction] = useState(false);
  const [matchUnmatchTransactions, setMatchUnmatchTransactions] = useState({});
  const [isReAuth, setIsReAuth] = useState(false);
  const [account, setAccount] = useState({});

  const listAccounts = async (fromDelete = false) => {
    setLoader(true);
    setBankAccountLoader(true);
    const result = await listBankAccounts();
    if (result.success) {
      let banks = [];
      if (
        typeof result.data.banks_accounts !== "undefined" &&
        result.data.banks_accounts.length > 0
      ) {
        for (let i = 0; i < result.data.banks_accounts.length; i++) {
          if (result.data.banks_accounts[i].is_primary > 0) {
            if (
              result.data.banks_accounts.length > 1 &&
              typeof result.data.banks_accounts[i].bank_connection
                .bank_connection_id !== "undefined"
            ) {
              store.store.dispatch(
                setPrimaryAccount(
                  result.data.banks_accounts[i].bank_connection
                    .bank_connection_id
                )
              );
            }
            banks.push(result.data.banks_accounts[i]);
            setBank(result.data.banks_accounts[i]);
          } else {
            banks.push(result.data.banks_accounts[i]);
          }
        }
        setDisplayAddToBank(false);
      } else {
        if (fromDelete) {
          setBank({});
          setDisplayAddToBank(true);
          setAccountAdded(false);
        }
      }
      setAccounts(banks);
    } else {
      setAccounts([]);
      setBank({});
      setDisplayAddToBank(true);
      setAccountAdded(false);
    }
    setLoader(false);
    setBankAccountLoader(false);
  };

  const deleteAccount = async (id) => {
    if (id) {
      const formData = new FormData();
      formData.append("id", id);

      const result = await deleteBankAccount(formData);
      if (result.success) {
        if (accounts.length === 1) {
          setBank({});
          setDisplayAddToBank(true);
          setAccountAdded(false);
          setAccounts([]);
        } else {
          await listAccounts(true);
          setRefresh(!refresh);
        }
        setConfirmation(false);
      } else {
        setConfirmation(false);
      }
      setDeleteId("");
    } else {
      setDeleteId("");
      setConfirmation(false);
    }
  };

  const makePrimary = async (id) => {
    const formData = new FormData();
    formData.append("bank_account_id", id);

    const result = await makeBankPrimary(formData);
    if (result.success) {
      await listAccounts();
    }
  };

  const confirmDelete = async (id) => {
    setDeleteId(id);
    setConfirmation(true);
  };

  const getAccountCategories = async () => {
    if (categories.expanse.length === 0 && categories.income.length === 0) {
      const income = await getSpecificCategories([config.incomeCategoryId]);
      const expanse = await getSpecificCategories([
        config.expanseCategoryId,
        config.assetCategoryId,
      ]);
      const expanseOnly = await getSpecificCategories([
        config.expanseCategoryId,
      ]);
      const assetOnly = await getSpecificCategories([config.assetCategoryId]);
      setCategories({
        income: income,
        expanse: expanse,
        expanseOnly: expanseOnly,
        assetOnly: assetOnly,
      });
    }
  };

  const openCloseReAuth = async (account = {}) => {
    if (Object.keys(account).length > 0) {
      setAccount(account);
      setIsReAuth(true);
      setOpenSlider(true);
    } else {
      setAccount({});
      setIsReAuth(false);
      setOpenSlider(false);
    }
  };

  useEffect(() => {
    if (
      (typeof storeData.user_details.bank !== "undefined" &&
        typeof storeData.user_details.bank.account_holder !== "undefined" &&
        storeData.user_details.bank.account_holder !== "") ||
      accountAdded
    ) {
      //setDisplayAddToBank(false);
      listAccounts();
      setRefreshTransaction(!refreshTransaction);
      getAccountCategories();
    }
  }, [accountAdded, storeData.user_details, refresh]);

  useEffect(() => {
    store.store.dispatch(snackbarClose());
  }, []);

  return (
    <div className="h-screen flex overflow-hidden bg-gray-100">
      <Head>
        <title> Bank Transactions | Sole Accounting made easy</title>
        <link rel="icon" href="/favicon.ico" />
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link
          href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;1,300&display=swap"
          rel="stylesheet"
        />
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link
          href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;200;300;400;500;600&display=swap"
          rel="stylesheet"
        />
      </Head>

      <Snackbar type={SNACKBAR_TYPE}></Snackbar>

      {/* For sideBar */}
      <SideDrawer title="bankaccounts" />

      <TokenExpiredDialog />

      <main className="flex-1 relative pb-8 z-0 overflow-x-hidden overflow-y-auto">
        {/* Top Bar */}
        <TopBar title="Bank Transactions" />

        {/* Page header */}
        <div className="mt-8">
          <div className="mx-auto  px-4 sm:px-6 lg:px-8 md:flex md:items-center md:justify-between">
            <div>
              <h2 className="text-3xl font-extrabold tracking-wide text-gray-900 sm:text-4xl">
                Bank Transactions
              </h2>
            </div>

            {displayAddToBank ? (
              ""
            ) : (
              <div className="mt-8 flex items-center md:mt-0 lg:flex-shrink-0">
                <div className="inline-flex rounded-md">
                  <button
                    className="inline-flex items-center justify-center uppercase tracking-wide pr-5 pl-2 py-1 border border-transparent text-sm  rounded-full text-white bg-brand-blue hover:bg-brand-darkblue"
                    onClick={() => setOpen(true)}
                  >
                    <IoIosAddCircleOutline className="w-9 h-9 pr-2 pl-0" />
                    Manange Bank Accounts
                  </button>
                </div>
              </div>
            )}
          </div>
        </div>

        {bankAccountLoader ? (
          <div className="box-center">
            <CircularProgress />
          </div>
        ) : (
          <>
            {displayAddToBank ? (
              <div className="space-y-8 px-7 items-start grid grid-cols-1 gap-y-2 sm:pt-0 sm:gap-x-8">
                <ConnectAccount setOpenSlider={setOpenSlider} />
              </div>
            ) : (
              <div className="space-y-8 px-7 items-start">
                <AccountSummary
                  loader={loader}
                  accounts={accounts}
                  matchUnmatchTransactions={matchUnmatchTransactions}
                  openCloseReAuth={openCloseReAuth}
                />

                <BankTransactions
                  refresh={refreshTransaction}
                  categories={categories}
                  accounts={accounts}
                  setMatchUnmatchTransactions={setMatchUnmatchTransactions}
                />
              </div>
            )}
          </>
        )}

        <AddAccount
          account={account}
          isReAuth={isReAuth}
          openSlider={openSlider}
          refresh={refresh}
          setOpenSlider={setOpenSlider}
          setAccountAdded={setAccountAdded}
          setRefresh={setRefresh}
          openCloseReAuth={openCloseReAuth}
          listAccounts={listAccounts}
        />

        <ConfirmationDialog
          id="ringtone-menu"
          title="Remove Bank Connection?"
          message="All matched transactions will be undone if you continue. Your invoices and expenses will still be in the app."
          keepMounted
          open={confirmation}
          onClose={deleteAccount}
          value={deleteId}
        />

        <ManageAccounts
          loader={loader}
          accounts={accounts}
          setOpenSlider={setOpenSlider}
          makePrimary={makePrimary}
          confirmDelete={confirmDelete}
          open={open}
          setOpen={setOpen}
          openCloseReAuth={openCloseReAuth}
        />

        {/* <Feedback /> */}
      </main>
    </div>
  );
}
