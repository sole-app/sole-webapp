import Head from 'next/head';
import TopBar from '../components/Dashboard/TopBar';
import SideDrawer from '../components/Dashboard/SideBar';
import SolePartners from '../components/Partners/SolePartners';
import TokenExpiredDialog from '../components/TokenExpiredDialog';

export default function Partners() {

	return (
		<div className="h-screen flex overflow-hidden bg-gray-100">
			<Head>
				<title> Partners | Sole Accounting made easy</title>
				<link rel='icon' href='/favicon.ico' />
			</Head>

			{/* For sideBar */}
			<SideDrawer title="partners" />

			<TokenExpiredDialog />

			<main className="flex-1 relative invoices pb-8 z-0 overflow-y-auto">
				{/* Top Bar */}
				<TopBar title="Partners" />

				{/* Page header */}
				<div className="mt-8">
					<div className="mx-auto  px-4 sm:px-6 lg:px-8 md:flex md:items-center md:justify-between">
						<div>
							<h2 className="text-3xl font-extrabold tracking-wide text-gray-900 sm:text-4xl">
								Partner Offers
							</h2>
						</div>
					</div>
				</div>

				{/* Page header */}
				<div className="px-7">
					<SolePartners />
				</div>
			</main>

		</div>
	);
}
