import Head from 'next/head';
import { useRouter } from 'next/router';
import TopBar from '../../../components/Dashboard/TopBar';
import SideDrawer from '../../../components/Dashboard/SideBar';
import ReportSidebar from '../../../components/Reports/ReportSidebar';
import cookies from 'next-cookies';
import Snackbar, { SNACKBAR_TYPE } from "../../../components/Layout/Snackbar";

export async function getServerSideProps(context) {
	var data = cookies(context);

	if (!data.token && !data.userData) {
		return {
			redirect: {
				permanent: false,
				destination: "/",
			},
		}
	}
	else {
		return {
			props: data,
		}
	}
}

export default function Reports() {

	const router = useRouter();
	const { id } = router.query;

	return (
		<div className="h-screen flex overflow-hidden bg-gray-100">
			<Snackbar type={SNACKBAR_TYPE}></Snackbar>
			<Head>
				<title> Reports | Sole Accounting made easy</title>
				<link rel='icon' href='/favicon.ico' />
			</Head>

			{/* For sideBar */}
			<SideDrawer title="reports" />

			<main className="flex-1 relative invoices  z-0 overflow-y-auto">
				{/* Top Bar */}
				<TopBar title="Reports" />

				<ReportSidebar reportType={id} />
			</main>

		</div>
	);
}
