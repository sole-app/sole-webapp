import Stripe from "stripe";
//const stripe = new Stripe("sk_test_51IhkoFHXdXGlciB8s025cL2OL7WOMpbGZ5Boj2ew02PiXtgvkSoOb6KcigPA8ATnfH0J2cgOI4glH6YCGoIWnIZ700Qe159C7d");
const stripe = new Stripe("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");

export default async (req, res) => {

  if (req.method === "POST") {

    const {
      paymentMethodId,
      email,
      phone,
      coupon_code,
      planId,
      trialDays
    } = JSON.parse(req.body);

    // Sandbox Env
    // $14.99 / Monthly Plan: price_1IwoeLHXdXGlciB8FrDKp89L
    // $149.99 / Yearly Plan: price_1IwofnHXdXGlciB8O7SlUNZ9

    // Production Env
    // $14.99 / Monthly Plan: price_1JC8TRHXdXGlciB8FSQ4rB8Z
    // $149.99 / Yearly Plan: price_1JC8TRHXdXGlciB8ADvdmZqF

    //const MONTHLY_PLAN_PRICE_ID = "price_1IwoeLHXdXGlciB8FrDKp89L";
    //const YEARLY_PLAN_PRICE_ID = "price_1IwofnHXdXGlciB8O7SlUNZ9";

    const MONTHLY_PLAN_PRICE_ID = "price_1JC8TRHXdXGlciB8FSQ4rB8Z";
    const YEARLY_PLAN_PRICE_ID = "price_1JC8TRHXdXGlciB8ADvdmZqF";

    var plan = planId === 1 ? MONTHLY_PLAN_PRICE_ID : YEARLY_PLAN_PRICE_ID;

    try {
      const customer = await stripe.customers.create({
        payment_method: paymentMethodId,
        phone: phone,
        email: email,
        invoice_settings: {
          default_payment_method: paymentMethodId
        }
      });

      const subscription = await stripe.subscriptions.create({
        customer: customer.id,
        items: [{
          plan: plan
        }],
        expand: ["latest_invoice.payment_intent"],
        coupon: coupon_code,
        trial_period_days: (trialDays > 0) ? trialDays : 0,
      });

      res.status(200).json(subscription);

    } catch (e) {
      res.status(500).json({
        statusCode: 500,
        message: e.message
      });
    }
  } else {
    res.setHeader("Allow", "POST");
    res.status(405).end("Method Not Allowed");
  }
};