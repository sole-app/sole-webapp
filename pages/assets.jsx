import Head from "next/head";
import { useState, useEffect } from "react";
import TopBar from "../components/Dashboard/TopBar";
import SideDrawer from "../components/Dashboard/SideBar";
import AssetsList from "../components/Assets/AssetList";
import AddAsset from "../components/Assets/AddAssets";
import EmptyView from "../components/Assets/EmptyView";
import { IoIosAddCircleOutline } from "react-icons/io";
import Snackbar, { SNACKBAR_TYPE } from "../components/Layout/Snackbar";
import cookies from "next-cookies";
import config from "../config/config";
import { getSpecificCategories } from "../services/api/accountCategory.services";
import CircularProgress from "@material-ui/core/CircularProgress";
import { List } from "../services/api/asset.services";
import { useRouter } from "next/router";
import { snackbarClose } from "../reducers/slices/snackbarSlice";
import store from "../store/store";
import TokenExpiredDialog from "../components/TokenExpiredDialog";

export async function getServerSideProps(context) {
  var data = cookies(context);

  if (!data.token && !data.userData) {
    return {
      redirect: {
        permanent: false,
        destination: "/",
      },
    };
  } else {
    return {
      props: data,
    };
  }
}

export default function Assets(props) {
  const router = useRouter();
  const [loader, setLoader] = useState(false);
  const [isOpen, setIsOpen] = useState(false);
  const [isListUpdate, setIsListUpdate] = useState(true);
  const [isInfo, setIsInfo] = useState(false);
  const [updateId, setUpdateId] = useState("");
  const [accountCategories, setAccountCategories] = useState([]);
  const [isEmptyView, setEmptyView] = useState(true);

  const getAccountCategories = async () => {
    if (accountCategories.length === 0) {
      const categories = await getSpecificCategories([config.assetCategoryId]);
      setAccountCategories(categories);
    }
  };

  // GET ALL LIST
  const getList = async () => {
    const object = "?page_size=1";

    const result = await List(object);
    if (result.success) {
      if (Object.keys(result.data.asset_details).length > 0) {
        setEmptyView(false);
      }
    }
    setLoader(false);
  };

  useEffect(() => {
    store.store.dispatch(snackbarClose());
    getAccountCategories();
    if (props.isListUpdate) {
      setLoader(true);
      getList();
    }
  }, [props.isListUpdate]);

  useEffect(async () => {
    if (
      typeof router.query !== "undefined" &&
      typeof router.query.add !== "undefined" &&
      router.query.add
    ) {
      await getAccountCategories();
      setIsOpen(true);
    }
  }, [router.query]);

  // First time view load
  useEffect(() => {
    setLoader(true);
    getList();
  }, []);

  return (
    <div className="h-screen flex overflow-hidden bg-gray-100">
      <Snackbar type={SNACKBAR_TYPE} />

      <Head>
        <title> Assets | Sole Accounting made easy</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      {/* For sideBar */}
      <SideDrawer title="assets" />

      <TokenExpiredDialog />

      <main className="flex-1 relative invoices pb-8 z-0 overflow-y-auto">
        {/* Top Bar */}
        <TopBar title="Assets" />

        {/* Page header */}
        <div className="mt-8">
          <div className="mx-auto  px-4 sm:px-6 lg:px-8 md:flex md:items-center md:justify-between">
            <div>
              <h2 className="text-3xl font-extrabold tracking-wide text-gray-900 sm:text-4xl">
                Assets
              </h2>
            </div>

            <div className="mt-8 flex md:mt-0 lg:flex-shrink-0">
              <div className="inline-flex rounded-md">
                <button
                  className="inline-flex items-center justify-center uppercase tracking-wide pr-5 pl-2 py-1 border border-transparent text-sm  rounded-full text-white bg-brand-blue hover:bg-brand-darkblue"
                  onClick={() => {
                    setUpdateId("");
                    setIsOpen(!isOpen);
                  }}
                >
                  <IoIosAddCircleOutline className="w-9 h-9 pr-2 pl-0" /> New
                  Asset
                </button>
              </div>
            </div>
          </div>
        </div>

        {loader ? (
          <div className="box-center">
            <CircularProgress />
          </div>
        ) : isEmptyView ? (
          <>
            <EmptyView />
          </>
        ) : (
          <>
            <div className="block  px-4 sm:px-6 lg:px-8">
              <AssetsList
                isListUpdate={isListUpdate}
                setIsListUpdate={setIsListUpdate}
                setIsOpen={setIsOpen}
                setUpdateId={setUpdateId}
                setIsInfo={setIsInfo}
                setEmptyView={setEmptyView}
                accountCategories={accountCategories}
              />
            </div>
          </>
        )}

        {/* Sideover for Add/Edit Quote */}
        <AddAsset
          accountCategories={accountCategories}
          isOpen={isOpen}
          setIsListUpdate={setIsListUpdate}
          setIsOpen={setIsOpen}
          updateId={updateId}
          setEmptyView={setEmptyView}
          isInfo={isInfo}
          setIsInfo={setIsInfo}
        />
      </main>
    </div>
  );
}
