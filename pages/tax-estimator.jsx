import { Fragment, useState, useEffect } from "react";
import Head from "next/head";
import TopBar from "../components/Dashboard/TopBar";
import SideDrawer from "../components/Dashboard/SideBar";
import Income from "../components/TaxCalculator/Income";
import CapitalGain from "../components/TaxCalculator/CapitalGain";
import SuperAnnuation from "../components/TaxCalculator/SuperAnnuation";
import Medicare from "../components/TaxCalculator/Medicare";
import Result from "../components/TaxCalculator/Result";
import { Listbox, Transition } from "@headlessui/react";
import { ChevronRightIcon, ChevronLeftIcon } from "@heroicons/react/outline";
import { CheckIcon, SelectorIcon } from "@heroicons/react/solid";
import cookies from "next-cookies";
import Snackbar, { SNACKBAR_TYPE } from "../components/Layout/Snackbar";
import {
  getFinancialYears,
  getTaxCalculation,
  calculateTax as calculateTaxEstimator,
} from "../services/api/taxCalculator.services";
import store from "../store/store";
import { snackbarOpen } from "../reducers/slices/snackbarSlice";
import CircularProgress from "@mui/material/CircularProgress";
import { snackbarClose } from "../reducers/slices/snackbarSlice";
import TokenExpiredDialog from "../components/TokenExpiredDialog";
import AlertDialog from "../components/AlertDialog";

export async function getServerSideProps(context) {
  let data = cookies(context);

  if (!data.token && !data.userData) {
    return {
      redirect: {
        permanent: false,
        destination: "/",
      },
    };
  } else {
    return {
      props: data,
    };
  }
}

const steps = [
  {
    id: "01",
    name: "Income",
    description:
      "Enter your details below for this financial year to calculate your Taxable Income.",
    href: "#",
    status: "complete",
  },
  {
    id: "02",
    name: "Capital Gains",
    description: "Did you have any capital gains or losses this year?",
    href: "#",
    status: "current",
  },
  {
    id: "03",
    name: "Superannuation",
    description:
      "Did you make any after tax personal Super contributions this financial year?",
    href: "#",
    status: "upcoming",
  },
  {
    id: "04",
    name: "Medicare Levy",
    description: "Did you have a spouse this Financial Year?",
    href: "#",
    status: "upcoming",
  },
];

function classNames(...classes) {
  return classes.filter(Boolean).join("  ");
}

export default function TaxEstimator() {
  const [financialYear, setFinancialYear] = useState();
  const [yearList, setyearList] = useState([]);
  const [loader, setLoader] = useState(true);
  const [currentStep, setCurrentStep] = useState(0);
  const [formData, setFormData] = useState([]);
  const [taxData, setTaxData] = useState({});
  const [flag, setFlag] = useState(false);
  const [isSubmit, setIsSubmit] = useState(false);
  const [openAlert, setOpenAlert] = useState(false);
  const [msg, setMsg] = useState("");
  const [alertType, setAlertType] = useState("Error");

  const getList = async () => {
    setLoader(true);
    const form_data = new FormData();
    const result = await getFinancialYears(form_data);

    if (result.success) {
      let temp = [];
      result.data.financialYear.map((v) =>
        temp.push({ id: v.id, currentyear: v.start_year + "-" + v.end_year })
      );
      setyearList(temp);
      setFinancialYear(temp[temp.length - 1]);
      setFlag(!flag);
    }
  };

  const getTaxDetail = async () => {
    if (financialYear) {
      setLoader(true);
      const form_data = new FormData();
      form_data.append("financial_year_id", financialYear.id);
      const result = await getTaxCalculation(form_data);

      let g_data = result.data.taxCalculation;
      setTaxData(g_data);

      let temp = [];
      temp["revenue"] = g_data ? g_data.revenue.toFixed(2) : "";
      temp["expenses"] = g_data ? g_data.expenses.toFixed(2) : "";
      temp["additional_income"] = g_data
        ? g_data.additional_income.toFixed(2)
        : "";
      temp["deductions"] = g_data ? g_data.deductions.toFixed(2) : "";
      temp["goverment_benifits"] = g_data ? g_data.goverment_benifits : "";
      temp["financial_investment_loss"] = g_data
        ? g_data.financial_investment_loss
        : "";
      temp["rental_property_loss"] = g_data ? g_data.rental_property_loss : "";
      temp["child_support"] = g_data ? g_data.child_support : "";

      temp["capital_gain_tax"] = g_data
        ? g_data.capital_gain_tax === "true"
        : false;
      temp["asset_list"] = [];
      if (
        g_data &&
        typeof g_data.cg_assets !== "undefined" &&
        g_data.cg_assets.length > 0
      ) {
        for (let i = 0; i < g_data.cg_assets.length; i++) {
          temp["asset_list"].push({
            description: g_data.cg_assets[i].description,
            owned_12_months: g_data.cg_assets[i].owned_12_months === "true",
            purchase_price: g_data.cg_assets[i].purchase_price,
            sale_price: g_data.cg_assets[i].sale_price,
            profit_loss: parseFloat(g_data.cg_assets[i].profit_loss),
          });
        }
      } else {
        temp["asset_list"].push({
          description: "",
          owned_12_months: "",
          purchase_price: "",
          sale_price: "",
          profit_loss: "",
        });
      }

      temp["current_year_capital_gain"] = g_data
        ? g_data.current_year_capital_gain
        : 0;
      temp["previous_year_capital_loss"] = g_data
        ? g_data.previous_year_capital_loss
        : 0;
      temp["diffrence"] = g_data
        ? g_data.current_year_capital_gain - g_data.previous_year_capital_loss
        : 0;

      if (g_data && parseInt(g_data.superannuation) !== 0) {
        temp["has_superannuation"] = true;
        temp["superannuation"] = g_data.superannuation;
      } else {
        temp["has_superannuation"] = false;
        temp["superannuation"] = 0;
      }
      temp["taxable_income"] = g_data ? g_data.taxable_income : 0;

      temp["spouse_financial_year"] = g_data && g_data.spouse_income > 0;
      temp["spouse_income"] = g_data ? g_data.spouse_income : 0;

      temp["medical_levy_examption"] = true;
      if (g_data && g_data.medical_levy_examption.toString() === "false") {
        temp["medical_levy_examption"] = false;
      }
      if (temp["medical_levy_examption"]) {
        temp["has_private_insurance"] = false;
      } else {
        temp["has_private_insurance"] =
          g_data && g_data.has_private_insurance.toString() === "true";
      }
      setFormData(temp);
      setLoader(false);
      setCurrentStep(0);
    }
  };

  const calculateTax = async () => {
    setIsSubmit(true);
    const form_data = new FormData();
    form_data.append("financial_year_id", financialYear.id);
    form_data.append("revenue", formData["revenue"]);
    form_data.append("expenses", formData["expenses"]);
    form_data.append(
      "additional_income",
      formData["additional_income"] === "" ? 0 : formData["additional_income"]
    );
    form_data.append(
      "deductions",
      formData["deductions"] === "" ? 0 : formData["deductions"]
    );

    form_data.append("capital_gain_tax", formData["capital_gain_tax"]);
    if (formData["capital_gain_tax"].toString() === "true") {
      formData["asset_list"].map(function (v, k) {
        form_data.append("cg_assets[" + k + "][description]", v["description"]);
        form_data.append(
          "cg_assets[" + k + "][owned_12_months]",
          v["owned_12_months"] ? 1 : 0
        );
        form_data.append(
          "cg_assets[" + k + "][purchase_price]",
          v["purchase_price"]
        );
        form_data.append("cg_assets[" + k + "][sale_price]", v["sale_price"]);
      });
    }
    form_data.append(
      "current_year_capital_gain",
      formData["current_year_capital_gain"]
    );
    form_data.append(
      "previous_year_capital_loss",
      formData["previous_year_capital_loss"]
    );
    //form_data.append('diffrence', formData['diffrence']);

    //form_data.append('goverment_benifits', formData['goverment_benifits']);
    //form_data.append('financial_investment_loss', formData['financial_investment_loss']);
    //form_data.append('rental_property_loss', formData['rental_property_loss']);
    //form_data.append('child_support', formData['child_support']);
    form_data.append("medicare_levy", 0);

    if (formData["has_superannuation"]) {
      form_data.append("superannuation", formData["superannuation"]);
    } else {
      form_data.append("superannuation", 0);
    }
    form_data.append("taxable_income", formData["taxable_income"]);

    if (formData["spouse_financial_year"]) {
      form_data.append("spouse_income", formData["spouse_income"]);
    } else {
      form_data.append("spouse_income", 0);
    }
    form_data.append(
      "medical_levy_examption",
      formData["medical_levy_examption"]
    );
    form_data.append(
      "has_private_insurance",
      formData["has_private_insurance"]
    );

    const result = await calculateTaxEstimator(form_data);

    if (result.success) {
      setTaxData(result.data.taxCalculation);
      setIsSubmit(false);
      setAlertType("Success");
      setMsg(result.message);
      setOpenAlert(true);
    } else {
      setIsSubmit(false);
      setAlertType("Error");
      setMsg(result.message);
      setOpenAlert(true);
    }
  };

  useEffect(() => {
    store.store.dispatch(snackbarClose());
    getList();
  }, []);

  useEffect(() => {
    getTaxDetail();
  }, [financialYear]);

  const assignedVal = (key, val, k = "", sk = "") => {
    let temp = formData;
    if (k >= 0 && sk !== "") {
      if (["sale_price", "purchase_price"].indexOf(sk) !== -1) {
        if (!isNaN(val)) {
          temp[key][k][sk] = val;
          let pp =
            temp[key][k]["purchase_price"] === ""
              ? 0
              : parseFloat(temp[key][k]["purchase_price"]);
          let sp =
            temp[key][k]["sale_price"] === ""
              ? 0
              : parseFloat(temp[key][k]["sale_price"]);
          if (sk === "purchase_price") {
            pp = val === "" ? 0 : parseFloat(val);
            temp[key][k]["profit_loss"] = pp - sp;
          } else {
            sp = val === "" ? 0 : parseFloat(val);
            temp[key][k]["profit_loss"] = pp - sp;
          }
        }
      } else {
        temp[key][k][sk] = val;
      }
    } else {
      let tab1Keys = [
        "revenue",
        "expenses",
        "additional_income",
        "deductions",
        "superannuation",
        "spouse_income",
      ];

      if (tab1Keys.indexOf(key) !== -1) {
        if (!isNaN(val)) {
          temp[key] = val;
        }
      } else {
        temp[key] = val;
      }

      //update the tax data report
      if (tab1Keys.indexOf(key) !== -1) {
        taxData[key] = temp[key];
      }
      if (key === "diffrence") {
        taxData.total_capital_gains = temp[key];
      }

      if (
        [
          "revenue",
          "expenses",
          "additional_income",
          "deductions",
          "superannuation",
          "diffrence",
        ].indexOf(key) !== -1
      ) {
        let tempRevenue =
          temp["revenue"] === "" ? 0 : parseFloat(temp["revenue"]);
        let tempExpenses =
          temp["expenses"] === "" ? 0 : parseFloat(temp["expenses"]);
        let tempAdditionalIncome =
          temp["additional_income"] === ""
            ? 0
            : parseFloat(temp["additional_income"]);
        let tempDeductions =
          temp["deductions"] === "" ? 0 : parseFloat(temp["deductions"]);

        let taxableIncome =
          parseFloat(tempRevenue) -
          parseFloat(tempExpenses) +
          parseFloat(tempAdditionalIncome) -
          parseFloat(tempDeductions);

        let tempdiffrence =
          temp["diffrence"] === "" ? 0 : parseFloat(temp["diffrence"]);
        let tempSuperannuation =
          temp["superannuation"] === ""
            ? 0
            : parseFloat(temp["superannuation"]);

        if (temp["capital_gain_tax"]) {
          taxableIncome +=
            parseFloat(tempdiffrence) - parseFloat(tempSuperannuation);
        } else {
          taxableIncome = taxableIncome - parseFloat(tempSuperannuation);
        }
        temp["taxable_income"] = taxableIncome;
        taxData.taxable_income = taxableIncome;
      }
    }

    if (key == "capital_gain_tax" && !val) {
      temp["asset_list"] = [
        {
          description: "",
          owned_12_months: "",
          purchase_price: "",
          sale_price: "",
          profit_loss: "",
        },
      ];
      temp["previous_year_capital_loss"] = taxData
        ? taxData.previous_year_capital_loss
        : 0;
      temp["current_year_capital_gain"] = taxData
        ? taxData.current_year_capital_gain
        : 0;
      temp["diffrence"] = taxData
        ? taxData.current_year_capital_gain - taxData.previous_year_capital_loss
        : 0;
    }

    setFormData(temp);
    setFlag(!flag);
  };

  const addAsset = () => {
    let temp = formData;
    temp["asset_list"].push({
      description: "",
      owned_12_months: "",
      purchase_price: "",
      sale_price: "",
      profit_loss: "",
    });
    setFormData(temp);
    setFlag(!flag);
  };

  const removeAsset = (key) => {
    let temp = formData;
    temp["asset_list"].splice(key, 1);
    setFormData(temp);
    setFlag(!flag);
  };

  const nxtButton = (type) => {
    let error = 0;
    if (currentStep === 0) {
      if (formData["revenue"] === "" || formData["expenses"] === "") {
        error++;
      }
    } else if (currentStep === 1) {
      if (formData["current_year_capital_gain"] === "") {
        error++;
      }
      if (
        formData["capital_gain_tax"] &&
        !!formData["asset_list"] &&
        formData["asset_list"].length > 0 &&
        formData["asset_list"][0]?.description == ""
      ) {
        error++;
      }
    } else if (currentStep === 2) {
      if (
        formData["has_superannuation"] &&
        (formData["superannuation"] === "" || formData["superannuation"] === 0)
      ) {
        error++;
      }
    } else if (currentStep === 3) {
      if (
        formData["spouse_financial_year"] &&
        formData["spouse_income"] === 0
      ) {
        error++;
      }
    }
    if (error > 0) {
      store.store.dispatch(
        snackbarOpen({
          type: SNACKBAR_TYPE.ERROR,
          message: "please fill the mandatory(*) fields",
        })
      );
    } else {
      if (currentStep === 3) {
        if (type === "button") {
          calculateTax();
        }
      } else {
        setCurrentStep(currentStep + 1);
      }
    }
  };

  return (
    <div className="h-screen flex overflow-hidden bg-gray-100">
      <Snackbar type={SNACKBAR_TYPE}></Snackbar>
      <Head>
        <title> Tax Estimator | Sole Accounting made easy</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      {/* For sideBar */}
      <SideDrawer title="tax" />

      <main className="flex-1 relative invoices pb-8 z-0 overflow-y-auto">
        {/* Top Bar */}
        <TopBar title="Tax Estimator" />

        <TokenExpiredDialog />

        <AlertDialog
          id="ringtone-menu2"
          title={alertType}
          message={msg}
          keepMounted
          open={openAlert}
          onClose={setOpenAlert}
        />

        {/* Page header */}
        <div className="mt-8">
          <div className="mx-auto  px-4 sm:px-6 lg:px-8 md:flex md:items-center md:justify-between">
            <h2 className="text-3xl font-extrabold tracking-tight text-gray-900 md:text-4xl">
              <span className="block">Financial Years</span>
              <span className="block mt-1 font-normal text-sm text-gray-500">
                Which financial year do you need to estimate tax for?
              </span>
            </h2>
            <div className="mt-8 flex md:mt-0 lg:flex-shrink-0">
              <Listbox value={financialYear} onChange={setFinancialYear}>
                {({ open }) => (
                  <>
                    <div className="mt-1 relative">
                      <Listbox.Button className="bg-white  relative w-96 border-2 border-gray-300 text-gray-500 rounded-md shadow-sm pl-4 pr-11 py-4 text-left cursor-default focus:outline-none focus:ring-1 focus:ring-indigo-500 focus:border-indigo-500 sm:text-xl">
                        <span className="block truncate">
                          {financialYear ? financialYear.currentyear : ""}
                        </span>
                        <span className="absolute inset-y-0 right-0 flex items-center pr-2 pointer-events-none">
                          <SelectorIcon
                            className="h-5 w-5  text-gray-400"
                            aria-hidden="true"
                          />
                        </span>
                      </Listbox.Button>

                      <Transition
                        show={open}
                        as={Fragment}
                        leave="transition ease-in duration-100"
                        leaveFrom="opacity-100"
                        leaveTo="opacity-0"
                      >
                        <Listbox.Options
                          static
                          className="absolute z-10 mt-1 w-full bg-white shadow-lg max-h-60 rounded-md py-1 text-base ring-1 ring-black ring-opacity-5 overflow-auto focus:outline-none sm:text-md"
                        >
                          {yearList.map((year) => (
                            <Listbox.Option
                              key={year.id}
                              className={({ active }) =>
                                classNames(
                                  active
                                    ? "text-white bg-brand-blue"
                                    : "text-gray-900",
                                  "cursor-default select-none relative py-2 pl-3 pr-9"
                                )
                              }
                              value={year}
                            >
                              {({ selected, active }) => (
                                <>
                                  <span
                                    className={classNames(
                                      selected
                                        ? "font-semibold"
                                        : "font-normal",
                                      "block truncate"
                                    )}
                                  >
                                    {year.currentyear}
                                  </span>

                                  {selected ? (
                                    <span
                                      className={classNames(
                                        active
                                          ? "text-white"
                                          : "text-brand-blue",
                                        "absolute inset-y-0 right-0 flex items-center pr-4"
                                      )}
                                    >
                                      <CheckIcon
                                        className="h-5 w-5"
                                        aria-hidden="true"
                                      />
                                    </span>
                                  ) : null}
                                </>
                              )}
                            </Listbox.Option>
                          ))}
                        </Listbox.Options>
                      </Transition>
                    </div>
                  </>
                )}
              </Listbox>
            </div>
          </div>
        </div>
        {!loader ? (
          <div className="block mt-8 px-4 sm:px-6 lg:px-8">
            <div className="bg-white border-rounded shadow ">
              <div className="bg-white lg:border-t lg:border-b lg:border-gray-200">
                <div className="xl:flex gap-8 p-5 2xl:p-10 2xl:px-10 2xl:py-5">
                  <div className="xl:w-7/12">
                    <div className="shadow-md rounded-md mt-3">
                      <div className="flex items-center  rounded-tl-md rounded-tr-md bg-gray-100 justify-between">
                        <nav
                          className="max-w-s7xl h-20  rounded-tl-md rounded-tr-md"
                          aria-label="Progress"
                        >
                          <ol className="rounded-md grid grid-cols-4 overflow-hidden  lg:border-gray-200 lg:rounded-none">
                            {steps.map((step, stepIdx) => (
                              <li
                                key={step.id}
                                className="absolute overflow-hidden lg:flex-1 step"
                              >
                                <div
                                  className={classNames(
                                    stepIdx === 0
                                      ? "border-b-0 rounded-t-md"
                                      : "",
                                    stepIdx === steps.length - 1
                                      ? "border-t-0 rounded-b-md"
                                      : "",
                                    "overflow-hidden lg:border-0  "
                                  )}
                                >
                                  <a href={step.href} className="group">
                                    <span
                                      className={classNames(
                                        stepIdx !== currentStep ? "hidden" : "",
                                        "px-6 py-5 flex items-start text-sm font-medium"
                                      )}
                                    >
                                      <span className="flex-shrink-0">
                                        {currentStep > stepIdx ? (
                                          <span className="w-10 h-10 flex items-center justify-center bg-brand-blue rounded-full">
                                            <CheckIcon
                                              className="w-6 h-6 text-white"
                                              aria-hidden="true"
                                            />
                                          </span>
                                        ) : (
                                          <span
                                            className={
                                              "w-10 h-10 flex items-center justify-center border-2 border-gray-300 rounded-full " +
                                              (currentStep == stepIdx
                                                ? "border-indigo-600"
                                                : "border-gray-300")
                                            }
                                          >
                                            <span className="text-gray-500">
                                              {step.id}
                                            </span>
                                          </span>
                                        )}
                                      </span>
                                      <span className="mt-0.5 ml-4 min-w-0 flex flex-col">
                                        <span
                                          className={
                                            "text-lg font-semibold  tracking-wide uppercase " +
                                            +(currentStep == stepIdx
                                              ? "text-brand-blue"
                                              : "text-gray-800")
                                          }
                                        >
                                          {step.name}
                                        </span>
                                        <span className="text-sm font-medium text-gray-500">
                                          {step.description}
                                        </span>
                                      </span>
                                    </span>
                                  </a>
                                </div>
                              </li>
                            ))}
                          </ol>
                        </nav>

                        <div>
                          <p className="pr-6 text-brand-navyblue">
                            Step {currentStep + 1} of 4
                          </p>
                        </div>
                      </div>

                      <div className="p-5">
                        <Income
                          currentStep={currentStep}
                          formData={formData}
                          assignedVal={(k, v) => assignedVal(k, v)}
                          flag={flag}
                          financialYear={financialYear}
                        />

                        <CapitalGain
                          currentStep={currentStep}
                          formData={formData}
                          assignedVal={(k, v, k1, k2) =>
                            assignedVal(k, v, k1, k2)
                          }
                          addAsset={() => addAsset()}
                          removeAsset={(key) => removeAsset(key)}
                        />

                        <SuperAnnuation
                          currentStep={currentStep}
                          formData={formData}
                          assignedVal={(k, v) => assignedVal(k, v)}
                          flag={flag}
                        />

                        <Medicare
                          currentStep={currentStep}
                          formData={formData}
                          assignedVal={(k, v) => assignedVal(k, v)}
                          flag={flag}
                        />

                        <div className="mt-6  flex justify-between">
                          {currentStep !== 0 ? (
                            <>
                              <button
                                type="button"
                                className="inline-flex w-32 items-center justify-center px-6 py-3 border border-transparent shadow-sm text-base font-medium rounded-full text-white bg-gray-600 hover:bg-gray-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                                onClick={() => setCurrentStep(currentStep - 1)}
                                disabled={
                                  currentStep === 0 || isSubmit ? true : false
                                }
                                style={
                                  currentStep === 0
                                    ? { cursor: "not-allowed" }
                                    : {}
                                }
                              >
                                <ChevronLeftIcon
                                  className="mr-3 -ml-1 h-5 w-5"
                                  aria-hidden="true"
                                />
                                Back
                              </button>
                            </>
                          ) : (
                            ""
                          )}
                          <button
                            type="button"
                            className={`${
                              isSubmit
                                ? ""
                                : currentStep === 3
                                ? "w-48"
                                : "w-32"
                            } inline-flex  ml-auto mr-0 items-center justify-center px-6 py-3 border border-transparent shadow-sm text-base font-medium rounded-full text-white bg-brand-blue hover:bg-brand-darkblue focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500`}
                            onClick={() => nxtButton("button")}
                            disabled={isSubmit}
                          >
                            {isSubmit
                              ? "Processing..."
                              : currentStep === 3
                              ? "Tax Calculate"
                              : "Next"}
                            <ChevronRightIcon
                              className="ml-3 -mr-1 h-5 w-5"
                              aria-hidden="true"
                            />
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="xl:w-5/12 mt-3">
                    <div className="shadow rounded-md bg-gray-100">
                      <div className="flex p-4 items-center justify-between">
                        <h2 className="text-brand-blue font-semibold text-2xl">
                          Your Results
                          <span className="block mt-0.5 font-normal text-sm text-gray-500">
                            Your estimated tax result
                          </span>
                        </h2>
                      </div>
                      <Result taxData={taxData} />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        ) : (
          <div
            style={{ width: "100%", textAlign: "center", padding: "50px 0px" }}
          >
            <CircularProgress />
          </div>
        )}
      </main>
    </div>
  );
}
