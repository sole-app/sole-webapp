import { Fragment, useState, useEffect } from "react";
import Head from "next/head";
import Link from "next/link";
import { Menu, Transition } from "@headlessui/react";
import { makeStyles } from "@material-ui/core/styles";
import TopBar from "../components/Dashboard/TopBar";
import SideDrawer from "../components/Dashboard/SideBar";
import QuoteSummary from "../components/Quotes/QuoteSummary";
import InvoiceSummary from "../components/Invoices/InvoiceSummary";
import BalanceSummary from "../components/Dashboard/BalanceSummary";
import BalanceChart from "../components/Dashboard/BalanceChart";
import EmptyBalance from "../components/Dashboard/EmptyBalance";
import IntroPopup from "../components/Dashboard/IntroPopup";
import CalculatorCta from "../components/Dashboard/CalculatorCta";
import Cashflow from "../components/Dashboard/Cashflow";
import SoleSnapshot from "../components/Dashboard/SoleSnapshot";
import { getUserData } from "../services/cookies.services";
import WelcomePopup from "../components/Dashboard/WelcomePopup";
import cookies from "next-cookies";
import ReactTooltip from "react-tooltip";
import Skeleton from "react-loading-skeleton";
import "react-loading-skeleton/dist/skeleton.css";
import CircularProgress from "@material-ui/core/CircularProgress";
import {
  ChevronDownIcon,
  DocumentTextIcon,
  InformationCircleIcon,
  NewspaperIcon,
  CollectionIcon,
  UserIcon,
  CurrencyDollarIcon,
} from "@heroicons/react/outline";
import {
  getDashboard,
  setGoal as setGoalData,
} from "../services/api/dashboard.services";
import { ImpulseSpinner } from "react-spinners-kit";
import clsx from "clsx";
import TextField from "@material-ui/core/TextField";
import Styles from "../styles/Register.module.scss";
import TopEarners from "../components/Invoices/TopEarners";
import store from "../store/store";
import { snackbarOpen, snackbarClose } from "../reducers/slices/snackbarSlice";
import { SNACKBAR_TYPE } from "../components/Layout/Snackbar";
import IncomeExpenseChart from "../components/Dashboard/IncomeExpenseChart";
import TokenExpiredDialog from "../components/TokenExpiredDialog";

const months = [
  "Jan",
  "Feb",
  "Mar",
  "Apr",
  "May",
  "Jun",
  "Jul",
  "Aug",
  "Sep",
  "Oct",
  "Nov",
  "Dec",
];

const useStyles = makeStyles((theme) => ({
  paper: {
    padding: theme.spacing(2),
    color: theme.palette.text.secondary,
  },
  btnMargin10: {
    margin: "10px 10px",
  },
}));

export async function getServerSideProps(context) {
  var data = cookies(context);

  if (!data.token && !data.userData) {
    return {
      redirect: {
        permanent: false,
        destination: "/",
      },
    };
  } else {
    return {
      props: data,
    };
  }
}

export default function Dashboard(users) {
  const [userData, setUserData] = useState(getUserData());
  const [setGoal, setSetGoal] = useState(false);
  const [showSetGoal, setShowSetGoal] = useState(false);
  const [amount, setAmount] = useState(0);
  const [dashboard, setDashboard] = useState({
    balance_summary: {},
    invoice_summary: {},
    quote_summary: {},
    bank_balance: 0,
    estimated_tax: 0,
    estimated_tax_calc_date: "",
    snapshot: {},
    credit_entries: [],
    debit_entries: [],
  });
  const [loader, setLoader] = useState(true);
  const [ieData, setIEData] = useState({
    labels: [],
    datasets: [
      {
        label: "Income",
        // backgroundColor: '#4C4DFE',
        // borderColor: '#4445E4',
        // borderWidth: 3,
        fill: true,
        backgroundColor: [
          "rgba(147, 250, 165, 0.2)",
          "rgba(54, 162, 235, 0.2)",
          "rgba(255, 206, 86, 0.2)",
          "rgba(75, 192, 192, 0.2)",
          "rgba(153, 102, 255, 0.2)",
          "rgba(255, 159, 64, 0.2)",
        ],
        borderColor: [
          "rgba(178, 222, 39, 1)",
          "rgba(178, 222, 39, 1)",
          "rgba(178, 222, 39, 1)",
          "rgba(178, 222, 39, 1)",
          "rgba(178, 222, 39, 1)",
          "rgba(178, 222, 39, 1)",
        ],
        borderWidth: 1,
        borderRadius: 3,
        data: [],
      },
      {
        label: "Expense",
        // backgroundColor: '#B0B5C5',
        // borderColor: '#9EA2B1',
        // borderWidth: 3,
        backgroundColor: [
          "rgba(255, 99, 71, 0.2)",
          "rgba(255, 99, 71, 0.2)",
          "rgba(255, 99, 71, 0.2)",
          "rgba(255, 99, 71, 0.2)",
          "rgba(255, 99, 71, 0.2)",
          "rgba(255, 99, 71, 0.2)",
        ],
        borderColor: [
          "rgba(255, 0, 0, 1)",
          "rgba(255, 0, 0, 1)",
          "rgba(255, 0, 0, 1)",
          "rgba(255, 0, 0, 1)",
          "rgba(255, 0, 0, 1)",
          "rgba(255, 0, 0, 1)",
        ],
        borderWidth: 1,
        fill: true,
        borderRadius: 3,
        data: [],
      },
    ],
  });
  const [hashIE, setHashIE] = useState(false);
  const classes = useStyles();

  const getData = async () => {
    setLoader(true);
    const result = await getDashboard();
    if (result.success) {
      setDashboard(result.data);

      if (
        typeof result.data.income !== "undefined" &&
        result.data.income.length > 0
      ) {
        let income = [];
        let expense = [];
        let labels = [];
        for (let i = 0; i < result.data.income.length; i++) {
          if (!hashIE && result.data.income[i].income != 0) {
            setHashIE(true);
          }
          income[i] = result.data.income[i].income;
          labels[i] =
            months[result.data.income[i].month - 1] +
            " " +
            result.data.income[i].year.toString().substring(2);
        }
        for (let i = 0; i < result.data.expense.length; i++) {
          if (!hashIE && result.data.expense[i].income != 0) {
            setHashIE(true);
          }
          expense[i] = result.data.expense[i].income;
        }
        let cData = ieData;
        cData.datasets[0].data = income.reverse();
        cData.datasets[1].data = expense.reverse();
        cData.labels = labels.reverse();
        setIEData(cData);
      }
    }
    setLoader(false);
  };

  const openSetGoal = (goalAmount) => {
    setAmount(goalAmount);
    setShowSetGoal(true);
  };

  const showGraph = () => {
    setShowSetGoal(false);
  };

  const setFinancialGoal = async () => {
    setSetGoal(true);
    const result = await setGoalData(amount);
    if (result.success) {
      store.store.dispatch(
        snackbarOpen({
          type: SNACKBAR_TYPE.SUCCESS,
          message: "Goal is set.",
        })
      );
      setSetGoal(false);
      await getData();
      showGraph();
    } else {
      store.store.dispatch(
        snackbarOpen({
          type: SNACKBAR_TYPE.ERROR,
          message: "Error occurred while setting goal.",
        })
      );
      setSetGoal(false);
    }
  };

  useEffect(() => {
    store.store.dispatch(snackbarClose());
    getData();
  }, []);

  return (
    <div className="h-screen flex overflow-hidden bg-gray-100">
      <Head>
        <title> Dashboard | Sole Accounting made easy</title>
        <link rel="icon" href="/favicon.ico" />
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link
          href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;1,300&display=swap"
          rel="stylesheet"
        />
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link
          href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;200;300;400;500;600&display=swap"
          rel="stylesheet"
        />
      </Head>

      {/* SUBSCRIPTION INFO POPUP */}
      <IntroPopup />

      <WelcomePopup />

      {/* For sideBar */}
      <SideDrawer title="dashboard" key="dashboard" />

      <TokenExpiredDialog />

      <main className="flex-1 relative pb-8 z-0 overflow-y-auto">
        {/* Top Bar */}
        <TopBar title="Dashboard" />

        {/* Page header */}
        <div className="p-7 pb-0">
          <div className="mx-auto  md:flex md:items-center md:justify-between">
            <h2 className="text-3xl font-bold tracking-tight text-gray-900 sm:text-4xl">
              Hi
              <Link href="/settings/profile">
                <a>
                  <span className="font-bold text-brand-blue border-b-2  border-dashed border-gray-500">
                    {userData
                      ? userData.full_name
                        ? " " + userData.full_name
                        : " Sole User"
                      : " Sole User"}
                  </span>
                </a>
              </Link>
            </h2>
            <div className="mt-8 flex md:mt-0 lg:flex-shrink-0">
              <div className="inline-flex rounded-md">
                <Menu as="div" className="ml-3 relative">
                  {({ open }) => (
                    <>
                      <div>
                        <Menu.Button className="inline-flex items-center justify-center pr-3 pl-5 py-1 border border-transparent text-base font-medium rounded-full text-white bg-brand-blue hover:bg-brand-darkblue">
                          Create a new{" "}
                          <ChevronDownIcon className="w-8 h-8 pr-0 pl-2" />
                        </Menu.Button>
                      </div>

                      <Transition
                        show={open}
                        as={Fragment}
                        enter="transition ease-out duration-100"
                        enterFrom="transform opacity-0 scale-95"
                        enterTo="transform opacity-100 scale-100"
                        leave="transition ease-in duration-75"
                        leaveFrom="transform opacity-100 scale-100"
                        leaveTo="transform opacity-0 scale-95"
                      >
                        <Menu.Items
                          static
                          className="origin-top-right z-10 drop-menu absolute right-0 mt-2 w-48 rounded-md shadow-lg p-1 bg-white ring-1 ring-black ring-opacity-5 focus:outline-none"
                        >
                          <Menu.Item>
                            <Link href="/quotes?add=1">
                              <a className="block p-2.5 text-sm lex rounded-md font-semibold flex items-center w-full text-gray-500 hover:bg-brand-blue hover:text-white">
                                <span className="text-indigo-400">
                                  <DocumentTextIcon className="text-gray-400 group-hover:text-gray-600 mr-3 flex-shrink-0 h-6 w-6 group-hover:text-white" />
                                </span>
                                Quote
                              </a>
                            </Link>
                          </Menu.Item>

                          <Menu.Item>
                            <Link href="/invoices?add=1">
                              <a className="block p-2.5 text-sm lex rounded-md font-semibold flex items-center w-full text-gray-500  hover:bg-brand-blue hover:text-white">
                                <span className="text-indigo-400">
                                  <NewspaperIcon className="text-gray-400 group-hover:text-gray-600 mr-3 flex-shrink-0 h-6 w-6 group-hover:text-white" />
                                </span>
                                Invoice
                              </a>
                            </Link>
                          </Menu.Item>

                          <Menu.Item>
                            <Link href="/expenses?add=1">
                              <a className="block p-2.5 text-sm lex rounded-md font-semibold flex items-center w-full text-gray-500  hover:bg-brand-blue hover:text-white">
                                <span className="text-indigo-400">
                                  <CurrencyDollarIcon className="text-gray-400 group-hover:text-gray-600 mr-3 flex-shrink-0 h-6 w-6 group-hover:text-white" />
                                </span>
                                Expense
                              </a>
                            </Link>
                          </Menu.Item>

                          <Menu.Item>
                            <Link href="/clients?add=1">
                              <a className="block p-2.5 text-sm lex rounded-md font-semibold flex items-center w-full text-gray-500 hover:bg-brand-blue hover:text-white">
                                <span className="text-indigo-400">
                                  <UserIcon className="text-gray-400 group-hover:text-gray-600 mr-3 flex-shrink-0 h-6 w-6 group-hover:text-white" />
                                </span>
                                Contact
                              </a>
                            </Link>
                          </Menu.Item>

                          <Menu.Item>
                            <Link href="/assets?add=1">
                              <a className="block p-2.5 text-sm lex rounded-md font-semibold flex items-center w-full text-gray-500 hover:bg-brand-blue hover:text-white">
                                <span className="text-indigo-400">
                                  <CollectionIcon className="text-gray-400 group-hover:text-gray-600 mr-3 flex-shrink-0 h-6 w-6 group-hover:text-white" />
                                </span>
                                Asset
                              </a>
                            </Link>
                          </Menu.Item>
                        </Menu.Items>
                      </Transition>
                    </>
                  )}
                </Menu>
              </div>
            </div>
          </div>
        </div>

        <div className="space-y-8 px-7 items-start grid grid-cols-1 gap-y-2 sm:pt-0 sm:gap-x-8">
          <div className="w-full mt-8">
            {dashboard.bank_balance === 0 ? (
              <EmptyBalance loader={loader} />
            ) : (
              <div className="grid grid-cols-1 h-full">
                <div className="bg-white overflow-hidden shadow rounded-lg">
                  <div className="mx-auto border-b border-grey-400 p-3 sm:p-4  xl:p-5 flex items-center justify-between">
                    <h3 className="text-lg inline-flex items-center font-medium tracking-wide text-gray-900 sm:text-xl">
                      Your Balance
                      <InformationCircleIcon
                        className="h-6 w-6 text-gray-400 ml-2  focus:outline-none"
                        data-for="help"
                        data-tip=""
                      />
                      <ReactTooltip
                        id="help"
                        className="custom-tooltip bg-gray-900"
                        textColor="#ffffff"
                        backgroundColor="#111827"
                        effect="solid"
                        aria-haspopup="true"
                      >
                        <p className="w-64">
                          Link your bank account to Sole and enable a (secure)
                          live bank feed to reconcile your transactions. Do it
                          all in the simplest and Aussie way possible.
                        </p>
                      </ReactTooltip>
                    </h3>

                    <div className="mt-8 flex sm:mt-0 lg:flex-shrink-0">
                      <Link href="/bankaccounts">
                        <a className="inline-flex items-center px-4 py-1.5 border border-indigo-400 text-sm font-medium  rounded-full text-brand-blue bg-gray-50 hover:bg-indigo-100 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-purple-500">
                          View Bank Feed
                        </a>
                      </Link>
                    </div>
                  </div>

                  <div className="grid lg:grid-cols-2">
                    <div className=" border-r border-gray-200">
                      <BalanceSummary
                        bank_balance={dashboard.bank_balance}
                        summary={dashboard.balance_summary}
                        loader={loader}
                      />
                    </div>

                    <div>
                      <BalanceChart
                        loader={loader}
                        credit={dashboard.credit_entries}
                        debit={dashboard.debit_entries}
                      />
                    </div>
                  </div>
                </div>
              </div>
            )}
          </div>
        </div>

        <div className="space-y-8 px-7 items-stretch grid grid-cols-1 gap-y-2 sm:pt-0 xl:grid-cols-2 sm:gap-x-8">
          {/* Latest Quote */}
          <div className="w-full mt-8">
            <div className="grid grid-cols-1 h-full">
              <div className="bg-white overflow-hidden shadow rounded-lg">
                <div className="mx-auto border-b border-grey-400 p-3 sm:p-4  xl:p-5 flex items-center justify-between">
                  <h3 className="text-lg inline-flex items-center font-medium tracking-wide text-gray-900 sm:text-xl">
                    Quotes
                    <InformationCircleIcon
                      className="h-6 w-6 text-gray-400 ml-2  focus:outline-none"
                      data-for="quoteinfo"
                      data-tip=""
                    />
                    <ReactTooltip
                      id="quoteinfo"
                      className="custom-tooltip bg-gray-900"
                      textColor="#ffffff"
                      backgroundColor="#111827"
                      effect="solid"
                      aria-haspopup="true"
                    >
                      <p className="w-64">
                        A summary of the quotes that you have created in Sole.
                        Keep an eye on your 'quotes to convert' to make sure
                        that you are winning work in your sales pipeline!
                      </p>
                    </ReactTooltip>
                  </h3>

                  <div className="mt-8 flex sm:mt-0 lg:flex-shrink-0">
                    {typeof dashboard.quote_summary.quotes !== "undefined" &&
                    dashboard.quote_summary.quotes.length > 0 ? (
                      <Link href="/quotes">
                        <a className="inline-flex items-center px-4 py-1.5 border border-indigo-400 text-sm font-medium  rounded-full text-brand-blue bg-gray-50 hover:bg-indigo-100 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-purple-500">
                          View All
                        </a>
                      </Link>
                    ) : (
                      <>
                        {loader ? (
                          <Skeleton />
                        ) : (
                          <Link href="/quotes?add=1">
                            <a className="inline-flex items-center px-4 py-1.5 border border-indigo-400 text-sm font-medium  rounded-full text-brand-blue bg-gray-50 hover:bg-indigo-100 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-purple-500">
                              Create Quote
                            </a>
                          </Link>
                        )}
                      </>
                    )}
                  </div>
                </div>

                <QuoteSummary
                  quotes={dashboard.quote_summary}
                  loader={loader}
                />
              </div>
            </div>
          </div>

          {/* Latest Invoice */}
          <div className="w-full mt-8">
            <div className="grid grid-cols-1 h-full">
              <div className="bg-white overflow-hidden shadow rounded-lg">
                <div className="mx-auto border-b border-grey-400 p-3 sm:p-4  xl:p-5 flex items-center justify-between">
                  <h3 className="text-lg inline-flex items-center font-medium tracking-wide text-gray-900 sm:text-xl">
                    Invoices
                    <InformationCircleIcon
                      className="h-6 w-6 text-gray-400 ml-2  focus:outline-none"
                      data-for="invoiceinfo"
                      data-tip=""
                    />
                    <ReactTooltip
                      id="invoiceinfo"
                      className="custom-tooltip bg-gray-900"
                      textColor="#ffffff"
                      backgroundColor="#111827"
                      effect="solid"
                      aria-haspopup="true"
                    >
                      <p className="w-64">
                        A summary of the invoices you have raised in Sole. Make
                        sure you monitor your 'due' and 'overdue' invoices to
                        maintain your cashflow, and ensure that your customers
                        are paying you on time!
                      </p>
                    </ReactTooltip>
                  </h3>

                  <div className="mt-8 flex sm:mt-0 lg:flex-shrink-0">
                    {typeof dashboard.invoice_summary.invoices !==
                      "undefined" &&
                    dashboard.invoice_summary.invoices.length > 0 ? (
                      <Link href="/invoices">
                        <a className="inline-flex items-center px-4 py-1.5 border border-indigo-400 text-sm font-medium  rounded-full text-brand-blue bg-gray-50 hover:bg-indigo-100 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-purple-500">
                          View All
                        </a>
                      </Link>
                    ) : (
                      <>
                        {loader ? (
                          <Skeleton />
                        ) : (
                          <Link href="/invoices?add=1">
                            <a className="inline-flex items-center px-4 py-1.5 border border-indigo-400 text-sm font-medium  rounded-full text-brand-blue bg-gray-50 hover:bg-indigo-100 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-purple-500">
                              Create Invoice
                            </a>
                          </Link>
                        )}
                      </>
                    )}
                  </div>
                </div>

                <InvoiceSummary
                  invoices={dashboard.invoice_summary}
                  loader={loader}
                />
              </div>
            </div>
          </div>
        </div>

        <div className="space-y-8 px-7 items-stretch grid grid-cols-1 gap-y-2 sm:pt-0 xl:grid-cols-2 sm:gap-x-8">
          <Cashflow />

          {/* Latest Invoice */}

          {loader ? (
            <div className=" mt-8">
              <div className="grid grid-cols-1 h-full">
                <div className="bg-white overflow-hidden shadow rounded-lg">
                  <div className="mx-auto border-b border-grey-400 p-3 sm:p-4  xl:p-5  flex items-center justify-between">
                    <h3 className="text-lg inline-flex items-center font-medium tracking-wide text-gray-900 sm:text-xl">
                      Sole Snapshot
                      <InformationCircleIcon
                        className="h-6 w-6 text-gray-400 ml-2  focus:outline-none"
                        data-for="bankinginfo"
                        data-tip=""
                      />
                      <ReactTooltip
                        id="bankinginfo"
                        className="custom-tooltip bg-gray-900"
                        textColor="#ffffff"
                        backgroundColor="#111827"
                        effect="solid"
                        aria-haspopup="true"
                      >
                        <p className="w-64">
                          The Snapshot graph allows you to set your financial
                          goal and it compares your performance against this
                          goal. It helps keep you on track!
                          <br></br>
                          <br></br>
                          Your performance in the graph updates as you match
                          transactions in your bank account.
                        </p>
                      </ReactTooltip>
                      <ReactTooltip
                        id="help"
                        className="custom-tooltip bg-gray-900"
                        border
                        textColor="#ffffff"
                        backgroundColor="#111827"
                        effect="solid"
                      />
                    </h3>
                  </div>

                  <div className="flex items-center w-full py-6 justify-center rounded-md">
                    <div className="text-center">
                      <div className="box-center" style={{ height: "130px" }}>
                        <CircularProgress />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          ) : (
            <>
              {dashboard.snapshot &&
              Object.keys(dashboard.snapshot).length > 0 &&
              !showSetGoal ? (
                <SoleSnapshot
                  snapshotdata={dashboard.snapshot}
                  openSetGoal={openSetGoal}
                />
              ) : dashboard.bank_balance > 0 || showSetGoal ? (
                <div className="w-full mt-8">
                  <div className="grid grid-cols-1 h-full">
                    <div className="bg-white overflow-hidden shadow rounded-lg">
                      <div className="mx-auto border-b border-grey-400 p-3 sm:p-4  xl:p-5  flex items-center justify-between">
                        <h3 className="text-lg inline-flex items-center font-medium tracking-wide text-gray-900 sm:text-xl">
                          What is your income goal for this financial year?
                          <InformationCircleIcon
                            className="h-6 w-6 text-gray-400 ml-2  focus:outline-none"
                            data-for="help"
                            data-tip="Set your income goal here here"
                          />
                          <ReactTooltip
                            id="help"
                            className="custom-tooltip bg-gray-900"
                            border
                            textColor="#ffffff"
                            backgroundColor="#111827"
                            effect="solid"
                          />
                        </h3>
                      </div>
                      <div className="py-10 px-4 ">
                        <TextField
                          name="goal_amount"
                          id="goal_amount"
                          label="Goal Amount"
                          type="number"
                          variant="outlined"
                          fullWidth={true}
                          className={classes.fControl}
                          value={amount}
                          onChange={(e) => setAmount(e.target.value)}
                        />
                      </div>
                      <div className="pb-5 px-4 mt-5 sm:mt-4 sm:flex">
                        <button
                          type="button"
                          className={`${clsx(
                            Styles.roundBtn,
                            "mtop0",
                            " inline-flex items-center px-4 py-1.5 border border-indigo-400 text-sm font-medium  rounded-full text-brand-blue bg-gray-50 hover:bg-indigo-100 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-purple-500"
                          )} ${classes.btnMargin10}`}
                          onClick={setFinancialGoal}
                          disabled={amount == 0}
                        >
                          Set Goal&nbsp;&nbsp;
                          {setGoal && (
                            <ImpulseSpinner
                              frontColor={"blue"}
                              backColor="rgba(255,255,255, 0.5)"
                            />
                          )}
                        </button>

                        {showSetGoal && (
                          <button
                            type="button"
                            className={`${clsx(
                              Styles.roundBtn,
                              "mtop0",
                              " inline-flex items-center px-4 py-1.5 border border-indigo-400 text-sm font-medium  rounded-full text-brand-blue bg-gray-50 hover:bg-indigo-100 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-purple-500"
                            )} ${classes.btnMargin10}`}
                            onClick={showGraph}
                          >
                            Cancel&nbsp;&nbsp;
                          </button>
                        )}
                      </div>
                    </div>
                  </div>
                </div>
              ) : (
                <div className=" mt-8">
                  <div className="grid grid-cols-1 h-full">
                    <div className="bg-white overflow-hidden shadow rounded-lg">
                      <div className="mx-auto border-b border-grey-400 p-3 sm:p-4  xl:p-5  flex items-center justify-between">
                        <h3 className="text-lg inline-flex items-center font-medium tracking-wide text-gray-900 sm:text-xl">
                          Sole Snapshot
                          <InformationCircleIcon
                            className="h-6 w-6 text-gray-400 ml-2  focus:outline-none"
                            data-for="bankinginfo"
                            data-tip=""
                          />
                          <ReactTooltip
                            id="bankinginfo"
                            className="custom-tooltip bg-gray-900"
                            textColor="#ffffff"
                            backgroundColor="#111827"
                            effect="solid"
                            aria-haspopup="true"
                          >
                            <p className="w-64">
                              The Snapshot graph allows you to set your
                              financial goal and it compares your performance
                              against this goal. It helps keep you on track!
                              <br></br>
                              <br></br>
                              Your performance in the graph updates as you match
                              transactions in your bank account.
                            </p>
                          </ReactTooltip>
                          <ReactTooltip
                            id="help"
                            className="custom-tooltip bg-gray-900"
                            border
                            textColor="#ffffff"
                            backgroundColor="#111827"
                            effect="solid"
                          />
                        </h3>
                      </div>

                      <div className="flex items-center w-full py-6 justify-center rounded-md">
                        <div className="py-10 px-4">
                          <h3 className="my-4 text-xl font-medium text-gray-700">
                            {" "}
                            Please connect your bank to view graph
                          </h3>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              )}
            </>
          )}
        </div>

        <div className="space-y-8 px-7  items-stretch  grid grid-cols-1 gap-y-2 sm:pt-0 xl:grid-cols-2 sm:gap-x-8">
          <IncomeExpenseChart loader={loader} ieData={ieData} hashIE={hashIE} />
          <TopEarners from="dashboard" refreshSummary={true} />
        </div>

        {!dashboard.estimated_tax_calc_date && (
          <div className="space-y-8 px-7 items-start grid grid-cols-1 gap-y-2 sm:pt-0 xl:grid-cols-1 sm:gap-x-8">
            <CalculatorCta />
          </div>
        )}

        {dashboard.estimated_tax_calc_date && (
          <div className="space-y-8 px-7 items-streach grid grid-cols-1 sm:pt-0 xl:grid-cols-1 sm:gap-x-8">
            <div className="mt-8">
              <div className="bg-brand-navyblue rounded-md">
                <div className="max-w-full mx-auto py-10 px-4 sm:px-6 3xl:py-12 3xl:px-12 lg:flex lg:items-center lg:justify-between">
                  <div className="xl:flex justify-between items-center  w-full">
                    <div className="flex justify-between items-center">
                      <div className="mr-10">
                        <img
                          src="/images/tax-income-calc.svg"
                          alt="Sole logo"
                          width="80"
                          height="80"
                          className="w-auto"
                        />

                        {/* <AiOutlineCalculator className="h-32 w-32" /> */}
                      </div>

                      <div>
                        <h2 className="text-2xl font-semibold text-white lg:text-3xl 2xl:text-4xl 3xl:text-4xl">
                          <span className="block">
                            Your taxable income estimate{" "}
                          </span>
                        </h2>

                        <div className="pt-3 flex justify-between items-start">
                          <span className="block font-light text-lg text-white">
                            Your predicted tax payment, based on your revenue
                            and expenses
                          </span>

                          <InformationCircleIcon
                            className="h-8 w-8 text-white ml-2  rounded-full p-1"
                            data-for="profile-image"
                            data-tip
                          />
                          <ReactTooltip
                            id="profile-image"
                            className="custom-tooltip bg-gray-900"
                            textColor="#ffffff"
                            backgroundColor="#111827"
                            effect="solid"
                            aria-haspopup="true"
                          >
                            <p className="w-64">
                              The tax estimator and all other analytical tools
                              provide an estimate to help you forecast your
                              cash-flows and understand your business finances.
                              Actual result may differ. You are able to share
                              your tax summary with your nominated tax agent
                              directly from sole to make things simple (access
                              to an accountant/bookkeeper portal coming soon)
                            </p>
                          </ReactTooltip>
                        </div>
                      </div>
                    </div>

                    <div>
                      <div className="mt-5 2xl:mt-7 mr-8 flex w-full">
                        <div className="rounded-md  bg-blue-pill cursor-pointer">
                          <Link href="/tax-estimator">
                            <div className="flex  items-center shadow bg-[#000836] justify-between p-3 text-xl border border-transparent text-md 2xl:text-2xl font-semibold rounded-md  text-white hover:bg-black-100">
                              <span>
                                {dashboard.estimated_tax == 0
                                  ? "No estimated tax payment expected!"
                                  : "$" + dashboard.estimated_tax}
                              </span>

                              {/* <a className="inline-flex items-center shadow justify-center px-5 py-3 border border-transparent text-md 2xl:text-xl font-semibold rounded-full text-gray-700 bg-white hover:bg-gray-100">
																	Go to Tax Estimator
																</a> */}
                              <div className="text-black ml-4 mr-2 xl:ml-12 2xl:ml-24 3xl:ml-28">
                                <img
                                  src="/images/icon/arrow.svg"
                                  alt="Sole logo"
                                  width="30"
                                  height="30"
                                  className="w-auto"
                                />
                              </div>
                            </div>
                          </Link>
                        </div>
                      </div>

                      <div className="flex justify-center items-center w-full  p-5">
                        <div className="block font-light text-white">
                          Calculated on {dashboard.estimated_tax_calc_date}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        )}

        <div className="space-y-8 px-7 items-streach grid grid-cols-1 sm:pt-0 xl:grid-cols-1 sm:gap-x-8">
          <div className="mt-8 items-streach grid grid-cols-1 gap-y-8 sm:pt-0 xl:grid-cols-2 sm:gap-x-8">
            <div className=" bg-brand-blue rounded-md">
              <Link href="/partners">
                <a>
                  <div>
                    <div className="max-w-full mx-auto py-10 px-4 sm:px-6 2xl:py-12 3xl:py-16 3xl:px-12 lg:flex lg:items-center lg:justify-between">
                      <div className="flex justify-between gap-5 items-center 2xl:w-auto">
                        <div>
                          <h2 className="text-2xl font-semibold text-white lg:text-3xl 2xl:text-4xl 3xl:text-4xl">
                            <span className="block">Partner Offers </span>
                          </h2>

                          <div className="pt-3">
                            <span className="block font-light text-white text-lg">
                              Get added benefits as a Sole member through our
                              partnership offerings.
                            </span>
                          </div>
                        </div>

                        <div className="text-white">
                          <img
                            src="/images/icon/image_2022_09_10T09_04_10_184Z.svg"
                            alt="Sole logo"
                            className="w-60 2xl:w-72 3xl:w-72"
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </a>
              </Link>
            </div>

            <div className=" bg-green-600 rounded-md">
              <Link href="https://soleapp.com.au/accountants-and-bookkeepers/">
                <a target="_blank">
                  <div className="h-full">
                    <div className="max-w-full h-full mx-auto py-10 px-4 sm:px-6 2xl:py-12 3xl:py-16 3xl:px-12 lg:flex lg:items-center lg:justify-between">
                      <div className="flex justify-between gap-10 items-center 2xl:w-auto">
                        <div>
                          <h2 className="text-2xl font-semibold text-white lg:text-3xl 2xl:text-4xl 3xl:text-4xl">
                            <span className="block">
                              Find a bookkeeper<br></br> or accountant{" "}
                            </span>
                          </h2>

                          <div className="pt-3">
                            <span className="block font-light text-white text-lg">
                              Find the right advisor to support you and your
                              business.
                            </span>
                          </div>
                        </div>

                        <div className="text-white ml-4">
                          <img
                            src="/images/icon/image_2022_09_10T09_04_24_309Z.png"
                            alt="Sole logo"
                            className="w-60 2xl:w-72  3xl:w-72"
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </a>
              </Link>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
}
