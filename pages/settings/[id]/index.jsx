import Head from 'next/head';
import { useState } from 'react';
import { useRouter } from 'next/router';
import TopBar from '../../../components/Dashboard/TopBar';
import SideDrawer from '../../../components/Dashboard/SideBar';
import SettingSidebar from '../../../components/Settings/SettingSidebar';
import cookies from 'next-cookies';
import Snackbar, { SNACKBAR_TYPE } from "../../../components/Layout/Snackbar";

export async function getServerSideProps(context) {
	let data = cookies(context);

	if (!data.token && !data.userData) {
		return {
			redirect: {
				permanent: false,
				destination: "/",
			},
		}
	}
	else {
		return {
			props: data,
		}
	}
}

export default function Settings() {
	const [user, setUser] = useState({});
	const router = useRouter();
	const { id } = router.query;

	return (
		<div className="h-screen flex overflow-hidden bg-gray-100">
			<Snackbar type={SNACKBAR_TYPE} />

			<Head>
				<title> Settings | Sole Accounting made easy</title>
				<link rel='icon' href='/favicon.ico' />
			</Head>

			{/* For sideBar */}
			<SideDrawer title="settings" user={user} />

			<main className="flex-1 relative invoices  z-0 overflow-y-auto">
				{/* Top Bar */}
				<TopBar title="Settings" user={user} />

				{/* Setting screen options */}
				<SettingSidebar settingType={id} setUser={setUser} />
			</main>
		</div>
	);
}
