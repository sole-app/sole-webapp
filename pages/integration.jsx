import Head from 'next/head';
import TopBar from '../components/Dashboard/TopBar';
import SideDrawer from '../components/Dashboard/SideBar';
import IntegrationStep from '../components/Integration/IntegrationStep';
import TokenExpiredDialog from '../components/TokenExpiredDialog';

export default function Integration() {

	return (
		<div className="h-screen flex overflow-hidden bg-gray-100">
			<Head>
				<title> Integration | Sole Accounting made easy</title>
				<link rel='icon' href='/favicon.ico' />
			</Head>

			{/* For sideBar */}
			<SideDrawer title="integration" />

			<TokenExpiredDialog />

			<main className="flex-1 relative invoices pb-8 z-0 overflow-y-auto">
				{/* Top Bar */}
				<TopBar title="Integration" />

				{/* Page header */}
				<div className="p-7">
					<IntegrationStep />
				</div>
			</main>

		</div>
	);
}
