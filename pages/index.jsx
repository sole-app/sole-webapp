import AuthContent from '../components/Auth/AuthContent';

export default function Home() {
	return (
		<AuthContent type="signin" />
	);
}