import Head from "next/head";
import TopBar from "../components/Dashboard/TopBar";
import SideDrawer from "../components/Dashboard/SideBar";
import ExpensesAnalytic from "../components/Expenses/ExpensesAnalytic";
import TopExpenses from "../components/Expenses/TopExpenses";
import ExpensesList from "../components/Expenses/ExpensesList";
import ExpensesInfo from "../components/Expenses/ExpensesInfo";
import AddExpenses from "../components/Expenses/AddExpenses";
import EmptyView from "../components/Expenses/EmptyView";
import { IoIosAddCircleOutline } from "react-icons/io";
import { useState, useEffect } from "react";
import Snackbar, { SNACKBAR_TYPE } from "../components/Layout/Snackbar";
import config from "../config/config";
import { getSpecificCategories } from "../services/api/accountCategory.services";
import cookies from "next-cookies";
import { List } from "../services/api/expense.services";
import CircularProgress from "@material-ui/core/CircularProgress";
import { useRouter } from "next/router";
import { snackbarClose } from "../reducers/slices/snackbarSlice";
import store from "../store/store";
import TokenExpiredDialog from "../components/TokenExpiredDialog";

export async function getServerSideProps(context) {
  var data = cookies(context);

  if (!data.token && !data.userData) {
    return {
      redirect: {
        permanent: false,
        destination: "/",
      },
    };
  } else {
    return {
      props: data,
    };
  }
}

export default function Expenses() {
  const router = useRouter();
  const [loader, setLoader] = useState(false);
  const [isOpen, setIsOpen] = useState(false);
  const [isListUpdate, setIsListUpdate] = useState(true);
  const [isInfoOpen, setIsInfoOpen] = useState(false);
  const [updateId, setUpdateId] = useState("");
  const [accountCategories, setAccountCategories] = useState([]);
  const [isEmptyView, setEmptyView] = useState(true);
  const [refreshSummary, setRefreshSummary] = useState(false);

  const getAccountCategories = async () => {
    if (accountCategories.length === 0) {
      const categories = await getSpecificCategories([
        config.expanseCategoryId,
      ]);
      setAccountCategories(categories);
    }
  };

  const getExpenseList = async () => {
    setLoader(true);
    const formData = "?page_size=1";

    const result = await List(formData);
    if (result.success) {
      if (Object.keys(result.data.expense_details).length > 0) {
        setEmptyView(false);
      }
    }
    setLoader(false);
  };

  useEffect(() => {
    store.store.dispatch(snackbarClose());
    getExpenseList();
    getAccountCategories();
  }, []);

  useEffect(async () => {
    if (
      typeof router.query !== "undefined" &&
      typeof router.query.add !== "undefined" &&
      router.query.add
    ) {
      await getAccountCategories();
      setIsOpen(true);
    }
  }, [router.query]);

  return (
    <div className="h-screen flex overflow-hidden bg-gray-100">
      <Snackbar type={SNACKBAR_TYPE}></Snackbar>

      <Head>
        <title> Expenses | Sole Accounting made easy</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      {/* For sideBar */}
      <SideDrawer title="expenses" />

      <TokenExpiredDialog />

      <main className="flex-1 relative invoices pb-8 z-0 overflow-y-auto">
        {/* Top Bar */}
        <TopBar title="Expenses" />

        {/* Page header */}
        <div className="mt-8">
          <div className="mx-auto  px-4 sm:px-6 lg:px-8 md:flex md:items-center md:justify-between">
            <div>
              <h2 className="text-3xl font-extrabold tracking-wide text-gray-900 sm:text-4xl">
                Expenses
              </h2>
              {/* <p className="mt-1 font-normal text-sm text-gray-500">
								List of all of your Expenses.
							</p> */}
            </div>

            <div className="mt-8 flex md:mt-0 lg:flex-shrink-0">
              <div className="inline-flex rounded-md">
                <button
                  className="inline-flex items-center justify-center uppercase tracking-wide pr-5 pl-2 py-1 border border-transparent text-sm  rounded-full text-white bg-brand-blue hover:bg-brand-darkblue"
                  onClick={() => {
                    setUpdateId("");
                    setIsOpen(!isOpen);
                  }}
                >
                  <IoIosAddCircleOutline className="w-9 h-9 pr-2 pl-0" /> New
                  Expense
                </button>
              </div>
            </div>
          </div>
        </div>

        {loader ? (
          <div className="box-center">
            <CircularProgress />
          </div>
        ) : isEmptyView ? (
          <>
            <EmptyView />
          </>
        ) : (
          <>
            <div className="block xl:flex justify-between flex-1 px-4 sm:px-6 lg:px-8">
              <ExpensesAnalytic refreshSummary={refreshSummary} />
              <TopExpenses refreshSummary={refreshSummary} />
            </div>

            <div className="block  px-4 sm:px-6 lg:px-8">
              <ExpensesList
                isListUpdate={isListUpdate}
                refreshSummary={refreshSummary}
                setIsListUpdate={setIsListUpdate}
                setIsOpen={setIsOpen}
                setUpdateId={setUpdateId}
                setIsInfoOpen={setIsInfoOpen}
                accountCategories={accountCategories}
                setEmptyView={setEmptyView}
                setRefreshSummary={setRefreshSummary}
              />
            </div>
          </>
        )}

        {/* Sideover for Add/Edit Expenses */}
        <AddExpenses
          isOpen={isOpen}
          refreshSummary={refreshSummary}
          accountCategories={accountCategories}
          setIsListUpdate={setIsListUpdate}
          setIsOpen={setIsOpen}
          updateId={updateId}
          setEmptyView={setEmptyView}
          setRefreshSummary={setRefreshSummary}
        />

        <ExpensesInfo
          isInfoOpen={isInfoOpen}
          setIsInfoOpen={setIsInfoOpen}
          updateId={updateId}
        />
      </main>
    </div>
  );
}
